﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.IO;
using UbiWeb.Data;
using UbiWeb.Services;

namespace UbiSqlFramework
{
    public static class UbiServiceHost
    {
        /*
         * This class acts as the intermediary between the FunctionHost and the client.
         * This includes converting DataTables to response types, etc.
         * 
         */


        static UbiServiceHost()
        {

        }

        public static TransactionResponse ProcessTransaction(Transaction t)
        {
            try
            {
                return TransactionHost.ProcessTransaction(t);
            }
            catch (Exception ex)
            {
                return new TransactionResponse(false, ex.Message);
            }
        }

        public static LoginResponse LoginUser(string sUn, string sPwd)
        {
            Single nTemp;
            try
            {
                DataRow dr = SqlHost.GetUserProfile(sUn, sPwd);
                if (dr == null)
                {
                    LoginResponse r = new LoginResponse(false);
                    return r;
                }
                else
                {
                    LoginResponse r = new LoginResponse(true);
                    r.Response.UserName = dr["UserName"].ToString();
                    r.Response.FirstName = dr["FirstName"].ToString();
                    r.Response.Surname = dr["Surname"].ToString();
                    r.Response.PrefName = dr["PrefName"].ToString();
                    r.Response.Email = dr["Email"].ToString();
                    r.Response.Phone1 = dr["Phone1"].ToString();
                    r.Response.Phone2 = dr["Phone2"].ToString();
                    if (Single.TryParse(dr["SecurityLevel"].ToString(), out nTemp))
                    {
                        r.Response.SecurityLevel = nTemp;
                    }
                    else
                    {
                        r.Response.SecurityLevel = 0;
                    }
                    return r;
                }
            }
            catch (Exception ex)
            {
                LoginResponse r = new LoginResponse(false, ex.Message);
                return r;
            }
        }

        //public static BoolResponse GetUser(string sUserName, string sPassword)
        //{
        //    try
        //    {
        //        bool b = FunctionHost.GetUser(sUserName, sPassword);
        //        BoolResponse r = new BoolResponse(true);
        //        r.Response = b;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        BoolResponse r = new BoolResponse(false, ex.Message);
        //        return r;
        //    }

        //}

        //public static StringResponse GetNextCBASqlServer(string sId)
        //{
        //    try
        //    {
        //        string newId = FunctionHost.GetNextCBASqlServer(sId);
        //        StringResponse r = new StringResponse(true);
        //        r.Response = newId;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new StringResponse(false, ex.Message);
        //    }

        //}

        //public static SupplierSearchResultResponse SupplierSearch(string companyId, string searchExpression, string parentId)
        //{
        //    try
        //    {
        //        DataTable d = FunctionHost.SupplierSearch(searchExpression, parentId, companyId);
        //        SupplierSearchResultResponse r = new SupplierSearchResultResponse(true);
        //        r.Response.SearchString = searchExpression;
        //        r.Response.ParentId = parentId;
        //        foreach (DataRow dr in d.Rows)
        //        {
        //            SupplierSearchItem i = new SupplierSearchItem();
        //            i.SupplierId = dr["SupplierId"].ToString();
        //            i.Name1 = dr["Name1"].ToString();
        //            i.Contact = dr["Contact"].ToString();
        //            i.ParentId = dr["ParentId"].ToString();
        //            i.ParentName = dr["ParentName"].ToString();
        //            r.Response.SupplierList.Add(i);
        //        }
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        SupplierSearchResultResponse r = new SupplierSearchResultResponse(false, ex.Message);
        //        return r;
        //    }
        //}

        //public static SupplierResponse GetSupplier(string companyId, string supplierId)
        //{
        //    try
        //    {
        //        Supplier s = FunctionHost.GetSupplier(supplierId, companyId);
        //        SupplierResponse r = new SupplierResponse(true);
        //        r.Response = s;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        SupplierResponse r = new SupplierResponse(false, ex.Message);
        //        return r;
        //    }
        //}

        //public static CustomerSearchResultResponse CustomerSearch(string companyId, string searchExpression)
        //{
        //    try
        //    {
        //        DataTable d = FunctionHost.CustomerSearch(searchExpression, companyId);
        //        CustomerSearchResultResponse r = new CustomerSearchResultResponse(true);
        //        r.Response.SearchString = searchExpression;
        //        foreach (DataRow dr in d.Rows)
        //        {
        //            CustomerSearchItem i = new CustomerSearchItem();
        //            i.CustomerId = dr["CustomerId"].ToString();
        //            i.Name1 = dr["Name1"].ToString();
        //            i.Name2 = dr["Name2"].ToString();
        //            i.BusinessName = dr["BusinessName"].ToString();
        //            r.Response.SupplierList.Add(i);
        //        }
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        CustomerSearchResultResponse r = new CustomerSearchResultResponse(false, ex.Message);
        //        return r;
        //    }
        //}
        //public static CustomerResponse GetCustomer(string companyId, string customerId)
        //{
        //    try
        //    {
        //        Customer s = FunctionHost.GetCustomer(customerId, companyId);
        //        CustomerResponse r = new CustomerResponse(true);
        //        r.Response = s;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        CustomerResponse r = new CustomerResponse(false, ex.Message);
        //        return r;
        //    }
        //}
        //public static CustomerAddressResponse GetCustomerAddress(string customerId, string addressId)
        //{
        //    try
        //    {
        //        CustomerAddress s = FunctionHost.GetCustomerAddress(customerId, addressId);
        //        CustomerAddressResponse r = new CustomerAddressResponse(true);
        //        r.Response = s;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        CustomerAddressResponse r = new CustomerAddressResponse(false, ex.Message);
        //        return r;
        //    }
        //}
        //public static CustomerContactResponse GetCustomerContact(string customerId, string contactId)
        //{
        //    try
        //    {
        //        CustomerContact s = FunctionHost.GetCustomerContact(customerId, contactId);
        //        CustomerContactResponse r = new CustomerContactResponse(true);
        //        r.Response = s;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        CustomerContactResponse r = new CustomerContactResponse(false, ex.Message);
        //        return r;
        //    }
        //}
        //public static CustomerOrderTermsResponse GetCustomerOrderTerms(string orderTermsId)
        //{
        //    try
        //    {
        //        CustomerOrderTerms s = FunctionHost.GetCustomerOrderTerms(orderTermsId);
        //        CustomerOrderTermsResponse r = new CustomerOrderTermsResponse(true);
        //        r.Response = s;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        CustomerOrderTermsResponse r = new CustomerOrderTermsResponse(false, ex.Message);
        //        return r;
        //    }
        //}
        //public static ContextProfileResponse GetContextProfile(string contextId, string currencyId, string countryId)
        //{
        //    try
        //    {
        //        ContextProfile s = FunctionHost.GetContextProfile(contextId, currencyId, countryId);
        //        ContextProfileResponse r = new ContextProfileResponse(true);
        //        r.Response = s;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        ContextProfileResponse r = new ContextProfileResponse(false, ex.Message);
        //        return r;
        //    }
        //}

        //public static DocTemplateResponse GetDocTemplate(string templateCode)
        //{
        //    try
        //    {
        //        DocTemplate s = FunctionHost.GetDocTemplate(templateCode);
        //        DocTemplateResponse r = new DocTemplateResponse(true);
        //        r.Response = s;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        DocTemplateResponse r = new DocTemplateResponse(false, ex.Message);
        //        return r;
        //    }
        //}

        //public static FileReturnResponse SetFileReturn(string PrintFile)
        //{
        //    try
        //    {
        //        FileReturn s = FunctionHost.SetFileReturn(PrintFile);
        //        FileReturnResponse r = new FileReturnResponse(true);
        //        r.Response = s;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        FileReturnResponse r = new FileReturnResponse(false, ex.Message);
        //        return r;
        //    }
        //}

        //public static StockResponse GetStock(string stockCode, string lotId, string whsId)
        //{
        //    try
        //    {
        //        Stock cStock = FunctionHost.GetStock(stockCode);
        //        StockLot cStockLot = FunctionHost.GetStockLot(stockCode, lotId);
        //        StockWhs cStockWhs = FunctionHost.GetStockWhs(stockCode, whsId);
        //        StockLotWhs cStockLotWhs = FunctionHost.GetStockLotWhs(stockCode, lotId, whsId);

        //        StockResponse r = new StockResponse(true);
        //        r.Response.StockCode = cStock.StockCode;
        //        r.Response.Description = cStock.Description;
        //        r.Response.Stock = cStock;
        //        r.Response.StockLot = cStockLot;
        //        r.Response.StockWhs = cStockWhs;
        //        r.Response.StockLotWhs = cStockLotWhs;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        StockResponse r = new StockResponse(false, ex.Message);
        //        return r;
        //    }
        //}

        //public static BoolResponse POExists(string poNum)
        //{
        //    try
        //    {
        //        bool b = FunctionHost.POExists(poNum);
        //        BoolResponse r = new BoolResponse(true);
        //        r.Response = b;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        BoolResponse r = new BoolResponse(false, ex.Message);
        //        return r;
        //    }

        //}
        //public static PurchaseOrderResponse RetrievePurchaseOrder(string poNum, bool byRef, bool headerOnly)
        //{
        //    try
        //    {
        //        POHeader cPO = FunctionHost.RetrievePO(poNum, byRef, headerOnly);
        //        PurchaseOrderResponse r = new PurchaseOrderResponse(true);
        //        r.Response = cPO;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        PurchaseOrderResponse r = new PurchaseOrderResponse(false, ex.Message);
        //        return r;
        //    }

        //}

        //public static BoolResponse OEExists(string orderNo)
        //{
        //    try
        //    {
        //        bool b = FunctionHost.OEExists(orderNo);
        //        BoolResponse r = new BoolResponse(true);
        //        r.Response = b;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        BoolResponse r = new BoolResponse(false, ex.Message);
        //        return r;
        //    }

        //}
        //public static SalesOrderResponse RetrieveSalesOrder(string orderNo, bool headerOnly)
        //{
        //    try
        //    {
        //        OEHeader cOE = FunctionHost.RetrieveOE(orderNo, headerOnly);
        //        SalesOrderResponse r = new SalesOrderResponse(true);
        //        r.Response = cOE;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        SalesOrderResponse r = new SalesOrderResponse(false, ex.Message);
        //        return r;
        //    }

        //}


        //public static BoolResponse MasterfilesJobExists(string jobNo)
        //{
        //    try
        //    {
        //        bool b = FunctionHost.MasterfilesJobExists(jobNo);
        //        BoolResponse r = new BoolResponse(true);
        //        r.Response = b;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        BoolResponse r = new BoolResponse(false, ex.Message);
        //        return r;
        //    }

        //}

        //public static MasterfilesJobSearchResultResponse MasterfilesJobSearch(string jobNo)
        //{
        //    try
        //    {
        //        DataTable d = FunctionHost.MasterfilesJobSearch(jobNo);
        //        MasterfilesJobSearchResultResponse r = new MasterfilesJobSearchResultResponse(true);
        //        r.Response.SearchString = jobNo;
        //        foreach (DataRow dr in d.Rows)
        //        {
        //            MasterfilesJobSearchItem i = new MasterfilesJobSearchItem();
        //            i.JobId = (Guid)dr["JobId"];
        //            i.PoNum = dr["PoNum"].ToString();
        //            i.ShipRef = dr["ShipRef"].ToString();
        //            i.JobStatus = dr["JobStatus"].ToString();
        //            i.FCL = dr["FCL"].ToString();
        //            i.GoodsDesc = dr["GoodsDesc"].ToString();
        //            if (dr["ETD"] != System.DBNull.Value)
        //            {
        //                i.ETD = (DateTime)dr["ETD"];
        //            }
        //            else
        //            {
        //                i.ETD = new DateTime(1901, 1, 1);
        //            }
        //            if (dr["ETA"] != System.DBNull.Value)
        //            {
        //                i.ETA = (DateTime)dr["ETA"];
        //            }
        //            else
        //            {
        //                i.ETA = new DateTime(1901, 1, 1);
        //            }
        //            i.Boss = dr["Boss"].ToString();
        //            r.Response.JobList.Add(i);
        //        }
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        MasterfilesJobSearchResultResponse r = new MasterfilesJobSearchResultResponse(false, ex.Message);
        //        return r;
        //    }
        //}
        //public static MasterfilesJobResponse RetrieveMasterfilesJob(string jobNo)
        //{
        //    try
        //    {
        //        MasterfilesJob j = FunctionHost.RetrieveMasterfilesJob(jobNo);
        //        MasterfilesJobResponse r = new MasterfilesJobResponse(true);
        //        r.Response = j;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        MasterfilesJobResponse r = new MasterfilesJobResponse(false, ex.Message);
        //        return r;
        //    }
        //}
        //public static ReportQuickHeaderResponse GetReportQuickHeader(string reportId)
        //{
        //    try
        //    {
        //        ReportQuickHeader h = FunctionHost.GetReportQuickHeader(reportId);
        //        ReportQuickHeaderResponse r = new ReportQuickHeaderResponse(true);
        //        r.Response = h;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        ReportQuickHeaderResponse r = new ReportQuickHeaderResponse(false, ex.Message);
        //        return r;
        //    }
        //}

        //public static StockItemMergeCollectionResponse RetrieveStockItemMergeCollection(StringList mergeList)
        //{
        //    try
        //    {
        //        StockItemMergeCollection c = FunctionHost.RetrieveStockItemMergeCollection(mergeList);
        //        StockItemMergeCollectionResponse r = new StockItemMergeCollectionResponse(true);
        //        r.Response = c;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        StockItemMergeCollectionResponse r = new StockItemMergeCollectionResponse(false, ex.Message);
        //        return r;
        //    }
        //}

        //public static StockItemSalesContractCollectionResponse RetrieveStockItemSalesContractCollection(StringList mergeList, string sCustId, string sWhsId)
        //{
        //    try
        //    {
        //        StockItemSalesContractCollection c = FunctionHost.RetrieveStockItemSalesContractCollection(mergeList, sCustId, sWhsId);
        //        StockItemSalesContractCollectionResponse r = new StockItemSalesContractCollectionResponse(true);

        //        r.Response = c;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        StockItemSalesContractCollectionResponse r = new StockItemSalesContractCollectionResponse(false, ex.Message);
        //        return r;
        //    }
        //}

        //public static StockItemInvoiceCollectionResponse RetrieveStockItemInvoiceCollection(StringList mergeList, string sCustId, string sWhsId)
        //{
        //    try
        //    {
        //        StockItemInvoiceCollection c = FunctionHost.RetrieveStockItemInvoiceCollection(mergeList, sCustId, sWhsId);
        //        StockItemInvoiceCollectionResponse r = new StockItemInvoiceCollectionResponse(true);
        //        r.Response = c;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        StockItemInvoiceCollectionResponse r = new StockItemInvoiceCollectionResponse(false, ex.Message);
        //        return r;
        //    }
        //}

        //public static StockItemPurchaseOrderCollectionResponse RetrieveStockItemPurchaseOrderCollection(StringList mergeList, string sCustId, string sWhsId)
        //{
        //    try
        //    {
        //        StockItemPurchaseOrderCollection c = FunctionHost.RetrieveStockItemPurchaseOrderCollection(mergeList, sCustId, sWhsId);
        //        StockItemPurchaseOrderCollectionResponse r = new StockItemPurchaseOrderCollectionResponse(true);
        //        r.Response = c;
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        StockItemPurchaseOrderCollectionResponse r = new StockItemPurchaseOrderCollectionResponse(false, ex.Message);
        //        return r;
        //    }
        //}

        //public static NonPrimitive IncrementPrimitive(NonPrimitive p)
        //{
        //    NonPrimitive q = new NonPrimitive();
        //    q.PrimitiveId = p.PrimitiveId;
        //    q.Description = p.Description;
        //    q.Counter = p.Counter + 20;
        //    return q;
        //}

        public static ReturnDataSetResponse SetReturnDataSet(string sSql, string sTable, ref DataSet oDs)
        {
            try
            {
                ReturnDataSet s = FunctionHost.SetReturnDataSet(sSql, sTable, ref oDs);
                ReturnDataSetResponse r = new ReturnDataSetResponse(true);
                r.Response = s;
                return r;
            }
            catch (Exception ex)
            {
                ReturnDataSetResponse r = new ReturnDataSetResponse(false, ex.Message);
                return r;
            }
        }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using UbiWeb.Data;
using UbiWeb.Services;

namespace UbiSqlFramework
{
    public static class TransactionHost
    {
        public static TransactionResponse ProcessTransaction(Transaction t)
        {
            try
            {
                TransactionResponse r = new TransactionResponse(true);
                OperationResponse cOR = new OperationResponse();

                r.TransactionId = t.TransactionId;
                foreach (string sOpId in t.OperationSequence)
                {
                    string sOpType = sOpId.Substring(0, sOpId.Length - 6);
                    cOR = new OperationResponse();

                    if (sOpType == "ReferenceTables")
                    {
                        for (int i = 0; i < t.ReferenceTablesOperations.Count; i++)
                        {
                            if (t.ReferenceTablesOperations[i].OperationId == sOpId)
                            {
                                cOR = ProcessReferenceTablesOperation(t.ReferenceTablesOperations[i]);
                                break;
                            }
                        }
                    }

                    if (sOpType == "ReferenceData")
                    {
                        for (int i = 0; i < t.ReferenceDataOperations.Count; i++)
                        {
                            if (t.ReferenceDataOperations[i].OperationId == sOpId)
                            {
                                cOR = ProcessReferenceDataOperation(t.ReferenceDataOperations[i]);
                                break;
                            }
                        }
                    }

                    //if (sOpType == "ReportQuickHeader")
                    //{
                    //    for (int i = 0; i < t.ReportQuickHeaderOperations.Count; i++)
                    //    {
                    //        if (t.ReportQuickHeaderOperations[i].OperationId == sOpId)
                    //        {
                    //            cOR = ProcessReportQuickHeaderOperation(t.ReportQuickHeaderOperations[i]);
                    //            break;
                    //        }
                    //    }
                    //}
                    //if (sOpType == "CBAJob")
                    //{
                    //    for (int i = 0; i < t.CBAJobOperations.Count; i++)
                    //    {
                    //        if (t.CBAJobOperations[i].OperationId == sOpId)
                    //        {
                    //            cOR = ProcessCBAJobOperation(t.CBAJobOperations[i]);
                    //            break;
                    //        }
                    //    }
                    //}
                    //if (sOpType == "MasterfilesJob")
                    //{
                    //    for (int i = 0; i < t.MasterfilesJobOperations.Count; i++)
                    //    {
                    //        if (t.MasterfilesJobOperations[i].OperationId == sOpId)
                    //        {
                    //            cOR = ProcessMasterfilesJobOperation(t.MasterfilesJobOperations[i]);
                    //            break;
                    //        }
                    //    }
                    //}
                    //if (sOpType == "OEHeader")
                    //{
                    //    for (int i = 0; i < t.OEHeaderOperations.Count; i++)
                    //    {
                    //        if (t.OEHeaderOperations[i].OperationId == sOpId)
                    //        {
                    //            cOR = ProcessOEHeaderOperation(t.OEHeaderOperations[i]);
                    //            break;
                    //        }
                    //    }
                    //}
                    //if (sOpType == "POHeader")
                    //{
                    //    for (int i = 0; i < t.POHeaderOperations.Count; i++)
                    //    {
                    //        if (t.POHeaderOperations[i].OperationId == sOpId)
                    //        {
                    //            cOR = ProcessPOHeaderOperation(t.POHeaderOperations[i]);
                    //            break;
                    //        }
                    //    }
                    //}
                    //if (sOpType == "Stock")
                    //{
                    //    for (int i = 0; i < t.StockOperations.Count; i++)
                    //    {
                    //        if (t.StockOperations[i].OperationId == sOpId)
                    //        {
                    //            cOR = ProcessStockOperation(t.StockOperations[i]);
                    //            break;
                    //        }
                    //    }
                    //}

                    if (cOR.Success)
                    {
                        // Currently doing nothing (logging etc) apart from counting successes and failures
                        r.SucessfulOperations++;
                    }
                    else
                    {
                        // Currently doing nothing (logging etc) apart from counting successes and failures
                        r.FailedOperations++;
                    }
                    r.OperationResponses.Add(cOR);
                }
                return r;
            }
            catch (Exception ex)
            {
                TransactionResponse err = new TransactionResponse(false, ex.Message);
                return err;
            }
        }

        public static OperationResponse ProcessReferenceTablesOperation(ReferenceTablesOperation o)
        {
            switch (o.Verb)
            {
                case OperationVerb.Create:
                    return ReferenceTablesCreate(o);
                case OperationVerb.Read:
                    break;
                case OperationVerb.Update:
                    return ReferenceTablesUpdate(o);
                case OperationVerb.Delete:
                    return ReferenceTablesDelete(o);
                default:
                    return null;
            }
            return null;
        }
        
        public static OperationResponse ReferenceTablesCreate(ReferenceTablesOperation o)
        {
            SqlCommand sqlCmd;
            string sSql;
            ReferenceTables c;
            try
            {
                c = o.DataMember;

                sSql = "INSERT INTO [REF_TABLE] " +
                       "([REF_TABLE_ID], [DESCRIPTION], [DISPLAY_YN], [ID_LABEL], [DESC_LABEL], [SHORT_DESC_LABEL], [EXT_STR_1_LABEL], " +
                       "[EXT_STR_2_LABEL], [EXT_STR_3_LABEL], [EXT_NUM_1_LABEL], [EXT_NUM_2_LABEL], [EXT_DT_1_LABEL], [EXT_DT_2_LABEL], " +
                       "[EXT_BOOL_1_LABEL], [EXT_BOOL_2_LABEL], [STR_1_FK], [STR_2_FK], [HELP_DESC], [NOTES], [EXTENDED_OPTIONS]) " +
                       "VALUES " +
                       "(@REF_TABLE_ID, @DESCRIPTION, @DISPLAY_YN, @ID_LABEL, @DESC_LABEL, @SHORT_DESC_LABEL, @EXT_STR_1_LABEL, " +
                       "@EXT_STR_2_LABEL, @EXT_STR_3_LABEL, @EXT_NUM_1_LABEL, @EXT_NUM_2_LABEL, @EXT_DT_1_LABEL, @EXT_DT_2_LABEL, " +
                       "@EXT_BOOL_1_LABEL, @EXT_BOOL_2_LABEL, @STR_1_FK, @STR_2_FK, @HELP_DESC, @NOTES, @EXTENDED_OPTIONS)";
                sqlCmd = null;
                sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                sqlCmd.Parameters.AddWithValue("@REF_TABLE_ID", c.RefTableId);
                sqlCmd.Parameters.AddWithValue("@DESCRIPTION", c.Description);
                sqlCmd.Parameters.AddWithValue("@DISPLAY_YN", c.DisplayYN);
                sqlCmd.Parameters.AddWithValue("@ID_LABEL", c.IDLabel);
                sqlCmd.Parameters.AddWithValue("@DESC_LABEL", c.DescLabel);
                sqlCmd.Parameters.AddWithValue("@SHORT_DESC_LABEL", c.ShortDescLabel);
                sqlCmd.Parameters.AddWithValue("@EXT_STR_1_LABEL", c.ExtStr1Label);
                sqlCmd.Parameters.AddWithValue("@EXT_STR_2_LABEL", c.ExtStr2Label);
                sqlCmd.Parameters.AddWithValue("@EXT_STR_3_LABEL", c.ExtStr3Label);
                sqlCmd.Parameters.AddWithValue("@EXT_NUM_1_LABEL", c.ExtNum1Label);
                sqlCmd.Parameters.AddWithValue("@EXT_NUM_2_LABEL", c.ExtNum2Label);
                sqlCmd.Parameters.AddWithValue("@EXT_DT_1_LABEL", c.ExtDT1Label);
                sqlCmd.Parameters.AddWithValue("@EXT_DT_2_LABEL", c.ExtDT2Label);
                sqlCmd.Parameters.AddWithValue("@EXT_BOOL_1_LABEL", c.ExtBool1Label);
                sqlCmd.Parameters.AddWithValue("@EXT_BOOL_2_LABEL", c.ExtBool2Label);
                sqlCmd.Parameters.AddWithValue("@STR_1_FK", c.Str1FK);
                sqlCmd.Parameters.AddWithValue("@STR_2_FK", c.Str2FK);
                sqlCmd.Parameters.AddWithValue("@HELP_DESC", c.HelpDesc);
                sqlCmd.Parameters.AddWithValue("@NOTES", c.Notes);
                sqlCmd.Parameters.AddWithValue("@EXTENDED_OPTIONS", c.ExtendedOptions);
                sqlCmd.ExecuteNonQuery();

                OperationResponse r = new OperationResponse(true);
                r.OperationId = o.OperationId;
                r.Sequence = o.Sequence;
                r.Response = "Record Added";

                return r;
            }
            catch (Exception ex)
            {
                return new OperationResponse(false, ex.Message);
            }
        }

        public static OperationResponse ReferenceTablesUpdate(ReferenceTablesOperation o)
        {
            SqlCommand sqlCmd;
            string sSql;
            ReferenceTables c;
            try
            {
                c = o.DataMember;

                sSql = "UPDATE [REF_TABLE] " +
                       "SET [DESCRIPTION] = @DESCRIPTION, [DISPLAY_YN] = @DISPLAY_YN, " +
                       "[ID_LABEL] = @ID_LABEL, [DESC_LABEL] = @DESC_LABEL, [SHORT_DESC_LABEL] = @SHORT_DESC_LABEL, " +
                       "[EXT_STR_1_LABEL] = @EXT_STR_1_LABEL, [EXT_STR_2_LABEL] = @EXT_STR_2_LABEL, [EXT_STR_3_LABEL] = @EXT_STR_3_LABEL, " +
                       "[EXT_NUM_1_LABEL] = @EXT_NUM_1_LABEL, [EXT_NUM_2_LABEL] = @EXT_NUM_2_LABEL, [EXT_DT_1_LABEL] = @EXT_DT_1_LABEL, " +
                       "[EXT_DT_2_LABEL] = @EXT_DT_2_LABEL, [EXT_BOOL_1_LABEL] = @EXT_BOOL_1_LABEL, [EXT_BOOL_2_LABEL] = @EXT_BOOL_2_LABEL, " +
                       "[STR_1_FK] = @STR_1_FK, [STR_2_FK] = @STR_2_FK, [HELP_DESC] = @HELP_DESC, [NOTES] = @NOTES,  " +
                       "[EXTENDED_OPTIONS] = @EXTENDED_OPTIONS " +
                       "WHERE [REF_TABLE_ID] = @REF_TABLE_ID "; 
                sqlCmd = null;
                sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                sqlCmd.Parameters.AddWithValue("@DESCRIPTION", c.Description);
                sqlCmd.Parameters.AddWithValue("@DISPLAY_YN", c.DisplayYN);
                sqlCmd.Parameters.AddWithValue("@ID_LABEL", c.IDLabel);
                sqlCmd.Parameters.AddWithValue("@DESC_LABEL", c.DescLabel);
                sqlCmd.Parameters.AddWithValue("@SHORT_DESC_LABEL", c.ShortDescLabel);
                sqlCmd.Parameters.AddWithValue("@EXT_STR_1_LABEL", c.ExtStr1Label);
                sqlCmd.Parameters.AddWithValue("@EXT_STR_2_LABEL", c.ExtStr2Label);
                sqlCmd.Parameters.AddWithValue("@EXT_STR_3_LABEL", c.ExtStr3Label);
                sqlCmd.Parameters.AddWithValue("@EXT_NUM_1_LABEL", c.ExtNum1Label);
                sqlCmd.Parameters.AddWithValue("@EXT_NUM_2_LABEL", c.ExtNum2Label);
                sqlCmd.Parameters.AddWithValue("@EXT_DT_1_LABEL", c.ExtDT1Label);
                sqlCmd.Parameters.AddWithValue("@EXT_DT_2_LABEL", c.ExtDT2Label);
                sqlCmd.Parameters.AddWithValue("@EXT_BOOL_1_LABEL", c.ExtBool1Label);
                sqlCmd.Parameters.AddWithValue("@EXT_BOOL_2_LABEL", c.ExtBool2Label);
                sqlCmd.Parameters.AddWithValue("@STR_1_FK", c.Str1FK);
                sqlCmd.Parameters.AddWithValue("@STR_2_FK", c.Str2FK);
                sqlCmd.Parameters.AddWithValue("@HELP_DESC", c.HelpDesc);
                sqlCmd.Parameters.AddWithValue("@NOTES", c.Notes);
                sqlCmd.Parameters.AddWithValue("@EXTENDED_OPTIONS", c.ExtendedOptions);
                sqlCmd.Parameters.AddWithValue("@REF_TABLE_ID", c.RefTableId);
                sqlCmd.ExecuteNonQuery();

                OperationResponse r = new OperationResponse(true);
                r.OperationId = o.OperationId;
                r.Sequence = o.Sequence;
                r.Response = "Record Updated";

                return r;
            }
            catch (Exception ex)
            {
                return new OperationResponse(false, ex.Message);
            }
        }

        public static OperationResponse ReferenceTablesDelete(ReferenceTablesOperation o)
        {
            SqlCommand sqlCmd;
            string sSql;
            ReferenceTables c;
            try
            {
                c = o.DataMember;

                sSql = "delete from [REF_TABLE] " +
                    "where [REF_TABLE_ID] = @REF_TABLE_ID";
                sqlCmd = null;
                sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                sqlCmd.Parameters.AddWithValue("@REF_TABLE_ID", c.RefTableId);
                sqlCmd.ExecuteNonQuery();

                sSql = "delete from [REF_DATA] " +
                    "where [REF_TABLE_ID] = @REF_TABLE_ID ";
                sqlCmd = null;
                sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                sqlCmd.Parameters.AddWithValue("@REF_TABLE_ID", c.RefTableId);
                sqlCmd.ExecuteNonQuery();

                OperationResponse r = new OperationResponse(true);
                r.OperationId = o.OperationId;
                r.Sequence = o.Sequence;
                r.Response = "Record Deleted";

                return r;
            }
            catch (Exception ex)
            {
                return new OperationResponse(false, ex.Message);
            }
        }

        public static OperationResponse ProcessReferenceDataOperation(ReferenceDataOperation o)
        {
            switch (o.Verb)
            {
                case OperationVerb.Create:
                    return ReferenceDataCreate(o);
                case OperationVerb.Read:
                    break;
                case OperationVerb.Update:
                    return ReferenceDataUpdate(o);
                case OperationVerb.Delete:
                    return ReferenceDataDelete(o);
                default:
                    return null;
            }
            return null;
        }

        public static OperationResponse ReferenceDataCreate(ReferenceDataOperation o)
        {
            SqlCommand sqlCmd;
            string sSql;
            ReferenceData c;
            try
            {
                c = o.DataMember;

                sSql = "INSERT INTO [REF_DATA] " +
                       "([REF_TABLE_ID], [REF_ID], [DESCRIPTION], [SHORT_DESC], [DISPLAY_YN], [EXT_STR_1], " +
                       "[EXT_STR_2], [EXT_STR_3], [EXT_NUM_1], [EXT_NUM_2], [EXT_DT_1], [EXT_DT_2], " +
                       "[EXT_BOOL_1], [EXT_BOOL_2], [NOTES], [EXTENDED_OPTIONS]) " +
                       "VALUES " +
                       "(@REF_TABLE_ID, @REF_ID, @DESCRIPTION, @SHORT_DESC, @DISPLAY_YN,  @EXT_STR_1, " +
                       "@EXT_STR_2, @EXT_STR_3, @EXT_NUM_1, @EXT_NUM_2, @EXT_DT_1, @EXT_DT_2, " +
                       "@EXT_BOOL_1, @EXT_BOOL_2, @NOTES, @EXTENDED_OPTIONS)";
                sqlCmd = null;
                sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                sqlCmd.Parameters.Add("@REF_TABLE_ID", SqlDbType.VarChar).Value = c.RefTableId;
                sqlCmd.Parameters.Add("@REF_ID", SqlDbType.VarChar).Value = c.RefId;
                sqlCmd.Parameters.Add("@DESCRIPTION", SqlDbType.VarChar).Value = c.Description;
                sqlCmd.Parameters.Add("@SHORT_DESC", SqlDbType.VarChar).Value = c.ShortDesc;
                sqlCmd.Parameters.Add("@DISPLAY_YN", SqlDbType.Int).Value = c.DisplayYN;
                sqlCmd.Parameters.Add("@EXT_STR_1", SqlDbType.VarChar).Value = c.ExtStr1;
                sqlCmd.Parameters.Add("@EXT_STR_2", SqlDbType.VarChar).Value = c.ExtStr2;
                sqlCmd.Parameters.Add("@EXT_STR_3", SqlDbType.VarChar).Value = c.ExtStr3;
                sqlCmd.Parameters.Add("@EXT_NUM_1", SqlDbType.Float).Value = c.ExtNum1;
                sqlCmd.Parameters.Add("@EXT_NUM_2", SqlDbType.Float).Value = c.ExtNum2;
                sqlCmd.Parameters.Add("@EXT_DT_1", SqlDbType.DateTime).Value = c.ExtDT1;
                sqlCmd.Parameters.Add("@EXT_DT_2", SqlDbType.DateTime).Value = c.ExtDT2;
                sqlCmd.Parameters.Add("@EXT_BOOL_1", SqlDbType.Bit).Value = c.ExtBool1;
                sqlCmd.Parameters.Add("@EXT_BOOL_2", SqlDbType.Bit).Value = c.ExtBool2;
                sqlCmd.Parameters.Add("@NOTES", SqlDbType.VarChar).Value = c.Notes;
                sqlCmd.Parameters.Add("@EXTENDED_OPTIONS", SqlDbType.Int).Value = c.ExtendedOptions;
                sqlCmd.ExecuteNonQuery();

                OperationResponse r = new OperationResponse(true);
                r.OperationId = o.OperationId;
                r.Sequence = o.Sequence;
                r.Response = "Record Added";

                return r;
            }
            catch (Exception ex)
            {
                return new OperationResponse(false, ex.Message);
            }
        }

        public static OperationResponse ReferenceDataUpdate(ReferenceDataOperation o)
        {
            SqlCommand sqlCmd;
            string sSql;
            ReferenceData c;
            try
            {
                c = o.DataMember;

                sSql = "UPDATE [REF_DATA] " +
                       "SET [DESCRIPTION] = @DESCRIPTION, [SHORT_DESC] = @SHORT_DESC, [DISPLAY_YN] = @DISPLAY_YN, " +
                       "[EXT_STR_1] = @EXT_STR_1, [EXT_STR_2] = @EXT_STR_2, [EXT_STR_3] = @EXT_STR_3, " +
                       "[EXT_NUM_1] = @EXT_NUM_1, [EXT_NUM_2] = @EXT_NUM_2, " +
                       "[EXT_DT_1] = @EXT_DT_1, [EXT_DT_2] = @EXT_DT_2, " +
                       "[EXT_BOOL_1] = @EXT_BOOL_1, [EXT_BOOL_2] = @EXT_BOOL_2, " +
                       "[NOTES] = @NOTES, [EXTENDED_OPTIONS] = @EXTENDED_OPTIONS " +
                       "WHERE [REF_TABLE_ID] = @REF_TABLE_ID AND [REF_ID] = @REF_ID";
                sqlCmd = null;
                sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                sqlCmd.Parameters.Add("@DESCRIPTION", SqlDbType.VarChar).Value = c.Description;
                sqlCmd.Parameters.Add("@SHORT_DESC", SqlDbType.VarChar).Value = c.ShortDesc;
                sqlCmd.Parameters.Add("@DISPLAY_YN", SqlDbType.Int).Value = c.DisplayYN;
                sqlCmd.Parameters.Add("@EXT_STR_1", SqlDbType.VarChar).Value = c.ExtStr1;
                sqlCmd.Parameters.Add("@EXT_STR_2", SqlDbType.VarChar).Value = c.ExtStr2;
                sqlCmd.Parameters.Add("@EXT_STR_3", SqlDbType.VarChar).Value = c.ExtStr3;
                sqlCmd.Parameters.Add("@EXT_NUM_1", SqlDbType.Float).Value = c.ExtNum1;
                sqlCmd.Parameters.Add("@EXT_NUM_2", SqlDbType.Float).Value = c.ExtNum2;
                sqlCmd.Parameters.Add("@EXT_DT_1", SqlDbType.DateTime).Value = c.ExtDT1;
                sqlCmd.Parameters.Add("@EXT_DT_2", SqlDbType.DateTime).Value = c.ExtDT2;
                sqlCmd.Parameters.Add("@EXT_BOOL_1", SqlDbType.Bit).Value = c.ExtBool1;
                sqlCmd.Parameters.Add("@EXT_BOOL_2", SqlDbType.Bit).Value = c.ExtBool2;
                sqlCmd.Parameters.Add("@NOTES", SqlDbType.VarChar).Value = c.Notes;
                sqlCmd.Parameters.Add("@EXTENDED_OPTIONS", SqlDbType.Int).Value = c.ExtendedOptions;
                sqlCmd.Parameters.Add("@REF_TABLE_ID", SqlDbType.VarChar).Value = c.RefTableId;
                sqlCmd.Parameters.Add("@REF_ID", SqlDbType.VarChar).Value = c.RefId;
                sqlCmd.ExecuteNonQuery();

                OperationResponse r = new OperationResponse(true);
                r.OperationId = o.OperationId;
                r.Sequence = o.Sequence;
                r.Response = "Record Updated";

                return r;
            }
            catch (Exception ex)
            {
                return new OperationResponse(false, ex.Message);
            }
        }

        public static OperationResponse ReferenceDataDelete(ReferenceDataOperation o)
        {
            SqlCommand sqlCmd;
            string sSql;
            ReferenceData c;
            try
            {
                c = o.DataMember;

                sSql = "delete from [REF_DATA] " +
                    "where [REF_TABLE_ID] = @REF_TABLE_ID " +
                    "and [REF_ID] = @REF_ID";
                sqlCmd = null;
                sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                sqlCmd.Parameters.AddWithValue("@REF_TABLE_ID", c.RefTableId);
                sqlCmd.Parameters.AddWithValue("@REF_ID", c.RefId);
                sqlCmd.ExecuteNonQuery();

                OperationResponse r = new OperationResponse(true);
                r.OperationId = o.OperationId;
                r.Sequence = o.Sequence;
                r.Response = "Record Deleted";

                return r;
            }
            catch (Exception ex)
            {
                return new OperationResponse(false, ex.Message);
            }
        }

        //public static OperationResponse ProcessReportQuickHeaderOperation(ReportQuickHeaderOperation o)
        //{
        //    switch (o.Verb)
        //    {
        //        case OperationVerb.Create:
        //            return ReportQuickHeaderCreate(o);
        //        case OperationVerb.Read:
        //            break;
        //        case OperationVerb.Update:
        //            return ReportQuickHeaderUpdate(o);
        //        case OperationVerb.Delete:
        //            return ReportQuickHeaderDelete(o);
        //        default:
        //            return null;
        //    }
        //    return null;
        //}
        //public static OperationResponse ReportQuickHeaderCreate(ReportQuickHeaderOperation o)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    ReportQuickHeader c;
        //    try
        //    {
        //        c = o.DataMember;

        //        sSql = "insert into REPORT_QUICK_HEADER " +
        //            "( REPORT_ID, TITLE, DESCRIPTION, ID_TYPE, COMMENT )" +
        //            " values " +
        //            "( @ReportId, @Title, @Description, @IdType, @Comment )";
        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        sqlCmd.Parameters.AddWithValue("@ReportId", c.ReportId);
        //        sqlCmd.Parameters.AddWithValue("@Title", c.Title);
        //        sqlCmd.Parameters.AddWithValue("@Description", c.Description);
        //        sqlCmd.Parameters.AddWithValue("@IdType", c.IdType);
        //        sqlCmd.Parameters.AddWithValue("@Comment", c.Comment);
        //        sqlCmd.ExecuteNonQuery();

        //        OperationResponse r = new OperationResponse(true);
        //        r.OperationId = o.OperationId;
        //        r.Sequence = o.Sequence;
        //        r.Response = "Record Deleted";

        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new OperationResponse(false, ex.Message);
        //    }
        //}
        //public static OperationResponse ReportQuickHeaderUpdate(ReportQuickHeaderOperation o)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    ReportQuickHeader c;
        //    try
        //    {
        //        c = o.DataMember;

        //        sSql = "update REPORT_QUICK_HEADER set " +
        //            "TITLE = @Title, DESCRIPTION = @Description, ID_TYPE = @IdType, COMMENT = @Comment " +
        //            "where REPORT_ID = @ReportId";
        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        sqlCmd.Parameters.AddWithValue("@Title", c.Title);
        //        sqlCmd.Parameters.AddWithValue("@Description", c.Description);
        //        sqlCmd.Parameters.AddWithValue("@IdType", c.IdType);
        //        sqlCmd.Parameters.AddWithValue("@Comment", c.Comment);
        //        sqlCmd.Parameters.AddWithValue("@ReportId", c.ReportId);
        //        sqlCmd.ExecuteNonQuery();

        //        OperationResponse r = new OperationResponse(true);
        //        r.OperationId = o.OperationId;
        //        r.Sequence = o.Sequence;
        //        r.Response = "Record Updated";

        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new OperationResponse(false, ex.Message);
        //    }
        //}
        //public static OperationResponse ReportQuickHeaderDelete(ReportQuickHeaderOperation o)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    ReportQuickHeader c;
        //    try
        //    {
        //        c = o.DataMember;

        //        sSql = "delete from REPORT_QUICK_HEADER " +
        //            "where REPORT_ID = @ReportId";
        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        sqlCmd.Parameters.AddWithValue("@ReportId", c.ReportId);
        //        sqlCmd.ExecuteNonQuery();

        //        OperationResponse r = new OperationResponse(true);
        //        r.OperationId = o.OperationId;
        //        r.Sequence = o.Sequence;
        //        r.Response = "Record Deleted";

        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new OperationResponse(false, ex.Message);
        //    }
        //}



        //public static OperationResponse ProcessCBAJobOperation(CBAJobOperation o)
        //{
        //    switch (o.Verb)
        //    {
        //        case OperationVerb.Create:
        //            return CBAJobCreate(o);
        //        case OperationVerb.Read:
        //            break;
        //        case OperationVerb.Update:
        //            return CBAJobUpdate(o);
        //        case OperationVerb.Delete:
        //            return CBAJobDelete(o);
        //        default:
        //            return null;
        //    }
        //    return null;
        //}
        //public static OperationResponse CBAJobCreate(CBAJobOperation o)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    CBAJob c;
        //    try
        //    {
        //        c = o.DataMember;

        //        sSql = "insert into REPORT_QUICK_HEADER " +
        //            "( REPORT_ID, TITLE, DESCRIPTION, ID_TYPE, COMMENT )" +
        //            " values " +
        //            "( @ReportId, @Title, @Description, @IdType, @Comment )";
        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        //sqlCmd.Parameters.AddWithValue("@ReportId", c.ReportId);
        //        //sqlCmd.Parameters.AddWithValue("@Title", c.Title);
        //        //sqlCmd.Parameters.AddWithValue("@Description", c.Description);
        //        //sqlCmd.Parameters.AddWithValue("@IdType", c.IdType);
        //        //sqlCmd.Parameters.AddWithValue("@Comment", c.Comment);
        //        sqlCmd.ExecuteNonQuery();

        //        OperationResponse r = new OperationResponse(true);
        //        r.OperationId = o.OperationId;
        //        r.Sequence = o.Sequence;
        //        r.Response = "Record Deleted";

        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new OperationResponse(false, ex.Message);
        //    }
        //}
        //public static OperationResponse CBAJobUpdate(CBAJobOperation o)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    CBAJob c;
        //    try
        //    {
        //        c = o.DataMember;

        //        sSql = "update REPORT_QUICK_HEADER set " +
        //            "TITLE = @Title, DESCRIPTION = @Description, ID_TYPE = @IdType, COMMENT = @Comment " +
        //            "where REPORT_ID = @ReportId";
        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        //sqlCmd.Parameters.AddWithValue("@Title", c.Title);
        //        //sqlCmd.Parameters.AddWithValue("@Description", c.Description);
        //        //sqlCmd.Parameters.AddWithValue("@IdType", c.IdType);
        //        //sqlCmd.Parameters.AddWithValue("@Comment", c.Comment);
        //        //sqlCmd.Parameters.AddWithValue("@ReportId", c.ReportId);
        //        sqlCmd.ExecuteNonQuery();

        //        OperationResponse r = new OperationResponse(true);
        //        r.OperationId = o.OperationId;
        //        r.Sequence = o.Sequence;
        //        r.Response = "Record Updated";

        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new OperationResponse(false, ex.Message);
        //    }
        //}
        //public static OperationResponse CBAJobDelete(CBAJobOperation o)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    CBAJob c;
        //    try
        //    {
        //        c = o.DataMember;

        //        sSql = "delete from REPORT_QUICK_HEADER " +
        //            "where REPORT_ID = @ReportId";
        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        //sqlCmd.Parameters.AddWithValue("@ReportId", c.ReportId);
        //        sqlCmd.ExecuteNonQuery();

        //        OperationResponse r = new OperationResponse(true);
        //        r.OperationId = o.OperationId;
        //        r.Sequence = o.Sequence;
        //        r.Response = "Record Deleted";

        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new OperationResponse(false, ex.Message);
        //    }
        //}




        //public static OperationResponse ProcessMasterfilesJobOperation(MasterfilesJobOperation o)
        //{
        //    switch (o.Verb)
        //    {
        //        case OperationVerb.Create:
        //            return MasterfilesJobCreate(o);
        //        case OperationVerb.Read:
        //            break;
        //        case OperationVerb.Update:
        //            return MasterfilesJobUpdate(o);
        //        case OperationVerb.Delete:
        //            return MasterfilesJobDelete(o);
        //        default:
        //            return null;
        //    }
        //    return null;
        //}
        //public static OperationResponse MasterfilesJobCreate(MasterfilesJobOperation o)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    MasterfilesJob c;
        //    try
        //    {
        //        c = o.DataMember;

        //        sSql = "insert into REPORT_QUICK_HEADER " +
        //            "( REPORT_ID, TITLE, DESCRIPTION, ID_TYPE, COMMENT )" +
        //            " values " +
        //            "( @ReportId, @Title, @Description, @IdType, @Comment )";
        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        //sqlCmd.Parameters.AddWithValue("@ReportId", c.ReportId);
        //        //sqlCmd.Parameters.AddWithValue("@Title", c.Title);
        //        //sqlCmd.Parameters.AddWithValue("@Description", c.Description);
        //        //sqlCmd.Parameters.AddWithValue("@IdType", c.IdType);
        //        //sqlCmd.Parameters.AddWithValue("@Comment", c.Comment);
        //        sqlCmd.ExecuteNonQuery();

        //        OperationResponse r = new OperationResponse(true);
        //        r.OperationId = o.OperationId;
        //        r.Sequence = o.Sequence;
        //        r.Response = "Record Deleted";

        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new OperationResponse(false, ex.Message);
        //    }
        //}
        //public static OperationResponse MasterfilesJobUpdate(MasterfilesJobOperation o)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    MasterfilesJob c;
        //    try
        //    {
        //        c = o.DataMember;

        //        sSql = "update REPORT_QUICK_HEADER set " +
        //            "TITLE = @Title, DESCRIPTION = @Description, ID_TYPE = @IdType, COMMENT = @Comment " +
        //            "where REPORT_ID = @ReportId";
        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        //sqlCmd.Parameters.AddWithValue("@Title", c.Title);
        //        //sqlCmd.Parameters.AddWithValue("@Description", c.Description);
        //        //sqlCmd.Parameters.AddWithValue("@IdType", c.IdType);
        //        //sqlCmd.Parameters.AddWithValue("@Comment", c.Comment);
        //        //sqlCmd.Parameters.AddWithValue("@ReportId", c.ReportId);
        //        sqlCmd.ExecuteNonQuery();

        //        OperationResponse r = new OperationResponse(true);
        //        r.OperationId = o.OperationId;
        //        r.Sequence = o.Sequence;
        //        r.Response = "Record Updated";

        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new OperationResponse(false, ex.Message);
        //    }
        //}
        //public static OperationResponse MasterfilesJobDelete(MasterfilesJobOperation o)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    MasterfilesJob c;
        //    try
        //    {
        //        c = o.DataMember;

        //        sSql = "delete from REPORT_QUICK_HEADER " +
        //            "where REPORT_ID = @ReportId";
        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        //sqlCmd.Parameters.AddWithValue("@ReportId", c.ReportId);
        //        sqlCmd.ExecuteNonQuery();

        //        OperationResponse r = new OperationResponse(true);
        //        r.OperationId = o.OperationId;
        //        r.Sequence = o.Sequence;
        //        r.Response = "Record Deleted";

        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new OperationResponse(false, ex.Message);
        //    }
        //}




        //public static OperationResponse ProcessOEHeaderOperation(OEHeaderOperation o)
        //{
        //    switch (o.Verb)
        //    {
        //        case OperationVerb.Create:
        //            return OEHeaderCreate(o);
        //        case OperationVerb.Read:
        //            break;
        //        case OperationVerb.Update:
        //            return OEHeaderUpdate(o);
        //        case OperationVerb.Delete:
        //            return OEHeaderDelete(o);
        //        default:
        //            return null;
        //    }
        //    return null;
        //}
        //public static OperationResponse OEHeaderCreate(OEHeaderOperation o)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    bool bUpdateMaster;
        //    bool bUpdateLot;
        //    string sSqlUpdateStock = "";
        //    string sSqlUpdateStockLot = "";
        //    OEHeader c;
        //    try
        //    {
        //        c = o.DataMember;

        //        if (c.StockMasterfileUpdateRequired())
        //        {
        //            bUpdateMaster = true;
        //            sSqlUpdateStock = "update STOCK set DEF_BASE_SELL = @DEF_BASE_SELL where STOCK_CODE = @STOCK_CODE";
        //        }
        //        else
        //        {
        //            bUpdateMaster = false;
        //        }
        //        if (c.StockLotMasterfileUpdateRequired())
        //        {
        //            bUpdateLot = true;
        //            sSqlUpdateStockLot = "update STOCK_LOT set ";
        //            if (c.DBUpdateOEPrice) sSqlUpdateStockLot = sSqlUpdateStockLot + "BASE_SELL = @BASE_SELL, ";
        //            if (c.DBUpdateCartonCubic) sSqlUpdateStockLot = sSqlUpdateStockLot + "VOLUME = @VOLUME, ";
        //            if (c.DBUpdateCtnQty) sSqlUpdateStockLot = sSqlUpdateStockLot + "CARTON_QTY = @CARTON_QTY, ";
        //            if (c.DBUpdateInnerQty) sSqlUpdateStockLot = sSqlUpdateStockLot + "INNER_QTY = @INNER_QTY, ";
        //            sSqlUpdateStockLot = sSqlUpdateStockLot.Substring(0, sSqlUpdateStockLot.Length - 2);
        //            sSqlUpdateStockLot = sSqlUpdateStockLot + " where STOCK_CODE = @STOCK_CODE and SERIAL_LOT_NO = @SERIAL_LOT_NO";
        //        }
        //        else
        //        {
        //            bUpdateLot = false;
        //        }

        //        sSql = "INSERT INTO SO_HEADER ( ";
        //        sSql = sSql + "ORDER_NO, ORDER_DATE, WAREHOUSE_ID, ORDER_TYPE, STATUS, UBI_STATUS, ";
        //        sSql = sSql + "CUSTOMER_ID, CUSTOMER_ADDR_ID, DEL_NAME, DEL_ADDR_1, DEL_ADDR_2, DEL_ADDR_3, DEL_STATE, ";
        //        sSql = sSql + "DEL_POST_CODE, DEL_COUNTRY, DEL_CONTACT, DEL_PHONE, DEL_FAX, ";
        //        sSql = sSql + "DEL_EMAIL, CUSTOMER_ORDER_NO, SALES_CONTRACT, ORIGIN, INVOICE_TYPE, ";
        //        sSql = sSql + "PI_TYPE, ETD, ETA, DEL_DATE, PORT_OF_LOADING, DESTINATION, ";
        //        sSql = sSql + "CONTAINER_TYPE, CONTAINER_NO, VESSEL, SEAL_NO, PRICE_CODE, CURRENCY, ";
        //        sSql = sSql + "QUOTE_EXCHANGE, EXCHANGE_RATE, DISCOUNT_RATE, DISCOUNT_AMT, ";
        //        sSql = sSql + "COMMISSION_RATE, COMMISSION_AMOUNT, SURCHARGE_RATE, SURCHARGE_AMOUNT, ";
        //        sSql = sSql + "PO_LINK_NO, ORIGIN_SO_NUM, VALUE_ORDERED, VALUE_DELIVERED, ";
        //        sSql = sSql + "VALUE_CANCELLED, TOTAL_CARTONS, TOTAL_VOLUME_MTR, TOTAL_WEIGHT, ";
        //        sSql = sSql + "SHIPPING_MARKS, SURCHARGE_REASON, CUST_REQ_DATE_START, ";
        //        sSql = sSql + "CUST_REQ_DATE_END, SCHEDULED_PROMO, QUOTED_MARGIN, BOSS, ";
        //        sSql = sSql + "DEL_CUST_ID, CONTEXT ";
        //        sSql = sSql + " ) VALUES ( ";
        //        sSql = sSql + "@ORDER_NO, @ORDER_DATE, @WAREHOUSE_ID, @ORDER_TYPE, @STATUS, @UBI_STATUS, ";
        //        sSql = sSql + "@CUSTOMER_ID, @CUSTOMER_ADDR_ID, @DEL_NAME, @DEL_ADDR_1, @DEL_ADDR_2, @DEL_ADDR_3, @DEL_STATE, ";
        //        sSql = sSql + "@DEL_POST_CODE, @DEL_COUNTRY, @DEL_CONTACT, @DEL_PHONE, @DEL_FAX, ";
        //        sSql = sSql + "@DEL_EMAIL, @CUSTOMER_ORDER_NO, @SALES_CONTRACT, @ORIGIN, @INVOICE_TYPE, ";
        //        sSql = sSql + "@PI_TYPE, @ETD, @ETA, @DEL_DATE, @PORT_OF_LOADING, @DESTINATION, ";
        //        sSql = sSql + "@CONTAINER_TYPE, @CONTAINER_NO, @VESSEL, @SEAL_NO, @PRICE_CODE, @CURRENCY, ";
        //        sSql = sSql + "@QUOTE_EXCHANGE, @EXCHANGE_RATE, @DISCOUNT_RATE, @DISCOUNT_AMT, ";
        //        sSql = sSql + "@COMMISSION_RATE, @COMMISSION_AMOUNT, @SURCHARGE_RATE, @SURCHARGE_AMOUNT, ";
        //        sSql = sSql + "@PO_LINK_NO, @ORIGIN_SO_NUM, @VALUE_ORDERED, @VALUE_DELIVERED, ";
        //        sSql = sSql + "@VALUE_CANCELLED, @TOTAL_CARTONS, @TOTAL_VOLUME_MTR, @TOTAL_WEIGHT, ";
        //        sSql = sSql + "@SHIPPING_MARKS, @SURCHARGE_REASON, @CUST_REQ_DATE_START, ";
        //        sSql = sSql + "@CUST_REQ_DATE_END, @SCHEDULED_PROMO, @QUOTED_MARGIN, @BOSS, ";
        //        sSql = sSql + "@DEL_CUST_ID, @CONTEXT ";
        //        sSql = sSql + " )";

        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);

        //        sqlCmd.Parameters.AddWithValue("@ORDER_NO", c.OrderNo);
        //        if (c.OrderDate < new DateTime(1901, 2, 2))
        //        {
        //            sqlCmd.Parameters.AddWithValue("@ORDER_DATE", DateTime.Today);
        //        }
        //        else
        //        {
        //            sqlCmd.Parameters.AddWithValue("@ORDER_DATE", c.OrderDate);

        //        }
        //        sqlCmd.Parameters.AddWithValue("@WAREHOUSE_ID", c.WarehouseId);
        //        sqlCmd.Parameters.AddWithValue("@ORDER_TYPE", c.OrderType);
        //        sqlCmd.Parameters.AddWithValue("@STATUS", c.Status);
        //        sqlCmd.Parameters.AddWithValue("@UBI_STATUS", c.UbiStatus);
        //        sqlCmd.Parameters.AddWithValue("@CUSTOMER_ID", c.CustomerId);
        //        sqlCmd.Parameters.AddWithValue("@CUSTOMER_ADDR_ID", c.CustomerAddrId);
        //        sqlCmd.Parameters.AddWithValue("@DEL_NAME", c.DelName);
        //        sqlCmd.Parameters.AddWithValue("@DEL_ADDR_1", c.DelAddr1);
        //        sqlCmd.Parameters.AddWithValue("@DEL_ADDR_2", c.DelAddr2);
        //        sqlCmd.Parameters.AddWithValue("@DEL_ADDR_3", c.DelAddr3);
        //        sqlCmd.Parameters.AddWithValue("@DEL_STATE", c.DelState);
        //        sqlCmd.Parameters.AddWithValue("@DEL_POST_CODE", c.DelPostCode);
        //        sqlCmd.Parameters.AddWithValue("@DEL_COUNTRY", c.DelCountry);
        //        sqlCmd.Parameters.AddWithValue("@DEL_CONTACT", c.DelContact);
        //        sqlCmd.Parameters.AddWithValue("@DEL_PHONE", c.DelPhone);
        //        sqlCmd.Parameters.AddWithValue("@DEL_FAX", c.DelFax);
        //        sqlCmd.Parameters.AddWithValue("@DEL_EMAIL", c.DelEmail);
        //        sqlCmd.Parameters.AddWithValue("@CUSTOMER_ORDER_NO", c.CustomerOrderNo);
        //        sqlCmd.Parameters.AddWithValue("@SALES_CONTRACT", c.SalesContract);
        //        sqlCmd.Parameters.AddWithValue("@ORIGIN", c.Origin);
        //        sqlCmd.Parameters.AddWithValue("@INVOICE_TYPE", c.InvoiceType);
        //        sqlCmd.Parameters.AddWithValue("@PI_TYPE", c.PiType);
        //        sqlCmd.Parameters.AddWithValue("@ETD", c.Etd);
        //        sqlCmd.Parameters.AddWithValue("@ETA", c.Eta);
        //        sqlCmd.Parameters.AddWithValue("@DEL_DATE", c.DelDate);
        //        sqlCmd.Parameters.AddWithValue("@PORT_OF_LOADING", c.PortOfLoading);
        //        sqlCmd.Parameters.AddWithValue("@DESTINATION", c.Destination);
        //        sqlCmd.Parameters.AddWithValue("@CONTAINER_TYPE", c.ContainerType);
        //        sqlCmd.Parameters.AddWithValue("@CONTAINER_NO", c.ContainerNo);
        //        sqlCmd.Parameters.AddWithValue("@VESSEL", c.Vessel);
        //        sqlCmd.Parameters.AddWithValue("@SEAL_NO", c.SealNo);
        //        sqlCmd.Parameters.AddWithValue("@PRICE_CODE", c.PriceCode);
        //        sqlCmd.Parameters.AddWithValue("@CURRENCY", c.Currency);
        //        sqlCmd.Parameters.AddWithValue("@QUOTE_EXCHANGE", c.QuoteExchange);
        //        sqlCmd.Parameters.AddWithValue("@EXCHANGE_RATE", c.ExchangeRate);
        //        sqlCmd.Parameters.AddWithValue("@DISCOUNT_RATE", c.DiscountRate);
        //        sqlCmd.Parameters.AddWithValue("@DISCOUNT_AMT", c.DiscountAmt);
        //        sqlCmd.Parameters.AddWithValue("@COMMISSION_RATE", c.CommissionRate);
        //        sqlCmd.Parameters.AddWithValue("@COMMISSION_AMOUNT", c.CommissionAmount);
        //        sqlCmd.Parameters.AddWithValue("@SURCHARGE_RATE", c.SurchargeRate);
        //        sqlCmd.Parameters.AddWithValue("@SURCHARGE_AMOUNT", c.SurchargeAmount);
        //        sqlCmd.Parameters.AddWithValue("@PO_LINK_NO", c.PoLinkNo);
        //        sqlCmd.Parameters.AddWithValue("@ORIGIN_SO_NUM", c.OriginSoNum);
        //        sqlCmd.Parameters.AddWithValue("@VALUE_ORDERED", c.ValueOrdered);
        //        sqlCmd.Parameters.AddWithValue("@VALUE_DELIVERED", c.ValueDelivered);
        //        sqlCmd.Parameters.AddWithValue("@VALUE_CANCELLED", c.ValueCancelled);
        //        sqlCmd.Parameters.AddWithValue("@TOTAL_CARTONS", c.TotalCartons);
        //        sqlCmd.Parameters.AddWithValue("@TOTAL_VOLUME_MTR", c.TotalVolumeMtr);
        //        sqlCmd.Parameters.AddWithValue("@TOTAL_WEIGHT", c.TotalWeight);
        //        sqlCmd.Parameters.AddWithValue("@SHIPPING_MARKS", c.ShippingMarks);
        //        sqlCmd.Parameters.AddWithValue("@SURCHARGE_REASON", c.SurchargeReason);
        //        sqlCmd.Parameters.AddWithValue("@CUST_REQ_DATE_START", c.CustReqDateStart);
        //        sqlCmd.Parameters.AddWithValue("@CUST_REQ_DATE_END", c.CustReqDateEnd);
        //        sqlCmd.Parameters.AddWithValue("@SCHEDULED_PROMO", c.ScheduledPromo);
        //        sqlCmd.Parameters.AddWithValue("@QUOTED_MARGIN", c.QuotedMargin);
        //        sqlCmd.Parameters.AddWithValue("@BOSS", c.Boss);
        //        sqlCmd.Parameters.AddWithValue("@DEL_CUST_ID", c.DelCustId);
        //        sqlCmd.Parameters.AddWithValue("@CONTEXT", c.Context);

        //        sqlCmd.ExecuteNonQuery();

        //        sSql = "INSERT INTO SO_LINE ( ";
        //        sSql = sSql + "ORDER_NO, SEQUENCE, ORDER_DATE, STOCK_CODE, CATEGORY, LOT_ID, ";
        //        sSql = sSql + "WAREHOUSE_ID, ORIGIN_SO_NUM, ORIGIN_SO_SEQUENCE, PO_LINK_PO_NUM, ";
        //        sSql = sSql + "PO_LINK_PO_SEQUENCE, CUSTOMER_ORDER_NO, SALES_CONTRACT, ";
        //        sSql = sSql + "UBI_STATUS, SPECIAL_ITEM, PRIORITY, FUMIGATION_REQ, GST_EXEMPT, ";
        //        sSql = sSql + "QTY_ORD, PRICE, VALUE_ORD, QTY_DELIVERED, VALUE_DELIVERED, ";
        //        sSql = sSql + "QTY_CANCELLED, VALUE_CANCELLED, TAX_RATE, TAX_AMOUNT, ";
        //        sSql = sSql + "COMMISSION_RATE, COMMISSION_AMT, DISCOUNT_AMT, DISCOUNT_RATE, ";
        //        sSql = sSql + "SURCHARGE_RATE, SURCHARGE_AMT, CANCEL_REASON, CANCEL_DATE, ";
        //        sSql = sSql + "CARTON_QTY, INNER_QTY, CUBIC_MTR, CARTON_WEIGHT, TOTAL_VOLUME, ";
        //        sSql = sSql + "NOTES, SPECIAL_INSTRUCTIONS, SURCHARGE_REASON";
        //        sSql = sSql + " ) VALUES ( ";
        //        sSql = sSql + "@ORDER_NO, @SEQUENCE, @ORDER_DATE, @STOCK_CODE, @CATEGORY, @LOT_ID, ";
        //        sSql = sSql + "@WAREHOUSE_ID, @ORIGIN_SO_NUM, @ORIGIN_SO_SEQUENCE, @PO_LINK_PO_NUM, ";
        //        sSql = sSql + "@PO_LINK_PO_SEQUENCE, @CUSTOMER_ORDER_NO, @SALES_CONTRACT, ";
        //        sSql = sSql + "@UBI_STATUS, @SPECIAL_ITEM, @PRIORITY, @FUMIGATION_REQ, @GST_EXEMPT, ";
        //        sSql = sSql + "@QTY_ORD, @PRICE, @VALUE_ORD, @QTY_DELIVERED, @VALUE_DELIVERED, ";
        //        sSql = sSql + "@QTY_CANCELLED, @VALUE_CANCELLED, @TAX_RATE, @TAX_AMOUNT, ";
        //        sSql = sSql + "@COMMISSION_RATE, @COMMISSION_AMT, @DISCOUNT_AMT, @DISCOUNT_RATE, ";
        //        sSql = sSql + "@SURCHARGE_RATE, @SURCHARGE_AMT, @CANCEL_REASON, @CANCEL_DATE, ";
        //        sSql = sSql + "@CARTON_QTY, @INNER_QTY, @CUBIC_MTR, @CARTON_WEIGHT, @TOTAL_VOLUME, ";
        //        sSql = sSql + "@NOTES, @SPECIAL_INSTRUCTIONS, @SURCHARGE_REASON";
        //        sSql = sSql + " )";


        //        foreach (OEItem cI in c.Items)
        //        {
        //            sqlCmd = null;
        //            sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);

        //            sqlCmd.Parameters.AddWithValue("@ORDER_NO", cI.OrderNo);
        //            sqlCmd.Parameters.AddWithValue("@SEQUENCE", cI.Sequence);
        //            sqlCmd.Parameters.AddWithValue("@ORDER_DATE", cI.OrderDate);
        //            sqlCmd.Parameters.AddWithValue("@STOCK_CODE", cI.StockCode);
        //            sqlCmd.Parameters.AddWithValue("@CATEGORY", cI.Category);
        //            sqlCmd.Parameters.AddWithValue("@LOT_ID", cI.LotId);
        //            sqlCmd.Parameters.AddWithValue("@WAREHOUSE_ID", cI.WarehouseId);
        //            sqlCmd.Parameters.AddWithValue("@ORIGIN_SO_NUM", cI.OriginSoNum);
        //            sqlCmd.Parameters.AddWithValue("@ORIGIN_SO_SEQUENCE", cI.OriginSoSequence);
        //            sqlCmd.Parameters.AddWithValue("@PO_LINK_PO_NUM", cI.PoLinkPoNum);
        //            sqlCmd.Parameters.AddWithValue("@PO_LINK_PO_SEQUENCE", cI.PoLinkPoSequence);
        //            //sqlCmd.Parameters.AddWithValue("@CUSTOMER_ORDER_NO", cI.CustomerOrderNo);
        //            sqlCmd.Parameters.AddWithValue("@CUSTOMER_ORDER_NO", "");
        //            sqlCmd.Parameters.AddWithValue("@SALES_CONTRACT", cI.SalesContract);
        //            if (cI.UbiStatus != c.UbiStatus) cI.UbiStatus = c.UbiStatus;
        //            sqlCmd.Parameters.AddWithValue("@UBI_STATUS", cI.UbiStatus);
        //            if (cI.SpecialItem)
        //            {
        //                sqlCmd.Parameters.AddWithValue("@SPECIAL_ITEM", 1);

        //            }
        //            else
        //            {
        //                sqlCmd.Parameters.AddWithValue("@SPECIAL_ITEM", 0);

        //            }
        //            sqlCmd.Parameters.AddWithValue("@PRIORITY", cI.Priority);
        //            if (cI.FumigationReq)
        //            {
        //                sqlCmd.Parameters.AddWithValue("@FUMIGATION_REQ", 1);
        //            }
        //            else
        //            {
        //                sqlCmd.Parameters.AddWithValue("@FUMIGATION_REQ", 0);
        //            }
        //            if (cI.GstExempt)
        //            {
        //                sqlCmd.Parameters.AddWithValue("@GST_EXEMPT", 1);
        //            }
        //            else
        //            {
        //                sqlCmd.Parameters.AddWithValue("@GST_EXEMPT", 0);
        //            }
        //            sqlCmd.Parameters.AddWithValue("@QTY_ORD", cI.QtyOrd);
        //            sqlCmd.Parameters.AddWithValue("@PRICE", cI.Price);
        //            sqlCmd.Parameters.AddWithValue("@VALUE_ORD", cI.ValueOrd);
        //            sqlCmd.Parameters.AddWithValue("@QTY_DELIVERED", cI.QtyDelivered);
        //            sqlCmd.Parameters.AddWithValue("@VALUE_DELIVERED", cI.ValueDelivered);
        //            sqlCmd.Parameters.AddWithValue("@QTY_CANCELLED", cI.QtyCancelled);
        //            sqlCmd.Parameters.AddWithValue("@VALUE_CANCELLED", cI.ValueCancelled);
        //            sqlCmd.Parameters.AddWithValue("@TAX_RATE", cI.TaxRate);
        //            sqlCmd.Parameters.AddWithValue("@TAX_AMOUNT", cI.TaxAmount);
        //            sqlCmd.Parameters.AddWithValue("@COMMISSION_RATE", cI.CommissionRate);
        //            sqlCmd.Parameters.AddWithValue("@COMMISSION_AMT", cI.CommissionAmt);
        //            sqlCmd.Parameters.AddWithValue("@DISCOUNT_AMT", cI.DiscountAmt);
        //            sqlCmd.Parameters.AddWithValue("@DISCOUNT_RATE", cI.DiscountRate);
        //            sqlCmd.Parameters.AddWithValue("@SURCHARGE_RATE", cI.SurchargeRate);
        //            sqlCmd.Parameters.AddWithValue("@SURCHARGE_AMT", cI.SurchargeAmt);
        //            sqlCmd.Parameters.AddWithValue("@CANCEL_REASON", cI.CancelReason);
        //            sqlCmd.Parameters.AddWithValue("@CANCEL_DATE", cI.CancelDate);
        //            sqlCmd.Parameters.AddWithValue("@CARTON_QTY", cI.CartonQty);
        //            sqlCmd.Parameters.AddWithValue("@INNER_QTY", cI.InnerQty);
        //            sqlCmd.Parameters.AddWithValue("@CUBIC_MTR", cI.CubicMtr);
        //            sqlCmd.Parameters.AddWithValue("@CARTON_WEIGHT", cI.CartonWeight);
        //            sqlCmd.Parameters.AddWithValue("@TOTAL_VOLUME", cI.TotalVolume);
        //            sqlCmd.Parameters.AddWithValue("@NOTES", cI.Notes);
        //            sqlCmd.Parameters.AddWithValue("@SPECIAL_INSTRUCTIONS", cI.SpecialInstructions);
        //            sqlCmd.Parameters.AddWithValue("@SURCHARGE_REASON", cI.SurchargeReason);

        //            sqlCmd.ExecuteNonQuery();

        //            if (bUpdateMaster)
        //            {
        //                sqlCmd = null;
        //                sqlCmd = new SqlCommand(sSqlUpdateStock, SqlHost.DBConn);
        //                sqlCmd.Parameters.AddWithValue("@STOCK_CODE", cI.StockCode);
        //                sqlCmd.Parameters.AddWithValue("@DEF_BASE_SELL", cI.Price);
        //                sqlCmd.ExecuteNonQuery();
        //            }
        //            if (bUpdateLot)
        //            {
        //                sqlCmd = null;
        //                sqlCmd = new SqlCommand(sSqlUpdateStockLot, SqlHost.DBConn);
        //                sqlCmd.Parameters.AddWithValue("@STOCK_CODE", cI.StockCode);
        //                sqlCmd.Parameters.AddWithValue("@SERIAL_LOT_NO", cI.LotId);
        //                if (c.DBUpdateOEPrice)
        //                {
        //                    sqlCmd.Parameters.AddWithValue("@BASE_SELL", cI.Price);
        //                }
        //                if (c.DBUpdateCartonCubic)
        //                {
        //                    sqlCmd.Parameters.AddWithValue("@VOLUME", cI.CubicMtr);
        //                }
        //                if (c.DBUpdateCtnQty)
        //                {
        //                    sqlCmd.Parameters.AddWithValue("@CARTON_QTY", cI.CartonQty);
        //                }
        //                if (c.DBUpdateInnerQty)
        //                {
        //                    sqlCmd.Parameters.AddWithValue("@INNER_QTY", cI.InnerQty);
        //                }
        //                sqlCmd.ExecuteNonQuery();
        //            }
        //        }

        //        OperationResponse r = new OperationResponse(true);
        //        r.OperationId = o.OperationId;
        //        r.Sequence = o.Sequence;
        //        r.Response = "Record Created";

        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new OperationResponse(false, ex.Message);
        //    }
        //}
        //public static OperationResponse OEHeaderUpdate(OEHeaderOperation o)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    OEHeader c;
        //    try
        //    {
        //        c = o.DataMember;

        //        sSql = "update REPORT_QUICK_HEADER set " +
        //            "TITLE = @Title, DESCRIPTION = @Description, ID_TYPE = @IdType, COMMENT = @Comment " +
        //            "where REPORT_ID = @ReportId";
        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        //sqlCmd.Parameters.AddWithValue("@Title", c.Title);
        //        //sqlCmd.Parameters.AddWithValue("@Description", c.Description);
        //        //sqlCmd.Parameters.AddWithValue("@IdType", c.IdType);
        //        //sqlCmd.Parameters.AddWithValue("@Comment", c.Comment);
        //        //sqlCmd.Parameters.AddWithValue("@ReportId", c.ReportId);
        //        sqlCmd.ExecuteNonQuery();

        //        OperationResponse r = new OperationResponse(true);
        //        r.OperationId = o.OperationId;
        //        r.Sequence = o.Sequence;
        //        r.Response = "Record Updated";

        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new OperationResponse(false, ex.Message);
        //    }
        //}
        //public static OperationResponse OEHeaderDelete(OEHeaderOperation o)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    OEHeader c;
        //    try
        //    {
        //        c = o.DataMember;

        //        sSql = "DELETE FROM SO_HEADER WHERE ORDER_NO = @ORDERNO";
        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        sqlCmd.Parameters.AddWithValue("@ORDERNO", c.OrderNo);
        //        sqlCmd.ExecuteNonQuery();

        //        sSql = "DELETE FROM SO_LINE WHERE ORDER_NO = @ORDERNO";
        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        sqlCmd.Parameters.AddWithValue("@ORDERNO", c.OrderNo);
        //        sqlCmd.ExecuteNonQuery();

        //        OperationResponse r = new OperationResponse(true);
        //        r.OperationId = o.OperationId;
        //        r.Sequence = o.Sequence;
        //        r.Response = "Record Deleted";

        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new OperationResponse(false, ex.Message);
        //    }
        //}



        //public static OperationResponse ProcessPOHeaderOperation(POHeaderOperation o)
        //{
        //    switch (o.Verb)
        //    {
        //        case OperationVerb.Create:
        //            return POHeaderCreate(o);
        //        case OperationVerb.Read:
        //            break;
        //        case OperationVerb.Update:
        //            return POHeaderUpdate(o);
        //        case OperationVerb.Delete:
        //            return POHeaderDelete(o);
        //        default:
        //            return null;
        //    }
        //    return null;
        //}
        //public static OperationResponse POHeaderCreate(POHeaderOperation o)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    bool bUpdateMaster;
        //    bool bUpdateLot;
        //    string sSqlUpdateStock = "";
        //    string sSqlUpdateStockLot = "";
        //    POHeader c;
        //    try
        //    {
        //        c = o.DataMember;

        //        if (c.StockMasterfileUpdateRequired())
        //        {
        //            bUpdateMaster = true;
        //            sSqlUpdateStock = "update STOCK set FIRST_PRICE = @FIRST_PRICE where STOCK_CODE = @STOCK_CODE";
        //        }
        //        else
        //        {
        //            bUpdateMaster = false;
        //        }
        //        if (c.StockLotMasterfileUpdateRequired())
        //        {
        //            bUpdateLot = true;
        //            sSqlUpdateStockLot = "update STOCK_LOT set ";
        //            if (c.DBUpdatePOPrice) sSqlUpdateStockLot = sSqlUpdateStockLot + "FIRST_PRICE = @FIRST_PRICE, ";
        //            if (c.DBUpdateCartonCubic) sSqlUpdateStockLot = sSqlUpdateStockLot + "VOLUME = @VOLUME, ";
        //            if (c.DBUpdateCtnQty) sSqlUpdateStockLot = sSqlUpdateStockLot + "CARTON_QTY = @CARTON_QTY, ";
        //            if (c.DBUpdateInnerQty) sSqlUpdateStockLot = sSqlUpdateStockLot + "INNER_QTY = @INNER_QTY, ";
        //            sSqlUpdateStockLot = sSqlUpdateStockLot.Substring(0, sSqlUpdateStockLot.Length - 2);
        //            sSqlUpdateStockLot = sSqlUpdateStockLot + " where STOCK_CODE = @STOCK_CODE and SERIAL_LOT_NO = @SERIAL_LOT_NO";
        //        }
        //        else
        //        {
        //            bUpdateLot = false;
        //        }

        //        sSql = "INSERT INTO PO_HEADER ( ";
        //        sSql = sSql + "PO_NUM, PO_DATE, SUPPLIER_ID, SUPP_NAME, SUPP_CONTACT_NAME, ";
        //        sSql = sSql + "PORT_OF_ORIGIN, DESTINATION_PORT, DEL_INSTRUCTION_1, DEL_INSTRUCTION_2, ";
        //        sSql = sSql + "CURRENCY, EXCHANGE_RATE, REQ_DATE, EXP_DATE_1, EXP_DATE_2, FIRST_SHIP_DATE, ";
        //        sSql = sSql + "DEPOSIT_STATUS, SUPP_PO_TYPE, ADDITIONAL_REFERENCE, FCL, ORIGIN_PO_NUM, ";
        //        sSql = sSql + "OE_LINK_NO, WAREHOUSE_ID, CBA_SUPP_TERMS, COMMISSION_RATE, ";
        //        sSql = sSql + "COMMISSION_AMOUNT, COMMISSION_AGENT, COSTINGS_AMT, COSTINGS_APPLD, ";
        //        sSql = sSql + "ADDITIONAL_CHARGES_AMT, ADDITIONAL_CHARGES_APPLD, DELETE_FLAG, ";
        //        sSql = sSql + "ORDER_VALUE, SHIPPED_VALUE, RECD_VALUE, EXTRA_CURRENCY, FUMIGATION, ";
        //        sSql = sSql + "TOTAL_CARTONS, TOTAL_VOLUME_MTR, TOTAL_WEIGHT, EXTRA_EXCHANGE_RATE, ";
        //        sSql = sSql + "ORDER_STATUS, FINAL_SHIP_DATE, SPECIAL_INSTRUCTIONS, NOTES, RAISED_BY, ";
        //        sSql = sSql + "SHIPPING_LINE, SHIPPING_CONTRACT, GOODS_DESC, BOSS, CONTEXT ";
        //        sSql = sSql + " ) VALUES ( ";
        //        sSql = sSql + "@PO_NUM, @PO_DATE, @SUPPLIER_ID, @SUPP_NAME, @SUPP_CONTACT_NAME, ";
        //        sSql = sSql + "@PORT_OF_ORIGIN, @DESTINATION_PORT, @DEL_INSTRUCTION_1, @DEL_INSTRUCTION_2, ";
        //        sSql = sSql + "@CURRENCY, @EXCHANGE_RATE, @REQ_DATE, @EXP_DATE_1, @EXP_DATE_2, @FIRST_SHIP_DATE, ";
        //        sSql = sSql + "@DEPOSIT_STATUS, @SUPP_PO_TYPE, @ADDITIONAL_REFERENCE, @FCL, @ORIGIN_PO_NUM, ";
        //        sSql = sSql + "@OE_LINK_NO, @WAREHOUSE_ID, @CBA_SUPP_TERMS, @COMMISSION_RATE, ";
        //        sSql = sSql + "@COMMISSION_AMOUNT, @COMMISSION_AGENT, @COSTINGS_AMT, @COSTINGS_APPLD, ";
        //        sSql = sSql + "@ADDITIONAL_CHARGES_AMT, @ADDITIONAL_CHARGES_APPLD, @DELETE_FLAG, ";
        //        sSql = sSql + "@ORDER_VALUE, @SHIPPED_VALUE, @RECD_VALUE, @EXTRA_CURRENCY, @FUMIGATION, ";
        //        sSql = sSql + "@TOTAL_CARTONS, @TOTAL_VOLUME_MTR, @TOTAL_WEIGHT, @EXTRA_EXCHANGE_RATE, ";
        //        sSql = sSql + "@ORDER_STATUS, @FINAL_SHIP_DATE, @SPECIAL_INSTRUCTIONS, @NOTES, @RAISED_BY, ";
        //        sSql = sSql + "@SHIPPING_LINE, @SHIPPING_CONTRACT, @GOODS_DESC, @BOSS, @CONTEXT ";
        //        sSql = sSql + " )";

        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);

        //        sqlCmd.Parameters.AddWithValue("@PO_NUM", c.PoNum);
        //        if (c.PoDate < new DateTime(1901, 2, 2))
        //        {
        //            sqlCmd.Parameters.AddWithValue("@PO_DATE", DateTime.Today);
        //        }
        //        else
        //        {
        //            sqlCmd.Parameters.AddWithValue("@PO_DATE", c.PoDate);
        //        }
        //        sqlCmd.Parameters.AddWithValue("@SUPPLIER_ID", c.SupplierId);
        //        sqlCmd.Parameters.AddWithValue("@SUPP_NAME", c.SuppName);
        //        sqlCmd.Parameters.AddWithValue("@SUPP_CONTACT_NAME", c.SuppContactName);
        //        sqlCmd.Parameters.AddWithValue("@PORT_OF_ORIGIN", c.PortOfOrigin);
        //        sqlCmd.Parameters.AddWithValue("@DESTINATION_PORT", c.DestinationPort);
        //        sqlCmd.Parameters.AddWithValue("@DEL_INSTRUCTION_1", c.DelInstruction1);
        //        sqlCmd.Parameters.AddWithValue("@DEL_INSTRUCTION_2", c.DelInstruction2);
        //        sqlCmd.Parameters.AddWithValue("@CURRENCY", c.Currency);
        //        sqlCmd.Parameters.AddWithValue("@EXCHANGE_RATE", c.ExchangeRate);
        //        sqlCmd.Parameters.AddWithValue("@REQ_DATE", c.ReqDate);
        //        sqlCmd.Parameters.AddWithValue("@EXP_DATE_1", c.ExpDate1);
        //        sqlCmd.Parameters.AddWithValue("@EXP_DATE_2", c.ExpDate2);
        //        sqlCmd.Parameters.AddWithValue("@FIRST_SHIP_DATE", c.FirstShipDate);
        //        sqlCmd.Parameters.AddWithValue("@DEPOSIT_STATUS", c.DepositStatus);
        //        sqlCmd.Parameters.AddWithValue("@SUPP_PO_TYPE", c.SuppPoType);
        //        sqlCmd.Parameters.AddWithValue("@ADDITIONAL_REFERENCE", c.AdditionalReference);
        //        sqlCmd.Parameters.AddWithValue("@FCL", c.Fcl);
        //        sqlCmd.Parameters.AddWithValue("@ORIGIN_PO_NUM", c.OriginPoNum);
        //        sqlCmd.Parameters.AddWithValue("@OE_LINK_NO", c.OeLinkNo);
        //        sqlCmd.Parameters.AddWithValue("@WAREHOUSE_ID", c.WarehouseId);
        //        sqlCmd.Parameters.AddWithValue("@CBA_SUPP_TERMS", c.CbaSuppTerms);
        //        sqlCmd.Parameters.AddWithValue("@COMMISSION_RATE", c.CommissionRate);
        //        sqlCmd.Parameters.AddWithValue("@COMMISSION_AMOUNT", c.CommissionAmount);
        //        sqlCmd.Parameters.AddWithValue("@COMMISSION_AGENT", c.CommissionAgent);
        //        sqlCmd.Parameters.AddWithValue("@COSTINGS_AMT", c.CostingsAmt);
        //        sqlCmd.Parameters.AddWithValue("@COSTINGS_APPLD", c.CostingsAppld);
        //        sqlCmd.Parameters.AddWithValue("@ADDITIONAL_CHARGES_AMT", c.AdditionalChargesAmt);
        //        sqlCmd.Parameters.AddWithValue("@ADDITIONAL_CHARGES_APPLD", c.AdditionalChargesAppld);
        //        if (c.DeleteFlag)
        //        {
        //            sqlCmd.Parameters.AddWithValue("@DELETE_FLAG", 1);
        //        }
        //        else
        //        {
        //            sqlCmd.Parameters.AddWithValue("@DELETE_FLAG", 0);
        //        }
        //        sqlCmd.Parameters.AddWithValue("@ORDER_VALUE", c.OrderValue);
        //        sqlCmd.Parameters.AddWithValue("@SHIPPED_VALUE", c.ShippedValue);
        //        sqlCmd.Parameters.AddWithValue("@RECD_VALUE", c.RecdValue);
        //        sqlCmd.Parameters.AddWithValue("@EXTRA_CURRENCY", c.ExtraCurrency);
        //        if (c.Fumigation)
        //        {
        //            sqlCmd.Parameters.AddWithValue("@FUMIGATION", 1);
        //        }
        //        else
        //        {
        //            sqlCmd.Parameters.AddWithValue("@FUMIGATION", 0);
        //        }
        //        sqlCmd.Parameters.AddWithValue("@TOTAL_CARTONS", c.TotalCartons);
        //        sqlCmd.Parameters.AddWithValue("@TOTAL_VOLUME_MTR", c.TotalVolumeMtr);
        //        sqlCmd.Parameters.AddWithValue("@TOTAL_WEIGHT", c.TotalWeight);
        //        sqlCmd.Parameters.AddWithValue("@EXTRA_EXCHANGE_RATE", c.ExtraExchangeRate);
        //        sqlCmd.Parameters.AddWithValue("@ORDER_STATUS", c.OrderStatus);
        //        sqlCmd.Parameters.AddWithValue("@FINAL_SHIP_DATE", c.FinalShipDate);
        //        sqlCmd.Parameters.AddWithValue("@SPECIAL_INSTRUCTIONS", c.SpecialInstructions);
        //        sqlCmd.Parameters.AddWithValue("@NOTES", c.Notes);
        //        sqlCmd.Parameters.AddWithValue("@RAISED_BY", c.RaisedBy);
        //        sqlCmd.Parameters.AddWithValue("@SHIPPING_LINE", c.ShippingLine);
        //        sqlCmd.Parameters.AddWithValue("@SHIPPING_CONTRACT", c.ShippingContract);
        //        sqlCmd.Parameters.AddWithValue("@GOODS_DESC", c.GoodsDesc);
        //        sqlCmd.Parameters.AddWithValue("@BOSS", c.Boss);
        //        sqlCmd.Parameters.AddWithValue("@CONTEXT", c.Context);

        //        sqlCmd.ExecuteNonQuery();

        //        sSql = "INSERT INTO PO_LINE ( ";
        //        sSql = sSql + "PO_NUM, SEQUENCE, STOCK_CODE, LOT_ID, WAREHOUSE_ID, ";
        //        sSql = sSql + "PRICE, TAX_RATE, CARTON_QTY, INNER_QTY, CUBIC_MTR, BARCODE, ";
        //        sSql = sSql + "FUMIGATION_REQ, SUPPLIER_PART_NO, CARTON_WEIGHT, QTY_ORDERED, ";
        //        sSql = sSql + "QTY_SHIPPED, QTY_RECD, QTY_CANCEL, VALUE_ORDERED, VALUE_SHIPPED, ";
        //        sSql = sSql + "VALUE_RECD, VALUE_CANCEL, ORIGIN_PO_NUM, ORIGIN_PO_SEQUENCE, ";
        //        sSql = sSql + "OE_LINK_ORDER_NUM, OE_LINK_ORDER_SEQUENCE, ";
        //        sSql = sSql + "SPECIAL_INSTRUCTIONS, NOTES, STATUS ";
        //        sSql = sSql + " ) VALUES ( ";
        //        sSql = sSql + "@PO_NUM, @SEQUENCE, @STOCK_CODE, @LOT_ID, @WAREHOUSE_ID, ";
        //        sSql = sSql + "@PRICE, @TAX_RATE, @CARTON_QTY, @INNER_QTY, @CUBIC_MTR, @BARCODE, ";
        //        sSql = sSql + "@FUMIGATION_REQ, @SUPPLIER_PART_NO, @CARTON_WEIGHT, @QTY_ORDERED, ";
        //        sSql = sSql + "@QTY_SHIPPED, @QTY_RECD, @QTY_CANCEL, @VALUE_ORDERED, @VALUE_SHIPPED, ";
        //        sSql = sSql + "@VALUE_RECD, @VALUE_CANCEL, @ORIGIN_PO_NUM, @ORIGIN_PO_SEQUENCE, ";
        //        sSql = sSql + "@OE_LINK_ORDER_NUM, @OE_LINK_ORDER_SEQUENCE, ";
        //        sSql = sSql + "@SPECIAL_INSTRUCTIONS, @NOTES, @STATUS ";
        //        sSql = sSql + " )";

        //        foreach (POItem cI in c.Items)
        //        {
        //            sqlCmd = null;
        //            sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);

        //            sqlCmd.Parameters.AddWithValue("@PO_NUM", cI.PoNum);
        //            sqlCmd.Parameters.AddWithValue("@SEQUENCE", cI.Sequence);

        //            sqlCmd.Parameters.AddWithValue("@STOCK_CODE", cI.StockCode);
        //            sqlCmd.Parameters.AddWithValue("@LOT_ID", cI.LotId);
        //            sqlCmd.Parameters.AddWithValue("@WAREHOUSE_ID", cI.WarehouseId);
        //            sqlCmd.Parameters.AddWithValue("@PRICE", cI.Price);
        //            sqlCmd.Parameters.AddWithValue("@TAX_RATE", cI.TaxRate);
        //            sqlCmd.Parameters.AddWithValue("@CARTON_QTY", cI.CartonQty);
        //            sqlCmd.Parameters.AddWithValue("@INNER_QTY", cI.InnerQty);
        //            sqlCmd.Parameters.AddWithValue("@CUBIC_MTR", cI.CubicMtr);
        //            sqlCmd.Parameters.AddWithValue("@BARCODE", cI.Barcode);
        //            if (cI.FumigationReq)
        //            {
        //                sqlCmd.Parameters.AddWithValue("@FUMIGATION_REQ", 1);
        //            }
        //            else
        //            {
        //                sqlCmd.Parameters.AddWithValue("@FUMIGATION_REQ", 0);
        //            }
        //            sqlCmd.Parameters.AddWithValue("@SUPPLIER_PART_NO", cI.SupplierPartNo);
        //            sqlCmd.Parameters.AddWithValue("@CARTON_WEIGHT", cI.CartonWeight);
        //            sqlCmd.Parameters.AddWithValue("@QTY_ORDERED", cI.QtyOrdered);
        //            sqlCmd.Parameters.AddWithValue("@QTY_SHIPPED", cI.QtyShipped);
        //            sqlCmd.Parameters.AddWithValue("@QTY_RECD", cI.QtyRecd);
        //            sqlCmd.Parameters.AddWithValue("@QTY_CANCEL", cI.QtyCancel);
        //            sqlCmd.Parameters.AddWithValue("@VALUE_ORDERED", cI.ValueOrdered);
        //            sqlCmd.Parameters.AddWithValue("@VALUE_SHIPPED", cI.ValueShipped);
        //            sqlCmd.Parameters.AddWithValue("@VALUE_RECD", cI.ValueRecd);
        //            sqlCmd.Parameters.AddWithValue("@VALUE_CANCEL", cI.ValueCancel);
        //            sqlCmd.Parameters.AddWithValue("@ORIGIN_PO_NUM", cI.OriginPoNum);
        //            sqlCmd.Parameters.AddWithValue("@ORIGIN_PO_SEQUENCE", cI.OriginPoSequence);
        //            sqlCmd.Parameters.AddWithValue("@OE_LINK_ORDER_NUM", cI.OeLinkOrderNum);
        //            sqlCmd.Parameters.AddWithValue("@OE_LINK_ORDER_SEQUENCE", cI.OeLinkOrderSequence);
        //            sqlCmd.Parameters.AddWithValue("@SPECIAL_INSTRUCTIONS", cI.SpecialInstructions);
        //            sqlCmd.Parameters.AddWithValue("@NOTES", cI.Notes);
        //            sqlCmd.Parameters.AddWithValue("@STATUS", cI.Status);

        //            sqlCmd.ExecuteNonQuery();

        //            if (bUpdateMaster)
        //            {
        //                sqlCmd = null;
        //                sqlCmd = new SqlCommand(sSqlUpdateStock, SqlHost.DBConn);
        //                sqlCmd.Parameters.AddWithValue("@STOCK_CODE", cI.StockCode);
        //                sqlCmd.Parameters.AddWithValue("@FIRST_PRICE", cI.Price);
        //                sqlCmd.ExecuteNonQuery();
        //            }
        //            if (bUpdateLot)
        //            {
        //                sqlCmd = null;
        //                sqlCmd = new SqlCommand(sSqlUpdateStockLot, SqlHost.DBConn);
        //                sqlCmd.Parameters.AddWithValue("@STOCK_CODE", cI.StockCode);
        //                sqlCmd.Parameters.AddWithValue("@SERIAL_LOT_NO", cI.LotId);
        //                if (c.DBUpdatePOPrice)
        //                {
        //                    sqlCmd.Parameters.AddWithValue("@FIRST_PRICE", cI.Price);
        //                }
        //                if (c.DBUpdateCartonCubic)
        //                {
        //                    sqlCmd.Parameters.AddWithValue("@VOLUME", cI.CubicMtr);
        //                }
        //                if (c.DBUpdateCtnQty)
        //                {
        //                    sqlCmd.Parameters.AddWithValue("@CARTON_QTY", cI.CartonQty);
        //                }
        //                if (c.DBUpdateInnerQty)
        //                {
        //                    sqlCmd.Parameters.AddWithValue("@INNER_QTY", cI.InnerQty);
        //                }
        //                sqlCmd.ExecuteNonQuery();
        //            }
        //        }

        //        OperationResponse r = new OperationResponse(true);
        //        r.OperationId = o.OperationId;
        //        r.Sequence = o.Sequence;
        //        r.Response = "Record Created";

        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new OperationResponse(false, ex.Message);
        //    }
        //}
        //public static OperationResponse POHeaderUpdate(POHeaderOperation o)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    POHeader c;
        //    try
        //    {
        //        c = o.DataMember;

        //        sSql = "update REPORT_QUICK_HEADER set " +
        //            "TITLE = @Title, DESCRIPTION = @Description, ID_TYPE = @IdType, COMMENT = @Comment " +
        //            "where REPORT_ID = @ReportId";
        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        //sqlCmd.Parameters.AddWithValue("@Title", c.Title);
        //        //sqlCmd.Parameters.AddWithValue("@Description", c.Description);
        //        //sqlCmd.Parameters.AddWithValue("@IdType", c.IdType);
        //        //sqlCmd.Parameters.AddWithValue("@Comment", c.Comment);
        //        //sqlCmd.Parameters.AddWithValue("@ReportId", c.ReportId);
        //        sqlCmd.ExecuteNonQuery();

        //        OperationResponse r = new OperationResponse(true);
        //        r.OperationId = o.OperationId;
        //        r.Sequence = o.Sequence;
        //        r.Response = "Record Updated";

        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new OperationResponse(false, ex.Message);
        //    }
        //}
        //public static OperationResponse POHeaderDelete(POHeaderOperation o)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    POHeader c;
        //    try
        //    {
        //        c = o.DataMember;

        //        sSql = "DELETE FROM PO_HEADER WHERE PO_NUM = @PONUM";
        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        sqlCmd.Parameters.AddWithValue("@PONUM", c.PoNum);
        //        sqlCmd.ExecuteNonQuery();

        //        sSql = "DELETE FROM PO_LINE WHERE PO_NUM = @PONUM";
        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        sqlCmd.Parameters.AddWithValue("@PONUM", c.PoNum);
        //        sqlCmd.ExecuteNonQuery();

        //        OperationResponse r = new OperationResponse(true);
        //        r.OperationId = o.OperationId;
        //        r.Sequence = o.Sequence;
        //        r.Response = "Record Deleted";

        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new OperationResponse(false, ex.Message);
        //    }
        //}


        //public static OperationResponse ProcessStockOperation(StockOperation o)
        //{
        //    switch (o.Verb)
        //    {
        //        case OperationVerb.Create:
        //            return StockCreate(o);
        //        case OperationVerb.Read:
        //            break;
        //        case OperationVerb.Update:
        //            return StockUpdate(o);
        //        case OperationVerb.Delete:
        //            return StockDelete(o);
        //        default:
        //            return null;
        //    }
        //    return null;
        //}
        //public static OperationResponse StockCreate(StockOperation o)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    Stock c;
        //    try
        //    {
        //        c = o.DataMember;

        //        sSql = "insert into REPORT_QUICK_HEADER " +
        //            "( REPORT_ID, TITLE, DESCRIPTION, ID_TYPE, COMMENT )" +
        //            " values " +
        //            "( @ReportId, @Title, @Description, @IdType, @Comment )";
        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        //sqlCmd.Parameters.AddWithValue("@ReportId", c.ReportId);
        //        //sqlCmd.Parameters.AddWithValue("@Title", c.Title);
        //        //sqlCmd.Parameters.AddWithValue("@Description", c.Description);
        //        //sqlCmd.Parameters.AddWithValue("@IdType", c.IdType);
        //        //sqlCmd.Parameters.AddWithValue("@Comment", c.Comment);
        //        sqlCmd.ExecuteNonQuery();

        //        OperationResponse r = new OperationResponse(true);
        //        r.OperationId = o.OperationId;
        //        r.Sequence = o.Sequence;
        //        r.Response = "Record Deleted";

        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new OperationResponse(false, ex.Message);
        //    }
        //}
        //public static OperationResponse StockUpdate(StockOperation o)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    Stock c;
        //    try
        //    {
        //        c = o.DataMember;

        //        sSql = "update REPORT_QUICK_HEADER set " +
        //            "TITLE = @Title, DESCRIPTION = @Description, ID_TYPE = @IdType, COMMENT = @Comment " +
        //            "where REPORT_ID = @ReportId";
        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        //sqlCmd.Parameters.AddWithValue("@Title", c.Title);
        //        //sqlCmd.Parameters.AddWithValue("@Description", c.Description);
        //        //sqlCmd.Parameters.AddWithValue("@IdType", c.IdType);
        //        //sqlCmd.Parameters.AddWithValue("@Comment", c.Comment);
        //        //sqlCmd.Parameters.AddWithValue("@ReportId", c.ReportId);
        //        sqlCmd.ExecuteNonQuery();

        //        OperationResponse r = new OperationResponse(true);
        //        r.OperationId = o.OperationId;
        //        r.Sequence = o.Sequence;
        //        r.Response = "Record Updated";

        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new OperationResponse(false, ex.Message);
        //    }
        //}
        //public static OperationResponse StockDelete(StockOperation o)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    Stock c;
        //    try
        //    {
        //        c = o.DataMember;

        //        sSql = "delete from REPORT_QUICK_HEADER " +
        //            "where REPORT_ID = @ReportId";
        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        //sqlCmd.Parameters.AddWithValue("@ReportId", c.ReportId);
        //        sqlCmd.ExecuteNonQuery();

        //        OperationResponse r = new OperationResponse(true);
        //        r.OperationId = o.OperationId;
        //        r.Sequence = o.Sequence;
        //        r.Response = "Record Deleted";

        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        return new OperationResponse(false, ex.Message);
        //    }
        //}



    }
}

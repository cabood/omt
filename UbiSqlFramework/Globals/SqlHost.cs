﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.IO;
using UbiWeb.Security;
using UbiSqlFramework.MFServices;

namespace UbiSqlFramework
{
    public static class SqlHost
    {
        private static SqlConnection oCn = new SqlConnection();
        private static SqlConnection oCnReport = new SqlConnection();
        private static SqlConnection oCnForecast = new SqlConnection();

        //private static string sForecastDatabaseConnectionName;

        private static Dictionary<string, DatabaseConnectionProfile> cDbConnections = new Dictionary<string, DatabaseConnectionProfile>();
        private static DatabaseConnectionProfile cCurrentConnection;
        private static DatabaseConnectionProfile cCurrentForecastConnection;

        private static XmlDocument xmlSettings;
        private static string sAppPath;

        //private static DataRow cUser;
        private static String sWebSessionKey;
        private static String sDBConnectionFilter;
        
        static SqlHost()
        {
            LoadSettings();
        }


        public static bool LoadSettings()
        {
            try
            {
                sAppPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);

                string sFile = sAppPath + "\\DBConfig.xml";
                xmlSettings = new XmlDocument();
                xmlSettings.Load(sFile);
                LoadDatabaseConnections();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static string WebSessionKey
        {
            get { return sWebSessionKey; }
            set { sWebSessionKey = value; }
        }
        public static string DBConnectionFilter
        {
            get { return sDBConnectionFilter; }
            set { sDBConnectionFilter = value; }
        }

        //public static bool LoadSettings(string sFilePath)
        //{
        //    try
        //    {
        //        sAppPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
        //        //sAppPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        //        //sAppPath = System.Web.Hosting.HostingEnvironment;
        //        //sAppPath = (new System.Uri(Assembly.Get))
        //        string sFile = sAppPath + "\\DBConfig.xml";
        //        xmlSettings = new XmlDocument();
        //        xmlSettings.Load(sFile);
        //        LoadDatabaseConnections();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        //MessageBox.Show("XML settings failed to load!", "Damnit!");
        //        return false;
        //    }
        //}



        private static bool LoadDatabaseConnections()
        {
            try
            {
                string sTemp = "";
                SqlHost.ConnectionProfiles = new Dictionary<string, DatabaseConnectionProfile>();
                XmlNodeList cNodes = xmlSettings.SelectNodes("/ConfigurationSettings/ConnectionSettings/Connections/Connection");
                foreach (XmlNode nd in cNodes)
                {
                    DatabaseConnectionProfile db = new DatabaseConnectionProfile();
                    db.Id = nd.Attributes.GetNamedItem("id").Value;
                    db.Name = nd.Attributes.GetNamedItem("name").Value;
                    db.ServerName = nd.Attributes.GetNamedItem("servername").Value;
                    db.DatabaseName = nd.Attributes.GetNamedItem("dbname").Value;
                    sTemp = nd.Attributes.GetNamedItem("username").Value;
                    if (sTemp.Length > 0)
                    {
                        db.UserId = SecurityManager.DecryptText(sTemp);
                    }
                    else
                    {
                        db.UserId = "";
                    }
                    sTemp = nd.Attributes.GetNamedItem("password").Value;
                    if (sTemp.Length > 0)
                    {
                        db.Password = SecurityManager.DecryptText(sTemp);
                    }
                    else
                    {
                        db.Password = "";
                    }
                    db.AdditionalSettings = nd.Attributes.GetNamedItem("other").Value;
                    db.ReplicationProfileId = nd.Attributes.GetNamedItem("replicationid").Value;
                    db.ColourCode = nd.Attributes.GetNamedItem("colourcode").Value;
                    db.Context = nd.Attributes.GetNamedItem("context").Value;
                    db.SingleContext = nd.Attributes.GetNamedItem("singleContext").Value.ToUpper().Trim() == "Y" ? true : false;
                    db.Dbtype = nd.Attributes.GetNamedItem("dbtype").Value;

                    SqlHost.ConnectionProfiles.Add(db.Id, db);
                }

                XmlNode ndFilter = xmlSettings.SelectSingleNode("/ConfigurationSettings/ConnectionSettings/LoginFilter");
                if (ndFilter == null)
                {
                    DBConnectionFilter = "Core";
                }
                else
                {
                    DBConnectionFilter = ndFilter.Attributes.GetNamedItem("filterId").Value;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static string ConfigValue(string sNodeName, string sAttrib)
        {
            XmlNode xmlN = xmlSettings.SelectSingleNode(sNodeName);
            XmlElement xmlE = (XmlElement)xmlN;
            string sVal = xmlE.GetAttribute(sAttrib);
            return sVal;
        }

        //public static DataRow CurrentUser
        //{
        //    get { return cUser; }
        //    set { cUser = value; }
        //}

        public static bool IsConnected
        {
            get { return !(oCn.State == ConnectionState.Closed || oCn.State == ConnectionState.Broken); }
        }

        public static bool IsReportConnected
        {
            get { return !(oCnReport.State == ConnectionState.Closed || oCnReport.State == ConnectionState.Broken); }
        }

        public static SqlConnection DBConn
        {
            get { return oCn; }
        }

        //public static SqlConnection DBForecastConn
        //{
        //    get { return oCnForecast; }
        //}
        //public static string ForecastDatabaseConnectionName
        //{
        //    get { return sForecastDatabaseConnectionName; }
        //    set { sForecastDatabaseConnectionName = value; }
        //}

        //public static SqlConnection DBConnReport
        //{
        //    get { return oCnReport; }
        //}
        public static Dictionary<string, DatabaseConnectionProfile> ConnectionProfiles
        {
            get { return cDbConnections; }
            set { cDbConnections = value; }
        }

        public static DatabaseConnectionProfile CurrentConnectionProfile
        {
            get { return cCurrentConnection; }
            set { cCurrentConnection = value; }
        }

        public static DatabaseConnectionProfile CurrentForecastConnectionProfile
        {
            get { return cCurrentForecastConnection; }
            set { cCurrentForecastConnection = value; }
        }
        public static void ShutDown()
        {
            if (IsConnected)
            {
                oCn.Close();
                oCnForecast.Close();
            }
            if (IsReportConnected)
                oCnReport.Close();
        }

        public static bool OpenConnectionMain(string sConnId)
        {
            if (cDbConnections.ContainsKey(sConnId))
            {
                cCurrentConnection = cDbConnections[sConnId];
            }
            if (cCurrentConnection != null && cCurrentConnection.Id.Length > 0)
            {
                return OpenConnectionMain();
            }
            return false;
        }

        //public static bool OpenForecastConnectionMain(string sConnId)
        //{
        //    if (cDbConnections.ContainsKey(sConnId))
        //    {
        //        cCurrentForecastConnection = cDbConnections[sConnId];
        //    }
        //    if (cCurrentForecastConnection != null && cCurrentForecastConnection.Id.Length > 0)
        //    {
        //        return OpenForecastConnectionMain();
        //    }
        //    return false;
        //}

        public static bool OpenServiceConnection()
        {
            try
            {
                XmlNode nd = xmlSettings.SelectSingleNode("/ConfigurationSettings/ConnectionSettings/ServiceSettings");
                if (nd == null)
                {
                    LoadSettings();
                    nd = xmlSettings.SelectSingleNode("/ConfigurationSettings/ConnectionSettings/ServiceSettings");
                }
                string sConnId = nd.Attributes.GetNamedItem("connectionId").Value;
                return OpenConnectionMain(sConnId);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool OpenConnectionMain()
        {
            try
            {
                string sConn;

                sConn = cCurrentConnection.GenerateConnectionString();

                if (oCn.State == ConnectionState.Open)
                    oCn.Close();
                if (oCnReport.State == ConnectionState.Open)
                    oCnReport.Close();

                oCn.ConnectionString = sConn;
                oCn.Open();

                oCnReport.ConnectionString = sConn;
                oCnReport.Open();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //public static bool OpenForecastConnectionMain()
        //{
        //    try
        //    {
        //        string sConn;

        //        sConn = cCurrentForecastConnection.GenerateConnectionString();

        //        if (oCnForecast.State == ConnectionState.Open)
        //            oCnForecast.Close();

        //        oCnForecast.ConnectionString = sConn;
        //        oCnForecast.Open();

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        public static bool GetDataSet(string sSql, string sTable, ref DataSet oDs)
        {
            try
            {
                ReturnDataSetResponse wsR = new MFServices.ReturnDataSetResponse();
                MFServicesSoapClient r = new MFServices.MFServicesSoapClient();
                wsR = r.SetReturnDataSet(SqlHost.WebSessionKey, sSql, sTable, ref oDs);
                oDs = wsR.Response.DataSetToReturn;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        ////public static bool GetDataSet(string sSql, string sTable, ref DataSet oDs, QueryParam[] qParams)
        ////{
        ////    try
        ////    {
        ////        DataTable oDt;
        ////        SqlDataAdapter oDa;
        ////        oDt = new DataTable(sTable);
        ////        oDa = new SqlDataAdapter(sSql, oCn);
        ////        foreach (QueryParam qP in qParams)
        ////        {
        ////            oDa.Parameters.AddWithValue("@" + qP.ParamName, qP.Value);
        ////        }
        ////        oDa.Fill(oDs, sTable);
        ////        return true;
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        return false;
        ////    }
        ////}

        //public static bool GetDataTable(string sSql, string sTable, out DataTable oDt, QueryParam[] qParams)
        //{
        //    // XXXXXXXXXXXXXXXXX NEEED TO CODE THIS TO USE A SQLCOMMAND WITH PARAMETERS
        //    try
        //    {
        //        oDt = new DataTable();
        //        SqlCommand oCmd = new SqlCommand(sSql, oCn);
        //        foreach (QueryParam qP in qParams)
        //        {
        //            oCmd.Parameters.AddWithValue("@" + qP.ParamName, qP.Value);
        //        }

        //        //SqlDataReader oDr = oCmd.ExecuteReader();
        //        oDt = new DataTable();
        //        //oDt.Load(oDr);
        //        oDt.Load(oCmd.ExecuteReader());
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        oDt = new DataTable();
        //        return false;
        //    }
        //}

        //public static bool ExecuteSqlCommand(string sSql)
        //{
        //    try
        //    {
        //        SqlCommand cmd = new SqlCommand(sSql, oCn);
        //        cmd.ExecuteNonQuery();
        //        cmd = null;
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        //public static void CreateJobNote(Guid gJob, string sType, string sSubject, string sDetail, string sUser)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    sSql = "insert into JOB_NOTE ( NOTE_ID, JOB_ID, NOTE_TYPE, SUBJECT, DETAIL, USER_NAME, CREATED ) values ( NEWID(), " +
        //        "@JobId, @NoteType, @Subject, @Detail, @UserName, @Created ) ";
        //    sqlCmd = new SqlCommand(sSql, DBConn);
        //    sqlCmd.Parameters.AddWithValue("@JobId", gJob);
        //    sqlCmd.Parameters.AddWithValue("@NoteType", sType);
        //    sqlCmd.Parameters.AddWithValue("@Subject", sSubject);
        //    sqlCmd.Parameters.AddWithValue("@Detail", sDetail);
        //    sqlCmd.Parameters.AddWithValue("@UserName", sUser);
        //    sqlCmd.Parameters.AddWithValue("@Created", DateTime.Now);
        //    sqlCmd.ExecuteNonQuery();
        //}

        //WEBSERVICE REPLACE - This method is obsolete as now done via Web Service
        //public static bool LoginUser(string un, string pwd)
        //{
        //    string sSql;
        //    SqlCommand sqlCmd;
        //    SqlDataReader cReader;
        //    DataSet dataLogin;
        //    try
        //    {
        //        sSql = "select USER_NAME as UserName, FIRST_NAME as FirstName, SURNAME as Surname, " +
        //            "PREF_NAME as PrefName, EMAIL as Email, PHONE1 as Phone1, PHONE2 as Phone2, " +
        //            "SECURITY_LEVEL as SecurityLevel " +
        //            "from USERS where USER_NAME = @UN and PASSWORD = @PWD";
        //        sqlCmd = new SqlCommand(sSql, DBConn);
        //        sqlCmd = SqlHost.DBConn.CreateCommand();
        //        SqlDataAdapter da = new SqlDataAdapter();
        //        sqlCmd.CommandText = sSql;
        //        sqlCmd.Parameters.AddWithValue("@UN", un);
        //        sqlCmd.Parameters.AddWithValue("@PWD", pwd);
        //        da.SelectCommand = sqlCmd;
        //        dataLogin = new DataSet();
        //        da.Fill(dataLogin);
        //        if (dataLogin.Tables[0].Rows.Count > 0)
        //        {
        //            cUser = dataLogin.Tables[0].Rows[0];
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        public static DataRow GetUserProfile(string un, string pwd)
        {
            string sSql;
            SqlCommand sqlCmd;
            SqlDataReader cReader;
            DataSet dataLogin;
            try
            {
                sSql = "select USER_NAME as UserName, FIRST_NAME as FirstName, SURNAME as Surname, " +
                    "PREF_NAME as PrefName, EMAIL as Email, PHONE1 as Phone1, PHONE2 as Phone2, " +
                    "SECURITY_LEVEL as SecurityLevel " +
                    "from USERS where USER_NAME = @UN and PASSWORD = @PWD";
                sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                sqlCmd = SqlHost.DBConn.CreateCommand();
                SqlDataAdapter da = new SqlDataAdapter();
                sqlCmd.CommandText = sSql;
                sqlCmd.Parameters.AddWithValue("@UN", un);
                sqlCmd.Parameters.AddWithValue("@PWD", pwd);
                //sqlCmd.Parameters.AddWithValue("@UN", "chris");
                //sqlCmd.Parameters.AddWithValue("@PWD", "chris123");
                da.SelectCommand = sqlCmd;
                dataLogin = new DataSet();
                da.Fill(dataLogin);
                if (dataLogin.Tables[0].Rows.Count > 0)
                {
                    return dataLogin.Tables[0].Rows[0];
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //public static void ParseSQL(ref string sSql, out string sInto, out string sWhere)
        //{
        //    if (sSql.Contains(" into ") || sSql.Contains(" INTO ") || sSql.Contains(" Into "))
        //    {
        //        sInto = sSql.Substring(sSql.IndexOf(" into ", StringComparison.OrdinalIgnoreCase));
        //        sSql = sSql.Substring(0, sSql.Length - sInto.Length);
        //        sInto = sInto.Substring(5);
        //        if (sInto.Contains(" where ") || sInto.Contains(" WHERE ") || sInto.Contains(" Where "))
        //        {
        //            sWhere = sInto.Substring(sInto.IndexOf(" where ", StringComparison.OrdinalIgnoreCase));
        //            sInto = sInto.Substring(0, sInto.Length - sWhere.Length);
        //            sSql = sSql + sWhere;
        //        }
        //    }
        //    else
        //    {
        //        sInto = "";
        //    }
        //    if (sSql.Contains(" where ") || sSql.Contains(" WHERE ") || sSql.Contains(" Where "))
        //    {
        //        sWhere = sSql.Substring(sSql.IndexOf(" where ", StringComparison.OrdinalIgnoreCase));
        //        sSql = sSql.Substring(0, sSql.IndexOf(" where ", StringComparison.OrdinalIgnoreCase));
        //    }
        //    else
        //    {
        //        sWhere = "";
        //    }
        //}

        //public static void GetSQLParameters(ref string sWhere, out QueryParam[] queryParams)
        //{
        //    var listOfParams = new List<QueryParam>();
        //    while (sWhere.Contains("{"))
        //    {
        //        int nStart = sWhere.IndexOf("{");
        //        int nEnd = sWhere.IndexOf("}", nStart);
        //        string sRawParam = sWhere.Substring(nStart + 1, nEnd - (nStart + 1));
        //        string sBefore = sWhere.Substring(0, nStart);
        //        string sAfter = sWhere.Substring(nEnd + 1);
        //        //sRawParam = sRawParam.Substring(1, sRawParam.Length - 2);
        //        string[] sTokens = sRawParam.Split(';');
        //        QueryParam q = new QueryParam();
        //        foreach (string sToken in sTokens)
        //        {
        //            if (sToken.Length > 0)
        //            {
        //                string sKey = sToken.Trim().Substring(0, 1);
        //                string sValue = sToken.Trim().Substring(1);
        //                switch (sKey)
        //                {
        //                    case "@":
        //                        q.ParamName = sValue;
        //                        break;
        //                    case "|":
        //                        q.Prompt = sValue;
        //                        break;
        //                    case "[":
        //                        switch (sValue)
        //                        {
        //                            case "D":
        //                                q.DataType = typeof(DateTime);
        //                                break;
        //                            case "S":
        //                                q.DataType = typeof(string);
        //                                break;
        //                            case "N":
        //                                q.DataType = typeof(double);
        //                                break;
        //                            case "B":
        //                                q.DataType = typeof(bool);
        //                                break;
        //                            case "G":
        //                                q.DataType = typeof(Guid);
        //                                break;
        //                            default:
        //                                q.DataType = typeof(string);
        //                                break;
        //                        }
        //                        break;
        //                    case "!":
        //                        if (sValue == "1")
        //                        {
        //                            q.AllowNull = true;
        //                        }
        //                        else
        //                        {
        //                            q.AllowNull = false;
        //                        }
        //                        break;
        //                    case "=":
        //                        q.DefaultValue = sValue;
        //                        break;
        //                    default:
        //                        break;
        //                }
        //            }
        //        }
        //        sWhere = sBefore + "@" + q.ParamName + sAfter;
        //        listOfParams.Add(q);
        //    }
        //    queryParams = listOfParams.ToArray();
        //}

        //public static Guid CreateTransitionLookup(List<string> list)
        //{
        //    try
        //    {
        //        Guid g = Guid.NewGuid();
        //        SqlCommand sqlCmd;
        //        string sSql;
        //        sSql = "insert into TRANSITION_LOOKUP_STRING ( TRAN_ID, HEADER_ID, LOOKUP_VALUE  ) values ( NEWID(), @H_ID, @L_VAL ) ";
        //        foreach (string s in list)
        //        {
        //            sqlCmd = null;
        //            sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //            sqlCmd.Parameters.AddWithValue("@H_ID", g);
        //            sqlCmd.Parameters.AddWithValue("@L_VAL", s);

        //            sqlCmd.ExecuteNonQuery();
        //        }

        //        return g;
        //    }
        //    catch (Exception ex)
        //    {
        //        Guid g = new Guid();
        //        g = Guid.Empty;
        //        return g;
        //    }
        //}

        //public static bool DeleteTransitionLookup(Guid g)
        //{
        //    try
        //    {
        //        SqlCommand sqlCmd;
        //        string sSql;

        //        sSql = "delete from TRANSITION_LOOKUP_STRING where HEADER_ID = @H_ID ";
        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        sqlCmd.Parameters.AddWithValue("@H_ID", g);

        //        sqlCmd.ExecuteNonQuery();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}


    }
}

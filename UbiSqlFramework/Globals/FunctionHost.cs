﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.IO;
using UbiWeb.Data;
using UbiWeb.Services;
using System.Configuration;
using System.Net;

namespace UbiSqlFramework
{
    public static class FunctionHost
    {
        /*
         * This class does all of the Data Access work - SQL Commands Etc.
         * 
         */ 
         static FunctionHost()
        {

        }

        //public static string GetNextCBASqlServer(string sId)
        //{
        //    try
        //    {
        //        using (SqlCommand cmd = new SqlCommand("GetIncrementalCounter", SqlHost.DBConn))
        //        {
        //            cmd.CommandType = CommandType.StoredProcedure;

        //            SqlParameter parm1 = new SqlParameter("@CounterId", SqlDbType.VarChar);
        //            parm1.Value = sId;
        //            parm1.Direction = ParameterDirection.Input;
        //            cmd.Parameters.Add(parm1);

        //            SqlParameter parm2 = new SqlParameter("@NewCounterId", SqlDbType.VarChar);
        //            parm2.Size = 100;
        //            parm2.Direction = ParameterDirection.Output;
        //            cmd.Parameters.Add(parm2);

        //            cmd.ExecuteNonQuery();
        //            return parm2.Value.ToString();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}


        //public static DataTable SupplierSearch(string sSearch, string sParentId, string sCompanyId)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        DataTable dtSuppiers;

        //        string sWhere = "";

        //        sSql = "select " +
        //            "A.SUPPLIER_ID as SupplierId, A.NAME_1 as Name1, A.CONTACT as Contact, A.PARENT_SUPP as ParentId, B.NAME_1 as ParentName " +
        //            "from " +
        //            "SUPPLIER A " +
        //            "left outer join SUPPLIER B on A.PARENT_SUPP = B.SUPPLIER_ID " +
        //            "left outer join CONTEXT_SUPP_PROFILE C on A.SUPPLIER_ID = C.SYSTEM_SUPP_ID and C.CONTEXT_ID = '" + sCompanyId + "' ";
        //        sWhere = "";
        //        if (sSearch.Length > 0)
        //        {
        //            sWhere = "where ( A.SUPPLIER_ID like '%" + sSearch + "%' or A.NAME_1 like'%" + sSearch + "%' or A.NAME_2 like'%" + sSearch + "%' or A.PARENT_SUPP like'%" + sSearch + "%' or B.NAME_1 like '%" + sSearch + "%' ) ";
        //        }
        //        if (sParentId.Length > 0)
        //        {
        //            if (sWhere.Length == 0)
        //            {
        //                sWhere = "where ";
        //            }
        //            else
        //            {
        //                sWhere = sWhere + " AND ";
        //            }
        //            sWhere = sWhere + "( A.PARENT_SUPP = '" + sParentId + "' ) ";
        //        }
        //        sSql = sSql + sWhere;
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cDr = cSqlCmd.ExecuteReader();
        //        dtSuppiers = new DataTable();
        //        dtSuppiers.Load(cDr);
        //        cDr.Close();
        //        return dtSuppiers;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return null;
        //    }
        //}

        //public static Supplier GetSupplier(string sSupplierId, string sCompanyId)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        Supplier cSupp = new Supplier();

        //        sSql = "Select " +
        //                "A.SUPPLIER_ID As SupplierId, A.NAME_1 As Name1, A.NAME_2 As Name2, " +
        //                "A.DEF_CURRENCY As DefCurrency, A.COUNTRY_CODE As CountryCode, A.DEF_PORT As DefPort, " +
        //                "case when (B.DEF_PAY_TERMS is null or B.DEF_PAY_TERMS = '') then A.DEF_PAY_TERMS else B.DEF_PAY_TERMS end as DefPayTerms, " +
        //                "A.ACTIVE As Active, A.PARENT_SUPP As ParentSupp, " +
        //                "A.NOTES As Notes, A.CBA_TERMS As CbaTerms, A.CBA_SUPPTYPE As CbaSuppType, " +
        //                "A.LOCK_CODE As LockCode, A.EXTRA_CURRENCY As ExtraCurrency, " +
        //                "case when (B.DEF_PO_TYPE is null or B.DEF_PO_TYPE = '') then A.DEF_PO_TYPE else B.DEF_PO_TYPE end as DefPOType, " +
        //                "A.CONTACT As Contact, B.INVOICE_HEADER_URI as InvoiceHeaderURI, B.VENDOR_SHORTDESC as VendorShortDesc  " +
        //                "from SUPPLIER A " +
        //                "inner join CONTEXT_SUPP_PROFILE B on A.SUPPLIER_ID = B.SYSTEM_SUPP_ID and B.CONTEXT_ID = '" + sCompanyId + "' " +
        //                " WHERE A.SUPPLIER_ID = '" + sSupplierId + "' ";
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        //cSqlCmd.Parameters.AddWithValue("@SUPPID", sSupplierId);
        //        cDr = cSqlCmd.ExecuteReader();
        //        while (cDr.Read())
        //        {
        //            cSupp = new Supplier();
        //            cSupp.SupplierId = cDr["SupplierId"].ToString();
        //            cSupp.Name1 = cDr["Name1"].ToString();
        //            cSupp.Name2 = cDr["Name2"].ToString();
        //            cSupp.DefCurrency = cDr["DefCurrency"].ToString();
        //            cSupp.CountryCode = cDr["CountryCode"].ToString();
        //            cSupp.DefPort = cDr["DefPort"].ToString();
        //            cSupp.DefPayTerms = cDr["DefPayTerms"].ToString();
        //            if (cDr["Active"] != System.DBNull.Value)
        //            {
        //                cSupp.Active = (bool)cDr["Active"];
        //            }
        //            cSupp.ParentSupp = cDr["ParentSupp"].ToString();
        //            cSupp.Notes = cDr["Notes"].ToString();
        //            cSupp.CbaTerms = cDr["CbaTerms"].ToString();
        //            cSupp.CbaSuppType = cDr["CbaSuppType"].ToString();
        //            cSupp.LockCode = cDr["LockCode"].ToString();
        //            cSupp.ExtraCurrency = cDr["ExtraCurrency"].ToString();
        //            cSupp.DefPOType = cDr["DefPOType"].ToString();
        //            cSupp.Contact = cDr["Contact"].ToString();
        //            cSupp.InvoiceHeaderURI = cDr["InvoiceHeaderURI"].ToString(); 
        //            cSupp.VendorShortDesc = cDr["VendorShortDesc"].ToString(); 
        //        }
        //        cDr.Close();
        //        return cSupp;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return new Supplier();
        //    }

        //}
        //public static DataTable CustomerSearch(string sSearch, string sCompanyId)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        DataTable dtCustomers;

        //        string sWhere = "";

        //        sSql = "select " +
        //            "A.CUSTOMER_ID as CustomerId, A.NAME_1 as Name1, A.NAME_2 as Name2, A.BUSINESS_NAME as BusinessName " +
        //            "from " +
        //            "CUSTOMER A " +
        //            "left outer join CONTEXT_CUST_PROFILE B on A.CUSTOMER_ID = B.SYSTEM_CUST_ID and B.CONTEXT_ID = '" + sCompanyId + "' ";
        //        sWhere = "where A.ACTIVE = 1 ";

        //        if (sSearch.Length > 0)
        //        {
        //            sWhere = " and ( A.CUSTOMER_ID Like '%" + sSearch + "%' or A.NAME_1 like'%" + sSearch + "%' or A.NAME_2 like'%" + sSearch + "%' or A.BUSINESS_NAME like '%" + sSearch + "%' )";
        //        }
        //        sSql = sSql + sWhere;
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cDr = cSqlCmd.ExecuteReader();
        //        dtCustomers = new DataTable();
        //        dtCustomers.Load(cDr);
        //        cDr.Close();
        //        return dtCustomers;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return null;
        //    }
        //}

        //public static Customer GetCustomer(string sCustomerId, string sCompanyId)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        Single nTemp;
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        Customer cCust = new Customer();

        //        sSql = "Select " +
        //                "A.CUSTOMER_ID as CustomerId, A.NAME_1 as Name1, A.NAME_2 as Name2, A.BUSINESS_NAME as BusinessName, " +
        //                "CUSTOMER_TYPE as CustomerType, A.DEF_CURRENCY as DefCurrency, " +
        //                "case when (B.ORDER_TERMS is null or B.ORDER_TERMS = '') then A.ORDER_TERMS else B.ORDER_TERMS end as OrderTerms, " +
        //                "case when (B.PAYMENT_TERMS is null or B.PAYMENT_TERMS = '') then A.PAYMENT_TERMS else B.PAYMENT_TERMS end as PaymentTerms, " +
        //                "case when (B.DEPOSIT_REQ is null or B.DEPOSIT_REQ = '') then A.DEPOSIT_REQ else B.DEPOSIT_REQ end as DepositReq, " +
        //                "A.DEF_PORT as DefPort, PARENT_CUST as ParentCust, A.ACTIVE as Active, A.NOTES as Notes, A.PRIMARY_ADDRESS as PrimaryAddress, " +
        //                "PRIMARY_DEL_ADDR as PrimaryDelAddr, A.CBA_TERMS as CbaTerms, A.CBA_BRANCH as CbaBranch, " +
        //                "CBA_SALESMAN as CbaSalesman, A.CBA_PRICE_CODE as CbaPriceCode, A.CBA_INV_DISC as CbaInvDisc, " +
        //                "case when (B.QUOTE_CURRENCY is null or B.QUOTE_CURRENCY = '') then A.QUOTE_CURRENCY else B.QUOTE_CURRENCY end as QuoteCurrency, " +
        //                "case when (B.DEF_INVOICE_TYPE is null or B.DEF_INVOICE_TYPE = '') then A.DEF_INVOICE_TYPE else B.DEF_INVOICE_TYPE end as DefInvoiceType, " +
        //                "case when (B.DEF_PROFORMA_TYPE is null or B.DEF_PROFORMA_TYPE = '') then A.DEF_PROFORMA_TYPE else B.DEF_PROFORMA_TYPE end as DefProformaType, " +
        //                "A.RESTRICTIONS as Restrictions, " +
        //                "case when (B.ORDER_ALERT is null or B.ORDER_ALERT = '') then A.ORDER_ALERT else B.ORDER_ALERT end as OrderAlert, " +
        //                "A.SHIPPING_MARKS as ShippingMarks, A.PRIMARY_CONTACT_NO as PrimaryContactNo, A.LOCK_CODE as LockCode, A.NON_ACCOUNT as NonAccount, " +
        //                "case when (B.DISCOUNT_NOTE is null or B.DISCOUNT_NOTE = '') then A.DISCOUNT_NOTE else B.DISCOUNT_NOTE end as DiscountNote, " +
        //                "case when (B.DEF_QUOTE_REBATE is null or B.DEF_QUOTE_REBATE = '') then A.DEF_QUOTE_REBATE else B.DEF_QUOTE_REBATE end as DefQuoteRebate, " +
        //                "case when (B.DEF_QUOTE_DISCOUNT is null or B.DEF_QUOTE_DISCOUNT = '') then A.DEF_QUOTE_DISCOUNT else B.DEF_QUOTE_DISCOUNT end as DefQuoteDiscount, " +
        //                "case when (B.DEF_SLUSHIE is null or B.DEF_SLUSHIE = '') then A.DEF_SLUSHIE else B.DEF_SLUSHIE end as DefSlushie, " +
        //                "A.ENTITY_TYPE as EntityType, ABN_NO as AbnNo, A.PRODUCT_PROFILE_CUST as ProductProfileCust, A.TAX_NO as TaxNo, " +
        //                "B.VENDOR_NUMBER as VendorNumber, B.INVOICE_HEADER_URI as InvoiceHeaderUri, " +
        //                "A.VOLUME_CONV as VolumeConv, A.WEIGHT_CONV as WeightConv, B.VENDOR_SHORTDESC as VendorShortDesc " +
        //                "from CUSTOMER A " +
        //                "inner join CONTEXT_CUST_PROFILE B on A.CUSTOMER_ID = B.SYSTEM_CUST_ID and B.CONTEXT_ID = '" + sCompanyId + "' " +
        //                " WHERE A.CUSTOMER_ID = @CUSTID";
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@CUSTID", sCustomerId);
        //        cDr = cSqlCmd.ExecuteReader();
        //        while (cDr.Read())
        //        {
        //            cCust = new Customer();

        //            cCust.CustomerId = cDr["CustomerId"].ToString();
        //            cCust.Name1 = cDr["Name1"].ToString();
        //            cCust.Name2 = cDr["Name2"].ToString();
        //            cCust.BusinessName = cDr["BusinessName"].ToString();
        //            cCust.CustomerType = cDr["CustomerType"].ToString();
        //            cCust.DefCurrency = cDr["DefCurrency"].ToString();
        //            cCust.OrderTerms = cDr["OrderTerms"].ToString();
        //            cCust.PaymentTerms = cDr["PaymentTerms"].ToString();
        //            cCust.DepositReq = cDr["DepositReq"].ToString();
        //            cCust.DefPort = cDr["DefPort"].ToString();
        //            cCust.ParentCust = cDr["ParentCust"].ToString();
        //            if (cDr["Active"] != System.DBNull.Value)
        //            {
        //                cCust.Active = (bool)cDr["Active"];
        //            }
        //            cCust.Notes = cDr["Notes"].ToString();
        //            cCust.PrimaryAddress = cDr["PrimaryAddress"].ToString();
        //            cCust.PrimaryDelAddr = cDr["PrimaryDelAddr"].ToString();
        //            cCust.CbaTerms = cDr["CbaTerms"].ToString();
        //            cCust.CbaBranch = cDr["CbaBranch"].ToString();
        //            cCust.CbaSalesman = cDr["CbaSalesman"].ToString();
        //            cCust.CbaPriceCode = cDr["CbaPriceCode"].ToString();
        //            if (Single.TryParse(cDr["CbaInvDisc"].ToString(), out nTemp))
        //            {
        //                cCust.CbaInvDisc = nTemp;
        //            }
        //            else
        //            {
        //                cCust.CbaInvDisc = 0;
        //            }
        //            cCust.QuoteCurrency = cDr["QuoteCurrency"].ToString();
        //            cCust.DefInvoiceType = cDr["DefInvoiceType"].ToString();
        //            cCust.DefProformaType = cDr["DefProformaType"].ToString();
        //            cCust.Restrictions = cDr["Restrictions"].ToString();
        //            cCust.OrderAlert = cDr["OrderAlert"].ToString();
        //            cCust.ShippingMarks = cDr["ShippingMarks"].ToString();
        //            cCust.PrimaryContactNo = cDr["PrimaryContactNo"].ToString();
        //            cCust.LockCode = cDr["LockCode"].ToString();
        //            if (cDr["NonAccount"] != System.DBNull.Value)
        //            {
        //                cCust.NonAccount = (bool)cDr["NonAccount"];
        //            }
        //            cCust.DiscountNote = cDr["DiscountNote"].ToString();
        //            if (Single.TryParse(cDr["DefQuoteRebate"].ToString(), out nTemp))
        //            {
        //                cCust.DefQuoteRebate = nTemp;
        //            }
        //            else
        //            {
        //                cCust.DefQuoteRebate = 0;
        //            }
        //            if (Single.TryParse(cDr["DefQuoteDiscount"].ToString(), out nTemp))
        //            {
        //                cCust.DefQuoteDiscount = nTemp;
        //            }
        //            else
        //            {
        //                cCust.DefQuoteDiscount = 0;
        //            }
        //            if (Single.TryParse(cDr["DefSlushie"].ToString(), out nTemp))
        //            {
        //                cCust.DefSlushie = nTemp;
        //            }
        //            else
        //            {
        //                cCust.DefSlushie = 0;
        //            }
        //            cCust.EntityType = cDr["EntityType"].ToString();
        //            cCust.AbnNo = cDr["AbnNo"].ToString();
        //            cCust.ProductProfileCust = cDr["ProductProfileCust"].ToString();
        //            cCust.TaxNo = cDr["TaxNo"].ToString();
        //            cCust.VendorNumber = cDr["VendorNumber"].ToString();
        //            cCust.InvoiceHeaderURI = cDr["InvoiceHeaderUri"].ToString();
        //            cCust.VendorShortDesc = cDr["VendorShortDesc"].ToString();
        //            cCust.VolumeConv = cDr["VolumeConv"].ToString();
        //            cCust.WeightConv = cDr["WeightConv"].ToString();

        //        }
        //        cDr.Close();
        //        return cCust;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return new Customer();
        //    }

        //}
        //public static CustomerAddress GetCustomerAddress(string sCustomerId, string sAddressNo)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        CustomerAddress cAddr = new CustomerAddress();

        //        sSql = "Select " +
        //            "CUSTOMER_ID as CustomerId, ADDRESS_ID as AddressId, " +
        //            "ADDRESS_TYPE as AddressType, ADDR_1 as Addr1, ADDR_2 as Addr2, ADDR_3 as Addr3, " +
        //            "STATE as State, POST_CODE as PostCode, COUNTRY as Country, PHONE as Phone, FAX as Fax, " +
        //            "EMAIL_ADDRESS as EmailAddress, IS_DELIVERY as IsDelivery, IS_3PL as Is3Pl, NOTES as Notes, " +
        //            "ADDR_LABEL as AddrLabel " +
        //            "from CUSTOMER_ADDR " +
        //            " WHERE CUSTOMER_ID = @CUSTID and ADDRESS_ID = @ADDRNO";
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@CUSTID", sCustomerId);
        //        cSqlCmd.Parameters.AddWithValue("@ADDRNO", sAddressNo);
        //        cDr = cSqlCmd.ExecuteReader();
        //        while (cDr.Read())
        //        {
        //            cAddr = new CustomerAddress();

        //            cAddr.CustomerId = cDr["CustomerId"].ToString();
        //            cAddr.AddressId = cDr["AddressId"].ToString();
        //            cAddr.AddressType = cDr["AddressType"].ToString();
        //            cAddr.Addr1 = cDr["Addr1"].ToString();
        //            cAddr.Addr2 = cDr["Addr2"].ToString();
        //            cAddr.Addr3 = cDr["Addr3"].ToString();
        //            cAddr.State = cDr["State"].ToString();
        //            cAddr.PostCode = cDr["PostCode"].ToString();
        //            cAddr.Country = cDr["Country"].ToString();
        //            cAddr.Phone = cDr["Phone"].ToString();
        //            cAddr.Fax = cDr["Fax"].ToString();
        //            cAddr.EmailAddress = cDr["EmailAddress"].ToString();
        //            if (cDr["IsDelivery"] != System.DBNull.Value)
        //            {
        //                cAddr.IsDelivery = (bool)cDr["IsDelivery"];
        //            }
        //            if (cDr["Is3Pl"] != System.DBNull.Value)
        //            {
        //                cAddr.Is3Pl = (bool)cDr["Is3Pl"];
        //            }
        //            cAddr.Notes = cDr["Notes"].ToString();
        //            cAddr.AddressLabel = cDr["AddrLabel"].ToString();
        //        }
        //        cDr.Close();
        //        return cAddr;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return new CustomerAddress();
        //    }

        //}
        //public static CustomerContact GetCustomerContact(string sCustomerId, string sContactId)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        CustomerContact cContact = new CustomerContact();

        //        sSql = "Select " +
        //            "CUSTOMER_ID as CustomerId, CONTACT_ID as ContactId, " +
        //            "CONTACT_TYPE as ContactType, NAME_1 as Name1, NAME_2 as Name2, CONTACT_NUMBER_1 as ContactNumber1, " +
        //            "CONTACT_NUMBER_2 as ContactNumber2, CONTACT_NUMBER_3 as ContactNumber3, " +
        //            "EMAIL_ADDRESS as EmailAddress, NOTES as Notes " +
        //            "from CUSTOMER_CONTACT " +
        //            " WHERE CUSTOMER_ID = @CUSTID and CONTACT_ID = @CONTACTID";
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@CUSTID", sCustomerId);
        //        cSqlCmd.Parameters.AddWithValue("@CONTACTID", sContactId);
        //        cDr = cSqlCmd.ExecuteReader();
        //        while (cDr.Read())
        //        {
        //            cContact = new CustomerContact();

        //            cContact.CustomerId = cDr["CustomerId"].ToString();
        //            cContact.ContactId = cDr["ContactId"].ToString();
        //            cContact.ContactType = cDr["ContactType"].ToString();
        //            cContact.Name1 = cDr["Name1"].ToString();
        //            cContact.Name2 = cDr["Name2"].ToString();
        //            cContact.ContactNumber1 = cDr["ContactNumber1"].ToString();
        //            cContact.ContactNumber2 = cDr["ContactNumber2"].ToString();
        //            cContact.ContactNumber3 = cDr["ContactNumber3"].ToString();
        //            cContact.EmailAddress = cDr["EmailAddress"].ToString();
        //            cContact.Notes = cDr["Notes"].ToString();

        //        }
        //        cDr.Close();
        //        return cContact;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return new CustomerContact();
        //    }

        //}


        //public static CustomerOrderTerms GetCustomerOrderTerms(string sOrderTermsId)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        float nfTemp;
        //        Int32 nTemp;
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        CustomerOrderTerms cTerms = new CustomerOrderTerms();

        //        sSql = "Select " +
        //            "REF_ID as OrderTermsId, DESCRIPTION as Description, " + 
        //            "SHORT_DESC as InvoiceAbbreviation, DISPLAY_YN as DisplayYN, " +
        //            "EXT_STR_1 as ExtStr1, EXT_STR_2 as ExtStr2, EXT_STR_3 as ExtStr3, " +
        //            "EXT_NUM_1 as ExtNum1, EXT_NUM_2 as ExtNum2, EXT_DT_1 as ExtDate1, " +
        //            "EXT_DT_2 as ExtDate2, EXT_BOOL_1 as CompleteAfterMerging, " +
        //            "EXT_BOOL_2 as ExtBool2, EXTENDED_OPTIONS as ExtendedOptions " +
        //            "from REF_DATA where REF_TABLE_ID = 'CUSTTERMS' and REF_ID = @REFID";
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@REFID", sOrderTermsId);
        //        cDr = cSqlCmd.ExecuteReader();
        //        while (cDr.Read())
        //        {
        //            cTerms = new CustomerOrderTerms();

        //            cTerms.OrderTermsId = cDr["OrderTermsId"].ToString();
        //            cTerms.Description = cDr["Description"].ToString();
        //            cTerms.InvoiceAbbreviation = cDr["InvoiceAbbreviation"].ToString();
        //            //if (cDr["DisplayYN"] != System.DBNull.Value)
        //            //{
        //            //    cTerms.DisplayYN = (bool)cDr["DisplayYN"];
        //            //}
        //            if (Int32.TryParse(cDr["DisplayYN"].ToString(), out nTemp))
        //            {
        //                cTerms.DisplayYN = (nTemp != 0 ? true:false);
        //            }
        //            else
        //            {
        //                cTerms.DisplayYN = true;
        //            }
        //            cTerms.ExtStr1 = cDr["ExtStr1"].ToString();
        //            cTerms.ExtStr2 = cDr["ExtStr2"].ToString();
        //            cTerms.ExtStr3 = cDr["ExtStr3"].ToString();
        //            if (float.TryParse(cDr["ExtNum1"].ToString(), out nfTemp))
        //            {
        //                cTerms.ExtNum1 = nfTemp;
        //            }
        //            else
        //            {
        //                cTerms.ExtNum1 = 0;
        //            }
        //            if (float.TryParse(cDr["ExtNum2"].ToString(), out nfTemp))
        //            {
        //                cTerms.ExtNum2 = nfTemp;
        //            }
        //            else
        //            {
        //                cTerms.ExtNum2 = 0;
        //            }
        //            if (cDr["ExtDate1"] != System.DBNull.Value)
        //            {
        //                cTerms.ExtDate1 = (DateTime)cDr["ExtDate1"];
        //            }
        //            else
        //            {
        //                cTerms.ExtDate1 = new DateTime(1901, 1, 1);
        //            }
        //            if (cDr["ExtDate2"] != System.DBNull.Value)
        //            {
        //                cTerms.ExtDate2 = (DateTime)cDr["ExtDate2"];
        //            }
        //            else
        //            {
        //                cTerms.ExtDate2 = new DateTime(1901, 1, 1);
        //            }
        //            if (cDr["CompleteAfterMerging"] != System.DBNull.Value)
        //            {
        //                cTerms.CompleteAfterMerging = (bool)cDr["CompleteAfterMerging"];
        //            }
        //            if (cDr["ExtBool2"] != System.DBNull.Value)
        //            {
        //                cTerms.ExtBool2 = (bool)cDr["ExtBool2"];
        //            }
        //            if (Int32.TryParse(cDr["ExtendedOptions"].ToString(), out nTemp))
        //            {
        //                cTerms.ExtendedInvoiceOptions = nTemp;
        //            }
        //            else
        //            {
        //                cTerms.ExtendedInvoiceOptions = 0;
        //            }
        //        }
        //        cDr.Close();
        //        return cTerms;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return new CustomerOrderTerms();
        //    }

        //}


        //public static Stock GetStock(string stockCode)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        Single nTemp;
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        Stock cStock = new Stock();

        //        sSql = "Select " +
        //                "STOCK_CODE as StockCode, DESCRIPTION as Description, CLASS_LEVEL_ID as ClassLevelId, " + 
        //                "FIRST_PRICE as FirstPrice, FIRST_PRC_CURRENCY as FirstPrcCurrency, DEF_LOT as DefLot, " + 
        //                "DEF_WHS as DefWhs, STD_EST_LANDED_COST as StdEstLandedCost, COMPOSITION as Composition, " + 
        //                "NOTES as Notes, DEF_BARCODE as DefBarcode, ALT_BARCODE as AltBarcode, LOCK_CODE as LockCode, " + 
        //                "FUMIGATION_REQ as FumigationReq, SHIPPING_GROUP as ShippingGroup, DEF_BASE_SELL as DefBaseSell, " + 
        //                "DEF_DUTY_RATE as DefDutyRate, ORIGIN as Origin, BUYER_NAME as BuyerName, TAX_RATE as TaxRate, " + 
        //                "SOURCE_TYPE as SourceType, JOB_NUMBER as JobNumber, QIC as Qic, CE as Ce, CUSTOMS_CAT as CustomsCat, " + 
        //                "REGION as Region, FORECAST as Forecast, MARKETING_BLURB as MarketingBlurb " +
        //                "from STOCK " +
        //                " WHERE STOCK_CODE = @STOCKCODE";
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@STOCKCODE", stockCode);
        //        cDr = cSqlCmd.ExecuteReader();
        //        while (cDr.Read())
        //        {
        //            cStock = new Stock();

        //            cStock.StockCode = cDr["StockCode"].ToString();
        //            cStock.Description = cDr["Description"].ToString();
        //            cStock.ClassLevelId = cDr["ClassLevelId"].ToString();
        //            if (Single.TryParse(cDr["FirstPrice"].ToString(), out nTemp))
        //            {
        //                cStock.FirstPrice = nTemp;
        //            }
        //            else
        //            {
        //                cStock.FirstPrice = 0;
        //            }
        //            cStock.FirstPrcCurrency = cDr["FirstPrcCurrency"].ToString();
        //            cStock.DefLot = cDr["DefLot"].ToString();
        //            cStock.DefWhs = cDr["DefWhs"].ToString();
        //            if (Single.TryParse(cDr["StdEstLandedCost"].ToString(), out nTemp))
        //            {
        //                cStock.StdEstLandedCost = nTemp;
        //            }
        //            else
        //            {
        //                cStock.StdEstLandedCost = 0;
        //            }
        //            cStock.Composition = cDr["Composition"].ToString();
        //            cStock.Notes = cDr["Notes"].ToString();
        //            cStock.DefBarcode = cDr["DefBarcode"].ToString();
        //            cStock.AltBarcode = cDr["AltBarcode"].ToString();
        //            cStock.LockCode = cDr["LockCode"].ToString();
        //            if (cDr["FumigationReq"] != System.DBNull.Value)
        //            {
        //                cStock.FumigationReq = (bool)cDr["FumigationReq"];
        //            }
        //            cStock.ShippingGroup = cDr["ShippingGroup"].ToString();
        //            if (Single.TryParse(cDr["DefBaseSell"].ToString(), out nTemp))
        //            {
        //                cStock.DefBaseSell = nTemp;
        //            }
        //            else
        //            {
        //                cStock.DefBaseSell = 0;
        //            }
        //            if (Single.TryParse(cDr["DefDutyRate"].ToString(), out nTemp))
        //            {
        //                cStock.DefDutyRate = nTemp;
        //            }
        //            else
        //            {
        //                cStock.DefDutyRate = 0;
        //            }
        //            cStock.Origin = cDr["Origin"].ToString();
        //            cStock.BuyerName = cDr["BuyerName"].ToString();
        //            if (Single.TryParse(cDr["TaxRate"].ToString(), out nTemp))
        //            {
        //                cStock.TaxRate = nTemp;
        //            }
        //            else
        //            {
        //                cStock.TaxRate = 0;
        //            }
        //            cStock.SourceType = cDr["SourceType"].ToString();
        //            cStock.JobNumber = cDr["JobNumber"].ToString();
        //            if (cDr["Qic"] != System.DBNull.Value)
        //            {
        //                cStock.QIC = (bool)cDr["Qic"];
        //            }
        //            if (cDr["Ce"] != System.DBNull.Value)
        //            {
        //                cStock.CE = (bool)cDr["Ce"];
        //            }
        //            cStock.CustomsCat = cDr["CustomsCat"].ToString();
        //            cStock.Region = cDr["Region"].ToString();
        //            if (cDr["Forecast"] != System.DBNull.Value)
        //            {
        //                cStock.Forecast = (bool)cDr["Forecast"];
        //            }
        //            cStock.MarketingBlurb = cDr["MarketingBlurb"].ToString();

        //        }
        //        cDr.Close();
        //        return cStock;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return new Stock();
        //    }
        //}

        //public static StockLot GetStockLot(string stockCode, string lotId)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        Single nTemp;
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        StockLot cStockLot = new StockLot();

        //        sSql = "Select " +
        //                "STOCK_CODE as StockCode, SERIAL_LOT_NO as SerialLotNo, DESCRIPTION as Description, " +
        //                "DEF_WHSE as DefWhse, DEF_SUPPLIER_ID as DefSupplierId, DEF_SUPP_PARTNO as DefSuppPartno, " +
        //                "PACKAGING as Packaging, CARTON_QTY as CartonQty, INNER_QTY as InnerQty, UNIT as Unit, " +
        //                "FIRST_PRICE as FirstPrice, FIRST_PRC_CURRENCY as FirstPrcCurrency, DIMENSIONS as Dimensions, " +
        //                "WEIGHT_DESC as WeightDesc, COLOURS as Colours, SPECIAL_LABELLING as SpecialLabelling, " +
        //                "SUPP_INSTRUCTIONS as SuppInstructions, BARCODE_1 as Barcode1, BARCODE_2 as Barcode2, " +
        //                "BASE_SELL as BaseSell, VOLUME as Volume, RATING as Rating, MAX_CUST_ORDER_QTY as MaxCustOrderQty, " +
        //                "LEAD_TIME as LeadTime, MIN_SOH as MinSoh, MAX_SOH as MaxSoh, WEIGHT as Weight, WIDTH as Width, " +
        //                "HEIGHT as Height, DEPTH as Depth, NOTES as Notes, MAN_DESC as ManDesc, SPARE_DESC as SpareDesc, " +
        //                "EXPIRY_DATE as ExpiryDate, EXCLUSIVE_CUST_ID as ExclusiveCustId, MIN_SUPP_ORD_QTY as MinSuppOrdQty, " +
        //                "WEEKS_OF_COVER as WeeksOfCover, PRE_PRICE as PrePrice, LOCK_CODE as LockCode, SIZE_RANGE as SizeRange, " +
        //                "SIZE_MATRIX as SizeMatrix, WARRANTY as Warranty, ACCESSORY_MATRIX as AccessoryMatrix, " +
        //                "LOADABILITY_20GP as Loadability20Gp, LOADABILITY_40GP as Loadability40Gp, " +
        //                "LOADABILITY_40HQ as Loadability40Hq, INTERNAL_NOTES as InternalNotes, PALLET_QTY as PalletQty, " +
        //                "TIHI_DESC as TihiDesc, BRANDING as Branding, NET_WEIGHT_DESC as NetWeightDesc, " +
        //                "NET_WEIGHT as NetWeight, GENDER_CODE as GenderCode, COMPLIANCE_REQUIREMENTS as ComplianceRequirements, " +
        //                "FACTORY_NAME as FactoryName, ROYALTY_RATE as RoyaltyRate, SUPP_ALT_PRC as SuppAltPrc, " +
        //                "SUPP_ALT_PRC_CURRENCY as SuppAltPrcCurrency, CARTON_DIMENSIONS as CartonDimensions " +
        //                "from STOCK_LOT " +
        //                " WHERE STOCK_CODE = @STOCKCODE and SERIAL_LOT_NO = @LOTID";
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@STOCKCODE", stockCode);
        //        cSqlCmd.Parameters.AddWithValue("@LOTID", lotId);
        //        cDr = cSqlCmd.ExecuteReader();
        //        while (cDr.Read())
        //        {
        //            cStockLot = new StockLot();

        //            cStockLot.StockCode = cDr["StockCode"].ToString();
        //            cStockLot.SerialLotNo = cDr["SerialLotNo"].ToString();
        //            cStockLot.Description = cDr["Description"].ToString();
        //            cStockLot.DefWhse = cDr["DefWhse"].ToString();
        //            cStockLot.DefSupplierId = cDr["DefSupplierId"].ToString();
        //            cStockLot.DefSuppPartno = cDr["DefSuppPartno"].ToString();
        //            cStockLot.Packaging = cDr["Packaging"].ToString();
        //            if (Single.TryParse(cDr["CartonQty"].ToString(), out nTemp))
        //            {
        //                cStockLot.CartonQty = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.CartonQty = 0;
        //            }
        //            if (Single.TryParse(cDr["InnerQty"].ToString(), out nTemp))
        //            {
        //                cStockLot.InnerQty = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.InnerQty = 0;
        //            }
        //            cStockLot.Unit = cDr["Unit"].ToString();
        //            if (Single.TryParse(cDr["FirstPrice"].ToString(), out nTemp))
        //            {
        //                cStockLot.FirstPrice = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.FirstPrice = 0;
        //            }
        //            cStockLot.FirstPrcCurrency = cDr["FirstPrcCurrency"].ToString();
        //            cStockLot.Dimensions = cDr["Dimensions"].ToString();
        //            cStockLot.WeightDesc = cDr["WeightDesc"].ToString();
        //            cStockLot.Colours = cDr["Colours"].ToString();
        //            cStockLot.SpecialLabelling = cDr["SpecialLabelling"].ToString();
        //            cStockLot.SuppInstructions = cDr["SuppInstructions"].ToString();
        //            cStockLot.Barcode1 = cDr["Barcode1"].ToString();
        //            cStockLot.Barcode2 = cDr["Barcode2"].ToString();
        //            if (Single.TryParse(cDr["BaseSell"].ToString(), out nTemp))
        //            {
        //                cStockLot.BaseSell = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.BaseSell = 0;
        //            }
        //            if (Single.TryParse(cDr["Volume"].ToString(), out nTemp))
        //            {
        //                cStockLot.Volume = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.Volume = 0;
        //            }
        //            cStockLot.Rating = cDr["Rating"].ToString();
        //            if (Single.TryParse(cDr["MaxCustOrderQty"].ToString(), out nTemp))
        //            {
        //                cStockLot.MaxCustOrderQty = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.MaxCustOrderQty = 0;
        //            }
        //            if (Single.TryParse(cDr["LeadTime"].ToString(), out nTemp))
        //            {
        //                cStockLot.LeadTime = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.LeadTime = 0;
        //            }
        //            if (Single.TryParse(cDr["MinSoh"].ToString(), out nTemp))
        //            {
        //                cStockLot.MinSoh = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.MinSoh = 0;
        //            }
        //            if (Single.TryParse(cDr["MaxSoh"].ToString(), out nTemp))
        //            {
        //                cStockLot.MaxSoh = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.MaxSoh = 0;
        //            }
        //            if (Single.TryParse(cDr["Weight"].ToString(), out nTemp))
        //            {
        //                cStockLot.Weight = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.Weight = 0;
        //            }
        //            if (Single.TryParse(cDr["Width"].ToString(), out nTemp))
        //            {
        //                cStockLot.Width = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.Width = 0;
        //            }
        //            if (Single.TryParse(cDr["Height"].ToString(), out nTemp))
        //            {
        //                cStockLot.Height = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.Height = 0;
        //            }
        //            if (Single.TryParse(cDr["Depth"].ToString(), out nTemp))
        //            {
        //                cStockLot.Depth = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.Depth = 0;
        //            }
        //            cStockLot.Notes = cDr["Notes"].ToString();
        //            cStockLot.ManDesc = cDr["ManDesc"].ToString();
        //            cStockLot.SpareDesc = cDr["SpareDesc"].ToString();
        //            if (cDr["ExpiryDate"] != System.DBNull.Value)
        //            {
        //                cStockLot.ExpiryDate = (DateTime)cDr["ExpiryDate"];
        //            }
        //            else
        //            {
        //                cStockLot.ExpiryDate = new DateTime(1901, 1, 1);
        //            }
        //            cStockLot.ExclusiveCustId = cDr["ExclusiveCustId"].ToString();
        //            if (Single.TryParse(cDr["MinSuppOrdQty"].ToString(), out nTemp))
        //            {
        //                cStockLot.MinSuppOrdQty = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.MinSuppOrdQty = 0;
        //            }
        //            if (Single.TryParse(cDr["WeeksOfCover"].ToString(), out nTemp))
        //            {
        //                cStockLot.WeeksOfCover = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.WeeksOfCover = 0;
        //            }
        //            if (Single.TryParse(cDr["PrePrice"].ToString(), out nTemp))
        //            {
        //                cStockLot.PrePrice = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.PrePrice = 0;
        //            }
        //            cStockLot.LockCode = cDr["LockCode"].ToString();
        //            cStockLot.SizeRange = cDr["SizeRange"].ToString();
        //            cStockLot.SizeMatrix = cDr["SizeMatrix"].ToString();
        //            cStockLot.Warranty = cDr["Warranty"].ToString();
        //            cStockLot.AccessoryMatrix = cDr["AccessoryMatrix"].ToString();
        //            if (Single.TryParse(cDr["Loadability20Gp"].ToString(), out nTemp))
        //            {
        //                cStockLot.Loadability20GP = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.Loadability20GP = 0;
        //            }
        //            if (Single.TryParse(cDr["Loadability40Gp"].ToString(), out nTemp))
        //            {
        //                cStockLot.Loadability40GP = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.Loadability40GP = 0;
        //            }
        //            if (Single.TryParse(cDr["Loadability40Hq"].ToString(), out nTemp))
        //            {
        //                cStockLot.Loadability40HQ = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.Loadability40HQ = 0;
        //            }
        //            cStockLot.InternalNotes = cDr["InternalNotes"].ToString();
        //            if (Single.TryParse(cDr["PalletQty"].ToString(), out nTemp))
        //            {
        //                cStockLot.PalletQty = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.PalletQty = 0;
        //            }
        //            cStockLot.TihiDesc = cDr["TihiDesc"].ToString();
        //            cStockLot.Branding = cDr["Branding"].ToString();
        //            cStockLot.NetWeightDesc = cDr["NetWeightDesc"].ToString();
        //            if (Single.TryParse(cDr["NetWeight"].ToString(), out nTemp))
        //            {
        //                cStockLot.NetWeight = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.NetWeight = 0;
        //            }
        //            cStockLot.GenderCode = cDr["GenderCode"].ToString();
        //            cStockLot.ComplianceRequirements = cDr["ComplianceRequirements"].ToString();
        //            cStockLot.FactoryName = cDr["FactoryName"].ToString();
        //            if (Single.TryParse(cDr["RoyaltyRate"].ToString(), out nTemp))
        //            {
        //                cStockLot.RoyaltyRate = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.RoyaltyRate = 0;
        //            }
        //            if (Single.TryParse(cDr["SuppAltPrc"].ToString(), out nTemp))
        //            {
        //                cStockLot.SuppAltPrc = nTemp;
        //            }
        //            else
        //            {
        //                cStockLot.SuppAltPrc = 0;
        //            }
        //            cStockLot.SuppAltPrcCurrency = cDr["SuppAltPrcCurrency"].ToString();
        //            cStockLot.CartonDimensions = cDr["CartonDimensions"].ToString();


        //        }
        //        cDr.Close();
        //        return cStockLot;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return new StockLot();
        //    }
        //}

        //public static StockWhs GetStockWhs(string stockCode, string whsId)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        Single nTemp;
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        StockWhs cStockWhs = new StockWhs();

        //        sSql = "Select " +
        //            "STOCK_CODE as StockCode, WAREHOUSE_ID as WarehouseId, DEF_LOT as DefLot, " +
        //            "DUTY_RATE as DutyRate, GST_EXEMPT as GstExempt, RESTRICTIONS as Restrictions, " +
        //            "LOCK_CODE as LockCode " +
        //            "from STOCK_WHS " +
        //            " WHERE STOCK_CODE = @STOCKCODE and WAREHOUSE_ID = @WHSID";
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@STOCKCODE", stockCode);
        //        cSqlCmd.Parameters.AddWithValue("@WHSID", whsId);
        //        cDr = cSqlCmd.ExecuteReader();
        //        while (cDr.Read())
        //        {
        //            cStockWhs = new StockWhs();
        //            cStockWhs.StockCode = cDr["StockCode"].ToString();
        //            cStockWhs.WarehouseId = cDr["WarehouseId"].ToString();
        //            cStockWhs.DefLot = cDr["DefLot"].ToString();
        //            if (Single.TryParse(cDr["DutyRate"].ToString(), out nTemp))
        //            {
        //                cStockWhs.DutyRate = nTemp;
        //            }
        //            else
        //            {
        //                cStockWhs.DutyRate = 0;
        //            }
        //            if (cDr["GstExempt"] != System.DBNull.Value)
        //            {
        //                cStockWhs.GSTExempt = (bool)cDr["GstExempt"];
        //            }
        //            cStockWhs.Restrictions = cDr["Restrictions"].ToString();
        //            cStockWhs.LockCode = cDr["LockCode"].ToString();
        //        }
        //        cDr.Close();
        //        return cStockWhs;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return new StockWhs();
        //    }
        //}
        //public static StockLotWhs GetStockLotWhs(string stockCode, string lotId, string whsId)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        StockLotWhs cStockLotWhs = new StockLotWhs();

        //        sSql = "Select " +
        //            "STOCK_CODE as StockCode, LOT_ID as LotId, WAREHOUSE_ID as WarehouseId, " +
        //            "LOCATION as Location, LOCK_CODE as LockCode " +
        //            "from STOCK_LOT_WHS " +
        //            " WHERE STOCK_CODE = @STOCKCODE and LOT_ID = @LOTID and WAREHOUSE_ID = @WHSID";
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@STOCKCODE", stockCode);
        //        cSqlCmd.Parameters.AddWithValue("@LOTID", lotId);
        //        cSqlCmd.Parameters.AddWithValue("@WHSID", whsId);
        //        cDr = cSqlCmd.ExecuteReader();
        //        while (cDr.Read())
        //        {
        //            cStockLotWhs = new StockLotWhs();
        //            cStockLotWhs.StockCode = cDr["StockCode"].ToString();
        //            cStockLotWhs.LotId = cDr["LotId"].ToString();
        //            cStockLotWhs.WarehouseId = cDr["WarehouseId"].ToString();
        //            cStockLotWhs.Location = cDr["Location"].ToString();
        //            cStockLotWhs.LockCode = cDr["LockCode"].ToString();

        //        }
        //        cDr.Close();
        //        return cStockLotWhs;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return new StockLotWhs();
        //    }
        //}

        //public static bool POExists(string poNum)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        string sPO;

        //        sSql = "Select " +
        //            "PO_NUM as PoNum " +
        //            "from PO_HEADER " +
        //            " WHERE PO_NUM = @PONUM";
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@PONUM", poNum);
        //        cDr = cSqlCmd.ExecuteReader();
        //        while (cDr.Read())
        //        {
        //            sPO = cDr["PoNum"].ToString();
        //            if (sPO == poNum)
        //            {
        //                cDr.Close();
        //                return true;
        //            }
        //        }
        //        cDr.Close();
        //        return false;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return false;
        //    }
        //}
        //public static POHeader RetrievePO(string poNum, bool byRef, bool headerOnly)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        Single nTemp;
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        POHeader cPO = new POHeader();
        //        POItem cItem = new POItem();

        //        sSql = "Select " +
        //                "PO_NUM as PoNum, PO_DATE as PoDate, SUPPLIER_ID as SupplierId, SUPP_NAME as SuppName, " +
        //                "SUPP_CONTACT_NAME as SuppContactName, PORT_OF_ORIGIN as PortOfOrigin, " +
        //                "DESTINATION_PORT as DestinationPort, DEL_INSTRUCTION_1 as DelInstruction1, " +
        //                "DEL_INSTRUCTION_2 as DelInstruction2, CURRENCY as Currency, EXCHANGE_RATE as ExchangeRate, " +
        //                "REQ_DATE as ReqDate, EXP_DATE_1 as ExpDate1, EXP_DATE_2 as ExpDate2, " +
        //                "FIRST_SHIP_DATE as FirstShipDate, DEPOSIT_STATUS as DepositStatus, SUPP_PO_TYPE as SuppPoType, " +
        //                "ADDITIONAL_REFERENCE as AdditionalReference, FCL as Fcl, ORIGIN_PO_NUM as OriginPoNum, " +
        //                "OE_LINK_NO as OeLinkNo, WAREHOUSE_ID as WarehouseId, CBA_SUPP_TERMS as CbaSuppTerms, " +
        //                "COMMISSION_RATE as CommissionRate, COMMISSION_AMOUNT as CommissionAmount, " +
        //                "COMMISSION_AGENT as CommissionAgent, COSTINGS_AMT as CostingsAmt, COSTINGS_APPLD as CostingsAppld, " +
        //                "ADDITIONAL_CHARGES_AMT as AdditionalChargesAmt, ADDITIONAL_CHARGES_APPLD as AdditionalChargesAppld, " +
        //                "DELETE_FLAG as DeleteFlag, ORDER_VALUE as OrderValue, SHIPPED_VALUE as ShippedValue, " +
        //                "RECD_VALUE as RecdValue, EXTRA_CURRENCY as ExtraCurrency, FUMIGATION as Fumigation, " +
        //                "TOTAL_CARTONS as TotalCartons, TOTAL_VOLUME_MTR as TotalVolumeMtr, TOTAL_WEIGHT as TotalWeight, " +
        //                "EXTRA_EXCHANGE_RATE as ExtraExchangeRate, ORDER_STATUS as OrderStatus, FINAL_SHIP_DATE as FinalShipDate, " +
        //                "SPECIAL_INSTRUCTIONS as SpecialInstructions, NOTES as Notes, RAISED_BY as RaisedBy, " +
        //                "CANCELLED_VALUE as CancelledValue, SHIPPING_LINE as ShippingLine, SHIPPING_CONTRACT as ShippingContract, " +
        //                "GOODS_DESC as GoodsDesc, BOSS as Boss, CONTEXT as Context " +
        //                "from PO_HEADER ";
        //        if (byRef)
        //        {
        //            sSql = sSql + "WHERE ADDITIONAL_REFERENCE = @PONUM";
        //        }
        //        else
        //        {
        //            sSql = sSql + "WHERE PO_NUM = @PONUM";
        //        }
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@PONUM", poNum);
        //        cDr = cSqlCmd.ExecuteReader();
        //        while (cDr.Read())
        //        {
        //            cPO = new POHeader();

        //            cPO.PoNum = cDr["PoNum"].ToString();
        //            if (cDr["PoDate"] != System.DBNull.Value)
        //            {
        //                cPO.PoDate = (DateTime)cDr["PoDate"];
        //            }
        //            else
        //            {
        //                cPO.PoDate = new DateTime(1901, 1, 1);
        //            }
        //            cPO.SupplierId = cDr["SupplierId"].ToString();
        //            cPO.SuppName = cDr["SuppName"].ToString();
        //            cPO.SuppContactName = cDr["SuppContactName"].ToString();
        //            cPO.PortOfOrigin = cDr["PortOfOrigin"].ToString();
        //            cPO.DestinationPort = cDr["DestinationPort"].ToString();
        //            cPO.DelInstruction1 = cDr["DelInstruction1"].ToString();
        //            cPO.DelInstruction2 = cDr["DelInstruction2"].ToString();
        //            cPO.Currency = cDr["Currency"].ToString();
        //            if (Single.TryParse(cDr["ExchangeRate"].ToString(), out nTemp))
        //            {
        //                cPO.ExchangeRate = nTemp;
        //            }
        //            else
        //            {
        //                cPO.ExchangeRate = 0;
        //            }
        //            if (cDr["ReqDate"] != System.DBNull.Value)
        //            {
        //                cPO.ReqDate = (DateTime)cDr["ReqDate"];
        //            }
        //            else
        //            {
        //                cPO.ReqDate = new DateTime(1901, 1, 1);
        //            }
        //            if (cDr["ExpDate1"] != System.DBNull.Value)
        //            {
        //                cPO.ExpDate1 = (DateTime)cDr["ExpDate1"];
        //            }
        //            else
        //            {
        //                cPO.ExpDate1 = new DateTime(1901, 1, 1);
        //            }
        //            if (cDr["ExpDate2"] != System.DBNull.Value)
        //            {
        //                cPO.ExpDate2 = (DateTime)cDr["ExpDate2"];
        //            }
        //            else
        //            {
        //                cPO.ExpDate2 = new DateTime(1901, 1, 1);
        //            }
        //            if (cDr["FirstShipDate"] != System.DBNull.Value)
        //            {
        //                cPO.FirstShipDate = (DateTime)cDr["FirstShipDate"];
        //            }
        //            else
        //            {
        //                cPO.FirstShipDate = new DateTime(1901, 1, 1);
        //            }
        //            cPO.DepositStatus = cDr["DepositStatus"].ToString();
        //            cPO.SuppPoType = cDr["SuppPoType"].ToString();
        //            cPO.AdditionalReference = cDr["AdditionalReference"].ToString();
        //            cPO.Fcl = cDr["Fcl"].ToString();
        //            cPO.OriginPoNum = cDr["OriginPoNum"].ToString();
        //            cPO.OeLinkNo = cDr["OeLinkNo"].ToString();
        //            cPO.WarehouseId = cDr["WarehouseId"].ToString();
        //            cPO.CbaSuppTerms = cDr["CbaSuppTerms"].ToString();
        //            if (Single.TryParse(cDr["CommissionRate"].ToString(), out nTemp))
        //            {
        //                cPO.CommissionRate = nTemp;
        //            }
        //            else
        //            {
        //                cPO.CommissionRate = 0;
        //            }
        //            if (Single.TryParse(cDr["CommissionAmount"].ToString(), out nTemp))
        //            {
        //                cPO.CommissionAmount = nTemp;
        //            }
        //            else
        //            {
        //                cPO.CommissionAmount = 0;
        //            }
        //            cPO.CommissionAgent = cDr["CommissionAgent"].ToString();
        //            if (Single.TryParse(cDr["CostingsAmt"].ToString(), out nTemp))
        //            {
        //                cPO.CostingsAmt = nTemp;
        //            }
        //            else
        //            {
        //                cPO.CostingsAmt = 0;
        //            }
        //            if (cDr["CostingsAppld"] != System.DBNull.Value)
        //            {
        //                cPO.CostingsAppld = (bool)cDr["CostingsAppld"];
        //            }
        //            if (Single.TryParse(cDr["AdditionalChargesAmt"].ToString(), out nTemp))
        //            {
        //                cPO.AdditionalChargesAmt = nTemp;
        //            }
        //            else
        //            {
        //                cPO.AdditionalChargesAmt = 0;
        //            }
        //            if (cDr["AdditionalChargesAppld"] != System.DBNull.Value)
        //            {
        //                cPO.AdditionalChargesAppld = (bool)cDr["AdditionalChargesAppld"];
        //            }
        //            if (cDr["DeleteFlag"] != System.DBNull.Value)
        //            {
        //                cPO.DeleteFlag = (bool)cDr["DeleteFlag"];
        //            }
        //            if (Single.TryParse(cDr["OrderValue"].ToString(), out nTemp))
        //            {
        //                cPO.OrderValue = nTemp;
        //            }
        //            else
        //            {
        //                cPO.OrderValue = 0;
        //            }
        //            if (Single.TryParse(cDr["ShippedValue"].ToString(), out nTemp))
        //            {
        //                cPO.ShippedValue = nTemp;
        //            }
        //            else
        //            {
        //                cPO.ShippedValue = 0;
        //            }
        //            if (Single.TryParse(cDr["RecdValue"].ToString(), out nTemp))
        //            {
        //                cPO.RecdValue = nTemp;
        //            }
        //            else
        //            {
        //                cPO.RecdValue = 0;
        //            }
        //            cPO.ExtraCurrency = cDr["ExtraCurrency"].ToString();
        //            if (cDr["Fumigation"] != System.DBNull.Value)
        //            {
        //                cPO.Fumigation = (bool)cDr["Fumigation"];
        //            }
        //            if (Single.TryParse(cDr["TotalCartons"].ToString(), out nTemp))
        //            {
        //                cPO.TotalCartons = nTemp;
        //            }
        //            else
        //            {
        //                cPO.TotalCartons = 0;
        //            }
        //            if (Single.TryParse(cDr["TotalVolumeMtr"].ToString(), out nTemp))
        //            {
        //                cPO.TotalVolumeMtr = nTemp;
        //            }
        //            else
        //            {
        //                cPO.TotalVolumeMtr = 0;
        //            }
        //            if (Single.TryParse(cDr["TotalWeight"].ToString(), out nTemp))
        //            {
        //                cPO.TotalWeight = nTemp;
        //            }
        //            else
        //            {
        //                cPO.TotalWeight = 0;
        //            }
        //            if (Single.TryParse(cDr["ExtraExchangeRate"].ToString(), out nTemp))
        //            {
        //                cPO.ExtraExchangeRate = nTemp;
        //            }
        //            else
        //            {
        //                cPO.ExtraExchangeRate = 0;
        //            }
        //            cPO.OrderStatus = cDr["OrderStatus"].ToString();
        //            if (cDr["FinalShipDate"] != System.DBNull.Value)
        //            {
        //                cPO.FinalShipDate = (DateTime)cDr["FinalShipDate"];
        //            }
        //            else
        //            {
        //                cPO.FinalShipDate = new DateTime(1901, 1, 1);
        //            }
        //            cPO.SpecialInstructions = cDr["SpecialInstructions"].ToString();
        //            cPO.Notes = cDr["Notes"].ToString();
        //            cPO.RaisedBy = cDr["RaisedBy"].ToString();
        //            if (Single.TryParse(cDr["CancelledValue"].ToString(), out nTemp))
        //            {
        //                cPO.CancelledValue = nTemp;
        //            }
        //            else
        //            {
        //                cPO.CancelledValue = 0;
        //            }
        //            cPO.ShippingLine = cDr["ShippingLine"].ToString();
        //            cPO.ShippingContract = cDr["ShippingContract"].ToString();
        //            cPO.GoodsDesc = cDr["GoodsDesc"].ToString();
        //            cPO.Boss = cDr["Boss"].ToString();
        //            cPO.Context = cDr["Context"].ToString();
        //        }
        //        cDr.Close();
        //        if (headerOnly)
        //        {
        //            return cPO;
        //        }
        //        if (RetrievePOItems(poNum, byRef, ref cPO))
        //        {
        //            return cPO;
        //        }
        //        else
        //        {
        //            return new POHeader();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return new POHeader();
        //    }
        //}
        //private static bool RetrievePOItems(string poNum, bool byRef, ref POHeader cPO)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        Single nTemp;
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        DataTable dtItems;
        //        POItem cItem = new POItem();

        //        sSql = "Select " +
        //            "A.PO_NUM as PoNum, A.SEQUENCE as Sequence, A.STOCK_CODE as StockCode, A.LOT_ID as LotId, " +
        //            "A.WAREHOUSE_ID as WarehouseId, A.PRICE as Price, A.TAX_RATE as TaxRate, A.CARTON_QTY as CartonQty, " +
        //            "A.INNER_QTY as InnerQty, A.CUBIC_MTR as CubicMtr, A.BARCODE as Barcode, " +
        //            "A.FUMIGATION_REQ as FumigationReq, A.SUPPLIER_PART_NO as SupplierPartNo, " +
        //            "A.CARTON_WEIGHT as CartonWeight, A.QTY_ORDERED as QtyOrdered, A.QTY_SHIPPED as QtyShipped, " +
        //            "A.QTY_RECD as QtyRecd, A.QTY_CANCEL as QtyCancel, A.VALUE_ORDERED as ValueOrdered, " +
        //            "A.VALUE_SHIPPED as ValueShipped, A.VALUE_RECD as ValueRecd, A.VALUE_CANCEL as ValueCancel, " +
        //            "A.ORIGIN_PO_NUM as OriginPoNum, A.ORIGIN_PO_SEQUENCE as OriginPoSequence, " +
        //            "A.OE_LINK_ORDER_NUM as OeLinkOrderNum, A.OE_LINK_ORDER_SEQUENCE as OeLinkOrderSequence, " +
        //            "A.SPECIAL_INSTRUCTIONS as SpecialInstructions, A.NOTES as Notes, A.STATUS as Status, " +
        //            "A.ROYALTY_RATE as RoyaltyRate, A.ROYALTY_AMOUNT as RoyaltyAmount, A.COMMISSION_RATE as CommissionRate, " +
        //            "A.COMMISSION_AMOUNT as CommissionAmount, A.SURCHARGE_RATE as SurchargeRate, " +
        //            "A.SURCHARGE_AMOUNT as SurchargeAmount, A.SURCHARGE_REASON as SurchargeReason, A.TAX_AMOUNT as TaxAmount " +
        //            "from PO_LINE A inner join PO_HEADER B on A.PO_NUM = B.PO_NUM ";
        //        if (byRef)
        //        {
        //            sSql = sSql + "WHERE B.ADDITIONAL_REFERENCE = @PONUM";
        //        }
        //        else
        //        {
        //            sSql = sSql + "WHERE A.PO_NUM = @PONUM";
        //        }
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@PONUM", poNum);
        //        cDr = cSqlCmd.ExecuteReader();
        //        dtItems = new DataTable();
        //        dtItems.Load(cDr);

        //        foreach (DataRow cRow in dtItems.Rows)
        //        {
        //            cItem = new POItem();
        //            cItem.PoNum = cRow["PoNum"].ToString();
        //            if (Single.TryParse(cRow["Sequence"].ToString(), out nTemp))
        //            {
        //                cItem.Sequence = nTemp;
        //            }
        //            else
        //            {
        //                cItem.Sequence = 0;
        //            }
        //            cItem.StockCode = cRow["StockCode"].ToString();
        //            cItem.LotId = cRow["LotId"].ToString();
        //            cItem.WarehouseId = cRow["WarehouseId"].ToString();
        //            if (Single.TryParse(cRow["Price"].ToString(), out nTemp))
        //            {
        //                cItem.Price = nTemp;
        //            }
        //            else
        //            {
        //                cItem.Price = 0;
        //            }
        //            if (Single.TryParse(cRow["TaxRate"].ToString(), out nTemp))
        //            {
        //                cItem.TaxRate = nTemp;
        //            }
        //            else
        //            {
        //                cItem.TaxRate = 0;
        //            }
        //            if (Single.TryParse(cRow["CartonQty"].ToString(), out nTemp))
        //            {
        //                cItem.CartonQty = nTemp;
        //            }
        //            else
        //            {
        //                cItem.CartonQty = 0;
        //            }
        //            if (Single.TryParse(cRow["InnerQty"].ToString(), out nTemp))
        //            {
        //                cItem.InnerQty = nTemp;
        //            }
        //            else
        //            {
        //                cItem.InnerQty = 0;
        //            }
        //            if (Single.TryParse(cRow["CubicMtr"].ToString(), out nTemp))
        //            {
        //                cItem.CubicMtr = nTemp;
        //            }
        //            else
        //            {
        //                cItem.CubicMtr = 0;
        //            }
        //            cItem.Barcode = cRow["Barcode"].ToString();
        //            if (cRow["FumigationReq"] != System.DBNull.Value)
        //            {
        //                cItem.FumigationReq = (bool)cRow["FumigationReq"];
        //            }
        //            cItem.SupplierPartNo = cRow["SupplierPartNo"].ToString();
        //            if (Single.TryParse(cRow["CartonWeight"].ToString(), out nTemp))
        //            {
        //                cItem.CartonWeight = nTemp;
        //            }
        //            else
        //            {
        //                cItem.CartonWeight = 0;
        //            }
        //            if (Single.TryParse(cRow["QtyOrdered"].ToString(), out nTemp))
        //            {
        //                cItem.QtyOrdered = nTemp;
        //            }
        //            else
        //            {
        //                cItem.QtyOrdered = 0;
        //            }
        //            if (Single.TryParse(cRow["QtyShipped"].ToString(), out nTemp))
        //            {
        //                cItem.QtyShipped = nTemp;
        //            }
        //            else
        //            {
        //                cItem.QtyShipped = 0;
        //            }
        //            if (Single.TryParse(cRow["QtyRecd"].ToString(), out nTemp))
        //            {
        //                cItem.QtyRecd = nTemp;
        //            }
        //            else
        //            {
        //                cItem.QtyRecd = 0;
        //            }
        //            if (Single.TryParse(cRow["QtyCancel"].ToString(), out nTemp))
        //            {
        //                cItem.QtyCancel = nTemp;
        //            }
        //            else
        //            {
        //                cItem.QtyCancel = 0;
        //            }
        //            if (Single.TryParse(cRow["ValueOrdered"].ToString(), out nTemp))
        //            {
        //                cItem.ValueOrdered = nTemp;
        //            }
        //            else
        //            {
        //                cItem.ValueOrdered = 0;
        //            }
        //            if (Single.TryParse(cRow["ValueShipped"].ToString(), out nTemp))
        //            {
        //                cItem.ValueShipped = nTemp;
        //            }
        //            else
        //            {
        //                cItem.ValueShipped = 0;
        //            }
        //            if (Single.TryParse(cRow["ValueRecd"].ToString(), out nTemp))
        //            {
        //                cItem.ValueRecd = nTemp;
        //            }
        //            else
        //            {
        //                cItem.ValueRecd = 0;
        //            }
        //            if (Single.TryParse(cRow["ValueCancel"].ToString(), out nTemp))
        //            {
        //                cItem.ValueCancel = nTemp;
        //            }
        //            else
        //            {
        //                cItem.ValueCancel = 0;
        //            }
        //            cItem.OriginPoNum = cRow["OriginPoNum"].ToString();
        //            if (Single.TryParse(cRow["OriginPoSequence"].ToString(), out nTemp))
        //            {
        //                cItem.OriginPoSequence = nTemp;
        //            }
        //            else
        //            {
        //                cItem.OriginPoSequence = 0;
        //            }
        //            cItem.OeLinkOrderNum = cRow["OeLinkOrderNum"].ToString();
        //            if (Single.TryParse(cRow["OeLinkOrderSequence"].ToString(), out nTemp))
        //            {
        //                cItem.OeLinkOrderSequence = nTemp;
        //            }
        //            else
        //            {
        //                cItem.OeLinkOrderSequence = 0;
        //            }
        //            cItem.SpecialInstructions = cRow["SpecialInstructions"].ToString();
        //            cItem.Notes = cRow["Notes"].ToString();
        //            cItem.Status = cRow["Status"].ToString();
        //            if (Single.TryParse(cRow["RoyaltyRate"].ToString(), out nTemp))
        //            {
        //                cItem.RoyaltyRate = nTemp;
        //            }
        //            else
        //            {
        //                cItem.RoyaltyRate = 0;
        //            }
        //            if (Single.TryParse(cRow["RoyaltyAmount"].ToString(), out nTemp))
        //            {
        //                cItem.RoyaltyAmount = nTemp;
        //            }
        //            else
        //            {
        //                cItem.RoyaltyAmount = 0;
        //            }
        //            if (Single.TryParse(cRow["CommissionRate"].ToString(), out nTemp))
        //            {
        //                cItem.CommissionRate = nTemp;
        //            }
        //            else
        //            {
        //                cItem.CommissionRate = 0;
        //            }
        //            if (Single.TryParse(cRow["CommissionAmount"].ToString(), out nTemp))
        //            {
        //                cItem.CommissionAmount = nTemp;
        //            }
        //            else
        //            {
        //                cItem.CommissionAmount = 0;
        //            }
        //            if (Single.TryParse(cRow["SurchargeRate"].ToString(), out nTemp))
        //            {
        //                cItem.SurchargeRate = nTemp;
        //            }
        //            else
        //            {
        //                cItem.SurchargeRate = 0;
        //            }
        //            if (Single.TryParse(cRow["SurchargeAmount"].ToString(), out nTemp))
        //            {
        //                cItem.SurchargeAmount = nTemp;
        //            }
        //            else
        //            {
        //                cItem.SurchargeAmount = 0;
        //            }
        //            cItem.SurchargeReason = cRow["SurchargeReason"].ToString();
        //            if (Single.TryParse(cRow["TaxAmount"].ToString(), out nTemp))
        //            {
        //                cItem.TaxAmount = nTemp;
        //            }
        //            else
        //            {
        //                cItem.TaxAmount = 0;
        //            }

        //            cPO.Items.Add(cItem);
        //        }
        //        cDr.Close();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return false;
        //    }
        //}



        //public static bool OEExists(string orderNo)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        string sOE;

        //        sSql = "Select " +
        //            "ORDER_NO as OrderNo " +
        //            "from SO_HEADER " +
        //            " WHERE ORDER_NO = @ORDERNO";
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@ORDERNO", orderNo);
        //        cDr = cSqlCmd.ExecuteReader();
        //        while (cDr.Read())
        //        {
        //            sOE = cDr["OrderNo"].ToString();
        //            if (sOE == orderNo)
        //            {
        //                cDr.Close();
        //                return true;
        //            }
        //        }
        //        cDr.Close();
        //        return false;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return false;
        //    }
        //}
        //public static OEHeader RetrieveOE(string orderNo, bool headerOnly)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        Single nTemp;
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        OEHeader cOE = new OEHeader();

        //        sSql = "Select " +
        //                "ORDER_NO as OrderNo, ORDER_DATE as OrderDate, WAREHOUSE_ID as WarehouseId, " +
        //                "ORDER_TYPE as OrderType, STATUS as Status, UBI_STATUS as UbiStatus, " +
        //                "CUSTOMER_ID as CustomerId, CUSTOMER_ADDR_ID as CustomerAddrId, DEL_NAME as DelName, " +
        //                "DEL_ADDR_1 as DelAddr1, DEL_ADDR_2 as DelAddr2, DEL_ADDR_3 as DelAddr3, " +
        //                "DEL_STATE as DelState, DEL_POST_CODE as DelPostCode, DEL_COUNTRY as DelCountry, " +
        //                "DEL_CONTACT as DelContact, DEL_PHONE as DelPhone, DEL_FAX as DelFax, " +
        //                "DEL_EMAIL as DelEmail, CUSTOMER_ORDER_NO as CustomerOrderNo, SALES_CONTRACT as SalesContract, " +
        //                "ORIGIN as Origin, INVOICE_TYPE as InvoiceType, PI_TYPE as PiType, ETD as Etd, " +
        //                "ETA as Eta, DEL_DATE as DelDate, PORT_OF_LOADING as PortOfLoading, DESTINATION as Destination, " +
        //                "CONTAINER_TYPE as ContainerType, CONTAINER_NO as ContainerNo, VESSEL as Vessel, " +
        //                "SEAL_NO as SealNo, PRICE_CODE as PriceCode, CURRENCY as Currency, QUOTE_EXCHANGE as QuoteExchange, " +
        //                "EXCHANGE_RATE as ExchangeRate, DISCOUNT_RATE as DiscountRate, DISCOUNT_AMT as DiscountAmt, " +
        //                "COMMISSION_RATE as CommissionRate, COMMISSION_AMOUNT as CommissionAmount, " +
        //                "SURCHARGE_RATE as SurchargeRate, SURCHARGE_AMOUNT as SurchargeAmount, PO_LINK_NO as PoLinkNo, " +
        //                "ORIGIN_SO_NUM as OriginSoNum, VALUE_ORDERED as ValueOrdered, VALUE_DELIVERED as ValueDelivered, " +
        //                "VALUE_CANCELLED as ValueCancelled, TOTAL_CARTONS as TotalCartons, TOTAL_VOLUME_MTR as TotalVolumeMtr, " +
        //                "TOTAL_WEIGHT as TotalWeight, SHIPPING_MARKS as ShippingMarks, SURCHARGE_REASON as SurchargeReason, " +
        //                "CUST_REQ_DATE_START as CustReqDateStart, CUST_REQ_DATE_END as CustReqDateEnd, " +
        //                "SCHEDULED_PROMO as ScheduledPromo, QUOTED_MARGIN as QuotedMargin, BOSS as Boss, " +
        //                "DEL_CUST_ID as DelCustId, CONTEXT as Context " +
        //                "from SO_HEADER " +
        //                "WHERE ORDER_NO = @ORDERNO";

        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@ORDERNO", orderNo);
        //        cDr = cSqlCmd.ExecuteReader();
        //        while (cDr.Read())
        //        {
        //            cOE = new OEHeader();

        //            cOE.OrderNo = cDr["OrderNo"].ToString();
        //            if (cDr["OrderDate"] != System.DBNull.Value)
        //            {
        //                cOE.OrderDate = (DateTime)cDr["OrderDate"];
        //            }
        //            else
        //            {
        //                cOE.OrderDate = new DateTime(1901, 1, 1);
        //            }
        //            cOE.WarehouseId = cDr["WarehouseId"].ToString();
        //            cOE.OrderType = cDr["OrderType"].ToString();
        //            cOE.Status = cDr["Status"].ToString();
        //            cOE.UbiStatus = cDr["UbiStatus"].ToString();
        //            cOE.CustomerId = cDr["CustomerId"].ToString();
        //            cOE.CustomerAddrId = cDr["CustomerAddrId"].ToString();
        //            cOE.DelName = cDr["DelName"].ToString();
        //            cOE.DelAddr1 = cDr["DelAddr1"].ToString();
        //            cOE.DelAddr2 = cDr["DelAddr2"].ToString();
        //            cOE.DelAddr3 = cDr["DelAddr3"].ToString();
        //            cOE.DelState = cDr["DelState"].ToString();
        //            cOE.DelPostCode = cDr["DelPostCode"].ToString();
        //            cOE.DelCountry = cDr["DelCountry"].ToString();
        //            cOE.DelContact = cDr["DelContact"].ToString();
        //            cOE.DelPhone = cDr["DelPhone"].ToString();
        //            cOE.DelFax = cDr["DelFax"].ToString();
        //            cOE.DelEmail = cDr["DelEmail"].ToString();
        //            cOE.CustomerOrderNo = cDr["CustomerOrderNo"].ToString();
        //            cOE.SalesContract = cDr["SalesContract"].ToString();
        //            cOE.Origin = cDr["Origin"].ToString();
        //            cOE.InvoiceType = cDr["InvoiceType"].ToString();
        //            cOE.PiType = cDr["PiType"].ToString();
        //            if (cDr["Etd"] != System.DBNull.Value)
        //            {
        //                cOE.Etd = (DateTime)cDr["Etd"];
        //            }
        //            else
        //            {
        //                cOE.Etd = new DateTime(1901, 1, 1);
        //            }
        //            if (cDr["Eta"] != System.DBNull.Value)
        //            {
        //                cOE.Eta = (DateTime)cDr["Eta"];
        //            }
        //            else
        //            {
        //                cOE.Eta = new DateTime(1901, 1, 1);
        //            }
        //            if (cDr["DelDate"] != System.DBNull.Value)
        //            {
        //                cOE.DelDate = (DateTime)cDr["DelDate"];
        //            }
        //            else
        //            {
        //                cOE.DelDate = new DateTime(1901, 1, 1);
        //            }
        //            cOE.PortOfLoading = cDr["PortOfLoading"].ToString();
        //            cOE.Destination = cDr["Destination"].ToString();
        //            cOE.ContainerType = cDr["ContainerType"].ToString();
        //            cOE.ContainerNo = cDr["ContainerNo"].ToString();
        //            cOE.Vessel = cDr["Vessel"].ToString();
        //            cOE.SealNo = cDr["SealNo"].ToString();
        //            cOE.PriceCode = cDr["PriceCode"].ToString();
        //            cOE.Currency = cDr["Currency"].ToString();
        //            if (Single.TryParse(cDr["QuoteExchange"].ToString(), out nTemp))
        //            {
        //                cOE.QuoteExchange = nTemp;
        //            }
        //            else
        //            {
        //                cOE.QuoteExchange = 0;
        //            }
        //            if (Single.TryParse(cDr["ExchangeRate"].ToString(), out nTemp))
        //            {
        //                cOE.ExchangeRate = nTemp;
        //            }
        //            else
        //            {
        //                cOE.ExchangeRate = 0;
        //            }
        //            if (Single.TryParse(cDr["DiscountRate"].ToString(), out nTemp))
        //            {
        //                cOE.DiscountRate = nTemp;
        //            }
        //            else
        //            {
        //                cOE.DiscountRate = 0;
        //            }
        //            if (Single.TryParse(cDr["DiscountAmt"].ToString(), out nTemp))
        //            {
        //                cOE.DiscountAmt = nTemp;
        //            }
        //            else
        //            {
        //                cOE.DiscountAmt = 0;
        //            }
        //            if (Single.TryParse(cDr["CommissionRate"].ToString(), out nTemp))
        //            {
        //                cOE.CommissionRate = nTemp;
        //            }
        //            else
        //            {
        //                cOE.CommissionRate = 0;
        //            }
        //            if (Single.TryParse(cDr["CommissionAmount"].ToString(), out nTemp))
        //            {
        //                cOE.CommissionAmount = nTemp;
        //            }
        //            else
        //            {
        //                cOE.CommissionAmount = 0;
        //            }
        //            if (Single.TryParse(cDr["SurchargeRate"].ToString(), out nTemp))
        //            {
        //                cOE.SurchargeRate = nTemp;
        //            }
        //            else
        //            {
        //                cOE.SurchargeRate = 0;
        //            }
        //            if (Single.TryParse(cDr["SurchargeAmount"].ToString(), out nTemp))
        //            {
        //                cOE.SurchargeAmount = nTemp;
        //            }
        //            else
        //            {
        //                cOE.SurchargeAmount = 0;
        //            }
        //            cOE.PoLinkNo = cDr["PoLinkNo"].ToString();
        //            cOE.OriginSoNum = cDr["OriginSoNum"].ToString();
        //            if (Single.TryParse(cDr["ValueOrdered"].ToString(), out nTemp))
        //            {
        //                cOE.ValueOrdered = nTemp;
        //            }
        //            else
        //            {
        //                cOE.ValueOrdered = 0;
        //            }
        //            if (Single.TryParse(cDr["ValueDelivered"].ToString(), out nTemp))
        //            {
        //                cOE.ValueDelivered = nTemp;
        //            }
        //            else
        //            {
        //                cOE.ValueDelivered = 0;
        //            }
        //            if (Single.TryParse(cDr["ValueCancelled"].ToString(), out nTemp))
        //            {
        //                cOE.ValueCancelled = nTemp;
        //            }
        //            else
        //            {
        //                cOE.ValueCancelled = 0;
        //            }
        //            if (Single.TryParse(cDr["TotalCartons"].ToString(), out nTemp))
        //            {
        //                cOE.TotalCartons = nTemp;
        //            }
        //            else
        //            {
        //                cOE.TotalCartons = 0;
        //            }
        //            if (Single.TryParse(cDr["TotalVolumeMtr"].ToString(), out nTemp))
        //            {
        //                cOE.TotalVolumeMtr = nTemp;
        //            }
        //            else
        //            {
        //                cOE.TotalVolumeMtr = 0;
        //            }
        //            if (Single.TryParse(cDr["TotalWeight"].ToString(), out nTemp))
        //            {
        //                cOE.TotalWeight = nTemp;
        //            }
        //            else
        //            {
        //                cOE.TotalWeight = 0;
        //            }
        //            cOE.ShippingMarks = cDr["ShippingMarks"].ToString();
        //            cOE.SurchargeReason = cDr["SurchargeReason"].ToString();
        //            if (cDr["CustReqDateStart"] != System.DBNull.Value)
        //            {
        //                cOE.CustReqDateStart = (DateTime)cDr["CustReqDateStart"];
        //            }
        //            else
        //            {
        //                cOE.CustReqDateStart = new DateTime(1901, 1, 1);
        //            }
        //            if (cDr["CustReqDateEnd"] != System.DBNull.Value)
        //            {
        //                cOE.CustReqDateEnd = (DateTime)cDr["CustReqDateEnd"];
        //            }
        //            else
        //            {
        //                cOE.CustReqDateEnd = new DateTime(1901, 1, 1);
        //            }
        //            cOE.ScheduledPromo = cDr["ScheduledPromo"].ToString();
        //            if (Single.TryParse(cDr["QuotedMargin"].ToString(), out nTemp))
        //            {
        //                cOE.QuotedMargin = nTemp;
        //            }
        //            else
        //            {
        //                cOE.QuotedMargin = 0;
        //            }
        //            cOE.Boss = cDr["Boss"].ToString();
        //            cOE.DelCustId = cDr["DelCustId"].ToString();
        //            cOE.Context = cDr["Context"].ToString();

        //        }
        //        cDr.Close();
        //        if (headerOnly)
        //        {
        //            return cOE;
        //        }
        //        if (RetrieveOEItems(orderNo, ref cOE))
        //        {
        //            return cOE;
        //        }
        //        else
        //        {
        //            return new OEHeader();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return new OEHeader();
        //    }
        //}
        //private static bool RetrieveOEItems(string orderNo, ref OEHeader cOE)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        Single nTemp;
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        DataTable dtItems;
        //        OEItem cItem = new OEItem();

        //        sSql = "Select " +
        //            "ORDER_NO as OrderNo, SEQUENCE as Sequence, ORDER_DATE as OrderDate, STOCK_CODE as StockCode, " + 
        //            "CATEGORY as Category, LOT_ID as LotId, WAREHOUSE_ID as WarehouseId, ORIGIN_SO_NUM as OriginSoNum, " +
        //            "ORIGIN_SO_SEQUENCE as OriginSoSequence, PO_LINK_PO_NUM as PoLinkPoNum, " +
        //            "PO_LINK_PO_SEQUENCE as PoLinkPoSequence, CUSTOMER_ORDER_NO as CustomerOrderNo, " +
        //            "SALES_CONTRACT as SalesContract, UBI_STATUS as UbiStatus, SPECIAL_ITEM as SpecialItem, " +
        //            "PRIORITY as Priority, FUMIGATION_REQ as FumigationReq, GST_EXEMPT as GstExempt, " +
        //            "QTY_ORD as QtyOrd, PRICE as Price, VALUE_ORD as ValueOrd, QTY_DELIVERED as QtyDelivered, " +
        //            "VALUE_DELIVERED as ValueDelivered, QTY_CANCELLED as QtyCancelled, VALUE_CANCELLED as ValueCancelled, " +
        //            "TAX_RATE as TaxRate, TAX_AMOUNT as TaxAmount, COMMISSION_RATE as CommissionRate, " +
        //            "COMMISSION_AMT as CommissionAmt, DISCOUNT_AMT as DiscountAmt, DISCOUNT_RATE as DiscountRate, " +
        //            "SURCHARGE_RATE as SurchargeRate, SURCHARGE_AMT as SurchargeAmt, CANCEL_REASON as CancelReason, " +
        //            "CANCEL_DATE as CancelDate, CARTON_QTY as CartonQty, INNER_QTY as InnerQty, CUBIC_MTR as CubicMtr, " +
        //            "CARTON_WEIGHT as CartonWeight, TOTAL_VOLUME as TotalVolume, NOTES as Notes, " +
        //            "SPECIAL_INSTRUCTIONS as SpecialInstructions, SURCHARGE_REASON as SurchargeReason " +
        //            "from SO_LINE " + 
        //            "WHERE ORDER_NO = @OENUM";
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@OENUM", orderNo);
        //        cDr = cSqlCmd.ExecuteReader();
        //        dtItems = new DataTable();
        //        dtItems.Load(cDr);

        //        foreach (DataRow cRow in dtItems.Rows)
        //        {
        //            cItem = new OEItem();

        //            cItem.OrderNo = cRow["OrderNo"].ToString();
        //            if (Single.TryParse(cRow["Sequence"].ToString(), out nTemp))
        //            {
        //                cItem.Sequence = nTemp;
        //            }
        //            else
        //            {
        //                cItem.Sequence = 0;
        //            }
        //            if (cRow["OrderDate"] != System.DBNull.Value)
        //            {
        //                cItem.OrderDate = (DateTime)cRow["OrderDate"];
        //            }
        //            else
        //            {
        //                cItem.OrderDate = new DateTime(1901, 1, 1);
        //            }
        //            cItem.StockCode = cRow["StockCode"].ToString();
        //            cItem.Category = cRow["Category"].ToString();
        //            cItem.LotId = cRow["LotId"].ToString();
        //            cItem.WarehouseId = cRow["WarehouseId"].ToString();
        //            cItem.OriginSoNum = cRow["OriginSoNum"].ToString();
        //            if (Single.TryParse(cRow["OriginSoSequence"].ToString(), out nTemp))
        //            {
        //                cItem.OriginSoSequence = nTemp;
        //            }
        //            else
        //            {
        //                cItem.OriginSoSequence = 0;
        //            }
        //            cItem.PoLinkPoNum = cRow["PoLinkPoNum"].ToString();
        //            if (Single.TryParse(cRow["PoLinkPoSequence"].ToString(), out nTemp))
        //            {
        //                cItem.PoLinkPoSequence = nTemp;
        //            }
        //            else
        //            {
        //                cItem.PoLinkPoSequence = 0;
        //            }
        //            cItem.CustomerOrderNo = cRow["CustomerOrderNo"].ToString();
        //            cItem.SalesContract = cRow["SalesContract"].ToString();
        //            cItem.UbiStatus = cRow["UbiStatus"].ToString();
        //            if (cRow["SpecialItem"] != System.DBNull.Value)
        //            {
        //                cItem.SpecialItem = (bool)cRow["SpecialItem"];
        //            }
        //            cItem.Priority = cRow["Priority"].ToString();
        //            if (cRow["FumigationReq"] != System.DBNull.Value)
        //            {
        //                cItem.FumigationReq = (bool)cRow["FumigationReq"];
        //            }
        //            if (cRow["GstExempt"] != System.DBNull.Value)
        //            {
        //                cItem.GstExempt = (bool)cRow["GstExempt"];
        //            }
        //            if (Single.TryParse(cRow["QtyOrd"].ToString(), out nTemp))
        //            {
        //                cItem.QtyOrd = nTemp;
        //            }
        //            else
        //            {
        //                cItem.QtyOrd = 0;
        //            }
        //            if (Single.TryParse(cRow["Price"].ToString(), out nTemp))
        //            {
        //                cItem.Price = nTemp;
        //            }
        //            else
        //            {
        //                cItem.Price = 0;
        //            }
        //            if (Single.TryParse(cRow["ValueOrd"].ToString(), out nTemp))
        //            {
        //                cItem.ValueOrd = nTemp;
        //            }
        //            else
        //            {
        //                cItem.ValueOrd = 0;
        //            }
        //            if (Single.TryParse(cRow["QtyDelivered"].ToString(), out nTemp))
        //            {
        //                cItem.QtyDelivered = nTemp;
        //            }
        //            else
        //            {
        //                cItem.QtyDelivered = 0;
        //            }
        //            if (Single.TryParse(cRow["ValueDelivered"].ToString(), out nTemp))
        //            {
        //                cItem.ValueDelivered = nTemp;
        //            }
        //            else
        //            {
        //                cItem.ValueDelivered = 0;
        //            }
        //            if (Single.TryParse(cRow["QtyCancelled"].ToString(), out nTemp))
        //            {
        //                cItem.QtyCancelled = nTemp;
        //            }
        //            else
        //            {
        //                cItem.QtyCancelled = 0;
        //            }
        //            if (Single.TryParse(cRow["ValueCancelled"].ToString(), out nTemp))
        //            {
        //                cItem.ValueCancelled = nTemp;
        //            }
        //            else
        //            {
        //                cItem.ValueCancelled = 0;
        //            }
        //            if (Single.TryParse(cRow["TaxRate"].ToString(), out nTemp))
        //            {
        //                cItem.TaxRate = nTemp;
        //            }
        //            else
        //            {
        //                cItem.TaxRate = 0;
        //            }
        //            if (Single.TryParse(cRow["TaxAmount"].ToString(), out nTemp))
        //            {
        //                cItem.TaxAmount = nTemp;
        //            }
        //            else
        //            {
        //                cItem.TaxAmount = 0;
        //            }
        //            if (Single.TryParse(cRow["CommissionRate"].ToString(), out nTemp))
        //            {
        //                cItem.CommissionRate = nTemp;
        //            }
        //            else
        //            {
        //                cItem.CommissionRate = 0;
        //            }
        //            if (Single.TryParse(cRow["CommissionAmt"].ToString(), out nTemp))
        //            {
        //                cItem.CommissionAmt = nTemp;
        //            }
        //            else
        //            {
        //                cItem.CommissionAmt = 0;
        //            }
        //            if (Single.TryParse(cRow["DiscountAmt"].ToString(), out nTemp))
        //            {
        //                cItem.DiscountAmt = nTemp;
        //            }
        //            else
        //            {
        //                cItem.DiscountAmt = 0;
        //            }
        //            if (Single.TryParse(cRow["DiscountRate"].ToString(), out nTemp))
        //            {
        //                cItem.DiscountRate = nTemp;
        //            }
        //            else
        //            {
        //                cItem.DiscountRate = 0;
        //            }
        //            if (Single.TryParse(cRow["SurchargeRate"].ToString(), out nTemp))
        //            {
        //                cItem.SurchargeRate = nTemp;
        //            }
        //            else
        //            {
        //                cItem.SurchargeRate = 0;
        //            }
        //            if (Single.TryParse(cRow["SurchargeAmt"].ToString(), out nTemp))
        //            {
        //                cItem.SurchargeAmt = nTemp;
        //            }
        //            else
        //            {
        //                cItem.SurchargeAmt = 0;
        //            }
        //            cItem.CancelReason = cRow["CancelReason"].ToString();
        //            if (cRow["CancelDate"] != System.DBNull.Value)
        //            {
        //                cItem.CancelDate = (DateTime)cRow["CancelDate"];
        //            }
        //            else
        //            {
        //                cItem.CancelDate = new DateTime(1901, 1, 1);
        //            }
        //            if (Single.TryParse(cRow["CartonQty"].ToString(), out nTemp))
        //            {
        //                cItem.CartonQty = nTemp;
        //            }
        //            else
        //            {
        //                cItem.CartonQty = 0;
        //            }
        //            if (Single.TryParse(cRow["InnerQty"].ToString(), out nTemp))
        //            {
        //                cItem.InnerQty = nTemp;
        //            }
        //            else
        //            {
        //                cItem.InnerQty = 0;
        //            }
        //            if (Single.TryParse(cRow["CubicMtr"].ToString(), out nTemp))
        //            {
        //                cItem.CubicMtr = nTemp;
        //            }
        //            else
        //            {
        //                cItem.CubicMtr = 0;
        //            }
        //            if (Single.TryParse(cRow["CartonWeight"].ToString(), out nTemp))
        //            {
        //                cItem.CartonWeight = nTemp;
        //            }
        //            else
        //            {
        //                cItem.CartonWeight = 0;
        //            }
        //            if (Single.TryParse(cRow["TotalVolume"].ToString(), out nTemp))
        //            {
        //                cItem.TotalVolume = nTemp;
        //            }
        //            else
        //            {
        //                cItem.TotalVolume = 0;
        //            }
        //            cItem.Notes = cRow["Notes"].ToString();
        //            cItem.SpecialInstructions = cRow["SpecialInstructions"].ToString();
        //            cItem.SurchargeReason = cRow["SurchargeReason"].ToString();


        //            cOE.Items.Add(cItem);
        //        }
        //        cDr.Close();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return false;
        //    }
        //}


        //public static bool MasterfilesJobExists(string jobNo)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        SqlCommand cSqlCmd;
        //        string sSql;

        //            sSql = "Select " +
        //            "JOB_ID as JobId, PO_NUM as PoNum, SHIP_REF as ShipRef " +
        //            "from JOB " +
        //            " WHERE PO_NUM like '%' + @PONUM + '%' or SHIP_REF like '%' + @SHIPREF + '%'";
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@PONUM", jobNo);
        //        cSqlCmd.Parameters.AddWithValue("@SHIPREF", jobNo);
        //        cDr = cSqlCmd.ExecuteReader();
        //        DataTable dtJobs = new DataTable();
        //        dtJobs.Load(cDr);
        //        cDr.Close();
        //        if (dtJobs.Rows.Count >= 1)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return false;
        //    }
        //}


        //public static DataTable MasterfilesJobSearch(string sJobNo)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        DataTable dtJobs;

        //        string sWhere = "";

        //        sSql = "select " +
        //            "A.JOB_ID as JobId, A.PO_NUM as PoNum, A.SHIP_REF as ShipRef, B.DESCRIPTION as JobStatus, " +
        //            "C.DESCRIPTION as FCL, A.GOODS_DESC as GoodsDesc, A.ETD_SHIP_DATE as ETD, A.ETA_PORT_DATE as ETA, " +
        //            "A.BOSS as Boss " +
        //            "from JOB A " +
        //            "left outer join REF_DATA B on A.JOB_STATUS = B.REF_ID and B.REF_TABLE_ID = 'JOBSTATUS' " +
        //            "left outer join REF_DATA C on A.FCL = C.REF_ID and C.REF_TABLE_ID = 'FCL' " +
        //            "where A.PO_NUM like '%' + @PONUM + '%' or A.SHIP_REF like '%' + @SHIPREF + '%' " +
        //            "or A.JOB_ID like '%' + @JOBID + '%' ";
        //        sSql = sSql + sWhere;
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@PONUM", sJobNo);
        //        cSqlCmd.Parameters.AddWithValue("@SHIPREF", sJobNo);
        //        cSqlCmd.Parameters.AddWithValue("@JOBID", sJobNo);
        //        cDr = cSqlCmd.ExecuteReader();
        //        dtJobs = new DataTable();
        //        dtJobs.Load(cDr);
        //        cDr.Close();
        //        return dtJobs;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return null;
        //    }
        //}

        //public static MasterfilesJob RetrieveMasterfilesJob(string jobNo)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        Single nTemp;
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        MasterfilesJob cJob = new MasterfilesJob();

        //        sSql = "Select " +
        //                "JOB_ID as JobId, ORDER_DATE as OrderDate, PO_NUM as PoNum, SHIP_TYPE as ShipType, " +
        //                "SOURCE_TYPE as SourceType, JOB_STATUS as JobStatus, SHIP_REF as ShipRef, JOB_TYPE as JobType, " +
        //                "SUPPLIER_ID as SupplierId, SUPPLIER_NAME as SupplierName, INVOICE_CUST_ID as InvoiceCustId, " +
        //                "INVOICE_CUST_NAME as InvoiceCustName, JOB_CUST_TERMS as JobCustTerms, JOB_SUPP_TERMS as JobSuppTerms, " +
        //                "CUST_PAY_TERMS as CustPayTerms, CUST_DEPOSIT as CustDeposit, SUPP_DEPOSIT as SuppDeposit, " +
        //                "CONSIGNEE as Consignee, INVOICED_FROM as InvoicedFrom, DEST_PORT as DestPort, " +
        //                "ETD_SHIP_DATE as EtdShipDate, ETD_CONFIRMED as EtdConfirmed, ETA_PORT_DATE as EtaPortDate, " +
        //                "ETA_CONFIRMED as EtaConfirmed, CUST_REQ_DATE_START as CustReqDateStart, " +
        //                "CUST_REQ_DATE_END as CustReqDateEnd, VESSEL_VOYAGE as VesselVoyage, VESSEL_CONFIRMED as VesselConfirmed, " +
        //                "PROMO_ORDER_CUBICS as PromoOrderCubics, SCHEDULED_PROMO as ScheduledPromo, FCL as Fcl, " +
        //                "CUST_PO as CustPo, SUPP_PI as SuppPi, CONTAINER_NO as ContainerNo, DELIVERY_CUST as DeliveryCust, " +
        //                "DELIVERY_CUST_ADDR as DeliveryCustAddr, DEL_ADDR_1 as DelAddr1, DEL_ADDR_2 as DelAddr2, " +
        //                "DEL_ADDR_3 as DelAddr3, DEL_ADDR_STATE as DelAddrState, DEL_ADDR_POSTCODE as DelAddrPostcode, " +
        //                "DEL_ADDR_COUNTRY as DelAddrCountry, DROP_CUST as DropCust, DROP_CUST_ADDR as DropCustAddr, "  +
        //                "DROP_ADDR_1 as DropAddr1, DROP_ADDR_2 as DropAddr2, DROP_ADDR_3 as DropAddr3, " +
        //                "DROP_ADDR_STATE as DropAddrState, DROP_ADDR_POSTCODE as DropAddrPostcode, " +
        //                "DROP_ADDR_COUNTRY as DropAddrCountry, SHIPPING_LINE as ShippingLine, GOODS_DESC as GoodsDesc, " +
        //                "PORT_OF_ORIGIN as PortOfOrigin, SUPP_INV_NO as SuppInvNo, SUPP_INV_VAL as SuppInvVal, " +
        //                "PO_CURRENCY as PoCurrency, PO_EXCHANGE as PoExchange, QUOTED_MARGIN as QuotedMargin, " +
        //                "CUST_INV_VAL as CustInvVal, SALES_CURRENCY as SalesCurrency, BOSS as Boss, " +
        //                "CONT_TRACK_FIRST_AVL as ContTrackFirstAvl, CONT_TRACK_DET_START as ContTrackDetStart, " +
        //                "CONT_TRACK_EMPTY_RET as ContTrackEmptyRet, CONT_TRACK_DET_VAL as ContTrackDetVal, " +
        //                "MERGE_DATE as MergeDate, FIRST_DROP_DEL_DATE as FirstDropDelDate, CUST_DEL_DATE as CustDelDate, " +
        //                "PL_SENT_TO_CUST as PlSentToCust, INV_SENT_TO_CUST as InvSentToCust, CUST_INV_IN_MYOB as CustInvInMyob, " +
        //                "SUPP_INV_IN_MYOB as SuppInvInMyob, MYOB_CUST_INV_CRE as MyobCustInvCre, MYOB_VERSION as MyobVersion, " +
        //                "DOCS_TO_CUSTOMS as DocsToCustoms, INV_CRE_USER as InvCreUser, CBA_RCPT_BATCH as CbaRcptBatch, " +
        //                "CBA_INV_BATCH as CbaInvBatch, CBA_INV_NO as CbaInvNo, CBA_OUTPUT as CbaOutput, DUTY_STATUS as DutyStatus, " +
        //                "CARTAGE_STATUS as CartageStatus, DANGEROUS_GOODS as DangerousGoods, THIRD_PARTY_LOGISTICS as ThirdPartyLogistics, " +
        //                "COMPLETED as Completed, PARENT_JOB_ID as ParentJobId, CONSOL_TRACK as ConsolTrack, " +
        //                "LOGISTICS_PRIORITY as LogisticsPriority, REPORT_EXCLUSION as ReportExclusion, SEAL_NO as SealNo, " +
        //                "CUSTOMER_INVOICE_TYPE as CustomerInvoiceType, CUSTOMER_PROFORMA_TYPE as CustomerProformaType, " +
        //                "CONTEXT as Context, JOB_DETAILS as JobDetails, BUYING_TEAM as BuyingTeam, " +
        //                "SUPP_PROFORMA_RECD as SuppProformaRecd, SUPP_INVOICE_RECD as SuppInvoiceRecd, " +
        //                "SUPP_PACKINGLIST_RECD as SuppPackinglistRecd, SUPP_BLFCR_RECD as SuppBlfcrRecd, " +
        //                "SUPP_PACKINGDEC_RECD as SuppPackingdecRecd, SUPP_MANDEC_RECD as SuppMandecRecd, " +
        //                "SUPP_FUMO_RECD as SuppFumoRecd, SUPP_FUMOSTORAGE_RECD as SuppFumostorageRecd, " +
        //                "SUPP_COO_RECD as SuppCooRecd, SUPP_TELEX_REQUESTED as SuppTelexRequested, " +
        //                "SUPP_SHIPPER_BOOKING_CONF_RECD as SuppShipperBookingConfRecd, CUST_PO_RECD as CustPoRecd, " +
        //                "DOCS_MERGED_BY as DocsMergedBy, STOCK_READY_BY as StockReadyBy, WHARF_CUTOFF as WharfCutoff, " +
        //                "ORIGINAL_ETD as OriginalEtd, CONTAINER_NETTING_REQ as ContainerNettingReq, " +
        //                "CONTAINER_NETTING_RECD as ContainerNettingRecd, SHIPMENT_BOOKING_REQ as ShipmentBookingReq, " +
        //                "SHIPMENT_BOOKING_REQ_DATE as ShipmentBookingReqDate, SHIPPING_DOCS_REQ as ShippingDocsReq, " +
        //                "SHIPPING_DOCS_REQ_DATE as ShippingDocsReqDate, FSC_RECD as FscRecd, CUST_PI_SENT as CustPiSent, " +
        //                "SHIP_SO_NUMBER as ShipSoNumber, TIMBER_COMPLIANCE as TimberCompliance, " +
        //                "EXT_STR_1 as ExtStr1, EXT_STR_2 as ExtStr2, EXT_STR_3 as ExtStr3, EXT_STR_4 as ExtStr4, " +
        //                "EXT_STR_5 as ExtStr5, EXT_STR_6 as ExtStr6, EXT_STR_7 as ExtStr7, EXT_STR_8 as ExtStr8, " +
        //                "EXT_NOTE_1 as ExtNote1, EXT_NUM_1 as ExtNum1, EXT_NUM_2 as ExtNum2, EXT_NUM_3 as ExtNum3, " +
        //                "EXT_DATE_1 as ExtDate1, EXT_DATE_2 as ExtDate2, EXT_DATE_3 as ExtDate3, " +
        //                "EXT_BOOL_1 as ExtBool1, EXT_BOOL_2 as ExtBool2, EXT_BOOL_3 as ExtBool3, " +
        //                "OMT_PASSED as OMTPassed " +
        //                "from JOB " +
        //                "WHERE JOB_ID = @JOBID";

        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@JOBID", jobNo);
        //        cDr = cSqlCmd.ExecuteReader();
        //        while (cDr.Read())
        //        {
        //            cJob = new MasterfilesJob();

        //            cJob.JobId = (Guid)cDr["JobId"];
        //            if (cDr["OrderDate"] != System.DBNull.Value)
        //            {
        //                cJob.OrderDate = (DateTime)cDr["OrderDate"];
        //            }
        //            else
        //            {
        //                cJob.OrderDate = new DateTime(1901, 1, 1);
        //            }
        //            cJob.PoNum = cDr["PoNum"].ToString();
        //            cJob.ShipType = cDr["ShipType"].ToString();
        //            cJob.SourceType = cDr["SourceType"].ToString();
        //            cJob.JobStatus = cDr["JobStatus"].ToString();
        //            cJob.ShipRef = cDr["ShipRef"].ToString();
        //            cJob.JobType = cDr["JobType"].ToString();
        //            cJob.SupplierId = cDr["SupplierId"].ToString();
        //            cJob.SupplierName = cDr["SupplierName"].ToString();
        //            cJob.InvoiceCustId = cDr["InvoiceCustId"].ToString();
        //            cJob.InvoiceCustName = cDr["InvoiceCustName"].ToString();
        //            cJob.JobCustTerms = cDr["JobCustTerms"].ToString();
        //            cJob.JobSuppTerms = cDr["JobSuppTerms"].ToString();
        //            cJob.CustPayTerms = cDr["CustPayTerms"].ToString();
        //            cJob.CustDeposit = cDr["CustDeposit"].ToString();
        //            cJob.SuppDeposit = cDr["SuppDeposit"].ToString();
        //            cJob.Consignee = cDr["Consignee"].ToString();
        //            cJob.InvoicedFrom = cDr["InvoicedFrom"].ToString();
        //            cJob.DestPort = cDr["DestPort"].ToString();
        //            if (cDr["EtdShipDate"] != System.DBNull.Value)
        //            {
        //                cJob.EtdShipDate = (DateTime)cDr["EtdShipDate"];
        //            }
        //            else
        //            {
        //                cJob.EtdShipDate = new DateTime(1901, 1, 1);
        //            }
        //            if (cDr["EtdConfirmed"] != System.DBNull.Value)
        //            {
        //                cJob.EtdConfirmed = (DateTime)cDr["EtdConfirmed"];
        //            }
        //            else
        //            {
        //                cJob.EtdConfirmed = new DateTime(1901, 1, 1);
        //            }
        //            if (cDr["EtaPortDate"] != System.DBNull.Value)
        //            {
        //                cJob.EtaPortDate = (DateTime)cDr["EtaPortDate"];
        //            }
        //            else
        //            {
        //                cJob.EtaPortDate = new DateTime(1901, 1, 1);
        //            }
        //            if (cDr["EtaConfirmed"] != System.DBNull.Value)
        //            {
        //                cJob.EtaConfirmed = (DateTime)cDr["EtaConfirmed"];
        //            }
        //            else
        //            {
        //                cJob.EtaConfirmed = new DateTime(1901, 1, 1);
        //            }
        //            if (cDr["CustReqDateStart"] != System.DBNull.Value)
        //            {
        //                cJob.CustReqDateStart = (DateTime)cDr["CustReqDateStart"];
        //            }
        //            else
        //            {
        //                cJob.CustReqDateStart = new DateTime(1901, 1, 1);
        //            }
        //            if (cDr["CustReqDateEnd"] != System.DBNull.Value)
        //            {
        //                cJob.CustReqDateEnd = (DateTime)cDr["CustReqDateEnd"];
        //            }
        //            else
        //            {
        //                cJob.CustReqDateEnd = new DateTime(1901, 1, 1);
        //            }
        //            cJob.VesselVoyage = cDr["VesselVoyage"].ToString();
        //            if (cDr["VesselConfirmed"] != System.DBNull.Value)
        //            {
        //                cJob.VesselConfirmed = (DateTime)cDr["VesselConfirmed"];
        //            }
        //            else
        //            {
        //                cJob.VesselConfirmed = new DateTime(1901, 1, 1);
        //            }
        //            if (Single.TryParse(cDr["PromoOrderCubics"].ToString(), out nTemp))
        //            {
        //                cJob.PromoOrderCubics = nTemp;
        //            }
        //            else
        //            {
        //                cJob.PromoOrderCubics = 0;
        //            }
        //            cJob.ScheduledPromo = cDr["ScheduledPromo"].ToString();
        //            cJob.Fcl = cDr["Fcl"].ToString();
        //            cJob.CustPo = cDr["CustPo"].ToString();
        //            cJob.SuppPi = cDr["SuppPi"].ToString();
        //            cJob.ContainerNo = cDr["ContainerNo"].ToString();
        //            cJob.DeliveryCust = cDr["DeliveryCust"].ToString();
        //            cJob.DeliveryCustAddr = cDr["DeliveryCustAddr"].ToString();
        //            cJob.DelAddr1 = cDr["DelAddr1"].ToString();
        //            cJob.DelAddr2 = cDr["DelAddr2"].ToString();
        //            cJob.DelAddr3 = cDr["DelAddr3"].ToString();
        //            cJob.DelAddrState = cDr["DelAddrState"].ToString();
        //            cJob.DelAddrPostcode = cDr["DelAddrPostcode"].ToString();
        //            cJob.DelAddrCountry = cDr["DelAddrCountry"].ToString();
        //            cJob.DropCust = cDr["DropCust"].ToString();
        //            cJob.DropCustAddr = cDr["DropCustAddr"].ToString();
        //            cJob.DropAddr1 = cDr["DropAddr1"].ToString();
        //            cJob.DropAddr2 = cDr["DropAddr2"].ToString();
        //            cJob.DropAddr3 = cDr["DropAddr3"].ToString();
        //            cJob.DropAddrState = cDr["DropAddrState"].ToString();
        //            cJob.DropAddrPostcode = cDr["DropAddrPostcode"].ToString();
        //            cJob.DropAddrCountry = cDr["DropAddrCountry"].ToString();
        //            cJob.ShippingLine = cDr["ShippingLine"].ToString();
        //            cJob.GoodsDesc = cDr["GoodsDesc"].ToString();
        //            cJob.PortOfOrigin = cDr["PortOfOrigin"].ToString();
        //            cJob.SuppInvNo = cDr["SuppInvNo"].ToString();
        //            if (Single.TryParse(cDr["SuppInvVal"].ToString(), out nTemp))
        //            {
        //                cJob.SuppInvVal = nTemp;
        //            }
        //            else
        //            {
        //                cJob.SuppInvVal = 0;
        //            }
        //            cJob.PoCurrency = cDr["PoCurrency"].ToString();
        //            if (Single.TryParse(cDr["PoExchange"].ToString(), out nTemp))
        //            {
        //                cJob.PoExchange = nTemp;
        //            }
        //            else
        //            {
        //                cJob.PoExchange = 0;
        //            }
        //            if (Single.TryParse(cDr["QuotedMargin"].ToString(), out nTemp))
        //            {
        //                cJob.QuotedMargin = nTemp;
        //            }
        //            else
        //            {
        //                cJob.QuotedMargin = 0;
        //            }
        //            if (Single.TryParse(cDr["CustInvVal"].ToString(), out nTemp))
        //            {
        //                cJob.CustInvVal = nTemp;
        //            }
        //            else
        //            {
        //                cJob.CustInvVal = 0;
        //            }
        //            cJob.SalesCurrency = cDr["SalesCurrency"].ToString();
        //            cJob.Boss = cDr["Boss"].ToString();
        //            if (cDr["ContTrackFirstAvl"] != System.DBNull.Value)
        //            {
        //                cJob.ContTrackFirstAvl = (DateTime)cDr["ContTrackFirstAvl"];
        //            }
        //            else
        //            {
        //                cJob.ContTrackFirstAvl = new DateTime(1901, 1, 1);
        //            }
        //            if (cDr["ContTrackDetStart"] != System.DBNull.Value)
        //            {
        //                cJob.ContTrackDetStart = (DateTime)cDr["ContTrackDetStart"];
        //            }
        //            else
        //            {
        //                cJob.ContTrackDetStart = new DateTime(1901, 1, 1);
        //            }
        //            if (cDr["ContTrackEmptyRet"] != System.DBNull.Value)
        //            {
        //                cJob.ContTrackEmptyRet = (DateTime)cDr["ContTrackEmptyRet"];
        //            }
        //            else
        //            {
        //                cJob.ContTrackEmptyRet = new DateTime(1901, 1, 1);
        //            }
        //            if (Single.TryParse(cDr["ContTrackDetVal"].ToString(), out nTemp))
        //            {
        //                cJob.ContTrackDetVal = nTemp;
        //            }
        //            else
        //            {
        //                cJob.ContTrackDetVal = 0;
        //            }

        //            if (cDr["ConsolTrack"] != System.DBNull.Value)
        //            {
        //                cJob.ConsolTrack = (bool)cDr["ConsolTrack"];
        //            }
        //            if (cDr["MergeDate"] != System.DBNull.Value)
        //            {
        //                cJob.MergeDate = (DateTime)cDr["MergeDate"];
        //            }
        //            else
        //            {
        //                cJob.MergeDate = new DateTime(1901, 1, 1);
        //            }
        //            if (cDr["FirstDropDelDate"] != System.DBNull.Value)
        //            {
        //                cJob.FirstDropDelDate = (DateTime)cDr["FirstDropDelDate"];
        //            }
        //            else
        //            {
        //                cJob.FirstDropDelDate = new DateTime(1901, 1, 1);
        //            }
        //            if (cDr["CustDelDate"] != System.DBNull.Value)
        //            {
        //                cJob.CustDelDate = (DateTime)cDr["CustDelDate"];
        //            }
        //            else
        //            {
        //                cJob.CustDelDate = new DateTime(1901, 1, 1);
        //            }
        //            cJob.PlSentToCust = cDr["PlSentToCust"].ToString();
        //            cJob.InvSentToCust = cDr["InvSentToCust"].ToString();
        //            cJob.CustInvInMyob = cDr["CustInvInMyob"].ToString();
        //            cJob.SuppInvInMyob = cDr["SuppInvInMyob"].ToString();
        //            if (cDr["MyobCustInvCre"] != System.DBNull.Value)
        //            {
        //                cJob.MyobCustInvCre = (DateTime)cDr["MyobCustInvCre"];
        //            }
        //            else
        //            {
        //                cJob.MyobCustInvCre = new DateTime(1901, 1, 1);
        //            }
        //            cJob.MyobVersion = cDr["MyobVersion"].ToString();
        //            cJob.DocsToCustoms = cDr["DocsToCustoms"].ToString();
        //            cJob.InvCreUser = cDr["InvCreUser"].ToString();
        //            cJob.CbaRcptBatch = cDr["CbaRcptBatch"].ToString();
        //            cJob.CbaInvBatch = cDr["CbaInvBatch"].ToString();
        //            cJob.CbaInvNo = cDr["CbaInvNo"].ToString();
        //            cJob.CbaOutput = cDr["CbaOutput"].ToString();
        //            cJob.DutyStatus = cDr["DutyStatus"].ToString();
        //            cJob.CartageStatus = cDr["CartageStatus"].ToString();
        //            if (cDr["DangerousGoods"] != System.DBNull.Value)
        //            {
        //                cJob.DangerousGoods = (bool)cDr["DangerousGoods"];
        //            }
        //            if (cDr["ThirdPartyLogistics"] != System.DBNull.Value)
        //            {
        //                cJob.ThirdPartyLogistics = (bool)cDr["ThirdPartyLogistics"];
        //            }
        //            if (cDr["Completed"] != System.DBNull.Value)
        //            {
        //                cJob.Completed = (bool)cDr["Completed"];
        //            }
        //            if (cDr["ParentJobId"] != System.DBNull.Value)
        //            {
        //                cJob.ParentJobId = (Guid)cDr["ParentJobId"];
        //            }
        //            else
        //            {
        //                cJob.ParentJobId = Guid.Empty;
        //            }
        //            if (cDr["ConsolTrack"] != System.DBNull.Value)
        //            {
        //                cJob.ConsolTrack = (bool)cDr["ConsolTrack"];
        //            }
        //            cJob.LogisticsPriority = cDr["LogisticsPriority"].ToString();
        //            cJob.ReportExclusion = cDr["ReportExclusion"].ToString();
        //            cJob.SealNo = cDr["SealNo"].ToString();
        //            cJob.CustomerInvoiceType = cDr["CustomerInvoiceType"].ToString();
        //            cJob.CustomerProformaType = cDr["CustomerProformaType"].ToString();
        //            cJob.Context = cDr["Context"].ToString();
        //            cJob.JobDetails = cDr["JobDetails"].ToString();
        //            cJob.BuyingTeam = cDr["BuyingTeam"].ToString();
        //            cJob.SuppProformaRecd = cDr["SuppProformaRecd"].ToString();
        //            cJob.SuppInvoiceRecd = cDr["SuppInvoiceRecd"].ToString();
        //            cJob.SuppPackinglistRecd = cDr["SuppPackinglistRecd"].ToString();
        //            cJob.SuppBlfcrRecd = cDr["SuppBlfcrRecd"].ToString();
        //            cJob.SuppPackingdecRecd = cDr["SuppPackingdecRecd"].ToString();
        //            cJob.SuppMandecRecd = cDr["SuppMandecRecd"].ToString();
        //            cJob.SuppFumoRecd = cDr["SuppFumoRecd"].ToString();
        //            cJob.SuppFumostorageRecd = cDr["SuppFumostorageRecd"].ToString();
        //            cJob.SuppCooRecd = cDr["SuppCooRecd"].ToString();
        //            cJob.SuppTelexRequested = cDr["SuppTelexRequested"].ToString();
        //            cJob.SuppShipperBookingConfRecd = cDr["SuppShipperBookingConfRecd"].ToString();
        //            cJob.CustPoRecd = cDr["CustPoRecd"].ToString();
        //            cJob.DocsMergedBy = cDr["DocsMergedBy"].ToString();

        //            if (cDr["StockReadyBy"] != System.DBNull.Value)
        //            {
        //                cJob.StockReadyBy = (DateTime)cDr["StockReadyBy"];
        //            }
        //            else
        //            {
        //                cJob.StockReadyBy = new DateTime(1901, 1, 1);
        //            }

        //            if (cDr["WharfCutoff"] != System.DBNull.Value)
        //            {
        //                cJob.WharfCutoff = (DateTime)cDr["WharfCutoff"];
        //            }
        //            else
        //            {
        //                cJob.WharfCutoff = new DateTime(1901, 1, 1);
        //            }

        //            if (cDr["OriginalEtd"] != System.DBNull.Value)
        //            {
        //                cJob.OriginalEtd = (DateTime)cDr["OriginalEtd"];
        //            }
        //            else
        //            {
        //                cJob.OriginalEtd = new DateTime(1901, 1, 1);
        //            }

        //            cJob.ContainerNettingReq = cDr["ContainerNettingReq"].ToString();
        //            cJob.ContainerNettingRecd = cDr["ContainerNettingRecd"].ToString();
        //            cJob.ShipmentBookingReq = cDr["ShipmentBookingReq"].ToString();

        //            if (cDr["ShipmentBookingReqDate"] != System.DBNull.Value)
        //            {
        //                cJob.ShipmentBookingReqDate = (DateTime)cDr["ShipmentBookingReqDate"];
        //            }
        //            else
        //            {
        //                cJob.ShipmentBookingReqDate = new DateTime(1901, 1, 1);
        //            }

        //            cJob.ShippingDocsReq = cDr["ShippingDocsReq"].ToString();

        //            if (cDr["ShippingDocsReqDate"] != System.DBNull.Value)
        //            {
        //                cJob.ShippingDocsReqDate = (DateTime)cDr["ShippingDocsReqDate"];
        //            }
        //            else
        //            {
        //                cJob.ShippingDocsReqDate = new DateTime(1901, 1, 1);
        //            }

        //            cJob.FscRecd = cDr["FscRecd"].ToString();
        //            cJob.CustPiSent = cDr["CustPiSent"].ToString();
        //            cJob.ShipSoNumber = cDr["ShipSoNumber"].ToString();
        //            cJob.TimberCompliance = cDr["TimberCompliance"].ToString();
        //            cJob.ExtStr1 = cDr["ExtStr1"].ToString();
        //            cJob.ExtStr2 = cDr["ExtStr2"].ToString();
        //            cJob.ExtStr3 = cDr["ExtStr3"].ToString();
        //            cJob.ExtStr4 = cDr["ExtStr4"].ToString();
        //            cJob.ExtStr5 = cDr["ExtStr5"].ToString();
        //            cJob.ExtStr6 = cDr["ExtStr6"].ToString();
        //            cJob.ExtStr7 = cDr["ExtStr7"].ToString();
        //            cJob.ExtStr8 = cDr["ExtStr8"].ToString();
        //            cJob.ExtNote1 = cDr["ExtNote1"].ToString();

        //            if (Single.TryParse(cDr["ExtNum1"].ToString(), out nTemp))
        //            {
        //                cJob.ExtNum1 = nTemp;
        //            }
        //            else
        //            {
        //                cJob.ExtNum1 = 0;
        //            }

        //            if (Single.TryParse(cDr["ExtNum2"].ToString(), out nTemp))
        //            {
        //                cJob.ExtNum2 = nTemp;
        //            }
        //            else
        //            {
        //                cJob.ExtNum2 = 0;
        //            }

        //            if (Single.TryParse(cDr["ExtNum3"].ToString(), out nTemp))
        //            {
        //                cJob.ExtNum3 = nTemp;
        //            }
        //            else
        //            {
        //                cJob.ExtNum3 = 0;
        //            }

        //            if (cDr["ExtDate1"] != System.DBNull.Value)
        //            {
        //                cJob.ExtDate1 = (DateTime)cDr["ExtDate1"];
        //            }
        //            else
        //            {
        //                cJob.ExtDate1 = new DateTime(1901, 1, 1);
        //            }

        //            if (cDr["ExtDate2"] != System.DBNull.Value)
        //            {
        //                cJob.ExtDate2 = (DateTime)cDr["ExtDate2"];
        //            }
        //            else
        //            {
        //                cJob.ExtDate2 = new DateTime(1901, 1, 1);
        //            }

        //            if (cDr["ExtDate3"] != System.DBNull.Value)
        //            {
        //                cJob.ExtDate3 = (DateTime)cDr["ExtDate3"];
        //            }
        //            else
        //            {
        //                cJob.ExtDate3 = new DateTime(1901, 1, 1);
        //            }

        //            if (cDr["ExtBool1"] != System.DBNull.Value)
        //            {
        //                cJob.ExtBool1 = (bool)cDr["ExtBool1"];
        //            }

        //            if (cDr["ExtBool2"] != System.DBNull.Value)
        //            {
        //                cJob.ExtBool2 = (bool)cDr["ExtBool2"];
        //            }

        //            if (cDr["ExtBool3"] != System.DBNull.Value)
        //            {
        //                cJob.ExtBool3 = (bool)cDr["ExtBool3"];
        //            }

        //            cJob.OMTPassed = cDr["OMTPassed"].ToString();

        //        }
        //        cDr.Close();
        //        return cJob;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return new MasterfilesJob();
        //    }
        //}

        //public static ReportQuickHeader GetReportQuickHeader(string sReportId)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        ReportQuickHeader cHeader = new ReportQuickHeader();

        //        sSql = "Select " +
        //            "REPORT_ID as ReportId, TITLE as Title, DESCRIPTION as Description, " +
        //            "ID_TYPE as IdType, COMMENT as Comment " +
        //            "from REPORT_QUICK_HEADER " +
        //            " WHERE REPORT_ID = @REPORTID";
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@REPORTID", sReportId);
        //        cDr = cSqlCmd.ExecuteReader();
        //        while (cDr.Read())
        //        {
        //            cHeader = new ReportQuickHeader();
        //            cHeader.ReportId = cDr["ReportId"].ToString();
        //            cHeader.Title = cDr["Title"].ToString();
        //            cHeader.Description = cDr["Description"].ToString();
        //            cHeader.IdType = cDr["IdType"].ToString();
        //            cHeader.Comment = cDr["Comment"].ToString();
        //        }
        //        cDr.Close();
        //        return cHeader;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return new ReportQuickHeader();
        //    }

        //}

        //public static StockItemMergeCollection RetrieveStockItemMergeCollection(StringList mergeList)
        //{
        //    try
        //    {
        //        SqlDataReader cDr = null;
        //        List<string> list = mergeList.ListItems;
        //        Guid g = SqlHost.CreateTransitionLookup(list);
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        DataTable dtItems;
        //        StockItemMerge cItem = new StockItemMerge();
        //        StockItemMergeCollection col = new StockItemMergeCollection();

        //        sSql = "Select A.LOOKUP_VALUE as StockCode, B.DESCRIPTION as Description, C.DEF_SUPP_PARTNO as SupplierPartNo " +
        //            "from TRANSITION_LOOKUP_STRING A inner join STOCK B on A.LOOKUP_VALUE = B.STOCK_CODE " +
        //            "left outer join STOCK_LOT C on B.STOCK_CODE = C.STOCK_CODE and B.DEF_LOT = C.SERIAL_LOT_NO " +
        //            "where A.HEADER_ID = @H_ID";
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@H_ID", g);
        //        cDr = cSqlCmd.ExecuteReader();
        //        dtItems = new DataTable();
        //        dtItems.Load(cDr);

        //        foreach (DataRow cRow in dtItems.Rows)
        //        {
        //            cItem = new StockItemMerge();

        //            cItem.StockCode = cRow["StockCode"].ToString();
        //            cItem.Description = cRow["Description"].ToString();
        //            cItem.SupplierPartNo = cRow["SupplierPartNo"].ToString();

        //            col.StockItems.Add(cItem);
        //        }
        //        cDr.Close();
        //        SqlHost.DeleteTransitionLookup(g);

        //        return col;
        //    }
        //    catch (Exception ex)
        //    {
        //        StockItemMergeCollection col = new StockItemMergeCollection();
        //        return col;
        //    }
        //}

        //public static StockItemSalesContractCollection RetrieveStockItemSalesContractCollection(StringList mergeList, string sCustId, string sWhsId)
        //{
        //    try
        //    {
        //        Single nTemp;
        //        SqlDataReader cDr = null;
        //        List<string> list = mergeList.ListItems;
        //        Guid g = SqlHost.CreateTransitionLookup(list);
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        DataTable dtItems;
        //        StockItemSalesContract cItem = new StockItemSalesContract();
        //        StockItemSalesContractCollection col = new StockItemSalesContractCollection();

        //        sSql = "select A.LOOKUP_VALUE as StockCode, B.DESCRIPTION as Description, " +
        //            "C.SERIAL_LOT_NO as LotId, D.WAREHOUSE_ID as WhsId, C.WEIGHT as Weight, " +
        //            "C.UNIT as Unit, C.DEF_SUPP_PARTNO as SupplierPartNo, D.GST_EXEMPT as GSTExempt, " +
        //            "C.BARCODE_1 as Barcode1, E.CODE as CustomerStockCode, E.BARCODE as CustomerBarcode, " +
        //            "E.DESCRIPTION as CustomerDesc, E.LABELLING as CustomerLabelling, " +
        //            "E.INSTRUCTIONS as CustomerInstructions, E.TIHI_DESC as CustomerTiHiDesc, " +
        //            "E.EXT_BOOL_1 as CustExtBool1, E.EXT_BOOL_2 as CustExtBool2, " +
        //            "E.EXT_STR_1 as CustExtStr1, E.EXT_STR_2 as CustExtStr2, " +
        //            "E.EXT_NUM_1 as CustExtNum1, E.EXT_NUM_2 as CustExtNum2 " +
        //            "from TRANSITION_LOOKUP_STRING A " +
        //            "inner join STOCK B on A.LOOKUP_VALUE = B.STOCK_CODE " +
        //            "left outer join STOCK_LOT C on B.STOCK_CODE = C.STOCK_CODE and B.DEF_LOT = C.SERIAL_LOT_NO " +
        //            "left outer join STOCK_WHS D on B.STOCK_CODE = D.STOCK_CODE and D.WAREHOUSE_ID = @WHSID " +
        //            "left outer join CUSTOMER_PRODUCT_PROFILE E on A.LOOKUP_VALUE = E.STOCK_CODE and E.CUSTOMER_ID = @CUSTID " +
        //            "where A.HEADER_ID = @H_ID";

        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@WHSID", sWhsId);
        //        cSqlCmd.Parameters.AddWithValue("@CUSTID", sCustId);
        //        cSqlCmd.Parameters.AddWithValue("@H_ID", g);
        //        cDr = cSqlCmd.ExecuteReader();
        //        dtItems = new DataTable();
        //        dtItems.Load(cDr);

        //        foreach (DataRow cRow in dtItems.Rows)
        //        {
        //            cItem = new StockItemSalesContract();

        //            cItem.StockCode = cRow["StockCode"].ToString();
        //            cItem.Description = cRow["Description"].ToString();
        //            cItem.LotId = cRow["LotId"].ToString();
        //            if (Single.TryParse(cRow["Weight"].ToString(), out nTemp))
        //            {
        //                cItem.Weight = nTemp;
        //            }
        //            else
        //            {
        //                cItem.Weight = 0;
        //            }
        //            cItem.Barcode1 = cRow["Barcode1"].ToString();
        //            cItem.Unit = cRow["Unit"].ToString();
        //            cItem.SupplierPartNo = cRow["SupplierPartNo"].ToString();
        //            if (cRow["GSTExempt"] != System.DBNull.Value)
        //            {
        //                cItem.GSTExempt = (bool)cRow["GSTExempt"];
        //            }
        //            else
        //            {
        //                cItem.GSTExempt = false;
        //            }
        //            cItem.CustomerCode = cRow["CustomerStockCode"].ToString();
        //            cItem.CustomerBarcode = cRow["CustomerBarcode"].ToString();
        //            cItem.CustomerDesc = cRow["CustomerDesc"].ToString();
        //            cItem.CustomerLabelling = cRow["CustomerLabelling"].ToString();
        //            cItem.CustomerInstructions = cRow["CustomerInstructions"].ToString();
        //            cItem.CustomerTiHiDesc = cRow["CustomerTiHiDesc"].ToString();
        //            if (cRow["CustExtBool1"] != System.DBNull.Value)
        //            {
        //                cItem.CustExtBool1 = (bool)cRow["CustExtBool1"];
        //            }
        //            else
        //            {
        //                cItem.CustExtBool1 = false;
        //            }
        //            if (cRow["CustExtBool2"] != System.DBNull.Value)
        //            {
        //                cItem.CustExtBool2 = (bool)cRow["CustExtBool2"];
        //            }
        //            else
        //            {
        //                cItem.CustExtBool2 = false;
        //            }
        //            cItem.CustExtStr1 = cRow["CustExtStr1"].ToString();
        //            cItem.CustExtStr2 = cRow["CustExtStr2"].ToString();
        //            if (Single.TryParse(cRow["CustExtNum1"].ToString(), out nTemp))
        //            {
        //                cItem.CustExtNum1 = nTemp;
        //            }
        //            else
        //            {
        //                cItem.CustExtNum1 = 0;
        //            }
        //            if (Single.TryParse(cRow["CustExtNum2"].ToString(), out nTemp))
        //            {
        //                cItem.CustExtNum2 = nTemp;
        //            }
        //            else
        //            {
        //                cItem.CustExtNum2 = 0;
        //            }


        //            col.StockItems.Add(cItem);
        //        }
        //        cDr.Close();
        //        SqlHost.DeleteTransitionLookup(g);

        //        return col;
        //    }
        //    catch (Exception ex)
        //    {
        //        StockItemSalesContractCollection col = new StockItemSalesContractCollection();
        //        return col;
        //    }
        //}

        //public static StockItemInvoiceCollection RetrieveStockItemInvoiceCollection(StringList mergeList, string sCustId, string sWhsId)
        //{
        //    try
        //    {
        //        SqlDataReader cDr = null;
        //        List<string> list = mergeList.ListItems;
        //        Guid g = SqlHost.CreateTransitionLookup(list);
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        DataTable dtItems;
        //        Single nTemp;
        //        StockItemInvoice cItem = new StockItemInvoice();
        //        StockItemInvoiceCollection col = new StockItemInvoiceCollection();

        //        sSql = "select A.LOOKUP_VALUE as StockCode, B.DESCRIPTION as Description, " +
        //            "C.SERIAL_LOT_NO as LotId, D.WAREHOUSE_ID as WhsId, C.WEIGHT as Weight, " +
        //            "C.UNIT as Unit, C.DEF_SUPP_PARTNO as SupplierPartNo, D.GST_EXEMPT as GSTExempt, " +
        //            "C.BARCODE_1 as Barcode1, E.CODE as CustomerStockCode, E.BARCODE as CustomerBarcode, " +
        //            "E.PRE_PRICE as PrePrice, B.FUMIGATION_REQ as Fumigation, B.TAX_RATE as TaxRate, " +
        //            "E.DESCRIPTION as CustomerDesc, E.LABELLING as CustomerLabelling, " +
        //            "E.INSTRUCTIONS as CustomerInstructions, E.TIHI_DESC as CustomerTiHiDesc, " +
        //            "E.EXT_BOOL_1 as CustExtBool1, E.EXT_BOOL_2 as CustExtBool2, " +
        //            "E.EXT_STR_1 as CustExtStr1, E.EXT_STR_2 as CustExtStr2, " +
        //            "E.EXT_NUM_1 as CustExtNum1, E.EXT_NUM_2 as CustExtNum2 " +
        //            "from TRANSITION_LOOKUP_STRING A " +
        //            "inner join STOCK B on A.LOOKUP_VALUE = B.STOCK_CODE " +
        //            "left outer join STOCK_LOT C on B.STOCK_CODE = C.STOCK_CODE and B.DEF_LOT = C.SERIAL_LOT_NO " +
        //            "left outer join STOCK_WHS D on B.STOCK_CODE = D.STOCK_CODE and D.WAREHOUSE_ID = @WHSID " +
        //            "left outer join CUSTOMER_PRODUCT_PROFILE E on A.LOOKUP_VALUE = E.STOCK_CODE and E.CUSTOMER_ID = @CUSTID " +
        //            "where A.HEADER_ID = @H_ID";

        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@WHSID", sWhsId);
        //        cSqlCmd.Parameters.AddWithValue("@CUSTID", sCustId);
        //        cSqlCmd.Parameters.AddWithValue("@H_ID", g);
        //        cDr = cSqlCmd.ExecuteReader();
        //        dtItems = new DataTable();
        //        dtItems.Load(cDr);

        //        foreach (DataRow cRow in dtItems.Rows)
        //        {
        //            cItem = new StockItemInvoice();

        //            cItem.StockCode = cRow["StockCode"].ToString();
        //            cItem.Description = cRow["Description"].ToString();
        //            cItem.LotId = cRow["LotId"].ToString();
        //            if (Single.TryParse(cRow["Weight"].ToString(), out nTemp))
        //            {
        //                cItem.Weight = nTemp;
        //            }
        //            else
        //            {
        //                cItem.Weight = 0;
        //            }
        //            cItem.Barcode1 = cRow["Barcode1"].ToString();
        //            cItem.Unit = cRow["Unit"].ToString();
        //            cItem.SupplierPartNo = cRow["SupplierPartNo"].ToString();
        //            if (cRow["GSTExempt"] != System.DBNull.Value)
        //            {
        //                cItem.GSTExempt = (bool)cRow["GSTExempt"];
        //            }
        //            else
        //            {
        //                cItem.GSTExempt = false;
        //            }
        //            cItem.CustomerCode = cRow["CustomerStockCode"].ToString();
        //            cItem.CustomerBarcode = cRow["CustomerBarcode"].ToString();
        //            if (Single.TryParse(cRow["PrePrice"].ToString(), out nTemp))
        //            {
        //                cItem.PrePrice = nTemp;
        //            }
        //            else
        //            {
        //                cItem.PrePrice = 0;
        //            }
        //            if (cRow["Fumigation"] != System.DBNull.Value)
        //            {
        //                cItem.Fumigation = (bool)cRow["Fumigation"];
        //            }
        //            else
        //            {
        //                cItem.Fumigation = false;
        //            }
        //            if (Single.TryParse(cRow["TaxRate"].ToString(), out nTemp))
        //            {
        //                cItem.TaxRate = nTemp;
        //            }
        //            else
        //            {
        //                cItem.TaxRate = 0;
        //            }
        //            cItem.CustomerDesc = cRow["CustomerDesc"].ToString();
        //            cItem.CustomerLabelling = cRow["CustomerLabelling"].ToString();
        //            cItem.CustomerInstructions = cRow["CustomerInstructions"].ToString();
        //            cItem.CustomerTiHiDesc = cRow["CustomerTiHiDesc"].ToString();
        //            if (cRow["CustExtBool1"] != System.DBNull.Value)
        //            {
        //                cItem.CustExtBool1 = (bool)cRow["CustExtBool1"];
        //            }
        //            else
        //            {
        //                cItem.CustExtBool1 = false;
        //            }
        //            if (cRow["CustExtBool2"] != System.DBNull.Value)
        //            {
        //                cItem.CustExtBool2 = (bool)cRow["CustExtBool2"];
        //            }
        //            else
        //            {
        //                cItem.CustExtBool2 = false;
        //            }
        //            cItem.CustExtStr1 = cRow["CustExtStr1"].ToString();
        //            cItem.CustExtStr2 = cRow["CustExtStr2"].ToString();
        //            if (Single.TryParse(cRow["CustExtNum1"].ToString(), out nTemp))
        //            {
        //                cItem.CustExtNum1 = nTemp;
        //            }
        //            else
        //            {
        //                cItem.CustExtNum1 = 0;
        //            }
        //            if (Single.TryParse(cRow["CustExtNum2"].ToString(), out nTemp))
        //            {
        //                cItem.CustExtNum2 = nTemp;
        //            }
        //            else
        //            {
        //                cItem.CustExtNum2 = 0;
        //            }

        //            col.StockItems.Add(cItem);
        //        }
        //        cDr.Close();
        //        SqlHost.DeleteTransitionLookup(g);

        //        return col;
        //    }
        //    catch (Exception ex)
        //    {
        //        StockItemInvoiceCollection col = new StockItemInvoiceCollection();
        //        return col;
        //    }
        //}



        //public static StockItemPurchaseOrderCollection RetrieveStockItemPurchaseOrderCollection(StringList mergeList, string sCustId, string sWhsId)
        //{
        //    try
        //    {
        //        SqlDataReader cDr = null;
        //        List<string> list = mergeList.ListItems;
        //        Guid g = SqlHost.CreateTransitionLookup(list);
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        DataTable dtItems;
        //        Single nTemp;
        //        StockItemPurchaseOrder cItem = new StockItemPurchaseOrder();
        //        StockItemPurchaseOrderCollection col = new StockItemPurchaseOrderCollection();

        //        sSql = "select A.LOOKUP_VALUE as StockCode, B.DESCRIPTION as Description, " +
        //            "C.SERIAL_LOT_NO as LotId, D.WAREHOUSE_ID as WhsId, C.WEIGHT as Weight, " +
        //            "C.UNIT as Unit, B.CUSTOMS_CAT as CustomsCat, B.FUMIGATION_REQ as Fumigation, " +
        //            "C.DEF_SUPP_PARTNO as SupplierPartNo, D.GST_EXEMPT as GSTExempt, " +
        //            "C.BARCODE_1 as Barcode1, E.CODE as CustomerCode, E.BARCODE as CustomerBarcode, " +
        //            "E.DESCRIPTION as CustomerDesc, E.LABELLING as CustomerLabelling, " +
        //            "E.INSTRUCTIONS as CustomerInstructions, E.TIHI_DESC as CustomerTiHiDesc, " +
        //            "E.EXT_BOOL_1 as CustExtBool1, E.EXT_BOOL_2 as CustExtBool2, " +
        //            "E.EXT_STR_1 as CustExtStr1, E.EXT_STR_2 as CustExtStr2, " +
        //            "E.EXT_NUM_1 as CustExtNum1, E.EXT_NUM_2 as CustExtNum2 " +
        //            "from TRANSITION_LOOKUP_STRING A " +
        //            "inner join STOCK B on A.LOOKUP_VALUE = B.STOCK_CODE " +
        //            "left outer join STOCK_LOT C on B.STOCK_CODE = C.STOCK_CODE and B.DEF_LOT = C.SERIAL_LOT_NO " +
        //            "left outer join STOCK_WHS D on B.STOCK_CODE = D.STOCK_CODE and D.WAREHOUSE_ID = @WHSID " +
        //            "left outer join CUSTOMER_PRODUCT_PROFILE E on A.LOOKUP_VALUE = E.STOCK_CODE and E.CUSTOMER_ID = @CUSTID " +
        //            "where A.HEADER_ID = @H_ID";

        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@WHSID", sWhsId);
        //        cSqlCmd.Parameters.AddWithValue("@CUSTID", sCustId);
        //        cSqlCmd.Parameters.AddWithValue("@H_ID", g);
        //        cDr = cSqlCmd.ExecuteReader();
        //        dtItems = new DataTable();
        //        dtItems.Load(cDr);

        //        foreach (DataRow cRow in dtItems.Rows)
        //        {
        //            cItem = new StockItemPurchaseOrder();

        //            cItem.StockCode = cRow["StockCode"].ToString();
        //            cItem.Description = cRow["Description"].ToString();
        //            cItem.LotId = cRow["LotId"].ToString();
        //            cItem.WhsId = cRow["WhsId"].ToString();
        //            if (Single.TryParse(cRow["Weight"].ToString(), out nTemp))
        //            {
        //                cItem.Weight = nTemp;
        //            }
        //            else
        //            {
        //                cItem.Weight = 0;
        //            }
        //            cItem.Unit = cRow["Unit"].ToString();
        //            cItem.CustomsCat = cRow["CustomsCat"].ToString();
        //            if (cRow["Fumigation"] != System.DBNull.Value)
        //            {
        //                cItem.Fumigation = (bool)cRow["Fumigation"];
        //            }
        //            else
        //            {
        //                cItem.Fumigation = false;
        //            }
        //            cItem.SupplierPartNo = cRow["SupplierPartNo"].ToString();
        //            if (cRow["GSTExempt"] != System.DBNull.Value)
        //            {
        //                cItem.GSTExempt = (bool)cRow["GSTExempt"];
        //            }
        //            else
        //            {
        //                cItem.GSTExempt = false;
        //            }
        //            cItem.Barcode1 = cRow["Barcode1"].ToString();
        //            cItem.CustomerCode = cRow["CustomerCode"].ToString();
        //            cItem.CustomerBarcode = cRow["CustomerBarcode"].ToString();
        //            cItem.CustomerDesc = cRow["CustomerDesc"].ToString();
        //            cItem.CustomerLabelling = cRow["CustomerLabelling"].ToString();
        //            cItem.CustomerInstructions = cRow["CustomerInstructions"].ToString();
        //            cItem.CustomerTiHiDesc = cRow["CustomerTiHiDesc"].ToString();
        //            if (cRow["CustExtBool1"] != System.DBNull.Value)
        //            {
        //                cItem.CustExtBool1 = (bool)cRow["CustExtBool1"];
        //            }
        //            else
        //            {
        //                cItem.CustExtBool1 = false;
        //            }
        //            if (cRow["CustExtBool2"] != System.DBNull.Value)
        //            {
        //                cItem.CustExtBool2 = (bool)cRow["CustExtBool2"];
        //            }
        //            else
        //            {
        //                cItem.CustExtBool2 = false;
        //            }
        //            cItem.CustExtStr1 = cRow["CustExtStr1"].ToString();
        //            cItem.CustExtStr2 = cRow["CustExtStr2"].ToString();
        //            if (Single.TryParse(cRow["CustExtNum1"].ToString(), out nTemp))
        //            {
        //                cItem.CustExtNum1 = nTemp;
        //            }
        //            else
        //            {
        //                cItem.CustExtNum1 = 0;
        //            }
        //            if (Single.TryParse(cRow["CustExtNum2"].ToString(), out nTemp))
        //            {
        //                cItem.CustExtNum2 = nTemp;
        //            }
        //            else
        //            {
        //                cItem.CustExtNum2 = 0;
        //            }

        //            col.StockItems.Add(cItem);
        //        }
        //        cDr.Close();
        //        SqlHost.DeleteTransitionLookup(g);

        //        return col;
        //    }
        //    catch (Exception ex)
        //    {
        //        StockItemPurchaseOrderCollection col = new StockItemPurchaseOrderCollection();
        //        return col;
        //    }
        //}

        //public static ContextProfile GetContextProfile(string contextId, string sCurrency, string sCountry)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        ContextProfile cContextProfile = new ContextProfile();

        //        sSql = "Select " +
        //            "a.Context_id as ContextId, a.name_1 as Name1, a.eori as EORI, " +
        //            "a.spare_1 as Spare1, a.spare_2 as Spare2, a.spare_3 as Spare3, " +
        //            "b.currency_code as CurrencyCode, b.name_1 as BankName1, b.name_2 as BankName2, " +
        //            "b.swift_code as SwiftCode, b.account_name As AccountName, b.account_number As AccountNo, " +
        //            "b.spare_1 as CurrencySpare1, b.spare_2 As CurrencySpare2, c.country_id As CountryId, " +
        //            "c.name_override as CountryNameOverride, c.tax_no As CountryTaxNo, c.spare_1 As CountrySpare1, " +
        //            "c.spare_2 as CountrySpare2, " +
        //            "c.ff_1 as CountryFF1, c.ff_2 as CountryFF2, " +
        //            "c.ff_3 as CountryFF3, c.ff_4 as CountryFF4, " +
        //            "a.DEF_INV_HDR_URI as DefInvHdrURI, " +
        //            "a.DEF_INV_STAMPSIG_URI as StampSignatureURI, a.INV_NAME as InvoiceName, " +
        //            "a.INV_ADDR1 as InvoiceAddr1, a.INV_ADDR2 as InvoiceAddr2, a.INV_EMAIL as InvoiceEmail, " +
        //            "a.INV_RETENTION as InvoiceRetention, b.SWIFT_LABEL as SwiftLabel " +
        //            "From context a " +
        //            "Left outer join CONTEXT_CURR_PROFILE b on a.context_id = b.context_id And b.currency_code = @CURRENCYCODE " +
        //            "Left outer join CONTEXT_COUNTRY_PROFILE c on a.context_id = c.context_id And c.country_id = @COUNTRYID " +
        //            "where a.Context_Id = @CONTEXTID";
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@CONTEXTID", contextId);
        //        cSqlCmd.Parameters.AddWithValue("@CURRENCYCODE", sCurrency);
        //        cSqlCmd.Parameters.AddWithValue("@COUNTRYID", sCountry);
        //        cDr = cSqlCmd.ExecuteReader();
        //        while (cDr.Read())
        //        {
        //            cContextProfile = new ContextProfile();
        //            cContextProfile.ContextId = cDr["ContextId"].ToString();
        //            cContextProfile.Name1 = cDr["Name1"].ToString();
        //            cContextProfile.EORI = cDr["EORI"].ToString();
        //            cContextProfile.Spare1 = cDr["Spare1"].ToString();
        //            cContextProfile.Spare2 = cDr["Spare2"].ToString();
        //            cContextProfile.Spare3 = cDr["Spare3"].ToString();
        //            cContextProfile.CurrencyCode = cDr["CurrencyCode"].ToString();
        //            cContextProfile.CountryId = cDr["CountryId"].ToString();
        //            cContextProfile.BankName1 = cDr["BankName1"].ToString();
        //            cContextProfile.BankName2 = cDr["BankName2"].ToString();
        //            cContextProfile.CurrencySwiftCode = cDr["SwiftCode"].ToString();
        //            cContextProfile.CurrencyAccountName = cDr["AccountName"].ToString();
        //            cContextProfile.CurrencyAccountNo = cDr["AccountNo"].ToString();
        //            cContextProfile.CurrencySpare1 = cDr["CurrencySpare1"].ToString();
        //            cContextProfile.CurrencySpare2 = cDr["CurrencySpare2"].ToString();
        //            cContextProfile.CountryNameOverride = cDr["CountryNameOverride"].ToString();
        //            cContextProfile.CountryTaxNo = cDr["CountryTaxNo"].ToString();
        //            cContextProfile.CountrySpare1 = cDr["CountrySpare1"].ToString();
        //            cContextProfile.CountrySpare2 = cDr["CountrySpare2"].ToString();
        //            cContextProfile.CountryFF1 = cDr["CountryFF1"].ToString();
        //            cContextProfile.CountryFF2 = cDr["CountryFF2"].ToString();
        //            cContextProfile.CountryFF3 = cDr["CountryFF3"].ToString();
        //            cContextProfile.CountryFF4 = cDr["CountryFF4"].ToString();
        //            cContextProfile.DefaultInvoiceHeaderURI = cDr["DefInvHdrURI"].ToString();
        //            cContextProfile.StampSignatureURI = cDr["StampSignatureURI"].ToString();
        //            cContextProfile.InvoiceName = cDr["InvoiceName"].ToString();
        //            cContextProfile.InvoiceEmail = cDr["InvoiceEmail"].ToString();
        //            cContextProfile.InvoiceAddr1 = cDr["InvoiceAddr1"].ToString();
        //            cContextProfile.InvoiceAddr2 = cDr["InvoiceAddr2"].ToString();
        //            cContextProfile.InvoiceRetention = cDr["InvoiceRetention"].ToString();
        //            cContextProfile.SwiftLabel = cDr["SwiftLabel"].ToString();
        //        }
        //        cDr.Close();
        //        return cContextProfile;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return new ContextProfile();
        //    }
        //}

        //public static DocTemplate GetDocTemplate(string templateCode)
        //{
        //    SqlDataReader cDr = null;
        //    try
        //    {
        //        SqlCommand cSqlCmd;
        //        string sSql;
        //        int iTemp = 0;
        //        Single siTemp = 0;
        //        DocTemplate cDocTemplate = new DocTemplate();

        //        sSql = "Select " +
        //            "a.template_code as TemplateCode, a.description as Description, a.template_file as TemplateFile, " +
        //            "a.template_sheet as TemplateSheet, a.template_row as TemplateRow, a.template_col as TemplateCol, " +
        //            "a.curr_warning as CurrencyWarning, a.def_currency as DefCurrency, " +
        //            "a.template_type as TemplateType, a.template_class as TemplateClass, " +
        //            "a.surcharge_1_type as Surcharge1Type, a.surcharge_1_val as Surcharge1Val, " +
        //            "a.surcharge_2_type as Surcharge2Type, a.surcharge_2_val as Surcharge2Val, " +
        //            "s1.ext_str_1 as Surcharge1ApplyType, s1.ext_str_2 as Surcharge1DisplayLabel, s1.ext_str_3 as Surcharge1Format, " +
        //            "a.surcharge_2_type as Surcharge2Type, a.surcharge_2_val as Surcharge2Val, " +
        //            "s2.ext_str_1 as Surcharge2ApplyType, s2.ext_str_2 as Surcharge2DisplayLabel, s2.ext_str_3 as Surcharge2Format " +
        //            "From doc_template a " +
        //            "left outer join ref_data s1 on a.SURCHARGE_1_TYPE = s1.REF_ID and s1.REF_TABLE_ID = 'INVSURCHTYPE' " +
        //            "left outer join ref_data s2 on a.SURCHARGE_2_TYPE = s2.REF_ID and s2.REF_TABLE_ID = 'INVSURCHTYPE' " +
        //            "where a.template_code = @TEMPLATECODE";
        //        cSqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cSqlCmd.Parameters.AddWithValue("@TEMPLATECODE", templateCode);
        //        cDr = cSqlCmd.ExecuteReader();
        //        while (cDr.Read())
        //        {
        //            cDocTemplate = new DocTemplate();
        //            cDocTemplate.TemplateCode = cDr["TemplateCode"].ToString();
        //            cDocTemplate.Description = cDr["Description"].ToString();
        //            cDocTemplate.TemplateFile = cDr["TemplateFile"].ToString();
        //            cDocTemplate.TemplateSheet = cDr["TemplateSheet"].ToString();
        //            if (int.TryParse(cDr["TemplateRow"].ToString(), out iTemp))
        //            {
        //                cDocTemplate.TemplateRow = iTemp;
        //            }
        //            else
        //            {
        //                cDocTemplate.TemplateRow = 0;
        //            }
        //            if (int.TryParse(cDr["TemplateCol"].ToString(), out iTemp))
        //            {
        //                cDocTemplate.TemplateCol = iTemp;
        //            }
        //            else
        //            {
        //                cDocTemplate.TemplateCol = 0;
        //            }
        //            cDocTemplate.CurrencyWarning = cDr["CurrencyWarning"].ToString();
        //            cDocTemplate.DefCurrency = cDr["DefCurrency"].ToString();
        //            cDocTemplate.TemplateType = cDr["TemplateType"].ToString();
        //            cDocTemplate.TemplateClass = cDr["TemplateClass"].ToString();
        //            cDocTemplate.Surcharge1Type = cDr["Surcharge1Type"].ToString();
        //            if (Single.TryParse(cDr["Surcharge1Val"].ToString(), out siTemp))
        //            {
        //                cDocTemplate.Surcharge1Val = siTemp;
        //            }
        //            else
        //            {
        //                cDocTemplate.Surcharge1Val = 0;
        //            }
        //            cDocTemplate.Surcharge2Type = cDr["Surcharge2Type"].ToString();
        //            if (Single.TryParse(cDr["Surcharge2Val"].ToString(), out siTemp))
        //            {
        //                cDocTemplate.Surcharge2Val = siTemp;
        //            }
        //            else
        //            {
        //                cDocTemplate.Surcharge2Val = 0;
        //            }
        //            cDocTemplate.Surcharge1ApplyType = cDr["Surcharge1ApplyType"].ToString();
        //            cDocTemplate.Surcharge1DisplayLabel = cDr["Surcharge1DisplayLabel"].ToString();
        //            cDocTemplate.Surcharge1Format = cDr["Surcharge1Format"].ToString();
        //            cDocTemplate.Surcharge2ApplyType = cDr["Surcharge2ApplyType"].ToString();
        //            cDocTemplate.Surcharge2DisplayLabel = cDr["Surcharge2DisplayLabel"].ToString();
        //            cDocTemplate.Surcharge2Format = cDr["Surcharge2Format"].ToString();

        //        }
        //        cDr.Close();
        //        return cDocTemplate;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (cDr != null && !cDr.IsClosed)
        //            cDr.Close();
        //        return new DocTemplate();
        //    }
        //}

        //public static FileReturn SetFileReturn(string PrintFile)
        //{
        //    FileReturn r = new FileReturn();
        //    String sAppPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);

        //    string DirectoryIn = "";
        //    string sFile = sAppPath + "\\DBConfig.xml";
        //    XmlDocument xmlSettings = new XmlDocument();
        //    xmlSettings.Load(sFile);
        //    XmlNodeList cNodes = xmlSettings.SelectNodes("/ConfigurationSettings/ConnectionSettings/Directories/Directory");
        //    foreach (XmlNode nd in cNodes)
        //    {
        //        if (nd.Attributes.GetNamedItem("id").Value == "TemplateDirectoryIn")
        //        {
        //            DirectoryIn = nd.Attributes.GetNamedItem("directoryname").Value;
        //        }
        //    }

        //    System.IO.FileStream fs1 = null;
        //    fs1 = System.IO.File.Open(DirectoryIn + PrintFile, FileMode.Open, FileAccess.Read);
        //    byte[] b1 = new byte[fs1.Length];
        //    fs1.Read(b1, 0, (int)fs1.Length);
        //    fs1.Close();
        //    r.FileToReturn = b1;
        //    return r;

        //}

        //public static bool GetUser(string sUserName, string sPassword)
        //{
        //    string sSql;
        //    SqlCommand sqlCmd;
        //    DataSet dataLogin;

        //    try
        //    {
        //        sSql = "select USER_NAME as UserName, FIRST_NAME as FirstName, SURNAME as Surname, " +
        //            "PREF_NAME as PrefName, EMAIL as Email, PHONE1 as Phone1, PHONE2 as Phone2, " +
        //            "SECURITY_LEVEL as SecurityLevel " +
        //            "from USERS where USER_NAME = @UN and PASSWORD = @PWD";
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        sqlCmd = SqlHost.DBConn.CreateCommand();
        //        SqlDataAdapter da = new SqlDataAdapter();
        //        sqlCmd.CommandText = sSql;
        //        sqlCmd.Parameters.AddWithValue("@UN", sUserName);
        //        sqlCmd.Parameters.AddWithValue("@PWD", sPassword);
        //        da.SelectCommand = sqlCmd;
        //        dataLogin = new DataSet();
        //        da.Fill(dataLogin);
        //        if (dataLogin.Tables[0].Rows.Count > 0)
        //        {
        //            SqlHost.CurrentUser = dataLogin.Tables[0].Rows[0];
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        public static ReturnDataSet SetReturnDataSet(string sSql, string sTable, ref DataSet oDs)
        {
            ReturnDataSet rDs = new ReturnDataSet();
            try
            {
                DataTable oDt;
                SqlDataAdapter oDa;
                oDt = new DataTable(sTable);
                oDa = new SqlDataAdapter(sSql, SqlHost.DBConn);
                oDa.Fill(oDs, sTable);
                rDs.DataSetToReturn = oDs;
                return rDs;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

    }
}

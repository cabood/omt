﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UbiSqlFramework
{
    public class ContextDetail
    {
        private string sId;
        private string sName1;
        private string sSettings;
        private bool bBHIntegration;
        private bool bDisplayExtJobDates;
        private bool bDisplayGoodsDetail;

        public string ContextId
        {
            get { return sId; }
            set { sId = value; }
        }
        public string Name1
        {
            get { return sName1; }
            set { sName1 = value; }
        }
        public string Settings
        {
            get { return sSettings; }
            set { sSettings = value; }
        }
        public bool BHIntegration
        {
            get { return bBHIntegration; }
            set { bBHIntegration = value; }
        }
        public bool DisplayExtJobDates
        {
            get { return bDisplayExtJobDates; }
            set { bDisplayExtJobDates = value; }
        }
        public bool DisplayGoodsDetail
        {
            get { return bDisplayGoodsDetail; }
            set { bDisplayGoodsDetail = value; }
        }
    }
}

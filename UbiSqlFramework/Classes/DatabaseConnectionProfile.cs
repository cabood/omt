﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UbiSqlFramework
{
    public class DatabaseConnectionProfile
    {
        private string sId = "";
        private string sName = "";
        private string sServerName = "";
        private string sDatabaseName = "";
        private string sUserId = "";
        private string sPassword = "";
        private string sAdditionalSettings = "";
        private string sReplicationProfileId = "";
        private string sColourCode = "";
        private string sContext = "";
        private string sDbtype = "";
        private bool bSingleContext = true;

        public string Id
        {
            get { return sId; }
            set { sId = value; }
        }
        public string Name
        {
            get { return sName; }
            set { sName = value; }
        }
        public string ServerName
        {
            get { return sServerName; }
            set { sServerName = value; }
        }
        public string DatabaseName
        {
            get { return sDatabaseName; }
            set { sDatabaseName = value; }
        }
        public string UserId
        {
            get { return sUserId; }
            set { sUserId = value; }
        }
        public string Password
        {
            get { return sPassword; }
            set { sPassword = value; }
        }
        public string AdditionalSettings
        {
            get { return sAdditionalSettings; }
            set { sAdditionalSettings = value; }
        }
        public string ReplicationProfileId
        {
            get { return sReplicationProfileId; }
            set { sReplicationProfileId = value; }
        }
        public string ColourCode
        {
            get { return sColourCode; }
            set { sColourCode = value; }
        }
        public string Context
        {
            get { return sContext; }
            set { sContext = value; }
        }
        public bool SingleContext
        {
            get { return bSingleContext; }
            set { bSingleContext = value; }
        }
        public string Dbtype
        {
            get { return sDbtype; }
            set { sDbtype = value; }
        }

        public string GenerateConnectionString()
        {
            string sConnectionString = "";
            try
            {
                sConnectionString = "Data Source=" + sServerName + ";" +
                    "Initial Catalog=" + sDatabaseName + ";" +
                    "User Id=" + sUserId + ";" +
                    "Password=" + sPassword;

                if (sAdditionalSettings.Length > 0)
                {
                    sConnectionString = sConnectionString + ";" + sAdditionalSettings;
                }
                return sConnectionString;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public override string ToString()
        {
            try
            {
                if (sName.Length > 0)
                {
                    return sName;
                }
                else
                {
                    return base.ToString();
                }
            }
            catch (Exception ex)
            {
                return base.ToString();
            }
        }

    }
}

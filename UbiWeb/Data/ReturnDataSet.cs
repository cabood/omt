﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace UbiWeb.Data
{
    public class ReturnDataSet : IUbiWebEntity
    {
        public DataSet DataSetToReturn { get; set; }
    }
}

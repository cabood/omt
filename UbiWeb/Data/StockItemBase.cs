﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UbiWeb.Data
{
    public class StockItemBase : IUbiWebEntity
    {
        //public string StockCode { get; set; }
        //public string Description { get; set; }

    }

    public class StockItemMerge : StockItemBase
    {
        //public string LotId { get; set; }
        //public string SupplierPartNo { get; set; }
    }

    public class StockItemMergeCollection : IUbiWebEntity
    {
        //private List<StockItemMerge> cList = new List<StockItemMerge>();
        //public List<StockItemMerge> StockItems
        //{
        //    get { return cList; }
        //    set { cList = value; }
        //}
    }

    public class StockItemSalesContract : StockItemBase
    {
        //public string LotId { get; set; }
        //public float Weight { get; set; }
        //public string Barcode1 { get; set; }
        //public string Unit { get; set; }
        //public string SupplierPartNo { get; set; }
        //public bool GSTExempt { get; set; }
        //public string CustomerCode { get; set; }
        //public string CustomerBarcode { get; set; }
        //public string CustomerDesc { get; set; }
        //public string CustomerLabelling { get; set; }
        //public string CustomerInstructions { get; set; }
        //public string CustomerTiHiDesc { get; set; }
        //public bool CustExtBool1 { get; set; }
        //public bool CustExtBool2 { get; set; }
        //public string CustExtStr1 { get; set; }
        //public string CustExtStr2 { get; set; }
        //public float CustExtNum1 { get; set; }
        //public float CustExtNum2 { get; set; }
    }

    public class StockItemSalesContractCollection : IUbiWebEntity
    {
        //private List<StockItemSalesContract> cList = new List<StockItemSalesContract>();
        //public List<StockItemSalesContract> StockItems
        //{
        //    get { return cList; }
        //    set { cList = value; }
        //}
    }



    public class StockItemInvoice : StockItemBase
    {
        //public string LotId { get; set; }
        //public float Weight { get; set; }
        //public string Barcode1 { get; set; }
        //public string Unit { get; set; }
        //public string SupplierPartNo { get; set; }
        //public bool GSTExempt { get; set; }
        //public string CustomerCode { get; set; }
        //public string CustomerBarcode { get; set; }
        //public float PrePrice { get; set; }
        //public float TaxRate { get; set; }
        //public bool Fumigation { get; set; }
        //public string CustomerDesc { get; set; }
        //public string CustomerLabelling { get; set; }
        //public string CustomerInstructions { get; set; }
        //public string CustomerTiHiDesc { get; set; }
        //public bool CustExtBool1 { get; set; }
        //public bool CustExtBool2 { get; set; }
        //public string CustExtStr1 { get; set; }
        //public string CustExtStr2 { get; set; }
        //public float CustExtNum1 { get; set; }
        //public float CustExtNum2 { get; set; }
    }
    public class StockItemInvoiceCollection : IUbiWebEntity
    {
        //private List<StockItemInvoice> cList = new List<StockItemInvoice>();
        //public List<StockItemInvoice> StockItems
        //{
        //    get { return cList; }
        //    set { cList = value; }
        //}
    }


    public class StockItemPurchaseOrder : StockItemBase
    {
        //public string LotId { get; set; }
        //public string WhsId { get; set; }
        //public float Weight { get; set; }
        //public string Unit { get; set; }
        //public string CustomsCat { get; set; }
        //public bool Fumigation { get; set; }
        //public string SupplierPartNo { get; set; }
        //public bool GSTExempt { get; set; }
        //public string Barcode1 { get; set; }
        //public string CustomerCode { get; set; }
        //public string CustomerBarcode { get; set; }
        //public string CustomerDesc { get; set; }
        //public string CustomerLabelling { get; set; }
        //public string CustomerInstructions { get; set; }
        //public string CustomerTiHiDesc { get; set; }
        //public bool CustExtBool1 { get; set; }
        //public bool CustExtBool2 { get; set; }
        //public string CustExtStr1 { get; set; }
        //public string CustExtStr2 { get; set; }
        //public float CustExtNum1 { get; set; }
        //public float CustExtNum2 { get; set; }
    }
    public class StockItemPurchaseOrderCollection : IUbiWebEntity
    {
        //private List<StockItemPurchaseOrder> cList = new List<StockItemPurchaseOrder>();
        //public List<StockItemPurchaseOrder> StockItems
        //{
        //    get { return cList; }
        //    set { cList = value; }
        //}
    }




}

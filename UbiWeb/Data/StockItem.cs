﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UbiWeb.Data
{
    //public class StockItem : StockItemBase
    //{
    //    private Stock cStock = new Stock();
    //    private StockLot cStockLot = new StockLot();
    //    private StockWhs cStockWhs = new StockWhs();
    //    private StockLotWhs cStockLotWhs = new StockLotWhs();
    //    public Stock Stock
    //    {
    //        get { return cStock; }
    //        set { cStock = value; }
    //    }
    //    public StockLot StockLot
    //    {
    //        get { return cStockLot; }
    //        set { cStockLot = value; }
    //    }
    //    public StockWhs StockWhs
    //    {
    //        get { return cStockWhs; }
    //        set { cStockWhs = value; }
    //    }
    //    public StockLotWhs StockLotWhs
    //    {
    //        get { return cStockLotWhs; }
    //        set { cStockLotWhs = value; }
    //    }

    //}

    //public class Stock
    //{
    //    public string StockCode { get; set; }
    //    public string Description { get; set; }
    //    public string ClassLevelId { get; set; }
    //    public Single FirstPrice { get; set; }
    //    public string FirstPrcCurrency { get; set; }
    //    public string DefLot { get; set; }
    //    public string DefWhs { get; set; }
    //    public Single StdEstLandedCost { get; set; }
    //    public string Composition { get; set; }
    //    public string Notes { get; set; }
    //    public string DefBarcode { get; set; }
    //    public string AltBarcode { get; set; }
    //    public string LockCode { get; set; }
    //    public bool FumigationReq { get; set; }
    //    public string ShippingGroup { get; set; }
    //    public Single DefBaseSell { get; set; }
    //    public Single DefDutyRate { get; set; }
    //    public string Origin { get; set; }
    //    public string BuyerName { get; set; }
    //    public Single TaxRate { get; set; }
    //    public string SourceType { get; set; }
    //    public string JobNumber { get; set; }
    //    public bool QIC { get; set; }
    //    public bool CE { get; set; }
    //    public string CustomsCat { get; set; }
    //    public string Region { get; set; }
    //    public bool Forecast { get; set; }
    //    public string MarketingBlurb { get; set; }
    //}

    //public class StockLot
    //{
    //    public string StockCode { get; set; }
    //    public string SerialLotNo { get; set; }
    //    public string Description { get; set; }
    //    public string DefWhse { get; set; }
    //    public string DefSupplierId { get; set; }
    //    public string DefSuppPartno { get; set; }
    //    public string Packaging { get; set; }
    //    public Single CartonQty { get; set; }
    //    public Single InnerQty { get; set; }
    //    public string Unit { get; set; }
    //    public Single FirstPrice { get; set; }
    //    public string FirstPrcCurrency { get; set; }
    //    public string Dimensions { get; set; }
    //    public string WeightDesc { get; set; }
    //    public string Colours { get; set; }
    //    public string SpecialLabelling { get; set; }
    //    public string SuppInstructions { get; set; }
    //    public string Barcode1 { get; set; }
    //    public string Barcode2 { get; set; }
    //    public Single BaseSell { get; set; }
    //    public Single Volume { get; set; }
    //    public string Rating { get; set; }
    //    public Single MaxCustOrderQty { get; set; }
    //    public Single LeadTime { get; set; }
    //    public Single MinSoh { get; set; }
    //    public Single MaxSoh { get; set; }
    //    public Single Weight { get; set; }
    //    public Single Width { get; set; }
    //    public Single Height { get; set; }
    //    public Single Depth { get; set; }
    //    public string Notes { get; set; }
    //    public string ManDesc { get; set; }
    //    public string SpareDesc { get; set; }
    //    public DateTime ExpiryDate { get; set; }
    //    public string ExclusiveCustId { get; set; }
    //    public Single MinSuppOrdQty { get; set; }
    //    public Single WeeksOfCover { get; set; }
    //    public Single PrePrice { get; set; }
    //    public string LockCode { get; set; }
    //    public string SizeRange { get; set; }
    //    public string SizeMatrix { get; set; }
    //    public string Warranty { get; set; }
    //    public string AccessoryMatrix { get; set; }
    //    public Single Loadability20GP { get; set; }
    //    public Single Loadability40GP { get; set; }
    //    public Single Loadability40HQ { get; set; }
    //    public string InternalNotes { get; set; }
    //    public Single PalletQty { get; set; }
    //    public string TihiDesc { get; set; }
    //    public string Branding { get; set; }
    //    public string NetWeightDesc { get; set; }
    //    public Single NetWeight { get; set; }
    //    public string GenderCode { get; set; }
    //    public string ComplianceRequirements { get; set; }
    //    public string FactoryName { get; set; }
    //    public Single RoyaltyRate { get; set; }
    //    public Single SuppAltPrc { get; set; }
    //    public string SuppAltPrcCurrency { get; set; }
    //    public string CartonDimensions { get; set; }
    //}

    //public class StockWhs
    //{
    //    public string StockCode { get; set; }
    //    public string WarehouseId { get; set; }
    //    public string DefLot { get; set; }
    //    public Single DutyRate { get; set; }
    //    public bool GSTExempt { get; set; }
    //    public string Restrictions { get; set; }
    //    public string LockCode { get; set; }

    //}

    //public class StockLotWhs
    //{
    //    public string StockCode { get; set; }
    //    public string LotId { get; set; }
    //    public string WarehouseId { get; set; }
    //    public string Location { get; set; }
    //    public string LockCode { get; set; }

    //}
}

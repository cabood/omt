﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UbiWeb.Data
{
    public class ReferenceTables : IUbiWebEntity
    {
        public string RefTableId { get; set; }
        public string Description { get; set; }
        public float DisplayYN { get; set; }
        public string IDLabel { get; set; }
        public string DescLabel { get; set; }
        public string ShortDescLabel { get; set; }
        public string ExtStr1Label { get; set; }
        public string ExtStr2Label { get; set; }
        public string ExtStr3Label { get; set; }
        public string ExtNum1Label { get; set; }
        public string ExtNum2Label { get; set; }
        public string ExtDT1Label { get; set; }
        public string ExtDT2Label { get; set; }
        public string ExtBool1Label { get; set; }
        public string ExtBool2Label { get; set; }
        public string Str1FK { get; set; }
        public string Str2FK { get; set; }
        public string HelpDesc { get; set; }
        public string Notes { get; set; }
        public string ExtendedOptions { get; set; }
    }
}

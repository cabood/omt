﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UbiWeb.Data
{
    public class ReferenceData : IUbiWebEntity
    {
        public string RefTableId { get; set; }
        public string RefId { get; set; }
        public string Description { get; set; }
        public string ShortDesc { get; set; }
        public float DisplayYN { get; set; }
        public string ExtStr1 { get; set; }
        public string ExtStr2 { get; set; }
        public string ExtStr3 { get; set; }
        public float ExtNum1 { get; set; }
        public float ExtNum2 { get; set; }
        public DateTime ExtDT1 { get; set; }
        public DateTime ExtDT2 { get; set; }
        public bool ExtBool1 { get; set; }
        public bool ExtBool2 { get; set; }
        public string Notes { get; set; }
        public int ExtendedOptions { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UbiWeb.Data;


namespace UbiWeb.Services
{
    public class LoginResponse : ResponseBase
    {
        private UserProfile cResponse = new UserProfile();
        public LoginResponse()
        {
        }
        public LoginResponse(bool isSuccessful)
        {
            Success = isSuccessful;
        }
        public LoginResponse(bool isSuccessful, string sError)
        {
            Success = isSuccessful;
            ErrorMessage = sError;
        }
        public UserProfile Response
        {
            get { return cResponse; }
            set { cResponse = value; }
        }
    }
}

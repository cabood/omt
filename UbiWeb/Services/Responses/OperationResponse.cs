﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UbiWeb.Data;


namespace UbiWeb.Services
{
    public class OperationResponse : ResponseBase
    {
        public OperationResponse()
        {
        }
        public OperationResponse(bool isSuccessful)
        {
            Success = isSuccessful;
        }
        public OperationResponse(bool isSuccessful, string sError)
        {
            Success = isSuccessful;
            ErrorMessage = sError;
        }
        public string Response { get; set; }
        public string OperationId { get; set; }
        public Single Sequence { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UbiWeb.Data;


namespace UbiWeb.Services
{
    public class BoolResponse : ResponseBase
    {
        public BoolResponse()
        {
        }
        public BoolResponse(bool isSuccessful)
        {
            Success = isSuccessful;
        }
        public BoolResponse(bool isSuccessful, string sError)
        {
            Success = isSuccessful;
            ErrorMessage = sError;
        }
        public bool Response { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UbiWeb.Data;

namespace UbiWeb.Services
{
    public class ReturnDataSetResponse : ResponseBase
    {
        private ReturnDataSet cResponse = new ReturnDataSet();
        public ReturnDataSetResponse()
        {
        }
        public ReturnDataSetResponse(bool isSuccessful)
        {
            Success = isSuccessful;
        }
        public ReturnDataSetResponse(bool isSuccessful, string sError)
        {
            Success = isSuccessful;
            ErrorMessage = sError;
        }
        public ReturnDataSet Response
        {
            get { return cResponse; }
            set { cResponse = value; }
        }
    }
}

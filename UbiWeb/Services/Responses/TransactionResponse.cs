﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UbiWeb.Data;


namespace UbiWeb.Services
{
    public class TransactionResponse : ResponseBase
    {
        private List<OperationResponse> cList = new List<OperationResponse>();
        public TransactionResponse()
        {
        }
        public TransactionResponse(bool isSuccessful)
        {
            Success = isSuccessful;
        }
        public TransactionResponse(bool isSuccessful, string sError)
        {
            Success = isSuccessful;
            ErrorMessage = sError;
        }
        public string Response { get; set; }
        public string TransactionId { get; set; }

        public int SucessfulOperations { get; set; }
        public int FailedOperations { get; set; }

        public List<OperationResponse> OperationResponses
        {
            get { return cList; }
            set { cList = value; }
        }

    }
}

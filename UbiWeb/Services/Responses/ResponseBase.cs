﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UbiWeb.Data;

namespace UbiWeb.Services
{
    public abstract class ResponseBase
    {
        public string ErrorMessage { get; set; }
        public bool Success { get; set; }

    }
}

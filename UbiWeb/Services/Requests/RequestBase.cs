﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UbiWeb.Services
{
    public abstract class RequestBase
    {
        public RequestBase()
        {
        }

        public RequestBase(string key)
        {
            ValidationKey = key;
        }


        public string ValidationKey { get; set; }

    }

}

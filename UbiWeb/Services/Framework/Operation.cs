﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UbiWeb.Data;

namespace UbiWeb.Services
{
    public enum OperationVerb
    {
        Create,
        Read,
        Update,
        Delete
    }

    public enum OperationEntity
    {
        //CBAJob,
        MasterfilesJob,
        //OEHeader,
        //POHeader,
        //Stock,
        //ReportQuickHeader,
        ReferenceTables,
        ReferenceData
    }

    public interface IOperation
    {
        string OperationId { get; set; }
        Single Sequence { get; set; }
        OperationVerb Verb { get; set; }
        OperationEntity OperationEntity { get; set; }
        List<string> FieldList { get; set; }
    }

    public class Operation : IOperation
    {
        private List<string> cFieldList = new List<string>();
        public string OperationId { get; set; }
        public Single Sequence { get; set; }
        public OperationVerb Verb { get; set; }
        public OperationEntity OperationEntity { get; set; }
        public List<string> FieldList
        {
            get { return cFieldList; }
            set { cFieldList = value; }
        }
    }

    //public class ReportQuickHeaderOperation : Operation
    //{
    //    public ReportQuickHeader c = new ReportQuickHeader();
    //    public ReportQuickHeader DataMember
    //    {
    //        get { return c; }
    //        set { c = value; }
    //    }
    //}

    //public class CBAJobOperation : Operation
    //{
    //    public CBAJob c = new CBAJob();
    //    public CBAJob DataMember
    //    {
    //        get { return c; }
    //        set { c = value; }
    //    }
    //}
    //public class MasterfilesJobOperation : Operation
    //{
    //    public MasterfilesJob c = new MasterfilesJob();
    //    public MasterfilesJob DataMember
    //    {
    //        get { return c; }
    //        set { c = value; }
    //    }
    //}
    //public class OEHeaderOperation : Operation
    //{
    //    public OEHeader c = new OEHeader();
    //    public OEHeader DataMember
    //    {
    //        get { return c; }
    //        set { c = value; }
    //    }
    //}
    //public class POHeaderOperation : Operation
    //{
    //    public POHeader c = new POHeader();
    //    public POHeader DataMember
    //    {
    //        get { return c; }
    //        set { c = value; }
    //    }
    //}
    //public class StockOperation : Operation
    //{
    //    public Stock c = new Stock();
    //    public Stock DataMember
    //    {
    //        get { return c; }
    //        set { c = value; }
    //    }
    //}

    public class ReferenceTablesOperation : Operation
    {
        public ReferenceTables c = new ReferenceTables();
        public ReferenceTables DataMember
        {
            get { return c; }
            set { c = value; }
        }
    }

    public class ReferenceDataOperation : Operation
    {
        public ReferenceData c = new ReferenceData();
        public ReferenceData DataMember
        {
            get { return c; }
            set { c = value; }
        }
    }

}

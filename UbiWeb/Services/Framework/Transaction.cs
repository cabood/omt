﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UbiWeb.Data;
using UbiWeb.Services;

namespace UbiWeb.Services
{
    public class Transaction
    {
        public List<String> sSequence = new List<String>();
        public string TransactionId { get; set; }

        //public List<ReportQuickHeaderOperation> cReportQuickHeaders = new List<ReportQuickHeaderOperation>();
        //public List<CBAJobOperation> cCBAJobs = new List<CBAJobOperation>();
        //public List<MasterfilesJobOperation> cMasterfilesJobs = new List<MasterfilesJobOperation>();
        //public List<OEHeaderOperation> cOEHeaders = new List<OEHeaderOperation>();
        //public List<POHeaderOperation> cPOHeaders = new List<POHeaderOperation>();
        //public List<StockOperation> cStockOperations = new List<StockOperation>();
        public List<ReferenceTablesOperation> cReferenceTables = new List<ReferenceTablesOperation>();
        public List<ReferenceDataOperation> cReferenceData = new List<ReferenceDataOperation>();

        public List<String> OperationSequence
        {
            get { return sSequence; }
            set { sSequence = value; }
        }

        public List<ReferenceTablesOperation> ReferenceTablesOperations
        {
            get { return cReferenceTables; }
            set { cReferenceTables = value; }
        }

        public List<ReferenceDataOperation> ReferenceDataOperations
        {
            get { return cReferenceData; }
            set { cReferenceData = value; }
        }

        //public List<ReportQuickHeaderOperation> ReportQuickHeaderOperations
        //{
        //    get { return cReportQuickHeaders; }
        //    set { cReportQuickHeaders = value; }
        //}
        //public List<CBAJobOperation> CBAJobOperations
        //{
        //    get { return cCBAJobs; }
        //    set { cCBAJobs = value; }
        //}
        //public List<MasterfilesJobOperation> MasterfilesJobOperations
        //{
        //    get { return cMasterfilesJobs; }
        //    set { cMasterfilesJobs = value; }
        //}
        //public List<OEHeaderOperation> OEHeaderOperations
        //{
        //    get { return cOEHeaders; }
        //    set { cOEHeaders = value; }
        //}
        //public List<POHeaderOperation> POHeaderOperations
        //{
        //    get { return cPOHeaders; }
        //    set { cPOHeaders = value; }
        //}
        //public List<StockOperation> StockOperations
        //{
        //    get { return cStockOperations; }
        //    set { cStockOperations = value; }
        //}

    }
}

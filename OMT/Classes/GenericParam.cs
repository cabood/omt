﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OMT
{
    public class GenericParam
    {
        private string sId;
        private string sValue;
        private Dictionary<string, object> cDictionary = new Dictionary<string,object>();
        private List<string> sValueList;
        private DateTime dtValue;
        private long longValue;
        private int intValue;
        private object objValue;
        private Guid guidValue;
        private float floatValue;
        private string sAction;
        private Int32 nInt32Value;
        private string sPrompt;

        public string IdField
        {
            get { return sId; }
            set { sId = value; }
        }

        public string ValueField
        {
            get { return sValue; }
            set { sValue = value; }
        }

        public Dictionary<string, object> DictionaryField
        {
            get { return cDictionary; }
            set { cDictionary = value;
            }
        }

        public List<string> ValueList
        {
            get { return sValueList; }
            set { sValueList = value; }
        }

        public DateTime ValueDate
        {
            get { return dtValue; }
            set { dtValue = value; }
        }

        public long ValueLong
        {
            get { return longValue; }
            set { longValue = value; }
        }

        public int ValueInt
        {
            get { return intValue; }
            set { intValue = value; }
        }

        public object ValueObject
        {
            get { return objValue; }
            set { objValue = value; }
        }

        public Guid ValueGuid
        {
            get { return guidValue; }
            set { guidValue = value; }
        }

        public float ValueFloat
        {
            get { return floatValue; }
            set { floatValue = value; }
        }

        public Int32 ValueInt32
        {
            get { return nInt32Value; }
            set { nInt32Value = value; }
        }

        public string Action
        {
            get { return sAction; }
            set { sAction = value; }
        }

        public string Prompt
        {
            get { return sPrompt; }
            set { sPrompt = value; }
        }

    }
}

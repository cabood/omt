﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace OMT
{
    public class SearchParam
    {
        private string sSql;
        private string[] sReturnKey;
        private string[] sReturnValue;
        private string sReturnObject;
        private ISearchHost cOwner;

        public string Sql
        {
            get { return sSql; }
            set { sSql = value; }
        }

        public string[] ReturnKey
        {
            get { return sReturnKey; }
            set { sReturnKey = value; }
        }

        public string[] ReturnValue
        {
            get { return sReturnValue; }
            set { sReturnValue = value; }
        }

        public string ReturnObject
        {
            get { return sReturnObject; }
            set { sReturnObject = value; }
        }

        public ISearchHost SearchPartner
        {
            get { return cOwner; }
            set { cOwner = value; }
        }
    }
}

/* PFXCLIBN.CS - POWERFlex C Library sample program	                  *\
**                                                                      **
** Copyright (C) 1985-2009 POWERflex Corporation, Melbourne Australia   **
** as an unpublished work.  All rights reserved worldwide.              **
**                                                                      **
** POWERflex is a Registered Trademark of POWERflex Corporation.        **
**                                                                      **
** $Id$                                                                 **
**                                                                      **
\*************************************************************************/
using System;
using System.Text;
using System.Runtime.InteropServices;

namespace PFXClib
{
   /* typedef for general use */
   using aResult     =   System.Int32;
   using aFlag       =   System.UInt32;
   using aDHandle    =   System.Int32;
   using anInt       =   System.Int32;
   using aLong       =   System.Int32;
   using aString     =   System.String;
   using aStringPtr  =   System.Text.StringBuilder;
   using aPointer    =   System.IntPtr;
   using aFileMode   =   System.Int32;
   using aKeyNo      =   System.Int32;
   using aByte       =   System.Byte;
   using aRelop      =   System.Int32;

   /* values used for relational operators */
   public enum erelop
   {
      EQ = 1,
      NE = 2,     /* use only for constraints */
      LT = 3,
      LE = 4,
      GT = 5,
      GE = 6
   }

   /* values used for key numbers */
   public enum ekeyno
   {
      INDEX_RECNUM,
      INDEX_1,
      INDEX_2,
      INDEX_3,
      INDEX_4,
      INDEX_5,
      INDEX_6,
      INDEX_7,
      INDEX_8,
      INDEX_9,
      INDEX_10,
      INDEX_11,
      INDEX_12,
      INDEX_13,
      INDEX_14,
      INDEX_15,
      INDEX_16,
      INDEX_17,
      INDEX_18,
      INDEX_19,
      INDEX_20
   }

   /* internal variable types */
   public enum etype
   {
      TLOGICAL = 1,
      TINTEGER = 2,
      TDATE = 3,
      TNUMBER = 4,
      TREAL = 5,
      TSTRING = 6
   }

   /* external element types */
   public enum eetype
   {
      ETSTRING =  0     /* space filled string (up to 255 byte)            */
     ,ETNUMBER =  1     /* BCD number (1-11 byte, 22 digit)                */
     ,ETDATE =    2     /* Date as BCD number (3 byte)                     */
     ,ETOVERLAP = 3     /* raw binary (up to 255 byte)                     */
     ,ETTEXT =    5     /* variable length text (up to 16382 bytes)        */
     ,ETBINARY =  6     /* variable length binary (up to 16382 bytes)      */
     ,ETRECNUM =  9     /* internal only                                   */
     ,ETINTEGER = 11    /* Integer (Intel format, 1-2-4 byte, Btrieve)     */
     ,ETFLOAT =   12    /* Floating point (IEEE, 8 byte, Btrieve)          */
     ,ETDMY =     13    /* Date as day-month-year (4 byte, Btrieve)        */
     ,ETDECIMAL = 15    /* BCD Number, trailing sign (1-12 byte, Btrieve)  */
     ,ETLOGICAL = 17    /* True/False (1 byte, Btrieve)                    */
     ,ETZSTRING = 18    /* Null-terminated string                          */
     ,ETSIZE =    19
   }

   /*
      FILE_MODE values as defined:
      FMDEFSU is the default for single user and files with reread disabled
      FMDEFMU is the default for multi-user on enabled files 
   */
   public enum efmode
   {
      FMREREAD = 1,      /* REREAD causes re-reading of active data buffers */
      FMLOCK = 2,      /* LOCK and REREAD do lock this file (if open) */
      FMREHDR = 4,      /* reread file/index header on add, delete */
      FMEDIT = 8,      /* allow edit of active buffer (error ERPFEDIT) */
      FMDELETE = 16,     /* allow DELETE  (error ERPFDEL) */
      FMFIND = 32,     /* allow FIND (error ERPFFIND) */
      FMCREATE = 64,     /* allow SAVE to create new record (error ERPFCREA) */
      FMREWRITE = 128,   /* rewrite on every update (safest, even in S/U) */
      FMFDUPGRP = 256,   /* find duplicate keys with differing recno as a group */
      FMJOIN = 512,   /* don't use table joins */
      FMBUFFER = 1024,  /* use internal buffering */
      FMACKDEL = 2048,  /* acknowledge deleted records */
      FMLINKIDX = 4096,  /* link to index file files */
      FMLINKVLD = 8192  /* Link to VLD file */
   }

   /* values used for byte swapped data */
   public enum byteswp
   {
      BO_NATIVE = 0,
      BO_LOHI = 1,
      BO_HILO = 2,
      BO_PDP = 3
   }

   /* error codes - same as PFXplus */
   public enum eerror
   {
      ERNUL = 0,
      ERDISKFULL = 1,	/* Disk Full				     */
      ERINT = 3,	/* Aborted by Operator			     */
      ERSYS = 4,	/* DOS Error				     */
      ERTOOMANY = 7,	/* Too many Locks or Open Files		     */
      ERVLDREAD = 8,	/* Read Error on ".VLD" File		     */
      ERVLDWRITE = 9,	/* Write Error on ".VLD" File		     */
      ERNOMEM = 10,	/* Out of memory			     */
      ERCONFIG = 18,	/* Cannot open configuration setup file      */
      ERMUTIMO = 19,	/* Multi-User Time Out, Busy Too Long	     */
      ERXREAD = 20,	/* Read Error on Index File		     */
      ERXWRITE = 21,	/* Write Error on Index File		     */
      ERXBAD = 22,	/* Index File Damaged			     */
      ERXFULL = 23,	/* Index File Full			     */
      ERXNOFIND = 25,	/* Record Not Found			     */
      ERXDUP = 28,	/* Duplicate Record Not Allowed		     */
      ERNOSORT = 40,	/* PclSortInit has not been called	     */
      ERFINDBOF = 41,	/* Find Past Beginning of File		     */
      ERFINDEOF = 42,	/* Find Past End of File		     */
      ERCONDX = 43,	/* Can't Open Index File		     */
      ERNOBTRV = 47,	/* Btrieve Services not Available	     */
      ERVERLIM = 49,      /* User number exceeds version limit         */
      ERNOLOCK = 51,	/* Cannot Update without LOCK		     */
      ERCODEF = 60,	/* Can't Open ".DEF" File		     */
      ERINVDEF = 61,	/* Invalid ".DEF" file			     */
      ERCOTAG = 62,	/* Can't Open ".TAG" File		     */
      ERPFDEL = 63,	/* Delete Not Permitted in this File	     */
      ERPFFIND = 64,	/* Find Not Permitted in this File	     */
      ERPFCREA = 65,	/* Create Not Permitted in this File	     */
      ERPFEDIT = 66,	/* Editing Not Permitted in this File	     */
      ERBADLOCK = 69,	/* Locking Not Permitted in this File	     */
      ERNODEL = 71,	/* No Record in Memory to Delete	     */
      ERNOTOPEN = 72,	/* File Not Open			     */
      ERELEOOR = 73,	/* Field Number Out of Range		     */
      ERCOFLCFG = 74,	/* Can't Open Files Table		     */
      ERCODAT = 75,	/* Can't Open ".DAT" File		     */
      ERELEDIF = 76,	/* Field Number Out of Range		     */
      ERFILEOOR = 77,	/* File Number Out of Range		     */
      ERRECZERO = 78,	/* Can't Update Record Zero of Data File     */
      ERNOTNDX = 79,	/* Can't Find by This Field: Not Indexed     */
      ERMISLICFIL = 80,	/* Invalid or Missing License File	     */
      ERRECNOOR = 81,	/* Record Number Out of Range		     */
      ERNOSAVE = 82,	/* Edited Record Not Saved		     */
      ERELEOFF = 84,	/* Field Outside Record			     */
      ERCOVLD = 85,      /* Can't Open ".VLD" File		     */
      ERINVEXPREG = 98,	/* Invalid or Expired Registration           */
      ERNOTIMP = 99,	/* Function Not Implemented		     */
      ERTRKCODAT = 1200,	/* Can't Open Tracking File		     */
      ERTRKREAD = 1201,	/* Can't Read Tracking File		     */
      ERTRKWRITE = 1202,	/* Can't Write to Tracking File		     */
      ERTRKTOOLARGE = 1203	/* Tracking Data Too Large for Field	     */
   }

   /* values for transaction state values */
   public enum etrsmode
   {
      TRS_OFF,
      TRS_ON,
      TRS_ABORT
   }

   /* values used for reindex locking */
   public enum erdxl
   {
      RDXL_NONE = 0,
      RDXL_TEMPONLY = 1,
      RDXL_BATCH = 2,
      RDXL_ALWAYS = 3
   }

   /* Reindex Callback function codes					     */
   public enum ecb
   {       /*   purpose			parameters		     */
      CB_START,     /* initial call	     rootname, number of passes      */
      CB_INDEX,	 /* start of each index      rootname, key number	     */
      CB_DATA,      /* after data record read   pass count, TRUE if last	     */
      CB_KEY,	 /* after each key written   pass count, TRUE if last	     */
      CB_DUPLICATE, /* duplicate found	     both record numbers	     */
      CB_STATS,	 /* end of reindex	     mem used, merge order	     */
      CB_FINISH,	 /* end of reindex	     actual count, error code	     */
      CB_MERGEPN	 /* merge pathname	     pathname			     */
   }

   /* magic values for CheckConfig */
   public enum ecft
   {
      CFT_NUL,
      CFT_LONGINT,
      CFT_ASCIIZ,
      CFT_CHAR
   }

   /* Get and Set Configuration */
   public enum ecfg 
   {
       CFG_NULL	     =0  /* Invalid CFG */
      ,CFG_PMWSAFE      =1  /* TRUE to flush file on extend */
      ,CFG_DATAPATH     =2  /* list of paths to search for files */
      ,CFG_TEMPPATH     =3  /* where to create temporaries */
      ,CFG_COLSEQ	     =4  /* collating sequence table */
      ,CFG_UPCOLSEQ     =5  /* uppercase collating sequence table */
      ,CFG_LOCKTOUT     =6  /* seconds before ERMUTIMO default=10 */
      ,CFG_MAXMEM	     =7  /* maximum memory requested during reindex */
      ,CFG_MERGEORD     =8  /* MergeOrder value for reindex */
      ,CFG_MULTIUSER    =9  /* TRUE if MULTIUSER set */
      ,CFG_MILESTONE    =10 /* Milestone value for reindex */
      ,CFG_FLISTPATH    =11 /* Path for Filelist */
      ,CFG_COLORTAB     =12 /* read/write color Table  */
      ,CFG_NUMHANDLES   =13 /* Maximum number of file handles */
      ,CFG_NUMFILES     =14 /* Maximum number of open files */
      ,CFG_FLUSHMETHOD  =15 /* Flushing method */
      ,CFG_BTRPATH      =16 /* Btrieve path */
      ,CFG_REINDEXLOCK  =17 /* Reindex locking level [RDXL_NEVER...RDXL_ALWAYS] */
      ,CFG_FLUSHUNLOCK  =18 /* set flush method on UNLOCK */
      ,CFG_OEMMODE      =19 /* set oem mode (windows only) */
      ,CFG_SPEED	     =20
      ,CFG_VERSION      =21
      ,CFG_PERIOD	     =22
      ,CFG_PRECISION    =23
      ,CFG_BTREXT	     =24 /* Btrieve Extension */
      ,CFG_BTRDLL	     =25 /* Btrieve DLL Name */
      ,CFG_TRACEFLAGS   =26 /* Bit flags to turn on tracing modes */
      ,CFG_DATEXT	     =27 /* .dat file Extension */
      ,CFG_VLDEXT	     =28 /* .vld file Extension */
      ,CFG_FINDLOCKMODE =29 /* Locking mode during finds */
      ,CFG_TRANSMODE    =30 /* Transaction Mode */
      ,CFG_INTNAMEMODE  =31 /* internal name mode for dat ceate */
      ,CFG_GETEXTERRCODE=32 /* get extended error code */
      ,CFG_GETEXTERRMSG =33 /* get extended error message */
      ,CFG_TRACKINFOPATH=34 /* file tracking info path */
      ,CFG_TRACKLOGPATH =35 /* file tracking log path */
      ,CFG_TRACKMODE    =36 /* file tracking mode */
      ,CFG_TRACKDATEFMT =37 /* file tracking date format */
      ,CFG_LOCKREQUIRED =38 /* locking required for changing and deleting records */
      ,CFG_CACHEBUFSIZE =39 /* size of cache buffer */
      ,CFG_TRACESTRING  =40 /* string values to turn on tracing modes (set config) */
      ,CFG_BATCHMODE    =41 /* set batch mode */
      ,CFG_FIELDLISTMODE=42 /* set field list mode */
      ,CFG_OPENORDER    =43 /* order to search drivers during open */
      ,CFG_AUDALERT     =44 /* TRUE if beep on error */
      ,CFG_FILECASEMODE =45 /* forces all paths and filenames to lower case */
      ,CFG_PATHCHAR     =46 /* Character between names in path */
      ,CFG_PATHSEP      =47 /* Character separating paths in list */
      ,CFG_TRACEFLUSHLEVEL=48 /* Controls flushing of trace file */
      ,CFG_TRACEPATH    =49 /* Path for trace file */
      ,CFG_VERSIONSTRING=50 /* Version information */
      /* ,CFG_STDHUFFTABLE =51  // Standard Huffman Table */
      ,CFG_BTRSEGMERGE  =52 /* Enable Index segment merging (for Btrieve) */
      ,CFG_FLUSHOPEN    =53 /* Flush file on Open, Win32 Oplocks workaround */
      ,CFG_BTRPAGEOFFSET=54 /* Page Offset (for Btrieve 7) */
   }

   /* item values for PclGet/SetInfo */
   public enum einfo
   {
      /*    enum			description		   size  Mk1   Mk2+  */
      /*							 (bytes)   offsets   */
      PCH_HIGHWATER = 1,  /* high water mark		    4     0      0   */
      PCH_DELETEDCHAIN = 2,  /* deleted rec chain	    4	  4	 4   */
      PCH_RECORDCOUNT = 3,  /* records			    4	  8	 8   */
      PCH_MAXRECORD = 4,  /* max records		    4	 12	12   */
      PCH_VERSION = 5,  /* version			    1	 28	28   */
      PCH_VERSION2 = 6,  /* version byte 2		    1	 29	29   */
      PCH_PROTECTION = 7,  /* protection		    1	 30	30   */
      PCH_COMPRESSION = 8,  /* compression		    1	 31	31   */
      PCH_PAGESIZE = 9,  /* page size		    2	 32	52   */
      PCH_PREVFILETYPE = 10,  /* prev file type		    1	 42	65   */
      PCH_BYTEORDER = 11,  /* dat/vld byte order	    1    97    173   */
      PCH_BFLAGS = 12,  /* vld/btrieve ext flags	    1    xx    161   */
      PCH_REUSE = 13,  /* re-use deleted recs	    1    88    164   */
      PCH_RECLEN = 14,  /* Record Length		    2    78    154   */
      PCH_REREAD = 15,  /* multi-user re-read	    1    92    158   */
      PCH_PATHNAME = 201,  /* fullname of open file       SIZEPATH+1       */
      PCH_INTNAME = 202,  /* internal name of open file  9		     */
      PCH_FAMODE = 301,  /* file access mode	    4		     */
      PCH_KEYNO = 302,  /* index for last find	    4		     */
      PCH_REGNO = 801,  /* registration no		    4    80    148   */
      PCH_RECRIGHTS = 802,  /* record rights		    2	 98	72   */
      PCH_FILERIGHTS = 803,  /* file rights		    2	 95	70   */
      PCH_RESERVED1 = 804,  /* reserved 1		    4	 34	56   */
      PCH_RESERVED2 = 805,  /* reserved 2		    2	 38	60   */
      PCH_RESERVED3 = 806,  /* reserved 3		    2	 40	62   */
      PCH_IDXRIGHTS = 1000, /* index number rights	    1	100    176   */
      /*    10nn   index number nn 0<nn<=(10|16) 1 +8*n-7 +34*n-33*/
      PCH_FLDRIGHTS = 2000  /* field number rights	    1   196    736   */
      /*    2nnn	field number nnn 0<nnn<256  1 +8*n-7  +8*n-7 */
   }

   /* Symbolic constants for the PclPathAccess() function */
   public enum eaccess
   {
      AC_EXISTS = 0,	/* F_OK - Test for existence of file */
      AC_EXECUTE = 1,	/* X_OK - Test for execute permission */
      AC_CANWRITE = 2,	/* W_OK - Test for write permission */
      AC_CANREAD = 4	/* R_OK - Test for read permission */
   }

   /* Symbolic constants for PclFieldClear function */
   public enum efclr
   {
      FCLR_LOWVAL = -1,	/* Set Field to Low Values */
      FCLR_DEFAULT = 0,	/* Set Field to Default (Clear) Values */
      FCLR_HIVAL = 1	/* Set Field to High Values */
   }

   /* To be used by PclReindex */
   public delegate aResult aReindexCallback(anInt funcno, aLong par1, aLong par2);

   public class pfxclibn
   {
      public const Int32 OK               = 0;
      public const Int32 ERROR            = -1;
      public const Int32 MAXINILEN        = 4096;
      public const Int32 SIZEPATH         = 256;
      public const Int32 SIZEFILE         = 64;
      public const Int32 SIZEEXT          = 32;

      public const Int32 MAXINDEX         = 20;           /* maximum number of indexes (1..) */
      public const Int32 MAXSEGS          = 16;           /* maximum number of index segments (1..) */
      public const Int32 MAXMAININDEX     = 15;           /* maximum value for main index */
      public const Int32 MAXLEVEL         = 16;           /* maximum index level */
      public const Int32 MAXRECLEN        = 16384;        /* maximum length of database record */
      public const Int32 MAXRECNO         = 0x7ffffff0;   /* maximum number of records in database file */
      public const Int32 MAX3RECNO        = 0xffffff;     /* -"- with 3-byte index */
      public const Int32 MAXFIELD         = 255;          /* maximum field number in file (1..) */
      public const Int32 SIZECHARTABLE    = 256;          /* size of a character table */
      public const Int32 SIZECOLORTABLE   = 256;          /* size of a color map table */
      public const Int32 MAXCOMP32SIZE    = 8000;         /* maximum size of type 32 compression table */

      /* definitions for key flags */
      public const Int32 XF_DOWN          = 1;
      public const Int32 XF_UPPER         = 2;

      /* flags used in getting/setting file size */
      public const Int32 DFG_REREAD       = 1;           /* multi-user reread is TRUE */
      public const Int32 DFG_REUSE        = 2;           /* re-use deleted record */
      public const Int32 DFG_SETFLAGS     = 0x8000;      /* set all flags to zero */

      /* additional flags used in getting/setting file size  - Btrieve Only*/
      public const Int32 DFG_RECNUM       = 4;           /* Btrieve Recnums in use if TRUE */
      public const Int32 DFG_CASEIND      = 8;           /* Case Independent Indexes in use if TRUE */
      public const Int32 DFG_VARFILE      = 16;          /* Variable length Btrieve File in use if TRUE */
      public const Int32 DFG_KEEP         = 32;          /* Keep Btrieve File (if it exist) if TRUE */

      /* File Version numbers and version flags for creating files */
      public const Int32 FV_MARK_1 = 1;         /* mark 1 - originalDAT file: 10 indexes */
      public const Int32 FV_MARK_2 = 2;         /* mark 2 - new DAT file: 16 indexes, 16 segments */
      public const Int32 FV_MARK_3 = 3;         /* mark 3 - VLD file: data compression */
      public const Int32 FV_MARK_4 = 4;         /* mark 4 - Btrieve */
      public const Int32 FV_MARK_5 = 5;         /* mark 5 - SQL/ODBC */ 

      public const Int32 FV_MASK = 15;          /* Mask to pick out mark number from compression */
      public const Int32 FV_BS_MASK = 0x0300;   /* mask to pick out byte swap type on creation */
      public const Int32 FV_CR_MASK = 0xf400;   /* mask to pick out flags on creation */

      public const Int32 FV_RLE = 16;           /* use Run Length Encoding (mark 3 only) */
                                                /* use Btrieve Compression (mark 4 only) */
      public const Int32 FV_HUFFMAN = 32;       /* use Huffman compression (mark 3 only) */
      public const Int32 FV_COMPTABIF = 64;     /* Huffman compression table stored in VLD file */

      public const Int32 FV_SQLB = 16;          /* use Mark 5 B (mark 5 only) */
      public const Int32 FV_SQLA = 32;          /* use Mark 5 A (mark 5 only) */

      public const Int32 FV_MARK_3_RLE	= (FV_MARK_3|FV_RLE);
      public const Int32 FV_MARK_3_HUFF =	(FV_MARK_3|FV_RLE|FV_HUFFMAN);
      public const Int32 FV_MARK_3_HTIF =	(FV_MARK_3|FV_RLE|FV_HUFFMAN|FV_COMPTABIF);
      public const Int32 FV_MARK_4_COMP = (FV_MARK_4 | FV_RLE);

      public const Int32 IND_ONLINE =   0x0000;	 /* on-line index */
      public const Int32 IND_BATCH =    0x0001;	 /* batch index */
      public const Int32 IND_UNIQUE =   0x0002;	 /* indexes unique data only */

      /* entry points */
      [DllImport("pfxlibn.dll", EntryPoint = "PclInit", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern void PclInit();
      [DllImport("pfxlibn.dll", EntryPoint = "PclRegister", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclRegister(String regname, String regcode);
      [DllImport("pfxlibn.dll", EntryPoint = "PclReadIni", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclReadIni(String configname, String section);
      [DllImport("pfxlibn.dll", EntryPoint = "PclAllocFile", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aDHandle PclAllocFile(anInt filno);
      [DllImport("pfxlibn.dll", EntryPoint = "PclOpen", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclOpen(aDHandle fdh, String name, anInt index);
      [DllImport("pfxlibn.dll", EntryPoint = "PclClose", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclClose(aDHandle fdh);
      [DllImport("pfxlibn.dll", EntryPoint = "PclFreeFile", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclFreeFile(aDHandle fdh);
      [DllImport("pfxlibn.dll", EntryPoint = "PclFileDef", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclFileDef(aDHandle fdh, out anInt ftype, out anInt cursiz,
                                                       out anInt maxsiz, out anInt nfld,
                                                       out anInt reclen, out aFlag flags,
                                                       IntPtr comptable);
      [DllImport("pfxlibn.dll", EntryPoint = "PclFileSet", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclFileSet(aDHandle fdh, aLong size, anInt nfld, aFlag flags);
      [DllImport("pfxlibn.dll", EntryPoint = "PclFieldDef", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclFieldDef(aDHandle fdh, anInt eno, out eetype typ, out anInt len,
                                                                   out anInt pos, out anInt dpl,
                                                                   out anInt idx, out anInt rfil,
                                                                   out anInt rele);
      [DllImport("pfxlibn.dll", EntryPoint = "PclFieldSet", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclFieldSet(aDHandle fdh, anInt eno, eetype typ, anInt len,
                                                                   anInt pos, anInt dpl,
                                                                   anInt idx, anInt rfil,
                                                                   anInt rele);
      [DllImport("pfxlibn.dll", EntryPoint = "PclIndexDef", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclIndexDef(aDHandle fdh, aKeyNo kno, out aFlag bat, out anInt nseg,
                                                                   anInt[] segs, anInt[] flags);
      [DllImport("pfxlibn.dll", EntryPoint = "PclIndexSet", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclIndexSet(aDHandle fdh, aKeyNo kno, aFlag bat, aFlag update, 
                                               anInt nseg, anInt[] segs, anInt[] flags);
      [DllImport("pfxlibn.dll", EntryPoint = "PclClear", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclClear(aDHandle fdh);
      [DllImport("pfxlibn.dll", EntryPoint = "PclFind", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclFind(aDHandle fdh, erelop relop, ekeyno kno);
      [DllImport("pfxlibn.dll", EntryPoint = "PclGetFieldRaw", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclGetFieldRaw(aDHandle fdh, anInt eno, anInt offset, anInt length, 
                                                  IntPtr data);
      [DllImport("pfxlibn.dll", EntryPoint = "PclIsFound", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern Boolean PclIsFound(aDHandle fdh);
      [DllImport("pfxlibn.dll", EntryPoint = "PclGetField", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclGetField(aDHandle fdh, anInt eno, aStringPtr data);
      /* PclSetConfig overloaded */
      [DllImport("pfxlibn.dll", EntryPoint = "PclSetConfig", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclSetConfig(ecfg cfgitem, anInt data);
      [DllImport("pfxlibn.dll", EntryPoint = "PclSetConfig", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclSetConfig(ecfg cfgitem, String data);
      [DllImport("pfxlibn.dll", EntryPoint = "PclSetConfig", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclSetConfig(ecfg cfgitem, Byte[] data);
      /* PclGetConfig overloaded */
      [DllImport("pfxlibn.dll", EntryPoint = "PclGetConfig", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclGetConfig(ecfg cfgitem, out anInt data);
      [DllImport("pfxlibn.dll", EntryPoint = "PclGetConfig", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclGetConfig(ecfg cfgitem, StringBuilder data);
      [DllImport("pfxlibn.dll", EntryPoint = "PclGetConfig", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclGetConfig(ecfg cfgitem, Byte[] data);
      [DllImport("pfxlibn.dll", EntryPoint = "PclAdd", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclAdd(aDHandle fdh);
      [DllImport("pfxlibn.dll", EntryPoint = "PclCalcOffset", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclCalcOffset(aDHandle fdh, anInt rno, out anInt offset);
      [DllImport("pfxlibn.dll", EntryPoint = "PclChange", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclChange(aDHandle fdh);
      [DllImport("pfxlibn.dll", EntryPoint = "PclCheckConfig", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclCheckConfig(ecfg cfgitem, out ecft type, out anInt len);
      [DllImport("pfxlibn.dll", EntryPoint = "PclCreateFile", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclCreateFile(aString name, anInt filver, anInt nfield, anInt reclen, 
                                                 aPointer comptable);
      [DllImport("pfxlibn.dll", EntryPoint = "PclDelete", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclDelete(aDHandle fdh);
      [DllImport("pfxlibn.dll", EntryPoint = "PclEncrypt", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern void PclEncrypt(aString source, aString key, aString dest);
      [DllImport("pfxlibn.dll", EntryPoint = "PclExit", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern void PclExit();
      [DllImport("pfxlibn.dll", EntryPoint = "PclFlush", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclFlush(aDHandle fdh);
      [DllImport("pfxlibn.dll", EntryPoint = "PclFreeHandle", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclFreeHandle();
      [DllImport("pfxlibn.dll", EntryPoint = "PclGetFieldLen", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclGetFieldLen(aDHandle fdh, anInt item, out anInt length);
      [DllImport("pfxlibn.dll", EntryPoint = "PclGetFieldName", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclGetFieldName(aDHandle fdh, anInt eno, aStringPtr name);
      [DllImport("pfxlibn.dll", EntryPoint = "PclGetFileHandle", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern anInt PclGetFileHandle(aDHandle fdh);
      [DllImport("pfxlibn.dll", EntryPoint = "PclGetInfo", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclGetInfo(aDHandle fdh, einfo item, aPointer data);
      [DllImport("pfxlibn.dll", EntryPoint = "PclGetMode", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aFileMode PclGetMode(aDHandle fdh);
      [DllImport("pfxlibn.dll", EntryPoint = "PclGetRecPtr", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aPointer PclGetRecPtr(aDHandle fdh);
      [DllImport("pfxlibn.dll", EntryPoint = "PclIsChanged", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aFlag PclIsChanged(aDHandle fdh);
      [DllImport("pfxlibn.dll", EntryPoint = "PclIsOpen", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aFlag PclIsOpen(aDHandle fdh);
      [DllImport("pfxlibn.dll", EntryPoint = "PclLock", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclLock(aDHandle fdh);
      [DllImport("pfxlibn.dll", EntryPoint = "PclOpenPseudo", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclOpenPseudo(aDHandle fdh, anInt reclen, aFlag flags);
      [DllImport("pfxlibn.dll", EntryPoint = "PclPathAccess", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclPathAccess(aString name, eaccess mode, aStringPtr fullpath);
      [DllImport("pfxlibn.dll", EntryPoint = "PclPathPack", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern void PclPathPack(aString dir, aString file, aString ext, aStringPtr path);
      [DllImport("pfxlibn.dll", EntryPoint = "PclPathUnpack", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern anInt PclPathUnpack(aString path, aStringPtr dir, aStringPtr file, aStringPtr ext);
      [DllImport("pfxlibn.dll", EntryPoint = "PclPurge", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclPurge(aDHandle fdh);
      [DllImport("pfxlibn.dll", EntryPoint = "PclPtrGetField", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclPtrGetField(aDHandle fdh, aPointer recbuff, anInt eno, aStringPtr data);
      [DllImport("pfxlibn.dll", EntryPoint = "PclPtrGetFieldLen", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclPtrGetFieldLen(aDHandle fdh, aPointer recbuff, anInt eno, out anInt len);
      [DllImport("pfxlibn.dll", EntryPoint = "PclPtrGetFieldRaw", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclPtrGetFieldRaw(aDHandle fdh, aPointer recbuff, anInt eno, 
                                                     anInt offset, anInt length, aPointer data);
      [DllImport("pfxlibn.dll", EntryPoint = "PclPutField", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclPutField(aDHandle fdh, anInt eno, aString data);
      [DllImport("pfxlibn.dll", EntryPoint = "PclPutFieldRaw", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclPutFieldRaw(aDHandle fdh, anInt eno,
                                                  anInt offset, anInt length, aPointer data);
      [DllImport("pfxlibn.dll", EntryPoint = "PclRelate", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclRelate(aDHandle fdh, anInt eno, aDHandle fromfdh, anInt fromeno,
                                             aDHandle mastfdh);
      [DllImport("pfxlibn.dll", EntryPoint = "PclReread", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclReread(aDHandle fdh);
      [DllImport("pfxlibn.dll", EntryPoint = "PclReindex", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclReindex(aDHandle fdh, aLong kmask, aReindexCallback callback);
      [DllImport("pfxlibn.dll", EntryPoint = "PclSetActive", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclSetActive(aDHandle fdh, aFlag active);
      [DllImport("pfxlibn.dll", EntryPoint = "PclSetChanged", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclSetChanged(aDHandle fdh);
      [DllImport("pfxlibn.dll", EntryPoint = "PclSetFieldList", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclSetFieldList(aDHandle fdh, anInt nfld, anInt[] flds);
      [DllImport("pfxlibn.dll", EntryPoint = "PclSetFieldName", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclSetFieldName(aDHandle fdh, anInt eno, aString name);
      [DllImport("pfxlibn.dll", EntryPoint = "PclSetFound", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclSetFound(aDHandle fdh);
      [DllImport("pfxlibn.dll", EntryPoint = "PclSetInfo", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclSetInfo(aDHandle fdh, anInt item, aPointer data);
      [DllImport("pfxlibn.dll", EntryPoint = "PclSetMode", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclSetMode(aDHandle fdh, aFileMode mode);
      [DllImport("pfxlibn.dll", EntryPoint = "PclSetSize", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclSetSize(aDHandle fdh, aLong size, aFlag flags);
      [DllImport("pfxlibn.dll", EntryPoint = "PclSetTransState", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclSetTransState(etrsmode state);
      [DllImport("pfxlibn.dll", EntryPoint = "PclSortAddKey", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclSortAddKey(aDHandle fdh, aKeyNo kno);
      [DllImport("pfxlibn.dll", EntryPoint = "PclSortBuild", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclSortBuild(aDHandle fdh, aKeyNo kno, aReindexCallback callback);
      [DllImport("pfxlibn.dll", EntryPoint = "PclSortExit", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclSortExit(aDHandle fdh, aLong pos);
      [DllImport("pfxlibn.dll", EntryPoint = "PclSortInit", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclSortInit(aDHandle fdh, aKeyNo kno, aLong maxmen);
      [DllImport("pfxlibn.dll", EntryPoint = "PclUnlock", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclUnlock(aDHandle fdh);
      [DllImport("pfxlibn.dll", EntryPoint = "PclTruncate", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern anInt PclTruncate(aDHandle fdh, aLong pos);

      /* Btrieve support - Mark 4 - not supported in all versions */
      [DllImport("pfxlibn.dll", EntryPoint = "PclGetExtErr", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclGetExtErr();
      /* overloaded PclBtrieve */
      [DllImport("pfxlibn.dll", EntryPoint = "PclBtrieve", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclBtrieve(aDHandle fdh, anInt opcode, Byte[] posblk, Byte[] databuf,
                                              out anInt datalen, Byte[] keybuff, anInt keylen, anInt keynum);
      [DllImport("pfxlibn.dll", EntryPoint = "PclBtrieve", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclBtrieve(aDHandle fdh, anInt opcode, Byte[] posblk, Byte[] databuf,
                                              aPointer datalen, Byte[] keybuff, anInt keylen, anInt keynum);

      /* Constraint support - not supported in all versions */
      [DllImport("pfxlibn.dll", EntryPoint = "PclSetConstraint", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclSetConstraint(aDHandle fdh, anInt opcode, anInt eno, aRelop relop,
                                                    aString data);

      /* Advanced data/key management calls */
      [DllImport("pfxlibn.dll", EntryPoint = "PclBuildKey", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclBuildKey(aDHandle fdh, aKeyNo kno, out anInt klen, aPointer kdata);
      [DllImport("pfxlibn.dll", EntryPoint = "PclFieldClear", CharSet = CharSet.Ansi, ExactSpelling = true)]
      public static extern aResult PclFieldClear(aDHandle fdh, anInt eno, efclr sw);
   }
}

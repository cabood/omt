﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OMT
{
    public class DateParam
    {
        private string sId = "";
        private DateTime dtVal;
        private bool bIsSet;
        private string sSourceControl = "";
        private string sAction = "";
        public string Id
        {
            get { return sId; }
            set { sId = value; }
        }
        public DateTime DtValue
        {
            get { return dtVal; }
            set { dtVal = value; }
        }
        public bool IsSet
        {
            get { return bIsSet; }
            set { bIsSet = value; }
        }
        public string SourceControl
        {
            get { return sSourceControl; }
            set { sSourceControl = value; }
        }
        public string Action
        {
            get { return sAction; }
            set { sAction = value; }
        }
    }

}

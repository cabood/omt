﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace OMT  
{
    public class AppUpdateSource
    {
        public string SourceId { get; set; }
        public string Description { get; set; }
        public string UpdateType { get; set; }
        public string Source { get; set; }
        public bool AutoCheck { get; set; }


        //    <UpdateSource id="UBALocal" description="network" type="" source="\\ubanas1\public\nas-store2\pc software\uba\masterfiles\autoupdate\" auto="0" />

        public bool PopulateFromNode(XmlNode n)
        {
            try
            {
                XmlElement e = (XmlElement)n;
                SourceId = e.GetAttribute("id");
                Description = e.GetAttribute("description");
                UpdateType = e.GetAttribute("type");
                Source = e.GetAttribute("source");
                string sVal = e.GetAttribute("source");
                if (sVal == "1")
                {
                    AutoCheck = true;
                }
                else
                {
                    AutoCheck = false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public AppUpdateSource (XmlNode n)
        {
            PopulateFromNode(n);
        }

        public override string ToString()
        {
            return Description;
        }
    }
}

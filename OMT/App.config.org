﻿<?xml version="1.0" encoding="utf-8" ?>
<configuration>
    <startup> 
        <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.7.2" />
    </startup>
    <system.serviceModel>
        <bindings>
            <basicHttpBinding>
                <binding name="MFServicesSoap" maxBufferPoolSize="2147483647"
                    maxReceivedMessageSize="2147483647" />
            </basicHttpBinding>
            <customBinding>
                <binding name="MFServicesSoap12">
                    <textMessageEncoding messageVersion="Soap12" />
                    <httpTransport maxReceivedMessageSize="2147483647"
                                   maxBufferSize="2147483647"
                                   maxBufferPoolSize="2147483647"/>
                </binding>
            </customBinding>
        </bindings>
        <client>
            <!--<endpoint address="http://localhost:53792/MFServices.asmx" binding="basicHttpBinding"
                bindingConfiguration="MFServicesSoap" contract="MFServices.MFServicesSoap"
                name="MFServicesSoap" />-->
            <endpoint address="http://localhost:53792/MFServices.asmx" binding="customBinding"
                bindingConfiguration="MFServicesSoap12" contract="MFServices.MFServicesSoap"
                name="MFServicesSoap12" />
        </client>
    </system.serviceModel>
  <system.web>
    <compilation debug="true" />
  </system.web>
</configuration>
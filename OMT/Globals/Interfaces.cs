﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace OMT
{
    public interface ISearchHost
    {
        void SearchTransfer(SearchParam sParam);
    }

    public interface IGenericExchange
    {
        void GenericTransfer(GenericParam gParam);
    }

    public interface IWorkbench
    {
        //void RefreshData();
        //void RefreshItemData(Guid g);
    }
    public interface IQuickFind
    {
        //void QuickFind(SearchParam sParam);
    }

    public interface IDateExchange
    {
        bool AcceptDate(DateParam dParam);
    }

    public interface IJobListExchange
    {
        //bool AcceptJobList(JobListParam param);
    }

    public interface INoteExchange
    {
        //bool AcceptNote(NoteParam np);
    }

    public interface IAddressExchange
    {
        //bool AcceptAddress(AddressParam p);
    }
    
    //public enum GridAction { Insert, Update, Delete, None }




}
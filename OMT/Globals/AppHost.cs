﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using System.IO;
using PFXClib;
//using Excel = Microsoft.Office.Interop.Excel;
//using Outlook = Microsoft.Office.Interop.Outlook;
using System.Runtime.InteropServices;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Reflection;
using System.Diagnostics;
using System.Collections;
using Microsoft.Win32;
using UbiSqlFramework;
using UbiWeb.Security;
using OMT.MFServices;


namespace OMT
{
    public static class AppHost
    {
        private static XmlDocument xmlSettings;
        private static string sAppPath;
        private static DataRow cUser;
        private static ContextDetail cCurrentContext;
        private static string sCbaPath;
        private static bool bPfxLoaded;

        private static bool bSingleContext = true;
        private static DateTime dtArchiveDate;
        //private static DateTime dtMinDate;

        //private static DataTable dataCustTerms;
        //private static DataTable dataJobType;
        //private static DataTable dataShipmentType;
        //private static DataTable dataJobStatus;
        //private static DataTable dataConsignee;
        //private static DataTable dataInvoiceAgent;
        //private static DataTable dataPorts;
        //private static DataTable dataDepositStatus;
        //private static DataTable dataFCL;
        //private static DataTable dataShippingLine;
        //private static DataTable dataCurrency;
        //private static DataTable dataCountry;
        //private static DataTable dataCustPayTerms;
        //private static DataTable dataUsers;
        //private static DataTable dataCustomerDocsSent;
        //private static DataTable dataInvInMyob;
        //private static DataTable dataMyobVersion;
        //private static DataTable dataDocsToCustoms;
        //private static DataTable dataDutyStatus;
        //private static DataTable dataSourceType;
        //private static DataTable dataNoteType;
        //private static DataTable dataTaskType;
        //private static DataTable dataJobTaskStatus;
        //private static DataTable dataJobTaskPriority;
        //private static DataTable dataGoodsDesc;
        //private static DataTable dataLogsPriority;
        //private static DataTable dataReportOperator;
        //private static DataTable dataReportDataTypes;
        //private static DataTable dataQueryParamType;
        //private static DataTable dataReportExclusion;
        private static DataTable dataEntityType;
        //private static DataTable dataImageType;
        private static DataTable dataContext;
        //private static DataTable dataBuyingTeam;
        private static DataTable dataAppControl;
        private static DataTable dataUOMConversions;

        //private static IWorkbench currentWorkbench;
        private static String sWebSessionKey;


        public static bool InitApp()
        {
            try
            {
                if (!LoadSettings())
                    return false;
                if (!SqlHost.LoadSettings())
                    return false;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static void DestructApp()
        {
            try
            {
                if (bPfxLoaded)
                {
                    pfxclibn.PclExit();
                }
                if (SqlHost.IsConnected)
                {
                    SqlHost.ShutDown();
                }
                return;
            }
            catch (Exception ex)
            {
                return;
            }
        }
        private static bool LoadSettings()
        {
            try
            {
                sAppPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                string sFile = sAppPath + "\\WBConfig.xml";
                xmlSettings = new XmlDocument();
                xmlSettings.Load(sFile);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("XML settings failed to load!", "Application Error");
                return false;
            }
        }

        //private static bool LoginUser()
        //{
        //    try
        //    {
        //        // TODO: Login functionality
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        public static bool LoadGlobals()
        {
            if (!LoadReferenceData())
            {
                MessageBox.Show("Global Reference Data failed to load.", "Load reference Data Error");
                return false;
            }
            if (!LoadConstants())
            {
                MessageBox.Show("Could not load XML Constants.", "Load Constants Error");
                return false;
            }
            if (!Constants.WORKBENCH_CBA_USEWEBSERVICES && !Constants.WORKBENCH_SYSCTR_SQLSERVER)
            {
                if (!PFXInit())
                {
                    MessageBox.Show("PFX Libraries failed to load! You will not be able to generate new Shipment or Order numbers.", "Load PFX libraries Error");
                }
            }
            return true;
        }

       public static bool LoadReferenceData()
       {
            string sBaseSql;
            string sFilter;
            string sSql;
            DataSet refDs;
            //    DataSet refShippingLine;
            //    DataSet refUsers;
            //    DataSet refAppControl;
            DataSet refContext;
            //    DataSet refSourceType;
            //    DataSet refImageTypes;
            bool b;
            try
            {
                // Context/Companies
                sBaseSql = "select CONTEXT_ID, NAME_1, SETTINGS, BH_INTEGRATION, DISPLAY_EXT_JOB_DATES, DISPLAY_GOODS_DETAILS from CONTEXT";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sBaseSql, "Context", ref refContext);
                dataContext = new DataTable();
                dataContext = refContext.Tables["Context"];

                // MDS XXXX - Create Current ContextDetail Class Object
                string sContextId = SqlHost.CurrentConnectionProfile.Context;
                DataRow[] x = dataContext.Select("CONTEXT_ID = '" + sContextId + "'");
                cCurrentContext = new ContextDetail();
                DataRow y = x[0];
                cCurrentContext.ContextId = y["CONTEXT_ID"].ToString();
                cCurrentContext.Name1 = y["NAME_1"].ToString();
                cCurrentContext.Settings = y["SETTINGS"].ToString();
                if (y["BH_INTEGRATION"] != System.DBNull.Value)
                {
                    cCurrentContext.BHIntegration = (bool)y["BH_INTEGRATION"];
                }
                else
                {
                    cCurrentContext.BHIntegration = false;
                }
                if (y["DISPLAY_EXT_JOB_DATES"] != System.DBNull.Value)
                {
                    cCurrentContext.DisplayExtJobDates = (bool)y["DISPLAY_EXT_JOB_DATES"];
                }
                else
                {
                    cCurrentContext.DisplayExtJobDates = false;
                }
                if (y["DISPLAY_GOODS_DETAILS"] != System.DBNull.Value)
                {
                    cCurrentContext.DisplayGoodsDetail = (bool)y["DISPLAY_GOODS_DETAILS"];
                }
                else
                {
                    cCurrentContext.DisplayGoodsDetail = false;
                }

                //        // Application Conrol Settings (added 20190403)
                //        sBaseSql = "select control_id, control_value, control_xml from app_control order by control_id";
                //        refAppControl = new DataSet();
                //        b = SqlHost.GetDataSet(sBaseSql, "AppControl", ref refAppControl);
                //        dataAppControl = new DataTable();
                //        dataAppControl = refAppControl.Tables["AppControl"];

                //        // Users
                //        sBaseSql = "select user_name, pref_name from users order by active desc, pref_name";
                //        refUsers = new DataSet();
                //        b = SqlHost.GetDataSet(sBaseSql, "Users", ref refUsers);
                //        dataUsers = new DataTable();
                //        dataUsers = refUsers.Tables["Users"];

                //        // Source Type
                //        sBaseSql = "select ref_id, description, short_desc, ext_str_1, ext_str_2 from ref_data where ref_table_id = 'SOURCETYPE'";
                //        refSourceType = new DataSet();
                //        b = SqlHost.GetDataSet(sBaseSql, "SourceType", ref refSourceType);
                //        dataSourceType = new DataTable();
                //        dataSourceType = refSourceType.Tables["SourceType"];

                //        // ShippingLine
                //        sBaseSql = "select ref_id, short_desc from ref_data where ref_table_id = 'SHIPLINE' order by short_desc";
                //        refShippingLine = new DataSet();
                //        b = SqlHost.GetDataSet(sBaseSql, "ShippingLine", ref refShippingLine);
                //        dataShippingLine = new DataTable();
                //        dataShippingLine = refShippingLine.Tables["ShippingLine"];

                //        // Image Types
                //        sBaseSql = "select ref_id, description, ext_bool_1 from ref_data where ref_table_id = 'IMAGETYPE'";
                //        refImageTypes = new DataSet();
                //        b = SqlHost.GetDataSet(sBaseSql, "ImageTypes", ref refImageTypes);
                //        dataImageType = new DataTable();
                //        dataImageType = refImageTypes.Tables["ImageTypes"];

                // Set base retrievals
                sBaseSql = "select ref_id, description, short_desc, ext_str_1, ext_str_2, ext_str_3, ext_num_1, ext_num_2, ext_dt_1, ext_dt_2, ext_bool_1, ext_bool_2, notes from ref_data where ref_table_id = '";
                refDs = new DataSet();

                //        // Job Status
                //        sFilter = "JOBSTATUS' and display_yn = 1 order by cast(ref_id as int)";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "JobStatus", ref refDs);
                //        dataJobStatus = new DataTable();
                //        dataJobStatus = refDs.Tables["JobStatus"];

                //        // Job Type
                //        sFilter = "JOBTYPE' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "JobType", ref refDs);
                //        dataJobType = new DataTable();
                //        dataJobType = refDs.Tables["JobType"];

                //        // Shipment Type Status
                //        sFilter = "SHIPTYPE' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "ShipmentType", ref refDs);
                //        dataShipmentType = new DataTable();
                //        dataShipmentType = refDs.Tables["ShipmentType"];

                //        // Job Cust Terms
                //        sFilter = "CUSTTERMS' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "JobCustTerms", ref refDs);
                //        dataCustTerms = new DataTable();
                //        dataCustTerms = refDs.Tables["JobCustTerms"];

                //        // Consignee
                //        sFilter = "CONSIGNEE' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "Consignee", ref refDs);
                //        dataConsignee = new DataTable();
                //        dataConsignee = refDs.Tables["Consignee"];

                //        // Invoice Agent
                //        sFilter = "INVAGENT' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "InvAgent", ref refDs);
                //        dataInvoiceAgent = new DataTable();
                //        dataInvoiceAgent = refDs.Tables["InvAgent"];

                //        // Ports
                //        sFilter = "PORT' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "Port", ref refDs);
                //        dataPorts = new DataTable();
                //        dataPorts = refDs.Tables["Port"];

                //        // DepositStatus
                //        sFilter = "DEPOSITPAID' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "Deposits", ref refDs);
                //        dataDepositStatus = new DataTable();
                //        dataDepositStatus = refDs.Tables["Deposits"];

                //        // CustPayTerms
                //        sFilter = "CUSTPAYTERMS' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "CustPayTerms", ref refDs);
                //        dataCustPayTerms = new DataTable();
                //        dataCustPayTerms = refDs.Tables["CustPayTerms"];

                //        // FCL
                //        sFilter = "FCL' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "FCL", ref refDs);
                //        dataFCL = new DataTable();
                //        dataFCL = refDs.Tables["FCL"];

                //        // Currency
                //        sFilter = "CURRENCY' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "Currency", ref refDs);
                //        dataCurrency = new DataTable();
                //        dataCurrency = refDs.Tables["Currency"];

                //        // Country
                //        sFilter = "COUNTRY' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "Country", ref refDs);
                //        dataCountry = new DataTable();
                //        dataCountry = refDs.Tables["Country"];

                //        // Customer Docs Sent
                //        sFilter = "CUSTOMERDOCSSENT' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "CustomerDocsSent", ref refDs);
                //        dataCustomerDocsSent = new DataTable();
                //        dataCustomerDocsSent = refDs.Tables["CustomerDocsSent"];

                //        // Invoice In MYOB
                //        sFilter = "INVINMYOB' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "InvInMyob", ref refDs);
                //        dataInvInMyob = new DataTable();
                //        dataInvInMyob = refDs.Tables["InvInMyob"];

                //        // MYOB Version
                //        sFilter = "MYOBVERSION' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "MyobVersion", ref refDs);
                //        dataMyobVersion = new DataTable();
                //        dataMyobVersion = refDs.Tables["MyobVersion"];

                //        // Docs To Customs
                //        sFilter = "DOCSTOCUSTOMS' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "DocsToCustoms", ref refDs);
                //        dataDocsToCustoms = new DataTable();
                //        dataDocsToCustoms = refDs.Tables["DocsToCustoms"];

                //        // Duty Status
                //        sFilter = "DUTYSTATUS' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "DutyStatus", ref refDs);
                //        dataDutyStatus = new DataTable();
                //        dataDutyStatus = refDs.Tables["DutyStatus"];

                //        // Note Type
                //        sFilter = "NOTETYPE' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "NoteType", ref refDs);
                //        dataNoteType = new DataTable();
                //        dataNoteType = refDs.Tables["NoteType"];

                //        // Task Type
                //        sFilter = "JOBTASKTYPE' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "TaskType", ref refDs);
                //        dataTaskType = new DataTable();
                //        dataTaskType = refDs.Tables["TaskType"];

                //        // Task Status
                //        sFilter = "JOBTASKSTATUS' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "TaskStatus", ref refDs);
                //        dataJobTaskStatus = new DataTable();
                //        dataJobTaskStatus = refDs.Tables["TaskStatus"];

                //        // Task Priority
                //        sFilter = "JOBTASKPRIORITY' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "TaskPriority", ref refDs);
                //        dataJobTaskPriority = new DataTable();
                //        dataJobTaskPriority = refDs.Tables["TaskPriority"];

                //        // Goods Desc
                //        sFilter = "GOODSDESC' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "GoodsDesc", ref refDs);
                //        dataGoodsDesc = new DataTable();
                //        dataGoodsDesc = refDs.Tables["GoodsDesc"];

                //        // Logs Priority
                //        sFilter = "LOGSPRIORITY' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "LogsPriority", ref refDs);
                //        dataLogsPriority = new DataTable();
                //        dataLogsPriority = refDs.Tables["LogsPriority"];

                //        // DataTypes
                //        sFilter = "DATATYPE' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "DataTypes", ref refDs);
                //        dataReportDataTypes = new DataTable();
                //        dataReportDataTypes = refDs.Tables["DataTypes"];

                //        // Report Operators
                //        sFilter = "REPORTOPERATOR' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "ReportOperators", ref refDs);
                //        dataReportOperator = new DataTable();
                //        dataReportOperator = refDs.Tables["ReportOperators"];

                //        // Query Param Types
                //        sFilter = "QUERYPARAMTYPE' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "QueryParamTypes", ref refDs);
                //        dataQueryParamType = new DataTable();
                //        dataQueryParamType = refDs.Tables["QueryParamTypes"];

                //        // Report Exclusions
                //        sFilter = "WEEKLYREPORTEXCLUDE' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "ReportExclusion", ref refDs);
                //        dataReportExclusion = new DataTable();
                //        dataReportExclusion = refDs.Tables["ReportExclusion"];

                // Entity Types
                sFilter = "ENTITYTYPE' order by ref_id";
                sSql = sBaseSql + sFilter;
                b = SqlHost.GetDataSet(sSql, "EntityType", ref refDs);
                dataEntityType = new DataTable();
                dataEntityType = refDs.Tables["EntityType"];

                //        // Buying Teams
                //        sFilter = "BUYINGTEAM' order by ref_id";
                //        sSql = sBaseSql + sFilter;
                //        b = SqlHost.GetDataSet(sSql, "BuyingTeam", ref refDs);
                //        dataBuyingTeam = new DataTable();
                //        dataBuyingTeam = refDs.Tables["BuyingTeam"];

                // UOM Conversions
                sFilter = "UOMCONVERSION' order by ref_id";
                sSql = sBaseSql + sFilter;
                b = SqlHost.GetDataSet(sSql, "UOMConversion", ref refDs);
                dataUOMConversions = new DataTable();
                dataUOMConversions = refDs.Tables["UOMConversion"];

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //public static bool SingleContext
        //{
        //    get { return bSingleContext; }
        //    set { bSingleContext = value; }
        //}

        public static DateTime ArchiveDate
        {
            get
            {
                if (dtArchiveDate == DateTime.MinValue)
                {
                    string sTemp = AppControlSetting("ARCHIVE_DATE_ETD");
                    dtArchiveDate = DateTime.ParseExact(sTemp, "yyyy-mm-dd", null);
                }
                return dtArchiveDate;
            }
            set { dtArchiveDate = value; }
        }

        //public static DateTime MinDate
        //{
        //    get { return DateTime.ParseExact("1901-01-01", "yyyy-mm-dd", null); }
        //}

        //public static string GetOperatorSymbol(string sOpId)
        //{
        //    return dataReportOperator.Select("REF_ID = '" + sOpId + "'")[0]["short_desc"].ToString();
        //}

        private static bool PFXInit()
        {
            int nReturn;
            try
            {
                pfxclibn.PclInit();
                nReturn = pfxclibn.PclRegister("AMIGO SOLUTIONS", "320058-E3RMI-TJRKI-WLC5-LBR-999");
                if (nReturn != pfxclibn.OK)
                {
                    MessageBox.Show("PclRegister call failed: " + nReturn);
                    bPfxLoaded = false;
                    return false;
                }
                sCbaPath = ConfigValue("/ConfigurationSettings/ConnectionSettings/CBA", "path");
                bPfxLoaded = true;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("PclInit Failed: (" + ex.Source + ") " + ex.Message);
                return false;
            }
        }

        //public static XmlDocument XmlSettings
        //{
        //    get { return xmlSettings; }
        //    set { xmlSettings = value; }
        //}

        private static bool LoadConstants()
        {
        //    Int32 nTemp;
            try
            {
        //        Constants.JOB_JOBSTATUS_HOLD = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobStatusSettings", "Hold");
        //        Constants.JOB_JOBSTATUS_PENDING = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobStatusSettings", "Pending");
        //        Constants.JOB_JOBSTATUS_NEEDORDERCONFIRMATION = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobStatusSettings", "NeedOrderConfirmation");
        //        Constants.JOB_JOBSTATUS_CONFIRMETD = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobStatusSettings", "ConfirmETD");
        //        Constants.JOB_JOBSTATUS_ETDCONFIRMED = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobStatusSettings", "ETDConfirmed");
        //        Constants.JOB_JOBSTATUS_MERGED = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobStatusSettings", "Merged");
        //        Constants.JOB_JOBSTATUS_SHIPPED = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobStatusSettings", "Shipped");
        //        Constants.JOB_JOBSTATUS_DELIVERED = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobStatusSettings", "Delivered");
        //        Constants.JOB_JOBSTATUS_EMPTYRET = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobStatusSettings", "EmptyRet");
        //        Constants.JOB_JOBSTATUS_COMPLETED = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobStatusSettings", "Completed");
        //        Constants.JOB_JOBSTATUS_CANCELLED = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobStatusSettings", "Cancelled");
        //        Constants.JOB_JOBSTATUS_BOOKNOW = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobStatusSettings", "BookNow");
        //        Constants.JOB_JOBSTATUS_CHECKDELTODLETDPAST = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobStatusSettings", "CheckDelToDlEtdPast");
        //        Constants.JOB_JOBSTATUS_OVERDUESENDSHIPPINGDOCS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobStatusSettings", "OverdueSendShippingDocs");
        //        Constants.JOB_JOBSTATUS_URGENTETDPAST = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobStatusSettings", "UrgentETDPast");

        //        Constants.JOB_SHIPTYPE_TRANS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/SoftConstants", "jobShipTypeTrans");
        //        Constants.JOB_SHIPTYPE_DIRECT = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/SoftConstants", "jobShipTypeDirect");
        //        Constants.JOB_CUSTPAYTERMS_COD = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/SoftConstants", "jobCustPayTermsCOD");
        //        Constants.JOB_JOBCUSTTERMS_FOB = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/SoftConstants", "jobJobCustTermsFOB");

        //        Constants.JOB_LOGSPRIORITY_HIGH = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/SoftConstants", "logsPriorityHigh");

        //        Constants.JOB_INVINMYOB_NO = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/SoftConstants", "jobInvInMyobNo");
        //        Constants.JOB_INVINMYOB_YES = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/SoftConstants", "jobInvInMyobYes");
        //        Constants.JOB_INVINMYOB_AMENDED = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/SoftConstants", "jobInvInMyobAmended");

        //        Constants.JOB_DEPOSIT_REQUIRED = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/SoftConstants", "jobDepositRequired");
        //        Constants.JOB_DEPOSIT_PAID = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/SoftConstants", "jobDepositPaid");

        //        Constants.CUST_TYPE_3PL = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/CustomerTypes", "Type3PL");

        //        Constants.JOB_MERGED_CLONESTATUS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/SetMergedAction", "cloneStatus");
        //        Constants.JOB_MERGED_NEWSTATUS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/SetMergedAction", "newJobStatus");
        //        Constants.JOB_MERGED_NEWSTATUSFOB = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/SetMergedAction", "newJobStatusFOB");
        //        Constants.JOB_MERGED_NEWDOCSSTATUS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/SetMergedAction", "newDocStatus");
        //        Constants.JOB_MERGED_FOBDOCSTOCUSTOMS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/SetMergedAction", "fobDocsToCustoms");
        //        Constants.JOB_MERGED_FOBDUTYSTATUS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/SetMergedAction", "fobDutyStatus");

        //        Constants.JOB_SHIPPED_FCLNONPRIMARY = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/SetShippedAction", "fclNonPrimary");
        //        Constants.JOB_SHIPPED_FCLSHAREDCUST = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/SetShippedAction", "fclSharedCust");
        //        Constants.JOB_SHIPPED_NEWSTATUS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/SetShippedAction", "newStatus");

        //        Constants.JOB_NEW_JOBSTATUS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewJob", "jobStatus");
        //        Constants.JOB_NEW_SOURCETYPE = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewJob", "sourceType");
        //        Constants.JOB_NEW_PLSENTTOCUST = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewJob", "plSentToCust");
        //        Constants.JOB_NEW_INVSENTTOCUST = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewJob", "invSentToCust");
        //        Constants.JOB_NEW_SUPPINVINMYOB = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewJob", "suppInvInMyob");
        //        Constants.JOB_NEW_CUSTINVINMYOB = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewJob", "custInvInMyob");
        //        Constants.JOB_NEW_DUTYSTATUS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewJob", "dutyStatus");
        //        Constants.JOB_NEW_DOCSTOCUSTOMS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewJob", "docsToCustoms");
        //        Constants.JOB_NEW_LOGSPRIORITY = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewJob", "logsPriority");
        //        Constants.JOB_NEW_REPORTEXCLUSION = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewJob", "reportExclusion");

        //        Constants.JOB_NEW_SUPPPROFORMARECD = AppHost.AppControlSetting("JOB.SUPP_PROFORMA_RECD.NEW_DEFAULTVALUE");
        //        Constants.JOB_NEW_SUPPINVOICERECD = AppHost.AppControlSetting("JOB.SUPP_INVOICE_RECD.NEW_DEFAULTVALUE");
        //        Constants.JOB_NEW_SUPPPACKINGLISTRECD = AppHost.AppControlSetting("JOB.SUPP_PACKINGLIST_RECD.NEW_DEFAULTVALUE");
        //        Constants.JOB_NEW_SUPPBLFCRRECD = AppHost.AppControlSetting("JOB.SUPP_BLFCR_RECD.NEW_DEFAULTVALUE");
        //        Constants.JOB_NEW_SUPPPACKINGDECRECD = AppHost.AppControlSetting("JOB.SUPP_PACKINGDEC_RECD.NEW_DEFAULTVALUE");
        //        Constants.JOB_NEW_SUPPMANDECRECD = AppHost.AppControlSetting("JOB.SUPP_MANDEC_RECD.NEW_DEFAULTVALUE");
        //        Constants.JOB_NEW_SUPPFUMORECD = AppHost.AppControlSetting("JOB.SUPP_FUMO_RECD.NEW_DEFAULTVALUE");
        //        Constants.JOB_NEW_SUPPFUMOSTORAGERECD = AppHost.AppControlSetting("JOB.SUPP_FUMOSTORAGE_RECD.NEW_DEFAULTVALUE");
        //        Constants.JOB_NEW_SUPPCOORECD = AppHost.AppControlSetting("JOB.SUPP_COO_RECD.NEW_DEFAULTVALUE");
        //        Constants.JOB_NEW_SUPPTELEXREQUESTED = AppHost.AppControlSetting("JOB.SUPP_TELEX_REQUESTED.NEW_DEFAULTVALUE");
        //        Constants.JOB_NEW_SUPPSHIPPERBOOKINGCONFRECD = AppHost.AppControlSetting("JOB.SUPP_SHIPPER_BOOKING_CONF_RECD.NEW_DEFAULTVALUE");
        //        Constants.JOB_NEW_CUSTPORECD = AppHost.AppControlSetting("JOB.CUST_PO_RECD.NEW_DEFAULTVALUE");
        //        Constants.JOB_NEW_CUSTPORECD = AppHost.AppControlSetting("JOB.CUST_PO_RECD.NEW_DEFAULTVALUE");
        //        Constants.JOB_NEW_CONTAINERNETTINGREQUESTED = AppHost.AppControlSetting("JOB.CONTAINER_NETTING_REQ.NEW_DEFAULTVALUE");
        //        Constants.JOB_NEW_CONTAINERNETTINGRECD = AppHost.AppControlSetting("JOB.CONTAINER_NETTING_RECD.NEW_DEFAULTVALUE");
        //        Constants.JOB_NEW_SHIPMENTBOOKINGREQUESTED = AppHost.AppControlSetting("JOB.SHIPMENT_BOOKING_REQ.NEW_DEFAULTVALUE");
        //        Constants.JOB_NEW_SHIPPINGDOCSREQUESTED = AppHost.AppControlSetting("JOB.SHIPPING_DOCS_REQ.NEW_DEFAULTVALUE");
        //        Constants.JOB_NEW_FSCRECD = AppHost.AppControlSetting("JOB.FSC_RECD.NEW_DEFAULTVALUE");
        //        Constants.JOB_NEW_CUSTPISENT = AppHost.AppControlSetting("JOB.CUST_PI_SENT.NEW_DEFAULTVALUE");
        //        Constants.JOB_NEW_TIMBERCOMPLIANCE = AppHost.AppControlSetting("JOB.TIMBER_COMPLIANCE.NEW_DEFAULTVALUE");
        //        Constants.JOB_NEW_OMTCOMPLETE= AppHost.AppControlSetting("JOB.OMT_PASSED.NEW_DEFAULTVALUE");

        //        Constants.REPORT_TABLE_JOB = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/ReportSettings", "tableJobs");
        //        Constants.REPORT_DEF_PARAMTYPE = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/ReportSettings", "defParamType");

        //        //Constants.COUNTERS_PONUM = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/Counters", "poNum");
        //        Constants.COUNTERS_PONUM = AppHost.ConfigValueCompany("Counters", "poNum");

        //        Constants.JOB_EDIT_MYOBLOCKEDSTATUS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/EditJobs", "MYOBLockedStatus");
        //        Constants.JOB_EDIT_MYOBLOCKEDALERT = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/EditJobs", "MYOBLockedAlert");

        //        Constants.JOB_UPDATE_CUSTINVINMYOB = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/AdminUpdates", "custInvInMyob");
        //        Constants.JOB_UPDATE_PLSENTTOCUST = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/AdminUpdates", "plSentToCust");
        //        Constants.JOB_UPDATE_INVSENTTOCUST = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/AdminUpdates", "invSentToCust");
        //        Constants.JOB_UPDATE_DOCSTOCUSTOMSSENT = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/AdminUpdates", "docsToCustomsSent");

        //        Constants.JOB_NOTETYPE_GENERAL = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobNoteTypes", "general");
        //        Constants.JOB_NOTETYPE_MISSINGDOCS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobNoteTypes", "missingDocs");
        //        Constants.JOB_NOTETYPE_PARTSHIP = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobNoteTypes", "partShip");
        //        Constants.JOB_NOTETYPE_TRANSSHIP = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobNoteTypes", "transShip");

                Constants.RELEASE_VERSION = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/CurrentVersion", "versionNo");
                //        Constants.RELEASE_DATE = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/CurrentVersion", "releaseDate");
                //        Constants.RELEASE_TIME = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/CurrentVersion", "releaseTime");

                Constants.SECURITY_REFDATAPASSWORD = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/Security", "refDataPassword");
                //        Constants.SECURITY_LOGSPASSWORD = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/Security", "logsPassword");
                //        Constants.SECURITY_JOBIDOVERRIDELEVEL = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/Security", "jobIdOverrideLevel");
                //        Constants.SECURITY_SUPERUSERSQL = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/Security", "superUserSQL");

                //        Constants.FCL_SHARED = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/FclTypes", "shared");

                //        Constants.JOB_PARENTCOPY_FCL = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobParentCopy", "defaultFCL");

                //        Constants.JOB_SOURCETYPE_LOCAL = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobSourceTypes", "local");
                //        Constants.JOB_SOURCETYPE_OVERSEAS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobSourceTypes", "overseas");

                //        Constants.JOB_DUTYSTATUS_LOCALS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobDutyStatus", "local");
                //        Constants.JOB_DUTYSTATUS_OVERSEAS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobDutyStatus", "overseas");

                //        Constants.JOB_DOCSTOCUSTOMS_LOCALS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobDocsToCustoms", "local");
                //        Constants.JOB_DOCSTOCUSTOMS_OVERSEAS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobDocsToCustoms", "overseas");

                //        Constants.JOB_TRACKING_VIEW_TYPE_ID = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobTracking", "viewBasedTypeId");
                //        Constants.JOB_TRACKING_ITEM_TYPE_ID = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobTracking", "itemBasedTypeId");

                //        Constants.JOB_TRACKING_ITEM_TYPE_BOOL = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobTracking", "panelBool");
                //        Constants.JOB_TRACKING_ITEM_TYPE_COMMENT = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobTracking", "panelComment");
                //        Constants.JOB_TRACKING_ITEM_TYPE_DATE = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobTracking", "panelDate");
                //        Constants.JOB_TRACKING_ITEM_TYPE_GUID = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobTracking", "panelGuid");
                //        Constants.JOB_TRACKING_ITEM_TYPE_NOTE = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobTracking", "panelNote");
                //        Constants.JOB_TRACKING_ITEM_TYPE_NOTERTF = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobTracking", "panelNoteRTF");
                //        Constants.JOB_TRACKING_ITEM_TYPE_NUMBER = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobTracking", "panelNumber");
                //        Constants.JOB_TRACKING_ITEM_TYPE_STATUS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobTracking", "panelStatus");
                //        Constants.JOB_TRACKING_ITEM_TYPE_URI = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobTracking", "panelUri");
                //        Constants.TRACKING_PROFILE_TYPE_JOB = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/TrackingProfiles", "typeJob");
                //        Constants.TRACKING_PROFILE_TYPE_JOBITEM = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/TrackingProfiles", "typeJobItem");
                //        Constants.TRACKING_PROFILE_TYPE_STOCK = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/TrackingProfiles", "typeStock");

                //        Constants.WORKBENCH_CUSTOMGRIDVIEW_DISPLAY = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/CustomGridView", "display");

                //        Constants.WORKBENCH_EMAIL_USEOUTLOOK = (AppHost.ConfigValue("/ConfigurationSettings/AppSettings/EmailSettings", "useOutlook") == "Y");

                //        Constants.CURRENCY_AUD = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/CurrencySettings", "AUD");

                Constants.WORKBENCH_CBA_USEWEBSERVICES = (SqlHost.ConfigValue("/ConfigurationSettings/ConnectionSettings/CBA", "useWebServices") == "Y");

                Constants.WORKBENCH_SYSCTR_SQLSERVER = (SqlHost.ConfigValue("/ConfigurationSettings/ConnectionSettings/CBA", "useSqlServer") == "Y");

                Constants.NEWSUPPLIER_SUPPTYPE = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewSupplierSettings", "SuppType");
                Constants.NEWSUPPLIER_POTYPE = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewSupplierSettings", "POType");
                Constants.NEWSUPPLIER_LOCKCODE = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewSupplierSettings", "LockCode");
                Constants.NEWSUPPLIER_CBATERMS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewSupplierSettings", "CBATerms");
                Constants.NEWSUPPLIER_ENTITYTYPE = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewSupplierSettings", "EntityType");

                Constants.NEWCUSTOMER_CBATERMS = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewCustomerSettings", "CBATerms");
                Constants.NEWCUSTOMER_CBABRANCH = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewCustomerSettings", "CBABranch");
                Constants.NEWCUSTOMER_CBASALESMAN = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewCustomerSettings", "CBASalesman");
                Constants.NEWCUSTOMER_CBAPRICECODE = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewCustomerSettings", "CBAPriceCode");
                Constants.NEWCUSTOMER_LOCKCODE = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewCustomerSettings", "LockCode");
                Constants.NEWCUSTOMER_ENTITYTYPE = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/NewCustomerSettings", "EntityType");

                //        Constants.JOB_TASKFILTER_Job = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobTaskSettings", "filterTypeJob");
                //        Constants.JOB_TASKFILTER_User = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobTaskSettings", "filterTypeUser");
                //        Constants.JOB_TASKSTATUS_Complete = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobTaskSettings", "statusComplete");
                //        Constants.JOB_TASKSTATUS_New = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobTaskSettings", "statusNew");
                //        Constants.JOB_TASKTYPE_Default = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobTaskSettings", "typeDefault");
                //        Constants.JOB_TASKPRIORITY_Urgent = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobTaskSettings", "priorityUrgent");
                //        Constants.JOB_TASKPRIORITY_Normal = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobTaskSettings", "priorityNormal");

                //        Constants.JOB_SEARCH_TextSearchFieldList = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/JobSearchSettings", "textSearchFieldList");

                //        if (Int32.TryParse(AppHost.AppControlSetting("REFDATA.CUSTTERMS.INV_INCLUDE_TAX"), out nTemp))
                //        {
                //            Constants.REFDATA_CUSTTERMS_InvIncludeTax = nTemp;
                //        }
                //        else
                //        {
                //            Constants.REFDATA_CUSTTERMS_InvIncludeTax = 0;
                //        }
                //        if (Int32.TryParse(AppHost.AppControlSetting("REFDATA.CUSTTERMS.INV_SUPPRESS_DATES"), out nTemp))
                //        {
                //            Constants.REFDATA_CUSTTERMS_InvSuppressDates = nTemp;
                //        }
                //        else
                //        {
                //            Constants.REFDATA_CUSTTERMS_InvSuppressDates = 0;
                //        }
                //        if (Int32.TryParse(AppHost.AppControlSetting("REFDATA.CUSTTERMS.INV_SUPPRESS_POL"), out nTemp))
                //        {
                //            Constants.REFDATA_CUSTTERMS_InvSuppressPOL = nTemp;
                //        }
                //        else
                //        {
                //            Constants.REFDATA_CUSTTERMS_InvSuppressPOL = 0;
                //        }
                //        if (Int32.TryParse(AppHost.AppControlSetting("REFDATA.CUSTTERMS.INV_SUPPRESS_VESSEL"), out nTemp))
                //        {
                //            Constants.REFDATA_CUSTTERMS_InvSuppressVessel = nTemp;
                //        }
                //        else
                //        {
                //            Constants.REFDATA_CUSTTERMS_InvSuppressVessel = 0;
                //        }
                //        if (Int32.TryParse(AppHost.AppControlSetting("REFDATA.CUSTTERMS.INV_PRINT_SHIPPING_CONF"), out nTemp))
                //        {
                //            Constants.REFDATA_CUSTTERMS_InvPrintShippingConf = nTemp;
                //        }
                //        else
                //        {
                //            Constants.REFDATA_CUSTTERMS_InvPrintShippingConf = 0;
                //        }
                //        if (Int32.TryParse(AppHost.AppControlSetting("REFDATA.CUSTTERMS.SHIP_CONF_SUPPRESS_DATES"), out nTemp))
                //        {
                //            Constants.REFDATA_CUSTTERMS_ShipConfSuppressDates = nTemp;
                //        }
                //        else
                //        {
                //            Constants.REFDATA_CUSTTERMS_ShipConfSuppressDates = 0;
                //        }
                //        if (Int32.TryParse(AppHost.AppControlSetting("REFDATA.CUSTTERMS.SHIP_CONF_SUPPRESS_POL"), out nTemp))
                //        {
                //            Constants.REFDATA_CUSTTERMS_ShipConfSuppressPOL = nTemp;
                //        }
                //        else
                //        {
                //            Constants.REFDATA_CUSTTERMS_ShipConfSuppressPOL = 0;
                //        }
                //        if (Int32.TryParse(AppHost.AppControlSetting("REFDATA.CUSTTERMS.SHIP_CONF_SUPPRESS_VESSEL"), out nTemp))
                //        {
                //            Constants.REFDATA_CUSTTERMS_ShipConfSuppressVessel = nTemp;
                //        }
                //        else
                //        {
                //            Constants.REFDATA_CUSTTERMS_ShipConfSuppressVessel = 0;
                //        }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //public static string GetNextCBA(string sId)
        //{
        //    if (Constants.WORKBENCH_CBA_USEWEBSERVICES == true)
        //    {
        //        return GetNextCBAWebService(sId);
        //    }
        //    else
        //    {
        //        if (Constants.WORKBENCH_SYSCTR_SQLSERVER == true)
        //        {
        //            return FunctionHost.GetNextCBASqlServer(sId);
        //        }
        //        else
        //        {
        //            return GetNextCBAPfx(sId);
        //        }
        //    }
        //}

        //public static string GetNextCBAWebService(string sId)
        //{
        //    MasterfilesServer.Counters c = new MasterfilesWorkbench.MasterfilesServer.Counters();
        //    string sNew = c.GetNextId(sId);
        //    //string sNew = c.HelloWorld();
        //    return sNew;
        //    //return "";
        //}




        //public static string GetNextCBASqlServerOld(string sId)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    SqlDataReader cReader;
        //    string sLastValue = "";
        //    string sPrefix = "";
        //    string sSuffix = "";
        //    string sFormat = "";
        //    sSql = "select LAST_VALUE, PREFIX, SUFFIX, FORMAT from SYS_COUNTER where CTR_ID = @CtrId";
        //    sqlCmd = null;
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@CtrId", sId);
        //    cReader = sqlCmd.ExecuteReader();
        //    while (cReader.Read())
        //    {
        //        sLastValue = cReader["LAST_VALUE"].ToString();
        //        sPrefix = cReader["PREFIX"].ToString();
        //        sSuffix = cReader["SUFFIX"].ToString();
        //        sFormat = cReader["FORMAT"].ToString();
        //    }
        //    cReader.Close();
        //    double newValue = Convert.ToDouble(sLastValue) + 1;
        //    sSql = "update SYS_COUNTER set LAST_VALUE = @NewVal where CTR_ID = @CtrId";
        //    sqlCmd = null;
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@NewVal", newValue);
        //    sqlCmd.Parameters.AddWithValue("@CtrId", sId);
        //    sqlCmd.ExecuteNonQuery();
        //    string sReturn = sPrefix + newValue.ToString(sFormat) + sSuffix;
        //    return sReturn;
        //}

        //public static string GetNextCBAPfx(string sId)
        //{
        //    Int32 nResult;
        //    try
        //    {
        //        if (!bPfxLoaded)
        //        {
        //            MessageBox.Show("Generating new numbers has been disabled.", "You can't do that!", MessageBoxButtons.OK);
        //            return "";
        //        }
        //        Int32 hnd = pfxclibn.PclAllocFile(0);
        //        if (hnd == -1)
        //        {
        //            return "";
        //        }
        //        nResult = pfxclibn.PclOpen(hnd, sCbaPath + "Sysctr.dat", 1);
        //        if (nResult != 0)
        //        {
        //            return "";
        //        }

        //        string sRId = sId.ToUpper();
        //        nResult = pfxclibn.PclClear(hnd);
        //        nResult = pfxclibn.PclPutField(hnd, 1, sRId);
        //        nResult = pfxclibn.PclFind(hnd, PFXClib.erelop.EQ, ekeyno.INDEX_1);
        //        if (nResult != 0)
        //        {
        //            nResult = pfxclibn.PclUnlock(hnd);
        //            nResult = pfxclibn.PclClose(hnd);
        //            nResult = pfxclibn.PclFreeFile(hnd);
        //            return "";
        //        }
        //        //nhCtr, 6, 30

        //        StringBuilder sBuffNum = new StringBuilder(30);
        //        nResult = pfxclibn.PclGetField(hnd, 6, sBuffNum);
        //        StringBuilder sBuffPre = new StringBuilder(10);
        //        nResult = pfxclibn.PclGetField(hnd, 7, sBuffPre);
        //        StringBuilder sBuffSuf = new StringBuilder(10);
        //        nResult = pfxclibn.PclGetField(hnd, 8, sBuffSuf);
        //        StringBuilder sBuffFmt = new StringBuilder(10);
        //        nResult = pfxclibn.PclGetField(hnd, 9, sBuffFmt);

        //        int oldVal = Convert.ToInt32(sBuffNum.ToString());
        //        int newVal = oldVal + 1;

        //        nResult = pfxclibn.PclLock(hnd);
        //        nResult = pfxclibn.PclReread(hnd);
        //        nResult = pfxclibn.PclPutField(hnd, 6, newVal.ToString());
        //        Int32 nResult2 = pfxclibn.PclChange(hnd);
        //        nResult = pfxclibn.PclUnlock(hnd);

        //        nResult = pfxclibn.PclClear(hnd);
        //        nResult = pfxclibn.PclClose(hnd);
        //        nResult = pfxclibn.PclFreeFile(hnd);

        //        return sBuffPre.ToString().Trim() + newVal.ToString().Trim() + sBuffSuf.ToString().Trim();

        //    }
        //    catch (Exception ex)
        //    {
        //        return "";
        //    }
        //}


        //public static Dictionary<string, string> GetSelectedJobPOShipRef(System.Windows.Forms.DataGridView dgv, string sKeyId, string sFieldList)
        //{
        //    string[] sFields;
        //    string sResults;
        //    try
        //    {
        //        sFields = sFieldList.Split(',');
        //        Dictionary<string, string> dict = new Dictionary<string, string>();
        //        foreach (System.Windows.Forms.DataGridViewCell cell in dgv.SelectedCells)
        //        {
        //            DataRow row = ((DataRowView)cell.OwningRow.DataBoundItem).Row;
        //            Guid g = (Guid)row[sKeyId];
        //            if (!dict.Keys.Contains(g.ToString()))
        //            {
        //                sResults = "";
        //                for (int i = 0; i < sFields.Length; i++)
        //                {
        //                    if (sResults.Length > 0)
        //                        sResults = sResults + ",";
        //                    sResults = sResults + row[sFields[i]].ToString();
        //                }
        //                dict.Add(g.ToString(), sResults);
        //            }
        //        }
        //        foreach (System.Windows.Forms.DataGridViewRow thisRow in dgv.SelectedRows)
        //        {
        //            DataRow row = ((DataRowView)thisRow.DataBoundItem).Row;
        //            Guid g = (Guid)row[sKeyId];
        //            if (!dict.Keys.Contains(g.ToString()))
        //            {
        //                sResults = "";
        //                for (int i = 0; i < sFields.Length; i++ )
        //                {
        //                    if (sResults.Length > 0)
        //                        sResults = sResults + ",";
        //                    sResults = sResults + row[sFields[i]].ToString();
        //                }
        //                dict.Add(g.ToString(), sResults);
        //            }
        //        }
        //        return dict;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        //public static Dictionary<string, Guid> GetSelectedJobs(System.Windows.Forms.DataGridView dgv, string sKeyId)
        //{
        //    try
        //    {
        //        Dictionary<string, Guid> dict = new Dictionary<string, Guid>();
        //        foreach (System.Windows.Forms.DataGridViewCell cell in dgv.SelectedCells)
        //        {
        //            DataRow row = ((DataRowView)cell.OwningRow.DataBoundItem).Row;
        //            Guid g = (Guid)row[sKeyId];
        //            if (!dict.Keys.Contains(g.ToString()))
        //            {
        //                dict.Add(g.ToString(), g);
        //            }
        //        }
        //        foreach (System.Windows.Forms.DataGridViewRow thisRow in dgv.SelectedRows)
        //        {
        //            DataRow row = ((DataRowView)thisRow.DataBoundItem).Row;
        //            Guid g = (Guid)row[sKeyId];
        //            if (!dict.Keys.Contains(g.ToString()))
        //            {
        //                dict.Add(g.ToString(), g);
        //            }
        //        }
        //        return dict;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        public static string ConfigValue(string sNodeName, string sAttrib)
        {
            XmlNode xmlN = xmlSettings.SelectSingleNode(sNodeName);
            XmlElement xmlE = (XmlElement)xmlN;
            string sVal = xmlE.GetAttribute(sAttrib);
            return sVal;
        }

        public static XmlNodeList ConfigValues(string sNodeName)
        {
            XmlNodeList xmlN = xmlSettings.SelectNodes(sNodeName);
            return xmlN;
        }

        //public static string ConfigValueCompany(string sNodeName, string sAttrib)
        //{
        //    XmlNodeList xList = xmlSettings.SelectNodes("/ConfigurationSettings/CompanySettings/Company[@id='" + AppHost.CurrentContextId + "']");
        //    XmlNode xCompany = xList[0];
        //    XmlNode xN = xCompany.SelectSingleNode(sNodeName);
        //    XmlElement xmlE = (XmlElement)xN;
        //    string sVal = xmlE.GetAttribute(sAttrib);
        //    return sVal;
        //}
        //public static string ConfigValue(string sFullPath)
        //{
        //    XmlNode xmlN = xmlSettings.SelectSingleNode(sFullPath);
        //    return (string)xmlN.Value;
        //}
        //public static XmlElement ConfigElement(string sElementName)
        //{
        //    XmlElement xlE = xmlSettings.GetElementById(sElementName);
        //    return xlE;
        //}
        //public static string AppPath
        //{
        //    get { return sAppPath; }
        //}
        public static string WebSessionKey
        {
            get { return sWebSessionKey; }
            set { sWebSessionKey = value; }
        }

        public static DataRow CurrentUser
        {
            get { return cUser; }
            set { cUser = value; }
        }

        //public static DataTable JobStatus
        //{
        //    get { return dataJobStatus; }
        //    set { dataJobStatus = value; }
        //}

        //public static DataTable ShipmentType
        //{
        //    get { return dataShipmentType; }
        //    set { dataShipmentType = value; }
        //}

        //public static DataTable JobType
        //{
        //    get { return dataJobType; }
        //    set { dataJobType = value; }
        //}

        //public static DataTable CustTerms
        //{
        //    get { return dataCustTerms; }
        //    set { dataCustTerms = value; }
        //}

        //public static DataTable DepositStatus
        //{
        //    get { return dataDepositStatus; }
        //    set { dataDepositStatus = value; }
        //}

        //public static DataTable CustPayTerms
        //{
        //    get { return dataCustPayTerms; }
        //    set { dataCustPayTerms = value; }
        //}

        //public static DataTable Consignee
        //{
        //    get { return dataConsignee; }
        //    set { dataConsignee = value; }
        //}

        //public static DataTable InvoiceAgent
        //{
        //    get { return dataInvoiceAgent; }
        //    set { dataInvoiceAgent = value; }
        //}

        //public static DataTable Ports
        //{
        //    get { return dataPorts; }
        //    set { dataPorts = value; }
        //}

        //public static DataTable FCL
        //{
        //    get { return dataFCL; }
        //    set { dataFCL = value; }
        //}

        //public static DataTable ShippingLine
        //{
        //    get { return dataShippingLine; }
        //    set { dataShippingLine = value; }
        //}

        //public static DataTable Currency
        //{
        //    get { return dataCurrency; }
        //    set { dataCurrency = value; }
        //}

        //public static DataTable Country
        //{
        //    get { return dataCountry; }
        //    set { dataCountry = value; }
        //}

        //public static DataTable Users
        //{
        //    get { return dataUsers; }
        //    set { dataUsers = value; }
        //}

        //public static DataTable CustomerDocsSent
        //{
        //    get { return dataCustomerDocsSent; }
        //    set { dataCustomerDocsSent = value; }
        //}

        //public static DataTable InvInMyob
        //{
        //    get { return dataInvInMyob; }
        //    set { dataInvInMyob = value; }
        //}

        //public static DataTable MyobVersion
        //{
        //    get { return dataMyobVersion; }
        //    set { dataMyobVersion = value; }
        //}

        //public static DataTable DocsToCustoms
        //{
        //    get { return dataDocsToCustoms; }
        //    set { dataDocsToCustoms = value; }
        //}

        //public static DataTable DutyStatus
        //{
        //    get { return dataDutyStatus; }
        //    set { dataDutyStatus = value; }
        //}

        //public static DataTable SourceType
        //{
        //    get { return dataSourceType; }
        //    set { dataSourceType = value; }
        //}

        //public static DataTable NoteType
        //{
        //    get { return dataNoteType; }
        //    set { dataNoteType = value; }
        //}

        //public static DataTable TaskType
        //{
        //    get { return dataTaskType; }
        //    set { dataTaskType = value; }
        //}

        //public static DataTable JobTaskStatus
        //{
        //    get { return dataJobTaskStatus; }
        //    set { dataJobTaskStatus = value; }
        //}

        //public static DataTable JobTaskPriority
        //{
        //    get { return dataJobTaskPriority; }
        //    set { dataJobTaskPriority = value; }
        //}

        //public static DataTable GoodsDesc
        //{
        //    get { return dataGoodsDesc; }
        //    set { dataGoodsDesc = value; }
        //}

        //public static DataTable LogsPriority
        //{
        //    get { return dataLogsPriority; }
        //    set { dataLogsPriority = value; }
        //}

        //public static DataTable ReportOperator
        //{
        //    get { return dataReportOperator; }
        //    set { dataReportOperator = value; }
        //}

        //public static DataTable ReportDataType
        //{
        //    get { return dataReportDataTypes; }
        //    set { dataReportDataTypes = value; }
        //}

        //public static DataTable QueryParamType
        //{
        //    get { return dataQueryParamType; }
        //    set { dataQueryParamType = value; }
        //}

        //public static DataTable ReportExclusion
        //{
        //    get { return dataReportExclusion; }
        //    set { dataReportExclusion = value; }
        //}

        public static DataTable EntityType
        {
            get { return dataEntityType; }
            set { dataEntityType = value; }
        }

        //public static DataTable ImageType
        //{
        //    get { return dataImageType; }
        //    set { dataImageType = value; }
        //}

        public static DataTable Context
        {
            get { return dataContext; }
            set { dataContext = value; }
        }

        //public static DataTable BuyingTeam
        //{
        //    get { return dataBuyingTeam; }
        //    set { dataBuyingTeam = value; }
        //}

        //public static DataTable AppControl
        //{
        //    get { return dataAppControl; }
        //    set { dataAppControl = value; }
        //}

        public static DataTable UOMConversions
        {
            get { return dataUOMConversions; }
            set { dataUOMConversions = value; }
        }


        public static string AppControlSetting(string sControlId)
        {
            string sValue;
            string sFilter;
            DataRow cRow;
            try
            {
                sFilter = "control_id = '" + sControlId + "'";
                cRow = dataAppControl.Select(sFilter)[0];
                sValue = cRow["control_value"].ToString();
                return sValue;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //public static XmlDocument AppControlXml(string sControlId)
        //{
        //    string sValue;
        //    string sFilter;
        //    DataRow cRow;
        //    XmlDocument xDoc;
        //    try
        //    {
        //        sFilter = "control_id = '" + sControlId + "'";
        //        cRow = dataAppControl.Select(sFilter)[0];
        //        sValue = cRow["control_xml"].ToString();
        //        xDoc = new XmlDocument();
        //        xDoc.LoadXml(sValue);
        //        return xDoc;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        //public static bool PFXLoaded
        //{
        //    get { return bPfxLoaded; }
        //    set { bPfxLoaded = value; }
        //}

        //public static IWorkbench CurrentWorkbench
        //{
        //    get { return currentWorkbench; }
        //    set { currentWorkbench = value; }
        //}

        //public static string LetterFromNumber(int column)
        //{
        //    string columnString = "";
        //    decimal columnNumber = column;
        //    while (columnNumber > 0)
        //    {
        //        decimal currentLetterNumber = (columnNumber - 1) % 26;
        //        char currentLetter = (char)(currentLetterNumber + 65);
        //        columnString = currentLetter + columnString;
        //        columnNumber = (columnNumber - (currentLetterNumber + 1)) / 26;
        //    }
        //    return columnString;
        //}

        //public static void CreateJobNote(Guid gJob, string sType, string sSubject, string sDetail)
        //{
        //    //SqlHost.CreateJobNote(gJob, sType, sSubject, sDetail, CurrentUser["UserName"].ToString());
        //    SqlHost.CreateJobNote(gJob, sType, sSubject, sDetail, CurrentUser["UserName"].ToString());
        //}

        public static bool LoginUser(string sUn, string sPwd)
        {
            string sEncryptedUn = "";
            string sEncryptedPw = "";
            try
            {
                sEncryptedUn = SecurityManager.EncryptWithRandom(sUn);
                sEncryptedPw = SecurityManager.EncryptWithRandom(sPwd);
                LoginResponse wsR = new MFServices.LoginResponse();
                MFServicesSoapClient r = new MFServices.MFServicesSoapClient();
                wsR = r.LoginUser(sEncryptedUn, sEncryptedPw);
                if (wsR.Success)
                {
                    WebSessionKey = wsR.Response.SessionKey;
                    SqlHost.WebSessionKey = wsR.Response.SessionKey;
                }
                bool bReturn = wsR.Success;

                if (bReturn)
                {
                    DataSet dataLogin = new DataSet();  
                    DataRow dataLogninRow;
                    dataLogin.Tables.Add(new DataTable());
                    dataLogin.Tables[0].Columns.Add("UserName");
                    dataLogin.Tables[0].Columns.Add("Surname");
                    dataLogin.Tables[0].Columns.Add("FirstName");
                    dataLogin.Tables[0].Columns.Add("PrefName");
                    dataLogin.Tables[0].Columns.Add("Email");
                    dataLogin.Tables[0].Columns.Add("Phone1");
                    dataLogin.Tables[0].Columns.Add("Phone2");
                    dataLogin.Tables[0].Columns.Add("SecurityLevel");

                    dataLogninRow = dataLogin.Tables[0].NewRow();
                    dataLogninRow["UserName"] = wsR.Response.UserName;
                    dataLogninRow["Surname"] = wsR.Response.Surname;
                    dataLogninRow["FirstName"] = wsR.Response.FirstName;
                    dataLogninRow["PrefName"] = wsR.Response.PrefName;
                    dataLogninRow["Email"] = wsR.Response.Email;
                    dataLogninRow["Phone1"] = wsR.Response.Phone1;
                    dataLogninRow["Phone2"] = wsR.Response.Phone2;
                    dataLogninRow["SecurityLevel"] = wsR.Response.SecurityLevel;
                    dataLogin.Tables[0].Rows.Add(dataLogninRow);
                    cUser = dataLogin.Tables[0].Rows[0];
                }

                return bReturn;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //public static void ExcelExport(DataGridView d)
        //{
        //    Excel.Application xlApp = null;
        //    Excel.Workbook xlWb;
        //    Excel.Worksheet xlWs;
        //    bool bIsNew = false;
        //    Forms.dlgStatusUpdateBox dlg = new MasterfilesWorkbench.Forms.dlgStatusUpdateBox();

        //    try
        //    {
        //        dlg.SetProgressBounds(1, d.Rows.Count + 2);
        //        object misValue = System.Reflection.Missing.Value;
        //        try
        //        {
        //            xlApp = (Excel.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application");
        //        }
        //        catch (System.Runtime.InteropServices.COMException)
        //        {
        //        }
        //        if (xlApp == null)
        //        {
        //            xlApp = new Excel.ApplicationClass();
        //            bIsNew = true;
        //        }
        //        xlWb = xlApp.Workbooks.Add(misValue);

        //        xlWs = (Excel.Worksheet)xlWb.Worksheets.get_Item(1);

        //        dlg.Show();

        //        int nCol = 1;
        //        foreach (DataGridViewColumn col in d.Columns)
        //        {
        //            xlWs.Cells[1, nCol] = col.HeaderText;
        //            nCol++;
        //        }
        //        int nOutRow = 2;
        //        foreach (DataGridViewRow row in d.Rows)
        //        {
        //            int nOutCol = 1;
        //            foreach (DataGridViewCell cell in row.Cells)
        //            {
        //                xlWs.Cells[nOutRow, nOutCol] = "'" + cell.Value.ToString();
        //                nOutCol++;
        //            }
        //            nOutRow++;
        //            dlg.UpdateStatus("Exporting row: " + nOutRow);
        //            dlg.UpdateProgress(nOutRow);
        //        }
        //        dlg.CloseMe();
        //        if (bIsNew || !xlApp.Visible)
        //        {
        //            xlApp.Visible = true;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        dlg.CloseMe();
        //    }
        //    finally
        //    {
        //    }
        //}

        //public static void ExcelNotesExport(DataGridView d, string sType)
        //{
        //    Excel.Application xlApp = null;
        //    Excel.Workbook xlWb;
        //    Excel.Worksheet xlWs;
        //    bool bIsNew = false;
        //    string sSql;
        //    SqlCommand sqlCmd;
        //    SqlDataReader cReader;

        //    try
        //    {
        //        object misValue = System.Reflection.Missing.Value;
        //        try
        //        {
        //            xlApp = (Excel.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application");
        //        }
        //        catch (System.Runtime.InteropServices.COMException)
        //        {
        //        }
        //        if (xlApp == null)
        //        {
        //            xlApp = new Excel.ApplicationClass();
        //            bIsNew = true;
        //        }
        //        xlWb = xlApp.Workbooks.Add(misValue);

        //        xlWs = (Excel.Worksheet)xlWb.Worksheets.get_Item(1);

        //        Dictionary<string, Guid> dict = GetSelectedJobs(d, "JobId");
        //        sSql = "select " +
        //            "B.PO_NUM as PONum, B.SHIP_REF as ShipRef, " +
        //            "case when B.VESSEL_CONFIRMED > '1902/01/01' THEN B.VESSEL_VOYAGE + ' *' ELSE B.VESSEL_VOYAGE END as VesselVoyage, " +
        //            "B.CONTAINER_NO as ContainerNo, C.SHORT_DESC as ShippingLine, " +
        //            "case when B.ETA_CONFIRMED > '1902/01/01' THEN CONVERT(varchar, B.ETA_PORT_DATE, 103) + ' *' ELSE CONVERT(varchar, B.ETA_PORT_DATE, 103) END as EtaPortDate, " +
        //            "A.SUBJECT as Subject, A.DETAIL as Detail, A.CREATED as Created " +
        //            "from JOB_NOTE A inner join JOB B on A.JOB_ID = B.JOB_ID " +
        //            "left outer join REF_DATA C on B.SHIPPING_LINE = C.REF_ID and C.REF_TABLE_ID = 'SHIPLINE' " +
        //            "where A.JOB_ID = @JobId ";
        //        if (sType.Length > 0)
        //        {
        //            sSql = sSql + "and A.NOTE_TYPE = @NoteType ";
        //        }
        //        sSql = sSql + "order by A.CREATED";
        //        xlWs.Cells[1, 1] = "PO Number";
        //        xlWs.Cells[1, 2] = "Ship Ref";
        //        xlWs.Cells[1, 3] = "Vessel";
        //        xlWs.Cells[1, 4] = "Container";
        //        xlWs.Cells[1, 5] = "Ship Line";
        //        xlWs.Cells[1, 6] = "Eta";
        //        xlWs.Cells[1, 7] = "Subject";
        //        xlWs.Cells[1, 8] = "Detail";
        //        xlWs.Cells[1, 9] = "Created";
        //        int nOutRow = 2;
        //        foreach (Guid g in dict.Values)
        //        {
        //            sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //            sqlCmd.Parameters.AddWithValue("@JobId", g);
        //            if (sType.Length > 0)
        //            {
        //                sqlCmd.Parameters.AddWithValue("@NoteType", sType);
        //            }
        //            cReader = sqlCmd.ExecuteReader();
        //            while (cReader.Read())
        //            {
        //                xlWs.Cells[nOutRow, 1] = cReader["PONum"].ToString();
        //                xlWs.Cells[nOutRow, 2] = cReader["ShipRef"].ToString();
        //                xlWs.Cells[nOutRow, 3] = cReader["VesselVoyage"].ToString();
        //                xlWs.Cells[nOutRow, 4] = cReader["ContainerNo"].ToString();
        //                xlWs.Cells[nOutRow, 5] = cReader["ShippingLine"].ToString();
        //                xlWs.Cells[nOutRow, 6] = cReader["EtaPortDate"].ToString();
        //                xlWs.Cells[nOutRow, 7] = cReader["Subject"].ToString();
        //                xlWs.Cells[nOutRow, 8] = cReader["Detail"].ToString();
        //                xlWs.Cells[nOutRow, 9] = cReader["Created"].ToString();
        //                nOutRow++;
        //            }
        //            cReader.Close();
        //        }

        //        if (bIsNew || !xlApp.Visible)
        //        {
        //            xlApp.Visible = true;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    finally
        //    {
        //    }
        //}


        //public static void ExcelExport(DataTable d, string sIntoString, ExcelFormat fmt)
        //{
        //    Excel.Application xlApp = null;
        //    Excel.Workbook xlWb;
        //    Excel.Worksheet xlWs;
        //    bool bIsNew = false;
        //    Forms.dlgStatusUpdateBox dlg = new MasterfilesWorkbench.Forms.dlgStatusUpdateBox();
        //    int nOutputStart = 0;
        //    int nOutRow;
        //    int nTitleRow;
        //    int nColHeadingRow;
        //    int nColIdRow;


        //    try
        //    {
        //        dlg.SetProgressBounds(1, d.Rows.Count + 2);
        //        object misValue = System.Reflection.Missing.Value;
        //        try
        //        {
        //            xlApp = (Excel.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application");
        //        }
        //        catch (System.Runtime.InteropServices.COMException)
        //        {
        //        }
        //        if (xlApp == null)
        //        {
        //            xlApp = new Excel.ApplicationClass();
        //            bIsNew = true;
        //        }
        //        xlWb = xlApp.Workbooks.Add(misValue);

        //        xlWs = (Excel.Worksheet)xlWb.Worksheets.get_Item(1);

        //        dlg.Show();

        //        if (fmt.IncludeValidationHeader)
        //        {
        //            xlWs.Cells[1, 1] = "'MASTERFILES_EXTRACT";
        //            ((Microsoft.Office.Interop.Excel.Range)xlWs.Cells[1, 1]).EntireRow.Hidden = true;
        //        }
        //        if (fmt.IncludeIdRow)
        //        {
        //            nColIdRow = 1 + (fmt.IncludeValidationHeader ? 1 : 0);
        //            int nCol = 1;
        //            foreach (DataColumn col in d.Columns)
        //            {
        //                xlWs.Cells[nColIdRow, nCol] = col.Caption;
        //                nCol++;
        //            }
        //            ((Microsoft.Office.Interop.Excel.Range)xlWs.Cells[nColIdRow, nCol]).EntireRow.Hidden = true;
        //        }
        //        if (fmt.IncludeHeaderRow)
        //        {
        //            nTitleRow = 1 + (fmt.IncludeValidationHeader ? 1 : 0) + (fmt.IncludeIdRow ? 1 : 0);
        //            xlWs.Cells[nTitleRow, 1] = "'" + fmt.Title;
        //            ((Microsoft.Office.Interop.Excel.Range)xlWs.Cells[nTitleRow, 1]).Font.Size = 16;
        //        }
        //        nColHeadingRow = 1 + (fmt.IncludeHeaderRow ? 1 : 0) + (fmt.IncludeIdRow ? 1 : 0) + (fmt.IncludeValidationHeader ? 1 : 0);
        //        {
        //            int nCol = 1;
        //            // First, set up default headings
        //            foreach (DataColumn col in d.Columns)
        //            {
        //                xlWs.Cells[nColHeadingRow, nCol] = col.Caption;
        //                nCol++;
        //            }
        //        }

        //        nOutputStart = nColHeadingRow + 1;
        //        nOutRow = nOutputStart;

        //        int nMaxCols = d.Columns.Count;
        //        foreach (DataRow row in d.Rows)
        //        {
        //            for (int x = 0; x < nMaxCols; x++)
        //            {
        //                if (d.Columns[x].DataType == typeof(string) || d.Columns[x].DataType == typeof(Guid))
        //                {
        //                    xlWs.Cells[nOutRow, x + 1] = "'" + row[x].ToString();
        //                }
        //                else
        //                {
        //                    xlWs.Cells[nOutRow, x + 1] = row[x];
        //                }
        //            }
        //            nOutRow++;
        //            dlg.UpdateStatus("Exporting row: " + (nOutRow - nColHeadingRow).ToString());
        //            dlg.UpdateProgress(nOutRow - nColHeadingRow);
        //        }
        //        dlg.CloseMe();

        //        // Now check the the sInto parameter, and if needed, run through for Column Heading Overrides, and special formatting (widths etc)
        //        if (sIntoString.Length > 0)
        //        {
        //            long nCol = 0;
        //            string[] columns = sIntoString.Split(',');
        //            foreach (string sCol in columns)
        //            {
        //                nCol++;
        //                string[] sTokens = sCol.Trim().Split(';');
        //                foreach (string sToken in sTokens)
        //                {
        //                    switch (sToken.Substring(0, 1))
        //                    {
        //                        case "|":
        //                            xlWs.Cells[nColHeadingRow, nCol] = "'" + sToken.Substring(1);
        //                            break;
        //                        case "=":
        //                            {
        //                                double nTemp = Convert.ToDouble(sToken.Substring(1));
        //                                if (nTemp == 0)
        //                                {
        //                                    ((Microsoft.Office.Interop.Excel.Range)xlWs.Cells[nColHeadingRow, nCol]).EntireColumn.Hidden = true;
        //                                }
        //                                else
        //                                {
        //                                    ((Microsoft.Office.Interop.Excel.Range)xlWs.Cells[nColHeadingRow, nCol]).EntireColumn.ColumnWidth = nTemp;
        //                                }
        //                            }
        //                            break;
        //                        case "[":
        //                            ((Microsoft.Office.Interop.Excel.Range)xlWs.Cells[nColHeadingRow, nCol]).EntireColumn.NumberFormat = sToken.Substring(1);
        //                            break;
        //                        default:
        //                            break;
        //                    }
        //                }
        //            }
        //        }
        //        if (fmt.SetBorders == true)
        //        {
        //            Microsoft.Office.Interop.Excel.Range dataCells = (Microsoft.Office.Interop.Excel.Range)xlWs.get_Range(xlWs.Cells[nColHeadingRow, 1], xlWs.Cells[nOutRow - 1, nMaxCols]);
        //            dataCells.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
        //            dataCells.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
        //            dataCells.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
        //            dataCells.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
        //            dataCells.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideHorizontal].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
        //            dataCells.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideVertical].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
        //            //((Microsoft.Office.Interop.Excel.Range)xlWs.Cells[nColHeadingRow, nColHeadingRow]).EntireColumn.NumberFormat = sToken.Substring(1);
        //        }

        //        if (bIsNew || !xlApp.Visible)
        //        {
        //            xlApp.Visible = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Filthy Pig!", MessageBoxButtons.OK);
        //        if (xlApp != null)
        //        {
        //            xlApp.Visible = true;
        //        }
        //        dlg.CloseMe();
        //    }
        //    finally
        //    {
        //    }
        //}


        //public static Forms.frmEditJob CloneRowWithForm(Guid gOld)
        //{
        //    Guid gNew = CloneRow(gOld);

        //    Forms.frmEditJob frm = new Forms.frmEditJob(gNew.ToString());
        //    return frm;
        //}

        //public static bool DeleteJob(Guid gId)
        //{
        //    string sSql = "";
        //    SqlCommand sqlCmd;

        //    try
        //    {
        //        sSql = "delete from job where JOB_ID = @JobId";
        //        sqlCmd = new SqlCommand();
        //        sqlCmd.Connection = SqlHost.DBConn;
        //        sqlCmd.CommandText = sSql;
        //        sqlCmd.Parameters.AddWithValue("@JobId", gId);
        //        sqlCmd.ExecuteNonQuery();

        //        return RemoveAllJobTracking(gId);
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        //public static Guid CloneRow(Guid gOld, string sResetList)
        //{
        //    Guid g = CloneRow(gOld);
        //    ResetJobFields(g, sResetList);
        //    return g;
        //}


        //public static Guid CloneRow(Guid gOld)     // MDS HERE 20190418
        //{
        //    string sSql;
        //    SqlCommand sqlCmd;
        //    SqlDataReader cReader;

        //    string sSqlInsert;
        //    SqlCommand sqlCmdInsert;
        //    Guid gNew = Guid.NewGuid();

        //    sSql = "select JOB_ID as JobId, ORDER_DATE as OrderDate, PO_NUM as PONum, SHIP_TYPE as ShipType, " +
        //        "SOURCE_TYPE as SourceType, JOB_STATUS as JobStatus, SHIP_REF as ShipRef, JOB_TYPE as JobType, " +
        //        "SUPPLIER_ID as SupplierId, SUPPLIER_NAME as SupplierName, INVOICE_CUST_ID as InvoiceCustId, " +
        //        "INVOICE_CUST_NAME as InvoiceCustName, JOB_CUST_TERMS as JobCustTerms, JOB_SUPP_TERMS as JobSuppTerms, " +
        //        "CUST_PAY_TERMS as CustPayTerms, CUST_DEPOSIT as CustDeposit, SUPP_DEPOSIT as SuppDeposit, " +
        //        "CONSIGNEE as Consignee, INVOICED_FROM as InvoicedFrom, DEST_PORT as DestPort, " +
        //        "ETD_SHIP_DATE as EtdShipDate, ETD_CONFIRMED as EtdConfirmed, ETA_PORT_DATE as EtaPortDate, " +
        //        "ETA_CONFIRMED as EtaConfirmed, CUST_REQ_DATE_START as CustReqDateStart, CUST_REQ_DATE_END as CustReqDateEnd, " +
        //        "VESSEL_VOYAGE as VesselVoyage, VESSEL_CONFIRMED as VesselConfirmed, PROMO_ORDER_CUBICS as PromoOrderCubics, " +
        //        "SCHEDULED_PROMO as ScheduledPromo, FCL as FCL,CUST_PO as CustPo, SUPP_PI as SuppPi, " +
        //        "CONTAINER_NO as ContainerNo, DELIVERY_CUST as DeliveryCust, DELIVERY_CUST_ADDR as DeliveryCustAddr, " +
        //        "DEL_ADDR_1 as DellAddr1, DEL_ADDR_2 as DelAddr2, DEL_ADDR_3 as DelAddr3, DEL_ADDR_STATE as DelAddrState, " +
        //        "DEL_ADDR_POSTCODE as DelAddrPostCode, DEL_ADDR_COUNTRY as DelAddrCountry, DROP_CUST as DropCust, " +
        //        "DROP_CUST_ADDR as DropCustAddr, DROP_ADDR_1 as DropAddr1, DROP_ADDR_2 as DropAddr2, DROP_ADDR_3 as DropAddr3, " +
        //        "DROP_ADDR_STATE as DropAddrState, DROP_ADDR_POSTCODE as DropAddrPostcode, DROP_ADDR_COUNTRY as DropAddrCountry, " +
        //        "SHIPPING_LINE as ShippingLine, GOODS_DESC as GoodsDesc, PORT_OF_ORIGIN as PortOfOrigin, " +
        //        "SUPP_INV_NO as SuppInvNo, SUPP_INV_VAL as SuppInvVal, PO_CURRENCY as PoCurrency, PO_EXCHANGE as PoExchange, " +
        //        "QUOTED_MARGIN as QuotedMargin, CUST_INV_VAL as CustInvVal, SALES_CURRENCY as SalesCurrency, BOSS as Boss, " +
        //        "CONT_TRACK_FIRST_AVL as ContTrackFirstAvl, CONT_TRACK_DET_START as ContTrackDetStart, " +
        //        "CONT_TRACK_EMPTY_RET as ContTrackEmptyRet, MERGE_DATE as MergeDate, " +
        //        "FIRST_DROP_DEL_DATE as FirstDropDelDate, CUST_DEL_DATE as CustDelDate, PL_SENT_TO_CUST as PLSentToCust, " +
        //        "INV_SENT_TO_CUST as InvSentToCust, CUST_INV_IN_MYOB as CustInvInMyob, SUPP_INV_IN_MYOB as SuppInvInMyob, " +
        //        "MYOB_VERSION as MyobVersion, DOCS_TO_CUSTOMS as DocsToCustoms, INV_CRE_USER as InvCreUser, " +
        //        "CBA_RCPT_BATCH as CbaRcptBatch, CBA_INV_BATCH as CbaInvBatch, CBA_INV_NO as CbaInvNo, CBA_OUTPUT as CbaOutput, " +
        //        "DUTY_STATUS as DutyStatus, CARTAGE_STATUS as CartageStatus, DANGEROUS_GOODS as DangerousGoods, " +
        //        "THIRD_PARTY_LOGISTICS as ThirdPartyLogistics, COMPLETED as Completed, MYOB_CUST_INV_CRE as MyobCustInvCre, " +
        //        "CONSOL_TRACK as ConsolTrack, LOGISTICS_PRIORITY as LogsPriority, REPORT_EXCLUSION as ReportExclusion, " +
        //        "SEAL_NO as SealNo, CUSTOMER_INVOICE_TYPE as CustomerInvoiceType, " +
        //        "CUSTOMER_PROFORMA_TYPE as CustomerProformaType, CONTEXT as Context, JOB_DETAILS as JobDetails, " +
        //        "BUYING_TEAM as BuyingTeam , SUPP_PROFORMA_RECD as SuppProformaRecd, SUPP_INVOICE_RECD as SuppInvoiceRecd, " +
        //        "SUPP_PACKINGLIST_RECD as SuppPackinglistRecd, SUPP_BLFCR_RECD as SuppBlfcrRecd, " +
        //        "SUPP_PACKINGDEC_RECD as SuppPackingdecRecd, SUPP_MANDEC_RECD as SuppMandecRecd, " +
        //        "SUPP_FUMO_RECD as SuppFumoRecd, SUPP_FUMOSTORAGE_RECD as SuppFumostorageRecd, SUPP_COO_RECD as SuppCooRecd, " +
        //        "SUPP_TELEX_REQUESTED as SuppTelexRequested, SUPP_SHIPPER_BOOKING_CONF_RECD as SuppShipperBookingConfRecd, " +
        //        "CUST_PO_RECD as CustPoRecd, DOCS_MERGED_BY as DocsMergedBy, STOCK_READY_BY as StockReadyBy, " +
        //        "WHARF_CUTOFF as WharfCutoff, ORIGINAL_ETD as OriginalEtd, CONTAINER_NETTING_REQ as ContainerNettingReq, " +
        //        "CONTAINER_NETTING_RECD as ContainerNettingRecd, SHIPMENT_BOOKING_REQ as ShipmentBookingReq, " +
        //        "SHIPMENT_BOOKING_REQ_DATE as ShipmentBookingReqDate, SHIPPING_DOCS_REQ as ShippingDocsReq, " +
        //        "SHIPPING_DOCS_REQ_DATE as ShippingDocsReqDate, FSC_RECD as FscRecd, CUST_PI_SENT as CustPiSent, " +
        //        "SHIP_SO_NUMBER as ShipSONumber, TIMBER_COMPLIANCE as TimberCompliance, " +
        //        "EXT_STR_1 as ExtStr1, EXT_STR_2 as ExtStr2, EXT_STR_3 as ExtStr3, EXT_STR_4 as ExtStr4, " +
        //        "EXT_STR_5 as ExtStr5, EXT_STR_6 as ExtStr6, EXT_STR_7 as ExtStr7, EXT_STR_8 as ExtStr8, " +
        //        "EXT_NOTE_1 as ExtNote1, EXT_NUM_1 as ExtNum1, EXT_NUM_2 as ExtNum2, EXT_NUM_3 as ExtNum3, " +
        //        "EXT_DATE_1 as ExtDate1, EXT_DATE_2 as ExtDate2, EXT_DATE_3 as ExtDate3, " +
        //        "EXT_BOOL_1 as ExtBool1, EXT_BOOL_2 as ExtBool2, EXT_BOOL_3 as ExtBool3, " +
        //        "OMT_PASSED as OMTPassed " +
        //        "from JOB where JOB_ID = @OldId";

        //    sSqlInsert = "insert into JOB ( " +
        //        "JOB_ID, ORDER_DATE, PO_NUM, SHIP_TYPE, SOURCE_TYPE, JOB_STATUS, SHIP_REF, " +
        //        "JOB_TYPE, SUPPLIER_ID, SUPPLIER_NAME, INVOICE_CUST_ID, INVOICE_CUST_NAME, " +
        //        "JOB_CUST_TERMS, JOB_SUPP_TERMS, CUST_PAY_TERMS, CUST_DEPOSIT, SUPP_DEPOSIT, " +
        //        "CONSIGNEE, INVOICED_FROM, DEST_PORT, ETD_SHIP_DATE, ETD_CONFIRMED, ETA_PORT_DATE, " +
        //        "ETA_CONFIRMED, CUST_REQ_DATE_START, CUST_REQ_DATE_END, VESSEL_VOYAGE, " +
        //        "VESSEL_CONFIRMED, PROMO_ORDER_CUBICS, SCHEDULED_PROMO, FCL, CUST_PO, SUPP_PI, " +
        //        "CONTAINER_NO, DELIVERY_CUST, DELIVERY_CUST_ADDR, DEL_ADDR_1, DEL_ADDR_2, " +
        //        "DEL_ADDR_3, DEL_ADDR_STATE, DEL_ADDR_POSTCODE, DEL_ADDR_COUNTRY, DROP_CUST, " +
        //        "DROP_CUST_ADDR, DROP_ADDR_1, DROP_ADDR_2, DROP_ADDR_3, DROP_ADDR_STATE, " +
        //        "DROP_ADDR_POSTCODE, DROP_ADDR_COUNTRY, SHIPPING_LINE, GOODS_DESC, PORT_OF_ORIGIN, " +
        //        "SUPP_INV_NO, SUPP_INV_VAL, PO_CURRENCY, PO_EXCHANGE, QUOTED_MARGIN, CUST_INV_VAL, " +
        //        "SALES_CURRENCY, BOSS, CONT_TRACK_FIRST_AVL, CONT_TRACK_DET_START, CONT_TRACK_EMPTY_RET, " +
        //        "MERGE_DATE, FIRST_DROP_DEL_DATE, CUST_DEL_DATE, PL_SENT_TO_CUST, " +
        //        "INV_SENT_TO_CUST, CUST_INV_IN_MYOB, SUPP_INV_IN_MYOB, MYOB_VERSION, DOCS_TO_CUSTOMS, " +
        //        "INV_CRE_USER, CBA_RCPT_BATCH, CBA_INV_BATCH, CBA_INV_NO, CBA_OUTPUT, DUTY_STATUS, " +
        //        "CARTAGE_STATUS, DANGEROUS_GOODS, THIRD_PARTY_LOGISTICS, COMPLETED, MYOB_CUST_INV_CRE, " +
        //        "CONSOL_TRACK, LOGISTICS_PRIORITY, REPORT_EXCLUSION, SEAL_NO, CUSTOMER_INVOICE_TYPE, " +
        //        "CUSTOMER_PROFORMA_TYPE, CONTEXT, JOB_DETAILS, BUYING_TEAM, SUPP_PROFORMA_RECD, " +
        //        "SUPP_INVOICE_RECD, SUPP_PACKINGLIST_RECD, SUPP_BLFCR_RECD, SUPP_PACKINGDEC_RECD, " +
        //        "SUPP_MANDEC_RECD, SUPP_FUMO_RECD, SUPP_FUMOSTORAGE_RECD, SUPP_COO_RECD, " +
        //        "SUPP_TELEX_REQUESTED, SUPP_SHIPPER_BOOKING_CONF_RECD, CUST_PO_RECD, DOCS_MERGED_BY, " +
        //        "STOCK_READY_BY, WHARF_CUTOFF, ORIGINAL_ETD, CONTAINER_NETTING_REQ, CONTAINER_NETTING_RECD, " +
        //        "SHIPMENT_BOOKING_REQ, SHIPMENT_BOOKING_REQ_DATE, SHIPPING_DOCS_REQ, SHIPPING_DOCS_REQ_DATE, " +
        //        "FSC_RECD, CUST_PI_SENT, SHIP_SO_NUMBER, TIMBER_COMPLIANCE, EXT_STR_1, EXT_STR_2, " +
        //        "EXT_STR_3, EXT_STR_4, EXT_STR_5, EXT_STR_6, EXT_STR_7, EXT_STR_8, EXT_NOTE_1, " +
        //        "EXT_NUM_1, EXT_NUM_2, EXT_NUM_3, EXT_DATE_1, EXT_DATE_2, EXT_DATE_3, " +
        //        "EXT_BOOL_1, EXT_BOOL_2, EXT_BOOL_3, OMT_PASSED " +
        //        ") VALUES ( " +
        //        "@JobId, @OrderDate, @PONum, NULL, @SourceType, @JobStatus, @ShipRef, @JobType, " +
        //        "@SupplierId, @SupplierName, @InvoiceCustId, @InvoiceCustName, @JobCustTerms, " +
        //        "@JobSuppTerms, @CustPayTerms, @CustDeposit, @SuppDeposit, @Consignee, @InvoicedFrom, " +
        //        "@DestPort, @EtdShipDate, @EtdConfirmed, @EtaPortDate, @EtaConfirmed, @CustReqDateStart, " +
        //        "@CustReqDateEnd, @VesselVoyage, @VesselConfirmed, @PromoOrderCubics, @ScheduledPromo, " +
        //        "@FCL, @CustPo, @SuppPi, @ContainerNo, @DeliveryCust, @DeliveryCustAddr, @DellAddr1, " +
        //        "@DelAddr2, @DelAddr3, @DelAddrState, @DelAddrPostCode, @DelAddrCountry, @DropCust, " +
        //        "@DropCustAddr, @DropAddr1, @DropAddr2, @DropAddr3, @DropAddrState, @DropAddrPostcode, " +
        //        "@DropAddrCountry, @ShippingLine, @GoodsDesc, @PortOfOrigin, @SuppInvNo, @SuppInvVal, " +
        //        "@PoCurrency, @PoExchange, @QuotedMargin, @CustInvVal, @SalesCurrency, @Boss, " +
        //        "@ContTrackFirstAvl, @ContTrackDetStart, @ContTrackEmptyRet, " +
        //        "@MergeDate, @FirstDropDelDate, @CustDelDate, @PLSentToCust, @InvSentToCust, " +
        //        "@CustInvInMyob, @SuppInvInMyob, @MyobVersion, @DocsToCustoms, @InvCreUser, " +
        //        "@CbaRcptBatch, @CbaInvBatch, @CbaInvNo, @CbaOutput, @DutyStatus, @CartageStatus, " +
        //        "@DangerousGoods, @ThirdPartyLogistics, @Completed, @MyobCustInvCre, " +
        //        "@ConsolTrack, @LogsPriority, @ReportExclusion, @SealNo, @CustomerInvoiceType, " +
        //        "@CustomerProformaType, @Context, @JobDetails, @BuyingTeam, @SuppProformaRecd, " +
        //        "@SuppInvoiceRecd, @SuppPackinglistRecd, @SuppBlfcrRecd, @SuppPackingdecRecd, " +
        //        "@SuppMandecRecd, @SuppFumoRecd, @SuppFumostorageRecd, @SuppCooRecd, @SuppTelexRequested, " +
        //        "@SuppShipperBookingConfRecd, @CustPoRecd, @DocsMergedBy, @StockReadyBy, @WharfCutoff, " +
        //        "@OriginalEtd, @ContainerNettingReq, @ContainerNettingRecd, @ShipmentBookingReq, " +
        //        "@ShipmentBookingReqDate, @ShippingDocsReq, @ShippingDocsReqDate, @FscRecd, @CustPiSent, " +
        //        "@ShipSONumber, @TimberCompliance, @ExtStr1, @ExtStr2, @ExtStr3, @ExtStr4, @ExtStr5, " +
        //        "@ExtStr6, @ExtStr7, @ExtStr8, @ExtNote1, @ExtNum1, @ExtNum2, @ExtNum3, @ExtDate1, " +
        //        "@ExtDate2, @ExtDate3, @ExtBool1, @ExtBool2, @ExtBool3, @OMTPassed " +
        //        ")";


        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@OldId", gOld);
        //    cReader = sqlCmd.ExecuteReader();

        //    sqlCmdInsert = new SqlCommand();
        //    sqlCmdInsert.CommandText = sSqlInsert;

        //    while (cReader.Read())
        //    {
        //        sqlCmdInsert.Parameters.AddWithValue("@JobId", gNew);
        //        sqlCmdInsert.Parameters.AddWithValue("@OrderDate", cReader["OrderDate"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@PONum", cReader["PONum"]);
        //        // MDS - Changed so that TRANS shipments would not be copied.
        //        //sqlCmdInsert.Parameters.AddWithValue("@ShipType", cReader["ShipType"]);
        //        //sqlCmdInsert.Parameters.AddWithValue("@ShipType", Constants.JOB_SHIPTYPE_DIRECT);
        //        //sqlCmdInsert.Parameters.AddWithValue("@ShipType", null);
        //        sqlCmdInsert.Parameters.AddWithValue("@SourceType", cReader["SourceType"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@JobStatus", cReader["JobStatus"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ShipRef", cReader["ShipRef"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@JobType", cReader["JobType"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@SupplierId", cReader["SupplierId"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@SupplierName", cReader["SupplierName"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@InvoiceCustId", cReader["InvoiceCustId"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@InvoiceCustName", cReader["InvoiceCustName"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@JobCustTerms", cReader["JobCustTerms"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@JobSuppTerms", cReader["JobSuppTerms"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@CustPayTerms", cReader["CustPayTerms"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@CustDeposit", cReader["CustDeposit"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@SuppDeposit", cReader["SuppDeposit"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@Consignee", cReader["Consignee"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@InvoicedFrom", cReader["InvoicedFrom"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DestPort", cReader["DestPort"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@EtdShipDate", cReader["EtdShipDate"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@EtdConfirmed", cReader["EtdConfirmed"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@EtaPortDate", cReader["EtaPortDate"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@EtaConfirmed", cReader["EtaConfirmed"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@CustReqDateStart", cReader["CustReqDateStart"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@CustReqDateEnd", cReader["CustReqDateEnd"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@VesselVoyage", cReader["VesselVoyage"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@VesselConfirmed", cReader["VesselConfirmed"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@PromoOrderCubics", cReader["PromoOrderCubics"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ScheduledPromo", cReader["ScheduledPromo"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@FCL", cReader["FCL"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@CustPo", cReader["CustPo"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@SuppPi", cReader["SuppPi"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ContainerNo", cReader["ContainerNo"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DeliveryCust", cReader["DeliveryCust"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DeliveryCustAddr", cReader["DeliveryCustAddr"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DellAddr1", cReader["DellAddr1"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DelAddr2", cReader["DelAddr2"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DelAddr3", cReader["DelAddr3"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DelAddrState", cReader["DelAddrState"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DelAddrPostCode", cReader["DelAddrPostCode"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DelAddrCountry", cReader["DelAddrCountry"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DropCust", cReader["DropCust"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DropCustAddr", cReader["DropCustAddr"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DropAddr1", cReader["DropAddr1"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DropAddr2", cReader["DropAddr2"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DropAddr3", cReader["DropAddr3"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DropAddrState", cReader["DropAddrState"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DropAddrPostcode", cReader["DropAddrPostcode"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DropAddrCountry", cReader["DropAddrCountry"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ShippingLine", cReader["ShippingLine"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@GoodsDesc", cReader["GoodsDesc"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@PortOfOrigin", cReader["PortOfOrigin"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@SuppInvNo", cReader["SuppInvNo"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@SuppInvVal", cReader["SuppInvVal"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@PoCurrency", cReader["PoCurrency"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@PoExchange", cReader["PoExchange"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@QuotedMargin", cReader["QuotedMargin"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@CustInvVal", cReader["CustInvVal"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@SalesCurrency", cReader["SalesCurrency"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@Boss", cReader["Boss"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ContTrackFirstAvl", cReader["ContTrackFirstAvl"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ContTrackDetStart", cReader["ContTrackDetStart"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ContTrackEmptyRet", cReader["ContTrackEmptyRet"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@MergeDate", cReader["MergeDate"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@FirstDropDelDate", cReader["FirstDropDelDate"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@CustDelDate", cReader["CustDelDate"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@PLSentToCust", cReader["PLSentToCust"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@InvSentToCust", cReader["InvSentToCust"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@CustInvInMyob", cReader["CustInvInMyob"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@SuppInvInMyob", cReader["SuppInvInMyob"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@MyobVersion", cReader["MyobVersion"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DocsToCustoms", cReader["DocsToCustoms"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@InvCreUser", cReader["InvCreUser"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@CbaRcptBatch", cReader["CbaRcptBatch"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@CbaInvBatch", cReader["CbaInvBatch"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@CbaInvNo", cReader["CbaInvNo"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@CbaOutput", cReader["CbaOutput"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DutyStatus", cReader["DutyStatus"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@CartageStatus", cReader["CartageStatus"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DangerousGoods", cReader["DangerousGoods"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ThirdPartyLogistics", cReader["ThirdPartyLogistics"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@Completed", cReader["Completed"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@MyobCustInvCre", cReader["MyobCustInvCre"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ConsolTrack", cReader["ConsolTrack"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@LogsPriority", cReader["LogsPriority"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ReportExclusion", cReader["ReportExclusion"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@SealNo", cReader["SealNo"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@CustomerInvoiceType", cReader["CustomerInvoiceType"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@CustomerProformaType", cReader["CustomerProformaType"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@Context", cReader["Context"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@JobDetails", cReader["JobDetails"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@BuyingTeam", cReader["BuyingTeam"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@SuppProformaRecd", cReader["SuppProformaRecd"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@SuppInvoiceRecd", cReader["SuppInvoiceRecd"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@SuppPackinglistRecd", cReader["SuppPackinglistRecd"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@SuppBlfcrRecd", cReader["SuppBlfcrRecd"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@SuppPackingdecRecd", cReader["SuppPackingdecRecd"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@SuppMandecRecd", cReader["SuppMandecRecd"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@SuppFumoRecd", cReader["SuppFumoRecd"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@SuppFumostorageRecd", cReader["SuppFumostorageRecd"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@SuppCooRecd", cReader["SuppCooRecd"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@SuppTelexRequested", cReader["SuppTelexRequested"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@SuppShipperBookingConfRecd", cReader["SuppShipperBookingConfRecd"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@CustPoRecd", cReader["CustPoRecd"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@DocsMergedBy", cReader["DocsMergedBy"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@StockReadyBy", cReader["StockReadyBy"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@WharfCutoff", cReader["WharfCutoff"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@OriginalEtd", cReader["OriginalEtd"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ContainerNettingReq", cReader["ContainerNettingReq"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ContainerNettingRecd", cReader["ContainerNettingRecd"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ShipmentBookingReq", cReader["ShipmentBookingReq"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ShipmentBookingReqDate", cReader["ShipmentBookingReqDate"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ShippingDocsReq", cReader["ShippingDocsReq"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ShippingDocsReqDate", cReader["ShippingDocsReqDate"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@FscRecd", cReader["FscRecd"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@CustPiSent", cReader["CustPiSent"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ShipSONumber", cReader["ShipSONumber"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@TimberCompliance", cReader["TimberCompliance"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ExtStr1", cReader["ExtStr1"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ExtStr2", cReader["ExtStr2"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ExtStr3", cReader["ExtStr3"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ExtStr4", cReader["ExtStr4"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ExtStr5", cReader["ExtStr5"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ExtStr6", cReader["ExtStr6"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ExtStr7", cReader["ExtStr7"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ExtStr8", cReader["ExtStr8"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ExtNote1", cReader["ExtNote1"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ExtNum1", cReader["ExtNum1"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ExtNum2", cReader["ExtNum2"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ExtNum3", cReader["ExtNum3"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ExtDate1", cReader["ExtDate1"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ExtDate2", cReader["ExtDate2"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ExtDate3", cReader["ExtDate3"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ExtBool1", cReader["ExtBool1"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ExtBool2", cReader["ExtBool2"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@ExtBool3", cReader["ExtBool3"]);
        //        sqlCmdInsert.Parameters.AddWithValue("@OMTPassed", cReader["OMTPassed"]);
        //    }
        //    cReader.Close();
        //    sqlCmd = null;
        //    sqlCmdInsert.Connection = SqlHost.DBConn;
        //    sqlCmdInsert.ExecuteNonQuery();

        //    return gNew;
        //}

        // The following method accepts a list of updates for a particular job, FieldList should like the following:
        // <FIELDNAME>,<VALUE>,<DATATYPE>,<etc etc>.    EG: JOB_STATUS,510,string,SUPP_INVOICE_RECD,Y,string, etc etc
        //public static void ResetJobFields(Guid g, string sFieldList) 
        //{
        //    string sSqlUpdate;
        //    SqlCommand sqlCmdUpdate;
        //    string sFields;
        //    string[] sReset;

        //    if (sFieldList.Length == 0) return;

        //    sReset = sFieldList.Split(',');

        //    sFields = "";
        //    sSqlUpdate = "update job set ";
        //    for (int i = 0; i < sReset.Count(); i += 3)
        //    {
        //        if (sFields.Length > 0)
        //        {
        //            sFields = sFields + ", ";
        //        }
        //        sFields = sFields + sReset[i] + " = @" + sReset[i];
        //    }
        //    sSqlUpdate = sSqlUpdate + sFields + " where job_id = @JobId";

        //    sqlCmdUpdate = new SqlCommand();
        //    sqlCmdUpdate.CommandText = sSqlUpdate;

        //    sqlCmdUpdate.Parameters.AddWithValue("@JobId", g);
        //    for (int i = 0; i < sReset.Count(); i += 3)
        //    {
        //        if (sReset[i + 2] == "string" || sReset[i + 2] == "Guid")
        //        {
        //            sqlCmdUpdate.Parameters.AddWithValue("@" + sReset[i], sReset[i + 1]);
        //        }
        //        else if (sReset[i + 2] == "int")
        //        {
        //            int nValInt = Convert.ToInt32(sReset[i + 1]);
        //            sqlCmdUpdate.Parameters.AddWithValue("@" + sReset[i], nValInt);
        //        }
        //        else if (sReset[i + 2] == "long")
        //        {
        //            long nValLng = Convert.ToInt64(sReset[i + 1]);
        //            sqlCmdUpdate.Parameters.AddWithValue("@" + sReset[i], nValLng);
        //        }
        //        else if (sReset[i + 2] == "float")
        //        {
        //            float nValFlt = float.Parse(sReset[i + 1]);
        //            sqlCmdUpdate.Parameters.AddWithValue("@" + sReset[i], nValFlt);
        //        }
        //        else if (sReset[i + 2] == "bool")
        //        {
        //            bool bVal = Boolean.Parse(sReset[i + 1]);
        //            sqlCmdUpdate.Parameters.AddWithValue("@" + sReset[i], bVal);
        //        }
        //        else if (sReset[i + 2] == "DateTime")
        //        {
        //            DateTime dtVal = DateTime.Parse(sReset[i + 1]);
        //            sqlCmdUpdate.Parameters.AddWithValue("@" + sReset[i], dtVal);
        //        }
        //    }
        //    sqlCmdUpdate.Connection = SqlHost.DBConn;
        //    sqlCmdUpdate.ExecuteNonQuery();
        //}
        //public static void LogJobNote(Guid g, string sNoteType, string sSubject, string sDetail)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    sSql = "insert into JOB_NOTE ( NOTE_ID, JOB_ID, NOTE_TYPE, SUBJECT, DETAIL, USER_NAME, CREATED ) values " +
        //            "( NEWID(), @JobId, @NoteType, @Subject, @Detail, @UserName, @Created )";
        //    sqlCmd = null;
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@JobId", g);
        //    sqlCmd.Parameters.AddWithValue("@NoteType", sNoteType);
        //    sqlCmd.Parameters.AddWithValue("@Subject", sSubject);
        //    sqlCmd.Parameters.AddWithValue("@Detail", sDetail);
        //    sqlCmd.Parameters.AddWithValue("@UserName", AppHost.CurrentUser["UserName"].ToString());
        //    sqlCmd.Parameters.AddWithValue("@Created", DateTime.Now);
        //    sqlCmd.ExecuteNonQuery();
        //}

        //public static void ChangeJobDate(Guid g, string sColName, DateTime dt)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    sSql = "update JOB set " + sColName + " = @NewDate where JOB_ID = @JobId";
        //    sqlCmd = null;
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@JobId", g);
        //    sqlCmd.Parameters.AddWithValue("@NewDate", dt);
        //    sqlCmd.ExecuteNonQuery();
        //}

        //public static void ChangeJobStatus(Guid g, string sNewStatus)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    sSql = "update JOB set JOB_STATUS = @NewStatus where JOB_ID = @JobId";
        //    sqlCmd = null;
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@JobId", g);
        //    sqlCmd.Parameters.AddWithValue("@NewStatus", sNewStatus);
        //    sqlCmd.ExecuteNonQuery();
        //}

        //public static void MarkJobAsInvoiced(Guid g)
        //{

        //    string sSql;
        //    SqlCommand sqlCmd;
        //    sSql = "update JOB set " +
        //        "CUST_INV_IN_MYOB = @CustInv " +
        //        "where JOB_ID = @JobId";
        //    sqlCmd = null;
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@JobId", g);
        //    sqlCmd.Parameters.AddWithValue("@CustInv", Constants.JOB_UPDATE_CUSTINVINMYOB);
        //    //sqlCmd.Parameters.AddWithValue("@PLSent", Constants.JOB_UPDATE_PLSENTTOCUST);
        //    //sqlCmd.Parameters.AddWithValue("@InvSent", Constants.JOB_UPDATE_INVSENTTOCUST);
        //    sqlCmd.ExecuteNonQuery();
        //    sqlCmd = null;

        //}

        //public static void CheckETAContTracking(Guid g)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    SqlDataReader cReader;
        //    DateTime dtETA;
        //    DateTime dtNewContTrack = new DateTime(1901, 1, 1);
        //    sSql = "select ETA_PORT_DATE, ETA_CONFIRMED from JOB where JOB_ID = @JobId";
        //    sqlCmd = null;
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@JobId", g);
        //    cReader = sqlCmd.ExecuteReader();
        //    while (cReader.Read())
        //    {
        //        if (cReader["ETA_CONFIRMED"] != System.DBNull.Value && (DateTime)cReader["ETA_CONFIRMED"] > new DateTime(1901, 1, 1))
        //        {
        //            dtETA = (DateTime)cReader["ETA_PORT_DATE"];
        //            dtNewContTrack = dtETA.AddDays(2);
        //        }
        //    }
        //    cReader.Close();
        //    if (dtNewContTrack > new DateTime(1901, 1, 1))
        //    {
        //        sSql = "update JOB set CONT_TRACK_FIRST_AVL = @NewDate where JOB_ID = @JobId";
        //        sqlCmd = null;
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        sqlCmd.Parameters.AddWithValue("@JobId", g);
        //        sqlCmd.Parameters.AddWithValue("@NewDate", dtNewContTrack);
        //        sqlCmd.ExecuteNonQuery();
        //    }
        //}
        //                    bIsFOB = (((DataRowView)row.DataBoundItem).Row["JobCustTerms"].ToString() == Constants.JOB_JOBCUSTTERMS_FOB);

        //public static bool IsJobFOB(Guid g)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    string sJobCustTerms;
        //    SqlDataReader cReader;
        //    sSql = "select JOB_CUST_TERMS from JOB where JOB_ID = @JobId";
        //    sqlCmd = null;
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@JobId", g);
        //    cReader = sqlCmd.ExecuteReader();
        //    while (cReader.Read())
        //    {
        //        sJobCustTerms = cReader["JOB_CUST_TERMS"].ToString();
        //        if (sJobCustTerms == Constants.JOB_JOBCUSTTERMS_FOB)
        //        {
        //            cReader.Close();
        //            return true;
        //        }
        //        else
        //        {
        //            cReader.Close();
        //            return false;
        //        }
        //    }
        //    cReader.Close();
        //    return false;
        //}


        //public static int CompareETAETD(Guid g)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    SqlDataReader cReader;
        //    DateTime dtETD;
        //    DateTime dtETA;
        //    DateTime dtNewContTrack = new DateTime(1901, 1, 1);
        //    sSql = "select ETD_SHIP_DATE, ETA_PORT_DATE from JOB where JOB_ID = @JobId";
        //    sqlCmd = null;
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@JobId", g);
        //    cReader = sqlCmd.ExecuteReader();
        //    while (cReader.Read())
        //    {
        //        dtETD = (DateTime)cReader["ETD_SHIP_DATE"];
        //        dtETA = (DateTime)cReader["ETA_PORT_DATE"];
        //        //if (dtETD == dtETA)
        //        //{
        //        //    cReader.Close();
        //        //    return 30;
        //        //}
        //        //else
        //        //{
        //        //    cReader.Close();
        //        //    return ((TimeSpan)(dtETA - dtETD)).Days;
        //        //}
        //        if (dtETA == new DateTime(1901, 01, 01))
        //        {
        //            cReader.Close();
        //            return -99;
        //        }
        //        else
        //        {
        //            cReader.Close();
        //            return ((TimeSpan)(dtETA - dtETD)).Days;
        //        }

        //    }
        //    cReader.Close();
        //    return 30;
        //}
        //public static int CompareWharfCutoffETD(Guid g)
        //{
        //    SqlCommand sqlCmd;
        //    string sSql;
        //    SqlDataReader cReader;
        //    DateTime dtETD;
        //    DateTime dtWharf;
        //    DateTime dtNewContTrack = new DateTime(1901, 1, 1);
        //    sSql = "select ETD_SHIP_DATE, WHARF_CUTOFF from JOB where JOB_ID = @JobId";
        //    sqlCmd = null;
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@JobId", g);
        //    cReader = sqlCmd.ExecuteReader();
        //    while (cReader.Read())
        //    {
        //        dtETD = (DateTime)cReader["ETD_SHIP_DATE"];
        //        if (cReader["WHARF_CUTOFF"] != System.DBNull.Value)
        //        {
        //            dtWharf = (DateTime)cReader["WHARF_CUTOFF"];
        //        }
        //        else
        //        {
        //            dtWharf = new DateTime(1901, 1, 1);
        //        }
        //        //if (dtETD == dtETA)
        //        //{
        //        //    cReader.Close();
        //        //    return 30;
        //        //}
        //        //else
        //        //{
        //        //    cReader.Close();
        //        //    return ((TimeSpan)(dtETA - dtETD)).Days;
        //        //}
        //        if (dtWharf == new DateTime(1901, 01, 01))
        //        {
        //            cReader.Close();
        //            return -99;
        //        }
        //        else
        //        {
        //            cReader.Close();
        //            return ((TimeSpan)(dtWharf - dtETD)).Days;
        //        }

        //    }
        //    cReader.Close();
        //    return 30;
        //}


        //public static void LaunchOutlook()
        //{
        //    try
        //    {
        //        Outlook.Application appOutlook = null;

        //        if (Process.GetProcessesByName("OUTLOOK").Count() > 0)
        //        {
        //            appOutlook = (Outlook.Application)Marshal.GetActiveObject("Outlook.Application");
        //        }
        //        else
        //        {
        //            appOutlook = new Outlook.Application();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    finally
        //    {
        //    }
        //}

        //public static string ReadSignature()
        //{
        //    string appDataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Microsoft\\Signatures";
        //    string signature = string.Empty;
        //    DirectoryInfo diInfo = new DirectoryInfo(appDataDir);

        //    if (diInfo.Exists)
        //    {
        //        FileInfo[] fiSignature = diInfo.GetFiles("*.htm");

        //        if (fiSignature.Length > 0)
        //        {
        //            StreamReader sr = new StreamReader(fiSignature[0].FullName, Encoding.Default);
        //            signature = sr.ReadToEnd();

        //            if (!string.IsNullOrEmpty(signature))
        //            {
        //                string fileName = fiSignature[0].Name.Replace(fiSignature[0].Extension, string.Empty);
        //                signature = signature.Replace(fileName + "_files/", appDataDir + "/" + fileName + "_files/");
        //            }
        //        }
        //    }
        //    return signature;
        //}

        //public static void SendEmailGeneric(string sTo, string sSubject, string sBody)
        //{
        //    if (Constants.WORKBENCH_EMAIL_USEOUTLOOK)
        //    {
        //        Outlook.Application appOutlook = null;

        //        if (Process.GetProcessesByName("OUTLOOK").Count() > 0)
        //        {
        //            appOutlook = (Outlook.Application)Marshal.GetActiveObject("Outlook.Application");
        //        }
        //        else
        //        {
        //            appOutlook = new Outlook.Application();
        //        }
        //        Outlook.MailItem email = (Outlook.MailItem)(appOutlook.CreateItem(Outlook.OlItemType.olMailItem));
        //        object o = email.GetInspector;
        //        if (sTo.Length > 0)
        //            email.Recipients.Add(sTo);
        //        if (sSubject.Length > 0)
        //            email.Subject = sSubject;
        //        if (sBody.Length > 0)
        //            email.HTMLBody = sBody + email.HTMLBody;
        //        email.Display(false);
        //    }
        //    else
        //    {
        //        System.Diagnostics.Process.Start("mailto:" + sTo + "?subject=" + sSubject.Replace("&", "%26") + "&body=" + sBody.Replace("&", "%26"));
        //    }
        //}

        //public static void SendEmailAdminInvoiceHeader(Guid g)
        //{
        //    string sSql;
        //    SqlCommand sqlCmd;
        //    SqlDataReader cReader;
        //    sSql = "select SHIP_REF as ShipRef, CONTAINER_NO as ContainerNo, INVOICE_CUST_NAME as CustName, CONVERT(varchar, CUST_DEL_DATE, 103) as CustDelDate from job where JOB_ID = @JobId";
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@JobId", g);
        //    cReader = sqlCmd.ExecuteReader();
        //    while (cReader.Read())
        //    {
        //        string sTo = "";
        //        string sHeader = "Regarding " + cReader["ShipRef"].ToString() + " (Container: " + cReader["ContainerNo"].ToString() + ") for " + cReader["CustName"].ToString() + ", delivery on " + cReader["CustDelDate"].ToString();
        //        string sBody = "Customer Invoice has been entered into MYOB";
        //        SendEmailGeneric(sTo, sHeader, sBody);
        //    }
        //    cReader.Close();
        //}

        //public static void SendEmailGeneralBossNote(Guid g)
        //{
        //    string sSql;
        //    SqlCommand sqlCmd;
        //    SqlDataReader cReader;

        //    sSql = "select A.PO_NUM as PoNumber, A.SHIP_REF as ShipRef, A.INVOICE_CUST_NAME as CustName, B.EMAIL as EmailAddr " +
        //        "from job A inner join users B on A.BOSS = B.USER_NAME where JOB_ID = @JobId";
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@JobId", g);
        //    cReader = sqlCmd.ExecuteReader();
        //    while (cReader.Read())
        //    {
        //        string sEmailAddr = cReader["EmailAddr"].ToString();
        //        string sHeader;
        //        sHeader = "Order for " + cReader["CustName"].ToString() + " - PO Number: " + cReader["PoNumber"].ToString();
        //        if (cReader["ShipRef"] != System.DBNull.Value && cReader["ShipRef"].ToString().Trim().Length > 0)
        //            sHeader = sHeader + " (Shipment: " + cReader["ShipRef"].ToString().Trim() + " )";
        //        string sBody = "";

        //        SendEmailGeneric(sEmailAddr, sHeader, sBody);
        //    }
        //    cReader.Close();
        //}

        //public static void SendEmailCustomerPI(Guid g)
        //{
        //    string sSql;
        //    SqlCommand sqlCmd;
        //    SqlDataReader cReader;

        //    sSql = "select A.PO_NUM as PoNumber, A.INVOICE_CUST_NAME as CustName, A.GOODS_DESC as GoodsDesc, B.DESCRIPTION as Fcl, " +
        //        "A.DEL_ADDR_1 as Addr1, A.DEL_ADDR_2 as Addr2, A.DEL_ADDR_3 as Addr3, A.DEL_ADDR_STATE as DelState, A.DEL_ADDR_POSTCODE as PostCode, C.DESCRIPTION as Country " +
        //        "from job A left outer join REF_DATA B on A.FCL = B.REF_ID and B.REF_TABLE_ID = 'FCL' " +
        //        "left outer join REF_DATA C on A.DEL_ADDR_COUNTRY = C.REF_ID and C.REF_TABLE_ID = 'COUNTRY' " +
        //        "where JOB_ID = @JobId";
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@JobId", g);
        //    cReader = sqlCmd.ExecuteReader();
        //    while (cReader.Read())
        //    {
        //        //                string sNewLine = (Constants.WORKBENCH_EMAIL_USEOUTLOOK ? "\n" : "%0A");
        //        string sNewLine = (Constants.WORKBENCH_EMAIL_USEOUTLOOK ? "<br />" : "%0A");
        //        string sEmailAddr = "";
        //        string sHeader;
        //        sHeader = "UBA Proforma Invoice: " + cReader["PoNumber"].ToString() + " - " + cReader["GoodsDesc"];
        //        string sBody = sNewLine + "Hello." + sNewLine + "Please see attached pro-forma invoice relating to above mentioned Purchase Order." + sNewLine +
        //            "Details are as follows:" + sNewLine + sNewLine +
        //            "Purchase Order: " + cReader["PoNumber"].ToString() + sNewLine +
        //            "Container Size: " + cReader["Fcl"].ToString() + sNewLine +
        //            "Goods Description: " + cReader["GoodsDesc"].ToString() + sNewLine + sNewLine +
        //            "Delivery Details:" + sNewLine;
        //        if (cReader["Addr1"] != System.DBNull.Value && cReader["Addr1"].ToString().Length > 0)
        //        {
        //            sBody = sBody + cReader["Addr1"].ToString() + sNewLine;
        //        }
        //        if (cReader["Addr2"] != System.DBNull.Value && cReader["Addr2"].ToString().Length > 0)
        //        {
        //            sBody = sBody + cReader["Addr2"].ToString() + sNewLine;
        //        }
        //        if (cReader["Addr3"] != System.DBNull.Value && cReader["Addr3"].ToString().Length > 0)
        //        {
        //            sBody = sBody + cReader["Addr3"].ToString() + sNewLine;
        //        }
        //        if (cReader["DelState"] != System.DBNull.Value && cReader["DelState"].ToString().Length > 0)
        //        {
        //            sBody = sBody + cReader["DelState"].ToString() + sNewLine;
        //        }
        //        if (cReader["PostCode"] != System.DBNull.Value && cReader["PostCode"].ToString().Length > 0)
        //        {
        //            sBody = sBody + cReader["PostCode"].ToString() + sNewLine;
        //        }
        //        if (cReader["Country"] != System.DBNull.Value && cReader["Country"].ToString().Length > 0)
        //        {
        //            sBody = sBody + cReader["Country"].ToString() + sNewLine;
        //        }
        //        sBody = sBody + sNewLine + "Regards," + sNewLine + "Uncle Bills Australia";

        //        SendEmailGeneric(sEmailAddr, sHeader, sBody);
        //    }
        //    cReader.Close();
        //}

        //public static void SendEmailPackingList(Guid g)
        //{
        //    string sSql;
        //    SqlCommand sqlCmd;
        //    SqlDataReader cReader;
        //    sSql = "select " +
        //        "A.SHIP_REF as ShipRef, B.DESCRIPTION as Fcl, A.CONTAINER_NO as ContainerNo, " +
        //        "A.GOODS_DESC as GoodsDesc, CONVERT(varchar, A.ETA_PORT_DATE, 103) as EtaPortDate, " +
        //        "A.CUST_PO as CustomerPO " +
        //        "from job A " +
        //        "left outer join REF_DATA B on A.FCL = B.REF_ID and B.REF_TABLE_ID = 'FCL' " +
        //        "where A.JOB_ID = @JobId";
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@JobId", g);
        //    cReader = sqlCmd.ExecuteReader();
        //    while (cReader.Read())
        //    {
        //        //                string sNewLine = (Constants.WORKBENCH_EMAIL_USEOUTLOOK ? "\n" : "%0A");
        //        string sNewLine = (Constants.WORKBENCH_EMAIL_USEOUTLOOK ? "<br />" : "%0A");
        //        string sTo = "";
        //        string sHeader = cReader["ShipRef"].ToString() + " - Uncle Bills Shipment Notification";
        //        string sBody = sNewLine + "Hello." + sNewLine + "Please see attached Packing List relating to your recently shipped container. Details are:%0A%0A";
        //        sBody = sBody + "Ref: " + cReader["ShipRef"].ToString() + sNewLine;
        //        sBody = sBody + "Container Type: " + cReader["Fcl"].ToString() + sNewLine;
        //        sBody = sBody + "Container Number: " + cReader["ContainerNo"].ToString() + sNewLine;
        //        sBody = sBody + "Goods Description: " + cReader["GoodsDesc"].ToString() + sNewLine;
        //        sBody = sBody + "Approx. Port ETA: " + cReader["EtaPortDate"].ToString() + sNewLine;
        //        sBody = sBody + "Customer Purchase Order Number: " + cReader["CustomerPO"].ToString() + sNewLine + sNewLine;
        //        sBody = sBody + "If you have any queries, please contact me using the details below." + sNewLine + sNewLine;
        //        SendEmailGeneric(sTo, sHeader, sBody);
        //    }
        //    cReader.Close();
        //}

        //public static void SendEmailCustInvoice(Guid g)
        //{
        //    string sSql;
        //    SqlCommand sqlCmd;
        //    SqlDataReader cReader;
        //    sSql = "select " +
        //        "A.SHIP_REF as ShipRef, B.DESCRIPTION as Fcl, A.CONTAINER_NO as ContainerNo, " +
        //        "A.GOODS_DESC as GoodsDesc, CONVERT(varchar, A.CUST_DEL_DATE, 103) as CustDelDate, " +
        //        "A.CUST_PO as CustomerPO " +
        //        "from job A " +
        //        "left outer join REF_DATA B on A.FCL = B.REF_ID and B.REF_TABLE_ID = 'FCL' " +
        //        "where A.JOB_ID = @JobId";
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@JobId", g);
        //    cReader = sqlCmd.ExecuteReader();
        //    while (cReader.Read())
        //    {
        //        //                string sNewLine = (Constants.WORKBENCH_EMAIL_USEOUTLOOK ? "\n" : "%0A");
        //        string sNewLine = (Constants.WORKBENCH_EMAIL_USEOUTLOOK ? "<br />" : "%0A");
        //        string sTo = "";
        //        string sHeader = cReader["ShipRef"].ToString() + " - Uncle Bills Invoice";
        //        string sBody = sNewLine + "Hello." + sNewLine + "Please see attached Invoice relating to your recently booked/delivered container. Details are:" + sNewLine + sNewLine;
        //        sBody = sBody + "Ref: " + cReader["ShipRef"].ToString() + sNewLine;
        //        sBody = sBody + "Container Type: " + cReader["Fcl"].ToString() + sNewLine;
        //        sBody = sBody + "Container Number: " + cReader["ContainerNo"].ToString() + sNewLine;
        //        sBody = sBody + "Goods Description: " + cReader["GoodsDesc"].ToString() + sNewLine;
        //        sBody = sBody + "Delivery Date: " + cReader["CustDelDate"].ToString() + sNewLine;
        //        if (cReader["CustomerPO"] != System.DBNull.Value && cReader["CustomerPO"].ToString().Length > 0)
        //        {
        //            sBody = sBody + "Customer Purchase Order Number: " + cReader["CustomerPO"].ToString() + sNewLine + sNewLine;
        //        }
        //        else
        //        {
        //            sBody = sBody + sNewLine;
        //        }
        //        sBody = sBody + "If you have any queries, please contact me using the details below." + sNewLine + sNewLine;
        //        SendEmailGeneric(sTo, sHeader, sBody);
        //    }
        //    cReader.Close();
        //}

        // XXX MDS
        //public static void SendEmailShippingDocs(Guid g)
        //{
        //    string sSql;
        //    SqlCommand sqlCmd;
        //    SqlDataReader cReader;

        //    sSql = "select SHIP_REF as ShipRef, INVOICE_CUST_NAME as CustName, " +
        //        "CONTAINER_NO as ContainerNo, GOODS_DESC as GoodsDesc, " +
        //        "case when LEN(DROP_CUST) > 0 then DROP_CUST else DELIVERY_CUST end as DelCust, " +
        //        "case when LEN(DROP_CUST) > 0 then DROP_CUST_ADDR else DELIVERY_CUST_ADDR end as DelCustAddr, " +
        //        "case when LEN(DROP_CUST) > 0 then DROP_ADDR_1 else DEL_ADDR_1 end as Addr1, " +
        //        "case when LEN(DROP_CUST) > 0 then DROP_ADDR_2 else DEL_ADDR_2 end as Addr2, " +
        //        "case when LEN(DROP_CUST) > 0 then DROP_ADDR_3 else DEL_ADDR_3 end as Addr3, " +
        //        "case when LEN(DROP_CUST) > 0 then DROP_ADDR_STATE else DEL_ADDR_STATE end as AddrState, " +
        //        "case when LEN(DROP_CUST) > 0 then DROP_ADDR_POSTCODE else DEL_ADDR_POSTCODE end as PostCode, " +
        //        "case when LEN(CUST_PO) > 0 then CUST_PO else 'Not Given' end as CustPO " +
        //        "from job where JOB_ID = @JobId";
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@JobId", g);
        //    cReader = sqlCmd.ExecuteReader();
        //    while (cReader.Read())
        //    {
        //        string sEmailAddr = "";
        //        string sHeader;
        //        sHeader = cReader["ShipRef"].ToString() + " : " + cReader["CustName"].ToString() + " - New Job";
        //        //                string sNewLine = (Constants.WORKBENCH_EMAIL_USEOUTLOOK ? "\n" : "%0A");
        //        string sNewLine = (Constants.WORKBENCH_EMAIL_USEOUTLOOK ? "<br />" : "%0A");
        //        string sBody = sNewLine + "Hi Guys," + sNewLine + "Please find attached, new set of shipping docs relating to:" + sNewLine + sNewLine +
        //            "Ref: " + cReader["ShipRef"].ToString() + sNewLine +
        //            "Customer PO: " + cReader["CustPO"].ToString() + sNewLine +
        //            "Container Number: " + cReader["ContainerNo"].ToString() + sNewLine +
        //            "Goods Description: " + cReader["GoodsDesc"].ToString() + sNewLine +
        //            "Customer: " + cReader["CustName"].ToString() + sNewLine;
        //        sBody = sBody + sNewLine + "Delivery Address:" + sNewLine;
        //        if (cReader["Addr1"] != System.DBNull.Value && cReader["Addr1"].ToString().Length > 0)
        //        {
        //            sBody = sBody + cReader["Addr1"].ToString() + sNewLine;
        //        }
        //        if (cReader["Addr2"] != System.DBNull.Value && cReader["Addr2"].ToString().Length > 0)
        //        {
        //            sBody = sBody + cReader["Addr2"].ToString() + sNewLine;
        //        }
        //        if (cReader["Addr3"] != System.DBNull.Value && cReader["Addr3"].ToString().Length > 0)
        //        {
        //            sBody = sBody + cReader["Addr3"].ToString() + sNewLine;
        //        }
        //        if (cReader["AddrState"] != System.DBNull.Value && cReader["AddrState"].ToString().Length > 0)
        //        {
        //            sBody = sBody + cReader["AddrState"].ToString() + sNewLine;
        //        }
        //        if (cReader["PostCode"] != System.DBNull.Value && cReader["PostCode"].ToString().Length > 0)
        //        {
        //            sBody = sBody + cReader["PostCode"].ToString() + sNewLine;
        //        }
        //        sBody = sBody + sNewLine + "Thanks." + sNewLine;

        //        SendEmailGeneric(sEmailAddr, sHeader, sBody);
        //    }
        //    cReader.Close();
        //}




        //public static void UpdateJobPackingListSent(Guid g, string sValue)
        //{
        //    string sSql;
        //    SqlCommand sqlCmd;

        //    sSql = "update JOB set PL_SENT_TO_CUST = @NewValue where JOB_ID = @JobId";
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@JobId", g);
        //    sqlCmd.Parameters.AddWithValue("@NewValue", sValue);
        //    sqlCmd.ExecuteNonQuery();

        //}

        //public static void UpdateJobInvoiceSent(Guid g, string sValue)
        //{
        //    string sSql;
        //    SqlCommand sqlCmd;

        //    sSql = "update JOB set INV_SENT_TO_CUST = @NewValue where JOB_ID = @JobId";
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@JobId", g);
        //    sqlCmd.Parameters.AddWithValue("@NewValue", sValue);
        //    sqlCmd.ExecuteNonQuery();

        //}

        //public static void UpdateShippingDocsSent(Guid g, string sValue)
        //{
        //    string sSql;
        //    SqlCommand sqlCmd;

        //    sSql = "update JOB set DOCS_TO_CUSTOMS = @NewValue where JOB_ID = @JobId";
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@JobId", g);
        //    sqlCmd.Parameters.AddWithValue("@NewValue", sValue);
        //    sqlCmd.ExecuteNonQuery();

        //}

        //public static void UpdateJobCustPiSent(Guid g, string sValue)
        //{
        //    string sSql;
        //    SqlCommand sqlCmd;

        //    sSql = "update JOB set CUST_PI_SENT = @NewValue where JOB_ID = @JobId";
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@JobId", g);
        //    sqlCmd.Parameters.AddWithValue("@NewValue", sValue);
        //    sqlCmd.ExecuteNonQuery();

        //}

        //public static int PlacedChildren(Guid g)
        //{
        //    string sSql;
        //    SqlCommand sqlCmd;
        //    SqlDataReader cReader;
        //    int nChildren = 0;
        //    sSql = "select count(*) from JOB where PARENT_JOB_ID = @JobId " +
        //        "and CAST(JOB_STATUS as float) >= 0 and cast(JOB_STATUS as float) <= 100 ";
        //    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //    sqlCmd.Parameters.AddWithValue("@JobId", g);
        //    cReader = sqlCmd.ExecuteReader();
        //    while (cReader.Read())
        //    {
        //        nChildren = Convert.ToInt32(cReader[0]);
        //    }
        //    cReader.Close();
        //    return nChildren;
        //}

        //public static DataGridViewRow AddGridRow(DataRow row, DataGridView dgv, bool upsert, string sKeyList)
        //{
        //    DataGridViewRow hRow;
        //    bool bFound;
        //    if (upsert)
        //    {

        //    }
        //    else
        //    {
        //    }
        //    hRow = new DataGridViewRow();
        //    hRow.CreateCells(dgv);
        //    hRow.SetValues(row.ItemArray);
        //    dgv.Rows.Add(hRow);
        //    if (upsert)
        //    {
        //    }
        //    else
        //    {
        //    }
        //    return hRow;
        //}

        //public static bool SecurityCheck(string sLevel)
        //{
        //    int nLevel;
        //    try
        //    {
        //        if (int.TryParse(sLevel, out nLevel) == false)
        //        {
        //            return false;
        //        }
        //        return SecurityCheck(nLevel);
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}


        //public static bool SecurityCheck(int nLevel)
        //{
        //    try
        //    {
        //        return (Convert.ToInt32(AppHost.CurrentUser["SecurityLevel"]) >= nLevel);
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        //public static bool CompanyExists(string sId)
        //{
        //    try
        //    {
        //        string sSql;
        //        SqlCommand sqlCmd;
        //        SqlDataReader cReader;
        //        int nResult = 0;
        //        sSql = "select count(*) from CONTEXT where CONTEXT_ID = @CID ";
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        sqlCmd.Parameters.AddWithValue("@CID", sId);
        //        cReader = sqlCmd.ExecuteReader();
        //        while (cReader.Read())
        //        {
        //            nResult = Convert.ToInt32(cReader[0]);
        //        }
        //        cReader.Close();
        //        return (nResult > 0 ? true : false);
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }

        //}

        //public static string GenerateJobContextFilter(string sMoniker)
        //{
        //    string sFilter = "";
        //    string sContext = "";
        //    try
        //    {
        //        sContext = SqlHost.CurrentConnectionProfile.Context;
        //        if (sContext.Length == 0)
        //            return "";
        //        if (sMoniker.Length > 0)
        //        {
        //            sFilter = sMoniker + ".";
        //        }
        //        sFilter = sFilter + "CONTEXT = '" + sContext + "'";
        //        return sFilter;
        //    }
        //    catch (Exception ex)
        //    {
        //        return "";
        //    }
        //}

        //public static string GenerateIdContextFilter(string sMoniker)
        //{
        //    string sFilter = "";
        //    string sContext = "";
        //    try
        //    {
        //        //sContext = SqlHost.CurrentConnectionProfile.Context;
        //        sContext = AppHost.CurrentContextId;
        //        if (sContext.Length == 0)
        //            return "";
        //        if (sMoniker.Length > 0)
        //        {
        //            sFilter = sMoniker + ".";
        //        }
        //        sFilter = sFilter + "CONTEXT_ID = '" + sContext + "'";
        //        return sFilter;
        //    }
        //    catch (Exception ex)
        //    {
        //        return "";
        //    }
        //}

        public static string CurrentContextId
        {
            get { return SqlHost.CurrentConnectionProfile.Context; }
        }

        public static ContextDetail CurrentContext
        {
            get { return cCurrentContext; }
        }

        //public static bool ContextExists
        //{
        //    get { return (SqlHost.CurrentConnectionProfile.Context.Length > 0 ? true : false); }
        //}


        //public static DataTable GetJobTrackingHeaders(Guid gId)
        //{
        //    DataTable oDt;
        //    try
        //    {
        //        string sSql = "select A.TEMPLATE_ID as TemplateId, B.SHORT_DESC as Tracking, B.TRACKING_TYPE as TrackingType " +
        //            "from JOB_TRACK_ALLOC A inner join JOB_TRACK_HEADER_TEMPLATE B on A.TEMPLATE_ID = B.TEMPLATE_PROFILE_ID " +
        //            "where A.JOB_ID = @JOBID";
        //        oDt = new DataTable();
        //        SqlCommand oCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        oCmd.Parameters.AddWithValue("@JOBID", gId);

        //        oDt = new DataTable();
        //        oDt.Load(oCmd.ExecuteReader());
        //        return oDt;
        //    }
        //    catch (Exception ex)
        //    {
        //        oDt = new DataTable();
        //        return oDt;
        //    }
        //}
        //public static DataTable GetJobTrackingItemTemplates(string sTemplateId)
        //{
        //    DataTable oDt;
        //    try
        //    {
        //        string sSql = "select * from JOB_TRACK_ITEM_TEMPLATE where TEMPLATE_ID = @TEMPLATE_ID";
        //        oDt = new DataTable();
        //        SqlCommand oCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        oCmd.Parameters.AddWithValue("@TEMPLATE_ID", sTemplateId);

        //        oDt = new DataTable();
        //        oDt.Load(oCmd.ExecuteReader());
        //        return oDt;
        //    }
        //    catch (Exception ex)
        //    {
        //        oDt = new DataTable();
        //        return oDt;
        //    }
        //}
        //public static DataTable GetJobTrackingItems(Guid g, string sTemplateId)
        //{
        //    DataTable oDt;
        //    try
        //    {
        //        string sSql = "select * from JOB_TRACK_ITEM_ALLOC where JOB_ID = @JOB_ID and TEMPLATE_ID = @TEMPLATE_ID";
        //        oDt = new DataTable();
        //        SqlCommand oCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        oCmd.Parameters.AddWithValue("@JOB_ID", g);
        //        oCmd.Parameters.AddWithValue("@TEMPLATE_ID", sTemplateId);

        //        oDt = new DataTable();
        //        oDt.Load(oCmd.ExecuteReader());
        //        return oDt;
        //    }
        //    catch (Exception ex)
        //    {
        //        oDt = new DataTable();
        //        return oDt;
        //    }
        //}

        //public static DataTable GetRefDataTable(string sRefTableId)
        //{
        //    DataTable oDt;
        //    try
        //    {
        //        string sSql = "select * from REF_DATA where REF_TABLE_ID = @REF_TABLE_ID";
        //        oDt = new DataTable();
        //        SqlCommand oCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        oCmd.Parameters.AddWithValue("@REF_TABLE_ID", sRefTableId);

        //        oDt = new DataTable();
        //        oDt.Load(oCmd.ExecuteReader());
        //        return oDt;
        //    }
        //    catch (Exception ex)
        //    {
        //        oDt = new DataTable();
        //        return oDt;
        //    }
        //}

        //public static MasterfilesJob RetrieveMasterfilesJob(Guid g)
        //{
        //    DataTable oDt;
        //    MasterfilesJob cJob;
        //    try
        //    {
        //        string sSql = "select * from JOB where JOB_ID = @JOB_ID";
        //        oDt = new DataTable();
        //        SqlCommand oCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        oCmd.Parameters.AddWithValue("@JOB_ID", g);

        //        oDt = new DataTable();
        //        oDt.Load(oCmd.ExecuteReader());
        //        if (oDt.Rows.Count > 0)
        //        {
        //            DataRow dR = oDt.Rows[0];

        //            cJob = new MasterfilesJob();
        //            cJob.JobId = (Guid)dR["JOB_ID"];
        //            cJob.PoNum = dR["PO_NUM"].ToString();
        //            cJob.ShipRef = dR["SHIP_REF"].ToString();

        //            // MDS - TO DO XXXXXX - FILL THE REST IN!!

        //            return cJob;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}
        //public static bool SaveJobTrackItem(JobTrackItem cItem)
        //{
        //    string sSql;
        //    try
        //    {
        //        sSql = "update JOB_TRACK_ITEM_ALLOC set " +
        //            "STATUS_DATA = @STATUS_DATA, GUID_DATA = @GUID_DATA, DATE_DATA = @DATE_DATA, " +
        //            "NUMERIC_DATA = @NUMERIC_DATA, CHECK_DATA = @CHECK_DATA,  " +
        //            "LONG_STR = @LONG_STR, SHORT_STR = @SHORT_STR " +
        //            "where JOB_ID = @JOB_ID and TEMPLATE_ID = @TEMPLATE_ID and ITEM_NO = @ITEM_NO";
        //        SqlCommand sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        sqlCmd.Parameters.AddWithValue("@STATUS_DATA", cItem.StatusData);
        //        sqlCmd.Parameters.AddWithValue("@GUID_DATA", cItem.UIDData);
        //        if (cItem.DateTimeData <= new DateTime(1901, 01, 01))
        //        {
        //            sqlCmd.Parameters.AddWithValue("@DATE_DATA", new DateTime(1901, 01, 01));
        //        }
        //        else
        //        {
        //            sqlCmd.Parameters.AddWithValue("@DATE_DATA", cItem.DateTimeData);
        //        }
        //        sqlCmd.Parameters.AddWithValue("@NUMERIC_DATA", cItem.NumericData);
        //        sqlCmd.Parameters.AddWithValue("@CHECK_DATA", cItem.CheckData);
        //        sqlCmd.Parameters.AddWithValue("@LONG_STR", cItem.LongStr);
        //        sqlCmd.Parameters.AddWithValue("@SHORT_STR", cItem.ShortStr);

        //        sqlCmd.Parameters.AddWithValue("@JOB_ID", cItem.JobId);
        //        sqlCmd.Parameters.AddWithValue("@TEMPLATE_ID", cItem.TemplateId);
        //        sqlCmd.Parameters.AddWithValue("@ITEM_NO", cItem.ItemNo);

        //        sqlCmd.ExecuteNonQuery();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        //public static bool RemoveTrackingHeaderFromCurrentJob(Guid gJobId, string sTemplateId)
        //{
        //    string sSql = "";
        //    SqlCommand sqlCmd;
        //    try
        //    {

        //        sSql = "delete from JOB_TRACK_ALLOC where JOB_ID = @JOB_ID and TEMPLATE_ID = @TEMPLATE_ID";
        //        sqlCmd = new SqlCommand();
        //        sqlCmd.Connection = SqlHost.DBConn;
        //        sqlCmd.CommandText = sSql;
        //        sqlCmd.Parameters.AddWithValue("@JOB_ID", gJobId);
        //        sqlCmd.Parameters.AddWithValue("@TEMPLATE_ID", sTemplateId);
        //        sqlCmd.ExecuteNonQuery();

        //        sSql = "delete from JOB_TRACK_ITEM_ALLOC where JOB_ID = @JOB_ID and TEMPLATE_ID = @TEMPLATE_ID";
        //        sqlCmd = new SqlCommand();
        //        sqlCmd.Connection = SqlHost.DBConn;
        //        sqlCmd.CommandText = sSql;
        //        sqlCmd.Parameters.AddWithValue("@JOB_ID", gJobId);
        //        sqlCmd.Parameters.AddWithValue("@TEMPLATE_ID", sTemplateId);
        //        sqlCmd.ExecuteNonQuery();

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        //public static bool RemoveAllJobTracking(Guid gJobId)
        //{
        //    string sSql = "";
        //    SqlCommand sqlCmd;
        //    try
        //    {

        //        sSql = "delete from JOB_TRACK_ALLOC where JOB_ID = @JOB_ID";
        //        sqlCmd = new SqlCommand();
        //        sqlCmd.Connection = SqlHost.DBConn;
        //        sqlCmd.CommandText = sSql;
        //        sqlCmd.Parameters.AddWithValue("@JOB_ID", gJobId);
        //        sqlCmd.ExecuteNonQuery();

        //        sSql = "delete from JOB_TRACK_ITEM_ALLOC where JOB_ID = @JOB_ID";
        //        sqlCmd = new SqlCommand();
        //        sqlCmd.Connection = SqlHost.DBConn;
        //        sqlCmd.CommandText = sSql;
        //        sqlCmd.Parameters.AddWithValue("@JOB_ID", gJobId);
        //        sqlCmd.ExecuteNonQuery();

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        //public static void AddTrackingHeaderToCurrentJob(Guid gJobId, string sTemplateId)
        //{
        //    string sSql = "";
        //    SqlCommand sqlCmd;
        //    DataTable dtTemplateItems;
        //    try
        //    {
        //        sSql = "insert into JOB_TRACK_ALLOC ( JOB_ID, TEMPLATE_ID ) values ( @JOB_ID, @TEMPLATE_ID )";
        //        sqlCmd = new SqlCommand();
        //        sqlCmd.Connection = SqlHost.DBConn;
        //        sqlCmd.CommandText = sSql;
        //        sqlCmd.Parameters.AddWithValue("@JOB_ID", gJobId);
        //        sqlCmd.Parameters.AddWithValue("@TEMPLATE_ID", sTemplateId);
        //        sqlCmd.ExecuteNonQuery();

        //        // Now create alloc items for each template item
        //        sSql = "select A.TEMPLATE_ID as TemplateId, A.ITEM_NO as ItemNo " +
        //            "from JOB_TRACK_ITEM_TEMPLATE A " +
        //            "where A.TEMPLATE_ID = @TEMPLATE_ID";
        //        sqlCmd = new SqlCommand();
        //        sqlCmd.Connection = SqlHost.DBConn;
        //        sqlCmd.CommandText = sSql;
        //        sqlCmd.Parameters.AddWithValue("@TEMPLATE_ID", sTemplateId);
        //        using (SqlDataReader dr = sqlCmd.ExecuteReader())
        //        {
        //            dtTemplateItems = null;
        //            dtTemplateItems = new DataTable();
        //            dtTemplateItems.Load(dr);
        //        }

        //        foreach (DataRow dRow in dtTemplateItems.Rows)
        //        {
        //            sSql = "insert into JOB_TRACK_ITEM_ALLOC ( JOB_ID, TEMPLATE_ID, ITEM_NO ) values ( @JOB_ID, @TEMPLATE_ID, @ITEM_NO )";
        //            sqlCmd = new SqlCommand();
        //            sqlCmd.Connection = SqlHost.DBConn;
        //            sqlCmd.CommandText = sSql;
        //            sqlCmd.Parameters.AddWithValue("@JOB_ID", gJobId);
        //            sqlCmd.Parameters.AddWithValue("@TEMPLATE_ID", dRow["TemplateId"].ToString());
        //            sqlCmd.Parameters.AddWithValue("@ITEM_NO", dRow["ItemNo"].ToString());
        //            sqlCmd.ExecuteNonQuery();
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        //public static bool JobExists(Guid g)
        //{
        //    try
        //    {
        //        string sSql = "select count(*) from JOB where JOB_ID = @JOB_ID";
        //        SqlCommand oCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        oCmd.Parameters.AddWithValue("@JOB_ID", g);
        //        return ((Int32)oCmd.ExecuteScalar() != 0 ? true : false);
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        public static bool RegistryValueExists(string hive_HKLM_or_HKCU, string registryRoot, string valueName)
        {
            RegistryKey root;
            switch (hive_HKLM_or_HKCU.ToUpper())
            {
                case "HKLM":
                    root = Registry.LocalMachine.OpenSubKey(registryRoot, false);
                    break;
                case "HKCU":
                    root = Registry.CurrentUser.OpenSubKey(registryRoot, false);
                    break;
                default:
                    throw new System.InvalidOperationException("parameter registryRoot must be either \"HKLM\" or \"HKCU\"");
            }
            if (root == null) return false;
            return root.GetValue(valueName) != null;
        }

        public static bool RegistryCreateKey(string hive_HKLM_or_HKCU, string registryRoot, string valueName, string value)
        {
            RegistryKey root;
            try
            {
                switch (hive_HKLM_or_HKCU.ToUpper())
                {
                    case "HKLM":
                        root = Registry.LocalMachine.OpenSubKey(registryRoot, true);
                        break;
                    case "HKCU":
                        root = Registry.CurrentUser.OpenSubKey(registryRoot, true);
                        break;
                    default:
                        throw new System.InvalidOperationException("parameter registryRoot must be either \"HKLM\" or \"HKCU\"");
                }
                if (root == null)
                {
                    switch (hive_HKLM_or_HKCU.ToUpper())
                    {
                        case "HKLM":
                            root = Registry.LocalMachine.CreateSubKey(registryRoot);
                            break;
                        case "HKCU":
                            root = Registry.CurrentUser.CreateSubKey(registryRoot);
                            break;
                        default:
                            throw new System.InvalidOperationException("parameter registryRoot must be either \"HKLM\" or \"HKCU\"");
                    }
                }
                root.SetValue(valueName, value, RegistryValueKind.String);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool RegistryUpdateKeyValue(string hive_HKLM_or_HKCU, string registryRoot, string keyName, string value)
        {
            RegistryKey root;
            try
            {
                switch (hive_HKLM_or_HKCU.ToUpper())
                {
                    case "HKLM":
                        root = Registry.LocalMachine.OpenSubKey(registryRoot, true);
                        break;
                    case "HKCU":
                        root = Registry.CurrentUser.OpenSubKey(registryRoot, true);
                        break;
                    default:
                        throw new System.InvalidOperationException("parameter registryRoot must be either \"HKLM\" or \"HKCU\"");
                }
                if (root != null)
                {
                    root.SetValue(keyName, value, RegistryValueKind.String);
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static string RegistryGetKeyValue(string hive_HKLM_or_HKCU, string registryRoot, string valueName)
        {
            RegistryKey root;
            try
            {
                switch (hive_HKLM_or_HKCU.ToUpper())
                {
                    case "HKLM":
                        root = Registry.LocalMachine.OpenSubKey(registryRoot, false);
                        break;
                    case "HKCU":
                        root = Registry.CurrentUser.OpenSubKey(registryRoot, false);
                        break;
                    default:
                        throw new System.InvalidOperationException("parameter registryRoot must be either \"HKLM\" or \"HKCU\"");
                }
                if (root == null) return "";
                string sReturn = root.GetValue(valueName).ToString();
                return sReturn;
            }
            catch (Exception ex)
            {
                return "";
            }
        }


        //public static XmlNodeList GetAppAutoUpdateSources()
        //{
        //    return ConfigValues("/ConfigurationSettings/AutoUpdateSettings/UpdateSource");
        //}
        
        public static void AutoUpdateClientConfig(bool bAuto)
        {
            try
            {
                XmlNodeList l = ConfigValues("/ConfigurationSettings/AutoUpdateSettings/UpdateSource");
                foreach (XmlNode n in l)
                {
                    AppUpdateSource c = new AppUpdateSource(n);
                    if (c.AutoCheck)
                    {
                        RunAppUpdate(c);
                    }
                }

            }
            catch (Exception ex)
            {
                return;
            }
        }

        //public static void AutoUpdateClientConfig(string sUpdateId)
        //{
        //    try
        //    {
        //        XmlNodeList l = ConfigValues("/ConfigurationSettings/AutoUpdateSettings/UpdateSource");
        //        foreach (XmlNode n in l)
        //        {
        //            AppUpdateSource c = new AppUpdateSource(n);
        //            if (c.SourceId == sUpdateId)
        //            {
        //                RunAppUpdate(c);
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        return;
        //    }
        //}

        public static bool RunAppUpdate(AppUpdateSource s)
        {
            try
            {
                if (s.UpdateType == "network")
                {
                    return RunLocalAppUpdate(s.Source);
                }
                if (s.UpdateType == "web")
                {
                    return RunWebAppUpdate(s.Source);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool RunLocalAppUpdate(string sSource)
        {
            try
            {
                string sChangeDetail = "";
                bool bChangesMade = false;
                string sAppPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                if (Directory.Exists(sSource))
                {
                    string[] files = Directory.GetFiles(sSource);
                    foreach (string s in files)
                    {
                        string f = Path.GetFileName(s);
                        if (f.ToUpper() == "WBCONFIG.XML")
                        {
                            string sSourceFile = sSource + "\\" + f;
                            string sDestFile = sAppPath + "\\" + f;
                            File.Copy(sSourceFile, sDestFile, true);
                            sChangeDetail = sChangeDetail + "\nWBConfig Update";
                            bChangesMade = true;
                        }
                        if (f.ToUpper() == "DBCONFIG.XML")
                        {
                            string sSourceFile = sSource + "\\" + f;
                            string sDestFile = sAppPath + "\\" + f;
                            File.Copy(sSourceFile, sDestFile, true);

                            sChangeDetail = sChangeDetail + "\nDBConfig Update";
                            bChangesMade = true;
                        }
                        if (f.ToUpper() == "UPDATE.BAT")
                        {
                            string sSourceFile = sSource + "\\" + f;

                            var processInfo = new ProcessStartInfo();
                            processInfo.CreateNoWindow = true;
                            processInfo.FileName = @"cmd.exe";
                            processInfo.Verb = "runas";
                            processInfo.Arguments = "/C " + sSourceFile;
                            var process = new Process();
                            process.StartInfo = processInfo;
                            process.Start();
                            process.WaitForExit();
                            string output = process.StandardOutput.ReadToEnd();
                            string error = process.StandardError.ReadToEnd();

                            sChangeDetail = sChangeDetail + "\nUpdate script run";
                            bChangesMade = true;
                        }

                    }
                }
                else
                {
                    MessageBox.Show("Could not resolve update source location.", "Update Location Error", MessageBoxButtons.OK);
                }
                if (bChangesMade)
                {
                    MessageBox.Show("Masterfiles updates have been applied:" + sChangeDetail + "\nPlease Restart Masterfiles for these changes to take effect", "Information", MessageBoxButtons.OK);
                }
                else
                {
                    MessageBox.Show("No updates have been applied", "Maybe Next Time...", MessageBoxButtons.OK);
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Update failed.", MessageBoxButtons.OK);
                return false;
            }
        }

        public static bool RunWebAppUpdate(string sSource)
        {
            try
            {

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //public static DataTable GenerateRefDataFromAppControl(string sControlId)
        //{
        //    DataTable dt;
        //    string sRefTableId;
        //    string sSql;
        //    DataSet refDs;
        //    bool b;
        //    try
        //    {
        //        sRefTableId = AppControlSetting(sControlId);
        //        sSql = "select ref_id, description, short_desc, " +
        //            "ext_str_1, ext_str_2, ext_str_3, ext_num_1, ext_num_2, " +
        //            "ext_dt_1, ext_dt_2, ext_bool_1, ext_bool_2, notes " +
        //            "from ref_data where ref_table_id = '" + sRefTableId + "'";
        //        refDs = new DataSet();
        //        b = SqlHost.GetDataSet(sSql, "RefData", ref refDs);
        //        dt = new DataTable();
        //        dt = refDs.Tables["RefData"];
        //        return dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }

        //}


        //public static DataTable GenerateRefDataUsingRefId(string sRefTableId)
        //{
        //    DataTable dt;
        //    string sSql;
        //    DataSet refDs;
        //    bool b;
        //    try
        //    {
        //        sSql = "select ref_id, description, short_desc, " +
        //            "ext_str_1, ext_str_2, ext_str_3, ext_num_1, ext_num_2, " +
        //            "ext_dt_1, ext_dt_2, ext_bool_1, ext_bool_2, notes " +
        //            "from ref_data where ref_table_id = '" + sRefTableId + "'";
        //        refDs = new DataSet();
        //        b = SqlHost.GetDataSet(sSql, "RefData", ref refDs);
        //        dt = new DataTable();
        //        dt = refDs.Tables["RefData"];
        //        return dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }

        //}


        //public static bool StatusTriggered(string sStatus, string sList)
        //{
        //    string[] oList;
        //    try
        //    {
        //        oList = sList.Split(',');
        //        if (oList.Contains(sStatus))
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}


        //public static Dictionary<string, ExtensionFieldConfig> LoadExtensionFieldConfig()
        //{
        //    string sSql;
        //    SqlCommand sqlCmd;
        //    SqlDataReader cReader;
        //    Dictionary<string, ExtensionFieldConfig> ExtensionConfig = new Dictionary<string, ExtensionFieldConfig>();
        //    ExtensionFieldConfig cField;
        //    XmlDocument d = new XmlDocument();
        //    string sVal;
        //    int i;
        //    try
        //    {
        //        sSql = "select CONTROL_ID as ControlId, CONTROL_XML as ControlXml, DESCRIPTION as Description " +
        //            "from APP_CONTROL where control_id like 'JOBEXTENSION.%'";
        //        sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
        //        cReader = sqlCmd.ExecuteReader();
        //        while (cReader.Read())
        //        {
        //            cField = new ExtensionFieldConfig();
        //            string sValue = cReader["ControlXml"].ToString();

        //            d.LoadXml(sValue);

        //            XmlNode xn = d.SelectSingleNode("ExtensionField");
        //            cField.Id = xn.Attributes["id"].Value;
        //            cField.ExtType = xn.Attributes["extType"].Value;
        //            cField.FormEnabled = (xn.Attributes["formEnabled"].Value.ToUpper() == "TRUE");
        //            cField.FormLabel = xn.Attributes["formLabel"].Value;
        //            cField.FormFormat = xn.Attributes["formFormat"].Value;
        //            cField.RefTableId = xn.Attributes["refTableId"].Value;
        //            cField.DefaultValue = xn.Attributes["defaultValue"].Value;
        //            cField.DashboardEnabled = (xn.Attributes["dashboardEnabled"].Value.ToUpper() == "TRUE");
        //            cField.DashboardLabel = xn.Attributes["dashboardLabel"].Value;
        //            cField.DashboardFormat = xn.Attributes["dashboardFormat"].Value;
        //            cField.DashboardAlignment = xn.Attributes["dashboardAlignment"].Value;
        //            cField.DatabaseFieldName = xn.Attributes["databaseFieldName"].Value;
        //            sVal = xn.Attributes["dashboardWidth"].Value;
        //            if (int.TryParse(sVal, out i) == false)
        //            {
        //                i = 100;
        //            }
        //            cField.DashboardWidth = i;
        //            cField.DashboardShowPlaced = (xn.Attributes["dashboardShowPlaced"].Value.ToUpper() == "TRUE");
        //            cField.DashboardShowShipped = (xn.Attributes["dashboardShowShipped"].Value.ToUpper() == "TRUE");
        //            cField.DashboardShowDelivered = (xn.Attributes["dashboardShowDelivered"].Value.ToUpper() == "TRUE");
        //            cField.DashboardShowCompleted = (xn.Attributes["dashboardShowCompleted"].Value.ToUpper() == "TRUE");
        //            cField.DashboardShowAdmin = (xn.Attributes["dashboardShowAdmin"].Value.ToUpper() == "TRUE");
        //            cField.DashboardShowOMT = (xn.Attributes["dashboardShowOMT"].Value.ToUpper() == "TRUE");

        //            ExtensionConfig.Add(cField.Id, cField);
        //        }
        //        cReader.Close();
        //        return ExtensionConfig;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }

        //}


    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OMT
{
    public static class Constants
    {
        //public const string DEV_UN = "Marlon";
        //public const string DEV_PWD = "Marlon!@#";
        //public const bool USE_DEV_SETTINGS = true;

        //public static string JOB_JOBSTATUS_NEEDORDERCONFIRMATION;
        //public static string JOB_JOBSTATUS_HOLD;
        //public static string JOB_JOBSTATUS_PENDING;
        //public static string JOB_JOBSTATUS_CONFIRMETD;
        //public static string JOB_JOBSTATUS_ETDCONFIRMED;
        //public static string JOB_JOBSTATUS_SHIPPED;
        //public static string JOB_JOBSTATUS_MERGED;
        //public static string JOB_JOBSTATUS_DELIVERED;
        //public static string JOB_JOBSTATUS_EMPTYRET;
        //public static string JOB_JOBSTATUS_COMPLETED;
        //public static string JOB_JOBSTATUS_CANCELLED;
        //public static string JOB_JOBSTATUS_BOOKNOW;
        //public static string JOB_JOBSTATUS_CHECKDELTODLETDPAST;
        //public static string JOB_JOBSTATUS_OVERDUESENDSHIPPINGDOCS;
        //public static string JOB_JOBSTATUS_URGENTETDPAST;

        //public static string JOB_SHIPTYPE_TRANS;
        //public static string JOB_SHIPTYPE_DIRECT;
        //public static string JOB_CUSTPAYTERMS_COD;
        //public static string JOB_JOBCUSTTERMS_FOB;

        //public static string JOB_INVINMYOB_NO;
        //public static string JOB_INVINMYOB_YES;
        //public static string JOB_INVINMYOB_AMENDED;

        //public static string JOB_DEPOSIT_PAID;
        //public static string JOB_DEPOSIT_REQUIRED;

        //public static string CUST_TYPE_3PL;

        //public static string JOB_MERGED_NEWSTATUS;
        //public static string JOB_MERGED_NEWSTATUSFOB;
        //public static string JOB_MERGED_CLONESTATUS;
        //public static string JOB_MERGED_NEWDOCSSTATUS;
        //public static string JOB_MERGED_FOBDOCSTOCUSTOMS;
        //public static string JOB_MERGED_FOBDUTYSTATUS;

        //public static string JOB_SHIPPED_FCLNONPRIMARY;
        //public static string JOB_SHIPPED_FCLSHAREDCUST;
        //public static string JOB_SHIPPED_NEWSTATUS;

        //public static string JOB_LOGSPRIORITY_HIGH;

        //public static string JOB_NEW_JOBSTATUS;
        //public static string JOB_NEW_SOURCETYPE;
        //public static string JOB_NEW_PLSENTTOCUST;
        //public static string JOB_NEW_INVSENTTOCUST;
        //public static string JOB_NEW_SUPPINVINMYOB;
        //public static string JOB_NEW_CUSTINVINMYOB;
        //public static string JOB_NEW_DUTYSTATUS;
        //public static string JOB_NEW_DOCSTOCUSTOMS;
        //public static string JOB_NEW_LOGSPRIORITY;
        //public static string JOB_NEW_REPORTEXCLUSION;

        //public static string JOB_NEW_SUPPPROFORMARECD;
        //public static string JOB_NEW_SUPPINVOICERECD;
        //public static string JOB_NEW_SUPPPACKINGLISTRECD;
        //public static string JOB_NEW_SUPPBLFCRRECD;
        //public static string JOB_NEW_SUPPPACKINGDECRECD;
        //public static string JOB_NEW_SUPPMANDECRECD;
        //public static string JOB_NEW_SUPPFUMORECD;
        //public static string JOB_NEW_SUPPFUMOSTORAGERECD;
        //public static string JOB_NEW_SUPPCOORECD;
        //public static string JOB_NEW_SUPPTELEXREQUESTED;
        //public static string JOB_NEW_SUPPSHIPPERBOOKINGCONFRECD;
        //public static string JOB_NEW_CUSTPORECD;
        //public static string JOB_NEW_CONTAINERNETTINGREQUESTED;
        //public static string JOB_NEW_CONTAINERNETTINGRECD;
        //public static string JOB_NEW_SHIPMENTBOOKINGREQUESTED;
        //public static string JOB_NEW_SHIPPINGDOCSREQUESTED;
        //public static string JOB_NEW_FSCRECD;
        //public static string JOB_NEW_CUSTPISENT;
        //public static string JOB_NEW_TIMBERCOMPLIANCE;
        //public static string JOB_NEW_OMTCOMPLETE;

        //public static string COUNTERS_PONUM;

        //public static string JOB_EDIT_MYOBLOCKEDSTATUS;
        //public static string JOB_EDIT_MYOBLOCKEDALERT;

        //public static string JOB_UPDATE_CUSTINVINMYOB;
        //public static string JOB_UPDATE_PLSENTTOCUST;
        //public static string JOB_UPDATE_INVSENTTOCUST;
        //public static string JOB_UPDATE_DOCSTOCUSTOMSSENT;

        //public static string JOB_SOURCETYPE_LOCAL;
        //public static string JOB_SOURCETYPE_OVERSEAS;

        //public static string JOB_NOTETYPE_GENERAL;
        //public static string JOB_NOTETYPE_MISSINGDOCS;
        //public static string JOB_NOTETYPE_PARTSHIP;
        //public static string JOB_NOTETYPE_TRANSSHIP;

        //public static string JOB_TRACKING_VIEW_TYPE_ID;
        //public static string JOB_TRACKING_ITEM_TYPE_ID;

        //public static string JOB_TRACKING_ITEM_TYPE_BOOL;
        //public static string JOB_TRACKING_ITEM_TYPE_COMMENT;
        //public static string JOB_TRACKING_ITEM_TYPE_DATE;
        //public static string JOB_TRACKING_ITEM_TYPE_GUID;
        //public static string JOB_TRACKING_ITEM_TYPE_NOTE;
        //public static string JOB_TRACKING_ITEM_TYPE_NOTERTF;
        //public static string JOB_TRACKING_ITEM_TYPE_NUMBER;
        //public static string JOB_TRACKING_ITEM_TYPE_STATUS;
        //public static string JOB_TRACKING_ITEM_TYPE_URI;

        //public static string TRACKING_PROFILE_TYPE_JOB;
        //public static string TRACKING_PROFILE_TYPE_JOBITEM;
        //public static string TRACKING_PROFILE_TYPE_STOCK;

        public static string RELEASE_VERSION;
        //public static string RELEASE_DATE;
        //public static string RELEASE_TIME;

        public static string SECURITY_REFDATAPASSWORD;
        //public static string SECURITY_LOGSPASSWORD;
        //public static string SECURITY_JOBIDOVERRIDELEVEL;
        //public static string SECURITY_SUPERUSERSQL;

        //public static string FCL_SHARED;

        //public static string JOB_PARENTCOPY_FCL;

        //public static string JOB_DUTYSTATUS_LOCALS;
        //public static string JOB_DUTYSTATUS_OVERSEAS;

        //public static string JOB_DOCSTOCUSTOMS_LOCALS;
        //public static string JOB_DOCSTOCUSTOMS_OVERSEAS;

        //public static string WORKBENCH_CUSTOMGRIDVIEW_DISPLAY;

        //public static bool WORKBENCH_EMAIL_USEOUTLOOK;

        //public static string CURRENCY_AUD;

        //public static bool WORKBENCH_FORMAT_PLACED;
        //public static bool WORKBENCH_FORMAT_SHIPPED;
        //public static bool WORKBENCH_FORMAT_DELIVERED;
        //public static bool WORKBENCH_FORMAT_COMPLETED;
        //public static bool WORKBENCH_FORMAT_ADMIN;
        //public static bool WORKBENCH_FORMAT_CUSTOM;

        public static bool WORKBENCH_CBA_USEWEBSERVICES;
        public static bool WORKBENCH_SYSCTR_SQLSERVER;

        //public static string REPORT_TABLE_JOB;
        //public static string REPORT_DEF_PARAMTYPE;

        public static string NEWSUPPLIER_SUPPTYPE;
        public static string NEWSUPPLIER_POTYPE;
        public static string NEWSUPPLIER_LOCKCODE;
        public static string NEWSUPPLIER_CBATERMS;
        public static string NEWSUPPLIER_ENTITYTYPE;

        public static string NEWCUSTOMER_CBATERMS;
        public static string NEWCUSTOMER_CBABRANCH;
        public static string NEWCUSTOMER_CBASALESMAN;
        public static string NEWCUSTOMER_CBAPRICECODE;
        public static string NEWCUSTOMER_LOCKCODE;
        public static string NEWCUSTOMER_ENTITYTYPE;

        //public static string JOB_TASKFILTER_User;
        //public static string JOB_TASKFILTER_Job;
        //public static string JOB_TASKSTATUS_Complete;
        //public static string JOB_TASKSTATUS_New;
        //public static string JOB_TASKTYPE_Default;
        //public static string JOB_TASKPRIORITY_Urgent;
        //public static string JOB_TASKPRIORITY_Normal;

        //public static string JOB_SEARCH_TextSearchFieldList;

        //public static Int32 REFDATA_CUSTTERMS_InvIncludeTax;
        //public static Int32 REFDATA_CUSTTERMS_InvSuppressDates;
        //public static Int32 REFDATA_CUSTTERMS_InvSuppressPOL;
        //public static Int32 REFDATA_CUSTTERMS_InvSuppressVessel;
        //public static Int32 REFDATA_CUSTTERMS_InvPrintShippingConf;
        //public static Int32 REFDATA_CUSTTERMS_ShipConfSuppressDates;
        //public static Int32 REFDATA_CUSTTERMS_ShipConfSuppressPOL;
        //public static Int32 REFDATA_CUSTTERMS_ShipConfSuppressVessel;



        //public enum PanelType { PanelBool, PanelComment, PanelDate, PanelGuid, PanelNote, PanelNoteRTF, PanelNumber, PanelStatus, PanelURI };

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OMT.Forms
{
    public partial class dlgDatePicker : Form, IDateExchange
    {
        private IDateExchange dtPartner;
        private DateParam dtParam = new DateParam();

        public IDateExchange DatePartner
        {
            get { return dtPartner; }
            set { dtPartner = value; }
        }

        public dlgDatePicker()
        {
            InitializeComponent();
        }

        private void pbCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public bool AcceptDate(DateParam dtP)
        {
            try
            {
                dtParam.DtValue = dtP.DtValue;
                dtParam.Id = dtP.Id;
                dtParam.Action = dtP.Action;
                dtParam.SourceControl = dtP.SourceControl;
                mcChooser.SelectionStart = dtP.DtValue;
                mcChooser.SelectionEnd = dtP.DtValue;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void pbOK_Click(object sender, EventArgs e)
        {
            dtParam.DtValue = mcChooser.SelectionStart;
            dtPartner.AcceptDate(dtParam);
            this.Close();
        }

    }
}

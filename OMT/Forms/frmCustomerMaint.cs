﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using UbiSqlFramework;

namespace OMT.Forms
{
    public partial class frmCustomerMaint : Form, ISearchHost //, IGenericExchange
    {
        bool bAutoLoad;
        bool bNewRow;
        bool bNewCustContext;
        string sCustomerId;
        string sDefMainAddrId;
        string sDefDelAddrId;
        DataTable dtContextCust;
        //DataTable dtContext;
        string sLastImageDir;

        public frmCustomerMaint()
        {
            InitializeComponent();
        }

        private void frmCustomerMaint_Load(object sender, EventArgs e)
        {
            InitFormData();
            this.ActiveControl = textCustomerId;
        }

        private void InitFormData()
        {
            LoadCountries();
            LoadCurrencies();
            LoadCustTypes();
            LoadPorts();
            LoadOrderTerms();
            LoadPaymentTerms();
            LoadDepositReq();
            LoadQuoteCurrencies();
            LoadDefInvoiceTypes();
            LoadDefProformaTypes();
            LoadLockCodes();
            LoadEntityTypes();
            LoadContexts();
            LoadUOMConversions();
            LoadHeaders();

            LoadContextQuoteCurrencies();
            LoadContextOrderTerms();
            LoadContextPaymentTerms();
            LoadContextDepositReq();
            LoadContextInvoiceTypes();
            LoadContextProformaTypes();

            ClearFormDetails(true);
        }

        private void LoadCountries()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbMainAddrCountry.DataSource = null;
                cmbMainAddrCountry.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'COUNTRY' order by DESCRIPTION";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "CountryData", ref refContext);
                cmbMainAddrCountry.DataSource = refContext.Tables["CountryData"];
                cmbMainAddrCountry.ValueMember = "REF_ID";
                cmbMainAddrCountry.DisplayMember = "DESCRIPTION";
                cmbMainAddrCountry.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Countries Error", MessageBoxButtons.OK);
            }
        }

        private void LoadCurrencies()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbDefCurrency.DataSource = null;
                cmbDefCurrency.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'CURRENCY' ORDER BY EXT_NUM_1 DESC, REF_ID";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "DefCurrencyData", ref refContext);
                cmbDefCurrency.DataSource = refContext.Tables["DefCurrencyData"];
                cmbDefCurrency.ValueMember = "REF_ID";
                cmbDefCurrency.DisplayMember = "DESCRIPTION";
                cmbDefCurrency.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Currencies Error", MessageBoxButtons.OK);
            }
        }

        private void LoadQuoteCurrencies()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbCBAQCurrency.DataSource = null;
                cmbCBAQCurrency.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'CURRENCY' ORDER BY EXT_NUM_1 DESC, REF_ID";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "CBAQCurrencyData", ref refContext);
                cmbCBAQCurrency.DataSource = refContext.Tables["CBAQCurrencyData"];
                cmbCBAQCurrency.ValueMember = "REF_ID";
                cmbCBAQCurrency.DisplayMember = "DESCRIPTION";
                cmbCBAQCurrency.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Quote Currencies Error", MessageBoxButtons.OK);
            }
        }

        private void LoadContextQuoteCurrencies()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbContextQuoteCurrency.DataSource = null;
                cmbContextQuoteCurrency.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'CURRENCY' ORDER BY EXT_NUM_1 DESC, REF_ID";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "CQCurrencyData", ref refContext);
                cmbContextQuoteCurrency.DataSource = refContext.Tables["CQCurrencyData"];
                cmbContextQuoteCurrency.ValueMember = "REF_ID";
                cmbContextQuoteCurrency.DisplayMember = "DESCRIPTION";
                cmbContextQuoteCurrency.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Context Quote Currencies Error", MessageBoxButtons.OK);
            }
        }
        private void LoadPorts()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbDefPort.DataSource = null;
                cmbDefPort.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'PORT'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "PortData", ref refContext);
                cmbDefPort.DataSource = refContext.Tables["PortData"];
                cmbDefPort.ValueMember = "REF_ID";
                cmbDefPort.DisplayMember = "DESCRIPTION";
                cmbDefPort.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Ports Error", MessageBoxButtons.OK);
            }
        }
        private void LoadCustTypes()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbCustType.DataSource = null;
                cmbCustType.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'CUSTTYPE'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "TermsData", ref refContext);
                cmbCustType.DataSource = refContext.Tables["TermsData"];
                cmbCustType.ValueMember = "REF_ID";
                cmbCustType.DisplayMember = "DESCRIPTION";
                cmbCustType.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Customer Types Error", MessageBoxButtons.OK);
            }
        }
        
        private void LoadOrderTerms()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbOrderTerms.DataSource = null;
                cmbOrderTerms.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'CUSTTERMS'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "TermsData", ref refContext);
                cmbOrderTerms.DataSource = refContext.Tables["TermsData"];
                cmbOrderTerms.ValueMember = "REF_ID";
                cmbOrderTerms.DisplayMember = "DESCRIPTION";
                cmbOrderTerms.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Order Terms Error", MessageBoxButtons.OK);
            }
        }
        private void LoadContextOrderTerms()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbContextOrderTerms.DataSource = null;
                cmbContextOrderTerms.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'CUSTTERMS'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "TermsData", ref refContext);
                cmbContextOrderTerms.DataSource = refContext.Tables["TermsData"];
                cmbContextOrderTerms.ValueMember = "REF_ID";
                cmbContextOrderTerms.DisplayMember = "DESCRIPTION";
                cmbContextOrderTerms.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Context Order Terms Error", MessageBoxButtons.OK);
            }
        }
        
        private void LoadPaymentTerms()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbPaymentTerms.DataSource = null;
                cmbPaymentTerms.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'CUSTPAYTERMS'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "PaymentData", ref refContext);
                cmbPaymentTerms.DataSource = refContext.Tables["PaymentData"];
                cmbPaymentTerms.ValueMember = "REF_ID";
                cmbPaymentTerms.DisplayMember = "DESCRIPTION";
                cmbPaymentTerms.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Payment Terms Error", MessageBoxButtons.OK);
            }
        }

        private void LoadContextPaymentTerms()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbContextPaymentTerms.DataSource = null;
                cmbContextPaymentTerms.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'CUSTPAYTERMS'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "PaymentData", ref refContext);
                cmbContextPaymentTerms.DataSource = refContext.Tables["PaymentData"];
                cmbContextPaymentTerms.ValueMember = "REF_ID";
                cmbContextPaymentTerms.DisplayMember = "DESCRIPTION";
                cmbContextPaymentTerms.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Context Payment Terms Error", MessageBoxButtons.OK);
            }
        }

        private void LoadDepositReq()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbDepositReq.DataSource = null;
                cmbDepositReq.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'DEPOSITREQ'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "DepositData", ref refContext);
                cmbDepositReq.DataSource = refContext.Tables["DepositData"];
                cmbDepositReq.ValueMember = "REF_ID";
                cmbDepositReq.DisplayMember = "DESCRIPTION";
                cmbDepositReq.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Deposits Required Error", MessageBoxButtons.OK);
            }
        }

        private void LoadContextDepositReq()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbContextDepositReq.DataSource = null;
                cmbContextDepositReq.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'DEPOSITREQ'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "DepositData", ref refContext);
                cmbContextDepositReq.DataSource = refContext.Tables["DepositData"];
                cmbContextDepositReq.ValueMember = "REF_ID";
                cmbContextDepositReq.DisplayMember = "DESCRIPTION";
                cmbContextDepositReq.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Context Deposits Error", MessageBoxButtons.OK);
            }
        }

        private void LoadDefInvoiceTypes()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbCBADefInvType.DataSource = null;
                cmbCBADefInvType.Items.Clear();
                sSql = "select TEMPLATE_CODE, DESCRIPTION from DOC_TEMPLATE where TEMPLATE_TYPE = '1'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "InvoiceTypeData", ref refContext);
                cmbCBADefInvType.DataSource = refContext.Tables["InvoiceTypeData"];
                cmbCBADefInvType.ValueMember = "TEMPLATE_CODE";
                cmbCBADefInvType.DisplayMember = "DESCRIPTION";
                cmbCBADefInvType.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Default invoice Types Error", MessageBoxButtons.OK);
            }
        }

        private void LoadContextInvoiceTypes()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbContextInvoiceType.DataSource = null;
                cmbContextInvoiceType.Items.Clear();
                sSql = "select TEMPLATE_CODE, DESCRIPTION from DOC_TEMPLATE where TEMPLATE_TYPE = '1'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "InvoiceTypeData", ref refContext);
                cmbContextInvoiceType.DataSource = refContext.Tables["InvoiceTypeData"];
                cmbContextInvoiceType.ValueMember = "TEMPLATE_CODE";
                cmbContextInvoiceType.DisplayMember = "DESCRIPTION";
                cmbContextInvoiceType.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Context Invoice Types Error", MessageBoxButtons.OK);
            }
        }

        private void LoadDefProformaTypes()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbCBADefPIType.DataSource = null;
                cmbCBADefPIType.Items.Clear();
                sSql = "select TEMPLATE_CODE, DESCRIPTION from DOC_TEMPLATE where TEMPLATE_TYPE = '2'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "ProformaTypeData", ref refContext);
                cmbCBADefPIType.DataSource = refContext.Tables["ProformaTypeData"];
                cmbCBADefPIType.ValueMember = "TEMPLATE_CODE";
                cmbCBADefPIType.DisplayMember = "DESCRIPTION";
                cmbCBADefPIType.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Default Proforma Types Error", MessageBoxButtons.OK);
            }
        }

        private void LoadContextProformaTypes()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbContextProformaType.DataSource = null;
                cmbContextProformaType.Items.Clear();
                sSql = "select TEMPLATE_CODE, DESCRIPTION from DOC_TEMPLATE where TEMPLATE_TYPE = '2'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "ProformaTypeData", ref refContext);
                cmbContextProformaType.DataSource = refContext.Tables["ProformaTypeData"];
                cmbContextProformaType.ValueMember = "TEMPLATE_CODE";
                cmbContextProformaType.DisplayMember = "DESCRIPTION";
                cmbContextProformaType.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Context Proforma Types Error", MessageBoxButtons.OK);
            }
        }

        private void LoadContacts(string sId)
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbPrimaryContactNo.DataSource = null;
                cmbPrimaryContactNo.Items.Clear();
                if (sId.Length > 0)
                {
                    sSql = "select CONTACT_ID, NAME_1 from CUSTOMER_CONTACT where CUSTOMER_ID = '" + sId + "'";
                    refContext = new DataSet();
                    b = SqlHost.GetDataSet(sSql, "ContactData", ref refContext);
                    cmbPrimaryContactNo.DataSource = refContext.Tables["ContactData"];
                    cmbPrimaryContactNo.ValueMember = "CONTACT_ID";
                    cmbPrimaryContactNo.DisplayMember = "NAME_1";
                    cmbPrimaryContactNo.SelectedIndex = -1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Contacts Error", MessageBoxButtons.OK);
            }
        }

        private void LoadLockCodes()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbCBALockCode.DataSource = null;
                cmbCBALockCode.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'CBACUSTLOCK'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "LockCodes", ref refContext);
                cmbCBALockCode.DataSource = refContext.Tables["LockCodes"];
                cmbCBALockCode.ValueMember = "REF_ID";
                cmbCBALockCode.DisplayMember = "DESCRIPTION";
                cmbCBALockCode.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Lock Codes Error", MessageBoxButtons.OK);
            }
        }

        private void LoadEntityTypes()
        {
            try
            {
                cmbEntityType.DataSource = AppHost.EntityType.Copy();
                cmbEntityType.DisplayMember = "Description";
                cmbEntityType.ValueMember = "ref_id";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Entity Types Error", MessageBoxButtons.OK);
            }
        }

        private void LoadUOMConversions()
        {
            try
            {
                cmbVolumeConv.DataSource = AppHost.UOMConversions.Copy();
                cmbVolumeConv.DisplayMember = "Description";
                cmbVolumeConv.ValueMember = "ref_id";

                cmbWeightConv.DataSource = AppHost.UOMConversions.Copy();
                cmbWeightConv.DisplayMember = "Description";
                cmbWeightConv.ValueMember = "ref_id";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load UOM Conversion Error", MessageBoxButtons.OK);
            }
        }

        private void LoadHeaders()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbContextDefInvHdrImg.DataSource = null;
                cmbContextDefInvHdrImg.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'HEADERFILE' order by DESCRIPTION";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "ImageHeaderData", ref refContext);
                cmbContextDefInvHdrImg.DataSource = refContext.Tables["ImageHeaderData"];
                cmbContextDefInvHdrImg.ValueMember = "REF_ID";
                cmbContextDefInvHdrImg.DisplayMember = "DESCRIPTION";
                cmbContextDefInvHdrImg.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Headers Error", MessageBoxButtons.OK);
            }
        }

        private void LoadContexts()
        {
            try
            {
                cmbContextId.DataSource = AppHost.Context.Copy();
                cmbContextId.DisplayMember = "NAME_1";
                cmbContextId.ValueMember = "CONTEXT_ID";
                cmbContextId.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Context Error", MessageBoxButtons.OK);
            }
        }
        
        private void textCustomerId_Validating(object sender, CancelEventArgs e)
        {
            if (!bAutoLoad)
            {
                if (textCustomerId.Text != textCustomerId.Text.ToUpper())
                {
                    textCustomerId.Text = textCustomerId.Text.ToUpper();
                }
                RetrieveCustomerDetails(textCustomerId.Text);
                this.ActiveControl = textCustName1;
            }
        }

        private void textCustomerId_TextChanged(object sender, EventArgs e)
        {
            sCustomerId = textCustomerId.Text;
        }

        private void RetrieveCustomerDetails(string sId)
        {
            SqlCommand rCom;
            SqlDataReader cReader;
            try
            {
                ClearFormDetails(false);
                if (sId.Length == 0)
                {
                    return;
                }
                LoadContacts(textCustomerId.Text);
                rCom = new SqlCommand("select * from CUSTOMER where CUSTOMER_ID = @CUST_ID", SqlHost.DBConn);
                rCom.Parameters.AddWithValue("@CUST_ID", textCustomerId.Text.ToUpper());

                cReader = rCom.ExecuteReader();

                bNewRow = !cReader.HasRows;

                while (cReader.Read())
                {
                    if (cReader["NAME_1"] != System.DBNull.Value)
                        textCustName1.Text = cReader["NAME_1"].ToString();
                    if (cReader["NAME_2"] != System.DBNull.Value)
                        textCustName2.Text = cReader["NAME_2"].ToString();
                    if (cReader["BUSINESS_NAME"] != System.DBNull.Value)
                        textCustBusinessName.Text = cReader["BUSINESS_NAME"].ToString();
                    if (cReader["CUSTOMER_TYPE"] != System.DBNull.Value && cReader["CUSTOMER_TYPE"].ToString().Length > 0)
                    {
                        cmbCustType.SelectedValue = (string)cReader["CUSTOMER_TYPE"];
                    }
                    else
                    {
                        cmbCustType.SelectedIndex = -1;
                    }
                    if (cReader["DEF_CURRENCY"] != System.DBNull.Value && cReader["DEF_CURRENCY"].ToString().Length > 0)
                    {
                        cmbDefCurrency.SelectedValue = (string)cReader["DEF_CURRENCY"];
                    }
                    else
                    {
                        cmbDefCurrency.SelectedIndex = -1;
                    }
                    if (cReader["ORDER_TERMS"] != System.DBNull.Value && cReader["ORDER_TERMS"].ToString().Length > 0)
                    {
                        cmbOrderTerms.SelectedValue = (string)cReader["ORDER_TERMS"];
                    }
                    else
                    {
                        cmbOrderTerms.SelectedIndex = -1;
                    }
                    if (cReader["PAYMENT_TERMS"] != System.DBNull.Value && cReader["PAYMENT_TERMS"].ToString().Length > 0)
                    {
                        cmbPaymentTerms.SelectedValue = (string)cReader["PAYMENT_TERMS"];
                    }
                    else
                    {
                        cmbPaymentTerms.SelectedIndex = -1;
                    }
                    if (cReader["DEF_PORT"] != System.DBNull.Value && cReader["DEF_PORT"].ToString().Length > 0)
                    {
                        cmbDefPort.SelectedValue = (string)cReader["DEF_PORT"];
                    }
                    else
                    {
                        cmbDefPort.SelectedIndex = -1;
                    }
                    if (cReader["PARENT_CUST"] != System.DBNull.Value)
                    {
                        textParentCustId.Text = cReader["PARENT_CUST"].ToString();
                    }
                    if (cReader["PRODUCT_PROFILE_CUST"] != System.DBNull.Value)
                    {
                        textProductProfileId.Text = cReader["PRODUCT_PROFILE_CUST"].ToString();
                    }
                    if (cReader["DEPOSIT_REQ"] != System.DBNull.Value && cReader["DEPOSIT_REQ"].ToString().Length > 0)
                    {
                        cmbDepositReq.SelectedValue = (string)cReader["DEPOSIT_REQ"];
                    }
                    else
                    {
                        cmbDepositReq.SelectedIndex = -1;
                    }
                    cbActive.Checked = (bool)cReader["ACTIVE"];
                    if (cReader["NON_ACCOUNT"] != System.DBNull.Value)
                    {
                        cbSplitOnly.Checked = (bool)cReader["NON_ACCOUNT"];
                    }
                    else
                    {
                        cbSplitOnly.Checked = false;
                    }
                    if (cReader["NOTES"] != System.DBNull.Value)
                        textNotes.Text = cReader["NOTES"].ToString();
                    if (cReader["DISCOUNT_NOTE"] != System.DBNull.Value)
                        textDiscountNotes.Text = cReader["DISCOUNT_NOTE"].ToString();
                    if (cReader["PRIMARY_ADDRESS"] != System.DBNull.Value)
                        sDefMainAddrId = cReader["PRIMARY_ADDRESS"].ToString();
                    if (cReader["PRIMARY_DEL_ADDR"] != System.DBNull.Value)
                        sDefDelAddrId = cReader["PRIMARY_DEL_ADDR"].ToString();
                    if (cReader["TAX_NO"] != System.DBNull.Value)
                        textTaxNumber.Text = cReader["TAX_NO"].ToString();

                    cbIsDel.Checked = (sDefDelAddrId == sDefMainAddrId);

                    // New CBA Integration details
                    if (cReader["CBA_TERMS"] != System.DBNull.Value)
                        textCBATerms.Text = cReader["CBA_TERMS"].ToString();
                    if (cReader["CBA_BRANCH"] != System.DBNull.Value)
                        textCBABranch.Text = cReader["CBA_BRANCH"].ToString();
                    if (cReader["CBA_SALESMAN"] != System.DBNull.Value)
                        textCBASalesman.Text = cReader["CBA_SALESMAN"].ToString();
                    if (cReader["CBA_PRICE_CODE"] != System.DBNull.Value)
                        textCBAPriceCode.Text = cReader["CBA_PRICE_CODE"].ToString();
                    if (cReader["CBA_INV_DISC"] != System.DBNull.Value)
                        textCBAInvoiceDiscount.Text = cReader["CBA_INV_DISC"].ToString();
                    if (cReader["QUOTE_CURRENCY"] != System.DBNull.Value && cReader["QUOTE_CURRENCY"].ToString().Length > 0)
                    {
                        cmbCBAQCurrency.SelectedValue = (string)cReader["QUOTE_CURRENCY"];
                    }
                    else
                    {
                        cmbCBAQCurrency.SelectedIndex = -1;
                    }
                    if (cReader["DEF_INVOICE_TYPE"] != System.DBNull.Value && cReader["DEF_INVOICE_TYPE"].ToString().Length > 0)
                    {
                        cmbCBADefInvType.SelectedValue = (string)cReader["DEF_INVOICE_TYPE"];
                    }
                    else
                    {
                        cmbCBADefInvType.SelectedIndex = -1;
                    }
                    if (cReader["DEF_PROFORMA_TYPE"] != System.DBNull.Value && cReader["DEF_PROFORMA_TYPE"].ToString().Length > 0)
                    {
                        cmbCBADefPIType.SelectedValue = (string)cReader["DEF_PROFORMA_TYPE"];
                    }
                    else
                    {
                        cmbCBADefPIType.SelectedIndex = -1;
                    }
                    if (cReader["PRIMARY_CONTACT_NO"] != System.DBNull.Value && cReader["PRIMARY_CONTACT_NO"].ToString().Length > 0)
                    {
                        cmbPrimaryContactNo.SelectedValue = (string)cReader["PRIMARY_CONTACT_NO"];
                    }
                    else
                    {
                        cmbPrimaryContactNo.SelectedIndex = -1;
                    }
                    if (cReader["LOCK_CODE"] != System.DBNull.Value && cReader["LOCK_CODE"].ToString().Length > 0)
                    {
                        cmbCBALockCode.SelectedValue = (string)cReader["LOCK_CODE"];
                    }
                    else
                    {
                        cmbCBALockCode.SelectedIndex = -1;
                    }
                    if (cReader["RESTRICTIONS"] != System.DBNull.Value)
                        textCBARestrictions.Text = cReader["RESTRICTIONS"].ToString();
                    if (cReader["ORDER_ALERT"] != System.DBNull.Value)
                        textCBAOrderAlert.Text = cReader["ORDER_ALERT"].ToString();
                    if (cReader["SHIPPING_MARKS"] != System.DBNull.Value)
                        textCBAShippingMarks.Text = cReader["SHIPPING_MARKS"].ToString();

                    // Additional Detials Tab
                    textBoxDefaultQuoteRebate.Value = cReader["DEF_QUOTE_REBATE"];
                    textBoxDefaultQuoteDiscount.Value = cReader["DEF_QUOTE_DISCOUNT"];
                    textBoxDefaultSlushie.Value = cReader["DEF_SLUSHIE"];
                    if (cReader["ENTITY_TYPE"] != System.DBNull.Value && cReader["ENTITY_TYPE"].ToString().Length > 0)
                    {
                        cmbEntityType.SelectedValue = (string)cReader["ENTITY_TYPE"];
                    }
                    else
                    {
                        cmbEntityType.SelectedIndex = -1;
                    }
                    if (cReader["ABN_NO"] != System.DBNull.Value)
                        textABNNo.Text = cReader["ABN_NO"].ToString();
                    if (cReader["VOLUME_CONV"] != System.DBNull.Value && cReader["VOLUME_CONV"].ToString().Length > 0)
                    {
                        cmbVolumeConv.SelectedValue = (string)cReader["VOLUME_CONV"];
                    }
                    else
                    {
                        cmbVolumeConv.SelectedIndex = -1;
                    }
                    if (cReader["WEIGHT_CONV"] != System.DBNull.Value && cReader["WEIGHT_CONV"].ToString().Length > 0)
                    {
                        cmbWeightConv.SelectedValue = (string)cReader["WEIGHT_CONV"];
                    }
                    else
                    {
                        cmbWeightConv.SelectedIndex = -1;
                    }

                }
                cReader.Close();
                if (bNewRow)
                {
                    sDefMainAddrId = "001";
                    sDefDelAddrId = "001";
                    cbActive.Checked = true;
                    cbIsDel.Checked = true;
                    cbIsDel.Enabled = false;
                    pbAddresses.Enabled = false;
                    pbContacts.Enabled = false;
                    textCBATerms.Text = Constants.NEWCUSTOMER_CBATERMS;
                    textCBABranch.Text = Constants.NEWCUSTOMER_CBABRANCH;
                    textCBASalesman.Text = Constants.NEWCUSTOMER_CBASALESMAN;
                    textCBAPriceCode.Text = Constants.NEWCUSTOMER_CBAPRICECODE;
                    cmbCBALockCode.SelectedValue = Constants.NEWCUSTOMER_LOCKCODE;
                    cmbEntityType.SelectedValue = Constants.NEWCUSTOMER_ENTITYTYPE;
                }
                else
                {
                    RetrieveParentCustomerDetails(textParentCustId.Text);
                    RetrieveProductProfileCustomerDetails(textProductProfileId.Text);

                    // Retrieve Main Address Details
                    RetrieveAddressDetails(sId, sDefMainAddrId);

                    // Retrieve Del Addr Details
                    // new function - not being used
                    cbIsDel.Checked = (sDefDelAddrId == sDefMainAddrId);
                    cbIsDel.Enabled = true;
                    pbAddresses.Enabled = true;
                    pbContacts.Enabled = true;

                    // Refresh Context Assignments
                    RefreshContextData(sId);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Retrieve Customer Details Error", MessageBoxButtons.OK);
            }

        }

        private void ClearFormDetails(bool bClearCustId)
        {
            if (bClearCustId)
            {
                textCustomerId.Text = "";
                sCustomerId = "";
                sDefDelAddrId = "";
                sDefMainAddrId = "";
            }
            textCustName1.Text = "";
            textCustName2.Text = "";
            textCustBusinessName.Text = "";
            cmbCustType.SelectedIndex = -1;
            cmbDefCurrency.SelectedIndex = -1;
            cmbOrderTerms.SelectedIndex = -1;
            cmbPaymentTerms.SelectedIndex = -1;
            cmbDefPort.SelectedIndex = -1;
            cmbDepositReq.SelectedIndex = -1;
            cbActive.Checked = false;
            cbSplitOnly.Checked = false;
            textParentCustId.Text = "";
            textParentCustName.Text = "";
            textProductProfileId.Text = "";
            textProductProfileName.Text = "";
            textNotes.Text = "";
            textDiscountNotes.Text = "";
            textMainAddr1.Text = "";
            textMainAddr2.Text = "";
            textMainAddr3.Text = "";
            textMainAddrState.Text = "";
            textMainAddrPostCode.Text = "";
            cmbMainAddrCountry.SelectedIndex = -1;
            textMainAddrPhone.Text = "";
            textMainAddrFax.Text = "";
            textMainAddrEmail.Text = "";
            cbIsDel.Checked = true;
            cbIsDel.Enabled = false;
            textAddrLabel.Text = "";
            pbAddresses.Enabled = false;
            pbContacts.Enabled = false;
            textCBATerms.Text = "";
            textCBABranch.Text = "";
            textCBASalesman.Text = "";
            textCBAPriceCode.Text = "";
            textCBAInvoiceDiscount.Text = "";
            textCBARestrictions.Text = "";
            textCBAOrderAlert.Text = "";
            textCBAShippingMarks.Text = "";
            cmbCBAQCurrency.SelectedIndex = -1;
            cmbCBADefInvType.SelectedIndex = -1;
            cmbCBADefPIType.SelectedIndex = -1;
            LoadContacts("");
            cmbPrimaryContactNo.SelectedIndex = -1;
            cmbCBALockCode.SelectedIndex = -1;
            textBoxDefaultQuoteDiscount.Value = 0;
            textBoxDefaultQuoteRebate.Value = 0;
            textBoxDefaultSlushie.Value = 0;
            cmbEntityType.SelectedIndex = -1;
            textABNNo.Text = "";
            dgvContext.DataSource = null;
            cmbContextId.SelectedIndex = -1;
            textContextCustId.Text = "";
            textTaxNumber.Text = "";
            cmbVolumeConv.SelectedIndex = -1;
            cmbWeightConv.SelectedIndex = -1;
            ClearContextCustProfile();
        }

        private void ClearContextCustProfile()
        {
            cmbContextOrderTerms.SelectedIndex = -1;
            cmbContextPaymentTerms.SelectedIndex = -1;
            cmbContextDepositReq.SelectedIndex = -1;
            cmbContextQuoteCurrency.SelectedIndex = -1;
            cmbContextInvoiceType.SelectedIndex = -1;
            cmbContextProformaType.SelectedIndex = -1;
            textContextOrderAlert.Text = "";
            textContextDiscountNotes.Text = "";
            textContextQuoteRebate.Value = 0;
            textContextQuoteDiscount.Value = 0;
            textContextQuoteSlushie.Value = 0;
            textContextProfileNotes.Text = "";
            textContextVendorNumber.Text = "";
            textContextVendorShortDesc.Text = "";
            cmbContextDefInvHdrImg.SelectedIndex = -1;
        }

        private void RetrieveParentCustomerDetails(string sId)
        {
            try
            {
                if (sId.Length == 0)
                {
                    textParentCustName.Text = "";
                    return;
                }
                SqlCommand rCom = new SqlCommand("select * from CUSTOMER where CUSTOMER_ID = @CUST_ID", SqlHost.DBConn);
                rCom.Parameters.AddWithValue("@CUST_ID", sId);
                SqlDataReader cReader = rCom.ExecuteReader();

                if (!cReader.HasRows)
                {
                    MessageBox.Show("Customer does not exist.", "Information", MessageBoxButtons.OK);
                    textParentCustId.Text = "";
                    textParentCustName.Text = "";
                    cReader.Close();
                    return;
                }

                while (cReader.Read())
                {
                    if (cReader["NAME_1"] != System.DBNull.Value)
                        textParentCustName.Text = cReader["NAME_1"].ToString();
                }
                cReader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Retrieve Parent Customer Details Error", MessageBoxButtons.OK);
            }

        }
        private void RetrieveProductProfileCustomerDetails(string sId)
        {
            try
            {
                if (sId.Length == 0)
                {
                    textProductProfileName.Text = "";
                    return;
                }
                SqlCommand rCom = new SqlCommand("select * from CUSTOMER where CUSTOMER_ID = @CUST_ID", SqlHost.DBConn);
                rCom.Parameters.AddWithValue("@CUST_ID", sId);
                SqlDataReader cReader = rCom.ExecuteReader();

                if (!cReader.HasRows)
                {
                    MessageBox.Show("Customer does not exist.", "Information", MessageBoxButtons.OK);
                    textProductProfileId.Text = "";
                    textProductProfileName.Text = "";
                    cReader.Close();
                    return;
                }

                while (cReader.Read())
                {
                    if (cReader["NAME_1"] != System.DBNull.Value)
                        textProductProfileName.Text = cReader["NAME_1"].ToString();
                }
                cReader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Retrieve Product Profile Customer Details Error", MessageBoxButtons.OK);
            }
        }

        private void RetrieveAddressDetails(string sCustId, string sAddrId)
        {
            try
            {
                if (sCustId.Length == 0 || sAddrId.Length == 0)
                {
                    return;
                }
                SqlCommand rCom = new SqlCommand("select * from CUSTOMER_ADDR where CUSTOMER_ID = @CUST_ID and ADDRESS_ID = @ADDR_ID", SqlHost.DBConn);
                rCom.Parameters.AddWithValue("@CUST_ID", sCustId);
                rCom.Parameters.AddWithValue("@ADDR_ID", sAddrId);
                SqlDataReader cReader = rCom.ExecuteReader();

                if (!cReader.HasRows)
                {
                    MessageBox.Show("Could not find office address details.", "Information", MessageBoxButtons.OK);
                    cReader.Close();
                    return;
                }

                while (cReader.Read())
                {
                    if (cReader["ADDR_1"] != System.DBNull.Value)
                        textMainAddr1.Text = cReader["ADDR_1"].ToString();
                    if (cReader["ADDR_2"] != System.DBNull.Value)
                        textMainAddr2.Text = cReader["ADDR_2"].ToString();
                    if (cReader["ADDR_3"] != System.DBNull.Value)
                        textMainAddr3.Text = cReader["ADDR_3"].ToString();
                    if (cReader["STATE"] != System.DBNull.Value)
                        textMainAddrState.Text = cReader["STATE"].ToString();
                    if (cReader["POST_CODE"] != System.DBNull.Value)
                        textMainAddrPostCode.Text = cReader["POST_CODE"].ToString();
                    if (cReader["COUNTRY"] != System.DBNull.Value)
                        cmbMainAddrCountry.SelectedValue = cReader["COUNTRY"].ToString();
                    if (cReader["PHONE"] != System.DBNull.Value)
                        textMainAddrPhone.Text = cReader["PHONE"].ToString();
                    if (cReader["FAX"] != System.DBNull.Value)
                        textMainAddrFax.Text = cReader["FAX"].ToString();
                    if (cReader["EMAIL_ADDRESS"] != System.DBNull.Value)
                        textMainAddrEmail.Text = cReader["EMAIL_ADDRESS"].ToString();
                    if (cReader["ADDR_LABEL"] != System.DBNull.Value)
                        textAddrLabel.Text = cReader["ADDR_LABEL"].ToString();
                }
                cReader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Retrieve Address Details Error", MessageBoxButtons.OK);
            }
        }

        private bool RefreshContextData(string sCustId)
        {
            try
            {
                string sSql;
                SqlCommand sqlCmd;
                bool b;
                sSql = "select CONTEXT_ID, CONTEXT_CUST_ID " +
                       "from CONTEXT_CUST_PROFILE " +
                       "where SYSTEM_CUST_ID = @CUSTID";

                sqlCmd = new SqlCommand();
                sqlCmd.Connection = SqlHost.DBConn;
                sqlCmd.CommandText = sSql;
                sqlCmd.Parameters.AddWithValue("@CUSTID", sCustId);
                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    dtContextCust = null;
                    dtContextCust = new DataTable();
                    dtContextCust.Load(dr);
                }
                dgvContext.DataSource = dtContextCust;
                FormatContextDataView();
                textContextCustId.Text = "";
                cmbContextId.SelectedIndex = -1;
                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Refresh Context Data Error", MessageBoxButtons.OK);
                return false;
            }
        }

        private void FormatContextDataView()
        {
            try
            {
                if (dgvContext.DataSource != null)
                {
                    dgvContext.Columns["CONTEXT_ID"].HeaderText = "Company";
                    dgvContext.Columns["CONTEXT_ID"].Width = 100;
                    dgvContext.Columns["CONTEXT_CUST_ID"].HeaderText = "Customer Id";
                    dgvContext.Columns["CONTEXT_CUST_ID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void pbCustIdSearch_Click(object sender, EventArgs e)
        {
            SearchParam p = new SearchParam();
            p.SearchPartner = (ISearchHost)this;

            p.Sql = "Select C.CONTEXT_CUST_ID as Customer_Id, A.NAME_1 as Customer_Name, A.CUSTOMER_ID as System_Id " +
                    "from CONTEXT_CUST_PROFILE C inner join CUSTOMER A on C.SYSTEM_CUST_ID = A.CUSTOMER_ID and C.CONTEXT_ID = '" + AppHost.CurrentContextId + "' " +
                    "where A.ACTIVE = 1 order by C.CONTEXT_CUST_ID";
            p.ReturnObject = "CustId";
            p.ReturnKey = new string[] { "System_Id", "Customer_Name" };
            dlgSearch dlg = new dlgSearch();
            ((ISearchHost)dlg).SearchTransfer(p);
            dlg.ShowDialog(this);
        }

        private void pbAllCustSearch_Click(object sender, EventArgs e)
        {
            SearchParam p = new SearchParam();
            p.SearchPartner = (ISearchHost)this;
            p.Sql = "Select CUSTOMER_ID as Customer_Id, NAME_1 as Customer_Name from CUSTOMER where ACTIVE = 1 order by CUSTOMER_ID";
            p.ReturnObject = "CustId";
            p.ReturnKey = new string[] { "Customer_Id", "Customer_Name" };
            dlgSearch dlg = new dlgSearch();
            ((ISearchHost)dlg).SearchTransfer(p);
            dlg.ShowDialog(this);
        }

        private void textParentCustId_Validating(object sender, CancelEventArgs e)
        {
            if (textParentCustId.Text != textParentCustId.Text.ToUpper())
            {
                textParentCustId.Text = textParentCustId.Text.ToUpper();
            }
            RetrieveParentCustomerDetails(textParentCustId.Text);
        }

        private void textProductProfileId_Validating(object sender, CancelEventArgs e)
        {
            if (textProductProfileId.Text != textProductProfileId.Text.ToUpper())
            {
                textProductProfileId.Text = textProductProfileId.Text.ToUpper();
            }
            RetrieveProductProfileCustomerDetails(textProductProfileId.Text);
        }

        private void pbParentCustIdSearch_Click(object sender, EventArgs e)
        {
            SearchParam p = new SearchParam();
            p.SearchPartner = (ISearchHost)this;
            p.Sql = "Select CUSTOMER_ID as Customer_Id, NAME_1 as Customer_Name from CUSTOMER where ACTIVE = 1 order by CUSTOMER_ID";
            p.ReturnObject = "ParentCustId";
            p.ReturnKey = new string[] { "Customer_Id", "Customer_Name" };
            dlgSearch dlg = new dlgSearch();
            ((ISearchHost)dlg).SearchTransfer(p);
            dlg.ShowDialog(this);
        }

        private void pbProductProfileIdSearch_Click(object sender, EventArgs e)
        {
            SearchParam p = new SearchParam();
            p.SearchPartner = (ISearchHost)this;
            p.Sql = "Select CUSTOMER_ID as Customer_Id, NAME_1 as Customer_Name from CUSTOMER where ACTIVE = 1 order by CUSTOMER_ID";
            p.ReturnObject = "ProductProfileId";
            p.ReturnKey = new string[] { "Customer_Id", "Customer_Name" };
            dlgSearch dlg = new dlgSearch();
            ((ISearchHost)dlg).SearchTransfer(p);
            dlg.ShowDialog(this);
        }

        public void SearchTransfer(SearchParam sParam)
        {
            if (sParam.ReturnObject == "CustId")
            {
                bAutoLoad = true;
                textCustomerId.Text = sParam.ReturnValue[0];
                RetrieveCustomerDetails(textCustomerId.Text);
                this.ActiveControl = textCustName1;
                bAutoLoad = false;
            }
            //if (sParam.ReturnObject == "CustIdRO")
            //{
            //    bAutoLoad = true;
            //    textCustomerId.Text = sParam.ReturnValue[0];
            //    RetrieveCustomerDetails(textCustomerId.Text);
            //    this.ActiveControl = textCustName1;
            //    bAutoLoad = false;
            //    pbSave.Enabled = false;
            //    pbDelete.Enabled = false;
            //}
            if (sParam.ReturnObject == "ParentCustId")
            {
                bAutoLoad = true;
                textParentCustId.Text = sParam.ReturnValue[0];
                RetrieveParentCustomerDetails(textParentCustId.Text);
                this.ActiveControl = textProductProfileId;
                bAutoLoad = false;
            }
            if (sParam.ReturnObject == "ProductProfileId")
            {
                bAutoLoad = true;
                textProductProfileId.Text = sParam.ReturnValue[0];
                RetrieveProductProfileCustomerDetails(textProductProfileId.Text);
                bAutoLoad = false;
            }
        }

        private void pbAddresses_Click(object sender, EventArgs e)
        {
            frmCustAddress frmA = new frmCustAddress();
            GenericParam gP = new GenericParam();
            gP.IdField = "CUSTOMER_ID";
            gP.ValueField = textCustomerId.Text;
            ((IGenericExchange)frmA).GenericTransfer(gP);
            frmA.ShowDialog(this);
            RetrieveCustomerDetails(sCustomerId);
        }

        private void pbContacts_Click(object sender, EventArgs e)
        {
            frmCustContact frmC = new frmCustContact();
            GenericParam gP = new GenericParam();
            gP.IdField = "CUSTOMER_ID";
            gP.ValueField = textCustomerId.Text;
            ((IGenericExchange)frmC).GenericTransfer(gP);
            frmC.ShowDialog(this);
        }

        private void cmbVolumeConv_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                ClearControlValue(cmbVolumeConv);
        }

        private void cmbWeightConv_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                ClearControlValue(cmbWeightConv);
        }

        private void ClearControlValue(System.Windows.Forms.Control c)
        {
            if (c.GetType() == typeof(System.Windows.Forms.ComboBox))
            {
                ((ComboBox)c).SelectedIndex = -1;
                return;
            }
            if (c.GetType() == typeof(System.Windows.Forms.DateTimePicker))
            {
                ((DateTimePicker)c).Value = new DateTime(1901, 1, 1);
                return;
            }
        }

        private void dgvContext_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvContext.SelectedCells.Count == 0)
            {
                return;
            }
            DataRow dRow = ((DataRowView)dgvContext.SelectedCells[0].OwningRow.DataBoundItem).Row;
            string sId = dRow["CONTEXT_ID"].ToString();
            cmbContextId.SelectedValue = sId;
        }

        private void dgvContext_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvContext.SelectedCells.Count == 0)
            {
                ClearContextCustProfile();
                return;
            }
            DataRow dRow = ((DataRowView)dgvContext.SelectedCells[0].OwningRow.DataBoundItem).Row;
            string sId = dRow["CONTEXT_ID"].ToString();
            cmbContextId.SelectedValue = sId;
        }

        private void cmbContextId_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!bAutoLoad)
            {
                if (cmbContextId.SelectedIndex != -1)
                {
                    LoadContextCustProfile(cmbContextId.SelectedValue.ToString(), sCustomerId);
                }
                else
                {
                    ClearContextCustProfile();
                }
            }
        }

        private void LoadContextCustProfile(string sContextId, string sCustId)
        {
            try
            {
                string sSql = "select " +
                              "CONTEXT_CUST_ID, ORDER_TERMS, PAYMENT_TERMS, DEPOSIT_REQ, QUOTE_CURRENCY, " +
                              "DEF_INVOICE_TYPE, DEF_PROFORMA_TYPE, ORDER_ALERT, DISCOUNT_NOTE, " +
                              "DEF_QUOTE_REBATE, DEF_QUOTE_DISCOUNT, DEF_SLUSHIE, PROFILE_NOTES, " +
                              "VENDOR_NUMBER, INVOICE_HEADER_URI, VENDOR_SHORTDESC " +
                              "from CONTEXT_CUST_PROFILE " +
                              "where CONTEXT_ID = @CONTEXTID and SYSTEM_CUST_ID = @SYSCUSTID";
                SqlCommand sqlCmd;
                sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                sqlCmd.Parameters.AddWithValue("@CONTEXTID", sContextId);
                sqlCmd.Parameters.AddWithValue("@SYSCUSTID", sCustId);
                
                SqlDataReader cReader = sqlCmd.ExecuteReader();

                bNewCustContext = !cReader.HasRows;
                textContextCustId.Text = "";

                while (cReader.Read())
                {
                    if (cReader["CONTEXT_CUST_ID"] != System.DBNull.Value)
                    {
                        textContextCustId.Text = cReader["CONTEXT_CUST_ID"].ToString();
                    }
                    else
                    {
                        textContextCustId.Text = "";
                    }
                    if (cReader["ORDER_TERMS"] != System.DBNull.Value)
                    {
                        cmbContextOrderTerms.SelectedValue = cReader["ORDER_TERMS"].ToString();
                    }
                    else
                    {
                        cmbContextOrderTerms.SelectedIndex = -1;
                    }
                    if (cReader["PAYMENT_TERMS"] != System.DBNull.Value)
                    {
                        cmbContextPaymentTerms.SelectedValue = cReader["PAYMENT_TERMS"].ToString();
                    }
                    else
                    {
                        cmbContextPaymentTerms.SelectedIndex = -1;
                    }
                    if (cReader["DEPOSIT_REQ"] != System.DBNull.Value)
                    {
                        cmbContextDepositReq.SelectedValue = cReader["DEPOSIT_REQ"].ToString();
                    }
                    else
                    {
                        cmbContextDepositReq.SelectedIndex = -1;
                    }
                    if (cReader["QUOTE_CURRENCY"] != System.DBNull.Value)
                    {
                        cmbContextQuoteCurrency.SelectedValue = cReader["QUOTE_CURRENCY"].ToString();
                    }
                    else
                    {
                        cmbContextQuoteCurrency.SelectedIndex = -1;
                    }
                    if (cReader["DEF_INVOICE_TYPE"] != System.DBNull.Value)
                    {
                        cmbContextInvoiceType.SelectedValue = cReader["DEF_INVOICE_TYPE"].ToString();
                    }
                    else
                    {
                        cmbContextInvoiceType.SelectedIndex = -1;
                    }
                    if (cReader["DEF_PROFORMA_TYPE"] != System.DBNull.Value)
                    {
                        cmbContextProformaType.Text = cReader["DEF_PROFORMA_TYPE"].ToString();
                    }
                    else
                    {
                        cmbContextProformaType.SelectedIndex = -1;
                    }
                    if (cReader["ORDER_ALERT"] != System.DBNull.Value)
                    {
                        textContextOrderAlert.Text = cReader["ORDER_ALERT"].ToString();
                    }
                    else
                    {
                        textContextOrderAlert.Text = "";
                    }
                    if (cReader["DISCOUNT_NOTE"] != System.DBNull.Value)
                    {
                        textContextDiscountNotes.Text = cReader["DISCOUNT_NOTE"].ToString();
                    }
                    else
                    {
                        textContextDiscountNotes.Text = "";
                    }
                    if (cReader["DEF_QUOTE_REBATE"] != System.DBNull.Value)
                    {
                        textContextQuoteRebate.Value = cReader["DEF_QUOTE_REBATE"];
                    }
                    else
                    {
                        textContextQuoteRebate.Value = 0;
                    }
                    if (cReader["DEF_QUOTE_DISCOUNT"] != System.DBNull.Value)
                    {
                        textContextQuoteDiscount.Value = cReader["DEF_QUOTE_DISCOUNT"];
                    }
                    else
                    {
                        textContextQuoteDiscount.Value = 0;
                    }
                    if (cReader["DEF_SLUSHIE"] != System.DBNull.Value)
                    {
                        textContextQuoteSlushie.Value = cReader["DEF_SLUSHIE"];
                    }
                    else
                    {
                        textContextQuoteSlushie.Value = 0;
                    }
                    if (cReader["PROFILE_NOTES"] != System.DBNull.Value)
                    {
                        textContextProfileNotes.Text = cReader["PROFILE_NOTES"].ToString();
                    }
                    else
                    {
                        textContextProfileNotes.Text = "";
                    }
                    if (cReader["VENDOR_NUMBER"] != System.DBNull.Value)
                    {
                        textContextVendorNumber.Text = cReader["VENDOR_NUMBER"].ToString();
                    }
                    else
                    {
                        textContextVendorNumber.Text = "";
                    }
                    if (cReader["INVOICE_HEADER_URI"] != System.DBNull.Value)
                    {
                        cmbContextDefInvHdrImg.Text = cReader["INVOICE_HEADER_URI"].ToString();
                    }
                    else
                    {
                        cmbContextDefInvHdrImg.SelectedIndex = -1;
                    }
                    if (cReader["VENDOR_SHORTDESC"] != System.DBNull.Value)
                    {
                        textContextVendorShortDesc.Text = cReader["VENDOR_SHORTDESC"].ToString();
                    }
                    else
                    {
                        textContextVendorShortDesc.Text = "";
                    }

                }
                cReader.Close();
            }
            catch (Exception ex)
            {
               //MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Context Customer Profile Error", MessageBoxButtons.OK);
            }
        }

        private void pbChooseInvHdrImg_Click(object sender, EventArgs e)
        {
            GetInvoiceHdrImageFilename();
        }

        private void GetInvoiceHdrImageFilename()
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                if (sLastImageDir == "")
                {
                    ofd.InitialDirectory = AppHost.ConfigValue("/ConfigurationSettings/AppSettings/StockImageSettings", "DefImagePath");
                }
                else
                {
                    ofd.InitialDirectory = sLastImageDir;
                }
                ofd.Filter = "Jpeg (*.jpg)|*.jpg|Png (*.png)|*.png|Bitmap (*.bmp)|*.bmp|All Files (*.*)|*.*";
                ofd.FilterIndex = 1;
                ofd.RestoreDirectory = true;
                ofd.Multiselect = false;
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    textContextDefInvHdrImg.Text = ofd.FileName;
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void pbContextSave_Click(object sender, EventArgs e)
        {
            SaveContextCustProfile();
        }

        private void SaveContextCustProfile()
        {
            string sSql;
            if (cmbContextId.SelectedIndex == -1)
            {
                return;
            }
            if (textContextCustId.Text.Length == 0)
            {
                DeleteContextCustProfile();
                return;
            }
            if (bNewCustContext)
            {
                sSql = "insert into " +
                       "CONTEXT_CUST_PROFILE " +
                       "( " +
                       "CONTEXT_ID, SYSTEM_CUST_ID, CONTEXT_CUST_ID, " +
                       "ORDER_TERMS, PAYMENT_TERMS, DEPOSIT_REQ, QUOTE_CURRENCY, " +
                       "DEF_INVOICE_TYPE, DEF_PROFORMA_TYPE, ORDER_ALERT, DISCOUNT_NOTE, " +
                       "DEF_QUOTE_REBATE, DEF_QUOTE_DISCOUNT, DEF_SLUSHIE, PROFILE_NOTES, " +
                       "VENDOR_NUMBER, INVOICE_HEADER_URI, VENDOR_SHORTDESC " +
                       ") values ( " +
                       "@CONTEXT_ID, @SYSTEM_CUST_ID, @CONTEXT_CUST_ID, " +
                       "@ORDER_TERMS, @PAYMENT_TERMS, @DEPOSIT_REQ, @QUOTE_CURRENCY, " +
                       "@DEF_INVOICE_TYPE, @DEF_PROFORMA_TYPE, @ORDER_ALERT, @DISCOUNT_NOTE, " +
                       "@DEF_QUOTE_REBATE, @DEF_QUOTE_DISCOUNT, @DEF_SLUSHIE, @PROFILE_NOTES, " +
                       "@VENDOR_NUMBER, @INVOICE_HEADER_URI, @VENDOR_SHORTDESC " +
                       ")";
            }
            else
            {
                sSql = "update CONTEXT_CUST_PROFILE set " +
                       "CONTEXT_CUST_ID = @CONTEXT_CUST_ID, " +
                       "ORDER_TERMS = @ORDER_TERMS, PAYMENT_TERMS = @PAYMENT_TERMS, " +
                       "DEPOSIT_REQ = @DEPOSIT_REQ, QUOTE_CURRENCY = @QUOTE_CURRENCY, " +
                       "DEF_INVOICE_TYPE = @DEF_INVOICE_TYPE, DEF_PROFORMA_TYPE = @DEF_PROFORMA_TYPE, " +
                       "ORDER_ALERT = @ORDER_ALERT, DISCOUNT_NOTE = @DISCOUNT_NOTE, " +
                       "DEF_QUOTE_REBATE = @DEF_QUOTE_REBATE, DEF_QUOTE_DISCOUNT = @DEF_QUOTE_DISCOUNT, " +
                       "DEF_SLUSHIE = @DEF_SLUSHIE, PROFILE_NOTES = @PROFILE_NOTES, " +
                       "VENDOR_NUMBER = @VENDOR_NUMBER, INVOICE_HEADER_URI = @INVOICE_HEADER_URI, " +
                       "VENDOR_SHORTDESC = @VENDOR_SHORTDESC " +
                       "where " +
                       "CONTEXT_ID = @CONTEXT_ID and SYSTEM_CUST_ID = @SYSTEM_CUST_ID";
            }
            SqlCommand sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
            sqlCmd.Parameters.AddWithValue("@CONTEXT_ID", cmbContextId.SelectedValue.ToString());
            sqlCmd.Parameters.AddWithValue("@SYSTEM_CUST_ID", sCustomerId);
            sqlCmd.Parameters.AddWithValue("@CONTEXT_CUST_ID", textContextCustId.Text);
            if (cmbContextOrderTerms.SelectedIndex != -1)
            {
                sqlCmd.Parameters.AddWithValue("@ORDER_TERMS", cmbContextOrderTerms.SelectedValue);
            }
            else
            {
                sqlCmd.Parameters.AddWithValue("@ORDER_TERMS", "");
            }
            if (cmbContextPaymentTerms.SelectedIndex != -1)
            {
                sqlCmd.Parameters.AddWithValue("@PAYMENT_TERMS", cmbContextPaymentTerms.SelectedValue);
            }
            else
            {
                sqlCmd.Parameters.AddWithValue("@PAYMENT_TERMS", "");
            }
            if (cmbContextDepositReq.SelectedIndex != -1)
            {
                sqlCmd.Parameters.AddWithValue("@DEPOSIT_REQ", cmbContextDepositReq.SelectedValue);
            }
            else
            {
                sqlCmd.Parameters.AddWithValue("@DEPOSIT_REQ", "");
            }
            if (cmbContextQuoteCurrency.SelectedIndex != -1)
            {
                sqlCmd.Parameters.AddWithValue("@QUOTE_CURRENCY", cmbContextQuoteCurrency.SelectedValue);
            }
            else
            {
                sqlCmd.Parameters.AddWithValue("@QUOTE_CURRENCY", "");
            }
            if (cmbContextInvoiceType.SelectedIndex != -1)
            {
                sqlCmd.Parameters.AddWithValue("@DEF_INVOICE_TYPE", cmbContextInvoiceType.SelectedValue);
            }
            else
            {
                sqlCmd.Parameters.AddWithValue("@DEF_INVOICE_TYPE", "");
            }
            if (cmbContextProformaType.SelectedIndex != -1)
            {
                sqlCmd.Parameters.AddWithValue("@DEF_PROFORMA_TYPE", cmbContextProformaType.SelectedValue);
            }
            else
            {
                sqlCmd.Parameters.AddWithValue("@DEF_PROFORMA_TYPE", "");
            }
            sqlCmd.Parameters.AddWithValue("@ORDER_ALERT", textContextOrderAlert.Text);
            sqlCmd.Parameters.AddWithValue("@DISCOUNT_NOTE", textContextDiscountNotes.Text);
            sqlCmd.Parameters.AddWithValue("@DEF_QUOTE_REBATE", textContextQuoteRebate.Value);
            sqlCmd.Parameters.AddWithValue("@DEF_QUOTE_DISCOUNT", textContextQuoteDiscount.Value);
            sqlCmd.Parameters.AddWithValue("@DEF_SLUSHIE", textContextQuoteSlushie.Value);
            sqlCmd.Parameters.AddWithValue("@PROFILE_NOTES", textContextProfileNotes.Text);
            sqlCmd.Parameters.AddWithValue("@VENDOR_NUMBER", textContextVendorNumber.Text);
            if (cmbContextDefInvHdrImg.SelectedIndex != -1)
            {
                sqlCmd.Parameters.AddWithValue("@INVOICE_HEADER_URI", cmbContextDefInvHdrImg.Text);
            }
            else
            {
                sqlCmd.Parameters.AddWithValue("@INVOICE_HEADER_URI", "");
            }
            sqlCmd.Parameters.AddWithValue("@VENDOR_SHORTDESC", textContextVendorShortDesc.Text);

            sqlCmd.ExecuteNonQuery();
            RefreshContextData(sCustomerId);
        }

        private void pbContextDelete_Click(object sender, EventArgs e)
        {
            DeleteContextCustProfile();
        }

        private void DeleteContextCustProfile()
        {
            if (cmbContextId.SelectedIndex == -1)
            {
                return;
            }
            if (MessageBox.Show("Are you sure you want to delete this Customer/Company relationship?", "This cannot be undone", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                SqlCommand sqlCmd = new SqlCommand("delete from CONTEXT_CUST_PROFILE where CONTEXT_ID = @CONTEXTID and SYSTEM_CUST_ID = @SYSTEMCUSTID", SqlHost.DBConn);
                sqlCmd.Parameters.AddWithValue("@CONTEXTID", cmbContextId.SelectedValue.ToString());
                sqlCmd.Parameters.AddWithValue("@SYSTEMCUSTID", sCustomerId);
                sqlCmd.ExecuteNonQuery();
            }
            RefreshContextData(sCustomerId);
        }

        private void pbContextCopyDefaults_Click(object sender, EventArgs e)
        {
            if (cmbContextId.SelectedIndex != -1)
            {
                if (cmbOrderTerms.SelectedIndex != -1)
                    cmbContextOrderTerms.SelectedValue = cmbOrderTerms.SelectedValue;
                if (cmbPaymentTerms.SelectedIndex != -1)
                    cmbContextPaymentTerms.SelectedValue = cmbPaymentTerms.SelectedValue;
                if (cmbDepositReq.SelectedIndex != -1)
                    cmbContextDepositReq.SelectedValue = cmbDepositReq.SelectedValue;
                if (cmbCBAQCurrency.SelectedIndex != -1)
                    cmbContextQuoteCurrency.SelectedValue = cmbCBAQCurrency.SelectedValue;
                if (cmbCBADefInvType.SelectedIndex != -1)
                    cmbContextInvoiceType.SelectedValue = cmbCBADefInvType.SelectedValue;
                if (cmbCBADefPIType.SelectedIndex != -1)
                    cmbContextProformaType.SelectedValue = cmbCBADefPIType.SelectedValue;
                textContextOrderAlert.Text = textCBAOrderAlert.Text;
                textContextDiscountNotes.Text = textDiscountNotes.Text;
                textContextQuoteRebate.Value = textBoxDefaultQuoteRebate.Value;
                textContextQuoteDiscount.Value = textBoxDefaultQuoteDiscount.Value;
                textContextQuoteSlushie.Value = textBoxDefaultSlushie.Value;
                textContextCustId.Text = textCustomerId.Text;
            }
        }

        private void pbSave_Click(object sender, EventArgs e)
        {
            if (SaveCustomer())
            {
                ClearFormDetails(true);
            }
            this.ActiveControl = textCustomerId;
        }


        private bool SaveCustomer()
        {
            string sSql;
            SqlCommand sqlCmd;
            try
            {
                if (textCustomerId.Text.Length > 0)
                {

                    if (bNewRow)
                    {
                        sSql = "insert into CUSTOMER ( " +
                               "CUSTOMER_ID, NAME_1, NAME_2, BUSINESS_NAME, CUSTOMER_TYPE, DEF_CURRENCY, ORDER_TERMS, " +
                               "PAYMENT_TERMS, DEPOSIT_REQ, DEF_PORT, PARENT_CUST, ACTIVE, NOTES, PRIMARY_ADDRESS, PRIMARY_DEL_ADDR, " +
                               "CBA_TERMS, CBA_BRANCH, CBA_SALESMAN, CBA_PRICE_CODE, CBA_INV_DISC, QUOTE_CURRENCY, DEF_INVOICE_TYPE, " +
                               "DEF_PROFORMA_TYPE, RESTRICTIONS, ORDER_ALERT, SHIPPING_MARKS, PRIMARY_CONTACT_NO, LOCK_CODE, " +
                               "NON_ACCOUNT, DISCOUNT_NOTE, DEF_QUOTE_REBATE, DEF_QUOTE_DISCOUNT, DEF_SLUSHIE, ENTITY_TYPE, ABN_NO, " +
                               "PRODUCT_PROFILE_CUST, TAX_NO, VOLUME_CONV, WEIGHT_CONV " +
                               " ) " +
                               "VALUES ( " +
                               "@CUSTOMER_ID, @NAME_1, @NAME_2, @BUSINESS_NAME, @CUSTOMER_TYPE, @DEF_CURRENCY, @ORDER_TERMS, " +
                               "@PAYMENT_TERMS, @DEPOSIT_REQ, @DEF_PORT, @PARENT_CUST, @ACTIVE, @NOTES, @PRIMARY_ADDRESS, @PRIMARY_DEL_ADDR," +
                               "@CBA_TERMS, @CBA_BRANCH, @CBA_SALESMAN, @CBA_PRICE_CODE, @CBA_INV_DISC, @QUOTE_CURRENCY, @DEF_INVOICE_TYPE, " +
                               "@DEF_PROFORMA_TYPE, @RESTRICTIONS, @ORDER_ALERT, @SHIPPING_MARKS, @PRIMARY_CONTACT_NO, @LOCK_CODE, " +
                               "@NON_ACCOUNT, @DISCOUNT_NOTE, @DEF_QUOTE_REBATE, @DEF_QUOTE_DISCOUNT, @DEF_SLUSHIE, @ENTITY_TYPE, @ABN_NO, " +
                               "@PRODUCT_PROFILE_CUST, @TAX_NO, @VOLUME_CONV, @WEIGHT_CONV " +
                               " )";
                    }
                    else
                    {
                        sSql = "update CUSTOMER set " +
                               "NAME_1 = @NAME_1, NAME_2 = @NAME_2, BUSINESS_NAME = @BUSINESS_NAME, " +
                               "CUSTOMER_TYPE = @CUSTOMER_TYPE, DEF_CURRENCY = @DEF_CURRENCY, " +
                               "ORDER_TERMS = @ORDER_TERMS, PAYMENT_TERMS = @PAYMENT_TERMS, DEPOSIT_REQ = @DEPOSIT_REQ, DEF_PORT = @DEF_PORT, " +
                               "PARENT_CUST = @PARENT_CUST, ACTIVE = @ACTIVE, NOTES = @NOTES, CBA_TERMS = @CBA_TERMS, CBA_BRANCH = @CBA_BRANCH, " +
                               "CBA_SALESMAN = @CBA_SALESMAN, CBA_PRICE_CODE = @CBA_PRICE_CODE, CBA_INV_DISC = @CBA_INV_DISC, " +
                               "QUOTE_CURRENCY = @QUOTE_CURRENCY, DEF_INVOICE_TYPE = @DEF_INVOICE_TYPE, DEF_PROFORMA_TYPE = @DEF_PROFORMA_TYPE, " +
                               "RESTRICTIONS = @RESTRICTIONS, ORDER_ALERT = @ORDER_ALERT, SHIPPING_MARKS = @SHIPPING_MARKS, " +
                               "PRIMARY_CONTACT_NO = @PRIMARY_CONTACT_NO, LOCK_CODE = @LOCK_CODE, NON_ACCOUNT = @NON_ACCOUNT, " +
                               "DISCOUNT_NOTE = @DISCOUNT_NOTE, DEF_QUOTE_REBATE = @DEF_QUOTE_REBATE, " +
                               "DEF_QUOTE_DISCOUNT = @DEF_QUOTE_DISCOUNT, DEF_SLUSHIE = @DEF_SLUSHIE, " +
                               "ENTITY_TYPE = @ENTITY_TYPE, ABN_NO = @ABN_NO, PRODUCT_PROFILE_CUST = @PRODUCT_PROFILE_CUST, " +
                               "TAX_NO = @TAX_NO, VOLUME_CONV = @VOLUME_CONV, WEIGHT_CONV = @WEIGHT_CONV " +
                               "where CUSTOMER_ID = @CUSTOMER_ID";
                    }
                    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                    sqlCmd.Parameters.AddWithValue("@CUSTOMER_ID", textCustomerId.Text.ToUpper());
                    sqlCmd.Parameters.AddWithValue("@NAME_1", textCustName1.Text);
                    sqlCmd.Parameters.AddWithValue("@NAME_2", textCustName2.Text);
                    sqlCmd.Parameters.AddWithValue("@BUSINESS_NAME", textCustBusinessName.Text);
                    sqlCmd.Parameters.AddWithValue("@TAX_NO", textTaxNumber.Text);
                    if (cmbCustType.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@CUSTOMER_TYPE", cmbCustType.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@CUSTOMER_TYPE", "");
                    }
                    if (cmbDefCurrency.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@DEF_CURRENCY", cmbDefCurrency.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@DEF_CURRENCY", "");
                    }
                    if (cmbOrderTerms.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@ORDER_TERMS", cmbOrderTerms.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@ORDER_TERMS", "");
                    }
                    if (cmbPaymentTerms.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@PAYMENT_TERMS", cmbPaymentTerms.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@PAYMENT_TERMS", "");
                    }
                    if (cmbDepositReq.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@DEPOSIT_REQ", cmbDepositReq.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@DEPOSIT_REQ", "");
                    }
                    if (cmbDefPort.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@DEF_PORT", cmbDefPort.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@DEF_PORT", "");
                    }
                    sqlCmd.Parameters.AddWithValue("@PARENT_CUST", textParentCustId.Text);
                    sqlCmd.Parameters.AddWithValue("@PRODUCT_PROFILE_CUST", textProductProfileId.Text);
                    sqlCmd.Parameters.AddWithValue("@ACTIVE", (cbActive.Checked ? 1 : 0));
                    sqlCmd.Parameters.AddWithValue("@NON_ACCOUNT", (cbSplitOnly.Checked ? 1 : 0));
                    sqlCmd.Parameters.AddWithValue("@NOTES", textNotes.Text);
                    sqlCmd.Parameters.AddWithValue("@DISCOUNT_NOTE", textDiscountNotes.Text);
                    sqlCmd.Parameters.AddWithValue("@PRIMARY_ADDRESS", sDefMainAddrId);
                    sqlCmd.Parameters.AddWithValue("@PRIMARY_DEL_ADDR", sDefDelAddrId);

                    sqlCmd.Parameters.AddWithValue("@CBA_TERMS", textCBATerms.Text);
                    sqlCmd.Parameters.AddWithValue("@CBA_BRANCH", textCBABranch.Text);
                    sqlCmd.Parameters.AddWithValue("@CBA_SALESMAN", textCBASalesman.Text);
                    sqlCmd.Parameters.AddWithValue("@CBA_PRICE_CODE", textCBAPriceCode.Text);
                    sqlCmd.Parameters.AddWithValue("@CBA_INV_DISC", textCBAInvoiceDiscount.Text);
                    if (cmbCBAQCurrency.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@QUOTE_CURRENCY", cmbCBAQCurrency.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@QUOTE_CURRENCY", "");
                    }
                    if (cmbCBADefInvType.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@DEF_INVOICE_TYPE", cmbCBADefInvType.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@DEF_INVOICE_TYPE", "");
                    }
                    if (cmbCBADefPIType.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@DEF_PROFORMA_TYPE", cmbCBADefPIType.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@DEF_PROFORMA_TYPE", "");
                    }
                    sqlCmd.Parameters.AddWithValue("@RESTRICTIONS", textCBARestrictions.Text);
                    sqlCmd.Parameters.AddWithValue("@ORDER_ALERT", textCBAOrderAlert.Text);
                    sqlCmd.Parameters.AddWithValue("@SHIPPING_MARKS", textCBAShippingMarks.Text);
                    if (cmbPrimaryContactNo.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@PRIMARY_CONTACT_NO", cmbPrimaryContactNo.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@PRIMARY_CONTACT_NO", "");
                    }
                    if (cmbCBALockCode.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@LOCK_CODE", cmbCBALockCode.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@LOCK_CODE", "");
                    }
                    sqlCmd.Parameters.AddWithValue("@DEF_QUOTE_REBATE", textBoxDefaultQuoteRebate.Value);
                    sqlCmd.Parameters.AddWithValue("@DEF_QUOTE_DISCOUNT", textBoxDefaultQuoteDiscount.Value);
                    sqlCmd.Parameters.AddWithValue("@DEF_SLUSHIE", textBoxDefaultSlushie.Value);
                    if (cmbEntityType.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@ENTITY_TYPE", cmbEntityType.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@ENTITY_TYPE", "");
                    }
                    sqlCmd.Parameters.AddWithValue("@ABN_NO", textABNNo.Text);
                    if (cmbVolumeConv.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@VOLUME_CONV", cmbVolumeConv.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@VOLUME_CONV", "");
                    }
                    if (cmbWeightConv.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@WEIGHT_CONV", cmbWeightConv.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@WEIGHT_CONV", "");
                    }
                    
                    sqlCmd.ExecuteNonQuery();
                    // Address Details
                    if (bNewRow)
                    {
                        sSql = "insert into CUSTOMER_ADDR ( " +
                               "CUSTOMER_ID, ADDRESS_ID, ADDRESS_TYPE, ADDR_1, ADDR_2, ADDR_3, STATE, POST_CODE, COUNTRY, PHONE, FAX, EMAIL_ADDRESS, IS_DELIVERY, ADDR_LABEL ) " +
                               "VALUES ( " +
                               "@CUSTOMER_ID, @ADDRESS_ID, 'OFFICE', @ADDR_1, @ADDR_2, @ADDR_3, @STATE, @POST_CODE, @COUNTRY, @PHONE, @FAX, @EMAIL, @IS_DELIVERY, @ADDR_LABEL )";
                    }
                    else
                    {
                        sSql = "update CUSTOMER_ADDR set " +
                               "ADDR_1 = @ADDR_1, ADDR_2 = @ADDR_2, ADDR_3 = @ADDR_3, STATE = @STATE, " +
                               "POST_CODE = @POST_CODE, COUNTRY = @COUNTRY, PHONE = @PHONE, FAX = @FAX, " +
                               "EMAIL_ADDRESS = @EMAIL, IS_DELIVERY = @IS_DELIVERY, ADDR_LABEL = @ADDR_LABEL " +
                               "where CUSTOMER_ID = @CUSTOMER_ID and ADDRESS_ID = @ADDRESS_ID";
                    }
                    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                    sqlCmd.Parameters.AddWithValue("@CUSTOMER_ID", textCustomerId.Text);
                    sqlCmd.Parameters.AddWithValue("@ADDRESS_ID", sDefMainAddrId);
                    sqlCmd.Parameters.AddWithValue("@ADDR_1", textMainAddr1.Text);
                    sqlCmd.Parameters.AddWithValue("@ADDR_2", textMainAddr2.Text);
                    sqlCmd.Parameters.AddWithValue("@ADDR_3", textMainAddr3.Text);
                    sqlCmd.Parameters.AddWithValue("@STATE", textMainAddrState.Text);
                    sqlCmd.Parameters.AddWithValue("@POST_CODE", textMainAddrPostCode.Text);
                    sqlCmd.Parameters.AddWithValue("@COUNTRY", cmbMainAddrCountry.SelectedValue);
                    sqlCmd.Parameters.AddWithValue("@PHONE", textMainAddrPhone.Text);
                    sqlCmd.Parameters.AddWithValue("@FAX", textMainAddrFax.Text);
                    sqlCmd.Parameters.AddWithValue("@EMAIL", textMainAddrEmail.Text);
                    sqlCmd.Parameters.AddWithValue("@IS_DELIVERY", (sDefDelAddrId == sDefMainAddrId ? 1 : 0));
                    sqlCmd.Parameters.AddWithValue("@ADDR_LABEL", textAddrLabel.Text);

                    sqlCmd.ExecuteNonQuery();
                }
                sqlCmd = null;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void pbCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pbDelete_Click(object sender, EventArgs e)
        {
            string sSql;
            if (bNewRow == false && MessageBox.Show(this, "Are you sure you want to delete this customer, along with addresses and contacts?", "Wait Wait!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Hand) == DialogResult.Yes)
            {
                SqlCommand sqlCmd = new SqlCommand();
                sSql = "DELETE FROM CUSTOMER WHERE CUSTOMER_ID = @CUST_ID";
                sqlCmd.Parameters.AddWithValue("@CUST_ID", textCustomerId.Text);
                sqlCmd.Connection = SqlHost.DBConn;
                sqlCmd.CommandText = sSql;
                sqlCmd.ExecuteNonQuery();
                sSql = "DELETE FROM CUSTOMER_ADDR WHERE CUSTOMER_ID = @CUST_ID";
                sqlCmd.CommandText = sSql;
                sqlCmd.ExecuteNonQuery();
                sSql = "DELETE FROM CUSTOMER_CONTACT WHERE CUSTOMER_ID = @CUST_ID";
                sqlCmd.CommandText = sSql;
                sqlCmd.ExecuteNonQuery();
                sSql = "DELETE FROM CONTEXT_CUST_PROFILE WHERE SYSTEM_CUST_ID = @CUST_ID";
                sqlCmd.CommandText = sSql;
                sqlCmd.ExecuteNonQuery();
                ClearFormDetails(true);
                this.ActiveControl = textCustomerId;
            }
        }

        //public void GenericTransfer(GenericParam g)
        //{
        //    //
        //}

    }
}

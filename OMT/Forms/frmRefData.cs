﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using UbiSqlFramework;
using UbiWeb.Data;
using UbiWeb.Services;
using UbiWeb.Security;
using OMT.MFServices;

namespace OMT.Forms
{
    public partial class frmRefData : Form, IGenericExchange
    {
        private DataTable tblRefTable = new DataTable("RefTable");
        private DataTable tblRefData = new DataTable("RefData");
        private DataRow currentRefTable;
        private DataRow currentRefData;
        private bool bShortDescDisplay;
        private bool bStr1Display;
        private bool bStr2Display;
        private bool bStr3Display;
        private bool bNum1Display;
        private bool bNum2Display;
        private bool bDt1Display;
        private bool bDt2Display;
        private bool bBool1Display;
        private bool bBool2Display;
        private bool bUseFK1;
        private bool bUseFK2;
        private bool bUseExtendedOptions;
        private bool bNewRow;
        private bool bNewTableRow;
        private MFServices.ReferenceTables cRT = new MFServices.ReferenceTables();
        private MFServices.ReferenceData cRD = new MFServices.ReferenceData();

        public frmRefData()
        {
            InitializeComponent();
            SetSecurity();
        }

        private void textBoxPassword_Validating(object sender, CancelEventArgs e)
        {
            SetSecurity();
        }

        private void SetSecurity()
        {
            bool bEnabled = false;
            bEnabled = (textBoxPassword.Text == Constants.SECURITY_REFDATAPASSWORD);

            pbRefTableAdd.Enabled = bEnabled;
            pbRefTableEdit.Enabled = bEnabled;
            pbRefTableDelete.Enabled = bEnabled;
            pbTableSave.Enabled = bEnabled;
            pbRefDataAdd.Enabled = bEnabled;
            pbRefDataDelete.Enabled = bEnabled;
            pbRefDataSave.Enabled = bEnabled;
        }

        private void frmRefData_Load(object sender, EventArgs e)
        {
            RefreshRefTable();
        }

        private void frmRefData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.N))
            {
                if (this.tabLayers.SelectedIndex == 0)
                {
                    pbRefTableAdd.PerformClick();
                }
                else if (this.tabLayers.SelectedIndex == 2)
                {
                    pbRefDataAdd.PerformClick();
                }
            }
        }

        private void RefreshRefTable()
        {
            string sSql;
            bool b;
            DataSet frmDs = new DataSet();

            sSql = "select * from REF_TABLE where DISPLAY_YN = 1";
            b = SqlHost.GetDataSet(sSql, "RefTable", ref frmDs);

            tblRefTable = null;
            tblRefTable = frmDs.Tables["RefTable"];

            dgvRefTable.DataSource = null;
            dgvRefTable.DataSource = tblRefTable;

            dgvRefTable.Columns["REF_TABLE_ID"].Visible = false;
            dgvRefTable.Columns["DESCRIPTION"].Width = (dgvRefTable.Width / 3) - 10;
            dgvRefTable.Columns["DESCRIPTION"].HeaderText = "Reference Table Description";
            dgvRefTable.Columns["DISPLAY_YN"].Visible = false;
            dgvRefTable.Columns["ID_LABEL"].Visible = false;
            dgvRefTable.Columns["DESC_LABEL"].Visible = false;
            dgvRefTable.Columns["SHORT_DESC_LABEL"].Visible = false;
            dgvRefTable.Columns["EXT_STR_1_LABEL"].Visible = false;
            dgvRefTable.Columns["EXT_STR_2_LABEL"].Visible = false;
            dgvRefTable.Columns["EXT_STR_3_LABEL"].Visible = false;
            dgvRefTable.Columns["EXT_NUM_1_LABEL"].Visible = false;
            dgvRefTable.Columns["EXT_NUM_2_LABEL"].Visible = false;
            dgvRefTable.Columns["EXT_DT_1_LABEL"].Visible = false;
            dgvRefTable.Columns["EXT_DT_2_LABEL"].Visible = false;
            dgvRefTable.Columns["EXT_BOOL_1_LABEL"].Visible = false;
            dgvRefTable.Columns["EXT_BOOL_2_LABEL"].Visible = false;
            dgvRefTable.Columns["STR_1_FK"].Visible = false;
            dgvRefTable.Columns["STR_2_FK"].Visible = false;
            dgvRefTable.Columns["EXTENDED_OPTIONS"].Visible = false;
            dgvRefTable.Columns["HELP_DESC"].Visible = true;
            dgvRefTable.Columns["HELP_DESC"].Width = (dgvRefTable.Width / 3) - 10;
            dgvRefTable.Columns["HELP_DESC"].HeaderText = "Help Text";
            dgvRefTable.Columns["NOTES"].Visible = true;
            dgvRefTable.Columns["NOTES"].Width = (dgvRefTable.Width / 3) - 10;
            dgvRefTable.Columns["NOTES"].HeaderText = "Notes";
        }

        private void dgvRefTable_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.N))
            {
                pbRefTableAdd.PerformClick();
            }
        }

        private void dgvRefTable_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            RefreshRefTableData();
        }

        private void dgvRefTable_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            RefreshRefTableData();
        }

        private void RefreshRefTableData()
        {
            DataRow dRow;
            if (dgvRefTable.SelectedRows.Count > 0)
            {
                dRow = ((DataRowView)dgvRefTable.SelectedRows[0].DataBoundItem).Row;
                currentRefTable = dRow;
                PopulateRefData(dRow);
                tabLayers.SelectedTab = tabLayers.TabPages["TabRefData"];
            }
            else if (dgvRefTable.SelectedCells.Count > 0)
            {
                dRow = ((DataRowView)dgvRefTable.SelectedCells[0].OwningRow.DataBoundItem).Row;
                currentRefTable = dRow;
                PopulateRefData(dRow);
                tabLayers.SelectedTab = tabLayers.TabPages["TabRefData"];
            }
        }

        private void PopulateRefData(DataRow row)
        {
            string sSql;
            bool b;
            DataSet frmDs = new DataSet();

            sSql = "select * from REF_DATA where REF_TABLE_ID = '" + row["REF_TABLE_ID"] + "'";
            b = SqlHost.GetDataSet(sSql, "RefData", ref frmDs);
            tblRefData = null;
            tblRefData = frmDs.Tables["RefData"];

            TextHelpNote.Text = "";
            dgvRefData.DataSource = null;
            dgvRefData.DataSource = tblRefData;

            SetRefDataDisplayOptions(row);

            SetRefDataDisplayHeaders(row);

            SetMaintDisplayOptions(row);

            ClearDataMaintData();
        }

        private void SetRefDataDisplayOptions(DataRow row)
        {
            bShortDescDisplay = (row["SHORT_DESC_LABEL"] != System.DBNull.Value && ((string)row["SHORT_DESC_LABEL"]).Length > 0);
            bStr1Display = (row["EXT_STR_1_LABEL"] != System.DBNull.Value && ((string)row["EXT_STR_1_LABEL"]).Length > 0);
            bStr2Display = (row["EXT_STR_2_LABEL"] != System.DBNull.Value && ((string)row["EXT_STR_2_LABEL"]).Length > 0);
            bStr3Display = (row["EXT_STR_3_LABEL"] != System.DBNull.Value && ((string)row["EXT_STR_3_LABEL"]).Length > 0);
            bNum1Display = (row["EXT_NUM_1_LABEL"] != System.DBNull.Value && ((string)row["EXT_NUM_1_LABEL"]).Length > 0);
            bNum2Display = (row["EXT_NUM_2_LABEL"] != System.DBNull.Value && ((string)row["EXT_NUM_2_LABEL"]).Length > 0);
            bDt1Display = (row["EXT_DT_1_LABEL"] != System.DBNull.Value && ((string)row["EXT_DT_1_LABEL"]).Length > 0);
            bDt2Display = (row["EXT_DT_2_LABEL"] != System.DBNull.Value && ((string)row["EXT_DT_2_LABEL"]).Length > 0);
            bBool1Display = (row["EXT_BOOL_1_LABEL"] != System.DBNull.Value && ((string)row["EXT_BOOL_1_LABEL"]).Length > 0);
            bBool2Display = (row["EXT_BOOL_2_LABEL"] != System.DBNull.Value && ((string)row["EXT_BOOL_2_LABEL"]).Length > 0);
            bUseFK1 = (row["STR_1_FK"] != System.DBNull.Value && ((string)row["STR_1_FK"]).Length > 0);
            bUseFK2 = (row["STR_2_FK"] != System.DBNull.Value && ((string)row["STR_2_FK"]).Length > 0);
            bUseExtendedOptions = (row["EXTENDED_OPTIONS"] != System.DBNull.Value && ((string)row["EXTENDED_OPTIONS"]).Length > 0);
        }

        private void SetRefDataDisplayHeaders(DataRow row)
        {
            dgvRefData.Columns["REF_TABLE_ID"].Visible = false;
            dgvRefData.Columns["DISPLAY_YN"].Visible = false;
            dgvRefData.Columns["REF_ID"].Width = 150;
            dgvRefData.Columns["REF_ID"].HeaderText = (string)row["ID_LABEL"];
            dgvRefData.Columns["DESCRIPTION"].Width = 300;
            dgvRefData.Columns["DESCRIPTION"].HeaderText = (string)row["DESC_LABEL"];
            dgvRefData.Columns["NOTES"].HeaderText = "Special Notes";
            dgvRefData.Columns["NOTES"].Width = 400;

            if (bShortDescDisplay)
            {
                dgvRefData.Columns["SHORT_DESC"].Visible = true;
                dgvRefData.Columns["SHORT_DESC"].Width = 150;
                dgvRefData.Columns["SHORT_DESC"].HeaderText = (string)row["SHORT_DESC_LABEL"];
            }
            else
            {
                dgvRefData.Columns["SHORT_DESC"].Visible = false;
            }

            if (bStr1Display)
            {
                dgvRefData.Columns["EXT_STR_1"].Visible = true;
                dgvRefData.Columns["EXT_STR_1"].Width = 150;
                dgvRefData.Columns["EXT_STR_1"].HeaderText = (string)row["EXT_STR_1_LABEL"];
            }
            else
            {
                dgvRefData.Columns["EXT_STR_1"].Visible = false;
            }

            if (bStr2Display)
            {
                dgvRefData.Columns["EXT_STR_2"].Visible = true;
                dgvRefData.Columns["EXT_STR_2"].Width = 150;
                dgvRefData.Columns["EXT_STR_2"].HeaderText = (string)row["EXT_STR_2_LABEL"];
            }
            else
            {
                dgvRefData.Columns["EXT_STR_2"].Visible = false;
            }

            if (bStr3Display)
            {
                dgvRefData.Columns["EXT_STR_3"].Visible = true;
                dgvRefData.Columns["EXT_STR_3"].Width = 150;
                dgvRefData.Columns["EXT_STR_3"].HeaderText = (string)row["EXT_STR_3_LABEL"];
            }
            else
            {
                dgvRefData.Columns["EXT_STR_3"].Visible = false;
            }

            if (bNum1Display)
            {
                dgvRefData.Columns["EXT_NUM_1"].Visible = true;
                dgvRefData.Columns["EXT_NUM_1"].Width = 150;
                dgvRefData.Columns["EXT_NUM_1"].HeaderText = (string)row["EXT_NUM_1_LABEL"];
            }
            else
            {
                dgvRefData.Columns["EXT_NUM_1"].Visible = false;
            }

            if (bNum2Display)
            {
                dgvRefData.Columns["EXT_NUM_2"].Visible = true;
                dgvRefData.Columns["EXT_NUM_2"].Width = 150;
                dgvRefData.Columns["EXT_NUM_2"].HeaderText = (string)row["EXT_NUM_2_LABEL"];
            }
            else
            {
                dgvRefData.Columns["EXT_NUM_2"].Visible = false;
            }

            if (bDt1Display)
            {
                dgvRefData.Columns["EXT_DT_1"].Visible = true;
                dgvRefData.Columns["EXT_DT_1"].Width = 150;
                dgvRefData.Columns["EXT_DT_1"].HeaderText = (string)row["EXT_DT_1_LABEL"];
            }
            else
            {
                dgvRefData.Columns["EXT_DT_1"].Visible = false;
            }

            if (bDt2Display)
            {
                dgvRefData.Columns["EXT_DT_2"].Visible = true;
                dgvRefData.Columns["EXT_DT_2"].Width = 150;
                dgvRefData.Columns["EXT_DT_2"].HeaderText = (string)row["EXT_DT_2_LABEL"];
            }
            else
            {
                dgvRefData.Columns["EXT_DT_2"].Visible = false;
            }

            if (bBool1Display)
            {
                dgvRefData.Columns["EXT_BOOL_1"].Visible = true;
                dgvRefData.Columns["EXT_BOOL_1"].Width = 150;
                dgvRefData.Columns["EXT_BOOL_1"].HeaderText = (string)row["EXT_BOOL_1_LABEL"];
            }
            else
            {
                dgvRefData.Columns["EXT_BOOL_1"].Visible = false;
            }

            if (bBool2Display)
            {
                dgvRefData.Columns["EXT_BOOL_2"].Visible = true;
                dgvRefData.Columns["EXT_BOOL_2"].Width = 150;
                dgvRefData.Columns["EXT_BOOL_2"].HeaderText = (string)row["EXT_BOOL_2_LABEL"];
            }
            else
            {
                dgvRefData.Columns["EXT_BOOL_2"].Visible = false;
            }

            if (bUseExtendedOptions)
            {
                dgvRefData.Columns["EXTENDED_OPTIONS"].Visible = true;
                dgvRefData.Columns["EXTENDED_OPTIONS"].Width = 200;
                dgvRefData.Columns["EXTENDED_OPTIONS"].HeaderText = "Extended Options";
            }
            else
            {
                dgvRefData.Columns["EXT_BOOL_2"].Visible = false;
            }

            if (row["HELP_DESC"] != System.DBNull.Value && ((string)row["HELP_DESC"]).Length > 0)
            {
                TextHelpNote.Text = (string)row["HELP_DESC"];
            }
        }

        private void SetMaintDisplayOptions(DataRow row)
        {
            textDataRefTableId.Text = (string)row["REF_TABLE_ID"];
            labelRefId.Text = (string)row["ID_LABEL"] + ":";
            labelDescription.Text = (string)row["DESC_LABEL"] + ":";
            labelNotes.Text = "Special Notes:";
            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();
            string sSql;
            bool b;

            if (bShortDescDisplay)
            {
                labelShortDesc.Text = (string)row["SHORT_DESC_LABEL"] + ":";
                labelShortDesc.Visible = true;
                textDataShortDesc.Visible = true;
                tlpDataMaintenance.RowStyles[6].Height = 24;
            }
            else
            {
                labelShortDesc.Visible = false;
                textDataShortDesc.Visible = false;
                tlpDataMaintenance.RowStyles[6].Height = 1;
            }

            if (bStr1Display)
            {
                if (bUseFK1)
                {
                    textDataExtStr1.Visible = false;
                    labelExtStr1a.Visible = false;
                    comboDataExtStr1.Visible = true;
                    labelExtStr1b.Visible = true;
                    labelExtStr1b.Text = (string)row["EXT_STR_1_LABEL"] + ":";
                    tlpDataMaintenance.RowStyles[7].Height = 1;
                    tlpDataMaintenance.RowStyles[8].Height = 24;
                    comboDataExtStr1.DataSource = null;
                    comboDataExtStr1.Items.Clear();
                    sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = '" + (string)row["STR_1_FK"] + "'";
                    b = SqlHost.GetDataSet(sSql, "Str1Data", ref ds1);
                    comboDataExtStr1.DataSource = ds1.Tables["Str1Data"];
                    comboDataExtStr1.ValueMember = "REF_ID";
                    comboDataExtStr1.DisplayMember = "DESCRIPTION";
                    comboDataExtStr1.DropDownStyle = ComboBoxStyle.DropDownList;
                }
                else
                {
                    textDataExtStr1.Visible = true;
                    labelExtStr1a.Visible = true;
                    comboDataExtStr1.Visible = false;
                    labelExtStr1b.Visible = false;
                    labelExtStr1a.Text = (string)row["EXT_STR_1_LABEL"] + ":";
                    tlpDataMaintenance.RowStyles[8].Height = 1;
                    tlpDataMaintenance.RowStyles[7].Height = 24;
                }
            }
            else
            {
                labelExtStr1a.Visible = false;
                textDataExtStr1.Visible = false;
                labelExtStr1b.Visible = false;
                comboDataExtStr1.Visible = false;
            }

            if (bStr2Display)
            {
                if (bUseFK2)
                {
                    textDataExtStr2.Visible = false;
                    labelExtStr2a.Visible = false;
                    comboDataExtStr2.Visible = true;
                    labelExtStr2b.Visible = true;
                    labelExtStr2b.Text = (string)row["EXT_STR_2_LABEL"] + ":";
                    tlpDataMaintenance.RowStyles[9].Height = 1;
                    tlpDataMaintenance.RowStyles[10].Height = 24;
                    comboDataExtStr2.DataSource = null;
                    comboDataExtStr2.Items.Clear();
                    sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = '" + (string)row["STR_2_FK"] + "'";
                    b = SqlHost.GetDataSet(sSql, "Str2Data", ref ds2);
                    comboDataExtStr2.DataSource = ds2.Tables["Str2Data"];
                    comboDataExtStr2.ValueMember = "REF_ID";
                    comboDataExtStr2.DisplayMember = "DESCRIPTION";
                    comboDataExtStr2.DropDownStyle = ComboBoxStyle.DropDownList;
                }
                else
                {
                    textDataExtStr2.Visible = true;
                    labelExtStr2a.Visible = true;
                    comboDataExtStr2.Visible = false;
                    labelExtStr2b.Visible = false;
                    labelExtStr2a.Text = (string)row["EXT_STR_2_LABEL"] + ":";
                    tlpDataMaintenance.RowStyles[10].Height = 1;
                    tlpDataMaintenance.RowStyles[9].Height = 24;
                }
            }
            else
            {
                labelExtStr2a.Visible = false;
                textDataExtStr2.Visible = false;
                labelExtStr2b.Visible = false;
                comboDataExtStr2.Visible = false;
            }

            if (bStr3Display)
            {
                labelExtStr3.Text = (string)row["EXT_STR_3_LABEL"] + ":";
                labelExtStr3.Visible = true;
                textDataExtStr3.Visible = true;
                tlpDataMaintenance.RowStyles[11].Height = 24;
            }
            else
            {
                labelExtStr3.Visible = false;
                textDataExtStr3.Visible = false;
                tlpDataMaintenance.RowStyles[11].Height = 1;
            }

            if (bNum1Display)
            {
                labelExtNum1.Text = (string)row["EXT_NUM_1_LABEL"] + ":";
                labelExtNum1.Visible = true;
                textDataExtNum1.Visible = true;
                tlpDataMaintenance.RowStyles[12].Height = 24;
            }
            else
            {
                labelExtNum1.Visible = false;
                textDataExtNum1.Visible = false;
                tlpDataMaintenance.RowStyles[12].Height = 1;
            }

            if (bNum2Display)
            {
                labelExtNum2.Text = (string)row["EXT_NUM_2_LABEL"] + ":";
                labelExtNum2.Visible = true;
                textDataExtNum2.Visible = true;
                tlpDataMaintenance.RowStyles[13].Height = 24;
            }
            else
            {
                labelExtNum2.Visible = false;
                textDataExtNum2.Visible = false;
                tlpDataMaintenance.RowStyles[13].Height = 1;
            }

            if (bDt1Display)
            {
                labelExtDt1.Text = (string)row["EXT_DT_1_LABEL"] + ":";
                labelExtDt1.Visible = true;
                dtpDataExtDt1.Visible = true;
                tlpDataMaintenance.RowStyles[14].Height = 24;
            }
            else
            {
                labelExtDt1.Visible = false;
                dtpDataExtDt1.Visible = false;
                tlpDataMaintenance.RowStyles[14].Height = 1;
            }

            if (bDt2Display)
            {
                labelExtDt2.Text = (string)row["EXT_DT_2_LABEL"] + ":";
                labelExtDt2.Visible = true;
                dtpDataExtDt2.Visible = true;
                tlpDataMaintenance.RowStyles[15].Height = 24;
            }
            else
            {
                labelExtDt2.Visible = false;
                dtpDataExtDt2.Visible = false;
                tlpDataMaintenance.RowStyles[15].Height = 1;
            }

            if (bBool1Display)
            {
                labelExtBool1.Text = (string)row["EXT_BOOL_1_LABEL"] + ":";
                labelExtBool1.Visible = true;
                checkDataExtBool1.Visible = true;
                tlpDataMaintenance.RowStyles[16].Height = 24;
            }
            else
            {
                labelExtBool1.Visible = false;
                checkDataExtBool1.Visible = false;
                tlpDataMaintenance.RowStyles[16].Height = 1;
            }

            if (bBool2Display)
            {
                labelExtBool2.Text = (string)row["EXT_BOOL_2_LABEL"] + ":";
                labelExtBool2.Visible = true;
                checkDataExtBool2.Visible = true;
                tlpDataMaintenance.RowStyles[17].Height = 24;
            }
            else
            {
                labelExtBool2.Visible = false;
                checkDataExtBool2.Visible = false;
                tlpDataMaintenance.RowStyles[17].Height = 1;
            }

            if (bUseExtendedOptions)
            {
                labelExtendedOptions.Visible = true;
                textDataExtendedOptions.Visible = true;
                pbDataExtendedOptions.Visible = true;
                tlpDataMaintenance.RowStyles[18].Height = 30;
            }
            else
            {
                labelExtendedOptions.Visible = false;
                textDataExtendedOptions.Visible = false;
                pbDataExtendedOptions.Visible = false;
                tlpDataMaintenance.RowStyles[18].Height = 1;
            }
        }

        private void ClearDataMaintData()
        {
            textDataRefTableId.Text = "";
            textDataRefId.Text = "";
            textDataDescription.Text = "";
            textDataShortDesc.Text = "";
            textDataExtStr1.Text = "";
            comboDataExtStr1.SelectedIndex = -1;
            comboDataExtStr2.SelectedIndex = -1;
            textDataExtStr2.Text = "";
            textDataExtStr3.Text = "";
            textDataExtNum1.Text = "";
            textDataExtNum2.Text = "";
            dtpDataExtDt1.Value = DateTime.Now; //DateTime.MinValue
            dtpDataExtDt2.Value = DateTime.Now; //DateTime.MinValue
            checkDataExtBool1.Checked = false;
            checkDataExtBool2.Checked = false;
            textDataExtendedOptions.Text = "";
            textDataNotes.Text = "";
        }

        private void pbRefTableAdd_Click(object sender, EventArgs e)
        {
            DataRow dRow;
            dRow = tblRefTable.NewRow();
            currentRefTable = dRow;
            PopulateRefTableMaint(dRow);
            tabLayers.SelectedTab = tabLayers.TabPages["TabRefTableRow"];
            bNewTableRow = true;
        }

        private void pbRefTableEdit_Click(object sender, EventArgs e)
        {
            DataRow dRow;
            if (dgvRefTable.SelectedRows.Count > 0)
            {
                dRow = ((DataRowView)dgvRefTable.SelectedRows[0].DataBoundItem).Row;
                currentRefTable = dRow;
                PopulateRefTableMaint(dRow);
                tabLayers.SelectedTab = tabLayers.TabPages["TabRefTableRow"];
                bNewTableRow = false;
                this.ActiveControl = textTableRefTableId;
            }
            else if (dgvRefTable.SelectedCells.Count > 0)
            {
                dRow = ((DataRowView)dgvRefTable.SelectedCells[0].OwningRow.DataBoundItem).Row;
                currentRefTable = dRow;
                PopulateRefTableMaint(dRow);
                tabLayers.SelectedTab = tabLayers.TabPages["TabRefTableRow"];
                bNewTableRow = false;
                this.ActiveControl = textTableDesc;
            }
        }

        private void PopulateRefTableMaint(DataRow row)
        {
            ClearTableMaintData();
            if (row["REF_TABLE_ID"] != System.DBNull.Value)
            {
                textTableRefTableId.Text = (string)row["REF_TABLE_ID"];
                if (textTableRefTableId.Text.Length > 0)
                {
                    textTableRefTableId.ReadOnly = true;
                }
                else
                {
                    textTableRefTableId.ReadOnly = false;
                }
            }
            else
            {
                textTableRefTableId.ReadOnly = false;
            }

            if (row["DESCRIPTION"] != System.DBNull.Value)
            {
                textTableDesc.Text = (string)row["DESCRIPTION"];
            }

            if (row["HELP_DESC"] != System.DBNull.Value)
            {
                textTableHelpDesc.Text = (string)row["HELP_DESC"];
            }

            if (row["DISPLAY_YN"] != System.DBNull.Value)
            {
                checkTableDisplayYn.Checked = Convert.ToBoolean(row["DISPLAY_YN"]);
            }

            if (row["ID_LABEL"] != System.DBNull.Value)
            {
                textTableIdLabel.Text = (string)row["ID_LABEL"];
            }

            if (row["DESC_LABEL"] != System.DBNull.Value)
            {
                textTableDescLabel.Text = (string)row["DESC_LABEL"];
            }

            if (row["SHORT_DESC_LABEL"] != System.DBNull.Value)
            {
                textTableShortDescLabel.Text = (string)row["SHORT_DESC_LABEL"];
            }

            if (row["EXT_STR_1_LABEL"] != System.DBNull.Value)
            {
                textTableExtStr1Label.Text = (string)row["EXT_STR_1_LABEL"];
            }

            if (row["EXT_STR_2_LABEL"] != System.DBNull.Value)
            {
                textTableExtStr2Label.Text = (string)row["EXT_STR_2_LABEL"];
            }

            if (row["EXT_STR_3_LABEL"] != System.DBNull.Value)
            {
                textTableExtStr3Label.Text = (string)row["EXT_STR_3_LABEL"];
            }

            if (row["EXT_NUM_1_LABEL"] != System.DBNull.Value)
            {
                textTableExtNum1Label.Text = (string)row["EXT_NUM_1_LABEL"];
            }

            if (row["EXT_NUM_2_LABEL"] != System.DBNull.Value)
            {
                textTableExtNum2Label.Text = (string)row["EXT_NUM_2_LABEL"];
            }

            if (row["EXT_DT_1_LABEL"] != System.DBNull.Value)
            {
                textTableExtDt1Label.Text = (string)row["EXT_DT_1_LABEL"];
            }

            if (row["EXT_DT_2_LABEL"] != System.DBNull.Value)
            {
                textTableExtDt2Label.Text = (string)row["EXT_DT_2_LABEL"];
            }

            if (row["EXT_BOOL_1_LABEL"] != System.DBNull.Value)
            {
                textTableExtBool1Label.Text = (string)row["EXT_BOOL_1_LABEL"];
            }

            if (row["EXT_BOOL_2_LABEL"] != System.DBNull.Value)
            {
                textTableExtBool2Label.Text = (string)row["EXT_BOOL_2_LABEL"];
            }

            if (row["STR_1_FK"] != System.DBNull.Value)
            {
                textTableStr1FK.Text = (string)row["STR_1_FK"];
            }

            if (row["STR_2_FK"] != System.DBNull.Value)
            {
                textTableStr2FK.Text = (string)row["STR_2_FK"];
            }

            if (row["EXTENDED_OPTIONS"] != System.DBNull.Value)
            {
                textTableExtendedOptions.Text = (string)row["EXTENDED_OPTIONS"];
            }

            if (row["NOTES"] != System.DBNull.Value)
            {
                textTableNotes.Text = (string)row["NOTES"];
            }
            else
            {
                textTableNotes.Text = "";
            }

        }
        private void pbRefTableDelete_Click(object sender, EventArgs e)
        {
            int nSeq;
            DataRow dRow;
            MFServices.ReferenceTables rt = new MFServices.ReferenceTables();
            MFServices.ReferenceTablesOperation cOp = new MFServices.ReferenceTablesOperation();
            MFServices.Transaction t = new MFServices.Transaction();
            List<string> ops;
            List<MFServices.ReferenceTablesOperation> cOps;

            if (dgvRefTable.SelectedCells.Count > 0)
            {
                dRow = ((DataRowView)dgvRefTable.SelectedCells[0].OwningRow.DataBoundItem).Row;
            }
            else if (dgvRefData.SelectedRows.Count > 0)
            {
                dRow = ((DataRowView)dgvRefTable.SelectedRows[0].DataBoundItem).Row;
            }
            else
            {
                dRow = null;
            }

            nSeq = 1;

            if (dRow != null && MessageBox.Show(this, "Are you sure you want to delete this entry?", "Delete Record", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Hand) == DialogResult.Yes)
            {
                try
                {
                    rt.RefTableId = (string)dRow["REF_TABLE_ID"];
                    cOp.Verb = MFServices.OperationVerb.Delete;
                    t.TransactionId = nSeq.ToString();
                    cOp.OperationId = "ReferenceTables-" + nSeq.ToString("00000");
                    cOp.Sequence = nSeq;
                    if (t.OperationSequence == null)
                        { ops = new List<string>(); }
                    else
                        { ops = t.OperationSequence.ToList(); }
                    ops.Add(cOp.OperationId);
                    t.OperationSequence = ops.ToArray();
                    cOp.OperationEntity = MFServices.OperationEntity.ReferenceTables;
                    cOp.DataMember = rt;
                    if (t.ReferenceTablesOperations == null)
                        { cOps = new List<MFServices.ReferenceTablesOperation>(); }
                    else
                        { cOps = t.ReferenceTablesOperations.ToList(); }
                    cOps.Add(cOp);
                    t.ReferenceTablesOperations = cOps.ToArray();

                    MFServices.TransactionResponse tr = new MFServices.TransactionResponse();
                    MFServicesSoapClient r = new MFServicesSoapClient();
                    tr = r.ProcessTransaction(AppHost.WebSessionKey, t);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Delete Error", MessageBoxButtons.OK);
                }
                RefreshRefTable();
                ClearTableMaintData();
            }
        }

        private void ClearTableMaintData()
        {
            textTableRefTableId.Text = "";
            textTableDesc.Text = "";
            textTableHelpDesc.Text = "";
            checkTableDisplayYn.Checked = false;
            textTableIdLabel.Text = "";
            textTableDescLabel.Text = "";
            textTableShortDescLabel.Text = "";
            textTableExtStr1Label.Text = "";
            textTableExtStr2Label.Text = "";
            textTableExtStr3Label.Text = "";
            textTableExtNum1Label.Text = "";
            textTableExtNum2Label.Text = "";
            textTableExtDt1Label.Text = "";
            textTableExtDt2Label.Text = "";
            textTableExtBool1Label.Text = "";
            textTableExtBool2Label.Text = "";
            textTableStr1FK.Text = "";
            textTableStr2FK.Text = "";
            textTableExtendedOptions.Text = "";
            textTableNotes.Text = "";
        }

        private void pbTableSave_Click(object sender, EventArgs e)
        {
            if (textTableRefTableId.Text.Length == 0)
            {
                MessageBox.Show(this, "You need to provide an ID for this record.", "Save", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }

            if (textTableIdLabel.Text.Length == 0)
            {
                MessageBox.Show(this, "You need to provide an ID Label for this record.", "Save", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }

            if (textTableDescLabel.Text.Length == 0)
            {
                MessageBox.Show(this, "You need to provide an Description Label for this record.", "Save", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }

            SaveRefTableRow(1);
        }

        private void SaveRefTableRow(int nSeq)
        {
            MFServices.ReferenceTables rt = new MFServices.ReferenceTables();
            MFServices.ReferenceTablesOperation cOp = new MFServices.ReferenceTablesOperation();
            MFServices.Transaction t = new MFServices.Transaction();
            List<string> ops;
            List<MFServices.ReferenceTablesOperation> cOps;

            fnPopulateTRClassFromScreen();

            try
            {
                if (bNewTableRow)
                    { cOp.Verb = MFServices.OperationVerb.Create; }
                else
                    { cOp.Verb = MFServices.OperationVerb.Update; }
                t.TransactionId = nSeq.ToString();
                cOp.OperationId = "ReferenceTables-" + nSeq.ToString("00000");
                cOp.Sequence = nSeq;
                if (t.OperationSequence == null)
                    { ops = new List<string>(); }
                else
                    { ops = t.OperationSequence.ToList(); }
                ops.Add(cOp.OperationId);
                t.OperationSequence = ops.ToArray();
                cOp.OperationEntity = MFServices.OperationEntity.ReferenceTables;
                cOp.DataMember = cRT;
                if (t.ReferenceTablesOperations == null)
                    { cOps = new List<MFServices.ReferenceTablesOperation>(); }
                else
                    { cOps = t.ReferenceTablesOperations.ToList(); }
                cOps.Add(cOp);
                t.ReferenceTablesOperations = cOps.ToArray();

                MFServices.TransactionResponse tr = new MFServices.TransactionResponse();
                MFServicesSoapClient r = new MFServicesSoapClient();
                tr = r.ProcessTransaction(AppHost.WebSessionKey, t);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Save Error", MessageBoxButtons.OK);
            }

            RefreshRefTable();
            ClearTableMaintData();

            tabLayers.SelectedTab = tabLayers.TabPages["TabRefTable"];
        }

        private void fnPopulateTRClassFromScreen()
        {
            cRT.RefTableId = textTableRefTableId.Text;
            cRT.IDLabel = textTableIdLabel.Text;
            cRT.Description = textTableDesc.Text;
            if (checkTableDisplayYn.Checked == true)
                { cRT.DisplayYN = 1; }
            else
                { cRT.DisplayYN = 0; }
            cRT.DescLabel = textTableDescLabel.Text;
            cRT.ShortDescLabel = textTableShortDescLabel.Text;
            cRT.ExtStr1Label = textTableExtStr1Label.Text;
            cRT.ExtStr2Label = textTableExtStr2Label.Text;
            cRT.ExtStr3Label = textTableExtStr1Label.Text;
            cRT.ExtNum1Label = textTableExtNum1Label.Text;
            cRT.ExtNum2Label = textTableExtNum2Label.Text;
            cRT.ExtDT1Label = textTableExtDt1Label.Text;
            cRT.ExtDT2Label = textTableExtDt2Label.Text;
            cRT.ExtBool1Label = textTableExtBool1Label.Text;
            cRT.ExtBool2Label = textTableExtBool2Label.Text;
            cRT.Str1FK = textTableStr1FK.Text;
            cRT.Str2FK = textTableStr2FK.Text;
            cRT.HelpDesc = textTableHelpDesc.Text;
            cRT.Notes = textTableNotes.Text;
            cRT.ExtendedOptions = textTableExtendedOptions.Text;
        }

        private void fnPopulateTDClassFromScreen()
        {
            cRD.RefTableId = textDataRefTableId.Text;
            cRD.RefId = textDataRefId.Text;
            cRD.Description = textDataDescription.Text;
            cRD.ShortDesc = textDataShortDesc.Text;
            if (checkTableDisplayYn.Checked == true)
                { cRD.DisplayYN = 1; }
            else
                { cRD.DisplayYN = 0; }
            if (bUseFK1)
            {
                cRD.ExtStr1 = comboDataExtStr1.SelectedValue.ToString();
            }
            else
            {
                cRD.ExtStr1 = textDataExtStr1.Text;
            }
            if (bUseFK2)
            {
                cRD.ExtStr2 = comboDataExtStr2.SelectedValue.ToString();
            }
            else
            {
                cRD.ExtStr2 = textDataExtStr2.Text;
            }
            cRD.ExtStr3 = textDataExtStr3.Text;
            float result;
            if (float.TryParse(textDataExtNum1.Text, out result))
            {
                cRD.ExtNum1 = result;
            }
            else
            {
                cRD.ExtNum1 = 0;
            }
            if (float.TryParse(textDataExtNum2.Text, out result))
            {
                cRD.ExtNum2 = result;
            }
            else
            {
                cRD.ExtNum2 = 0;
            }
            cRD.ExtDT1 = dtpDataExtDt1.Value;
            cRD.ExtDT2 = dtpDataExtDt2.Value;
            if (checkDataExtBool1.Checked)
            {
                cRD.ExtBool1 = true;
            }
            else
            {
                cRD.ExtBool1 = false;
            }
            if (checkDataExtBool2.Checked)
            {
                cRD.ExtBool2 = true;
            }
            else
            {
                cRD.ExtBool2 = false;
            }
            cRD.Notes = textDataNotes.Text;
            Int32 nOptions;
            if (Int32.TryParse(textDataExtendedOptions.Text, out nOptions))
            {
                cRD.ExtendedOptions =  nOptions;
            }
            else
            {
                cRD.ExtendedOptions = 0;
            }
        }

        private void pbTableCancel_Click(object sender, EventArgs e)
        {
            currentRefTable = null;
            ClearTableMaintData();
            tabLayers.SelectedTab = tabLayers.TabPages["TabRefTable"];
        }

        private void dgvRefData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.N))
            {
                pbRefDataAdd.PerformClick();
            }
        }

        private void dgvRefData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            RefreshRefDataData();
        }

        private void dgvRefData_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            RefreshRefDataData();
        }

        private void RefreshRefDataData()
        {
            DataRow dRow;
            if (dgvRefData.SelectedRows.Count > 0)
            {
                dRow = ((DataRowView)dgvRefData.SelectedRows[0].DataBoundItem).Row;
                currentRefData = dRow;
                PopulateRefDataMaint(dRow);
                tabLayers.SelectedTab = tabLayers.TabPages["TabRefDataRow"];
                bNewRow = false;
            }
            else if (dgvRefData.SelectedCells.Count > 0)
            {
                dRow = ((DataRowView)dgvRefData.SelectedCells[0].OwningRow.DataBoundItem).Row;
                currentRefData = dRow;
                PopulateRefDataMaint(dRow);
                tabLayers.SelectedTab = tabLayers.TabPages["TabRefDataRow"];
                bNewRow = false;
            }
            this.ActiveControl = textDataDescription;
        }

        private void pbAdd_Click(object sender, EventArgs e)
        {
            DataRow dRow;
            dRow = tblRefData.NewRow();
            dRow["REF_TABLE_ID"] = currentRefTable["REF_TABLE_ID"];
            currentRefData = dRow;
            PopulateRefDataMaint(dRow);
            tabLayers.SelectedTab = tabLayers.TabPages["TabRefDataRow"];
            bNewRow = true;
            this.ActiveControl = textDataRefId;
        }

        private void PopulateRefDataMaint(DataRow row)
        {
            ClearDataMaintData();
            if (row["REF_TABLE_ID"] != System.DBNull.Value)
            {
                textDataRefTableId.Text = (string)row["REF_TABLE_ID"];
            }

            if (row["REF_ID"] != System.DBNull.Value)
            {
                textDataRefId.Text = (string)row["REF_ID"];
                if (textDataRefId.Text.Length > 0)
                {
                    textDataRefId.ReadOnly = true;
                }
                else
                {
                    textDataRefId.ReadOnly = false;
                }
            }
            else
            {
                textDataRefId.ReadOnly = false;
            }

            if (row["DESCRIPTION"] != System.DBNull.Value)
            {
                textDataDescription.Text = (string)row["DESCRIPTION"];
            }

            if (bShortDescDisplay)
            {
                if (row["SHORT_DESC"] != System.DBNull.Value)
                {
                    textDataShortDesc.Text = (string)row["SHORT_DESC"];
                }
                else
                {
                    textDataShortDesc.Text = "";
                }
            }

            if (bStr1Display)
            {
                if (bUseFK1)
                {
                    if (row["EXT_STR_1"] != System.DBNull.Value && ((string)row["EXT_STR_1"]).Length > 0)
                    {
                        comboDataExtStr1.SelectedValue = (string)row["EXT_STR_1"];
                    }
                    else
                    {
                        comboDataExtStr1.SelectedIndex = -1;
                    }
                }
                else
                {
                    if (row["EXT_STR_1"] != System.DBNull.Value && ((string)row["EXT_STR_1"]).Length > 0)
                    {
                        textDataExtStr1.Text = (string)row["EXT_STR_1"];
                    }
                    else
                    {
                        textDataExtStr1.Text = "";
                    }
                }
            }

            if (bStr2Display)
            {
                if (bUseFK2)
                {
                    if (row["EXT_STR_2"] != System.DBNull.Value && ((string)row["EXT_STR_2"]).Length > 0)
                    {
                        comboDataExtStr2.SelectedValue = (string)row["EXT_STR_2"];
                    }
                    else
                    {
                        comboDataExtStr2.SelectedIndex = -1;
                    }
                }
                else
                {
                    if (row["EXT_STR_2"] != System.DBNull.Value && ((string)row["EXT_STR_2"]).Length > 0)
                    {
                        textDataExtStr2.Text = (string)row["EXT_STR_2"];
                    }
                    else
                    {
                        textDataExtStr2.Text = "";
                    }
                }
            }

            if (bStr3Display)
            {
                if (row["EXT_STR_3"] != System.DBNull.Value)
                {
                    textDataExtStr3.Text = (string)row["EXT_STR_3"];
                }
                else
                {
                    textDataExtStr3.Text = "";
                }
            }

            if (bNum1Display)
            {
                if (row["EXT_NUM_1"] != System.DBNull.Value)
                {
                    textDataExtNum1.Text = ((double)row["EXT_NUM_1"]).ToString();
                }
                else
                {
                    textDataExtNum1.Text = "";
                }
            }

            if (bNum2Display)
            {
                if (row["EXT_NUM_2"] != System.DBNull.Value)
                {
                    textDataExtNum2.Text = ((double)row["EXT_NUM_2"]).ToString();
                }
                else
                {
                    textDataExtNum2.Text = "";
                }
            }

            if (bDt1Display)
            {
                if (row["EXT_DT_1"] != System.DBNull.Value)
                {
                    dtpDataExtDt1.Value = (DateTime)row["EXT_DT_1"];
                }
                else
                {
                    dtpDataExtDt1.Value = DateTime.Now;
                }
            }

            if (bDt2Display)
            {
                if (row["EXT_DT_2"] != System.DBNull.Value)
                {
                    dtpDataExtDt2.Value = (DateTime)row["EXT_DT_2"];
                }
                else
                {
                    dtpDataExtDt2.Value = DateTime.Now;
                }
            }

            if (bBool1Display)
            {
                if (row["EXT_BOOL_1"] != System.DBNull.Value)
                {
                    checkDataExtBool1.Checked = (bool)row["EXT_BOOL_1"];
                }
                else
                {
                    checkDataExtBool1.Checked = false;
                }
            }

            if (bBool2Display)
            {
                if (row["EXT_BOOL_2"] != System.DBNull.Value)
                {
                    checkDataExtBool2.Checked = (bool)row["EXT_BOOL_2"];
                }
                else
                {
                    checkDataExtBool2.Checked = false;
                }
            }

            if (bUseExtendedOptions)
            {
                if (row["EXTENDED_OPTIONS"] != System.DBNull.Value)
                {
                    textDataExtendedOptions.Text = ((Int32)row["EXTENDED_OPTIONS"]).ToString("0");
                }
                else
                {
                    textDataExtendedOptions.Text = "0";
                }
            }

            if (row["NOTES"] != System.DBNull.Value)
            {
                textDataNotes.Text = (string)row["NOTES"];
            }
            else
            {
                textDataNotes.Text = "";
            }
        }

        private void pbDelete_Click(object sender, EventArgs e)
        {
            int nSeq;
            DataRow dRow;
            MFServices.ReferenceData rd = new MFServices.ReferenceData();
            MFServices.ReferenceDataOperation cOp = new MFServices.ReferenceDataOperation();
            MFServices.Transaction t = new MFServices.Transaction();
            List<string> ops;
            List<MFServices.ReferenceDataOperation> cOps;

            nSeq = 1;

            if (dgvRefData.SelectedRows.Count > 0 && MessageBox.Show(this, "Are you sure you want to delete this entry?", "Delete Record", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Hand) == DialogResult.Yes)
            {
                try
                {
                    dRow = ((DataRowView)dgvRefData.SelectedRows[0].DataBoundItem).Row;
                    rd.RefTableId = (string)dRow["REF_TABLE_ID"];
                    rd.RefId = (string)dRow["REF_ID"];
                    cOp.Verb = MFServices.OperationVerb.Delete;
                    t.TransactionId = nSeq.ToString();
                    cOp.OperationId = "ReferenceData-" + nSeq.ToString("00000");
                    cOp.Sequence = nSeq;
                    if (t.OperationSequence == null)
                        { ops = new List<string>(); }
                    else
                        { ops = t.OperationSequence.ToList(); }
                    ops.Add(cOp.OperationId);
                    t.OperationSequence = ops.ToArray();
                    cOp.OperationEntity = MFServices.OperationEntity.ReferenceData;
                    cOp.DataMember = rd;
                    if (t.ReferenceDataOperations == null)
                        { cOps = new List<MFServices.ReferenceDataOperation>(); }
                    else
                        { cOps = t.ReferenceDataOperations.ToList(); }
                    cOps.Add(cOp);
                    t.ReferenceDataOperations = cOps.ToArray();

                    MFServices.TransactionResponse tr = new MFServices.TransactionResponse();
                    MFServicesSoapClient r = new MFServicesSoapClient();
                    tr = r.ProcessTransaction(AppHost.WebSessionKey, t);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Delete Error", MessageBoxButtons.OK);
                }

                RefreshRefTableData();
                ClearDataMaintData();
            }
        }

        private void pbDataExtendedOptions_Click(object sender, EventArgs e)
        {
            Int32 x;
            dlgBitwiseOptionPicker dlg = new dlgBitwiseOptionPicker();
            dlg.Partner = this;
            GenericParam p = new GenericParam();
            p.IdField = "SetBitwiseOptions";
            p.ValueField = currentRefTable["EXTENDED_OPTIONS"].ToString();
            if (Int32.TryParse(textDataExtendedOptions.Text, out x))
            {
                p.ValueInt32 = x;
            }
            else
            {
                p.ValueInt32 = 0;
            }
            dlg.GenericTransfer(p);
            dlg.Show();
        }

        public void GenericTransfer(GenericParam g)
        {
            if (g.IdField == "SetBitwiseOptions")
            {
                textDataExtendedOptions.Text = g.ValueInt32.ToString("0");
            }
        }

        private void pbSave_Click(object sender, EventArgs e)
        {
            if (textDataRefId.Text.Length == 0)
            {
                MessageBox.Show(this, "You need to provide an ID for this record.", "Rubbish", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }
            else
            {
                SaveRefDataRow(1);
            }
        }

        private void SaveRefDataRow(int nSeq)
        {
            MFServices.ReferenceData rd = new MFServices.ReferenceData();
            MFServices.ReferenceDataOperation cOp = new MFServices.ReferenceDataOperation();
            MFServices.Transaction t = new MFServices.Transaction();
            List<string> ops;
            List<MFServices.ReferenceDataOperation> cOps;

            fnPopulateTDClassFromScreen();

            try
            {
                if (bNewRow)
                    { cOp.Verb = MFServices.OperationVerb.Create; }
                else
                    { cOp.Verb = MFServices.OperationVerb.Update; }
                t.TransactionId = nSeq.ToString();
                cOp.OperationId = "ReferenceData-" + nSeq.ToString("00000");
                cOp.Sequence = nSeq;
                if (t.OperationSequence == null)
                    { ops = new List<string>(); }
                else
                    { ops = t.OperationSequence.ToList(); }
                ops.Add(cOp.OperationId);
                t.OperationSequence = ops.ToArray();
                cOp.OperationEntity = MFServices.OperationEntity.ReferenceData;
                cOp.DataMember = cRD;
                if (t.ReferenceDataOperations == null)
                    { cOps = new List<MFServices.ReferenceDataOperation>(); }
                else
                    { cOps = t.ReferenceDataOperations.ToList(); }
                cOps.Add(cOp);
                t.ReferenceDataOperations = cOps.ToArray();

                MFServices.TransactionResponse td = new MFServices.TransactionResponse();
                MFServicesSoapClient r = new MFServicesSoapClient();
                td = r.ProcessTransaction(AppHost.WebSessionKey, t);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Save Error", MessageBoxButtons.OK);
            }

            RefreshRefTableData();
            ClearDataMaintData();
        }

        private void pbCancel_Click(object sender, EventArgs e)
        {
            currentRefData = null;
            ClearDataMaintData();
            tabLayers.SelectedTab = tabLayers.TabPages["TabRefData"];
        }

    }
}

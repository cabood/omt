﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UbiSqlFramework;

namespace OMT.Forms
{
    public partial class dlgSearch : Form, ISearchHost
    {
        private string sSql;
        private SearchParam sSearch;
        private DataSet cData;
        private DataTable dtUnfiltered;
        
        public dlgSearch()
        {
            InitializeComponent();
        }

        public void SearchTransfer(SearchParam sParam)
        {
            sSearch = sParam;
            ExecuteSearch();
        }
        private void ExecuteSearch()
        {
            cData = new DataSet();
            sSql = sSearch.Sql;
            bool b = SqlHost.GetDataSet(sSql, "TestTable", ref cData);
            dtUnfiltered = cData.Tables["TestTable"];
        }

        private void pbCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private string GetColumnHeading(string sColName)
        {
            string sColHeading;
            sColHeading = sColName.Replace("_", " ");
            return sColHeading;
        }

        private void dgvSearch_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (dgvSearch.SelectedRows.Count > 0)
                ReturnSearch((((DataRowView)dgvSearch.SelectedRows[0].DataBoundItem).Row));
        }

        private void ReturnSearch(DataRow row)
        {
            sSearch.ReturnValue = new string[sSearch.ReturnKey.Length];
            for (int n = 0; n < sSearch.ReturnKey.Length; n++)
            {
                sSearch.ReturnValue[n] = row[sSearch.ReturnKey[n]].ToString();
            }
            ((ISearchHost)sSearch.SearchPartner).SearchTransfer(sSearch);
            this.Close();
        }

        private void pbRefine_Click(object sender, EventArgs e)
        {
            string sFilter = textBoxRefine.Text;
            if (textBoxRefine.Text.Length == 0) sFilter = "";
            RefineView(sFilter);
            this.ActiveControl = dgvSearch;
        }

        private void RefineView(string sFilter)
        {
            string sDtFilter = "";
            if (sFilter.Length == 0)
            {
                sDtFilter = "";
            }
            else
            {
                sFilter = sFilter.Replace("'", "");
                foreach (DataColumn col in dtUnfiltered.Columns)
                {
                    if (col.DataType == typeof(System.String))
                    {
                        if (sDtFilter.Length > 0)
                        {
                            sDtFilter = sDtFilter + " or ";
                        }
                        sDtFilter = sDtFilter + col.ColumnName + " like '%" + sFilter + "%'";
                    }
                }
            }
            DataView dv = new DataView(dtUnfiltered);
            dv.RowFilter = sDtFilter;
            dgvSearch.DataSource = null;
            dgvSearch.DataSource = dv;
            foreach (DataGridViewColumn col in dgvSearch.Columns)
            {
                col.HeaderText = GetColumnHeading(col.Name);
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }
            dgvSearch.Refresh();
            if (dgvSearch.RowCount == 1)
            {
                ReturnSearch((((DataRowView)dgvSearch.Rows[0].DataBoundItem).Row));
            }
        }

        private void dgvSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ReturnSearch((((DataRowView)dgvSearch.Rows[dgvSearch.CurrentCell.RowIndex].DataBoundItem).Row));
            }
        }

        private void dlgSearch_Activated(object sender, EventArgs e)
        {
            this.ActiveControl = textBoxRefine;
        }

        private void dgvSearch_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvSearch.SelectedCells.Count > 0)
                ReturnSearch((((DataRowView)((DataGridViewRow)dgvSearch.SelectedCells[0].OwningRow).DataBoundItem).Row));
        }

        private void textBoxRefine_Leave(object sender, EventArgs e)
        {
            string sFilter = textBoxRefine.Text;
            if (textBoxRefine.Text.Length == 0) sFilter = "";
            RefineView(sFilter);
            this.ActiveControl = dgvSearch;
        }

    }
}

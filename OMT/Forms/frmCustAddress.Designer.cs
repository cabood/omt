﻿namespace OMT.Forms
{
    partial class frmCustAddress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustAddress));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.tlpActions = new System.Windows.Forms.TableLayoutPanel();
            this.pbSave = new System.Windows.Forms.Button();
            this.imageListActions = new System.Windows.Forms.ImageList(this.components);
            this.pbCancel = new System.Windows.Forms.Button();
            this.pbDeleteCurrent = new System.Windows.Forms.Button();
            this.lblActions = new System.Windows.Forms.Label();
            this.tlpHeader = new System.Windows.Forms.TableLayoutPanel();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.textCustName = new System.Windows.Forms.TextBox();
            this.textCustId = new System.Windows.Forms.TextBox();
            this.tlpDataGrid = new System.Windows.Forms.TableLayoutPanel();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.pbAdd = new System.Windows.Forms.Button();
            this.pbDelete = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tlpAddress = new System.Windows.Forms.TableLayoutPanel();
            this.textAddrLabel = new System.Windows.Forms.TextBox();
            this.lblDCAddressLabel = new System.Windows.Forms.Label();
            this.lblStreet = new System.Windows.Forms.Label();
            this.lblSuburbCity = new System.Windows.Forms.Label();
            this.lblAddress3 = new System.Windows.Forms.Label();
            this.textAddr1 = new System.Windows.Forms.TextBox();
            this.textAddr2 = new System.Windows.Forms.TextBox();
            this.textAddr3 = new System.Windows.Forms.TextBox();
            this.lblState = new System.Windows.Forms.Label();
            this.lblPostCode = new System.Windows.Forms.Label();
            this.lblCountry = new System.Windows.Forms.Label();
            this.textState = new System.Windows.Forms.TextBox();
            this.textPostCode = new System.Windows.Forms.TextBox();
            this.cmbCountry = new System.Windows.Forms.ComboBox();
            this.lblAddressID = new System.Windows.Forms.Label();
            this.tlpAddressId = new System.Windows.Forms.TableLayoutPanel();
            this.textAddressId = new System.Windows.Forms.TextBox();
            this.lblEmailAddress = new System.Windows.Forms.Label();
            this.textEmail = new System.Windows.Forms.TextBox();
            this.textFax = new System.Windows.Forms.TextBox();
            this.textPhone = new System.Windows.Forms.TextBox();
            this.lblFaxNumber = new System.Windows.Forms.Label();
            this.lblPhoneNumber = new System.Windows.Forms.Label();
            this.lblAddressType = new System.Windows.Forms.Label();
            this.cmbAddressType = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pbSetPrimaryDel = new System.Windows.Forms.Button();
            this.cbIsDefaultDel = new System.Windows.Forms.CheckBox();
            this.tlpDefaultDelAddr = new System.Windows.Forms.TableLayoutPanel();
            this.pbSetOffice = new System.Windows.Forms.Button();
            this.cbIsMain = new System.Windows.Forms.CheckBox();
            this.tlpAddressActions = new System.Windows.Forms.TableLayoutPanel();
            this.cbIsDel = new System.Windows.Forms.CheckBox();
            this.cbIs3PL = new System.Windows.Forms.CheckBox();
            this.lblNotes = new System.Windows.Forms.Label();
            this.textNotes = new System.Windows.Forms.TextBox();
            this.tlpMain.SuspendLayout();
            this.tlpActions.SuspendLayout();
            this.tlpHeader.SuspendLayout();
            this.tlpDataGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tlpAddress.SuspendLayout();
            this.tlpAddressId.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tlpDefaultDelAddr.SuspendLayout();
            this.tlpAddressActions.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 3;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpMain.Controls.Add(this.tlpActions, 1, 4);
            this.tlpMain.Controls.Add(this.tlpHeader, 1, 1);
            this.tlpMain.Controls.Add(this.tlpDataGrid, 1, 2);
            this.tlpMain.Controls.Add(this.groupBox1, 1, 3);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 6;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 385F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpMain.Size = new System.Drawing.Size(1371, 794);
            this.tlpMain.TabIndex = 0;
            // 
            // tlpActions
            // 
            this.tlpActions.ColumnCount = 6;
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpActions.Controls.Add(this.pbSave, 2, 0);
            this.tlpActions.Controls.Add(this.pbCancel, 3, 0);
            this.tlpActions.Controls.Add(this.pbDeleteCurrent, 4, 0);
            this.tlpActions.Controls.Add(this.lblActions, 1, 0);
            this.tlpActions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpActions.Location = new System.Drawing.Point(12, 736);
            this.tlpActions.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tlpActions.Name = "tlpActions";
            this.tlpActions.RowCount = 1;
            this.tlpActions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpActions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tlpActions.Size = new System.Drawing.Size(1347, 45);
            this.tlpActions.TabIndex = 27;
            // 
            // pbSave
            // 
            this.pbSave.ImageKey = "Add32.png";
            this.pbSave.ImageList = this.imageListActions;
            this.pbSave.Location = new System.Drawing.Point(871, 5);
            this.pbSave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbSave.Name = "pbSave";
            this.pbSave.Size = new System.Drawing.Size(112, 35);
            this.pbSave.TabIndex = 27;
            this.pbSave.Text = "&Save";
            this.pbSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbSave.UseVisualStyleBackColor = true;
            this.pbSave.Click += new System.EventHandler(this.pbSave_Click);
            // 
            // imageListActions
            // 
            this.imageListActions.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListActions.ImageStream")));
            this.imageListActions.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListActions.Images.SetKeyName(0, "Add32.png");
            this.imageListActions.Images.SetKeyName(1, "Cancel32.png");
            this.imageListActions.Images.SetKeyName(2, "Delete32.png");
            this.imageListActions.Images.SetKeyName(3, "Find32.png");
            this.imageListActions.Images.SetKeyName(4, "Address32.png");
            this.imageListActions.Images.SetKeyName(5, "Contact32.png");
            // 
            // pbCancel
            // 
            this.pbCancel.ImageKey = "Cancel32.png";
            this.pbCancel.ImageList = this.imageListActions;
            this.pbCancel.Location = new System.Drawing.Point(1021, 5);
            this.pbCancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbCancel.Name = "pbCancel";
            this.pbCancel.Size = new System.Drawing.Size(112, 35);
            this.pbCancel.TabIndex = 28;
            this.pbCancel.Text = "&Cancel";
            this.pbCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbCancel.UseVisualStyleBackColor = true;
            this.pbCancel.Click += new System.EventHandler(this.pbCancel_Click);
            // 
            // pbDeleteCurrent
            // 
            this.pbDeleteCurrent.ImageKey = "Delete32.png";
            this.pbDeleteCurrent.ImageList = this.imageListActions;
            this.pbDeleteCurrent.Location = new System.Drawing.Point(1171, 5);
            this.pbDeleteCurrent.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbDeleteCurrent.Name = "pbDeleteCurrent";
            this.pbDeleteCurrent.Size = new System.Drawing.Size(112, 35);
            this.pbDeleteCurrent.TabIndex = 29;
            this.pbDeleteCurrent.Text = "&Delete";
            this.pbDeleteCurrent.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbDeleteCurrent.UseVisualStyleBackColor = true;
            this.pbDeleteCurrent.Click += new System.EventHandler(this.pbDeleteCurrent_Click);
            // 
            // lblActions
            // 
            this.lblActions.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblActions.AutoSize = true;
            this.lblActions.Location = new System.Drawing.Point(797, 12);
            this.lblActions.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblActions.Name = "lblActions";
            this.lblActions.Size = new System.Drawing.Size(66, 20);
            this.lblActions.TabIndex = 0;
            this.lblActions.Text = "Actions:";
            // 
            // tlpHeader
            // 
            this.tlpHeader.ColumnCount = 4;
            this.tlpHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tlpHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 225F));
            this.tlpHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 525F));
            this.tlpHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpHeader.Controls.Add(this.lblCustomer, 0, 1);
            this.tlpHeader.Controls.Add(this.textCustName, 2, 1);
            this.tlpHeader.Controls.Add(this.textCustId, 1, 1);
            this.tlpHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpHeader.Location = new System.Drawing.Point(8, 8);
            this.tlpHeader.Margin = new System.Windows.Forms.Padding(0);
            this.tlpHeader.Name = "tlpHeader";
            this.tlpHeader.RowCount = 3;
            this.tlpHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpHeader.Size = new System.Drawing.Size(1355, 62);
            this.tlpHeader.TabIndex = 0;
            // 
            // lblCustomer
            // 
            this.lblCustomer.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.Location = new System.Drawing.Point(94, 21);
            this.lblCustomer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(82, 20);
            this.lblCustomer.TabIndex = 0;
            this.lblCustomer.Text = "Customer:";
            // 
            // textCustName
            // 
            this.textCustName.Dock = System.Windows.Forms.DockStyle.Top;
            this.textCustName.Location = new System.Drawing.Point(409, 16);
            this.textCustName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textCustName.Name = "textCustName";
            this.textCustName.ReadOnly = true;
            this.textCustName.Size = new System.Drawing.Size(517, 26);
            this.textCustName.TabIndex = 2;
            // 
            // textCustId
            // 
            this.textCustId.Dock = System.Windows.Forms.DockStyle.Top;
            this.textCustId.Location = new System.Drawing.Point(184, 16);
            this.textCustId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textCustId.Name = "textCustId";
            this.textCustId.ReadOnly = true;
            this.textCustId.Size = new System.Drawing.Size(217, 26);
            this.textCustId.TabIndex = 3;
            // 
            // tlpDataGrid
            // 
            this.tlpDataGrid.ColumnCount = 2;
            this.tlpDataGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDataGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tlpDataGrid.Controls.Add(this.dgvData, 0, 0);
            this.tlpDataGrid.Controls.Add(this.pbAdd, 1, 1);
            this.tlpDataGrid.Controls.Add(this.pbDelete, 1, 2);
            this.tlpDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpDataGrid.Location = new System.Drawing.Point(12, 75);
            this.tlpDataGrid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tlpDataGrid.Name = "tlpDataGrid";
            this.tlpDataGrid.RowCount = 4;
            this.tlpDataGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDataGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tlpDataGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tlpDataGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDataGrid.Size = new System.Drawing.Size(1347, 266);
            this.tlpDataGrid.TabIndex = 1;
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToAddRows = false;
            this.dgvData.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvData.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvData.Location = new System.Drawing.Point(4, 5);
            this.dgvData.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvData.Name = "dgvData";
            this.dgvData.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvData.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvData.RowHeadersWidth = 62;
            this.tlpDataGrid.SetRowSpan(this.dgvData, 4);
            this.dgvData.Size = new System.Drawing.Size(1285, 256);
            this.dgvData.TabIndex = 0;
            this.dgvData.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvData_CellContentDoubleClick);
            this.dgvData.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvData_CellDoubleClick);
            this.dgvData.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvData_RowHeaderMouseDoubleClick);
            // 
            // pbAdd
            // 
            this.pbAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbAdd.ImageKey = "Add32.png";
            this.pbAdd.ImageList = this.imageListActions;
            this.pbAdd.Location = new System.Drawing.Point(1297, 83);
            this.pbAdd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbAdd.Name = "pbAdd";
            this.pbAdd.Size = new System.Drawing.Size(46, 45);
            this.pbAdd.TabIndex = 1;
            this.pbAdd.UseVisualStyleBackColor = true;
            this.pbAdd.Click += new System.EventHandler(this.pbAdd_Click);
            // 
            // pbDelete
            // 
            this.pbDelete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbDelete.ImageKey = "Delete32.png";
            this.pbDelete.ImageList = this.imageListActions;
            this.pbDelete.Location = new System.Drawing.Point(1297, 138);
            this.pbDelete.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbDelete.Name = "pbDelete";
            this.pbDelete.Size = new System.Drawing.Size(46, 45);
            this.pbDelete.TabIndex = 2;
            this.pbDelete.UseVisualStyleBackColor = true;
            this.pbDelete.Click += new System.EventHandler(this.pbDelete_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tlpAddress);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(12, 351);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(1347, 375);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Address Information";
            // 
            // tlpAddress
            // 
            this.tlpAddress.ColumnCount = 4;
            this.tlpAddress.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 128F));
            this.tlpAddress.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpAddress.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 162F));
            this.tlpAddress.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpAddress.Controls.Add(this.textAddrLabel, 3, 1);
            this.tlpAddress.Controls.Add(this.lblDCAddressLabel, 2, 1);
            this.tlpAddress.Controls.Add(this.lblStreet, 0, 2);
            this.tlpAddress.Controls.Add(this.lblSuburbCity, 0, 3);
            this.tlpAddress.Controls.Add(this.lblAddress3, 0, 4);
            this.tlpAddress.Controls.Add(this.textAddr1, 1, 2);
            this.tlpAddress.Controls.Add(this.textAddr2, 1, 3);
            this.tlpAddress.Controls.Add(this.textAddr3, 1, 4);
            this.tlpAddress.Controls.Add(this.lblState, 0, 5);
            this.tlpAddress.Controls.Add(this.lblPostCode, 0, 6);
            this.tlpAddress.Controls.Add(this.lblCountry, 0, 7);
            this.tlpAddress.Controls.Add(this.textState, 1, 5);
            this.tlpAddress.Controls.Add(this.textPostCode, 1, 6);
            this.tlpAddress.Controls.Add(this.cmbCountry, 1, 7);
            this.tlpAddress.Controls.Add(this.lblAddressID, 0, 1);
            this.tlpAddress.Controls.Add(this.tlpAddressId, 1, 1);
            this.tlpAddress.Controls.Add(this.lblEmailAddress, 2, 5);
            this.tlpAddress.Controls.Add(this.textEmail, 3, 5);
            this.tlpAddress.Controls.Add(this.textFax, 3, 4);
            this.tlpAddress.Controls.Add(this.textPhone, 3, 3);
            this.tlpAddress.Controls.Add(this.lblFaxNumber, 2, 4);
            this.tlpAddress.Controls.Add(this.lblPhoneNumber, 2, 3);
            this.tlpAddress.Controls.Add(this.lblAddressType, 2, 2);
            this.tlpAddress.Controls.Add(this.cmbAddressType, 3, 2);
            this.tlpAddress.Controls.Add(this.tableLayoutPanel1, 2, 8);
            this.tlpAddress.Controls.Add(this.tlpDefaultDelAddr, 2, 7);
            this.tlpAddress.Controls.Add(this.tlpAddressActions, 2, 6);
            this.tlpAddress.Controls.Add(this.lblNotes, 0, 8);
            this.tlpAddress.Controls.Add(this.textNotes, 1, 8);
            this.tlpAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpAddress.Location = new System.Drawing.Point(4, 24);
            this.tlpAddress.Margin = new System.Windows.Forms.Padding(0);
            this.tlpAddress.Name = "tlpAddress";
            this.tlpAddress.RowCount = 10;
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAddress.Size = new System.Drawing.Size(1339, 346);
            this.tlpAddress.TabIndex = 11;
            // 
            // textAddrLabel
            // 
            this.textAddrLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.textAddrLabel.Location = new System.Drawing.Point(818, 13);
            this.textAddrLabel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textAddrLabel.Name = "textAddrLabel";
            this.textAddrLabel.Size = new System.Drawing.Size(517, 26);
            this.textAddrLabel.TabIndex = 17;
            // 
            // lblDCAddressLabel
            // 
            this.lblDCAddressLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDCAddressLabel.AutoSize = true;
            this.lblDCAddressLabel.Location = new System.Drawing.Point(656, 18);
            this.lblDCAddressLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDCAddressLabel.Name = "lblDCAddressLabel";
            this.lblDCAddressLabel.Size = new System.Drawing.Size(150, 20);
            this.lblDCAddressLabel.TabIndex = 27;
            this.lblDCAddressLabel.Text = "DC / Address Label:";
            // 
            // lblStreet
            // 
            this.lblStreet.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStreet.AutoSize = true;
            this.lblStreet.Location = new System.Drawing.Point(4, 58);
            this.lblStreet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStreet.Name = "lblStreet";
            this.lblStreet.Size = new System.Drawing.Size(57, 20);
            this.lblStreet.TabIndex = 0;
            this.lblStreet.Text = "Street:";
            // 
            // lblSuburbCity
            // 
            this.lblSuburbCity.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSuburbCity.AutoSize = true;
            this.lblSuburbCity.Location = new System.Drawing.Point(4, 98);
            this.lblSuburbCity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSuburbCity.Name = "lblSuburbCity";
            this.lblSuburbCity.Size = new System.Drawing.Size(103, 20);
            this.lblSuburbCity.TabIndex = 1;
            this.lblSuburbCity.Text = "Suburb / City:";
            // 
            // lblAddress3
            // 
            this.lblAddress3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblAddress3.AutoSize = true;
            this.lblAddress3.Location = new System.Drawing.Point(4, 138);
            this.lblAddress3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddress3.Name = "lblAddress3";
            this.lblAddress3.Size = new System.Drawing.Size(85, 20);
            this.lblAddress3.TabIndex = 2;
            this.lblAddress3.Text = "Address 3:";
            // 
            // textAddr1
            // 
            this.textAddr1.Dock = System.Windows.Forms.DockStyle.Top;
            this.textAddr1.Location = new System.Drawing.Point(132, 53);
            this.textAddr1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textAddr1.Name = "textAddr1";
            this.textAddr1.Size = new System.Drawing.Size(516, 26);
            this.textAddr1.TabIndex = 11;
            // 
            // textAddr2
            // 
            this.textAddr2.Dock = System.Windows.Forms.DockStyle.Top;
            this.textAddr2.Location = new System.Drawing.Point(132, 93);
            this.textAddr2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textAddr2.Name = "textAddr2";
            this.textAddr2.Size = new System.Drawing.Size(516, 26);
            this.textAddr2.TabIndex = 12;
            // 
            // textAddr3
            // 
            this.textAddr3.Dock = System.Windows.Forms.DockStyle.Top;
            this.textAddr3.Location = new System.Drawing.Point(132, 133);
            this.textAddr3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textAddr3.Name = "textAddr3";
            this.textAddr3.Size = new System.Drawing.Size(516, 26);
            this.textAddr3.TabIndex = 13;
            // 
            // lblState
            // 
            this.lblState.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(4, 178);
            this.lblState.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(52, 20);
            this.lblState.TabIndex = 6;
            this.lblState.Text = "State:";
            // 
            // lblPostCode
            // 
            this.lblPostCode.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblPostCode.AutoSize = true;
            this.lblPostCode.Location = new System.Drawing.Point(4, 218);
            this.lblPostCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPostCode.Name = "lblPostCode";
            this.lblPostCode.Size = new System.Drawing.Size(87, 20);
            this.lblPostCode.TabIndex = 7;
            this.lblPostCode.Text = "Post Code:";
            // 
            // lblCountry
            // 
            this.lblCountry.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblCountry.AutoSize = true;
            this.lblCountry.Location = new System.Drawing.Point(4, 262);
            this.lblCountry.Margin = new System.Windows.Forms.Padding(4, 9, 4, 0);
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Size = new System.Drawing.Size(68, 20);
            this.lblCountry.TabIndex = 8;
            this.lblCountry.Text = "Country:";
            // 
            // textState
            // 
            this.textState.Location = new System.Drawing.Point(132, 173);
            this.textState.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textState.Name = "textState";
            this.textState.Size = new System.Drawing.Size(252, 26);
            this.textState.TabIndex = 14;
            // 
            // textPostCode
            // 
            this.textPostCode.Location = new System.Drawing.Point(132, 213);
            this.textPostCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textPostCode.Name = "textPostCode";
            this.textPostCode.Size = new System.Drawing.Size(214, 26);
            this.textPostCode.TabIndex = 15;
            // 
            // cmbCountry
            // 
            this.cmbCountry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbCountry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCountry.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCountry.FormattingEnabled = true;
            this.cmbCountry.Location = new System.Drawing.Point(132, 253);
            this.cmbCountry.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbCountry.Name = "cmbCountry";
            this.cmbCountry.Size = new System.Drawing.Size(516, 28);
            this.cmbCountry.TabIndex = 16;
            // 
            // lblAddressID
            // 
            this.lblAddressID.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblAddressID.AutoSize = true;
            this.lblAddressID.Location = new System.Drawing.Point(4, 18);
            this.lblAddressID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddressID.Name = "lblAddressID";
            this.lblAddressID.Size = new System.Drawing.Size(93, 20);
            this.lblAddressID.TabIndex = 21;
            this.lblAddressID.Text = "Address ID:";
            // 
            // tlpAddressId
            // 
            this.tlpAddressId.ColumnCount = 2;
            this.tlpAddressId.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 225F));
            this.tlpAddressId.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAddressId.Controls.Add(this.textAddressId, 0, 0);
            this.tlpAddressId.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpAddressId.Location = new System.Drawing.Point(128, 8);
            this.tlpAddressId.Margin = new System.Windows.Forms.Padding(0);
            this.tlpAddressId.Name = "tlpAddressId";
            this.tlpAddressId.RowCount = 1;
            this.tlpAddressId.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAddressId.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddressId.Size = new System.Drawing.Size(524, 40);
            this.tlpAddressId.TabIndex = 23;
            // 
            // textAddressId
            // 
            this.textAddressId.Dock = System.Windows.Forms.DockStyle.Top;
            this.textAddressId.Location = new System.Drawing.Point(4, 5);
            this.textAddressId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textAddressId.Name = "textAddressId";
            this.textAddressId.ReadOnly = true;
            this.textAddressId.Size = new System.Drawing.Size(217, 26);
            this.textAddressId.TabIndex = 22;
            // 
            // lblEmailAddress
            // 
            this.lblEmailAddress.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblEmailAddress.AutoSize = true;
            this.lblEmailAddress.Location = new System.Drawing.Point(656, 178);
            this.lblEmailAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmailAddress.Name = "lblEmailAddress";
            this.lblEmailAddress.Size = new System.Drawing.Size(115, 20);
            this.lblEmailAddress.TabIndex = 14;
            this.lblEmailAddress.Text = "Email Address:";
            // 
            // textEmail
            // 
            this.textEmail.Dock = System.Windows.Forms.DockStyle.Top;
            this.textEmail.Location = new System.Drawing.Point(818, 173);
            this.textEmail.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textEmail.Name = "textEmail";
            this.textEmail.Size = new System.Drawing.Size(517, 26);
            this.textEmail.TabIndex = 21;
            // 
            // textFax
            // 
            this.textFax.Dock = System.Windows.Forms.DockStyle.Top;
            this.textFax.Location = new System.Drawing.Point(818, 133);
            this.textFax.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textFax.Name = "textFax";
            this.textFax.Size = new System.Drawing.Size(517, 26);
            this.textFax.TabIndex = 20;
            // 
            // textPhone
            // 
            this.textPhone.Dock = System.Windows.Forms.DockStyle.Top;
            this.textPhone.Location = new System.Drawing.Point(818, 93);
            this.textPhone.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textPhone.Name = "textPhone";
            this.textPhone.Size = new System.Drawing.Size(517, 26);
            this.textPhone.TabIndex = 19;
            // 
            // lblFaxNumber
            // 
            this.lblFaxNumber.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblFaxNumber.AutoSize = true;
            this.lblFaxNumber.Location = new System.Drawing.Point(656, 138);
            this.lblFaxNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFaxNumber.Name = "lblFaxNumber";
            this.lblFaxNumber.Size = new System.Drawing.Size(99, 20);
            this.lblFaxNumber.TabIndex = 13;
            this.lblFaxNumber.Text = "Fax Number:";
            // 
            // lblPhoneNumber
            // 
            this.lblPhoneNumber.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblPhoneNumber.AutoSize = true;
            this.lblPhoneNumber.Location = new System.Drawing.Point(656, 98);
            this.lblPhoneNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPhoneNumber.Name = "lblPhoneNumber";
            this.lblPhoneNumber.Size = new System.Drawing.Size(119, 20);
            this.lblPhoneNumber.TabIndex = 12;
            this.lblPhoneNumber.Text = "Phone Number:";
            // 
            // lblAddressType
            // 
            this.lblAddressType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblAddressType.AutoSize = true;
            this.lblAddressType.Location = new System.Drawing.Point(656, 58);
            this.lblAddressType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddressType.Name = "lblAddressType";
            this.lblAddressType.Size = new System.Drawing.Size(110, 20);
            this.lblAddressType.TabIndex = 26;
            this.lblAddressType.Text = "Address Type:";
            // 
            // cmbAddressType
            // 
            this.cmbAddressType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbAddressType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbAddressType.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbAddressType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAddressType.FormattingEnabled = true;
            this.cmbAddressType.Location = new System.Drawing.Point(818, 53);
            this.cmbAddressType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbAddressType.Name = "cmbAddressType";
            this.cmbAddressType.Size = new System.Drawing.Size(517, 28);
            this.cmbAddressType.TabIndex = 18;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tlpAddress.SetColumnSpan(this.tableLayoutPanel1, 2);
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tableLayoutPanel1.Controls.Add(this.pbSetPrimaryDel, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbIsDefaultDel, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(652, 288);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(687, 40);
            this.tableLayoutPanel1.TabIndex = 25;
            // 
            // pbSetPrimaryDel
            // 
            this.pbSetPrimaryDel.Location = new System.Drawing.Point(511, 5);
            this.pbSetPrimaryDel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbSetPrimaryDel.Name = "pbSetPrimaryDel";
            this.pbSetPrimaryDel.Size = new System.Drawing.Size(171, 30);
            this.pbSetPrimaryDel.TabIndex = 1;
            this.pbSetPrimaryDel.Text = "Set As Primary Del.";
            this.pbSetPrimaryDel.UseVisualStyleBackColor = true;
            this.pbSetPrimaryDel.Click += new System.EventHandler(this.pbSetPrimaryDel_Click);
            // 
            // cbIsDefaultDel
            // 
            this.cbIsDefaultDel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbIsDefaultDel.AutoSize = true;
            this.cbIsDefaultDel.Location = new System.Drawing.Point(79, 8);
            this.cbIsDefaultDel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbIsDefaultDel.Name = "cbIsDefaultDel";
            this.cbIsDefaultDel.Size = new System.Drawing.Size(281, 24);
            this.cbIsDefaultDel.TabIndex = 25;
            this.cbIsDefaultDel.Text = "This is the default Delivery Address";
            this.cbIsDefaultDel.UseVisualStyleBackColor = true;
            // 
            // tlpDefaultDelAddr
            // 
            this.tlpDefaultDelAddr.ColumnCount = 3;
            this.tlpAddress.SetColumnSpan(this.tlpDefaultDelAddr, 2);
            this.tlpDefaultDelAddr.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tlpDefaultDelAddr.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDefaultDelAddr.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tlpDefaultDelAddr.Controls.Add(this.pbSetOffice, 2, 0);
            this.tlpDefaultDelAddr.Controls.Add(this.cbIsMain, 1, 0);
            this.tlpDefaultDelAddr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpDefaultDelAddr.Location = new System.Drawing.Point(652, 248);
            this.tlpDefaultDelAddr.Margin = new System.Windows.Forms.Padding(0);
            this.tlpDefaultDelAddr.Name = "tlpDefaultDelAddr";
            this.tlpDefaultDelAddr.RowCount = 1;
            this.tlpDefaultDelAddr.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDefaultDelAddr.Size = new System.Drawing.Size(687, 40);
            this.tlpDefaultDelAddr.TabIndex = 24;
            // 
            // pbSetOffice
            // 
            this.pbSetOffice.Location = new System.Drawing.Point(511, 5);
            this.pbSetOffice.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbSetOffice.Name = "pbSetOffice";
            this.pbSetOffice.Size = new System.Drawing.Size(171, 30);
            this.pbSetOffice.TabIndex = 0;
            this.pbSetOffice.Text = "Set As Office Addr.";
            this.pbSetOffice.UseVisualStyleBackColor = true;
            this.pbSetOffice.Click += new System.EventHandler(this.pbSetOffice_Click);
            // 
            // cbIsMain
            // 
            this.cbIsMain.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbIsMain.AutoSize = true;
            this.cbIsMain.Location = new System.Drawing.Point(79, 8);
            this.cbIsMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbIsMain.Name = "cbIsMain";
            this.cbIsMain.Size = new System.Drawing.Size(215, 24);
            this.cbIsMain.TabIndex = 24;
            this.cbIsMain.Text = "This is the Office Address";
            this.cbIsMain.UseVisualStyleBackColor = true;
            // 
            // tlpAddressActions
            // 
            this.tlpAddressActions.ColumnCount = 3;
            this.tlpAddress.SetColumnSpan(this.tlpAddressActions, 2);
            this.tlpAddressActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tlpAddressActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpAddressActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpAddressActions.Controls.Add(this.cbIsDel, 1, 0);
            this.tlpAddressActions.Controls.Add(this.cbIs3PL, 2, 0);
            this.tlpAddressActions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpAddressActions.Location = new System.Drawing.Point(652, 208);
            this.tlpAddressActions.Margin = new System.Windows.Forms.Padding(0);
            this.tlpAddressActions.Name = "tlpAddressActions";
            this.tlpAddressActions.RowCount = 1;
            this.tlpAddressActions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAddressActions.Size = new System.Drawing.Size(687, 40);
            this.tlpAddressActions.TabIndex = 22;
            // 
            // cbIsDel
            // 
            this.cbIsDel.AutoSize = true;
            this.cbIsDel.Location = new System.Drawing.Point(79, 5);
            this.cbIsDel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbIsDel.Name = "cbIsDel";
            this.cbIsDel.Size = new System.Drawing.Size(170, 24);
            this.cbIsDel.TabIndex = 22;
            this.cbIsDel.Text = "Is Delivery Address";
            this.cbIsDel.UseVisualStyleBackColor = true;
            // 
            // cbIs3PL
            // 
            this.cbIs3PL.AutoSize = true;
            this.cbIs3PL.Location = new System.Drawing.Point(385, 5);
            this.cbIs3PL.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbIs3PL.Name = "cbIs3PL";
            this.cbIs3PL.Size = new System.Drawing.Size(156, 24);
            this.cbIs3PL.TabIndex = 23;
            this.cbIs3PL.Text = "Is a 3PL Address";
            this.cbIs3PL.UseVisualStyleBackColor = true;
            // 
            // lblNotes
            // 
            this.lblNotes.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblNotes.AutoSize = true;
            this.lblNotes.Location = new System.Drawing.Point(4, 298);
            this.lblNotes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNotes.Name = "lblNotes";
            this.lblNotes.Size = new System.Drawing.Size(55, 20);
            this.lblNotes.TabIndex = 28;
            this.lblNotes.Text = "Notes:";
            // 
            // textNotes
            // 
            this.textNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textNotes.Location = new System.Drawing.Point(132, 293);
            this.textNotes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textNotes.Multiline = true;
            this.textNotes.Name = "textNotes";
            this.textNotes.Size = new System.Drawing.Size(516, 30);
            this.textNotes.TabIndex = 26;
            // 
            // frmCustAddress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1371, 794);
            this.Controls.Add(this.tlpMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmCustAddress";
            this.Text = "Customer Addresses";
            this.Load += new System.EventHandler(this.frmCustAddress_Load);
            this.tlpMain.ResumeLayout(false);
            this.tlpActions.ResumeLayout(false);
            this.tlpActions.PerformLayout();
            this.tlpHeader.ResumeLayout(false);
            this.tlpHeader.PerformLayout();
            this.tlpDataGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.tlpAddress.ResumeLayout(false);
            this.tlpAddress.PerformLayout();
            this.tlpAddressId.ResumeLayout(false);
            this.tlpAddressId.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tlpDefaultDelAddr.ResumeLayout(false);
            this.tlpDefaultDelAddr.PerformLayout();
            this.tlpAddressActions.ResumeLayout(false);
            this.tlpAddressActions.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpHeader;
        private System.Windows.Forms.Label lblCustomer;
        private System.Windows.Forms.TextBox textCustName;
        private System.Windows.Forms.TableLayoutPanel tlpDataGrid;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tlpAddress;
        private System.Windows.Forms.Label lblStreet;
        private System.Windows.Forms.Label lblSuburbCity;
        private System.Windows.Forms.Label lblAddress3;
        private System.Windows.Forms.TextBox textAddr1;
        private System.Windows.Forms.TextBox textAddr2;
        private System.Windows.Forms.TextBox textAddr3;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.Label lblPostCode;
        private System.Windows.Forms.Label lblCountry;
        private System.Windows.Forms.TextBox textState;
        private System.Windows.Forms.TextBox textPostCode;
        private System.Windows.Forms.ComboBox cmbCountry;
        private System.Windows.Forms.Label lblPhoneNumber;
        private System.Windows.Forms.Label lblFaxNumber;
        private System.Windows.Forms.Label lblEmailAddress;
        private System.Windows.Forms.TableLayoutPanel tlpDefaultDelAddr;
        private System.Windows.Forms.CheckBox cbIsMain;
        private System.Windows.Forms.TextBox textPhone;
        private System.Windows.Forms.TextBox textFax;
        private System.Windows.Forms.TextBox textEmail;
        private System.Windows.Forms.Label lblAddressID;
        private System.Windows.Forms.TextBox textAddressId;
        private System.Windows.Forms.TableLayoutPanel tlpAddressId;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.CheckBox cbIsDefaultDel;
        private System.Windows.Forms.TextBox textCustId;
        private System.Windows.Forms.TableLayoutPanel tlpAddressActions;
        private System.Windows.Forms.Button pbSetOffice;
        private System.Windows.Forms.Button pbSetPrimaryDel;
        private System.Windows.Forms.Label lblAddressType;
        private System.Windows.Forms.Button pbAdd;
        private System.Windows.Forms.Button pbDelete;
        private System.Windows.Forms.ComboBox cmbAddressType;
        private System.Windows.Forms.ImageList imageListActions;
        private System.Windows.Forms.CheckBox cbIsDel;
        private System.Windows.Forms.CheckBox cbIs3PL;
        private System.Windows.Forms.TableLayoutPanel tlpActions;
        private System.Windows.Forms.Button pbSave;
        private System.Windows.Forms.Button pbCancel;
        private System.Windows.Forms.Button pbDeleteCurrent;
        private System.Windows.Forms.Label lblActions;
        private System.Windows.Forms.Label lblNotes;
        private System.Windows.Forms.TextBox textNotes;
        private System.Windows.Forms.TextBox textAddrLabel;
        private System.Windows.Forms.Label lblDCAddressLabel;
    }
}
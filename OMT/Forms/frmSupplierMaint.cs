﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using UbiSqlFramework;

namespace OMT.Forms
{
    public partial class frmSupplierMaint : Form, ISearchHost
    {
        bool bAutoLoad = false;
        bool bNewRow = false;
        bool bNewSuppContext = false;
        DataTable dtContextSupp;
        //DataTable dtContext;

        public frmSupplierMaint()
        {
            InitializeComponent();
        }

        private void frmSupplierMaint_Load(object sender, EventArgs e)
        {
            InitFormData();
            this.ActiveControl = textSupplierId;
        }

        private void InitFormData()
        {
            LoadCountries();
            LoadCurrencies();
            LoadPorts();
            LoadPaymentTerms();
            LoadExtraCurrencies();
            LoadLockCodes();
            LoadDefPOTypes();
            LoadEntityTypes();
            LoadContexts();
            LoadContextPaymentTerms();
            LoadContextPOTypes();
            LoadHeaders();
        }
        private void LoadCountries()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbCountry.DataSource = null;
                cmbCountry.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'COUNTRY'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "CountryData", ref refContext);
                cmbCountry.DataSource = refContext.Tables["CountryData"]; 
                cmbCountry.ValueMember = "REF_ID";
                cmbCountry.DisplayMember = "DESCRIPTION";
                cmbCountry.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Countries Error", MessageBoxButtons.OK);
            }
        }
        private void LoadCurrencies()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbDefCurrency.DataSource = null;
                cmbDefCurrency.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'CURRENCY' ORDER BY EXT_NUM_1 DESC, REF_ID";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "CurrencyData", ref refContext);
                cmbDefCurrency.DataSource = refContext.Tables["CurrencyData"];
                cmbDefCurrency.ValueMember = "REF_ID";
                cmbDefCurrency.DisplayMember = "DESCRIPTION";
                cmbDefCurrency.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Currencies Error", MessageBoxButtons.OK);
            }
        }

        private void LoadPorts()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbDefPort.DataSource = null;
                cmbDefPort.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'PORT'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "PortData", ref refContext);
                cmbDefPort.DataSource = refContext.Tables["PortData"];
                cmbDefPort.ValueMember = "REF_ID";
                cmbDefPort.DisplayMember = "DESCRIPTION";
                cmbDefPort.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Ports Error", MessageBoxButtons.OK);
            }
        }
        
        private void LoadPaymentTerms()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbPaymentTerms.DataSource = null;
                cmbPaymentTerms.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'SUPPPAYTERMS'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "PaymentData", ref refContext);
                cmbPaymentTerms.DataSource = refContext.Tables["PaymentData"];
                cmbPaymentTerms.ValueMember = "REF_ID";
                cmbPaymentTerms.DisplayMember = "DESCRIPTION";
                cmbPaymentTerms.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Payment Terms Error", MessageBoxButtons.OK);
            }
        }
        
        private void LoadExtraCurrencies()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbExtraCurrency.DataSource = null;
                cmbExtraCurrency.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'CURRENCY' ORDER BY EXT_NUM_1 DESC, REF_ID";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "CurrencyData", ref refContext);
                cmbExtraCurrency.DataSource = refContext.Tables["CurrencyData"];
                cmbExtraCurrency.ValueMember = "REF_ID";
                cmbExtraCurrency.DisplayMember = "DESCRIPTION";
                cmbExtraCurrency.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Extra Currencies Error", MessageBoxButtons.OK);
            }
        }
        private void LoadLockCodes()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbLockCode.DataSource = null;
                cmbLockCode.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'CBASUPPLOCK'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "LockCodes", ref refContext);
                cmbLockCode.DataSource = refContext.Tables["LockCodes"];
                cmbLockCode.ValueMember = "REF_ID";
                cmbLockCode.DisplayMember = "DESCRIPTION";
                cmbLockCode.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Lock Codes Error", MessageBoxButtons.OK);
            }
        }
        private void LoadDefPOTypes()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbDefPOType.DataSource = null;
                cmbDefPOType.Items.Clear();
                sSql = "select TEMPLATE_CODE, DESCRIPTION from DOC_TEMPLATE where TEMPLATE_TYPE = '3'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "POTypeData", ref refContext);
                cmbDefPOType.DataSource = refContext.Tables["POTypeData"];
                cmbDefPOType.ValueMember = "TEMPLATE_CODE";
                cmbDefPOType.DisplayMember = "DESCRIPTION";
                cmbDefPOType.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Default PO Types Error", MessageBoxButtons.OK);
            }
        }
        private void LoadContextPOTypes()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbContextPOType.DataSource = null;
                cmbContextPOType.Items.Clear();
                sSql = "select TEMPLATE_CODE, DESCRIPTION from DOC_TEMPLATE where TEMPLATE_TYPE = '3'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "POTypeData", ref refContext);
                cmbContextPOType.DataSource = refContext.Tables["POTypeData"];
                cmbContextPOType.ValueMember = "TEMPLATE_CODE";
                cmbContextPOType.DisplayMember = "DESCRIPTION";
                cmbContextPOType.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Context PO Types Error", MessageBoxButtons.OK);
            }
        }

        private void LoadContextPaymentTerms()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbContextPaymentTerms.DataSource = null;
                cmbContextPaymentTerms.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'SUPPPAYTERMS'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "PaymentData", ref refContext);
                cmbContextPaymentTerms.DataSource = refContext.Tables["PaymentData"];
                cmbContextPaymentTerms.ValueMember = "REF_ID";
                cmbContextPaymentTerms.DisplayMember = "DESCRIPTION";
                cmbContextPaymentTerms.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Context Payments Error", MessageBoxButtons.OK);
            }
        }
        private void LoadEntityTypes()
        {
            try
            {
                cmbEntityType.DataSource = AppHost.EntityType.Copy();
                cmbEntityType.DisplayMember = "Description";
                cmbEntityType.ValueMember = "ref_id";
                cmbEntityType.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Entity Types Error", MessageBoxButtons.OK);
            }
        }
        private void LoadContexts()
        {
            try
            {
                cmbContextId.DataSource = AppHost.Context.Copy();
                cmbContextId.DisplayMember = "NAME_1";
                cmbContextId.ValueMember = "CONTEXT_ID";
                cmbContextId.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Contexts Error", MessageBoxButtons.OK);
            }
        }

        private void LoadHeaders()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbContextDefInvHdrImg.DataSource = null;
                cmbContextDefInvHdrImg.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'HEADERFILE' order by DESCRIPTION";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "ImageHeaderData", ref refContext);
                cmbContextDefInvHdrImg.DataSource = refContext.Tables["ImageHeaderData"];
                cmbContextDefInvHdrImg.ValueMember = "REF_ID";
                cmbContextDefInvHdrImg.DisplayMember = "DESCRIPTION";
                cmbContextDefInvHdrImg.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Headers Error", MessageBoxButtons.OK);
            }
        }

        private void textSupplierId_Validating(object sender, CancelEventArgs e)
        {
            if (!bAutoLoad)
            {
                if (textSupplierId.Text != textSupplierId.Text.ToUpper())
                {
                    textSupplierId.Text = textSupplierId.Text.ToUpper();
                }
                RetrieveSupplierDetails(textSupplierId.Text);
                this.ActiveControl = textName1;
            }

        }

        private void textParentSuppId_Validating(object sender, CancelEventArgs e)
        {
            if (textParentSuppId.Text != textParentSuppId.Text.ToUpper())
            {
                textParentSuppId.Text = textParentSuppId.Text.ToUpper();
            }
            RetrieveParentSupplierDetails(textParentSuppId.Text);
        }

        private void RetrieveSupplierDetails(string sId)
        {
            try
            {
                ClearFormDetails(false);
                if (sId.Length == 0)
                {
                    return;
                }
                SqlCommand rCom = new SqlCommand("select * from SUPPLIER where SUPPLIER_ID = @SUPP_ID", SqlHost.DBConn);
                rCom.Parameters.AddWithValue("@SUPP_ID", textSupplierId.Text.ToUpper());

                SqlDataReader cReader = rCom.ExecuteReader();

                bNewRow = !cReader.HasRows;

                while (cReader.Read())
                {
                    if (cReader["NAME_1"] != System.DBNull.Value)
                        textName1.Text = cReader["NAME_1"].ToString();
                    if (cReader["NAME_2"] != System.DBNull.Value)
                        textName2.Text = cReader["NAME_2"].ToString();
                    if (cReader["DEF_CURRENCY"] != System.DBNull.Value && cReader["DEF_CURRENCY"].ToString().Length > 0)
                    {
                        cmbDefCurrency.SelectedValue = (string)cReader["DEF_CURRENCY"];
                    }
                    else
                    {
                        cmbDefCurrency.SelectedIndex = -1;
                    }
                    if (cReader["COUNTRY_CODE"] != System.DBNull.Value && cReader["COUNTRY_CODE"].ToString().Length > 0)
                    {
                        cmbCountry.SelectedValue = (string)cReader["COUNTRY_CODE"];
                    }
                    else
                    {
                        cmbCountry.SelectedIndex = -1;
                    }
                    if (cReader["DEF_PORT"] != System.DBNull.Value && cReader["DEF_PORT"].ToString().Length > 0)
                    {
                        cmbDefPort.SelectedValue = (string)cReader["DEF_PORT"];
                    }
                    else
                    {
                        cmbDefPort.SelectedIndex = -1;
                    }
                    if (cReader["DEF_PAY_TERMS"] != System.DBNull.Value && cReader["DEF_PAY_TERMS"].ToString().Length > 0)
                    {
                        cmbPaymentTerms.SelectedValue = (string)cReader["DEF_PAY_TERMS"];
                    }
                    else
                    {
                        cmbPaymentTerms.SelectedIndex = -1;
                    }
                    cbActive.Checked = (bool)cReader["ACTIVE"];
                    if (cReader["NON_ACCOUNT"] != System.DBNull.Value)
                    {
                        cbNonAccount.Checked = (bool)cReader["NON_ACCOUNT"];
                    }
                    else
                    {
                        cbNonAccount.Checked = false;
                    }
                    if (cReader["PARENT_SUPP"] != System.DBNull.Value)
                    {
                        textParentSuppId.Text = cReader["PARENT_SUPP"].ToString();
                    }
                    if (cReader["NOTES"] != System.DBNull.Value)
                        textNotes.Text = cReader["NOTES"].ToString();
                    if (cReader["REBATE_NOTES"] != System.DBNull.Value)
                        textRebateNotes.Text = cReader["REBATE_NOTES"].ToString();
                    if (cReader["CBA_TERMS"] != System.DBNull.Value)
                        textCBATerms.Text = cReader["CBA_TERMS"].ToString();
                    if (cReader["CBA_SUPPTYPE"] != System.DBNull.Value)
                        textCBASuppType.Text = cReader["CBA_SUPPTYPE"].ToString();
                    if (cReader["CONTACT"] != System.DBNull.Value)
                        textContact.Text = cReader["CONTACT"].ToString();
                    if (cReader["LOCK_CODE"] != System.DBNull.Value && cReader["LOCK_CODE"].ToString().Length > 0)
                    {
                        cmbLockCode.SelectedValue = (string)cReader["LOCK_CODE"];
                    }
                    else
                    {
                        cmbLockCode.SelectedIndex = -1;
                    }
                    if (cReader["EXTRA_CURRENCY"] != System.DBNull.Value && cReader["EXTRA_CURRENCY"].ToString().Length > 0)
                    {
                        cmbExtraCurrency.SelectedValue = (string)cReader["EXTRA_CURRENCY"];
                    }
                    else
                    {
                        cmbExtraCurrency.SelectedIndex = -1;
                    }
                    if (cReader["DEF_PO_TYPE"] != System.DBNull.Value && cReader["DEF_PO_TYPE"].ToString().Length > 0)
                    {
                        cmbDefPOType.SelectedValue = (string)cReader["DEF_PO_TYPE"];
                    }
                    else
                    {
                        cmbDefPOType.SelectedIndex = -1;
                    }
                    if (cReader["ADDR_1"] != System.DBNull.Value)
                        textAddr1.Text = cReader["ADDR_1"].ToString();
                    if (cReader["ADDR_2"] != System.DBNull.Value)
                        textAddr2.Text = cReader["ADDR_2"].ToString();
                    if (cReader["ADDR_3"] != System.DBNull.Value)
                        textAddr3.Text = cReader["ADDR_3"].ToString();
                    if (cReader["STATE"] != System.DBNull.Value)
                        textAddrState.Text = cReader["STATE"].ToString();
                    if (cReader["POSTCODE"] != System.DBNull.Value)
                        textPostCode.Text = cReader["POSTCODE"].ToString();
                    if (cReader["PHONE"] != System.DBNull.Value)
                        textPhone.Text = cReader["PHONE"].ToString();
                    if (cReader["FAX"] != System.DBNull.Value)
                        textFax.Text = cReader["FAX"].ToString();
                    if (cReader["MOBILE"] != System.DBNull.Value)
                        textMobile.Text = cReader["MOBILE"].ToString();
                    if (cReader["EMAIL_ADDRESS"] != System.DBNull.Value)
                        textEmailAddress.Text = cReader["EMAIL_ADDRESS"].ToString();
                    if (cReader["ABN_NO"] != System.DBNull.Value)
                        textABN.Text = cReader["ABN_NO"].ToString();
                    if (cReader["ENTITY_TYPE"] != System.DBNull.Value && cReader["ENTITY_TYPE"].ToString().Length > 0)
                    {
                        cmbEntityType.SelectedValue = (string)cReader["ENTITY_TYPE"];
                    }
                    else
                    {
                        cmbEntityType.SelectedIndex = -1;
                    }

                }
                cReader.Close();

                if (bNewRow)
                {
                    cbActive.Checked = true;
                    textCBASuppType.Text = Constants.NEWSUPPLIER_SUPPTYPE;
                    cmbDefPOType.SelectedValue = Constants.NEWSUPPLIER_POTYPE;
                    cmbLockCode.SelectedValue = Constants.NEWSUPPLIER_LOCKCODE;
                    textCBATerms.Text = Constants.NEWSUPPLIER_CBATERMS;
                    cmbEntityType.SelectedValue = Constants.NEWSUPPLIER_ENTITYTYPE;
                }
                else
                {
                    RetrieveParentSupplierDetails(textParentSuppId.Text);
                    // Refresh Context Assignments
                    RefreshContextData(sId);
                }
            }
            catch (Exception ex)
            {
                 MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Retrieve Supplier Error", MessageBoxButtons.OK);
            }

        }

        private void ClearFormDetails(bool bClearSuppId)
        {
            if (bClearSuppId)
            {
                textSupplierId.Text = "";
            }
            textName1.Text = "";
            textName2.Text = "";
            cmbDefCurrency.SelectedIndex = -1;
            cmbCountry.SelectedIndex = -1;
            cmbDefPort.SelectedIndex = -1;
            cmbPaymentTerms.SelectedIndex = -1;
            cbActive.Checked = false;
            cbNonAccount.Checked = false;
            textParentSuppId.Text = "";
            textParentSuppName.Text = "";
            textNotes.Text = "";
            textRebateNotes.Text = "";
            textCBATerms.Text = "";
            textCBASuppType.Text = "";
            textContact.Text = "";
            cmbLockCode.SelectedIndex = -1;
            cmbExtraCurrency.SelectedIndex = -1;
            cmbDefPOType.SelectedIndex = -1;
            textAddr1.Text = "";
            textAddr2.Text = "";
            textAddr3.Text = "";
            textAddrState.Text = "";
            textPostCode.Text = "";
            textPhone.Text = "";
            textFax.Text = "";
            textMobile.Text = "";
            textEmailAddress.Text = "";
            textABN.Text = "";
            cmbEntityType.SelectedIndex = -1;
            dgvContext.DataSource = null;
            cmbContextId.SelectedIndex = -1;
            ClearContextDetails();
        }
        
        private void ClearContextDetails()
        {
            textContextSuppId.Text = "";
            textContextProfileNotes.Text = "";
            textContextRebateNotes.Text = "";
            cmbContextPaymentTerms.SelectedIndex = -1;
            cmbContextPOType.SelectedIndex = -1;
            textContextVendorShortDesc.Text = "";
            cmbContextDefInvHdrImg.SelectedIndex = -1;
        }

        private void RetrieveParentSupplierDetails(string sId)
        {
            try
            {
                if (sId.Length == 0)
                {
                    textParentSuppName.Text = "";
                    return;
                }
                SqlCommand rCom = new SqlCommand("select * from SUPPLIER where SUPPLIER_ID = @SUPP_ID", SqlHost.DBConn);
                rCom.Parameters.AddWithValue("@SUPP_ID", sId);
                SqlDataReader cReader = rCom.ExecuteReader();

                if (!cReader.HasRows)
                {
                    MessageBox.Show("Supplier does not exist.", "Supplier Error", MessageBoxButtons.OK);
                    textParentSuppId.Text = "";
                    textParentSuppName.Text = "";
                    cReader.Close();
                    return;
                }

                while (cReader.Read())
                {
                    if (cReader["NAME_1"] != System.DBNull.Value)
                        textParentSuppName.Text = cReader["NAME_1"].ToString();
                }
                cReader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Retrieve Parent Supplier Details Error", MessageBoxButtons.OK);
            }

        }

        private bool RefreshContextData(string sSuppId)
        {
            try
            {
                string sSql;
                SqlCommand sqlCmd;
                bool b;
                sSql = "select CONTEXT_ID, CONTEXT_SUPP_ID " +
                       "from CONTEXT_SUPP_PROFILE " +
                       "where SYSTEM_SUPP_ID = @SUPPID";

                sqlCmd = new SqlCommand();
                sqlCmd.Connection = SqlHost.DBConn;
                sqlCmd.CommandText = sSql;
                sqlCmd.Parameters.AddWithValue("@SUPPID", sSuppId);
                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    dtContextSupp = null;
                    dtContextSupp = new DataTable();
                    dtContextSupp.Load(dr);
                }
                dgvContext.DataSource = dtContextSupp;
                FormatContextDataView();
                textContextSuppId.Text = "";
                cmbContextId.SelectedIndex = -1;

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void FormatContextDataView()
        {
            try
            {
                if (dgvContext.DataSource != null)
                {
                    dgvContext.Columns["CONTEXT_ID"].HeaderText = "Company";
                    dgvContext.Columns["CONTEXT_ID"].Width = 150;
                    dgvContext.Columns["CONTEXT_SUPP_ID"].HeaderText = "Supplier Id";
                    dgvContext.Columns["CONTEXT_SUPP_ID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void dgvContext_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvContext.SelectedCells.Count == 0)
            {
                return;
            }
            DataRow dRow = ((DataRowView)dgvContext.SelectedCells[0].OwningRow.DataBoundItem).Row;
            string sId = dRow["CONTEXT_ID"].ToString();
            cmbContextId.SelectedValue = sId;
        }

        private void dgvContext_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvContext.SelectedCells.Count == 0)
            {
                return;
            }
            DataRow dRow = ((DataRowView)dgvContext.SelectedCells[0].OwningRow.DataBoundItem).Row;
            string sId = dRow["CONTEXT_ID"].ToString();
            cmbContextId.SelectedValue = sId;
        }

        private void pbSuppIdSearch_Click(object sender, EventArgs e)
        {
            SearchParam p = new SearchParam();
            p.SearchPartner = (ISearchHost)this;
            p.Sql = "Select C.CONTEXT_SUPP_ID as Supplier_Id, A.NAME_1 as Supplier_Name, A.SUPPLIER_ID as System_Id " +
                "from SUPPLIER A inner join CONTEXT_SUPP_PROFILE C on A.SUPPLIER_ID = C.SYSTEM_SUPP_ID and C.CONTEXT_ID = '" + AppHost.CurrentContextId + "' " +
                "where A.ACTIVE = 1 order by C.CONTEXT_SUPP_ID";
            p.ReturnObject = "SuppId";
            p.ReturnKey = new string[] { "System_Id", "Supplier_Name" };
            dlgSearch dlg = new dlgSearch();
            ((ISearchHost)dlg).SearchTransfer(p);
            dlg.ShowDialog(this);
        }

        private void pbAllSuppIdSearch_Click(object sender, EventArgs e)
        {
            SearchParam p = new SearchParam();
            p.SearchPartner = (ISearchHost)this;
            p.Sql = "Select SUPPLIER_ID as Supplier_Id, NAME_1 as Supplier_Name from SUPPLIER where ACTIVE = 1 order by SUPPLIER_ID";
            p.ReturnObject = "SuppId";
            p.ReturnKey = new string[] { "Supplier_Id", "Supplier_Name" };
            dlgSearch dlg = new dlgSearch();
            ((ISearchHost)dlg).SearchTransfer(p);
            dlg.ShowDialog(this);
        }

        private void pbParentSuppIdSearch_Click(object sender, EventArgs e)
        {
            SearchParam p = new SearchParam();
            p.SearchPartner = (ISearchHost)this;
            p.Sql = "Select SUPPLIER_ID as Supplier_Id, NAME_1 as Supplier_Name from SUPPLIER where ACTIVE = 1 order by SUPPLIER_ID";
            p.ReturnObject = "ParentSuppId";
            p.ReturnKey = new string[] { "Supplier_Id", "Supplier_Name" };
            dlgSearch dlg = new dlgSearch();
            ((ISearchHost)dlg).SearchTransfer(p);
            dlg.ShowDialog(this);
        }

        public void SearchTransfer(SearchParam sParam)
        {
            if (sParam.ReturnObject == "SuppId")
            {
                bAutoLoad = true;
                textSupplierId.Text = sParam.ReturnValue[0];
                RetrieveSupplierDetails(textSupplierId.Text);
                this.ActiveControl = textName1;
                bAutoLoad = false;
            }
            //if (sParam.ReturnObject == "SuppIdRO")
            //{
            //    bAutoLoad = true;
            //    textSupplierId.Text = sParam.ReturnValue[0];
            //    RetrieveSupplierDetails(textSupplierId.Text);
            //    this.ActiveControl = textName1;
            //    bAutoLoad = false;
            //    pbSave.Enabled = false;
            //    pbDelete.Enabled = false;
            //}
            if (sParam.ReturnObject == "ParentSuppId")
            {
                bAutoLoad = true;
                textParentSuppId.Text = sParam.ReturnValue[0];
                RetrieveParentSupplierDetails(textParentSuppId.Text);
                this.ActiveControl = textParentSuppId;
                bAutoLoad = false;
            }
        }

        private void pbSave_Click(object sender, EventArgs e)
        {
            if (SaveSupplier())
            {
                ClearFormDetails(true);
            }
            this.ActiveControl = textSupplierId;
        }

        private void pbCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pbDelete_Click(object sender, EventArgs e)
        {
            string sSql;
            if (bNewRow == false && MessageBox.Show(this, "Are you sure you want to delete this supplier?", "Wait Wait!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Hand) == DialogResult.Yes)
            {
                SqlCommand sqlCmd = new SqlCommand();
                sSql = "DELETE FROM SUPPLIER WHERE SUPPLIER_ID = @SUPP_ID";
                sqlCmd.Parameters.AddWithValue("@SUPP_ID", textSupplierId.Text);
                sqlCmd.Connection = SqlHost.DBConn;
                sqlCmd.CommandText = sSql;
                sqlCmd.ExecuteNonQuery();
                sSql = "DELETE FROM CONTEXT_SUPP_PROFILE WHERE SYSTEM_SUPP_ID = @SUPP_ID";
                sqlCmd.CommandText = sSql;
                sqlCmd.ExecuteNonQuery();
                ClearFormDetails(true);
                this.ActiveControl = textSupplierId;
            }
        }

        private bool SaveSupplier()
        {
            string sSql;
            try
            {
                if (textSupplierId.Text.Length > 0)
                {

                    if (bNewRow)
                    {
                        sSql = "insert into SUPPLIER ( " +
                            "SUPPLIER_ID, NAME_1, NAME_2, DEF_CURRENCY, COUNTRY_CODE, DEF_PORT, DEF_PAY_TERMS, ACTIVE, PARENT_SUPP, NOTES, " +
                            "CBA_TERMS, CBA_SUPPTYPE, LOCK_CODE, EXTRA_CURRENCY, DEF_PO_TYPE, CONTACT, ADDR_1, ADDR_2, ADDR_3, STATE, " +
                            "POSTCODE, PHONE, FAX, MOBILE, EMAIL_ADDRESS, ABN_NO, NON_ACCOUNT, REBATE_NOTES, ENTITY_TYPE " +
                            " ) " +
                            "VALUES ( " +
                            "@SUPPLIER_ID, @NAME_1, @NAME_2, @DEF_CURRENCY, @COUNTRY_CODE, @DEF_PORT, @DEF_PAY_TERMS, @ACTIVE, @PARENT_SUPP, @NOTES, " +
                            "@CBA_TERMS, @CBA_SUPPTYPE, @LOCK_CODE, @EXTRA_CURRENCY, @DEF_PO_TYPE, @CONTACT, @ADDR_1, @ADDR_2, @ADDR_3, @STATE, " +
                            "@POSTCODE, @PHONE, @FAX, @MOBILE, @EMAIL_ADDRESS, @ABN_NO, @NON_ACCOUNT, @REBATE_NOTES, @ENTITY_TYPE " +
                            " )";
                    }
                    else
                    {
                        sSql = "update SUPPLIER set " +
                            "NAME_1 = @NAME_1, NAME_2 = @NAME_2, DEF_CURRENCY = @DEF_CURRENCY, " +
                            "COUNTRY_CODE = @COUNTRY_CODE, DEF_PORT = @DEF_PORT, DEF_PAY_TERMS = @DEF_PAY_TERMS, " +
                            "ACTIVE = @ACTIVE, PARENT_SUPP = @PARENT_SUPP, NOTES = @NOTES," +
                            "CBA_TERMS = @CBA_TERMS, CBA_SUPPTYPE = @CBA_SUPPTYPE, LOCK_CODE = @LOCK_CODE, " +
                            "EXTRA_CURRENCY = @EXTRA_CURRENCY, DEF_PO_TYPE = @DEF_PO_TYPE, CONTACT = @CONTACT, " +
                            "ADDR_1 = @ADDR_1, ADDR_2 = @ADDR_2, ADDR_3 = @ADDR_3, STATE = @STATE, POSTCODE = @POSTCODE, " +
                            "PHONE = @PHONE, FAX = @FAX, MOBILE = @MOBILE, EMAIL_ADDRESS = @EMAIL_ADDRESS, " +
                            "ABN_NO = @ABN_NO, NON_ACCOUNT = @NON_ACCOUNT, REBATE_NOTES = @REBATE_NOTES, " +
                            "ENTITY_TYPE = @ENTITY_TYPE " +
                            " where SUPPLIER_ID = @SUPPLIER_ID";
                    }
                    SqlCommand sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                    sqlCmd.Parameters.AddWithValue("@SUPPLIER_ID", textSupplierId.Text.ToUpper());
                    sqlCmd.Parameters.AddWithValue("@NAME_1", textName1.Text);
                    sqlCmd.Parameters.AddWithValue("@NAME_2", textName2.Text);
                    if (cmbDefCurrency.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@DEF_CURRENCY", cmbDefCurrency.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@DEF_CURRENCY", "");
                    }
                    if (cmbCountry.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@COUNTRY_CODE", cmbCountry.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@COUNTRY_CODE", "");
                    }
                    if (cmbDefPort.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@DEF_PORT", cmbDefPort.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@DEF_PORT", "");
                    }
                    if (cmbPaymentTerms.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@DEF_PAY_TERMS", cmbPaymentTerms.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@DEF_PAY_TERMS", "");
                    }
                    sqlCmd.Parameters.AddWithValue("@ACTIVE", (cbActive.Checked ? 1 : 0));
                    sqlCmd.Parameters.AddWithValue("@NON_ACCOUNT", (cbNonAccount.Checked ? 1 : 0));
                    sqlCmd.Parameters.AddWithValue("@PARENT_SUPP", textParentSuppId.Text);
                    sqlCmd.Parameters.AddWithValue("@NOTES", textNotes.Text);
                    sqlCmd.Parameters.AddWithValue("@REBATE_NOTES", textRebateNotes.Text);
                    sqlCmd.Parameters.AddWithValue("@CBA_TERMS", textCBATerms.Text);
                    sqlCmd.Parameters.AddWithValue("@CBA_SUPPTYPE", textCBASuppType.Text);
                    sqlCmd.Parameters.AddWithValue("@CONTACT", textContact.Text);
                    if (cmbLockCode.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@LOCK_CODE", cmbLockCode.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@LOCK_CODE", "");
                    }
                    if (cmbExtraCurrency.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@EXTRA_CURRENCY", cmbExtraCurrency.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@EXTRA_CURRENCY", "");
                    }
                    if (cmbDefPOType.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@DEF_PO_TYPE", cmbDefPOType.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@DEF_PO_TYPE", "");
                    }
                    sqlCmd.Parameters.AddWithValue("@ADDR_1", textAddr1.Text);
                    sqlCmd.Parameters.AddWithValue("@ADDR_2", textAddr2.Text);
                    sqlCmd.Parameters.AddWithValue("@ADDR_3", textAddr3.Text);
                    sqlCmd.Parameters.AddWithValue("@STATE", textAddrState.Text);
                    sqlCmd.Parameters.AddWithValue("@POSTCODE", textPostCode.Text);
                    sqlCmd.Parameters.AddWithValue("@PHONE", textPhone.Text);
                    sqlCmd.Parameters.AddWithValue("@FAX", textFax.Text);
                    sqlCmd.Parameters.AddWithValue("@MOBILE", textMobile.Text);
                    sqlCmd.Parameters.AddWithValue("@EMAIL_ADDRESS", textEmailAddress.Text);
                    sqlCmd.Parameters.AddWithValue("@ABN_NO", textABN.Text);
                    if (cmbEntityType.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@ENTITY_TYPE", cmbEntityType.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@ENTITY_TYPE", "");
                    }
                    sqlCmd.ExecuteNonQuery();

                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void cmbContextId_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!bAutoLoad)
            {
                if (cmbContextId.SelectedIndex != -1)
                {
                    LoadContextSuppProfile(cmbContextId.SelectedValue.ToString(), textSupplierId.Text);
                }
                else
                {
                    ClearContextDetails();
                }
            }
        }

        private void LoadContextSuppProfile(string sContextId, string sSuppId)
        {
            try
            {
                string sSql = "select " +
                    "CONTEXT_SUPP_ID, DEF_PAY_TERMS, PROFILE_NOTES, DEF_PO_TYPE, REBATE_NOTES, INVOICE_HEADER_URI, VENDOR_SHORTDESC " +
                    "from CONTEXT_SUPP_PROFILE where CONTEXT_ID = @CONTEXTID and SYSTEM_SUPP_ID = @SYSSUPPID";
                SqlCommand sqlCmd;
                sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                sqlCmd.Parameters.AddWithValue("@CONTEXTID", sContextId);
                sqlCmd.Parameters.AddWithValue("@SYSSUPPID", sSuppId);


                SqlDataReader cReader = sqlCmd.ExecuteReader();

                bNewSuppContext = !cReader.HasRows;
                textContextSuppId.Text = "";

                while (cReader.Read())
                {
                    if (cReader["CONTEXT_SUPP_ID"] != System.DBNull.Value)
                    {
                        textContextSuppId.Text = cReader["CONTEXT_SUPP_ID"].ToString();
                    }
                    else
                    {
                        textContextSuppId.Text = "";
                    }
                    if (cReader["DEF_PAY_TERMS"] != System.DBNull.Value)
                    {
                        cmbContextPaymentTerms.SelectedValue = cReader["DEF_PAY_TERMS"].ToString();
                    }
                    else
                    {
                        cmbContextPaymentTerms.SelectedIndex = -1;
                    }
                    if (cReader["PROFILE_NOTES"] != System.DBNull.Value)
                    {
                        textContextProfileNotes.Text = cReader["PROFILE_NOTES"].ToString();
                    }
                    else
                    {
                        textContextProfileNotes.Text = "";
                    }
                    if (cReader["DEF_PO_TYPE"] != System.DBNull.Value)
                    {
                        cmbContextPOType.SelectedValue = cReader["DEF_PO_TYPE"].ToString();
                    }
                    else
                    {
                        cmbContextPOType.SelectedIndex = -1;
                    }
                    if (cReader["REBATE_NOTES"] != System.DBNull.Value)
                    {
                        textContextRebateNotes.Text = cReader["REBATE_NOTES"].ToString();
                    }
                    else
                    {
                        textContextRebateNotes.Text = "";
                    }
                    if (cReader["INVOICE_HEADER_URI"] != System.DBNull.Value)
                    {
                        cmbContextDefInvHdrImg.Text = cReader["INVOICE_HEADER_URI"].ToString();
                    }
                    else
                    {
                        cmbContextDefInvHdrImg.SelectedIndex = -1;
                    }
                    if (cReader["VENDOR_SHORTDESC"] != System.DBNull.Value)
                    {
                        textContextVendorShortDesc.Text = cReader["VENDOR_SHORTDESC"].ToString();
                    }
                    else
                    {
                        textContextVendorShortDesc.Text = "";
                    }
                }
                cReader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Context Supplier Details Error", MessageBoxButtons.OK);
            }
        }

        private void pbContextSave_Click(object sender, EventArgs e)
        {
            SaveContextSuppProfile();
        }

        private void SaveContextSuppProfile()
        {
            string sSql;
            string sSupplierId;
            sSupplierId = textSupplierId.Text;
            if (sSupplierId.Length == 0)
            {
                return;
            }
            if (cmbContextId.SelectedIndex == -1)
            {
                return;
            }
            if (textContextSuppId.Text.Length == 0)
            {
                DeleteContextSuppProfile();
                return;
            }
            if (bNewSuppContext)
            {
                sSql = "insert into CONTEXT_SUPP_PROFILE ( " +
                    "CONTEXT_ID, SYSTEM_SUPP_ID, CONTEXT_SUPP_ID, DEF_PAY_TERMS, " +
                    "PROFILE_NOTES, DEF_PO_TYPE, REBATE_NOTES, INVOICE_HEADER_URI, VENDOR_SHORTDESC " +
                    " ) values ( " +
                    "@CONTEXT_ID, @SYSTEM_SUPP_ID, @CONTEXT_SUPP_ID, @DEF_PAY_TERMS, " +
                    "@PROFILE_NOTES, @DEF_PO_TYPE, @REBATE_NOTES, @INVOICE_HEADER_URI, @VENDOR_SHORTDESC " +
                    ")";
            }
            else
            {
                sSql = "update " +
                    "CONTEXT_SUPP_PROFILE set " +
                    "CONTEXT_SUPP_ID = @CONTEXT_SUPP_ID, DEF_PAY_TERMS = @DEF_PAY_TERMS, " +
                    "PROFILE_NOTES = @PROFILE_NOTES, DEF_PO_TYPE = @DEF_PO_TYPE, " +
                    "REBATE_NOTES = @REBATE_NOTES, " +
                    "INVOICE_HEADER_URI = @INVOICE_HEADER_URI, " +
                    "VENDOR_SHORTDESC = @VENDOR_SHORTDESC " +
                    "where CONTEXT_ID = @CONTEXT_ID and SYSTEM_SUPP_ID = @SYSTEM_SUPP_ID";
            }
            SqlCommand sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
            sqlCmd.Parameters.AddWithValue("@CONTEXT_ID", cmbContextId.SelectedValue.ToString());
            sqlCmd.Parameters.AddWithValue("@SYSTEM_SUPP_ID", sSupplierId);
            sqlCmd.Parameters.AddWithValue("@CONTEXT_SUPP_ID", textContextSuppId.Text);
            if (cmbContextPaymentTerms.SelectedIndex != -1)
            {
                sqlCmd.Parameters.AddWithValue("@DEF_PAY_TERMS", cmbContextPaymentTerms.SelectedValue);
            }
            else
            {
                sqlCmd.Parameters.AddWithValue("@DEF_PAY_TERMS", "");
            }
            sqlCmd.Parameters.AddWithValue("@PROFILE_NOTES", textContextProfileNotes.Text);
            if (cmbContextPOType.SelectedIndex != -1)
            {
                sqlCmd.Parameters.AddWithValue("@DEF_PO_TYPE", cmbContextPOType.SelectedValue);
            }
            else
            {
                sqlCmd.Parameters.AddWithValue("@DEF_PO_TYPE", "");
            }
            sqlCmd.Parameters.AddWithValue("@REBATE_NOTES", textContextRebateNotes.Text);
            if (cmbContextDefInvHdrImg.SelectedIndex != -1)
            {
                sqlCmd.Parameters.AddWithValue("@INVOICE_HEADER_URI", cmbContextDefInvHdrImg.Text);
            }
            else
            {
                sqlCmd.Parameters.AddWithValue("@INVOICE_HEADER_URI", "");
            }
            sqlCmd.Parameters.AddWithValue("@VENDOR_SHORTDESC", textContextVendorShortDesc.Text);

            sqlCmd.ExecuteNonQuery();
            RefreshContextData(sSupplierId);
        }

        private void pbContextDelete_Click(object sender, EventArgs e)
        {
            DeleteContextSuppProfile();
        }

        private void DeleteContextSuppProfile()
        {
            string sSupplierId;
            sSupplierId = textSupplierId.Text;
            if (sSupplierId.Length == 0)
            {
                return;
            }
            if (cmbContextId.SelectedIndex == -1)
            {
                return;
            }
            if (MessageBox.Show("Are you sure you want to delete this Supplier/Company relationship?", "This cannot be undone", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                SqlCommand sqlCmd = new SqlCommand("delete from CONTEXT_SUPP_PROFILE where CONTEXT_ID = @CONTEXTID and SYSTEM_SUPP_ID = @SYSTEMSUPPID", SqlHost.DBConn);
                sqlCmd.Parameters.AddWithValue("@CONTEXTID", cmbContextId.SelectedValue.ToString());
                sqlCmd.Parameters.AddWithValue("@SYSTEMSUPPID", sSupplierId);
                sqlCmd.ExecuteNonQuery();
            }
            RefreshContextData(sSupplierId);
        }

    }
}

﻿namespace OMT.Forms
{
    partial class frmCustomerMaint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomerMaint));
            this.tlpFramework = new System.Windows.Forms.TableLayoutPanel();
            this.tlpActions = new System.Windows.Forms.TableLayoutPanel();
            this.pbSave = new System.Windows.Forms.Button();
            this.imageListActions = new System.Windows.Forms.ImageList(this.components);
            this.pbCancel = new System.Windows.Forms.Button();
            this.pbDelete = new System.Windows.Forms.Button();
            this.lblActions = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tlpCustomer = new System.Windows.Forms.TableLayoutPanel();
            this.lblName1 = new System.Windows.Forms.Label();
            this.lblName2 = new System.Windows.Forms.Label();
            this.tlpCustId = new System.Windows.Forms.TableLayoutPanel();
            this.pbAllCustSearch = new System.Windows.Forms.Button();
            this.pbCustIdSearch = new System.Windows.Forms.Button();
            this.textCustomerId = new System.Windows.Forms.TextBox();
            this.textCustName1 = new System.Windows.Forms.TextBox();
            this.textCustName2 = new System.Windows.Forms.TextBox();
            this.cmbCustType = new System.Windows.Forms.ComboBox();
            this.lblBusinessName = new System.Windows.Forms.Label();
            this.lblLockCode = new System.Windows.Forms.Label();
            this.textCustBusinessName = new System.Windows.Forms.TextBox();
            this.lblParentCustomer = new System.Windows.Forms.Label();
            this.tlpParentCust = new System.Windows.Forms.TableLayoutPanel();
            this.pbParentCustIdSearch = new System.Windows.Forms.Button();
            this.textParentCustId = new System.Windows.Forms.TextBox();
            this.textParentCustName = new System.Windows.Forms.TextBox();
            this.lblCompanyNo = new System.Windows.Forms.Label();
            this.lblEnityType = new System.Windows.Forms.Label();
            this.textABNNo = new System.Windows.Forms.TextBox();
            this.cmbEntityType = new System.Windows.Forms.ComboBox();
            this.lblSystemID = new System.Windows.Forms.Label();
            this.lblCustomerType = new System.Windows.Forms.Label();
            this.textDiscountNotes = new System.Windows.Forms.TextBox();
            this.textNotes = new System.Windows.Forms.TextBox();
            this.lblDepositRequired = new System.Windows.Forms.Label();
            this.cmbDepositReq = new System.Windows.Forms.ComboBox();
            this.lblDefaultPort = new System.Windows.Forms.Label();
            this.cmbDefPort = new System.Windows.Forms.ComboBox();
            this.lblDefaultPaymentTerms = new System.Windows.Forms.Label();
            this.cmbPaymentTerms = new System.Windows.Forms.ComboBox();
            this.lblDefaultOrderTerms = new System.Windows.Forms.Label();
            this.cmbOrderTerms = new System.Windows.Forms.ComboBox();
            this.lblDefaultCurrency = new System.Windows.Forms.Label();
            this.cmbDefCurrency = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.cbSplitOnly = new System.Windows.Forms.CheckBox();
            this.cbActive = new System.Windows.Forms.CheckBox();
            this.lblDiscountNotes = new System.Windows.Forms.Label();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.cmbCBALockCode = new System.Windows.Forms.ComboBox();
            this.lblGeneralNotes = new System.Windows.Forms.Label();
            this.textTaxNumber = new System.Windows.Forms.TextBox();
            this.lblTaxNumber = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tlpAddress = new System.Windows.Forms.TableLayoutPanel();
            this.textAddrLabel = new System.Windows.Forms.TextBox();
            this.lblDCAddressLabel = new System.Windows.Forms.Label();
            this.lblStreet = new System.Windows.Forms.Label();
            this.lblsuburbCity = new System.Windows.Forms.Label();
            this.lblAddress3 = new System.Windows.Forms.Label();
            this.textMainAddr1 = new System.Windows.Forms.TextBox();
            this.textMainAddr2 = new System.Windows.Forms.TextBox();
            this.textMainAddr3 = new System.Windows.Forms.TextBox();
            this.lblState = new System.Windows.Forms.Label();
            this.lblPostCode = new System.Windows.Forms.Label();
            this.lblCoutry = new System.Windows.Forms.Label();
            this.textMainAddrState = new System.Windows.Forms.TextBox();
            this.textMainAddrPostCode = new System.Windows.Forms.TextBox();
            this.cmbMainAddrCountry = new System.Windows.Forms.ComboBox();
            this.lblPhonenumber = new System.Windows.Forms.Label();
            this.lblFaxNumber = new System.Windows.Forms.Label();
            this.lblEmailAddress = new System.Windows.Forms.Label();
            this.tlpDefaultDelAddr = new System.Windows.Forms.TableLayoutPanel();
            this.pbAddresses = new System.Windows.Forms.Button();
            this.cbIsDel = new System.Windows.Forms.CheckBox();
            this.textMainAddrPhone = new System.Windows.Forms.TextBox();
            this.textMainAddrFax = new System.Windows.Forms.TextBox();
            this.textMainAddrEmail = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.pbContacts = new System.Windows.Forms.Button();
            this.lblPrimaryContact = new System.Windows.Forms.Label();
            this.cmbPrimaryContactNo = new System.Windows.Forms.ComboBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.cmbWeightConv = new System.Windows.Forms.ComboBox();
            this.cmbVolumeConv = new System.Windows.Forms.ComboBox();
            this.lblDefQuoteRebate = new System.Windows.Forms.Label();
            this.lblDefQuoteDisc = new System.Windows.Forms.Label();
            this.lblDefaultSlushie = new System.Windows.Forms.Label();
            this.lblShippingMarks = new System.Windows.Forms.Label();
            this.textCBAShippingMarks = new System.Windows.Forms.TextBox();
            this.lblRestrictions = new System.Windows.Forms.Label();
            this.lblOrderAlert = new System.Windows.Forms.Label();
            this.textCBAOrderAlert = new System.Windows.Forms.TextBox();
            this.textCBARestrictions = new System.Windows.Forms.TextBox();
            this.lblQuoteCurrency = new System.Windows.Forms.Label();
            this.lblInvoiceType = new System.Windows.Forms.Label();
            this.lblinvoiceProformaType = new System.Windows.Forms.Label();
            this.cmbCBAQCurrency = new System.Windows.Forms.ComboBox();
            this.cmbCBADefInvType = new System.Windows.Forms.ComboBox();
            this.cmbCBADefPIType = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.pbProductProfileIdSearch = new System.Windows.Forms.Button();
            this.textProductProfileId = new System.Windows.Forms.TextBox();
            this.textProductProfileName = new System.Windows.Forms.TextBox();
            this.lblProductProfileID = new System.Windows.Forms.Label();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxDefaultQuoteRebate = new C1.Win.C1Input.C1NumericEdit();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxDefaultQuoteDiscount = new C1.Win.C1Input.C1NumericEdit();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxDefaultSlushie = new C1.Win.C1Input.C1NumericEdit();
            this.lblVolumePref = new System.Windows.Forms.Label();
            this.lblWeightPref = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTerms = new System.Windows.Forms.Label();
            this.lblBranch = new System.Windows.Forms.Label();
            this.lblsalesman = new System.Windows.Forms.Label();
            this.lblPrieCode = new System.Windows.Forms.Label();
            this.lblInvoiceDiscount = new System.Windows.Forms.Label();
            this.textCBATerms = new System.Windows.Forms.TextBox();
            this.textCBABranch = new System.Windows.Forms.TextBox();
            this.textCBASalesman = new System.Windows.Forms.TextBox();
            this.textCBAPriceCode = new System.Windows.Forms.TextBox();
            this.textCBAInvoiceDiscount = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tlpContext = new System.Windows.Forms.TableLayoutPanel();
            this.dgvContext = new System.Windows.Forms.DataGridView();
            this.tlpContextActions = new System.Windows.Forms.TableLayoutPanel();
            this.pbContextDelete = new System.Windows.Forms.Button();
            this.pbContextSave = new System.Windows.Forms.Button();
            this.lblInternalID = new System.Windows.Forms.Label();
            this.lblCustomerID = new System.Windows.Forms.Label();
            this.cmbContextId = new System.Windows.Forms.ComboBox();
            this.textContextCustId = new System.Windows.Forms.TextBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.lblOrderTerms = new System.Windows.Forms.Label();
            this.lblPaymentTerms = new System.Windows.Forms.Label();
            this.lblCPIDiscountNotes = new System.Windows.Forms.Label();
            this.lblTermsDiscountNotes = new System.Windows.Forms.Label();
            this.cmbContextOrderTerms = new System.Windows.Forms.ComboBox();
            this.cmbContextPaymentTerms = new System.Windows.Forms.ComboBox();
            this.cmbContextDepositReq = new System.Windows.Forms.ComboBox();
            this.textContextDiscountNotes = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.pbChooseInvHdrImg = new System.Windows.Forms.Button();
            this.lblTDInvoiceHdrImg = new System.Windows.Forms.Label();
            this.textContextDefInvHdrImg = new System.Windows.Forms.TextBox();
            this.lblTDInvoiceType = new System.Windows.Forms.Label();
            this.lblTDProformaType = new System.Windows.Forms.Label();
            this.lblTDOrderAlert = new System.Windows.Forms.Label();
            this.cmbContextInvoiceType = new System.Windows.Forms.ComboBox();
            this.cmbContextProformaType = new System.Windows.Forms.ComboBox();
            this.textContextOrderAlert = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.cmbContextDefInvHdrImg = new System.Windows.Forms.ComboBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.lblQDQuoteCurrency = new System.Windows.Forms.Label();
            this.lblQDRebate = new System.Windows.Forms.Label();
            this.lblQDDiscount = new System.Windows.Forms.Label();
            this.lblQDSlushie = new System.Windows.Forms.Label();
            this.cmbContextQuoteCurrency = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.textContextQuoteRebate = new C1.Win.C1Input.C1NumericEdit();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.textContextQuoteDiscount = new C1.Win.C1Input.C1NumericEdit();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.textContextQuoteSlushie = new C1.Win.C1Input.C1NumericEdit();
            this.textContextProfileNotes = new System.Windows.Forms.TextBox();
            this.pbContextCopyDefaults = new System.Windows.Forms.Button();
            this.VendorNumber = new System.Windows.Forms.Label();
            this.textContextVendorNumber = new System.Windows.Forms.TextBox();
            this.Profilenotes = new System.Windows.Forms.Label();
            this.VendorShort = new System.Windows.Forms.Label();
            this.textContextVendorShortDesc = new System.Windows.Forms.TextBox();
            this.tlpFramework.SuspendLayout();
            this.tlpActions.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tlpCustomer.SuspendLayout();
            this.tlpCustId.SuspendLayout();
            this.tlpParentCust.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tlpAddress.SuspendLayout();
            this.tlpDefaultDelAddr.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDefaultQuoteRebate)).BeginInit();
            this.tableLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDefaultQuoteDiscount)).BeginInit();
            this.tableLayoutPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDefaultSlushie)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tlpContext.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContext)).BeginInit();
            this.tlpContextActions.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textContextQuoteRebate)).BeginInit();
            this.tableLayoutPanel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textContextQuoteDiscount)).BeginInit();
            this.tableLayoutPanel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textContextQuoteSlushie)).BeginInit();
            this.SuspendLayout();
            // 
            // tlpFramework
            // 
            this.tlpFramework.ColumnCount = 3;
            this.tlpFramework.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpFramework.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpFramework.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpFramework.Controls.Add(this.tlpActions, 1, 4);
            this.tlpFramework.Controls.Add(this.groupBox2, 1, 1);
            this.tlpFramework.Controls.Add(this.tabControl1, 1, 2);
            this.tlpFramework.Controls.Add(this.groupBox6, 1, 3);
            this.tlpFramework.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpFramework.Location = new System.Drawing.Point(0, 0);
            this.tlpFramework.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tlpFramework.Name = "tlpFramework";
            this.tlpFramework.RowCount = 5;
            this.tlpFramework.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3F));
            this.tlpFramework.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 408F));
            this.tlpFramework.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 337F));
            this.tlpFramework.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpFramework.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tlpFramework.Size = new System.Drawing.Size(1455, 1071);
            this.tlpFramework.TabIndex = 0;
            // 
            // tlpActions
            // 
            this.tlpActions.ColumnCount = 6;
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpActions.Controls.Add(this.pbSave, 2, 0);
            this.tlpActions.Controls.Add(this.pbCancel, 3, 0);
            this.tlpActions.Controls.Add(this.pbDelete, 4, 0);
            this.tlpActions.Controls.Add(this.lblActions, 1, 0);
            this.tlpActions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpActions.Location = new System.Drawing.Point(12, 1023);
            this.tlpActions.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tlpActions.Name = "tlpActions";
            this.tlpActions.RowCount = 1;
            this.tlpActions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpActions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tlpActions.Size = new System.Drawing.Size(1431, 48);
            this.tlpActions.TabIndex = 66;
            // 
            // pbSave
            // 
            this.pbSave.ImageKey = "Add32.png";
            this.pbSave.ImageList = this.imageListActions;
            this.pbSave.Location = new System.Drawing.Point(955, 5);
            this.pbSave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbSave.Name = "pbSave";
            this.pbSave.Size = new System.Drawing.Size(112, 35);
            this.pbSave.TabIndex = 66;
            this.pbSave.Text = "&Save";
            this.pbSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbSave.UseVisualStyleBackColor = true;
            this.pbSave.Click += new System.EventHandler(this.pbSave_Click);
            // 
            // imageListActions
            // 
            this.imageListActions.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListActions.ImageStream")));
            this.imageListActions.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListActions.Images.SetKeyName(0, "Add32.png");
            this.imageListActions.Images.SetKeyName(1, "Cancel32.png");
            this.imageListActions.Images.SetKeyName(2, "Delete32.png");
            this.imageListActions.Images.SetKeyName(3, "Find32.png");
            this.imageListActions.Images.SetKeyName(4, "Address32.png");
            this.imageListActions.Images.SetKeyName(5, "Contact32.png");
            // 
            // pbCancel
            // 
            this.pbCancel.ImageKey = "Cancel32.png";
            this.pbCancel.ImageList = this.imageListActions;
            this.pbCancel.Location = new System.Drawing.Point(1105, 5);
            this.pbCancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbCancel.Name = "pbCancel";
            this.pbCancel.Size = new System.Drawing.Size(112, 35);
            this.pbCancel.TabIndex = 67;
            this.pbCancel.Text = "&Cancel";
            this.pbCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbCancel.UseVisualStyleBackColor = true;
            this.pbCancel.Click += new System.EventHandler(this.pbCancel_Click);
            // 
            // pbDelete
            // 
            this.pbDelete.ImageKey = "Delete32.png";
            this.pbDelete.ImageList = this.imageListActions;
            this.pbDelete.Location = new System.Drawing.Point(1255, 5);
            this.pbDelete.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbDelete.Name = "pbDelete";
            this.pbDelete.Size = new System.Drawing.Size(112, 35);
            this.pbDelete.TabIndex = 68;
            this.pbDelete.Text = "&Delete";
            this.pbDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbDelete.UseVisualStyleBackColor = true;
            this.pbDelete.Click += new System.EventHandler(this.pbDelete_Click);
            // 
            // lblActions
            // 
            this.lblActions.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblActions.AutoSize = true;
            this.lblActions.Location = new System.Drawing.Point(881, 14);
            this.lblActions.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblActions.Name = "lblActions";
            this.lblActions.Size = new System.Drawing.Size(66, 20);
            this.lblActions.TabIndex = 0;
            this.lblActions.Text = "Actions:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tlpCustomer);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(12, 3);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(1431, 408);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Customer Details";
            // 
            // tlpCustomer
            // 
            this.tlpCustomer.ColumnCount = 4;
            this.tlpCustomer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 144F));
            this.tlpCustomer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpCustomer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 191F));
            this.tlpCustomer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpCustomer.Controls.Add(this.lblName1, 0, 2);
            this.tlpCustomer.Controls.Add(this.lblName2, 0, 3);
            this.tlpCustomer.Controls.Add(this.tlpCustId, 1, 0);
            this.tlpCustomer.Controls.Add(this.textCustName1, 1, 2);
            this.tlpCustomer.Controls.Add(this.textCustName2, 1, 3);
            this.tlpCustomer.Controls.Add(this.cmbCustType, 3, 0);
            this.tlpCustomer.Controls.Add(this.lblBusinessName, 0, 4);
            this.tlpCustomer.Controls.Add(this.lblLockCode, 0, 8);
            this.tlpCustomer.Controls.Add(this.textCustBusinessName, 1, 4);
            this.tlpCustomer.Controls.Add(this.lblParentCustomer, 0, 7);
            this.tlpCustomer.Controls.Add(this.tlpParentCust, 1, 7);
            this.tlpCustomer.Controls.Add(this.lblCompanyNo, 0, 5);
            this.tlpCustomer.Controls.Add(this.lblEnityType, 0, 6);
            this.tlpCustomer.Controls.Add(this.textABNNo, 1, 5);
            this.tlpCustomer.Controls.Add(this.cmbEntityType, 1, 6);
            this.tlpCustomer.Controls.Add(this.lblSystemID, 0, 0);
            this.tlpCustomer.Controls.Add(this.lblCustomerType, 2, 0);
            this.tlpCustomer.Controls.Add(this.textDiscountNotes, 3, 8);
            this.tlpCustomer.Controls.Add(this.textNotes, 3, 9);
            this.tlpCustomer.Controls.Add(this.lblDepositRequired, 2, 7);
            this.tlpCustomer.Controls.Add(this.cmbDepositReq, 3, 7);
            this.tlpCustomer.Controls.Add(this.lblDefaultPort, 2, 6);
            this.tlpCustomer.Controls.Add(this.cmbDefPort, 3, 6);
            this.tlpCustomer.Controls.Add(this.lblDefaultPaymentTerms, 2, 5);
            this.tlpCustomer.Controls.Add(this.cmbPaymentTerms, 3, 5);
            this.tlpCustomer.Controls.Add(this.lblDefaultOrderTerms, 2, 4);
            this.tlpCustomer.Controls.Add(this.cmbOrderTerms, 3, 4);
            this.tlpCustomer.Controls.Add(this.lblDefaultCurrency, 2, 3);
            this.tlpCustomer.Controls.Add(this.cmbDefCurrency, 3, 3);
            this.tlpCustomer.Controls.Add(this.tableLayoutPanel3, 3, 2);
            this.tlpCustomer.Controls.Add(this.lblDiscountNotes, 2, 8);
            this.tlpCustomer.Controls.Add(this.tableLayoutPanel8, 1, 8);
            this.tlpCustomer.Controls.Add(this.lblGeneralNotes, 2, 9);
            this.tlpCustomer.Controls.Add(this.textTaxNumber, 1, 9);
            this.tlpCustomer.Controls.Add(this.lblTaxNumber, 0, 9);
            this.tlpCustomer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpCustomer.Location = new System.Drawing.Point(4, 24);
            this.tlpCustomer.Margin = new System.Windows.Forms.Padding(0);
            this.tlpCustomer.Name = "tlpCustomer";
            this.tlpCustomer.RowCount = 10;
            this.tlpCustomer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tlpCustomer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpCustomer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpCustomer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpCustomer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpCustomer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpCustomer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpCustomer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpCustomer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpCustomer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCustomer.Size = new System.Drawing.Size(1423, 379);
            this.tlpCustomer.TabIndex = 0;
            // 
            // lblName1
            // 
            this.lblName1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblName1.AutoSize = true;
            this.lblName1.Location = new System.Drawing.Point(4, 64);
            this.lblName1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName1.Name = "lblName1";
            this.lblName1.Size = new System.Drawing.Size(68, 20);
            this.lblName1.TabIndex = 1;
            this.lblName1.Text = "Name 1:";
            // 
            // lblName2
            // 
            this.lblName2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblName2.AutoSize = true;
            this.lblName2.Location = new System.Drawing.Point(4, 104);
            this.lblName2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName2.Name = "lblName2";
            this.lblName2.Size = new System.Drawing.Size(68, 20);
            this.lblName2.TabIndex = 2;
            this.lblName2.Text = "Name 2:";
            // 
            // tlpCustId
            // 
            this.tlpCustId.ColumnCount = 3;
            this.tlpCustId.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 225F));
            this.tlpCustId.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpCustId.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpCustId.Controls.Add(this.pbAllCustSearch, 2, 0);
            this.tlpCustId.Controls.Add(this.pbCustIdSearch, 1, 0);
            this.tlpCustId.Controls.Add(this.textCustomerId, 0, 0);
            this.tlpCustId.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpCustId.Location = new System.Drawing.Point(144, 0);
            this.tlpCustId.Margin = new System.Windows.Forms.Padding(0);
            this.tlpCustId.Name = "tlpCustId";
            this.tlpCustId.RowCount = 1;
            this.tlpCustId.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCustId.Size = new System.Drawing.Size(544, 46);
            this.tlpCustId.TabIndex = 0;
            // 
            // pbAllCustSearch
            // 
            this.pbAllCustSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbAllCustSearch.ImageKey = "Find32.png";
            this.pbAllCustSearch.ImageList = this.imageListActions;
            this.pbAllCustSearch.Location = new System.Drawing.Point(388, 5);
            this.pbAllCustSearch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbAllCustSearch.Name = "pbAllCustSearch";
            this.pbAllCustSearch.Size = new System.Drawing.Size(152, 36);
            this.pbAllCustSearch.TabIndex = 2;
            this.pbAllCustSearch.TabStop = false;
            this.pbAllCustSearch.Text = "All Cust";
            this.pbAllCustSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.pbAllCustSearch.UseVisualStyleBackColor = true;
            this.pbAllCustSearch.Click += new System.EventHandler(this.pbAllCustSearch_Click);
            // 
            // pbCustIdSearch
            // 
            this.pbCustIdSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbCustIdSearch.ImageKey = "Find32.png";
            this.pbCustIdSearch.ImageList = this.imageListActions;
            this.pbCustIdSearch.Location = new System.Drawing.Point(229, 5);
            this.pbCustIdSearch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbCustIdSearch.Name = "pbCustIdSearch";
            this.pbCustIdSearch.Size = new System.Drawing.Size(151, 36);
            this.pbCustIdSearch.TabIndex = 1;
            this.pbCustIdSearch.TabStop = false;
            this.pbCustIdSearch.Text = "My Cust";
            this.pbCustIdSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.pbCustIdSearch.UseVisualStyleBackColor = true;
            this.pbCustIdSearch.Click += new System.EventHandler(this.pbCustIdSearch_Click);
            // 
            // textCustomerId
            // 
            this.textCustomerId.Dock = System.Windows.Forms.DockStyle.Top;
            this.textCustomerId.Location = new System.Drawing.Point(4, 5);
            this.textCustomerId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textCustomerId.Name = "textCustomerId";
            this.textCustomerId.Size = new System.Drawing.Size(217, 26);
            this.textCustomerId.TabIndex = 0;
            this.textCustomerId.TextChanged += new System.EventHandler(this.textCustomerId_TextChanged);
            this.textCustomerId.Validating += new System.ComponentModel.CancelEventHandler(this.textCustomerId_Validating);
            // 
            // textCustName1
            // 
            this.textCustName1.Dock = System.Windows.Forms.DockStyle.Top;
            this.textCustName1.Location = new System.Drawing.Point(148, 59);
            this.textCustName1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textCustName1.Name = "textCustName1";
            this.textCustName1.Size = new System.Drawing.Size(536, 26);
            this.textCustName1.TabIndex = 3;
            // 
            // textCustName2
            // 
            this.textCustName2.Dock = System.Windows.Forms.DockStyle.Top;
            this.textCustName2.Location = new System.Drawing.Point(148, 99);
            this.textCustName2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textCustName2.Name = "textCustName2";
            this.textCustName2.Size = new System.Drawing.Size(536, 26);
            this.textCustName2.TabIndex = 4;
            // 
            // cmbCustType
            // 
            this.cmbCustType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbCustType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCustType.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbCustType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCustType.FormattingEnabled = true;
            this.cmbCustType.Location = new System.Drawing.Point(883, 5);
            this.cmbCustType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbCustType.Name = "cmbCustType";
            this.cmbCustType.Size = new System.Drawing.Size(536, 28);
            this.cmbCustType.TabIndex = 11;
            // 
            // lblBusinessName
            // 
            this.lblBusinessName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblBusinessName.AutoSize = true;
            this.lblBusinessName.Location = new System.Drawing.Point(4, 144);
            this.lblBusinessName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBusinessName.Name = "lblBusinessName";
            this.lblBusinessName.Size = new System.Drawing.Size(124, 20);
            this.lblBusinessName.TabIndex = 3;
            this.lblBusinessName.Text = "Business Name:";
            // 
            // lblLockCode
            // 
            this.lblLockCode.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblLockCode.AutoSize = true;
            this.lblLockCode.Location = new System.Drawing.Point(4, 304);
            this.lblLockCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLockCode.Name = "lblLockCode";
            this.lblLockCode.Size = new System.Drawing.Size(89, 20);
            this.lblLockCode.TabIndex = 9;
            this.lblLockCode.Text = "Lock Code:";
            // 
            // textCustBusinessName
            // 
            this.textCustBusinessName.Dock = System.Windows.Forms.DockStyle.Top;
            this.textCustBusinessName.Location = new System.Drawing.Point(148, 139);
            this.textCustBusinessName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textCustBusinessName.Name = "textCustBusinessName";
            this.textCustBusinessName.Size = new System.Drawing.Size(536, 26);
            this.textCustBusinessName.TabIndex = 5;
            // 
            // lblParentCustomer
            // 
            this.lblParentCustomer.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblParentCustomer.AutoSize = true;
            this.lblParentCustomer.Location = new System.Drawing.Point(4, 264);
            this.lblParentCustomer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblParentCustomer.Name = "lblParentCustomer";
            this.lblParentCustomer.Size = new System.Drawing.Size(133, 20);
            this.lblParentCustomer.TabIndex = 4;
            this.lblParentCustomer.Text = "Parent Customer:";
            // 
            // tlpParentCust
            // 
            this.tlpParentCust.ColumnCount = 3;
            this.tlpParentCust.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 225F));
            this.tlpParentCust.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tlpParentCust.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpParentCust.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpParentCust.Controls.Add(this.pbParentCustIdSearch, 1, 0);
            this.tlpParentCust.Controls.Add(this.textParentCustId, 0, 0);
            this.tlpParentCust.Controls.Add(this.textParentCustName, 2, 0);
            this.tlpParentCust.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpParentCust.Location = new System.Drawing.Point(144, 254);
            this.tlpParentCust.Margin = new System.Windows.Forms.Padding(0);
            this.tlpParentCust.Name = "tlpParentCust";
            this.tlpParentCust.RowCount = 1;
            this.tlpParentCust.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpParentCust.Size = new System.Drawing.Size(544, 40);
            this.tlpParentCust.TabIndex = 8;
            // 
            // pbParentCustIdSearch
            // 
            this.pbParentCustIdSearch.ImageKey = "Find32.png";
            this.pbParentCustIdSearch.ImageList = this.imageListActions;
            this.pbParentCustIdSearch.Location = new System.Drawing.Point(229, 5);
            this.pbParentCustIdSearch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbParentCustIdSearch.Name = "pbParentCustIdSearch";
            this.pbParentCustIdSearch.Size = new System.Drawing.Size(28, 30);
            this.pbParentCustIdSearch.TabIndex = 17;
            this.pbParentCustIdSearch.TabStop = false;
            this.pbParentCustIdSearch.UseVisualStyleBackColor = true;
            this.pbParentCustIdSearch.Click += new System.EventHandler(this.pbParentCustIdSearch_Click);
            // 
            // textParentCustId
            // 
            this.textParentCustId.Dock = System.Windows.Forms.DockStyle.Top;
            this.textParentCustId.Location = new System.Drawing.Point(4, 5);
            this.textParentCustId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textParentCustId.Name = "textParentCustId";
            this.textParentCustId.Size = new System.Drawing.Size(217, 26);
            this.textParentCustId.TabIndex = 8;
            this.textParentCustId.Validating += new System.ComponentModel.CancelEventHandler(this.textParentCustId_Validating);
            // 
            // textParentCustName
            // 
            this.textParentCustName.Dock = System.Windows.Forms.DockStyle.Top;
            this.textParentCustName.Location = new System.Drawing.Point(268, 5);
            this.textParentCustName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textParentCustName.Name = "textParentCustName";
            this.textParentCustName.ReadOnly = true;
            this.textParentCustName.Size = new System.Drawing.Size(272, 26);
            this.textParentCustName.TabIndex = 18;
            this.textParentCustName.TabStop = false;
            // 
            // lblCompanyNo
            // 
            this.lblCompanyNo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblCompanyNo.AutoSize = true;
            this.lblCompanyNo.Location = new System.Drawing.Point(4, 184);
            this.lblCompanyNo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCompanyNo.Name = "lblCompanyNo";
            this.lblCompanyNo.Size = new System.Drawing.Size(108, 20);
            this.lblCompanyNo.TabIndex = 33;
            this.lblCompanyNo.Text = "Company No.:";
            // 
            // lblEnityType
            // 
            this.lblEnityType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblEnityType.AutoSize = true;
            this.lblEnityType.Location = new System.Drawing.Point(4, 224);
            this.lblEnityType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEnityType.Name = "lblEnityType";
            this.lblEnityType.Size = new System.Drawing.Size(91, 20);
            this.lblEnityType.TabIndex = 34;
            this.lblEnityType.Text = "Entity Type:";
            // 
            // textABNNo
            // 
            this.textABNNo.Dock = System.Windows.Forms.DockStyle.Top;
            this.textABNNo.Location = new System.Drawing.Point(148, 179);
            this.textABNNo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textABNNo.Name = "textABNNo";
            this.textABNNo.Size = new System.Drawing.Size(536, 26);
            this.textABNNo.TabIndex = 6;
            // 
            // cmbEntityType
            // 
            this.cmbEntityType.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbEntityType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEntityType.FormattingEnabled = true;
            this.cmbEntityType.Location = new System.Drawing.Point(148, 219);
            this.cmbEntityType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbEntityType.Name = "cmbEntityType";
            this.cmbEntityType.Size = new System.Drawing.Size(536, 28);
            this.cmbEntityType.TabIndex = 7;
            // 
            // lblSystemID
            // 
            this.lblSystemID.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSystemID.AutoSize = true;
            this.lblSystemID.Location = new System.Drawing.Point(4, 13);
            this.lblSystemID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSystemID.Name = "lblSystemID";
            this.lblSystemID.Size = new System.Drawing.Size(87, 20);
            this.lblSystemID.TabIndex = 0;
            this.lblSystemID.Text = "System ID:";
            // 
            // lblCustomerType
            // 
            this.lblCustomerType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblCustomerType.AutoSize = true;
            this.lblCustomerType.Location = new System.Drawing.Point(692, 13);
            this.lblCustomerType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCustomerType.Name = "lblCustomerType";
            this.lblCustomerType.Size = new System.Drawing.Size(120, 20);
            this.lblCustomerType.TabIndex = 19;
            this.lblCustomerType.Text = "Customer Type:";
            // 
            // textDiscountNotes
            // 
            this.textDiscountNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textDiscountNotes.Location = new System.Drawing.Point(883, 299);
            this.textDiscountNotes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textDiscountNotes.Multiline = true;
            this.textDiscountNotes.Name = "textDiscountNotes";
            this.textDiscountNotes.Size = new System.Drawing.Size(536, 30);
            this.textDiscountNotes.TabIndex = 19;
            // 
            // textNotes
            // 
            this.textNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textNotes.Location = new System.Drawing.Point(883, 339);
            this.textNotes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textNotes.Multiline = true;
            this.textNotes.Name = "textNotes";
            this.textNotes.Size = new System.Drawing.Size(536, 35);
            this.textNotes.TabIndex = 20;
            // 
            // lblDepositRequired
            // 
            this.lblDepositRequired.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDepositRequired.AutoSize = true;
            this.lblDepositRequired.Location = new System.Drawing.Point(692, 264);
            this.lblDepositRequired.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDepositRequired.Name = "lblDepositRequired";
            this.lblDepositRequired.Size = new System.Drawing.Size(137, 20);
            this.lblDepositRequired.TabIndex = 29;
            this.lblDepositRequired.Text = "Deposit Required:";
            // 
            // cmbDepositReq
            // 
            this.cmbDepositReq.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbDepositReq.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDepositReq.FormattingEnabled = true;
            this.cmbDepositReq.Location = new System.Drawing.Point(883, 259);
            this.cmbDepositReq.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbDepositReq.Name = "cmbDepositReq";
            this.cmbDepositReq.Size = new System.Drawing.Size(536, 28);
            this.cmbDepositReq.TabIndex = 18;
            // 
            // lblDefaultPort
            // 
            this.lblDefaultPort.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDefaultPort.AutoSize = true;
            this.lblDefaultPort.Location = new System.Drawing.Point(692, 224);
            this.lblDefaultPort.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDefaultPort.Name = "lblDefaultPort";
            this.lblDefaultPort.Size = new System.Drawing.Size(98, 20);
            this.lblDefaultPort.TabIndex = 28;
            this.lblDefaultPort.Text = "Default Port:";
            // 
            // cmbDefPort
            // 
            this.cmbDefPort.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbDefPort.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDefPort.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbDefPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefPort.FormattingEnabled = true;
            this.cmbDefPort.Location = new System.Drawing.Point(883, 219);
            this.cmbDefPort.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbDefPort.Name = "cmbDefPort";
            this.cmbDefPort.Size = new System.Drawing.Size(536, 28);
            this.cmbDefPort.TabIndex = 17;
            // 
            // lblDefaultPaymentTerms
            // 
            this.lblDefaultPaymentTerms.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDefaultPaymentTerms.Location = new System.Drawing.Point(692, 184);
            this.lblDefaultPaymentTerms.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDefaultPaymentTerms.Name = "lblDefaultPaymentTerms";
            this.lblDefaultPaymentTerms.Size = new System.Drawing.Size(183, 20);
            this.lblDefaultPaymentTerms.TabIndex = 21;
            this.lblDefaultPaymentTerms.Text = "Default Payment Terms:";
            // 
            // cmbPaymentTerms
            // 
            this.cmbPaymentTerms.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbPaymentTerms.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbPaymentTerms.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbPaymentTerms.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPaymentTerms.FormattingEnabled = true;
            this.cmbPaymentTerms.Location = new System.Drawing.Point(883, 179);
            this.cmbPaymentTerms.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbPaymentTerms.Name = "cmbPaymentTerms";
            this.cmbPaymentTerms.Size = new System.Drawing.Size(536, 28);
            this.cmbPaymentTerms.TabIndex = 16;
            // 
            // lblDefaultOrderTerms
            // 
            this.lblDefaultOrderTerms.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDefaultOrderTerms.AutoSize = true;
            this.lblDefaultOrderTerms.Location = new System.Drawing.Point(692, 144);
            this.lblDefaultOrderTerms.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDefaultOrderTerms.Name = "lblDefaultOrderTerms";
            this.lblDefaultOrderTerms.Size = new System.Drawing.Size(157, 20);
            this.lblDefaultOrderTerms.TabIndex = 25;
            this.lblDefaultOrderTerms.Text = "Default Order Terms:";
            // 
            // cmbOrderTerms
            // 
            this.cmbOrderTerms.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbOrderTerms.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbOrderTerms.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbOrderTerms.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOrderTerms.FormattingEnabled = true;
            this.cmbOrderTerms.Location = new System.Drawing.Point(883, 139);
            this.cmbOrderTerms.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbOrderTerms.Name = "cmbOrderTerms";
            this.cmbOrderTerms.Size = new System.Drawing.Size(536, 28);
            this.cmbOrderTerms.TabIndex = 15;
            // 
            // lblDefaultCurrency
            // 
            this.lblDefaultCurrency.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDefaultCurrency.AutoSize = true;
            this.lblDefaultCurrency.Location = new System.Drawing.Point(692, 104);
            this.lblDefaultCurrency.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDefaultCurrency.Name = "lblDefaultCurrency";
            this.lblDefaultCurrency.Size = new System.Drawing.Size(132, 20);
            this.lblDefaultCurrency.TabIndex = 20;
            this.lblDefaultCurrency.Text = "Default Currency:";
            // 
            // cmbDefCurrency
            // 
            this.cmbDefCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbDefCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDefCurrency.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbDefCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefCurrency.FormattingEnabled = true;
            this.cmbDefCurrency.Location = new System.Drawing.Point(883, 99);
            this.cmbDefCurrency.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbDefCurrency.Name = "cmbDefCurrency";
            this.cmbDefCurrency.Size = new System.Drawing.Size(536, 28);
            this.cmbDefCurrency.TabIndex = 14;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.cbSplitOnly, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.cbActive, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(879, 54);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(544, 40);
            this.tableLayoutPanel3.TabIndex = 12;
            // 
            // cbSplitOnly
            // 
            this.cbSplitOnly.AutoSize = true;
            this.cbSplitOnly.Location = new System.Drawing.Point(276, 5);
            this.cbSplitOnly.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbSplitOnly.Name = "cbSplitOnly";
            this.cbSplitOnly.Size = new System.Drawing.Size(101, 24);
            this.cbSplitOnly.TabIndex = 13;
            this.cbSplitOnly.Text = "Split Only";
            this.cbSplitOnly.UseVisualStyleBackColor = true;
            this.cbSplitOnly.Visible = false;
            // 
            // cbActive
            // 
            this.cbActive.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbActive.AutoSize = true;
            this.cbActive.Location = new System.Drawing.Point(4, 8);
            this.cbActive.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbActive.Name = "cbActive";
            this.cbActive.Size = new System.Drawing.Size(87, 24);
            this.cbActive.TabIndex = 12;
            this.cbActive.Text = "Active?";
            this.cbActive.UseVisualStyleBackColor = true;
            // 
            // lblDiscountNotes
            // 
            this.lblDiscountNotes.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDiscountNotes.AutoSize = true;
            this.lblDiscountNotes.Location = new System.Drawing.Point(692, 304);
            this.lblDiscountNotes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDiscountNotes.Name = "lblDiscountNotes";
            this.lblDiscountNotes.Size = new System.Drawing.Size(122, 20);
            this.lblDiscountNotes.TabIndex = 28;
            this.lblDiscountNotes.Text = "Discount Notes:";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.cmbCBALockCode, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(144, 294);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(544, 40);
            this.tableLayoutPanel8.TabIndex = 9;
            // 
            // cmbCBALockCode
            // 
            this.cmbCBALockCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCBALockCode.FormattingEnabled = true;
            this.cmbCBALockCode.Location = new System.Drawing.Point(4, 5);
            this.cmbCBALockCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbCBALockCode.Name = "cmbCBALockCode";
            this.cmbCBALockCode.Size = new System.Drawing.Size(254, 28);
            this.cmbCBALockCode.TabIndex = 9;
            // 
            // lblGeneralNotes
            // 
            this.lblGeneralNotes.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblGeneralNotes.AutoSize = true;
            this.lblGeneralNotes.Location = new System.Drawing.Point(692, 346);
            this.lblGeneralNotes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGeneralNotes.Name = "lblGeneralNotes";
            this.lblGeneralNotes.Size = new System.Drawing.Size(116, 20);
            this.lblGeneralNotes.TabIndex = 27;
            this.lblGeneralNotes.Text = "General Notes:";
            // 
            // textTaxNumber
            // 
            this.textTaxNumber.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textTaxNumber.Location = new System.Drawing.Point(147, 343);
            this.textTaxNumber.Name = "textTaxNumber";
            this.textTaxNumber.Size = new System.Drawing.Size(525, 26);
            this.textTaxNumber.TabIndex = 10;
            // 
            // lblTaxNumber
            // 
            this.lblTaxNumber.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTaxNumber.AutoSize = true;
            this.lblTaxNumber.Location = new System.Drawing.Point(3, 350);
            this.lblTaxNumber.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            this.lblTaxNumber.Name = "lblTaxNumber";
            this.lblTaxNumber.Size = new System.Drawing.Size(98, 20);
            this.lblTaxNumber.TabIndex = 36;
            this.lblTaxNumber.Text = "Tax Number:";
            this.lblTaxNumber.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(12, 411);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1431, 337);
            this.tabControl1.TabIndex = 21;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage1.Size = new System.Drawing.Size(1423, 304);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Addresses / Contact";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tlpAddress);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(4, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(1415, 294);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Address Information";
            // 
            // tlpAddress
            // 
            this.tlpAddress.ColumnCount = 4;
            this.tlpAddress.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 132F));
            this.tlpAddress.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpAddress.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 158F));
            this.tlpAddress.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpAddress.Controls.Add(this.textAddrLabel, 3, 6);
            this.tlpAddress.Controls.Add(this.lblDCAddressLabel, 2, 6);
            this.tlpAddress.Controls.Add(this.lblStreet, 0, 1);
            this.tlpAddress.Controls.Add(this.lblsuburbCity, 0, 2);
            this.tlpAddress.Controls.Add(this.lblAddress3, 0, 3);
            this.tlpAddress.Controls.Add(this.textMainAddr1, 1, 1);
            this.tlpAddress.Controls.Add(this.textMainAddr2, 1, 2);
            this.tlpAddress.Controls.Add(this.textMainAddr3, 1, 3);
            this.tlpAddress.Controls.Add(this.lblState, 0, 4);
            this.tlpAddress.Controls.Add(this.lblPostCode, 0, 5);
            this.tlpAddress.Controls.Add(this.lblCoutry, 0, 6);
            this.tlpAddress.Controls.Add(this.textMainAddrState, 1, 4);
            this.tlpAddress.Controls.Add(this.textMainAddrPostCode, 1, 5);
            this.tlpAddress.Controls.Add(this.cmbMainAddrCountry, 1, 6);
            this.tlpAddress.Controls.Add(this.lblPhonenumber, 2, 1);
            this.tlpAddress.Controls.Add(this.lblFaxNumber, 2, 2);
            this.tlpAddress.Controls.Add(this.lblEmailAddress, 2, 3);
            this.tlpAddress.Controls.Add(this.tlpDefaultDelAddr, 2, 4);
            this.tlpAddress.Controls.Add(this.textMainAddrPhone, 3, 1);
            this.tlpAddress.Controls.Add(this.textMainAddrFax, 3, 2);
            this.tlpAddress.Controls.Add(this.textMainAddrEmail, 3, 3);
            this.tlpAddress.Controls.Add(this.tableLayoutPanel7, 2, 5);
            this.tlpAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpAddress.Location = new System.Drawing.Point(4, 24);
            this.tlpAddress.Margin = new System.Windows.Forms.Padding(0);
            this.tlpAddress.Name = "tlpAddress";
            this.tlpAddress.RowCount = 8;
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAddress.Size = new System.Drawing.Size(1407, 265);
            this.tlpAddress.TabIndex = 11;
            // 
            // textAddrLabel
            // 
            this.textAddrLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.textAddrLabel.Location = new System.Drawing.Point(852, 223);
            this.textAddrLabel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textAddrLabel.Name = "textAddrLabel";
            this.textAddrLabel.Size = new System.Drawing.Size(551, 26);
            this.textAddrLabel.TabIndex = 34;
            // 
            // lblDCAddressLabel
            // 
            this.lblDCAddressLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDCAddressLabel.AutoSize = true;
            this.lblDCAddressLabel.Location = new System.Drawing.Point(694, 232);
            this.lblDCAddressLabel.Margin = new System.Windows.Forms.Padding(4, 8, 4, 0);
            this.lblDCAddressLabel.Name = "lblDCAddressLabel";
            this.lblDCAddressLabel.Size = new System.Drawing.Size(150, 20);
            this.lblDCAddressLabel.TabIndex = 69;
            this.lblDCAddressLabel.Text = "DC / Address Label:";
            // 
            // lblStreet
            // 
            this.lblStreet.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStreet.AutoSize = true;
            this.lblStreet.Location = new System.Drawing.Point(4, 18);
            this.lblStreet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStreet.Name = "lblStreet";
            this.lblStreet.Size = new System.Drawing.Size(57, 20);
            this.lblStreet.TabIndex = 0;
            this.lblStreet.Text = "Street:";
            // 
            // lblsuburbCity
            // 
            this.lblsuburbCity.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblsuburbCity.AutoSize = true;
            this.lblsuburbCity.Location = new System.Drawing.Point(4, 58);
            this.lblsuburbCity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblsuburbCity.Name = "lblsuburbCity";
            this.lblsuburbCity.Size = new System.Drawing.Size(103, 20);
            this.lblsuburbCity.TabIndex = 1;
            this.lblsuburbCity.Text = "Suburb / City:";
            // 
            // lblAddress3
            // 
            this.lblAddress3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblAddress3.AutoSize = true;
            this.lblAddress3.Location = new System.Drawing.Point(4, 98);
            this.lblAddress3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddress3.Name = "lblAddress3";
            this.lblAddress3.Size = new System.Drawing.Size(85, 20);
            this.lblAddress3.TabIndex = 2;
            this.lblAddress3.Text = "Address 3:";
            // 
            // textMainAddr1
            // 
            this.textMainAddr1.Dock = System.Windows.Forms.DockStyle.Top;
            this.textMainAddr1.Location = new System.Drawing.Point(136, 13);
            this.textMainAddr1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textMainAddr1.Name = "textMainAddr1";
            this.textMainAddr1.Size = new System.Drawing.Size(550, 26);
            this.textMainAddr1.TabIndex = 21;
            // 
            // textMainAddr2
            // 
            this.textMainAddr2.Dock = System.Windows.Forms.DockStyle.Top;
            this.textMainAddr2.Location = new System.Drawing.Point(136, 53);
            this.textMainAddr2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textMainAddr2.Name = "textMainAddr2";
            this.textMainAddr2.Size = new System.Drawing.Size(550, 26);
            this.textMainAddr2.TabIndex = 22;
            // 
            // textMainAddr3
            // 
            this.textMainAddr3.Dock = System.Windows.Forms.DockStyle.Top;
            this.textMainAddr3.Location = new System.Drawing.Point(136, 93);
            this.textMainAddr3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textMainAddr3.Name = "textMainAddr3";
            this.textMainAddr3.Size = new System.Drawing.Size(550, 26);
            this.textMainAddr3.TabIndex = 23;
            // 
            // lblState
            // 
            this.lblState.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(4, 140);
            this.lblState.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(52, 20);
            this.lblState.TabIndex = 6;
            this.lblState.Text = "State:";
            // 
            // lblPostCode
            // 
            this.lblPostCode.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblPostCode.AutoSize = true;
            this.lblPostCode.Location = new System.Drawing.Point(4, 185);
            this.lblPostCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPostCode.Name = "lblPostCode";
            this.lblPostCode.Size = new System.Drawing.Size(87, 20);
            this.lblPostCode.TabIndex = 7;
            this.lblPostCode.Text = "Post Code:";
            // 
            // lblCoutry
            // 
            this.lblCoutry.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblCoutry.AutoSize = true;
            this.lblCoutry.Location = new System.Drawing.Point(4, 228);
            this.lblCoutry.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCoutry.Name = "lblCoutry";
            this.lblCoutry.Size = new System.Drawing.Size(68, 20);
            this.lblCoutry.TabIndex = 8;
            this.lblCoutry.Text = "Country:";
            // 
            // textMainAddrState
            // 
            this.textMainAddrState.Location = new System.Drawing.Point(136, 133);
            this.textMainAddrState.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textMainAddrState.Name = "textMainAddrState";
            this.textMainAddrState.Size = new System.Drawing.Size(252, 26);
            this.textMainAddrState.TabIndex = 24;
            // 
            // textMainAddrPostCode
            // 
            this.textMainAddrPostCode.Location = new System.Drawing.Point(136, 178);
            this.textMainAddrPostCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textMainAddrPostCode.Name = "textMainAddrPostCode";
            this.textMainAddrPostCode.Size = new System.Drawing.Size(214, 26);
            this.textMainAddrPostCode.TabIndex = 25;
            // 
            // cmbMainAddrCountry
            // 
            this.cmbMainAddrCountry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbMainAddrCountry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbMainAddrCountry.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbMainAddrCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMainAddrCountry.FormattingEnabled = true;
            this.cmbMainAddrCountry.Location = new System.Drawing.Point(136, 223);
            this.cmbMainAddrCountry.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbMainAddrCountry.Name = "cmbMainAddrCountry";
            this.cmbMainAddrCountry.Size = new System.Drawing.Size(550, 28);
            this.cmbMainAddrCountry.TabIndex = 26;
            // 
            // lblPhonenumber
            // 
            this.lblPhonenumber.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblPhonenumber.AutoSize = true;
            this.lblPhonenumber.Location = new System.Drawing.Point(694, 18);
            this.lblPhonenumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPhonenumber.Name = "lblPhonenumber";
            this.lblPhonenumber.Size = new System.Drawing.Size(119, 20);
            this.lblPhonenumber.TabIndex = 12;
            this.lblPhonenumber.Text = "Phone Number:";
            // 
            // lblFaxNumber
            // 
            this.lblFaxNumber.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblFaxNumber.AutoSize = true;
            this.lblFaxNumber.Location = new System.Drawing.Point(694, 58);
            this.lblFaxNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFaxNumber.Name = "lblFaxNumber";
            this.lblFaxNumber.Size = new System.Drawing.Size(99, 20);
            this.lblFaxNumber.TabIndex = 13;
            this.lblFaxNumber.Text = "Fax Number:";
            // 
            // lblEmailAddress
            // 
            this.lblEmailAddress.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblEmailAddress.AutoSize = true;
            this.lblEmailAddress.Location = new System.Drawing.Point(694, 98);
            this.lblEmailAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmailAddress.Name = "lblEmailAddress";
            this.lblEmailAddress.Size = new System.Drawing.Size(115, 20);
            this.lblEmailAddress.TabIndex = 14;
            this.lblEmailAddress.Text = "Email Address:";
            // 
            // tlpDefaultDelAddr
            // 
            this.tlpDefaultDelAddr.ColumnCount = 3;
            this.tlpAddress.SetColumnSpan(this.tlpDefaultDelAddr, 2);
            this.tlpDefaultDelAddr.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 158F));
            this.tlpDefaultDelAddr.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDefaultDelAddr.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDefaultDelAddr.Controls.Add(this.pbAddresses, 2, 0);
            this.tlpDefaultDelAddr.Controls.Add(this.cbIsDel, 1, 0);
            this.tlpDefaultDelAddr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpDefaultDelAddr.Location = new System.Drawing.Point(690, 128);
            this.tlpDefaultDelAddr.Margin = new System.Windows.Forms.Padding(0);
            this.tlpDefaultDelAddr.Name = "tlpDefaultDelAddr";
            this.tlpDefaultDelAddr.RowCount = 1;
            this.tlpDefaultDelAddr.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDefaultDelAddr.Size = new System.Drawing.Size(717, 45);
            this.tlpDefaultDelAddr.TabIndex = 30;
            // 
            // pbAddresses
            // 
            this.pbAddresses.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbAddresses.ImageKey = "Address32.png";
            this.pbAddresses.ImageList = this.imageListActions;
            this.pbAddresses.Location = new System.Drawing.Point(441, 5);
            this.pbAddresses.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbAddresses.Name = "pbAddresses";
            this.pbAddresses.Size = new System.Drawing.Size(272, 35);
            this.pbAddresses.TabIndex = 31;
            this.pbAddresses.Text = "View All Addresses...";
            this.pbAddresses.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbAddresses.UseVisualStyleBackColor = true;
            this.pbAddresses.Click += new System.EventHandler(this.pbAddresses_Click);
            // 
            // cbIsDel
            // 
            this.cbIsDel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbIsDel.AutoSize = true;
            this.cbIsDel.Location = new System.Drawing.Point(162, 10);
            this.cbIsDel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbIsDel.Name = "cbIsDel";
            this.cbIsDel.Size = new System.Drawing.Size(209, 24);
            this.cbIsDel.TabIndex = 30;
            this.cbIsDel.Text = "Default Delivery Address";
            this.cbIsDel.UseVisualStyleBackColor = true;
            // 
            // textMainAddrPhone
            // 
            this.textMainAddrPhone.Dock = System.Windows.Forms.DockStyle.Top;
            this.textMainAddrPhone.Location = new System.Drawing.Point(852, 13);
            this.textMainAddrPhone.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textMainAddrPhone.Name = "textMainAddrPhone";
            this.textMainAddrPhone.Size = new System.Drawing.Size(551, 26);
            this.textMainAddrPhone.TabIndex = 27;
            // 
            // textMainAddrFax
            // 
            this.textMainAddrFax.Dock = System.Windows.Forms.DockStyle.Top;
            this.textMainAddrFax.Location = new System.Drawing.Point(852, 53);
            this.textMainAddrFax.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textMainAddrFax.Name = "textMainAddrFax";
            this.textMainAddrFax.Size = new System.Drawing.Size(551, 26);
            this.textMainAddrFax.TabIndex = 28;
            // 
            // textMainAddrEmail
            // 
            this.textMainAddrEmail.Dock = System.Windows.Forms.DockStyle.Top;
            this.textMainAddrEmail.Location = new System.Drawing.Point(852, 93);
            this.textMainAddrEmail.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textMainAddrEmail.Name = "textMainAddrEmail";
            this.textMainAddrEmail.Size = new System.Drawing.Size(551, 26);
            this.textMainAddrEmail.TabIndex = 29;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 3;
            this.tlpAddress.SetColumnSpan(this.tableLayoutPanel7, 2);
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 158F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.pbContacts, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.lblPrimaryContact, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.cmbPrimaryContactNo, 1, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(690, 173);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(717, 45);
            this.tableLayoutPanel7.TabIndex = 32;
            // 
            // pbContacts
            // 
            this.pbContacts.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbContacts.ImageKey = "Contact32.png";
            this.pbContacts.ImageList = this.imageListActions;
            this.pbContacts.Location = new System.Drawing.Point(441, 5);
            this.pbContacts.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbContacts.Name = "pbContacts";
            this.pbContacts.Size = new System.Drawing.Size(272, 35);
            this.pbContacts.TabIndex = 33;
            this.pbContacts.Text = "View All Contacts...";
            this.pbContacts.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbContacts.UseVisualStyleBackColor = true;
            this.pbContacts.Click += new System.EventHandler(this.pbContacts_Click);
            // 
            // lblPrimaryContact
            // 
            this.lblPrimaryContact.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblPrimaryContact.AutoSize = true;
            this.lblPrimaryContact.Location = new System.Drawing.Point(4, 16);
            this.lblPrimaryContact.Margin = new System.Windows.Forms.Padding(4, 8, 4, 0);
            this.lblPrimaryContact.Name = "lblPrimaryContact";
            this.lblPrimaryContact.Size = new System.Drawing.Size(125, 20);
            this.lblPrimaryContact.TabIndex = 8;
            this.lblPrimaryContact.Text = "Primary Contact:";
            // 
            // cmbPrimaryContactNo
            // 
            this.cmbPrimaryContactNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPrimaryContactNo.FormattingEnabled = true;
            this.cmbPrimaryContactNo.Location = new System.Drawing.Point(162, 5);
            this.cmbPrimaryContactNo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbPrimaryContactNo.Name = "cmbPrimaryContactNo";
            this.cmbPrimaryContactNo.Size = new System.Drawing.Size(260, 28);
            this.cmbPrimaryContactNo.TabIndex = 32;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox5);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage3.Size = new System.Drawing.Size(1423, 304);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Transaction Defaults";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tableLayoutPanel2);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(4, 5);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox5.Size = new System.Drawing.Size(1415, 294);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Additional";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 6;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 159F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel2.Controls.Add(this.cmbWeightConv, 3, 6);
            this.tableLayoutPanel2.Controls.Add(this.cmbVolumeConv, 3, 5);
            this.tableLayoutPanel2.Controls.Add(this.lblDefQuoteRebate, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblDefQuoteDisc, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblDefaultSlushie, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblShippingMarks, 4, 5);
            this.tableLayoutPanel2.Controls.Add(this.textCBAShippingMarks, 5, 5);
            this.tableLayoutPanel2.Controls.Add(this.lblRestrictions, 4, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblOrderAlert, 4, 3);
            this.tableLayoutPanel2.Controls.Add(this.textCBAOrderAlert, 5, 3);
            this.tableLayoutPanel2.Controls.Add(this.textCBARestrictions, 5, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblQuoteCurrency, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblInvoiceType, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblinvoiceProformaType, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.cmbCBAQCurrency, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.cmbCBADefInvType, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.cmbCBADefPIType, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.lblProductProfileID, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel10, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel11, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel12, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblVolumePref, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.lblWeightPref, 2, 6);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(4, 24);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 9;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1407, 265);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // cmbWeightConv
            // 
            this.cmbWeightConv.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbWeightConv.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWeightConv.FormattingEnabled = true;
            this.cmbWeightConv.Location = new System.Drawing.Point(635, 213);
            this.cmbWeightConv.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbWeightConv.Name = "cmbWeightConv";
            this.cmbWeightConv.Size = new System.Drawing.Size(304, 28);
            this.cmbWeightConv.TabIndex = 41;
            this.cmbWeightConv.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbWeightConv_KeyDown);
            // 
            // cmbVolumeConv
            // 
            this.cmbVolumeConv.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbVolumeConv.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVolumeConv.FormattingEnabled = true;
            this.cmbVolumeConv.Location = new System.Drawing.Point(635, 173);
            this.cmbVolumeConv.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbVolumeConv.Name = "cmbVolumeConv";
            this.cmbVolumeConv.Size = new System.Drawing.Size(304, 28);
            this.cmbVolumeConv.TabIndex = 40;
            this.cmbVolumeConv.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbVolumeConv_KeyDown);
            // 
            // lblDefQuoteRebate
            // 
            this.lblDefQuoteRebate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDefQuoteRebate.AutoSize = true;
            this.lblDefQuoteRebate.Location = new System.Drawing.Point(4, 18);
            this.lblDefQuoteRebate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDefQuoteRebate.Name = "lblDefQuoteRebate";
            this.lblDefQuoteRebate.Size = new System.Drawing.Size(144, 20);
            this.lblDefQuoteRebate.TabIndex = 0;
            this.lblDefQuoteRebate.Text = "Def Quote Rebate:";
            // 
            // lblDefQuoteDisc
            // 
            this.lblDefQuoteDisc.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDefQuoteDisc.AutoSize = true;
            this.lblDefQuoteDisc.Location = new System.Drawing.Point(4, 58);
            this.lblDefQuoteDisc.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDefQuoteDisc.Name = "lblDefQuoteDisc";
            this.lblDefQuoteDisc.Size = new System.Drawing.Size(126, 20);
            this.lblDefQuoteDisc.TabIndex = 1;
            this.lblDefQuoteDisc.Text = "Def Quote Disc.:";
            // 
            // lblDefaultSlushie
            // 
            this.lblDefaultSlushie.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDefaultSlushie.AutoSize = true;
            this.lblDefaultSlushie.Location = new System.Drawing.Point(4, 98);
            this.lblDefaultSlushie.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDefaultSlushie.Name = "lblDefaultSlushie";
            this.lblDefaultSlushie.Size = new System.Drawing.Size(121, 20);
            this.lblDefaultSlushie.TabIndex = 2;
            this.lblDefaultSlushie.Text = "Default Slushie:";
            // 
            // lblShippingMarks
            // 
            this.lblShippingMarks.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblShippingMarks.AutoSize = true;
            this.lblShippingMarks.Location = new System.Drawing.Point(947, 182);
            this.lblShippingMarks.Margin = new System.Windows.Forms.Padding(4, 9, 4, 0);
            this.lblShippingMarks.Name = "lblShippingMarks";
            this.lblShippingMarks.Size = new System.Drawing.Size(122, 20);
            this.lblShippingMarks.TabIndex = 12;
            this.lblShippingMarks.Text = "Shipping Marks:";
            // 
            // textCBAShippingMarks
            // 
            this.textCBAShippingMarks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textCBAShippingMarks.Location = new System.Drawing.Point(1097, 173);
            this.textCBAShippingMarks.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textCBAShippingMarks.Multiline = true;
            this.textCBAShippingMarks.Name = "textCBAShippingMarks";
            this.tableLayoutPanel2.SetRowSpan(this.textCBAShippingMarks, 3);
            this.textCBAShippingMarks.Size = new System.Drawing.Size(306, 79);
            this.textCBAShippingMarks.TabIndex = 44;
            // 
            // lblRestrictions
            // 
            this.lblRestrictions.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRestrictions.AutoSize = true;
            this.lblRestrictions.Location = new System.Drawing.Point(947, 22);
            this.lblRestrictions.Margin = new System.Windows.Forms.Padding(4, 9, 4, 0);
            this.lblRestrictions.Name = "lblRestrictions";
            this.lblRestrictions.Size = new System.Drawing.Size(97, 20);
            this.lblRestrictions.TabIndex = 10;
            this.lblRestrictions.Text = "Restrictions:";
            // 
            // lblOrderAlert
            // 
            this.lblOrderAlert.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblOrderAlert.AutoSize = true;
            this.lblOrderAlert.Location = new System.Drawing.Point(947, 102);
            this.lblOrderAlert.Margin = new System.Windows.Forms.Padding(4, 9, 4, 0);
            this.lblOrderAlert.Name = "lblOrderAlert";
            this.lblOrderAlert.Size = new System.Drawing.Size(90, 20);
            this.lblOrderAlert.TabIndex = 11;
            this.lblOrderAlert.Text = "Order Alert:";
            // 
            // textCBAOrderAlert
            // 
            this.textCBAOrderAlert.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textCBAOrderAlert.Location = new System.Drawing.Point(1097, 93);
            this.textCBAOrderAlert.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textCBAOrderAlert.Multiline = true;
            this.textCBAOrderAlert.Name = "textCBAOrderAlert";
            this.tableLayoutPanel2.SetRowSpan(this.textCBAOrderAlert, 2);
            this.textCBAOrderAlert.Size = new System.Drawing.Size(306, 70);
            this.textCBAOrderAlert.TabIndex = 43;
            // 
            // textCBARestrictions
            // 
            this.textCBARestrictions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textCBARestrictions.Location = new System.Drawing.Point(1097, 13);
            this.textCBARestrictions.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textCBARestrictions.Multiline = true;
            this.textCBARestrictions.Name = "textCBARestrictions";
            this.tableLayoutPanel2.SetRowSpan(this.textCBARestrictions, 2);
            this.textCBARestrictions.Size = new System.Drawing.Size(306, 70);
            this.textCBARestrictions.TabIndex = 42;
            // 
            // lblQuoteCurrency
            // 
            this.lblQuoteCurrency.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblQuoteCurrency.AutoSize = true;
            this.lblQuoteCurrency.Location = new System.Drawing.Point(485, 18);
            this.lblQuoteCurrency.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblQuoteCurrency.Name = "lblQuoteCurrency";
            this.lblQuoteCurrency.Size = new System.Drawing.Size(124, 20);
            this.lblQuoteCurrency.TabIndex = 5;
            this.lblQuoteCurrency.Text = "Quote Currency:";
            this.lblQuoteCurrency.Visible = false;
            // 
            // lblInvoiceType
            // 
            this.lblInvoiceType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblInvoiceType.AutoSize = true;
            this.lblInvoiceType.Location = new System.Drawing.Point(485, 58);
            this.lblInvoiceType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInvoiceType.Name = "lblInvoiceType";
            this.lblInvoiceType.Size = new System.Drawing.Size(101, 20);
            this.lblInvoiceType.TabIndex = 6;
            this.lblInvoiceType.Text = "Invoice Type:";
            // 
            // lblinvoiceProformaType
            // 
            this.lblinvoiceProformaType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblinvoiceProformaType.AutoSize = true;
            this.lblinvoiceProformaType.Location = new System.Drawing.Point(485, 98);
            this.lblinvoiceProformaType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblinvoiceProformaType.Name = "lblinvoiceProformaType";
            this.lblinvoiceProformaType.Size = new System.Drawing.Size(116, 20);
            this.lblinvoiceProformaType.TabIndex = 7;
            this.lblinvoiceProformaType.Text = "Proforma Type:";
            // 
            // cmbCBAQCurrency
            // 
            this.cmbCBAQCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCBAQCurrency.FormattingEnabled = true;
            this.cmbCBAQCurrency.Location = new System.Drawing.Point(635, 13);
            this.cmbCBAQCurrency.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbCBAQCurrency.Name = "cmbCBAQCurrency";
            this.cmbCBAQCurrency.Size = new System.Drawing.Size(301, 28);
            this.cmbCBAQCurrency.TabIndex = 37;
            this.cmbCBAQCurrency.Visible = false;
            // 
            // cmbCBADefInvType
            // 
            this.cmbCBADefInvType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCBADefInvType.FormattingEnabled = true;
            this.cmbCBADefInvType.Location = new System.Drawing.Point(635, 53);
            this.cmbCBADefInvType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbCBADefInvType.Name = "cmbCBADefInvType";
            this.cmbCBADefInvType.Size = new System.Drawing.Size(301, 28);
            this.cmbCBADefInvType.TabIndex = 38;
            // 
            // cmbCBADefPIType
            // 
            this.cmbCBADefPIType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCBADefPIType.FormattingEnabled = true;
            this.cmbCBADefPIType.Location = new System.Drawing.Point(635, 93);
            this.cmbCBADefPIType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbCBADefPIType.Name = "cmbCBADefPIType";
            this.cmbCBADefPIType.Size = new System.Drawing.Size(301, 28);
            this.cmbCBADefPIType.TabIndex = 39;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel4, 2);
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel4.Controls.Add(this.pbProductProfileIdSearch, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.textProductProfileId, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.textProductProfileName, 2, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(159, 128);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(472, 40);
            this.tableLayoutPanel4.TabIndex = 36;
            // 
            // pbProductProfileIdSearch
            // 
            this.pbProductProfileIdSearch.ImageKey = "Find32.png";
            this.pbProductProfileIdSearch.ImageList = this.imageListActions;
            this.pbProductProfileIdSearch.Location = new System.Drawing.Point(174, 5);
            this.pbProductProfileIdSearch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbProductProfileIdSearch.Name = "pbProductProfileIdSearch";
            this.pbProductProfileIdSearch.Size = new System.Drawing.Size(28, 30);
            this.pbProductProfileIdSearch.TabIndex = 18;
            this.pbProductProfileIdSearch.TabStop = false;
            this.pbProductProfileIdSearch.UseVisualStyleBackColor = true;
            this.pbProductProfileIdSearch.Click += new System.EventHandler(this.pbProductProfileIdSearch_Click);
            // 
            // textProductProfileId
            // 
            this.textProductProfileId.Dock = System.Windows.Forms.DockStyle.Top;
            this.textProductProfileId.Location = new System.Drawing.Point(4, 5);
            this.textProductProfileId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textProductProfileId.Name = "textProductProfileId";
            this.textProductProfileId.Size = new System.Drawing.Size(162, 26);
            this.textProductProfileId.TabIndex = 36;
            this.textProductProfileId.Validating += new System.ComponentModel.CancelEventHandler(this.textProductProfileId_Validating);
            // 
            // textProductProfileName
            // 
            this.textProductProfileName.Dock = System.Windows.Forms.DockStyle.Top;
            this.textProductProfileName.Location = new System.Drawing.Point(219, 5);
            this.textProductProfileName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textProductProfileName.Name = "textProductProfileName";
            this.textProductProfileName.ReadOnly = true;
            this.textProductProfileName.Size = new System.Drawing.Size(249, 26);
            this.textProductProfileName.TabIndex = 20;
            this.textProductProfileName.TabStop = false;
            // 
            // lblProductProfileID
            // 
            this.lblProductProfileID.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblProductProfileID.AutoSize = true;
            this.lblProductProfileID.Location = new System.Drawing.Point(4, 138);
            this.lblProductProfileID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblProductProfileID.Name = "lblProductProfileID";
            this.lblProductProfileID.Size = new System.Drawing.Size(137, 20);
            this.lblProductProfileID.TabIndex = 6;
            this.lblProductProfileID.Text = "Product Profile ID:";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Controls.Add(this.textBoxDefaultQuoteRebate, 0, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(159, 8);
            this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(322, 40);
            this.tableLayoutPanel10.TabIndex = 33;
            // 
            // textBoxDefaultQuoteRebate
            // 
            this.textBoxDefaultQuoteRebate.DisplayFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.Percent;
            this.textBoxDefaultQuoteRebate.DisplayFormat.Inherit = ((C1.Win.C1Input.FormatInfoInheritFlags)(((((C1.Win.C1Input.FormatInfoInheritFlags.CustomFormat | C1.Win.C1Input.FormatInfoInheritFlags.NullText) 
            | C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd)));
            this.textBoxDefaultQuoteRebate.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBoxDefaultQuoteRebate.EditFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.Percent;
            this.textBoxDefaultQuoteRebate.EditFormat.Inherit = ((C1.Win.C1Input.FormatInfoInheritFlags)(((((C1.Win.C1Input.FormatInfoInheritFlags.CustomFormat | C1.Win.C1Input.FormatInfoInheritFlags.NullText) 
            | C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd)));
            this.textBoxDefaultQuoteRebate.Enabled = false;
            this.textBoxDefaultQuoteRebate.ImagePadding = new System.Windows.Forms.Padding(0);
            this.textBoxDefaultQuoteRebate.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.textBoxDefaultQuoteRebate.Location = new System.Drawing.Point(4, 5);
            this.textBoxDefaultQuoteRebate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBoxDefaultQuoteRebate.Name = "textBoxDefaultQuoteRebate";
            this.textBoxDefaultQuoteRebate.Size = new System.Drawing.Size(153, 24);
            this.textBoxDefaultQuoteRebate.TabIndex = 33;
            this.textBoxDefaultQuoteRebate.Tag = null;
            this.textBoxDefaultQuoteRebate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxDefaultQuoteRebate.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Controls.Add(this.textBoxDefaultQuoteDiscount, 0, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(159, 48);
            this.tableLayoutPanel11.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(322, 40);
            this.tableLayoutPanel11.TabIndex = 34;
            // 
            // textBoxDefaultQuoteDiscount
            // 
            this.textBoxDefaultQuoteDiscount.DisplayFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.Percent;
            this.textBoxDefaultQuoteDiscount.DisplayFormat.Inherit = ((C1.Win.C1Input.FormatInfoInheritFlags)(((((C1.Win.C1Input.FormatInfoInheritFlags.CustomFormat | C1.Win.C1Input.FormatInfoInheritFlags.NullText) 
            | C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd)));
            this.textBoxDefaultQuoteDiscount.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBoxDefaultQuoteDiscount.EditFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.Percent;
            this.textBoxDefaultQuoteDiscount.EditFormat.Inherit = ((C1.Win.C1Input.FormatInfoInheritFlags)(((((C1.Win.C1Input.FormatInfoInheritFlags.CustomFormat | C1.Win.C1Input.FormatInfoInheritFlags.NullText) 
            | C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd)));
            this.textBoxDefaultQuoteDiscount.Enabled = false;
            this.textBoxDefaultQuoteDiscount.ImagePadding = new System.Windows.Forms.Padding(0);
            this.textBoxDefaultQuoteDiscount.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.textBoxDefaultQuoteDiscount.Location = new System.Drawing.Point(4, 5);
            this.textBoxDefaultQuoteDiscount.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBoxDefaultQuoteDiscount.Name = "textBoxDefaultQuoteDiscount";
            this.textBoxDefaultQuoteDiscount.Size = new System.Drawing.Size(153, 24);
            this.textBoxDefaultQuoteDiscount.TabIndex = 34;
            this.textBoxDefaultQuoteDiscount.Tag = null;
            this.textBoxDefaultQuoteDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxDefaultQuoteDiscount.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Controls.Add(this.textBoxDefaultSlushie, 0, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(159, 88);
            this.tableLayoutPanel12.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(322, 40);
            this.tableLayoutPanel12.TabIndex = 35;
            // 
            // textBoxDefaultSlushie
            // 
            this.textBoxDefaultSlushie.DisplayFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.Percent;
            this.textBoxDefaultSlushie.DisplayFormat.Inherit = ((C1.Win.C1Input.FormatInfoInheritFlags)(((((C1.Win.C1Input.FormatInfoInheritFlags.CustomFormat | C1.Win.C1Input.FormatInfoInheritFlags.NullText) 
            | C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd)));
            this.textBoxDefaultSlushie.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBoxDefaultSlushie.EditFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.Percent;
            this.textBoxDefaultSlushie.EditFormat.Inherit = ((C1.Win.C1Input.FormatInfoInheritFlags)(((((C1.Win.C1Input.FormatInfoInheritFlags.CustomFormat | C1.Win.C1Input.FormatInfoInheritFlags.NullText) 
            | C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd)));
            this.textBoxDefaultSlushie.Enabled = false;
            this.textBoxDefaultSlushie.ImagePadding = new System.Windows.Forms.Padding(0);
            this.textBoxDefaultSlushie.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.textBoxDefaultSlushie.Location = new System.Drawing.Point(4, 5);
            this.textBoxDefaultSlushie.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBoxDefaultSlushie.Name = "textBoxDefaultSlushie";
            this.textBoxDefaultSlushie.Size = new System.Drawing.Size(153, 24);
            this.textBoxDefaultSlushie.TabIndex = 35;
            this.textBoxDefaultSlushie.Tag = null;
            this.textBoxDefaultSlushie.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxDefaultSlushie.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // lblVolumePref
            // 
            this.lblVolumePref.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblVolumePref.AutoSize = true;
            this.lblVolumePref.Location = new System.Drawing.Point(484, 178);
            this.lblVolumePref.Name = "lblVolumePref";
            this.lblVolumePref.Size = new System.Drawing.Size(104, 20);
            this.lblVolumePref.TabIndex = 43;
            this.lblVolumePref.Text = "Volume Pref.:";
            // 
            // lblWeightPref
            // 
            this.lblWeightPref.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblWeightPref.AutoSize = true;
            this.lblWeightPref.Location = new System.Drawing.Point(484, 218);
            this.lblWeightPref.Name = "lblWeightPref";
            this.lblWeightPref.Size = new System.Drawing.Size(100, 20);
            this.lblWeightPref.TabIndex = 44;
            this.lblWeightPref.Text = "Weight Pref.:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage2.Size = new System.Drawing.Size(1423, 304);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Legacy Integration";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tableLayoutPanel1);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(4, 5);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox4.Size = new System.Drawing.Size(1415, 294);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "CBA Integration";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel1.Controls.Add(this.lblTerms, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblBranch, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblsalesman, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblPrieCode, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblInvoiceDiscount, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.textCBATerms, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.textCBABranch, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.textCBASalesman, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.textCBAPriceCode, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.textCBAInvoiceDiscount, 3, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 24);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1407, 265);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblTerms
            // 
            this.lblTerms.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTerms.AutoSize = true;
            this.lblTerms.Location = new System.Drawing.Point(4, 18);
            this.lblTerms.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTerms.Name = "lblTerms";
            this.lblTerms.Size = new System.Drawing.Size(57, 20);
            this.lblTerms.TabIndex = 0;
            this.lblTerms.Text = "Terms:";
            // 
            // lblBranch
            // 
            this.lblBranch.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblBranch.AutoSize = true;
            this.lblBranch.Location = new System.Drawing.Point(4, 58);
            this.lblBranch.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBranch.Name = "lblBranch";
            this.lblBranch.Size = new System.Drawing.Size(64, 20);
            this.lblBranch.TabIndex = 1;
            this.lblBranch.Text = "Branch:";
            // 
            // lblsalesman
            // 
            this.lblsalesman.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblsalesman.AutoSize = true;
            this.lblsalesman.Location = new System.Drawing.Point(4, 98);
            this.lblsalesman.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblsalesman.Name = "lblsalesman";
            this.lblsalesman.Size = new System.Drawing.Size(84, 20);
            this.lblsalesman.TabIndex = 2;
            this.lblsalesman.Text = "Salesman:";
            // 
            // lblPrieCode
            // 
            this.lblPrieCode.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblPrieCode.AutoSize = true;
            this.lblPrieCode.Location = new System.Drawing.Point(4, 138);
            this.lblPrieCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPrieCode.Name = "lblPrieCode";
            this.lblPrieCode.Size = new System.Drawing.Size(90, 20);
            this.lblPrieCode.TabIndex = 3;
            this.lblPrieCode.Text = "Price Code:";
            // 
            // lblInvoiceDiscount
            // 
            this.lblInvoiceDiscount.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblInvoiceDiscount.AutoSize = true;
            this.lblInvoiceDiscount.Location = new System.Drawing.Point(479, 18);
            this.lblInvoiceDiscount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInvoiceDiscount.Name = "lblInvoiceDiscount";
            this.lblInvoiceDiscount.Size = new System.Drawing.Size(130, 20);
            this.lblInvoiceDiscount.TabIndex = 4;
            this.lblInvoiceDiscount.Text = "Invoice Discount:";
            // 
            // textCBATerms
            // 
            this.textCBATerms.Dock = System.Windows.Forms.DockStyle.Top;
            this.textCBATerms.Location = new System.Drawing.Point(154, 13);
            this.textCBATerms.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textCBATerms.Name = "textCBATerms";
            this.textCBATerms.Size = new System.Drawing.Size(317, 26);
            this.textCBATerms.TabIndex = 43;
            // 
            // textCBABranch
            // 
            this.textCBABranch.Dock = System.Windows.Forms.DockStyle.Top;
            this.textCBABranch.Location = new System.Drawing.Point(154, 53);
            this.textCBABranch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textCBABranch.Name = "textCBABranch";
            this.textCBABranch.Size = new System.Drawing.Size(317, 26);
            this.textCBABranch.TabIndex = 44;
            // 
            // textCBASalesman
            // 
            this.textCBASalesman.Dock = System.Windows.Forms.DockStyle.Top;
            this.textCBASalesman.Location = new System.Drawing.Point(154, 93);
            this.textCBASalesman.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textCBASalesman.Name = "textCBASalesman";
            this.textCBASalesman.Size = new System.Drawing.Size(317, 26);
            this.textCBASalesman.TabIndex = 45;
            // 
            // textCBAPriceCode
            // 
            this.textCBAPriceCode.Dock = System.Windows.Forms.DockStyle.Top;
            this.textCBAPriceCode.Location = new System.Drawing.Point(154, 133);
            this.textCBAPriceCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textCBAPriceCode.Name = "textCBAPriceCode";
            this.textCBAPriceCode.Size = new System.Drawing.Size(317, 26);
            this.textCBAPriceCode.TabIndex = 46;
            // 
            // textCBAInvoiceDiscount
            // 
            this.textCBAInvoiceDiscount.Dock = System.Windows.Forms.DockStyle.Top;
            this.textCBAInvoiceDiscount.Location = new System.Drawing.Point(629, 13);
            this.textCBAInvoiceDiscount.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textCBAInvoiceDiscount.Name = "textCBAInvoiceDiscount";
            this.textCBAInvoiceDiscount.Size = new System.Drawing.Size(307, 26);
            this.textCBAInvoiceDiscount.TabIndex = 47;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.tlpContext);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Location = new System.Drawing.Point(12, 748);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox6.Size = new System.Drawing.Size(1431, 275);
            this.groupBox6.TabIndex = 48;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Company Profile Information";
            // 
            // tlpContext
            // 
            this.tlpContext.ColumnCount = 5;
            this.tlpContext.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 405F));
            this.tlpContext.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 141F));
            this.tlpContext.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 282F));
            this.tlpContext.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tlpContext.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpContext.Controls.Add(this.dgvContext, 0, 0);
            this.tlpContext.Controls.Add(this.tlpContextActions, 0, 5);
            this.tlpContext.Controls.Add(this.lblInternalID, 1, 0);
            this.tlpContext.Controls.Add(this.lblCustomerID, 1, 1);
            this.tlpContext.Controls.Add(this.cmbContextId, 2, 0);
            this.tlpContext.Controls.Add(this.textContextCustId, 2, 1);
            this.tlpContext.Controls.Add(this.tabControl2, 3, 0);
            this.tlpContext.Controls.Add(this.textContextProfileNotes, 2, 4);
            this.tlpContext.Controls.Add(this.pbContextCopyDefaults, 1, 5);
            this.tlpContext.Controls.Add(this.VendorNumber, 1, 2);
            this.tlpContext.Controls.Add(this.textContextVendorNumber, 2, 2);
            this.tlpContext.Controls.Add(this.Profilenotes, 1, 4);
            this.tlpContext.Controls.Add(this.VendorShort, 1, 3);
            this.tlpContext.Controls.Add(this.textContextVendorShortDesc, 2, 3);
            this.tlpContext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpContext.Location = new System.Drawing.Point(4, 24);
            this.tlpContext.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tlpContext.Name = "tlpContext";
            this.tlpContext.RowCount = 6;
            this.tlpContext.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpContext.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpContext.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpContext.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpContext.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpContext.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpContext.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tlpContext.Size = new System.Drawing.Size(1423, 246);
            this.tlpContext.TabIndex = 0;
            // 
            // dgvContext
            // 
            this.dgvContext.AllowUserToAddRows = false;
            this.dgvContext.AllowUserToDeleteRows = false;
            this.dgvContext.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvContext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvContext.Location = new System.Drawing.Point(4, 5);
            this.dgvContext.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvContext.MultiSelect = false;
            this.dgvContext.Name = "dgvContext";
            this.dgvContext.ReadOnly = true;
            this.dgvContext.RowHeadersVisible = false;
            this.dgvContext.RowHeadersWidth = 62;
            this.tlpContext.SetRowSpan(this.dgvContext, 5);
            this.dgvContext.Size = new System.Drawing.Size(397, 190);
            this.dgvContext.TabIndex = 48;
            this.dgvContext.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvContext_CellContentDoubleClick);
            this.dgvContext.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvContext_CellDoubleClick);
            // 
            // tlpContextActions
            // 
            this.tlpContextActions.ColumnCount = 3;
            this.tlpContextActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpContextActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tlpContextActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tlpContextActions.Controls.Add(this.pbContextDelete, 2, 0);
            this.tlpContextActions.Controls.Add(this.pbContextSave, 1, 0);
            this.tlpContextActions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpContextActions.Location = new System.Drawing.Point(0, 200);
            this.tlpContextActions.Margin = new System.Windows.Forms.Padding(0);
            this.tlpContextActions.Name = "tlpContextActions";
            this.tlpContextActions.RowCount = 1;
            this.tlpContextActions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpContextActions.Size = new System.Drawing.Size(405, 46);
            this.tlpContextActions.TabIndex = 67;
            // 
            // pbContextDelete
            // 
            this.pbContextDelete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbContextDelete.ImageKey = "Delete32.png";
            this.pbContextDelete.ImageList = this.imageListActions;
            this.pbContextDelete.Location = new System.Drawing.Point(297, 5);
            this.pbContextDelete.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbContextDelete.Name = "pbContextDelete";
            this.pbContextDelete.Size = new System.Drawing.Size(104, 36);
            this.pbContextDelete.TabIndex = 68;
            this.pbContextDelete.Text = "&Delete";
            this.pbContextDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbContextDelete.UseVisualStyleBackColor = true;
            this.pbContextDelete.Click += new System.EventHandler(this.pbContextDelete_Click);
            // 
            // pbContextSave
            // 
            this.pbContextSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbContextSave.ImageKey = "Add32.png";
            this.pbContextSave.ImageList = this.imageListActions;
            this.pbContextSave.Location = new System.Drawing.Point(185, 5);
            this.pbContextSave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbContextSave.Name = "pbContextSave";
            this.pbContextSave.Size = new System.Drawing.Size(104, 36);
            this.pbContextSave.TabIndex = 67;
            this.pbContextSave.Text = "&Save";
            this.pbContextSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbContextSave.UseVisualStyleBackColor = true;
            this.pbContextSave.Click += new System.EventHandler(this.pbContextSave_Click);
            // 
            // lblInternalID
            // 
            this.lblInternalID.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblInternalID.Location = new System.Drawing.Point(409, 9);
            this.lblInternalID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInternalID.Name = "lblInternalID";
            this.lblInternalID.Size = new System.Drawing.Size(132, 22);
            this.lblInternalID.TabIndex = 28;
            this.lblInternalID.Text = "Internal ID:";
            // 
            // lblCustomerID
            // 
            this.lblCustomerID.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblCustomerID.AutoSize = true;
            this.lblCustomerID.Location = new System.Drawing.Point(409, 50);
            this.lblCustomerID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCustomerID.Name = "lblCustomerID";
            this.lblCustomerID.Size = new System.Drawing.Size(103, 20);
            this.lblCustomerID.TabIndex = 29;
            this.lblCustomerID.Text = "Customer ID:";
            // 
            // cmbContextId
            // 
            this.cmbContextId.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbContextId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContextId.FormattingEnabled = true;
            this.cmbContextId.Location = new System.Drawing.Point(550, 5);
            this.cmbContextId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbContextId.Name = "cmbContextId";
            this.cmbContextId.Size = new System.Drawing.Size(274, 28);
            this.cmbContextId.TabIndex = 49;
            this.cmbContextId.SelectedIndexChanged += new System.EventHandler(this.cmbContextId_SelectedIndexChanged);
            // 
            // textContextCustId
            // 
            this.textContextCustId.Dock = System.Windows.Forms.DockStyle.Top;
            this.textContextCustId.Location = new System.Drawing.Point(550, 45);
            this.textContextCustId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textContextCustId.Name = "textContextCustId";
            this.textContextCustId.Size = new System.Drawing.Size(274, 26);
            this.textContextCustId.TabIndex = 50;
            // 
            // tabControl2
            // 
            this.tlpContext.SetColumnSpan(this.tabControl2, 2);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Controls.Add(this.tabPage6);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(831, 3);
            this.tabControl2.Multiline = true;
            this.tabControl2.Name = "tabControl2";
            this.tlpContext.SetRowSpan(this.tabControl2, 6);
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(589, 240);
            this.tabControl2.TabIndex = 54;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.tableLayoutPanel5);
            this.tabPage4.Location = new System.Drawing.Point(4, 29);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(581, 207);
            this.tabPage4.TabIndex = 0;
            this.tabPage4.Text = "Terms";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 149F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.lblOrderTerms, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.lblPaymentTerms, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.lblCPIDiscountNotes, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.lblTermsDiscountNotes, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.cmbContextOrderTerms, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.cmbContextPaymentTerms, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.cmbContextDepositReq, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.textContextDiscountNotes, 1, 3);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 5;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(575, 201);
            this.tableLayoutPanel5.TabIndex = 54;
            // 
            // lblOrderTerms
            // 
            this.lblOrderTerms.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblOrderTerms.AutoSize = true;
            this.lblOrderTerms.Location = new System.Drawing.Point(3, 10);
            this.lblOrderTerms.Name = "lblOrderTerms";
            this.lblOrderTerms.Size = new System.Drawing.Size(101, 20);
            this.lblOrderTerms.TabIndex = 0;
            this.lblOrderTerms.Text = "Order Terms:";
            // 
            // lblPaymentTerms
            // 
            this.lblPaymentTerms.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblPaymentTerms.AutoSize = true;
            this.lblPaymentTerms.Location = new System.Drawing.Point(3, 50);
            this.lblPaymentTerms.Name = "lblPaymentTerms";
            this.lblPaymentTerms.Size = new System.Drawing.Size(123, 20);
            this.lblPaymentTerms.TabIndex = 1;
            this.lblPaymentTerms.Text = "Payment Terms:";
            // 
            // lblCPIDiscountNotes
            // 
            this.lblCPIDiscountNotes.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblCPIDiscountNotes.AutoSize = true;
            this.lblCPIDiscountNotes.Location = new System.Drawing.Point(3, 90);
            this.lblCPIDiscountNotes.Name = "lblCPIDiscountNotes";
            this.lblCPIDiscountNotes.Size = new System.Drawing.Size(137, 20);
            this.lblCPIDiscountNotes.TabIndex = 2;
            this.lblCPIDiscountNotes.Text = "Deposit Required:";
            // 
            // lblTermsDiscountNotes
            // 
            this.lblTermsDiscountNotes.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTermsDiscountNotes.AutoSize = true;
            this.lblTermsDiscountNotes.Location = new System.Drawing.Point(3, 130);
            this.lblTermsDiscountNotes.Name = "lblTermsDiscountNotes";
            this.lblTermsDiscountNotes.Size = new System.Drawing.Size(122, 20);
            this.lblTermsDiscountNotes.TabIndex = 3;
            this.lblTermsDiscountNotes.Text = "Discount Notes:";
            // 
            // cmbContextOrderTerms
            // 
            this.cmbContextOrderTerms.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbContextOrderTerms.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContextOrderTerms.FormattingEnabled = true;
            this.cmbContextOrderTerms.Location = new System.Drawing.Point(152, 3);
            this.cmbContextOrderTerms.Name = "cmbContextOrderTerms";
            this.cmbContextOrderTerms.Size = new System.Drawing.Size(420, 28);
            this.cmbContextOrderTerms.TabIndex = 54;
            // 
            // cmbContextPaymentTerms
            // 
            this.cmbContextPaymentTerms.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbContextPaymentTerms.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContextPaymentTerms.FormattingEnabled = true;
            this.cmbContextPaymentTerms.Location = new System.Drawing.Point(152, 43);
            this.cmbContextPaymentTerms.Name = "cmbContextPaymentTerms";
            this.cmbContextPaymentTerms.Size = new System.Drawing.Size(420, 28);
            this.cmbContextPaymentTerms.TabIndex = 55;
            // 
            // cmbContextDepositReq
            // 
            this.cmbContextDepositReq.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbContextDepositReq.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContextDepositReq.FormattingEnabled = true;
            this.cmbContextDepositReq.Location = new System.Drawing.Point(152, 83);
            this.cmbContextDepositReq.Name = "cmbContextDepositReq";
            this.cmbContextDepositReq.Size = new System.Drawing.Size(420, 28);
            this.cmbContextDepositReq.TabIndex = 56;
            // 
            // textContextDiscountNotes
            // 
            this.textContextDiscountNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textContextDiscountNotes.Location = new System.Drawing.Point(152, 123);
            this.textContextDiscountNotes.Multiline = true;
            this.textContextDiscountNotes.Name = "textContextDiscountNotes";
            this.tableLayoutPanel5.SetRowSpan(this.textContextDiscountNotes, 2);
            this.textContextDiscountNotes.Size = new System.Drawing.Size(420, 75);
            this.textContextDiscountNotes.TabIndex = 57;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.tableLayoutPanel9);
            this.tabPage5.Location = new System.Drawing.Point(4, 29);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(581, 207);
            this.tabPage5.TabIndex = 1;
            this.tabPage5.Text = "Transaction Defaults";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 141F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this.pbChooseInvHdrImg, 0, 4);
            this.tableLayoutPanel9.Controls.Add(this.lblTDInvoiceHdrImg, 0, 3);
            this.tableLayoutPanel9.Controls.Add(this.textContextDefInvHdrImg, 1, 4);
            this.tableLayoutPanel9.Controls.Add(this.lblTDInvoiceType, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.lblTDProformaType, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.lblTDOrderAlert, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.cmbContextInvoiceType, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.cmbContextProformaType, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.textContextOrderAlert, 1, 2);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel16, 1, 3);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 5;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(575, 201);
            this.tableLayoutPanel9.TabIndex = 58;
            // 
            // pbChooseInvHdrImg
            // 
            this.pbChooseInvHdrImg.Location = new System.Drawing.Point(3, 168);
            this.pbChooseInvHdrImg.Name = "pbChooseInvHdrImg";
            this.pbChooseInvHdrImg.Size = new System.Drawing.Size(57, 30);
            this.pbChooseInvHdrImg.TabIndex = 63;
            this.pbChooseInvHdrImg.Text = "...";
            this.pbChooseInvHdrImg.UseVisualStyleBackColor = true;
            this.pbChooseInvHdrImg.Visible = false;
            this.pbChooseInvHdrImg.Click += new System.EventHandler(this.pbChooseInvHdrImg_Click);
            // 
            // lblTDInvoiceHdrImg
            // 
            this.lblTDInvoiceHdrImg.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTDInvoiceHdrImg.AutoSize = true;
            this.lblTDInvoiceHdrImg.Location = new System.Drawing.Point(3, 132);
            this.lblTDInvoiceHdrImg.Name = "lblTDInvoiceHdrImg";
            this.lblTDInvoiceHdrImg.Size = new System.Drawing.Size(124, 20);
            this.lblTDInvoiceHdrImg.TabIndex = 2;
            this.lblTDInvoiceHdrImg.Text = "Invoice Hdr Img:";
            // 
            // textContextDefInvHdrImg
            // 
            this.textContextDefInvHdrImg.Location = new System.Drawing.Point(145, 170);
            this.textContextDefInvHdrImg.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textContextDefInvHdrImg.Name = "textContextDefInvHdrImg";
            this.textContextDefInvHdrImg.Size = new System.Drawing.Size(304, 26);
            this.textContextDefInvHdrImg.TabIndex = 61;
            this.textContextDefInvHdrImg.Visible = false;
            // 
            // lblTDInvoiceType
            // 
            this.lblTDInvoiceType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTDInvoiceType.AutoSize = true;
            this.lblTDInvoiceType.Location = new System.Drawing.Point(3, 10);
            this.lblTDInvoiceType.Name = "lblTDInvoiceType";
            this.lblTDInvoiceType.Size = new System.Drawing.Size(101, 20);
            this.lblTDInvoiceType.TabIndex = 0;
            this.lblTDInvoiceType.Text = "Invoice Type:";
            // 
            // lblTDProformaType
            // 
            this.lblTDProformaType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTDProformaType.AutoSize = true;
            this.lblTDProformaType.Location = new System.Drawing.Point(3, 50);
            this.lblTDProformaType.Name = "lblTDProformaType";
            this.lblTDProformaType.Size = new System.Drawing.Size(116, 20);
            this.lblTDProformaType.TabIndex = 1;
            this.lblTDProformaType.Text = "Proforma Type:";
            // 
            // lblTDOrderAlert
            // 
            this.lblTDOrderAlert.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTDOrderAlert.AutoSize = true;
            this.lblTDOrderAlert.Location = new System.Drawing.Point(3, 90);
            this.lblTDOrderAlert.Name = "lblTDOrderAlert";
            this.lblTDOrderAlert.Size = new System.Drawing.Size(90, 20);
            this.lblTDOrderAlert.TabIndex = 2;
            this.lblTDOrderAlert.Text = "Order Alert:";
            // 
            // cmbContextInvoiceType
            // 
            this.cmbContextInvoiceType.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbContextInvoiceType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContextInvoiceType.FormattingEnabled = true;
            this.cmbContextInvoiceType.Location = new System.Drawing.Point(144, 3);
            this.cmbContextInvoiceType.Name = "cmbContextInvoiceType";
            this.cmbContextInvoiceType.Size = new System.Drawing.Size(428, 28);
            this.cmbContextInvoiceType.TabIndex = 58;
            // 
            // cmbContextProformaType
            // 
            this.cmbContextProformaType.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbContextProformaType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContextProformaType.FormattingEnabled = true;
            this.cmbContextProformaType.Location = new System.Drawing.Point(144, 43);
            this.cmbContextProformaType.Name = "cmbContextProformaType";
            this.cmbContextProformaType.Size = new System.Drawing.Size(428, 28);
            this.cmbContextProformaType.TabIndex = 59;
            // 
            // textContextOrderAlert
            // 
            this.textContextOrderAlert.Dock = System.Windows.Forms.DockStyle.Top;
            this.textContextOrderAlert.Location = new System.Drawing.Point(144, 83);
            this.textContextOrderAlert.Name = "textContextOrderAlert";
            this.textContextOrderAlert.Size = new System.Drawing.Size(428, 26);
            this.textContextOrderAlert.TabIndex = 60;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 1;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.Controls.Add(this.cmbContextDefInvHdrImg, 0, 0);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(141, 120);
            this.tableLayoutPanel16.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 1;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(434, 45);
            this.tableLayoutPanel16.TabIndex = 61;
            // 
            // cmbContextDefInvHdrImg
            // 
            this.cmbContextDefInvHdrImg.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbContextDefInvHdrImg.FormattingEnabled = true;
            this.cmbContextDefInvHdrImg.Location = new System.Drawing.Point(3, 3);
            this.cmbContextDefInvHdrImg.Name = "cmbContextDefInvHdrImg";
            this.cmbContextDefInvHdrImg.Size = new System.Drawing.Size(428, 28);
            this.cmbContextDefInvHdrImg.TabIndex = 0;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.tableLayoutPanel6);
            this.tabPage6.Location = new System.Drawing.Point(4, 29);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(581, 207);
            this.tabPage6.TabIndex = 2;
            this.tabPage6.Text = "Quote Defaults";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 141F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.lblQDQuoteCurrency, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.lblQDRebate, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.lblQDDiscount, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.lblQDSlushie, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.cmbContextQuoteCurrency, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel13, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel14, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel15, 1, 3);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 5;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(575, 201);
            this.tableLayoutPanel6.TabIndex = 61;
            // 
            // lblQDQuoteCurrency
            // 
            this.lblQDQuoteCurrency.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblQDQuoteCurrency.AutoSize = true;
            this.lblQDQuoteCurrency.Location = new System.Drawing.Point(3, 10);
            this.lblQDQuoteCurrency.Name = "lblQDQuoteCurrency";
            this.lblQDQuoteCurrency.Size = new System.Drawing.Size(124, 20);
            this.lblQDQuoteCurrency.TabIndex = 0;
            this.lblQDQuoteCurrency.Text = "Quote Currency:";
            this.lblQDQuoteCurrency.Visible = false;
            // 
            // lblQDRebate
            // 
            this.lblQDRebate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblQDRebate.AutoSize = true;
            this.lblQDRebate.Location = new System.Drawing.Point(3, 50);
            this.lblQDRebate.Name = "lblQDRebate";
            this.lblQDRebate.Size = new System.Drawing.Size(66, 20);
            this.lblQDRebate.TabIndex = 1;
            this.lblQDRebate.Text = "Rebate:";
            // 
            // lblQDDiscount
            // 
            this.lblQDDiscount.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblQDDiscount.AutoSize = true;
            this.lblQDDiscount.Location = new System.Drawing.Point(3, 90);
            this.lblQDDiscount.Name = "lblQDDiscount";
            this.lblQDDiscount.Size = new System.Drawing.Size(76, 20);
            this.lblQDDiscount.TabIndex = 2;
            this.lblQDDiscount.Text = "Discount:";
            // 
            // lblQDSlushie
            // 
            this.lblQDSlushie.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblQDSlushie.AutoSize = true;
            this.lblQDSlushie.Location = new System.Drawing.Point(3, 130);
            this.lblQDSlushie.Name = "lblQDSlushie";
            this.lblQDSlushie.Size = new System.Drawing.Size(65, 20);
            this.lblQDSlushie.TabIndex = 3;
            this.lblQDSlushie.Text = "Slushie:";
            // 
            // cmbContextQuoteCurrency
            // 
            this.cmbContextQuoteCurrency.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbContextQuoteCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContextQuoteCurrency.FormattingEnabled = true;
            this.cmbContextQuoteCurrency.Location = new System.Drawing.Point(144, 3);
            this.cmbContextQuoteCurrency.Name = "cmbContextQuoteCurrency";
            this.cmbContextQuoteCurrency.Size = new System.Drawing.Size(428, 28);
            this.cmbContextQuoteCurrency.TabIndex = 63;
            this.cmbContextQuoteCurrency.Visible = false;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 2;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Controls.Add(this.textContextQuoteRebate, 0, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(141, 40);
            this.tableLayoutPanel13.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(434, 40);
            this.tableLayoutPanel13.TabIndex = 62;
            // 
            // textContextQuoteRebate
            // 
            this.textContextQuoteRebate.DisplayFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.Percent;
            this.textContextQuoteRebate.DisplayFormat.Inherit = ((C1.Win.C1Input.FormatInfoInheritFlags)((((((C1.Win.C1Input.FormatInfoInheritFlags.CustomFormat | C1.Win.C1Input.FormatInfoInheritFlags.NullText) 
            | C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd) 
            | C1.Win.C1Input.FormatInfoInheritFlags.CalendarType)));
            this.textContextQuoteRebate.Enabled = false;
            this.textContextQuoteRebate.ImagePadding = new System.Windows.Forms.Padding(0);
            this.textContextQuoteRebate.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.textContextQuoteRebate.Location = new System.Drawing.Point(3, 3);
            this.textContextQuoteRebate.Name = "textContextQuoteRebate";
            this.textContextQuoteRebate.Size = new System.Drawing.Size(208, 24);
            this.textContextQuoteRebate.TabIndex = 64;
            this.textContextQuoteRebate.Tag = null;
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 2;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.Controls.Add(this.textContextQuoteDiscount, 0, 0);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(141, 80);
            this.tableLayoutPanel14.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 1;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(434, 40);
            this.tableLayoutPanel14.TabIndex = 63;
            // 
            // textContextQuoteDiscount
            // 
            this.textContextQuoteDiscount.DisplayFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.Percent;
            this.textContextQuoteDiscount.DisplayFormat.Inherit = ((C1.Win.C1Input.FormatInfoInheritFlags)((((((C1.Win.C1Input.FormatInfoInheritFlags.CustomFormat | C1.Win.C1Input.FormatInfoInheritFlags.NullText) 
            | C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd) 
            | C1.Win.C1Input.FormatInfoInheritFlags.CalendarType)));
            this.textContextQuoteDiscount.Enabled = false;
            this.textContextQuoteDiscount.ImagePadding = new System.Windows.Forms.Padding(0);
            this.textContextQuoteDiscount.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.textContextQuoteDiscount.Location = new System.Drawing.Point(3, 3);
            this.textContextQuoteDiscount.Name = "textContextQuoteDiscount";
            this.textContextQuoteDiscount.Size = new System.Drawing.Size(208, 24);
            this.textContextQuoteDiscount.TabIndex = 65;
            this.textContextQuoteDiscount.Tag = null;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 2;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Controls.Add(this.textContextQuoteSlushie, 0, 0);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(141, 120);
            this.tableLayoutPanel15.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 1;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(434, 40);
            this.tableLayoutPanel15.TabIndex = 64;
            // 
            // textContextQuoteSlushie
            // 
            this.textContextQuoteSlushie.DisplayFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.Percent;
            this.textContextQuoteSlushie.DisplayFormat.Inherit = ((C1.Win.C1Input.FormatInfoInheritFlags)((((((C1.Win.C1Input.FormatInfoInheritFlags.CustomFormat | C1.Win.C1Input.FormatInfoInheritFlags.NullText) 
            | C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) 
            | C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd) 
            | C1.Win.C1Input.FormatInfoInheritFlags.CalendarType)));
            this.textContextQuoteSlushie.Enabled = false;
            this.textContextQuoteSlushie.ImagePadding = new System.Windows.Forms.Padding(0);
            this.textContextQuoteSlushie.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.textContextQuoteSlushie.Location = new System.Drawing.Point(3, 3);
            this.textContextQuoteSlushie.Name = "textContextQuoteSlushie";
            this.textContextQuoteSlushie.Size = new System.Drawing.Size(208, 24);
            this.textContextQuoteSlushie.TabIndex = 66;
            this.textContextQuoteSlushie.Tag = null;
            // 
            // textContextProfileNotes
            // 
            this.textContextProfileNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textContextProfileNotes.Location = new System.Drawing.Point(549, 163);
            this.textContextProfileNotes.Multiline = true;
            this.textContextProfileNotes.Name = "textContextProfileNotes";
            this.tlpContext.SetRowSpan(this.textContextProfileNotes, 2);
            this.textContextProfileNotes.Size = new System.Drawing.Size(276, 80);
            this.textContextProfileNotes.TabIndex = 53;
            // 
            // pbContextCopyDefaults
            // 
            this.pbContextCopyDefaults.Dock = System.Windows.Forms.DockStyle.Right;
            this.pbContextCopyDefaults.Location = new System.Drawing.Point(421, 203);
            this.pbContextCopyDefaults.Name = "pbContextCopyDefaults";
            this.pbContextCopyDefaults.Size = new System.Drawing.Size(122, 40);
            this.pbContextCopyDefaults.TabIndex = 69;
            this.pbContextCopyDefaults.Text = "Copy Defaults";
            this.pbContextCopyDefaults.UseVisualStyleBackColor = true;
            this.pbContextCopyDefaults.Click += new System.EventHandler(this.pbContextCopyDefaults_Click);
            // 
            // VendorNumber
            // 
            this.VendorNumber.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.VendorNumber.AutoSize = true;
            this.VendorNumber.Location = new System.Drawing.Point(408, 90);
            this.VendorNumber.Name = "VendorNumber";
            this.VendorNumber.Size = new System.Drawing.Size(125, 20);
            this.VendorNumber.TabIndex = 54;
            this.VendorNumber.Text = "Vendor Number:";
            // 
            // textContextVendorNumber
            // 
            this.textContextVendorNumber.Dock = System.Windows.Forms.DockStyle.Top;
            this.textContextVendorNumber.Location = new System.Drawing.Point(549, 83);
            this.textContextVendorNumber.Name = "textContextVendorNumber";
            this.textContextVendorNumber.Size = new System.Drawing.Size(276, 26);
            this.textContextVendorNumber.TabIndex = 51;
            // 
            // Profilenotes
            // 
            this.Profilenotes.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Profilenotes.AutoSize = true;
            this.Profilenotes.Location = new System.Drawing.Point(408, 170);
            this.Profilenotes.Name = "Profilenotes";
            this.Profilenotes.Size = new System.Drawing.Size(103, 20);
            this.Profilenotes.TabIndex = 37;
            this.Profilenotes.Text = "Profile Notes:";
            // 
            // VendorShort
            // 
            this.VendorShort.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.VendorShort.AutoSize = true;
            this.VendorShort.Location = new System.Drawing.Point(409, 130);
            this.VendorShort.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.VendorShort.Name = "VendorShort";
            this.VendorShort.Size = new System.Drawing.Size(108, 20);
            this.VendorShort.TabIndex = 66;
            this.VendorShort.Text = "Vendor Short:";
            // 
            // textContextVendorShortDesc
            // 
            this.textContextVendorShortDesc.Dock = System.Windows.Forms.DockStyle.Top;
            this.textContextVendorShortDesc.Location = new System.Drawing.Point(550, 125);
            this.textContextVendorShortDesc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textContextVendorShortDesc.Name = "textContextVendorShortDesc";
            this.textContextVendorShortDesc.Size = new System.Drawing.Size(274, 26);
            this.textContextVendorShortDesc.TabIndex = 52;
            // 
            // frmCustomerMaint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1455, 1071);
            this.Controls.Add(this.tlpFramework);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmCustomerMaint";
            this.Text = "Maintain Customers";
            this.Load += new System.EventHandler(this.frmCustomerMaint_Load);
            this.tlpFramework.ResumeLayout(false);
            this.tlpActions.ResumeLayout(false);
            this.tlpActions.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tlpCustomer.ResumeLayout(false);
            this.tlpCustomer.PerformLayout();
            this.tlpCustId.ResumeLayout(false);
            this.tlpCustId.PerformLayout();
            this.tlpParentCust.ResumeLayout(false);
            this.tlpParentCust.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tlpAddress.ResumeLayout(false);
            this.tlpAddress.PerformLayout();
            this.tlpDefaultDelAddr.ResumeLayout(false);
            this.tlpDefaultDelAddr.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDefaultQuoteRebate)).EndInit();
            this.tableLayoutPanel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDefaultQuoteDiscount)).EndInit();
            this.tableLayoutPanel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDefaultSlushie)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.tlpContext.ResumeLayout(false);
            this.tlpContext.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContext)).EndInit();
            this.tlpContextActions.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textContextQuoteRebate)).EndInit();
            this.tableLayoutPanel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textContextQuoteDiscount)).EndInit();
            this.tableLayoutPanel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textContextQuoteSlushie)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpFramework;
        private System.Windows.Forms.TableLayoutPanel tlpAddress;
        private System.Windows.Forms.TableLayoutPanel tlpCustomer;
        private System.Windows.Forms.Label lblSystemID;
        private System.Windows.Forms.Label lblName1;
        private System.Windows.Forms.Label lblName2;
        private System.Windows.Forms.Label lblBusinessName;
        private System.Windows.Forms.Label lblParentCustomer;
        private System.Windows.Forms.TableLayoutPanel tlpCustId;
        private System.Windows.Forms.Button pbCustIdSearch;
        private System.Windows.Forms.TextBox textCustomerId;
        private System.Windows.Forms.ImageList imageListActions;
        private System.Windows.Forms.TableLayoutPanel tlpParentCust;
        private System.Windows.Forms.Button pbParentCustIdSearch;
        private System.Windows.Forms.TextBox textParentCustId;
        private System.Windows.Forms.TextBox textParentCustName;
        private System.Windows.Forms.TextBox textCustName1;
        private System.Windows.Forms.TextBox textCustName2;
        private System.Windows.Forms.TextBox textCustBusinessName;
        private System.Windows.Forms.Label lblCustomerType;
        private System.Windows.Forms.Label lblDefaultCurrency;
        private System.Windows.Forms.Label lblDefaultPaymentTerms;
        private System.Windows.Forms.ComboBox cmbCustType;
        private System.Windows.Forms.ComboBox cmbDefCurrency;
        private System.Windows.Forms.ComboBox cmbPaymentTerms;
        private System.Windows.Forms.Label lblDefaultOrderTerms;
        private System.Windows.Forms.ComboBox cmbOrderTerms;
        private System.Windows.Forms.TableLayoutPanel tlpActions;
        private System.Windows.Forms.Button pbSave;
        private System.Windows.Forms.Button pbCancel;
        private System.Windows.Forms.Button pbDelete;
        private System.Windows.Forms.Label lblActions;
        private System.Windows.Forms.CheckBox cbActive;
        private System.Windows.Forms.Label lblGeneralNotes;
        private System.Windows.Forms.TextBox textNotes;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblStreet;
        private System.Windows.Forms.Label lblsuburbCity;
        private System.Windows.Forms.Label lblAddress3;
        private System.Windows.Forms.TextBox textMainAddr1;
        private System.Windows.Forms.TextBox textMainAddr2;
        private System.Windows.Forms.TextBox textMainAddr3;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.Label lblPostCode;
        private System.Windows.Forms.Label lblCoutry;
        private System.Windows.Forms.TextBox textMainAddrState;
        private System.Windows.Forms.TextBox textMainAddrPostCode;
        private System.Windows.Forms.ComboBox cmbMainAddrCountry;
        private System.Windows.Forms.Label lblPhonenumber;
        private System.Windows.Forms.Label lblFaxNumber;
        private System.Windows.Forms.Label lblEmailAddress;
        private System.Windows.Forms.CheckBox cbIsDel;
        private System.Windows.Forms.Button pbAddresses;
        private System.Windows.Forms.TableLayoutPanel tlpDefaultDelAddr;
        private System.Windows.Forms.Button pbContacts;
        private System.Windows.Forms.ComboBox cmbDefPort;
        private System.Windows.Forms.Label lblDefaultPort;
        private System.Windows.Forms.TextBox textMainAddrPhone;
        private System.Windows.Forms.TextBox textMainAddrFax;
        private System.Windows.Forms.TextBox textMainAddrEmail;
        private System.Windows.Forms.Label lblDepositRequired;
        private System.Windows.Forms.ComboBox cmbDepositReq;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblTerms;
        private System.Windows.Forms.Label lblBranch;
        private System.Windows.Forms.Label lblsalesman;
        private System.Windows.Forms.Label lblPrieCode;
        private System.Windows.Forms.Label lblInvoiceDiscount;
        private System.Windows.Forms.Label lblQuoteCurrency;
        private System.Windows.Forms.Label lblInvoiceType;
        private System.Windows.Forms.Label lblinvoiceProformaType;
        private System.Windows.Forms.Label lblPrimaryContact;
        private System.Windows.Forms.Label lblLockCode;
        private System.Windows.Forms.Label lblRestrictions;
        private System.Windows.Forms.Label lblOrderAlert;
        private System.Windows.Forms.Label lblShippingMarks;
        private System.Windows.Forms.TextBox textCBATerms;
        private System.Windows.Forms.TextBox textCBABranch;
        private System.Windows.Forms.TextBox textCBASalesman;
        private System.Windows.Forms.TextBox textCBAPriceCode;
        private System.Windows.Forms.TextBox textCBAInvoiceDiscount;
        private System.Windows.Forms.TextBox textCBARestrictions;
        private System.Windows.Forms.TextBox textCBAOrderAlert;
        private System.Windows.Forms.TextBox textCBAShippingMarks;
        private System.Windows.Forms.ComboBox cmbCBAQCurrency;
        private System.Windows.Forms.ComboBox cmbCBADefInvType;
        private System.Windows.Forms.ComboBox cmbCBADefPIType;
        private System.Windows.Forms.ComboBox cmbCBALockCode;
        private System.Windows.Forms.ComboBox cmbPrimaryContactNo;
        private System.Windows.Forms.CheckBox cbSplitOnly;
        private System.Windows.Forms.Label lblDiscountNotes;
        private System.Windows.Forms.TextBox textDiscountNotes;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lblDefQuoteRebate;
        private System.Windows.Forms.Label lblDefQuoteDisc;
        private System.Windows.Forms.Label lblDefaultSlushie;
        private C1.Win.C1Input.C1NumericEdit textBoxDefaultQuoteRebate;
        private C1.Win.C1Input.C1NumericEdit textBoxDefaultQuoteDiscount;
        private C1.Win.C1Input.C1NumericEdit textBoxDefaultSlushie;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label lblCompanyNo;
        private System.Windows.Forms.Label lblEnityType;
        private System.Windows.Forms.TextBox textABNNo;
        private System.Windows.Forms.ComboBox cmbEntityType;
        private System.Windows.Forms.Label lblProductProfileID;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button pbProductProfileIdSearch;
        private System.Windows.Forms.TextBox textProductProfileId;
        private System.Windows.Forms.TextBox textProductProfileName;
        private System.Windows.Forms.DataGridView dgvContext;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TableLayoutPanel tlpContext;
        private System.Windows.Forms.Label lblInternalID;
        private System.Windows.Forms.Label lblCustomerID;
        private System.Windows.Forms.TableLayoutPanel tlpContextActions;
        private System.Windows.Forms.Button pbContextDelete;
        private System.Windows.Forms.Button pbContextSave;
        private System.Windows.Forms.TextBox textContextCustId;
        private System.Windows.Forms.ComboBox cmbContextId;
        private System.Windows.Forms.Button pbAllCustSearch;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label lblOrderTerms;
        private System.Windows.Forms.Label lblPaymentTerms;
        private System.Windows.Forms.Label lblCPIDiscountNotes;
        private System.Windows.Forms.Label lblTermsDiscountNotes;
        private System.Windows.Forms.ComboBox cmbContextOrderTerms;
        private System.Windows.Forms.ComboBox cmbContextPaymentTerms;
        private System.Windows.Forms.ComboBox cmbContextDepositReq;
        private System.Windows.Forms.TextBox textContextDiscountNotes;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label lblTDInvoiceType;
        private System.Windows.Forms.Label lblTDProformaType;
        private System.Windows.Forms.Label lblTDOrderAlert;
        private System.Windows.Forms.ComboBox cmbContextInvoiceType;
        private System.Windows.Forms.ComboBox cmbContextProformaType;
        private System.Windows.Forms.TextBox textContextOrderAlert;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label lblQDQuoteCurrency;
        private System.Windows.Forms.Label lblQDRebate;
        private System.Windows.Forms.Label lblQDDiscount;
        private System.Windows.Forms.Label lblQDSlushie;
        private System.Windows.Forms.ComboBox cmbContextQuoteCurrency;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private C1.Win.C1Input.C1NumericEdit textContextQuoteRebate;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private C1.Win.C1Input.C1NumericEdit textContextQuoteDiscount;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private C1.Win.C1Input.C1NumericEdit textContextQuoteSlushie;
        private System.Windows.Forms.TextBox textContextProfileNotes;
        private System.Windows.Forms.Label Profilenotes;
        private System.Windows.Forms.Button pbContextCopyDefaults;
        private System.Windows.Forms.Label VendorNumber;
        private System.Windows.Forms.TextBox textContextVendorNumber;
        private System.Windows.Forms.TextBox textTaxNumber;
        private System.Windows.Forms.Label lblTaxNumber;
        private System.Windows.Forms.TextBox textAddrLabel;
        private System.Windows.Forms.Label lblDCAddressLabel;
        private System.Windows.Forms.Label lblTDInvoiceHdrImg;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.TextBox textContextDefInvHdrImg;
        private System.Windows.Forms.ComboBox cmbWeightConv;
        private System.Windows.Forms.ComboBox cmbVolumeConv;
        private System.Windows.Forms.Label lblVolumePref;
        private System.Windows.Forms.Label lblWeightPref;
        private System.Windows.Forms.Label VendorShort;
        private System.Windows.Forms.TextBox textContextVendorShortDesc;
        private System.Windows.Forms.Button pbChooseInvHdrImg;
        private System.Windows.Forms.ComboBox cmbContextDefInvHdrImg;
    }
}
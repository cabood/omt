﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UbiSqlFramework;

namespace OMT.Forms
{
    public partial class MDIWorkbench : Form, IDateExchange
    {
        //private int childFormNumber = 0;
        private bool bShutDownOnClose = true;

        public MDIWorkbench()
        {
            InitializeComponent();
        }

        public bool ShutDownOnClose
        {
            get { return bShutDownOnClose; }
            set { bShutDownOnClose = value; }
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            //Form childForm = new Form();
            //childForm.MdiParent = this;
            //childForm.Text = "Window " + childFormNumber++;
            //childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            //OpenFileDialog openFileDialog = new OpenFileDialog();
            //openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            //openFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            //if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            //{
            //    string FileName = openFileDialog.FileName;
            //}
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    SaveFileDialog saveFileDialog = new SaveFileDialog();
        //    saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        //    saveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
        //    if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
        //    {
        //        string FileName = saveFileDialog.FileName;
        //    }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void dashboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmWorkbench frmD = new frmWorkbench();
        //    frmD.MdiParent = this;
        //    frmD.WindowState = FormWindowState.Normal;
        //    //this.AddOwnedForm(frmD);
        //    //frmD.Show(this);
        //    frmD.Show();
        }

        private void changeUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    if (this.MdiChildren.Count() > 0)
        //    {
        //        MessageBox.Show("Close all windows first!", "Shiteful Effort", MessageBoxButtons.OK);
                
        //    }
        //    else
        //    {
        //        //this.;
        //        bShutDownOnClose = false;
        //        this.Close();
        //        dlgLogin dlg;
        //        dlg = new dlgLogin();
        //        //Application.Run(new dlgLogin());
        //        dlg.ShowDialog();
        //    }
        }

        private void MDIWorkbench_Load(object sender, EventArgs e)
        {
            if (!AppHost.LoadGlobals())
            {
                MessageBox.Show("Application failed to load.", "Filth!");
                AppHost.DestructApp();
                Application.Exit();
            }
            AppHost.AutoUpdateClientConfig(true);
            string sVersion = Constants.RELEASE_VERSION;
            string sStatus = "Connected to " +
                "Server: " + SqlHost.CurrentConnectionProfile.ServerName + ", " +
                "Database: " + SqlHost.CurrentConnectionProfile.DatabaseName + ". " +
                "Logged in as " + AppHost.CurrentUser["UserName"].ToString() + ".";
            this.Text = "Masterfiles Workbench. Version: " + sVersion + "      " + AppHost.CurrentContext.Name1.ToUpper();
            this.toolStripStatusLabel.Text = sStatus;
            string sCol = SqlHost.CurrentConnectionProfile.ColourCode;
            if (sCol.Length > 0)
            {
                this.statusStrip.BackColor = Color.FromArgb(Convert.ToInt32(sCol));
            }
            else
            {
                this.statusStrip.BackColor = SystemColors.Control;
            }
        }

        private void referenceDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmRefData frmR = new frmRefData();
        //    //this.AddOwnedForm(frmR);
        //    frmR.MdiParent = this;
        //    frmR.WindowState = FormWindowState.Normal;
        //    frmR.Show();
        }

        private void suppliersToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //
        }

        private void referenceDataToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            frmRefData frmR = new frmRefData();
            frmR.MdiParent = this;
            frmR.WindowState = FormWindowState.Normal;
            frmR.Show();
        }

        private void customersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCustomerMaint frmC = new frmCustomerMaint();
            frmC.MdiParent = this;
            frmC.WindowState = FormWindowState.Normal;
            frmC.Show();
        }

        private void suppliersToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            frmSupplierMaint frmS = new frmSupplierMaint();
            frmS.MdiParent = this;
            frmS.WindowState = FormWindowState.Normal;
            frmS.Show();
        }

        private void MDIWorkbench_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (bShutDownOnClose)
            {
                AppHost.DestructApp();
                Application.Exit();
            }
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
        //
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmAbout dlg = new frmAbout();
        //    dlg.Show();
        }

        private void refreshReferenceDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    if (AppHost.LoadReferenceData())
        //    {
        //        MessageBox.Show("Done", "Sure thing!");
        //    }
        }

        private void userMaintenanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmUserMaintenance frm = new frmUserMaintenance();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }

        private void countersToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //
        }

        private void quickExtractToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmReportCustomExtract frm = new frmReportCustomExtract();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
        //    frmSysCtr frm = new frmSysCtr();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }

        private void warehousesToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmWarehouse frm = new frmWarehouse();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }

        private void documentTemplatesToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmDocTemplate frm = new frmDocTemplate();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }

        private void hierarchyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmHierarchy frm = new frmHierarchy();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }

        private void hierarchyLevelsToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmHierarchyLevel frm = new frmHierarchyLevel();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }

        private void stockMaintenanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmStockMaint frm = new frmStockMaint();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }


        private void customerProductProfilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmCustProductProfile frm = new frmCustProductProfile();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }

        private void sqlRawToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    if (!AppHost.SecurityCheck(Constants.SECURITY_SUPERUSERSQL))
        //    {
        //        MessageBox.Show("Insufficient Clearance", "Not Allowed");
        //        return;
        //    }
        //    frmSQLDirect frm = new frmSQLDirect();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }

        private void quotesToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmQuoteView frm = new frmQuoteView();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }

        private void masterfilesAnalysisToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmOLAPAnalysis frm = new frmOLAPAnalysis();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }
        private void qaqcReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmQAQCReport frm = new frmQAQCReport();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }
        
        private void userProfileGroupsToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmUserProfileMaint frm = new frmUserProfileMaint();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }

        private void teamBasedDashboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmWorkbenchTeam frmD = new frmWorkbenchTeam();
        //    frmD.MdiParent = this;
        //    frmD.WindowState = FormWindowState.Normal;
        //    //this.AddOwnedForm(frmD);
        //    frmD.Show();
        }

        private void portPairManagementToolStripMenuItem1_Click(object sender, EventArgs e)
        {
        //    frmPortPairManager frm = new frmPortPairManager();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }

        private void shippingContractsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
        //    frmShippingContract frm = new frmShippingContract();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }

        private void shippingLineContactsToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmShippingLineContact frm = new frmShippingLineContact();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }

        private void contextCompanyMaintenanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmContextMaintenance frm = new frmContextMaintenance();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }

        private void trackingProfilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmJobTrackingTemplate frm = new frmJobTrackingTemplate();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }

        private void myTasksToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    string sUser = AppHost.CurrentUser["UserName"].ToString();
        //    dlgJobTaskList dlg = new dlgJobTaskList(sUser);
        //    dlg.MdiParent = this;
        //    dlg.WindowState = FormWindowState.Normal;
        //    dlg.Show();
        }

        private void sqlRawToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
        //
        }

        private void sQLUtilityToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    if (!AppHost.SecurityCheck(Constants.SECURITY_SUPERUSERSQL))
        //    {
        //        MessageBox.Show("Insufficient Clearance", "Not Allowed");
        //        return;
        //    }
        //    frmSQLDirect frm = new frmSQLDirect();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }

        private void orderResetToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmBHOrderReset frm = new frmBHOrderReset();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }

        private void checkForUpdatesToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    AppHost.AutoUpdateClientConfig(true);
        }

        private void manualUpdateToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    dlgSoftwareUpdateManual dlg = new dlgSoftwareUpdateManual();
        //    dlg.ShowDialog(this);
        }

        private void applicationControlSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    frmAppControl frm = new frmAppControl();
        //    frm.MdiParent = this;
        //    frm.WindowState = FormWindowState.Normal;
        //    frm.Show();
        }

        private void showAllCompaniesToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    showAllCompaniesToolStripMenuItem.Checked = !showAllCompaniesToolStripMenuItem.Checked;
        //    bool bGlobal = showAllCompaniesToolStripMenuItem.Checked;
        //    AppHost.SingleContext = !bGlobal;
        }

        private void jobMinimumDateFIlterToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //
        }

        private void LaunchEditJobMinimumDateFilter(DataGridView d)
        {
        //    DateTime dt;

        //    dt = AppHost.ArchiveDate;

        //    dlgDatePicker dlg = new dlgDatePicker();
        //    dlg.DatePartner = this;
        //    DateParam dtP = new DateParam();
        //    dtP.DtValue = dt;
        //    dtP.Action = "ChangeJobMinDateFilter";
        //    dtP.SourceControl = "MDI";
        //    dlg.Location = MousePosition;
        //    dlg.AcceptDate(dtP);
        //    dlg.ShowDialog(this);
        }

        public bool AcceptDate(DateParam dt)
        {
            try
            {
                if (dt.Action == "ChangeJobMinDateFilter")
                {
                    AppHost.ArchiveDate = dt.DtValue;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}

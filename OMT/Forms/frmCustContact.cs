﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using UbiSqlFramework;

namespace OMT.Forms
{
    public partial class frmCustContact : Form, IGenericExchange
    {
        string sCustId;
        //string sDefAddrId;
        //string sDefDelAddrId;
        DataSet dsContacts;
        bool bNewRow = false;

        public frmCustContact()
        {
            InitializeComponent();
            InitFormData();
        }

        private void frmCustContact_Load(object sender, EventArgs e)
        {
            this.ActiveControl = dgvData;
        }

        private void InitFormData()
        {
            LoadContactTypes();
            ClearFormDetails(true);
        }
        private void LoadContactTypes()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbContactType.DataSource = null;
                cmbContactType.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'CONTACTTYPE'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "AddrTypeData", ref refContext);
                cmbContactType.DataSource = refContext.Tables["AddrTypeData"];
                cmbContactType.ValueMember = "REF_ID";
                cmbContactType.DisplayMember = "DESCRIPTION";
                cmbContactType.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Contact Types Error", MessageBoxButtons.OK);
            }
        }

        private void ClearFormDetails(bool bClearCustId)
        {
            if (bClearCustId)
            {
                textCustId.Text = "";
                textCustName.Text = "";
            }
            textContactId.Text = "";
            textName1.Text = "";
            textName2.Text = "";
            textEmail.Text = "";
            cmbContactType.SelectedIndex = -1;
            textPhone1.Text = "";
            textPhone2.Text = "";
            textPhone3.Text = "";
            textNotes.Text = "";
        }

        private void dgvData_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string sId = "";
            if (dgvData.SelectedRows.Count > 0)
            {
                sId = ((string)(((DataRowView)dgvData.SelectedRows[0].DataBoundItem).Row)["CONTACT_ID"]).ToString();
            }
            else if (dgvData.SelectedCells.Count > 0)
            {
                int nId = dgvData.SelectedCells[0].RowIndex;
                object cId = ((DataGridViewRow)(dgvData.Rows[nId]).DataBoundItem);
            }
            if (sId.Length > 0)
                EditRow(sId);
        }

        private void dgvData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string sId = "";
            if (dgvData.SelectedRows.Count > 0)
            {
                sId = ((string)(((DataRowView)dgvData.SelectedRows[0].DataBoundItem).Row)["CONTACT_ID"]).ToString();
            }
            else if (dgvData.SelectedCells.Count > 0)
            {
                int nId = dgvData.SelectedCells[0].RowIndex;
                sId = ((DataRowView)(dgvData.Rows[nId]).DataBoundItem).Row["CONTACT_ID"].ToString();
            }
            if (sId.Length > 0)
                EditRow(sId);
        }

        private void dgvData_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string sId = "";
            if (dgvData.SelectedRows.Count > 0)
            {
                sId = ((string)(((DataRowView)dgvData.SelectedRows[0].DataBoundItem).Row)["CONTACT_ID"]).ToString();
            }
            if (sId.Length > 0)
                EditRow(sId);
        }

        private void EditRow(string sId)
        {
            RetrieveContactDetails(sId);
        }

        private void RetrieveContactDetails(string sContactId)
        {
            SqlCommand rCom;
            SqlDataReader cReader;
            try
            {
                ClearFormDetails(false);
                if (sContactId.Length == 0)
                {
                    return;
                }

                textContactId.Text = sContactId;

                rCom = new SqlCommand("select A.CUSTOMER_ID, A.CONTACT_ID, A.CONTACT_TYPE, A.NAME_1, A.NAME_2, A.CONTACT_NUMBER_1, A.CONTACT_NUMBER_2, A.CONTACT_NUMBER_3, A.EMAIL_ADDRESS, A.NOTES from CUSTOMER_CONTACT A WHERE A.CUSTOMER_ID = @CUST_ID and A.CONTACT_ID = @CONTACT_ID", SqlHost.DBConn);
                rCom.Parameters.AddWithValue("@CUST_ID", sCustId);
                rCom.Parameters.AddWithValue("@CONTACT_ID", sContactId);

                cReader = rCom.ExecuteReader();

                bNewRow = !cReader.HasRows;

                while (cReader.Read())
                {
                    if (cReader["NAME_1"] != System.DBNull.Value)
                        textName1.Text = cReader["NAME_1"].ToString();
                    if (cReader["NAME_2"] != System.DBNull.Value)
                        textName2.Text = cReader["NAME_2"].ToString();
                    if (cReader["CONTACT_NUMBER_1"] != System.DBNull.Value)
                        textPhone1.Text = cReader["CONTACT_NUMBER_1"].ToString();
                    if (cReader["CONTACT_NUMBER_2"] != System.DBNull.Value)
                        textPhone2.Text = cReader["CONTACT_NUMBER_2"].ToString();
                    if (cReader["CONTACT_NUMBER_3"] != System.DBNull.Value)
                        textPhone3.Text = cReader["CONTACT_NUMBER_3"].ToString();
                    if (cReader["EMAIL_ADDRESS"] != System.DBNull.Value)
                        textEmail.Text = cReader["EMAIL_ADDRESS"].ToString();
                    if (cReader["NOTES"] != System.DBNull.Value)
                        textNotes.Text = cReader["NOTES"].ToString();
                    if (cReader["CONTACT_TYPE"] != System.DBNull.Value && cReader["CONTACT_TYPE"].ToString().Length > 0)
                    {
                        cmbContactType.SelectedValue = (string)cReader["CONTACT_TYPE"];
                    }
                    else
                    {
                        cmbContactType.SelectedIndex = -1;
                    }
                }
                cReader.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Retrieve Contact Details Error", MessageBoxButtons.OK);
            }

        }

        private void pbAdd_Click(object sender, EventArgs e)
        {
            string sContactId = GetNextContactId(sCustId);
            RetrieveContactDetails(sContactId);
            this.ActiveControl = textName1;
        }

        private string GetNextContactId(string sId)
        {
            string sSql;
            SqlCommand sqlCmd;
            SqlDataReader cReader;
            string sCurrentMax;
            string sNewId = "";
            int nCurrentMax;
            int nNewMax;
            try
            {
                sSql = "select max(CONTACT_ID) as CURRENT_MAX from CUSTOMER_CONTACT where CUSTOMER_ID = @CUST_ID";
                sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                sqlCmd.Parameters.AddWithValue("@CUST_ID", sId);
                cReader = sqlCmd.ExecuteReader();
                if (cReader.HasRows)
                {
                    while (cReader.Read())
                    {
                        sCurrentMax = cReader["CURRENT_MAX"].ToString();
                        if (sCurrentMax.Length == 0)
                        {
                            nCurrentMax = 0;
                        }
                        else
                        {
                            nCurrentMax = Convert.ToInt32(sCurrentMax);
                        }
                        nNewMax = nCurrentMax + 1;
                        sNewId = nNewMax.ToString("000");
                    }
                }
                else
                {
                    sNewId = "001";
                }
                cReader.Close();
                return sNewId;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        private void pbDelete_Click(object sender, EventArgs e)
        {
            string sId = "";
            if (dgvData.SelectedRows.Count > 0)
            {
                sId = ((string)(((DataRowView)dgvData.SelectedRows[0].DataBoundItem).Row)["CONTACT_ID"]).ToString();
            }
            else if (dgvData.SelectedCells.Count > 0)
            {
                int nId = dgvData.SelectedCells[0].RowIndex;
                sId = ((DataRowView)(dgvData.Rows[nId]).DataBoundItem).Row["CONTACT_ID"].ToString();
            }
            if (sId.Length > 0)
                DeleteRow(sId);
        }

        private void pbSave_Click(object sender, EventArgs e)
        {
            if (SaveRow())
            {
                ClearFormDetails(false);
                RetrieveCustomerDetails(sCustId);
                this.ActiveControl = dgvData;
            }
        }

        private bool SaveRow()
        {
            string sSql;
            SqlCommand sqlCmd;
            try
            {
                if (textContactId.Text.Length > 0)
                {
                    if (bNewRow)
                    {
                        sSql = "insert into CUSTOMER_CONTACT ( " +
                               "CUSTOMER_ID, CONTACT_ID, CONTACT_TYPE, NAME_1, NAME_2, CONTACT_NUMBER_1, CONTACT_NUMBER_2, CONTACT_NUMBER_3, EMAIL_ADDRESS, NOTES ) " +
                               "VALUES ( " +
                               "@CUSTOMER_ID, @CONTACT_ID, @CONTACT_TYPE, @NAME_1, @NAME_2, @CONTACT_NUMBER_1, @CONTACT_NUMBER_2, @CONTACT_NUMBER_3, @EMAIL_ADDRESS, @NOTES ) ";
                    }
                    else
                    {
                        sSql = "update CUSTOMER_CONTACT set " +
                               "CUSTOMER_ID = @CUSTOMER_ID, CONTACT_ID = @CONTACT_ID, CONTACT_TYPE = @CONTACT_TYPE, NAME_1 = @NAME_1, NAME_2 = @NAME_2, " +
                               "CONTACT_NUMBER_1 = @CONTACT_NUMBER_1, CONTACT_NUMBER_2 = @CONTACT_NUMBER_2, CONTACT_NUMBER_3 = @CONTACT_NUMBER_3, " +
                               "EMAIL_ADDRESS = @EMAIL_ADDRESS, NOTES = @NOTES " +
                               "where CUSTOMER_ID = @CUSTOMER_ID and CONTACT_ID = @CONTACT_ID";
                    }
                    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                    sqlCmd.Parameters.AddWithValue("@CUSTOMER_ID", sCustId.ToUpper());
                    sqlCmd.Parameters.AddWithValue("@CONTACT_ID", textContactId.Text.ToUpper());
                    sqlCmd.Parameters.AddWithValue("@NAME_1", textName1.Text);
                    sqlCmd.Parameters.AddWithValue("@NAME_2", textName2.Text);
                    sqlCmd.Parameters.AddWithValue("@CONTACT_NUMBER_1", textPhone1.Text);
                    sqlCmd.Parameters.AddWithValue("@CONTACT_NUMBER_2", textPhone2.Text);
                    sqlCmd.Parameters.AddWithValue("@CONTACT_NUMBER_3", textPhone3.Text);
                    sqlCmd.Parameters.AddWithValue("@EMAIL_ADDRESS", textEmail.Text);
                    sqlCmd.Parameters.AddWithValue("@NOTES", textNotes.Text);
                    if (cmbContactType.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@CONTACT_TYPE", cmbContactType.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@CONTACT_TYPE", "DEL");
                    }
                    sqlCmd.ExecuteNonQuery();
                }
                sqlCmd = null;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        private void pbCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pbDeleteCurrent_Click(object sender, EventArgs e)
        {
            DeleteRow(textContactId.Text);
        }

        private void DeleteRow(string sContactId)
        {
            string sSql;
            if (bNewRow == false && MessageBox.Show(this, "Are you sure you want to delete this contact?", "Delete Contact", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Hand) == DialogResult.Yes)
            {
                SqlCommand sqlCmd = new SqlCommand();
                sSql = "DELETE FROM CUSTOMER_CONTACT WHERE CUSTOMER_ID = @CUST_ID and CONTACT_ID = @CONTACT_ID";
                sqlCmd.Parameters.AddWithValue("@CUST_ID", sCustId);
                sqlCmd.Parameters.AddWithValue("@CONTACT_ID", sContactId);
                sqlCmd.Connection = SqlHost.DBConn;
                sqlCmd.CommandText = sSql;
                sqlCmd.ExecuteNonQuery();
                ClearFormDetails(false);
                this.ActiveControl = dgvData;
            }
            RetrieveCustomerDetails(sCustId);
            this.ActiveControl = dgvData;
            ClearFormDetails(false);
        }

        public void GenericTransfer(GenericParam g)
        {
            if (g.IdField == "CUSTOMER_ID")
            {
                sCustId = g.ValueField;
                RetrieveCustomerDetails(sCustId);
            }
        }

        private void RetrieveCustomerDetails(string sId)
        {
            string sSql;
            SqlCommand sqlCmd;
            SqlDataReader cReader;
            dsContacts = new DataSet();
            bool b;

            sSql = "select * from CUSTOMER where CUSTOMER_ID = @CUST_ID";
            sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
            sqlCmd.Parameters.AddWithValue("@CUST_ID", sId);
            cReader = sqlCmd.ExecuteReader();
            while (cReader.Read())
            {
                textCustId.Text = cReader["CUSTOMER_ID"].ToString();
                textCustName.Text = cReader["NAME_1"].ToString();
                this.Text = "Customer Contacts - " + textCustName.Text;
            }
            cReader.Close();

            sSql = "select A.CUSTOMER_ID, A.CONTACT_ID, B.DESCRIPTION, A.NAME_1, A.CONTACT_NUMBER_1, A.EMAIL_ADDRESS from CUSTOMER_CONTACT A left outer join REF_DATA B on A.CONTACT_TYPE = B.REF_ID where B.REF_TABLE_ID = 'CONTACTTYPE' and CUSTOMER_ID = '" + sId + "'";
            b = SqlHost.GetDataSet(sSql, "Contacts", ref dsContacts);
            dgvData.DataSource = dsContacts.Tables["Contacts"];
            dgvData.Refresh();

            FormatDataView();

        }
        
        private void FormatDataView()
        {
            dgvData.Columns["CUSTOMER_ID"].Visible = false;
            dgvData.Columns["CONTACT_ID"].Width = 80;
            dgvData.Columns["CONTACT_ID"].HeaderText = "Contact Id";
            dgvData.Columns["DESCRIPTION"].Width = 120;
            dgvData.Columns["DESCRIPTION"].HeaderText = "Contact Type";
            dgvData.Columns["NAME_1"].Width = 190;
            dgvData.Columns["NAME_1"].HeaderText = "Name";
            dgvData.Columns["CONTACT_NUMBER_1"].Width = 120;
            dgvData.Columns["CONTACT_NUMBER_1"].HeaderText = "Phone";
            dgvData.Columns["EMAIL_ADDRESS"].Width = 190;
            dgvData.Columns["EMAIL_ADDRESS"].HeaderText = "Email";
        }

    }

}

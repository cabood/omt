﻿namespace OMT.Forms
{
    partial class dlgBitwiseOptionPicker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbAccept = new System.Windows.Forms.Button();
            this.pbCancel = new System.Windows.Forms.Button();
            this.tlpGrid = new System.Windows.Forms.TableLayoutPanel();
            this.checkOption1 = new System.Windows.Forms.CheckBox();
            this.checkOption2 = new System.Windows.Forms.CheckBox();
            this.checkOption3 = new System.Windows.Forms.CheckBox();
            this.checkOption4 = new System.Windows.Forms.CheckBox();
            this.checkOption5 = new System.Windows.Forms.CheckBox();
            this.checkOption6 = new System.Windows.Forms.CheckBox();
            this.checkOption7 = new System.Windows.Forms.CheckBox();
            this.checkOption8 = new System.Windows.Forms.CheckBox();
            this.checkOption9 = new System.Windows.Forms.CheckBox();
            this.checkOption10 = new System.Windows.Forms.CheckBox();
            this.checkOption11 = new System.Windows.Forms.CheckBox();
            this.checkOption12 = new System.Windows.Forms.CheckBox();
            this.checkOption13 = new System.Windows.Forms.CheckBox();
            this.checkOption14 = new System.Windows.Forms.CheckBox();
            this.checkOption15 = new System.Windows.Forms.CheckBox();
            this.checkOption16 = new System.Windows.Forms.CheckBox();
            this.checkOption17 = new System.Windows.Forms.CheckBox();
            this.checkOption18 = new System.Windows.Forms.CheckBox();
            this.checkOption19 = new System.Windows.Forms.CheckBox();
            this.checkOption20 = new System.Windows.Forms.CheckBox();
            this.checkOption21 = new System.Windows.Forms.CheckBox();
            this.checkOption22 = new System.Windows.Forms.CheckBox();
            this.checkOption23 = new System.Windows.Forms.CheckBox();
            this.checkOption24 = new System.Windows.Forms.CheckBox();
            this.checkOption25 = new System.Windows.Forms.CheckBox();
            this.checkOption26 = new System.Windows.Forms.CheckBox();
            this.checkOption27 = new System.Windows.Forms.CheckBox();
            this.checkOption28 = new System.Windows.Forms.CheckBox();
            this.checkOption29 = new System.Windows.Forms.CheckBox();
            this.checkOption30 = new System.Windows.Forms.CheckBox();
            this.checkOption31 = new System.Windows.Forms.CheckBox();
            this.checkOption32 = new System.Windows.Forms.CheckBox();
            this.tlpGrid.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbAccept
            // 
            this.pbAccept.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pbAccept.Location = new System.Drawing.Point(159, 524);
            this.pbAccept.Margin = new System.Windows.Forms.Padding(4);
            this.pbAccept.Name = "pbAccept";
            this.pbAccept.Size = new System.Drawing.Size(116, 29);
            this.pbAccept.TabIndex = 1;
            this.pbAccept.Text = "&Accept";
            this.pbAccept.UseVisualStyleBackColor = true;
            this.pbAccept.Click += new System.EventHandler(this.pbAccept_Click);
            // 
            // pbCancel
            // 
            this.pbCancel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pbCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.pbCancel.Location = new System.Drawing.Point(283, 524);
            this.pbCancel.Margin = new System.Windows.Forms.Padding(4);
            this.pbCancel.Name = "pbCancel";
            this.pbCancel.Size = new System.Drawing.Size(116, 29);
            this.pbCancel.TabIndex = 2;
            this.pbCancel.Text = "&Cancel";
            this.pbCancel.UseVisualStyleBackColor = true;
            this.pbCancel.Click += new System.EventHandler(this.pbCancel_Click);
            // 
            // tlpGrid
            // 
            this.tlpGrid.ColumnCount = 2;
            this.tlpGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpGrid.Controls.Add(this.pbAccept, 0, 18);
            this.tlpGrid.Controls.Add(this.pbCancel, 1, 18);
            this.tlpGrid.Controls.Add(this.checkOption1, 0, 1);
            this.tlpGrid.Controls.Add(this.checkOption2, 1, 1);
            this.tlpGrid.Controls.Add(this.checkOption3, 0, 2);
            this.tlpGrid.Controls.Add(this.checkOption4, 1, 2);
            this.tlpGrid.Controls.Add(this.checkOption5, 0, 3);
            this.tlpGrid.Controls.Add(this.checkOption6, 1, 3);
            this.tlpGrid.Controls.Add(this.checkOption7, 0, 4);
            this.tlpGrid.Controls.Add(this.checkOption8, 1, 4);
            this.tlpGrid.Controls.Add(this.checkOption9, 0, 5);
            this.tlpGrid.Controls.Add(this.checkOption10, 1, 5);
            this.tlpGrid.Controls.Add(this.checkOption11, 0, 6);
            this.tlpGrid.Controls.Add(this.checkOption12, 1, 6);
            this.tlpGrid.Controls.Add(this.checkOption13, 0, 7);
            this.tlpGrid.Controls.Add(this.checkOption14, 1, 7);
            this.tlpGrid.Controls.Add(this.checkOption15, 0, 8);
            this.tlpGrid.Controls.Add(this.checkOption16, 1, 8);
            this.tlpGrid.Controls.Add(this.checkOption17, 0, 9);
            this.tlpGrid.Controls.Add(this.checkOption18, 1, 9);
            this.tlpGrid.Controls.Add(this.checkOption19, 0, 10);
            this.tlpGrid.Controls.Add(this.checkOption20, 1, 10);
            this.tlpGrid.Controls.Add(this.checkOption21, 0, 11);
            this.tlpGrid.Controls.Add(this.checkOption22, 1, 11);
            this.tlpGrid.Controls.Add(this.checkOption23, 0, 12);
            this.tlpGrid.Controls.Add(this.checkOption24, 1, 12);
            this.tlpGrid.Controls.Add(this.checkOption25, 0, 13);
            this.tlpGrid.Controls.Add(this.checkOption26, 1, 13);
            this.tlpGrid.Controls.Add(this.checkOption27, 0, 14);
            this.tlpGrid.Controls.Add(this.checkOption28, 1, 14);
            this.tlpGrid.Controls.Add(this.checkOption29, 0, 15);
            this.tlpGrid.Controls.Add(this.checkOption30, 1, 15);
            this.tlpGrid.Controls.Add(this.checkOption31, 0, 16);
            this.tlpGrid.Controls.Add(this.checkOption32, 1, 16);
            this.tlpGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpGrid.Location = new System.Drawing.Point(0, 0);
            this.tlpGrid.Name = "tlpGrid";
            this.tlpGrid.RowCount = 19;
            this.tlpGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tlpGrid.Size = new System.Drawing.Size(558, 557);
            this.tlpGrid.TabIndex = 3;
            // 
            // checkOption1
            // 
            this.checkOption1.AutoSize = true;
            this.checkOption1.Location = new System.Drawing.Point(3, 23);
            this.checkOption1.Name = "checkOption1";
            this.checkOption1.Size = new System.Drawing.Size(98, 21);
            this.checkOption1.TabIndex = 3;
            this.checkOption1.Text = "checkBox1";
            this.checkOption1.UseVisualStyleBackColor = true;
            // 
            // checkOption2
            // 
            this.checkOption2.AutoSize = true;
            this.checkOption2.Location = new System.Drawing.Point(282, 23);
            this.checkOption2.Name = "checkOption2";
            this.checkOption2.Size = new System.Drawing.Size(98, 21);
            this.checkOption2.TabIndex = 4;
            this.checkOption2.Text = "checkBox2";
            this.checkOption2.UseVisualStyleBackColor = true;
            // 
            // checkOption3
            // 
            this.checkOption3.AutoSize = true;
            this.checkOption3.Location = new System.Drawing.Point(3, 53);
            this.checkOption3.Name = "checkOption3";
            this.checkOption3.Size = new System.Drawing.Size(98, 21);
            this.checkOption3.TabIndex = 5;
            this.checkOption3.Text = "checkBox3";
            this.checkOption3.UseVisualStyleBackColor = true;
            // 
            // checkOption4
            // 
            this.checkOption4.AutoSize = true;
            this.checkOption4.Location = new System.Drawing.Point(282, 53);
            this.checkOption4.Name = "checkOption4";
            this.checkOption4.Size = new System.Drawing.Size(98, 21);
            this.checkOption4.TabIndex = 6;
            this.checkOption4.Text = "checkBox4";
            this.checkOption4.UseVisualStyleBackColor = true;
            // 
            // checkOption5
            // 
            this.checkOption5.AutoSize = true;
            this.checkOption5.Location = new System.Drawing.Point(3, 83);
            this.checkOption5.Name = "checkOption5";
            this.checkOption5.Size = new System.Drawing.Size(98, 21);
            this.checkOption5.TabIndex = 7;
            this.checkOption5.Text = "checkBox5";
            this.checkOption5.UseVisualStyleBackColor = true;
            // 
            // checkOption6
            // 
            this.checkOption6.AutoSize = true;
            this.checkOption6.Location = new System.Drawing.Point(282, 83);
            this.checkOption6.Name = "checkOption6";
            this.checkOption6.Size = new System.Drawing.Size(98, 21);
            this.checkOption6.TabIndex = 8;
            this.checkOption6.Text = "checkBox6";
            this.checkOption6.UseVisualStyleBackColor = true;
            // 
            // checkOption7
            // 
            this.checkOption7.AutoSize = true;
            this.checkOption7.Location = new System.Drawing.Point(3, 113);
            this.checkOption7.Name = "checkOption7";
            this.checkOption7.Size = new System.Drawing.Size(98, 21);
            this.checkOption7.TabIndex = 9;
            this.checkOption7.Text = "checkBox7";
            this.checkOption7.UseVisualStyleBackColor = true;
            // 
            // checkOption8
            // 
            this.checkOption8.AutoSize = true;
            this.checkOption8.Location = new System.Drawing.Point(282, 113);
            this.checkOption8.Name = "checkOption8";
            this.checkOption8.Size = new System.Drawing.Size(98, 21);
            this.checkOption8.TabIndex = 10;
            this.checkOption8.Text = "checkBox8";
            this.checkOption8.UseVisualStyleBackColor = true;
            // 
            // checkOption9
            // 
            this.checkOption9.AutoSize = true;
            this.checkOption9.Location = new System.Drawing.Point(3, 143);
            this.checkOption9.Name = "checkOption9";
            this.checkOption9.Size = new System.Drawing.Size(98, 21);
            this.checkOption9.TabIndex = 11;
            this.checkOption9.Text = "checkBox9";
            this.checkOption9.UseVisualStyleBackColor = true;
            // 
            // checkOption10
            // 
            this.checkOption10.AutoSize = true;
            this.checkOption10.Location = new System.Drawing.Point(282, 143);
            this.checkOption10.Name = "checkOption10";
            this.checkOption10.Size = new System.Drawing.Size(106, 21);
            this.checkOption10.TabIndex = 12;
            this.checkOption10.Text = "checkBox10";
            this.checkOption10.UseVisualStyleBackColor = true;
            // 
            // checkOption11
            // 
            this.checkOption11.AutoSize = true;
            this.checkOption11.Location = new System.Drawing.Point(3, 173);
            this.checkOption11.Name = "checkOption11";
            this.checkOption11.Size = new System.Drawing.Size(106, 21);
            this.checkOption11.TabIndex = 13;
            this.checkOption11.Text = "checkBox11";
            this.checkOption11.UseVisualStyleBackColor = true;
            // 
            // checkOption12
            // 
            this.checkOption12.AutoSize = true;
            this.checkOption12.Location = new System.Drawing.Point(282, 173);
            this.checkOption12.Name = "checkOption12";
            this.checkOption12.Size = new System.Drawing.Size(106, 21);
            this.checkOption12.TabIndex = 14;
            this.checkOption12.Text = "checkBox12";
            this.checkOption12.UseVisualStyleBackColor = true;
            // 
            // checkOption13
            // 
            this.checkOption13.AutoSize = true;
            this.checkOption13.Location = new System.Drawing.Point(3, 203);
            this.checkOption13.Name = "checkOption13";
            this.checkOption13.Size = new System.Drawing.Size(106, 21);
            this.checkOption13.TabIndex = 15;
            this.checkOption13.Text = "checkBox13";
            this.checkOption13.UseVisualStyleBackColor = true;
            // 
            // checkOption14
            // 
            this.checkOption14.AutoSize = true;
            this.checkOption14.Location = new System.Drawing.Point(282, 203);
            this.checkOption14.Name = "checkOption14";
            this.checkOption14.Size = new System.Drawing.Size(106, 21);
            this.checkOption14.TabIndex = 16;
            this.checkOption14.Text = "checkBox14";
            this.checkOption14.UseVisualStyleBackColor = true;
            // 
            // checkOption15
            // 
            this.checkOption15.AutoSize = true;
            this.checkOption15.Location = new System.Drawing.Point(3, 233);
            this.checkOption15.Name = "checkOption15";
            this.checkOption15.Size = new System.Drawing.Size(106, 21);
            this.checkOption15.TabIndex = 17;
            this.checkOption15.Text = "checkBox15";
            this.checkOption15.UseVisualStyleBackColor = true;
            // 
            // checkOption16
            // 
            this.checkOption16.AutoSize = true;
            this.checkOption16.Location = new System.Drawing.Point(282, 233);
            this.checkOption16.Name = "checkOption16";
            this.checkOption16.Size = new System.Drawing.Size(106, 21);
            this.checkOption16.TabIndex = 18;
            this.checkOption16.Text = "checkBox16";
            this.checkOption16.UseVisualStyleBackColor = true;
            // 
            // checkOption17
            // 
            this.checkOption17.AutoSize = true;
            this.checkOption17.Location = new System.Drawing.Point(3, 263);
            this.checkOption17.Name = "checkOption17";
            this.checkOption17.Size = new System.Drawing.Size(106, 21);
            this.checkOption17.TabIndex = 19;
            this.checkOption17.Text = "checkBox17";
            this.checkOption17.UseVisualStyleBackColor = true;
            // 
            // checkOption18
            // 
            this.checkOption18.AutoSize = true;
            this.checkOption18.Location = new System.Drawing.Point(282, 263);
            this.checkOption18.Name = "checkOption18";
            this.checkOption18.Size = new System.Drawing.Size(106, 21);
            this.checkOption18.TabIndex = 20;
            this.checkOption18.Text = "checkBox18";
            this.checkOption18.UseVisualStyleBackColor = true;
            // 
            // checkOption19
            // 
            this.checkOption19.AutoSize = true;
            this.checkOption19.Location = new System.Drawing.Point(3, 293);
            this.checkOption19.Name = "checkOption19";
            this.checkOption19.Size = new System.Drawing.Size(106, 21);
            this.checkOption19.TabIndex = 21;
            this.checkOption19.Text = "checkBox19";
            this.checkOption19.UseVisualStyleBackColor = true;
            // 
            // checkOption20
            // 
            this.checkOption20.AutoSize = true;
            this.checkOption20.Location = new System.Drawing.Point(282, 293);
            this.checkOption20.Name = "checkOption20";
            this.checkOption20.Size = new System.Drawing.Size(106, 21);
            this.checkOption20.TabIndex = 22;
            this.checkOption20.Text = "checkBox20";
            this.checkOption20.UseVisualStyleBackColor = true;
            // 
            // checkOption21
            // 
            this.checkOption21.AutoSize = true;
            this.checkOption21.Location = new System.Drawing.Point(3, 323);
            this.checkOption21.Name = "checkOption21";
            this.checkOption21.Size = new System.Drawing.Size(106, 21);
            this.checkOption21.TabIndex = 23;
            this.checkOption21.Text = "checkBox21";
            this.checkOption21.UseVisualStyleBackColor = true;
            // 
            // checkOption22
            // 
            this.checkOption22.AutoSize = true;
            this.checkOption22.Location = new System.Drawing.Point(282, 323);
            this.checkOption22.Name = "checkOption22";
            this.checkOption22.Size = new System.Drawing.Size(106, 21);
            this.checkOption22.TabIndex = 24;
            this.checkOption22.Text = "checkBox22";
            this.checkOption22.UseVisualStyleBackColor = true;
            // 
            // checkOption23
            // 
            this.checkOption23.AutoSize = true;
            this.checkOption23.Location = new System.Drawing.Point(3, 353);
            this.checkOption23.Name = "checkOption23";
            this.checkOption23.Size = new System.Drawing.Size(106, 21);
            this.checkOption23.TabIndex = 25;
            this.checkOption23.Text = "checkBox23";
            this.checkOption23.UseVisualStyleBackColor = true;
            // 
            // checkOption24
            // 
            this.checkOption24.AutoSize = true;
            this.checkOption24.Location = new System.Drawing.Point(282, 353);
            this.checkOption24.Name = "checkOption24";
            this.checkOption24.Size = new System.Drawing.Size(106, 21);
            this.checkOption24.TabIndex = 26;
            this.checkOption24.Text = "checkBox24";
            this.checkOption24.UseVisualStyleBackColor = true;
            // 
            // checkOption25
            // 
            this.checkOption25.AutoSize = true;
            this.checkOption25.Location = new System.Drawing.Point(3, 383);
            this.checkOption25.Name = "checkOption25";
            this.checkOption25.Size = new System.Drawing.Size(106, 21);
            this.checkOption25.TabIndex = 27;
            this.checkOption25.Text = "checkBox25";
            this.checkOption25.UseVisualStyleBackColor = true;
            // 
            // checkOption26
            // 
            this.checkOption26.AutoSize = true;
            this.checkOption26.Location = new System.Drawing.Point(282, 383);
            this.checkOption26.Name = "checkOption26";
            this.checkOption26.Size = new System.Drawing.Size(106, 21);
            this.checkOption26.TabIndex = 28;
            this.checkOption26.Text = "checkBox26";
            this.checkOption26.UseVisualStyleBackColor = true;
            // 
            // checkOption27
            // 
            this.checkOption27.AutoSize = true;
            this.checkOption27.Location = new System.Drawing.Point(3, 413);
            this.checkOption27.Name = "checkOption27";
            this.checkOption27.Size = new System.Drawing.Size(106, 21);
            this.checkOption27.TabIndex = 29;
            this.checkOption27.Text = "checkBox27";
            this.checkOption27.UseVisualStyleBackColor = true;
            // 
            // checkOption28
            // 
            this.checkOption28.AutoSize = true;
            this.checkOption28.Location = new System.Drawing.Point(282, 413);
            this.checkOption28.Name = "checkOption28";
            this.checkOption28.Size = new System.Drawing.Size(106, 21);
            this.checkOption28.TabIndex = 30;
            this.checkOption28.Text = "checkBox28";
            this.checkOption28.UseVisualStyleBackColor = true;
            // 
            // checkOption29
            // 
            this.checkOption29.AutoSize = true;
            this.checkOption29.Location = new System.Drawing.Point(3, 443);
            this.checkOption29.Name = "checkOption29";
            this.checkOption29.Size = new System.Drawing.Size(106, 21);
            this.checkOption29.TabIndex = 31;
            this.checkOption29.Text = "checkBox29";
            this.checkOption29.UseVisualStyleBackColor = true;
            // 
            // checkOption30
            // 
            this.checkOption30.AutoSize = true;
            this.checkOption30.Location = new System.Drawing.Point(282, 443);
            this.checkOption30.Name = "checkOption30";
            this.checkOption30.Size = new System.Drawing.Size(106, 21);
            this.checkOption30.TabIndex = 32;
            this.checkOption30.Text = "checkBox30";
            this.checkOption30.UseVisualStyleBackColor = true;
            // 
            // checkOption31
            // 
            this.checkOption31.AutoSize = true;
            this.checkOption31.Location = new System.Drawing.Point(3, 473);
            this.checkOption31.Name = "checkOption31";
            this.checkOption31.Size = new System.Drawing.Size(106, 21);
            this.checkOption31.TabIndex = 33;
            this.checkOption31.Text = "checkBox31";
            this.checkOption31.UseVisualStyleBackColor = true;
            // 
            // checkOption32
            // 
            this.checkOption32.AutoSize = true;
            this.checkOption32.Location = new System.Drawing.Point(282, 473);
            this.checkOption32.Name = "checkOption32";
            this.checkOption32.Size = new System.Drawing.Size(106, 21);
            this.checkOption32.TabIndex = 34;
            this.checkOption32.Text = "checkBox32";
            this.checkOption32.UseVisualStyleBackColor = true;
            // 
            // dlgBitwiseOptionPicker
            // 
            this.AcceptButton = this.pbAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.pbCancel;
            this.ClientSize = new System.Drawing.Size(558, 557);
            this.ControlBox = false;
            this.Controls.Add(this.tlpGrid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "dlgBitwiseOptionPicker";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Tick Desired Options";
            this.tlpGrid.ResumeLayout(false);
            this.tlpGrid.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button pbAccept;
        private System.Windows.Forms.Button pbCancel;
        private System.Windows.Forms.TableLayoutPanel tlpGrid;
        private System.Windows.Forms.CheckBox checkOption1;
        private System.Windows.Forms.CheckBox checkOption2;
        private System.Windows.Forms.CheckBox checkOption3;
        private System.Windows.Forms.CheckBox checkOption4;
        private System.Windows.Forms.CheckBox checkOption5;
        private System.Windows.Forms.CheckBox checkOption6;
        private System.Windows.Forms.CheckBox checkOption7;
        private System.Windows.Forms.CheckBox checkOption8;
        private System.Windows.Forms.CheckBox checkOption9;
        private System.Windows.Forms.CheckBox checkOption10;
        private System.Windows.Forms.CheckBox checkOption11;
        private System.Windows.Forms.CheckBox checkOption12;
        private System.Windows.Forms.CheckBox checkOption13;
        private System.Windows.Forms.CheckBox checkOption14;
        private System.Windows.Forms.CheckBox checkOption15;
        private System.Windows.Forms.CheckBox checkOption16;
        private System.Windows.Forms.CheckBox checkOption17;
        private System.Windows.Forms.CheckBox checkOption18;
        private System.Windows.Forms.CheckBox checkOption19;
        private System.Windows.Forms.CheckBox checkOption20;
        private System.Windows.Forms.CheckBox checkOption21;
        private System.Windows.Forms.CheckBox checkOption22;
        private System.Windows.Forms.CheckBox checkOption23;
        private System.Windows.Forms.CheckBox checkOption24;
        private System.Windows.Forms.CheckBox checkOption25;
        private System.Windows.Forms.CheckBox checkOption26;
        private System.Windows.Forms.CheckBox checkOption27;
        private System.Windows.Forms.CheckBox checkOption28;
        private System.Windows.Forms.CheckBox checkOption29;
        private System.Windows.Forms.CheckBox checkOption30;
        private System.Windows.Forms.CheckBox checkOption31;
        private System.Windows.Forms.CheckBox checkOption32;
    }
}
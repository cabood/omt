﻿namespace OMT.Forms
{
    partial class frmSupplierMaint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSupplierMaint));
            this.tlpBackground = new System.Windows.Forms.TableLayoutPanel();
            this.tlpFields = new System.Windows.Forms.TableLayoutPanel();
            this.lblName1 = new System.Windows.Forms.Label();
            this.lblName2 = new System.Windows.Forms.Label();
            this.textName1 = new System.Windows.Forms.TextBox();
            this.textName2 = new System.Windows.Forms.TextBox();
            this.tlpSuppId = new System.Windows.Forms.TableLayoutPanel();
            this.pbAllSuppIdSearch = new System.Windows.Forms.Button();
            this.imageListActions = new System.Windows.Forms.ImageList(this.components);
            this.pbSuppIdSearch = new System.Windows.Forms.Button();
            this.textSupplierId = new System.Windows.Forms.TextBox();
            this.tlpActions = new System.Windows.Forms.TableLayoutPanel();
            this.pbSave = new System.Windows.Forms.Button();
            this.pbCancel = new System.Windows.Forms.Button();
            this.pbDelete = new System.Windows.Forms.Button();
            this.lblActions = new System.Windows.Forms.Label();
            this.tlpParentSupp = new System.Windows.Forms.TableLayoutPanel();
            this.pbParentSuppIdSearch = new System.Windows.Forms.Button();
            this.textParentSuppId = new System.Windows.Forms.TextBox();
            this.textParentSuppName = new System.Windows.Forms.TextBox();
            this.lblDefaultPayTerms = new System.Windows.Forms.Label();
            this.cmbPaymentTerms = new System.Windows.Forms.ComboBox();
            this.lblDefaultPOType = new System.Windows.Forms.Label();
            this.cmbDefPOType = new System.Windows.Forms.ComboBox();
            this.cmbDefCurrency = new System.Windows.Forms.ComboBox();
            this.lblContact = new System.Windows.Forms.Label();
            this.textContact = new System.Windows.Forms.TextBox();
            this.lblMobile = new System.Windows.Forms.Label();
            this.textMobile = new System.Windows.Forms.TextBox();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblFax = new System.Windows.Forms.Label();
            this.lblEmailAddress = new System.Windows.Forms.Label();
            this.textPhone = new System.Windows.Forms.TextBox();
            this.textFax = new System.Windows.Forms.TextBox();
            this.textEmailAddress = new System.Windows.Forms.TextBox();
            this.lblAddressLine1 = new System.Windows.Forms.Label();
            this.textAddr1 = new System.Windows.Forms.TextBox();
            this.lblAddressLine2 = new System.Windows.Forms.Label();
            this.textAddr2 = new System.Windows.Forms.TextBox();
            this.lblAddressLine3 = new System.Windows.Forms.Label();
            this.textAddr3 = new System.Windows.Forms.TextBox();
            this.lblState = new System.Windows.Forms.Label();
            this.textAddrState = new System.Windows.Forms.TextBox();
            this.lblCountry = new System.Windows.Forms.Label();
            this.cmbCountry = new System.Windows.Forms.ComboBox();
            this.lblDefaultPort = new System.Windows.Forms.Label();
            this.cmbDefPort = new System.Windows.Forms.ComboBox();
            this.lblPostCode = new System.Windows.Forms.Label();
            this.textPostCode = new System.Windows.Forms.TextBox();
            this.lblABN = new System.Windows.Forms.Label();
            this.textABN = new System.Windows.Forms.TextBox();
            this.LBLEntityType = new System.Windows.Forms.Label();
            this.cmbEntityType = new System.Windows.Forms.ComboBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabContext = new System.Windows.Forms.TabPage();
            this.tlpContext = new System.Windows.Forms.TableLayoutPanel();
            this.dgvContext = new System.Windows.Forms.DataGridView();
            this.lblInternalCompanyID = new System.Windows.Forms.Label();
            this.lblSupplier = new System.Windows.Forms.Label();
            this.tlpContextActions = new System.Windows.Forms.TableLayoutPanel();
            this.pbContextDelete = new System.Windows.Forms.Button();
            this.pbContextSave = new System.Windows.Forms.Button();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.textContextSuppId = new System.Windows.Forms.TextBox();
            this.cmbContextId = new System.Windows.Forms.ComboBox();
            this.lblPaymentTerms = new System.Windows.Forms.Label();
            this.lblRebateNotes = new System.Windows.Forms.Label();
            this.lblDefPOType = new System.Windows.Forms.Label();
            this.lblProfileNotes = new System.Windows.Forms.Label();
            this.textContextProfileNotes = new System.Windows.Forms.TextBox();
            this.textContextRebateNotes = new System.Windows.Forms.TextBox();
            this.cmbContextPaymentTerms = new System.Windows.Forms.ComboBox();
            this.cmbContextPOType = new System.Windows.Forms.ComboBox();
            this.textContextVendorShortDesc = new System.Windows.Forms.TextBox();
            this.lblVendorShort = new System.Windows.Forms.Label();
            this.lblheaderImage = new System.Windows.Forms.Label();
            this.cmbContextDefInvHdrImg = new System.Windows.Forms.ComboBox();
            this.tabNotes = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblNotes = new System.Windows.Forms.Label();
            this.textRebateNotes = new System.Windows.Forms.TextBox();
            this.lblRebNotes = new System.Windows.Forms.Label();
            this.textNotes = new System.Windows.Forms.TextBox();
            this.tabCBA = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblCBASupplierType = new System.Windows.Forms.Label();
            this.cmbExtraCurrency = new System.Windows.Forms.ComboBox();
            this.lblExtraCurrency = new System.Windows.Forms.Label();
            this.cmbLockCode = new System.Windows.Forms.ComboBox();
            this.lblLockCode = new System.Windows.Forms.Label();
            this.textCBATerms = new System.Windows.Forms.TextBox();
            this.textCBASuppType = new System.Windows.Forms.TextBox();
            this.lblCBATerms = new System.Windows.Forms.Label();
            this.cbActive = new System.Windows.Forms.CheckBox();
            this.cbNonAccount = new System.Windows.Forms.CheckBox();
            this.lblDefaultCurrency = new System.Windows.Forms.Label();
            this.lblParentSupplier = new System.Windows.Forms.Label();
            this.lblSupplierID = new System.Windows.Forms.Label();
            this.tlpBackground.SuspendLayout();
            this.tlpFields.SuspendLayout();
            this.tlpSuppId.SuspendLayout();
            this.tlpActions.SuspendLayout();
            this.tlpParentSupp.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabContext.SuspendLayout();
            this.tlpContext.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContext)).BeginInit();
            this.tlpContextActions.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tabNotes.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabCBA.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpBackground
            // 
            this.tlpBackground.ColumnCount = 3;
            this.tlpBackground.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpBackground.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpBackground.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpBackground.Controls.Add(this.tlpFields, 1, 1);
            this.tlpBackground.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpBackground.Location = new System.Drawing.Point(0, 0);
            this.tlpBackground.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tlpBackground.Name = "tlpBackground";
            this.tlpBackground.RowCount = 3;
            this.tlpBackground.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpBackground.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpBackground.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpBackground.Size = new System.Drawing.Size(1401, 1020);
            this.tlpBackground.TabIndex = 0;
            // 
            // tlpFields
            // 
            this.tlpFields.ColumnCount = 4;
            this.tlpFields.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 154F));
            this.tlpFields.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpFields.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 145F));
            this.tlpFields.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpFields.Controls.Add(this.lblName1, 0, 2);
            this.tlpFields.Controls.Add(this.lblName2, 0, 3);
            this.tlpFields.Controls.Add(this.textName1, 1, 2);
            this.tlpFields.Controls.Add(this.textName2, 1, 3);
            this.tlpFields.Controls.Add(this.tlpSuppId, 1, 0);
            this.tlpFields.Controls.Add(this.tlpActions, 0, 22);
            this.tlpFields.Controls.Add(this.tlpParentSupp, 1, 15);
            this.tlpFields.Controls.Add(this.lblDefaultPayTerms, 0, 14);
            this.tlpFields.Controls.Add(this.cmbPaymentTerms, 1, 14);
            this.tlpFields.Controls.Add(this.lblDefaultPOType, 2, 13);
            this.tlpFields.Controls.Add(this.cmbDefPOType, 3, 13);
            this.tlpFields.Controls.Add(this.cmbDefCurrency, 1, 13);
            this.tlpFields.Controls.Add(this.lblContact, 0, 4);
            this.tlpFields.Controls.Add(this.textContact, 1, 4);
            this.tlpFields.Controls.Add(this.lblMobile, 2, 4);
            this.tlpFields.Controls.Add(this.textMobile, 3, 4);
            this.tlpFields.Controls.Add(this.lblPhone, 0, 5);
            this.tlpFields.Controls.Add(this.lblFax, 2, 5);
            this.tlpFields.Controls.Add(this.lblEmailAddress, 0, 6);
            this.tlpFields.Controls.Add(this.textPhone, 1, 5);
            this.tlpFields.Controls.Add(this.textFax, 3, 5);
            this.tlpFields.Controls.Add(this.textEmailAddress, 1, 6);
            this.tlpFields.Controls.Add(this.lblAddressLine1, 0, 7);
            this.tlpFields.Controls.Add(this.textAddr1, 1, 7);
            this.tlpFields.Controls.Add(this.lblAddressLine2, 0, 8);
            this.tlpFields.Controls.Add(this.textAddr2, 1, 8);
            this.tlpFields.Controls.Add(this.lblAddressLine3, 0, 9);
            this.tlpFields.Controls.Add(this.textAddr3, 1, 9);
            this.tlpFields.Controls.Add(this.lblState, 0, 10);
            this.tlpFields.Controls.Add(this.textAddrState, 1, 10);
            this.tlpFields.Controls.Add(this.lblCountry, 0, 11);
            this.tlpFields.Controls.Add(this.cmbCountry, 1, 11);
            this.tlpFields.Controls.Add(this.lblDefaultPort, 2, 11);
            this.tlpFields.Controls.Add(this.cmbDefPort, 3, 11);
            this.tlpFields.Controls.Add(this.lblPostCode, 2, 10);
            this.tlpFields.Controls.Add(this.textPostCode, 3, 10);
            this.tlpFields.Controls.Add(this.lblABN, 2, 0);
            this.tlpFields.Controls.Add(this.textABN, 3, 0);
            this.tlpFields.Controls.Add(this.LBLEntityType, 2, 1);
            this.tlpFields.Controls.Add(this.cmbEntityType, 3, 1);
            this.tlpFields.Controls.Add(this.tabControl1, 0, 17);
            this.tlpFields.Controls.Add(this.cbActive, 3, 6);
            this.tlpFields.Controls.Add(this.cbNonAccount, 3, 7);
            this.tlpFields.Controls.Add(this.lblDefaultCurrency, 0, 13);
            this.tlpFields.Controls.Add(this.lblParentSupplier, 0, 15);
            this.tlpFields.Controls.Add(this.lblSupplierID, 0, 0);
            this.tlpFields.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpFields.Location = new System.Drawing.Point(12, 13);
            this.tlpFields.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tlpFields.Name = "tlpFields";
            this.tlpFields.RowCount = 23;
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpFields.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tlpFields.Size = new System.Drawing.Size(1377, 994);
            this.tlpFields.TabIndex = 0;
            // 
            // lblName1
            // 
            this.lblName1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblName1.AutoSize = true;
            this.lblName1.Location = new System.Drawing.Point(4, 96);
            this.lblName1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName1.Name = "lblName1";
            this.lblName1.Size = new System.Drawing.Size(68, 20);
            this.lblName1.TabIndex = 1;
            this.lblName1.Text = "Name 1:";
            // 
            // lblName2
            // 
            this.lblName2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblName2.AutoSize = true;
            this.lblName2.Location = new System.Drawing.Point(4, 136);
            this.lblName2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName2.Name = "lblName2";
            this.lblName2.Size = new System.Drawing.Size(68, 20);
            this.lblName2.TabIndex = 2;
            this.lblName2.Text = "Name 2:";
            // 
            // textName1
            // 
            this.tlpFields.SetColumnSpan(this.textName1, 2);
            this.textName1.Dock = System.Windows.Forms.DockStyle.Top;
            this.textName1.Location = new System.Drawing.Point(158, 91);
            this.textName1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textName1.Name = "textName1";
            this.textName1.Size = new System.Drawing.Size(676, 26);
            this.textName1.TabIndex = 3;
            // 
            // textName2
            // 
            this.tlpFields.SetColumnSpan(this.textName2, 2);
            this.textName2.Dock = System.Windows.Forms.DockStyle.Top;
            this.textName2.Location = new System.Drawing.Point(158, 131);
            this.textName2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textName2.Name = "textName2";
            this.textName2.Size = new System.Drawing.Size(676, 26);
            this.textName2.TabIndex = 4;
            // 
            // tlpSuppId
            // 
            this.tlpSuppId.ColumnCount = 3;
            this.tlpSuppId.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 225F));
            this.tlpSuppId.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpSuppId.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpSuppId.Controls.Add(this.pbAllSuppIdSearch, 2, 0);
            this.tlpSuppId.Controls.Add(this.pbSuppIdSearch, 1, 0);
            this.tlpSuppId.Controls.Add(this.textSupplierId, 0, 0);
            this.tlpSuppId.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpSuppId.Location = new System.Drawing.Point(154, 0);
            this.tlpSuppId.Margin = new System.Windows.Forms.Padding(0);
            this.tlpSuppId.Name = "tlpSuppId";
            this.tlpSuppId.RowCount = 1;
            this.tlpSuppId.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpSuppId.Size = new System.Drawing.Size(539, 42);
            this.tlpSuppId.TabIndex = 0;
            this.tlpSuppId.TabStop = true;
            // 
            // pbAllSuppIdSearch
            // 
            this.pbAllSuppIdSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbAllSuppIdSearch.ImageKey = "Find32.png";
            this.pbAllSuppIdSearch.ImageList = this.imageListActions;
            this.pbAllSuppIdSearch.Location = new System.Drawing.Point(386, 5);
            this.pbAllSuppIdSearch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbAllSuppIdSearch.Name = "pbAllSuppIdSearch";
            this.pbAllSuppIdSearch.Size = new System.Drawing.Size(149, 32);
            this.pbAllSuppIdSearch.TabIndex = 1;
            this.pbAllSuppIdSearch.TabStop = false;
            this.pbAllSuppIdSearch.Text = "All Supp.";
            this.pbAllSuppIdSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.pbAllSuppIdSearch.UseVisualStyleBackColor = true;
            this.pbAllSuppIdSearch.Click += new System.EventHandler(this.pbAllSuppIdSearch_Click);
            // 
            // imageListActions
            // 
            this.imageListActions.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListActions.ImageStream")));
            this.imageListActions.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListActions.Images.SetKeyName(0, "Add32.png");
            this.imageListActions.Images.SetKeyName(1, "Cancel32.png");
            this.imageListActions.Images.SetKeyName(2, "Delete32.png");
            this.imageListActions.Images.SetKeyName(3, "Find32.png");
            // 
            // pbSuppIdSearch
            // 
            this.pbSuppIdSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.pbSuppIdSearch.ImageKey = "Find32.png";
            this.pbSuppIdSearch.ImageList = this.imageListActions;
            this.pbSuppIdSearch.Location = new System.Drawing.Point(229, 5);
            this.pbSuppIdSearch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbSuppIdSearch.Name = "pbSuppIdSearch";
            this.pbSuppIdSearch.Size = new System.Drawing.Size(149, 32);
            this.pbSuppIdSearch.TabIndex = 0;
            this.pbSuppIdSearch.TabStop = false;
            this.pbSuppIdSearch.Text = "My Supp.";
            this.pbSuppIdSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.pbSuppIdSearch.UseVisualStyleBackColor = true;
            this.pbSuppIdSearch.Click += new System.EventHandler(this.pbSuppIdSearch_Click);
            // 
            // textSupplierId
            // 
            this.textSupplierId.Dock = System.Windows.Forms.DockStyle.Top;
            this.textSupplierId.Location = new System.Drawing.Point(4, 5);
            this.textSupplierId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textSupplierId.Name = "textSupplierId";
            this.textSupplierId.Size = new System.Drawing.Size(217, 26);
            this.textSupplierId.TabIndex = 0;
            this.textSupplierId.Validating += new System.ComponentModel.CancelEventHandler(this.textSupplierId_Validating);
            // 
            // tlpActions
            // 
            this.tlpActions.ColumnCount = 6;
            this.tlpFields.SetColumnSpan(this.tlpActions, 4);
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpActions.Controls.Add(this.pbSave, 2, 0);
            this.tlpActions.Controls.Add(this.pbCancel, 3, 0);
            this.tlpActions.Controls.Add(this.pbDelete, 4, 0);
            this.tlpActions.Controls.Add(this.lblActions, 1, 0);
            this.tlpActions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpActions.Location = new System.Drawing.Point(4, 937);
            this.tlpActions.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tlpActions.Name = "tlpActions";
            this.tlpActions.RowCount = 1;
            this.tlpActions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpActions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tlpActions.Size = new System.Drawing.Size(1369, 52);
            this.tlpActions.TabIndex = 29;
            // 
            // pbSave
            // 
            this.pbSave.ImageKey = "Add32.png";
            this.pbSave.ImageList = this.imageListActions;
            this.pbSave.Location = new System.Drawing.Point(893, 5);
            this.pbSave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbSave.Name = "pbSave";
            this.pbSave.Size = new System.Drawing.Size(112, 42);
            this.pbSave.TabIndex = 29;
            this.pbSave.Text = "&Save";
            this.pbSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbSave.UseVisualStyleBackColor = true;
            this.pbSave.Click += new System.EventHandler(this.pbSave_Click);
            // 
            // pbCancel
            // 
            this.pbCancel.ImageKey = "Cancel32.png";
            this.pbCancel.ImageList = this.imageListActions;
            this.pbCancel.Location = new System.Drawing.Point(1043, 5);
            this.pbCancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbCancel.Name = "pbCancel";
            this.pbCancel.Size = new System.Drawing.Size(112, 42);
            this.pbCancel.TabIndex = 30;
            this.pbCancel.Text = "&Cancel";
            this.pbCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbCancel.UseVisualStyleBackColor = true;
            this.pbCancel.Click += new System.EventHandler(this.pbCancel_Click);
            // 
            // pbDelete
            // 
            this.pbDelete.ImageKey = "Delete32.png";
            this.pbDelete.ImageList = this.imageListActions;
            this.pbDelete.Location = new System.Drawing.Point(1193, 5);
            this.pbDelete.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbDelete.Name = "pbDelete";
            this.pbDelete.Size = new System.Drawing.Size(112, 42);
            this.pbDelete.TabIndex = 31;
            this.pbDelete.Text = "&Delete";
            this.pbDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbDelete.UseVisualStyleBackColor = true;
            this.pbDelete.Click += new System.EventHandler(this.pbDelete_Click);
            // 
            // lblActions
            // 
            this.lblActions.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblActions.AutoSize = true;
            this.lblActions.Location = new System.Drawing.Point(819, 16);
            this.lblActions.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblActions.Name = "lblActions";
            this.lblActions.Size = new System.Drawing.Size(66, 20);
            this.lblActions.TabIndex = 0;
            this.lblActions.Text = "Actions:";
            // 
            // tlpParentSupp
            // 
            this.tlpParentSupp.ColumnCount = 4;
            this.tlpFields.SetColumnSpan(this.tlpParentSupp, 3);
            this.tlpParentSupp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 225F));
            this.tlpParentSupp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tlpParentSupp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 450F));
            this.tlpParentSupp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpParentSupp.Controls.Add(this.pbParentSuppIdSearch, 1, 0);
            this.tlpParentSupp.Controls.Add(this.textParentSuppId, 0, 0);
            this.tlpParentSupp.Controls.Add(this.textParentSuppName, 2, 0);
            this.tlpParentSupp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpParentSupp.Location = new System.Drawing.Point(154, 581);
            this.tlpParentSupp.Margin = new System.Windows.Forms.Padding(0);
            this.tlpParentSupp.Name = "tlpParentSupp";
            this.tlpParentSupp.RowCount = 1;
            this.tlpParentSupp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpParentSupp.Size = new System.Drawing.Size(1223, 40);
            this.tlpParentSupp.TabIndex = 20;
            // 
            // pbParentSuppIdSearch
            // 
            this.pbParentSuppIdSearch.ImageKey = "Find32.png";
            this.pbParentSuppIdSearch.ImageList = this.imageListActions;
            this.pbParentSuppIdSearch.Location = new System.Drawing.Point(229, 5);
            this.pbParentSuppIdSearch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbParentSuppIdSearch.Name = "pbParentSuppIdSearch";
            this.pbParentSuppIdSearch.Size = new System.Drawing.Size(28, 30);
            this.pbParentSuppIdSearch.TabIndex = 17;
            this.pbParentSuppIdSearch.TabStop = false;
            this.pbParentSuppIdSearch.UseVisualStyleBackColor = true;
            this.pbParentSuppIdSearch.Click += new System.EventHandler(this.pbParentSuppIdSearch_Click);
            // 
            // textParentSuppId
            // 
            this.textParentSuppId.Location = new System.Drawing.Point(4, 5);
            this.textParentSuppId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textParentSuppId.Name = "textParentSuppId";
            this.textParentSuppId.Size = new System.Drawing.Size(214, 26);
            this.textParentSuppId.TabIndex = 20;
            this.textParentSuppId.Validating += new System.ComponentModel.CancelEventHandler(this.textParentSuppId_Validating);
            // 
            // textParentSuppName
            // 
            this.textParentSuppName.Location = new System.Drawing.Point(268, 5);
            this.textParentSuppName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textParentSuppName.Name = "textParentSuppName";
            this.textParentSuppName.ReadOnly = true;
            this.textParentSuppName.Size = new System.Drawing.Size(439, 26);
            this.textParentSuppName.TabIndex = 18;
            this.textParentSuppName.TabStop = false;
            // 
            // lblDefaultPayTerms
            // 
            this.lblDefaultPayTerms.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDefaultPayTerms.AutoSize = true;
            this.lblDefaultPayTerms.Location = new System.Drawing.Point(4, 551);
            this.lblDefaultPayTerms.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDefaultPayTerms.Name = "lblDefaultPayTerms";
            this.lblDefaultPayTerms.Size = new System.Drawing.Size(143, 20);
            this.lblDefaultPayTerms.TabIndex = 6;
            this.lblDefaultPayTerms.Text = "Default Pay Terms:";
            // 
            // cmbPaymentTerms
            // 
            this.cmbPaymentTerms.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbPaymentTerms.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbPaymentTerms.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbPaymentTerms.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPaymentTerms.FormattingEnabled = true;
            this.cmbPaymentTerms.Location = new System.Drawing.Point(158, 546);
            this.cmbPaymentTerms.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbPaymentTerms.Name = "cmbPaymentTerms";
            this.cmbPaymentTerms.Size = new System.Drawing.Size(531, 28);
            this.cmbPaymentTerms.TabIndex = 19;
            // 
            // lblDefaultPOType
            // 
            this.lblDefaultPOType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDefaultPOType.AutoSize = true;
            this.lblDefaultPOType.Location = new System.Drawing.Point(697, 511);
            this.lblDefaultPOType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDefaultPOType.Name = "lblDefaultPOType";
            this.lblDefaultPOType.Size = new System.Drawing.Size(129, 20);
            this.lblDefaultPOType.TabIndex = 20;
            this.lblDefaultPOType.Text = "Default PO Type:";
            // 
            // cmbDefPOType
            // 
            this.cmbDefPOType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbDefPOType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDefPOType.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbDefPOType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefPOType.FormattingEnabled = true;
            this.cmbDefPOType.Location = new System.Drawing.Point(842, 506);
            this.cmbDefPOType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbDefPOType.Name = "cmbDefPOType";
            this.cmbDefPOType.Size = new System.Drawing.Size(531, 28);
            this.cmbDefPOType.TabIndex = 18;
            // 
            // cmbDefCurrency
            // 
            this.cmbDefCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbDefCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDefCurrency.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbDefCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefCurrency.FormattingEnabled = true;
            this.cmbDefCurrency.Location = new System.Drawing.Point(158, 506);
            this.cmbDefCurrency.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbDefCurrency.Name = "cmbDefCurrency";
            this.cmbDefCurrency.Size = new System.Drawing.Size(531, 28);
            this.cmbDefCurrency.TabIndex = 17;
            // 
            // lblContact
            // 
            this.lblContact.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblContact.AutoSize = true;
            this.lblContact.Location = new System.Drawing.Point(4, 176);
            this.lblContact.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblContact.Name = "lblContact";
            this.lblContact.Size = new System.Drawing.Size(69, 20);
            this.lblContact.TabIndex = 17;
            this.lblContact.Text = "Contact:";
            // 
            // textContact
            // 
            this.textContact.Dock = System.Windows.Forms.DockStyle.Top;
            this.textContact.Location = new System.Drawing.Point(158, 171);
            this.textContact.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textContact.Name = "textContact";
            this.textContact.Size = new System.Drawing.Size(531, 26);
            this.textContact.TabIndex = 5;
            // 
            // lblMobile
            // 
            this.lblMobile.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblMobile.AutoSize = true;
            this.lblMobile.Location = new System.Drawing.Point(697, 176);
            this.lblMobile.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMobile.Name = "lblMobile";
            this.lblMobile.Size = new System.Drawing.Size(59, 20);
            this.lblMobile.TabIndex = 27;
            this.lblMobile.Text = "Mobile:";
            // 
            // textMobile
            // 
            this.textMobile.Dock = System.Windows.Forms.DockStyle.Top;
            this.textMobile.Location = new System.Drawing.Point(842, 171);
            this.textMobile.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textMobile.Name = "textMobile";
            this.textMobile.Size = new System.Drawing.Size(531, 26);
            this.textMobile.TabIndex = 6;
            // 
            // lblPhone
            // 
            this.lblPhone.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(4, 216);
            this.lblPhone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(59, 20);
            this.lblPhone.TabIndex = 39;
            this.lblPhone.Text = "Phone:";
            // 
            // lblFax
            // 
            this.lblFax.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblFax.AutoSize = true;
            this.lblFax.Location = new System.Drawing.Point(697, 216);
            this.lblFax.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFax.Name = "lblFax";
            this.lblFax.Size = new System.Drawing.Size(39, 20);
            this.lblFax.TabIndex = 40;
            this.lblFax.Text = "Fax:";
            // 
            // lblEmailAddress
            // 
            this.lblEmailAddress.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblEmailAddress.AutoSize = true;
            this.lblEmailAddress.Location = new System.Drawing.Point(4, 256);
            this.lblEmailAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmailAddress.Name = "lblEmailAddress";
            this.lblEmailAddress.Size = new System.Drawing.Size(115, 20);
            this.lblEmailAddress.TabIndex = 41;
            this.lblEmailAddress.Text = "Email Address:";
            // 
            // textPhone
            // 
            this.textPhone.Dock = System.Windows.Forms.DockStyle.Top;
            this.textPhone.Location = new System.Drawing.Point(158, 211);
            this.textPhone.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textPhone.Name = "textPhone";
            this.textPhone.Size = new System.Drawing.Size(531, 26);
            this.textPhone.TabIndex = 7;
            // 
            // textFax
            // 
            this.textFax.Dock = System.Windows.Forms.DockStyle.Top;
            this.textFax.Location = new System.Drawing.Point(842, 211);
            this.textFax.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textFax.Name = "textFax";
            this.textFax.Size = new System.Drawing.Size(531, 26);
            this.textFax.TabIndex = 8;
            // 
            // textEmailAddress
            // 
            this.tlpFields.SetColumnSpan(this.textEmailAddress, 2);
            this.textEmailAddress.Dock = System.Windows.Forms.DockStyle.Top;
            this.textEmailAddress.Location = new System.Drawing.Point(158, 251);
            this.textEmailAddress.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textEmailAddress.Name = "textEmailAddress";
            this.textEmailAddress.Size = new System.Drawing.Size(676, 26);
            this.textEmailAddress.TabIndex = 9;
            // 
            // lblAddressLine1
            // 
            this.lblAddressLine1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblAddressLine1.AutoSize = true;
            this.lblAddressLine1.Location = new System.Drawing.Point(4, 296);
            this.lblAddressLine1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddressLine1.Name = "lblAddressLine1";
            this.lblAddressLine1.Size = new System.Drawing.Size(119, 20);
            this.lblAddressLine1.TabIndex = 29;
            this.lblAddressLine1.Text = "Address Line 1:";
            // 
            // textAddr1
            // 
            this.tlpFields.SetColumnSpan(this.textAddr1, 2);
            this.textAddr1.Dock = System.Windows.Forms.DockStyle.Top;
            this.textAddr1.Location = new System.Drawing.Point(158, 291);
            this.textAddr1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textAddr1.Name = "textAddr1";
            this.textAddr1.Size = new System.Drawing.Size(676, 26);
            this.textAddr1.TabIndex = 10;
            // 
            // lblAddressLine2
            // 
            this.lblAddressLine2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblAddressLine2.AutoSize = true;
            this.lblAddressLine2.Location = new System.Drawing.Point(4, 336);
            this.lblAddressLine2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddressLine2.Name = "lblAddressLine2";
            this.lblAddressLine2.Size = new System.Drawing.Size(119, 20);
            this.lblAddressLine2.TabIndex = 30;
            this.lblAddressLine2.Text = "Address Line 2:";
            // 
            // textAddr2
            // 
            this.tlpFields.SetColumnSpan(this.textAddr2, 2);
            this.textAddr2.Dock = System.Windows.Forms.DockStyle.Top;
            this.textAddr2.Location = new System.Drawing.Point(158, 331);
            this.textAddr2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textAddr2.Name = "textAddr2";
            this.textAddr2.Size = new System.Drawing.Size(676, 26);
            this.textAddr2.TabIndex = 11;
            // 
            // lblAddressLine3
            // 
            this.lblAddressLine3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblAddressLine3.AutoSize = true;
            this.lblAddressLine3.Location = new System.Drawing.Point(4, 376);
            this.lblAddressLine3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddressLine3.Name = "lblAddressLine3";
            this.lblAddressLine3.Size = new System.Drawing.Size(119, 20);
            this.lblAddressLine3.TabIndex = 31;
            this.lblAddressLine3.Text = "Address Line 3:";
            // 
            // textAddr3
            // 
            this.tlpFields.SetColumnSpan(this.textAddr3, 2);
            this.textAddr3.Dock = System.Windows.Forms.DockStyle.Top;
            this.textAddr3.Location = new System.Drawing.Point(158, 371);
            this.textAddr3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textAddr3.Name = "textAddr3";
            this.textAddr3.Size = new System.Drawing.Size(676, 26);
            this.textAddr3.TabIndex = 12;
            // 
            // lblState
            // 
            this.lblState.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(4, 416);
            this.lblState.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(52, 20);
            this.lblState.TabIndex = 35;
            this.lblState.Text = "State:";
            // 
            // textAddrState
            // 
            this.textAddrState.Dock = System.Windows.Forms.DockStyle.Top;
            this.textAddrState.Location = new System.Drawing.Point(158, 411);
            this.textAddrState.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textAddrState.Name = "textAddrState";
            this.textAddrState.Size = new System.Drawing.Size(531, 26);
            this.textAddrState.TabIndex = 13;
            // 
            // lblCountry
            // 
            this.lblCountry.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblCountry.AutoSize = true;
            this.lblCountry.Location = new System.Drawing.Point(4, 456);
            this.lblCountry.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Size = new System.Drawing.Size(68, 20);
            this.lblCountry.TabIndex = 4;
            this.lblCountry.Text = "Country:";
            // 
            // cmbCountry
            // 
            this.cmbCountry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbCountry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCountry.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCountry.FormattingEnabled = true;
            this.cmbCountry.Location = new System.Drawing.Point(158, 451);
            this.cmbCountry.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbCountry.Name = "cmbCountry";
            this.cmbCountry.Size = new System.Drawing.Size(531, 28);
            this.cmbCountry.TabIndex = 15;
            // 
            // lblDefaultPort
            // 
            this.lblDefaultPort.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDefaultPort.AutoSize = true;
            this.lblDefaultPort.Location = new System.Drawing.Point(697, 456);
            this.lblDefaultPort.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDefaultPort.Name = "lblDefaultPort";
            this.lblDefaultPort.Size = new System.Drawing.Size(98, 20);
            this.lblDefaultPort.TabIndex = 5;
            this.lblDefaultPort.Text = "Default Port:";
            // 
            // cmbDefPort
            // 
            this.cmbDefPort.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbDefPort.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDefPort.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbDefPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefPort.FormattingEnabled = true;
            this.cmbDefPort.Location = new System.Drawing.Point(842, 451);
            this.cmbDefPort.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbDefPort.Name = "cmbDefPort";
            this.cmbDefPort.Size = new System.Drawing.Size(531, 28);
            this.cmbDefPort.TabIndex = 16;
            // 
            // lblPostCode
            // 
            this.lblPostCode.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblPostCode.AutoSize = true;
            this.lblPostCode.Location = new System.Drawing.Point(697, 416);
            this.lblPostCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPostCode.Name = "lblPostCode";
            this.lblPostCode.Size = new System.Drawing.Size(87, 20);
            this.lblPostCode.TabIndex = 36;
            this.lblPostCode.Text = "Post Code:";
            // 
            // textPostCode
            // 
            this.textPostCode.Dock = System.Windows.Forms.DockStyle.Top;
            this.textPostCode.Location = new System.Drawing.Point(842, 411);
            this.textPostCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textPostCode.Name = "textPostCode";
            this.textPostCode.Size = new System.Drawing.Size(531, 26);
            this.textPostCode.TabIndex = 14;
            // 
            // lblABN
            // 
            this.lblABN.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblABN.AutoSize = true;
            this.lblABN.Location = new System.Drawing.Point(697, 11);
            this.lblABN.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblABN.Name = "lblABN";
            this.lblABN.Size = new System.Drawing.Size(58, 20);
            this.lblABN.TabIndex = 45;
            this.lblABN.Text = "A.B.N.:";
            // 
            // textABN
            // 
            this.textABN.Dock = System.Windows.Forms.DockStyle.Top;
            this.textABN.Location = new System.Drawing.Point(842, 5);
            this.textABN.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textABN.Name = "textABN";
            this.textABN.Size = new System.Drawing.Size(531, 26);
            this.textABN.TabIndex = 1;
            // 
            // LBLEntityType
            // 
            this.LBLEntityType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.LBLEntityType.AutoSize = true;
            this.LBLEntityType.Location = new System.Drawing.Point(697, 54);
            this.LBLEntityType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LBLEntityType.Name = "LBLEntityType";
            this.LBLEntityType.Size = new System.Drawing.Size(91, 20);
            this.LBLEntityType.TabIndex = 50;
            this.LBLEntityType.Text = "Entity Type:";
            // 
            // cmbEntityType
            // 
            this.cmbEntityType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbEntityType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbEntityType.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbEntityType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEntityType.FormattingEnabled = true;
            this.cmbEntityType.Location = new System.Drawing.Point(842, 47);
            this.cmbEntityType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbEntityType.Name = "cmbEntityType";
            this.cmbEntityType.Size = new System.Drawing.Size(531, 28);
            this.cmbEntityType.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tlpFields.SetColumnSpan(this.tabControl1, 4);
            this.tabControl1.Controls.Add(this.tabContext);
            this.tabControl1.Controls.Add(this.tabNotes);
            this.tabControl1.Controls.Add(this.tabCBA);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(4, 641);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabControl1.Name = "tabControl1";
            this.tlpFields.SetRowSpan(this.tabControl1, 5);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1369, 286);
            this.tabControl1.TabIndex = 21;
            // 
            // tabContext
            // 
            this.tabContext.Controls.Add(this.tlpContext);
            this.tabContext.Location = new System.Drawing.Point(4, 29);
            this.tabContext.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabContext.Name = "tabContext";
            this.tabContext.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabContext.Size = new System.Drawing.Size(1361, 253);
            this.tabContext.TabIndex = 0;
            this.tabContext.Text = "Company Assignments";
            this.tabContext.UseVisualStyleBackColor = true;
            // 
            // tlpContext
            // 
            this.tlpContext.ColumnCount = 6;
            this.tlpContext.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 135F));
            this.tlpContext.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tlpContext.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 168F));
            this.tlpContext.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tlpContext.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 168F));
            this.tlpContext.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tlpContext.Controls.Add(this.dgvContext, 0, 0);
            this.tlpContext.Controls.Add(this.lblInternalCompanyID, 2, 0);
            this.tlpContext.Controls.Add(this.lblSupplier, 2, 1);
            this.tlpContext.Controls.Add(this.tlpContextActions, 1, 5);
            this.tlpContext.Controls.Add(this.tableLayoutPanel6, 3, 1);
            this.tlpContext.Controls.Add(this.cmbContextId, 3, 0);
            this.tlpContext.Controls.Add(this.lblPaymentTerms, 4, 0);
            this.tlpContext.Controls.Add(this.lblRebateNotes, 4, 3);
            this.tlpContext.Controls.Add(this.lblDefPOType, 4, 1);
            this.tlpContext.Controls.Add(this.lblProfileNotes, 2, 3);
            this.tlpContext.Controls.Add(this.textContextProfileNotes, 3, 3);
            this.tlpContext.Controls.Add(this.textContextRebateNotes, 5, 3);
            this.tlpContext.Controls.Add(this.cmbContextPaymentTerms, 5, 0);
            this.tlpContext.Controls.Add(this.cmbContextPOType, 5, 1);
            this.tlpContext.Controls.Add(this.textContextVendorShortDesc, 3, 5);
            this.tlpContext.Controls.Add(this.lblVendorShort, 2, 5);
            this.tlpContext.Controls.Add(this.lblheaderImage, 4, 5);
            this.tlpContext.Controls.Add(this.cmbContextDefInvHdrImg, 5, 5);
            this.tlpContext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpContext.Location = new System.Drawing.Point(4, 5);
            this.tlpContext.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tlpContext.Name = "tlpContext";
            this.tlpContext.RowCount = 6;
            this.tlpContext.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpContext.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpContext.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tlpContext.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpContext.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpContext.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tlpContext.Size = new System.Drawing.Size(1353, 243);
            this.tlpContext.TabIndex = 21;
            // 
            // dgvContext
            // 
            this.dgvContext.AllowUserToAddRows = false;
            this.dgvContext.AllowUserToDeleteRows = false;
            this.dgvContext.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tlpContext.SetColumnSpan(this.dgvContext, 2);
            this.dgvContext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvContext.Location = new System.Drawing.Point(4, 5);
            this.dgvContext.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvContext.MultiSelect = false;
            this.dgvContext.Name = "dgvContext";
            this.dgvContext.ReadOnly = true;
            this.dgvContext.RowHeadersVisible = false;
            this.dgvContext.RowHeadersWidth = 62;
            this.tlpContext.SetRowSpan(this.dgvContext, 5);
            this.dgvContext.Size = new System.Drawing.Size(391, 178);
            this.dgvContext.TabIndex = 21;
            this.dgvContext.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvContext_CellContentDoubleClick);
            this.dgvContext.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvContext_CellDoubleClick);
            // 
            // lblInternalCompanyID
            // 
            this.lblInternalCompanyID.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblInternalCompanyID.AutoSize = true;
            this.lblInternalCompanyID.Location = new System.Drawing.Point(403, 10);
            this.lblInternalCompanyID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInternalCompanyID.Name = "lblInternalCompanyID";
            this.lblInternalCompanyID.Size = new System.Drawing.Size(159, 20);
            this.lblInternalCompanyID.TabIndex = 28;
            this.lblInternalCompanyID.Text = "Internal Company ID:";
            // 
            // lblSupplier
            // 
            this.lblSupplier.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSupplier.AutoSize = true;
            this.lblSupplier.Location = new System.Drawing.Point(403, 50);
            this.lblSupplier.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSupplier.Name = "lblSupplier";
            this.lblSupplier.Size = new System.Drawing.Size(92, 20);
            this.lblSupplier.TabIndex = 29;
            this.lblSupplier.Text = "Supplier ID:";
            // 
            // tlpContextActions
            // 
            this.tlpContextActions.ColumnCount = 3;
            this.tlpContextActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpContextActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpContextActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpContextActions.Controls.Add(this.pbContextDelete, 1, 0);
            this.tlpContextActions.Controls.Add(this.pbContextSave, 0, 0);
            this.tlpContextActions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpContextActions.Location = new System.Drawing.Point(135, 188);
            this.tlpContextActions.Margin = new System.Windows.Forms.Padding(0);
            this.tlpContextActions.Name = "tlpContextActions";
            this.tlpContextActions.RowCount = 1;
            this.tlpContextActions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpContextActions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tlpContextActions.Size = new System.Drawing.Size(264, 55);
            this.tlpContextActions.TabIndex = 33;
            // 
            // pbContextDelete
            // 
            this.pbContextDelete.ImageKey = "Delete32.png";
            this.pbContextDelete.ImageList = this.imageListActions;
            this.pbContextDelete.Location = new System.Drawing.Point(154, 5);
            this.pbContextDelete.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbContextDelete.Name = "pbContextDelete";
            this.pbContextDelete.Size = new System.Drawing.Size(112, 37);
            this.pbContextDelete.TabIndex = 32;
            this.pbContextDelete.TabStop = false;
            this.pbContextDelete.Text = "&Delete";
            this.pbContextDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbContextDelete.UseVisualStyleBackColor = true;
            this.pbContextDelete.Click += new System.EventHandler(this.pbContextDelete_Click);
            // 
            // pbContextSave
            // 
            this.pbContextSave.ImageKey = "Add32.png";
            this.pbContextSave.ImageList = this.imageListActions;
            this.pbContextSave.Location = new System.Drawing.Point(4, 5);
            this.pbContextSave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbContextSave.Name = "pbContextSave";
            this.pbContextSave.Size = new System.Drawing.Size(112, 37);
            this.pbContextSave.TabIndex = 28;
            this.pbContextSave.Text = "&Save";
            this.pbContextSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbContextSave.UseVisualStyleBackColor = true;
            this.pbContextSave.Click += new System.EventHandler(this.pbContextSave_Click);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.textContextSuppId, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(567, 40);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(308, 40);
            this.tableLayoutPanel6.TabIndex = 23;
            // 
            // textContextSuppId
            // 
            this.textContextSuppId.Dock = System.Windows.Forms.DockStyle.Top;
            this.textContextSuppId.Location = new System.Drawing.Point(4, 5);
            this.textContextSuppId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textContextSuppId.Name = "textContextSuppId";
            this.textContextSuppId.Size = new System.Drawing.Size(146, 26);
            this.textContextSuppId.TabIndex = 23;
            // 
            // cmbContextId
            // 
            this.cmbContextId.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbContextId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContextId.FormattingEnabled = true;
            this.cmbContextId.Location = new System.Drawing.Point(571, 5);
            this.cmbContextId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbContextId.Name = "cmbContextId";
            this.cmbContextId.Size = new System.Drawing.Size(300, 28);
            this.cmbContextId.TabIndex = 22;
            this.cmbContextId.SelectedIndexChanged += new System.EventHandler(this.cmbContextId_SelectedIndexChanged);
            // 
            // lblPaymentTerms
            // 
            this.lblPaymentTerms.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblPaymentTerms.AutoSize = true;
            this.lblPaymentTerms.Location = new System.Drawing.Point(878, 10);
            this.lblPaymentTerms.Name = "lblPaymentTerms";
            this.lblPaymentTerms.Size = new System.Drawing.Size(123, 20);
            this.lblPaymentTerms.TabIndex = 37;
            this.lblPaymentTerms.Text = "Payment Terms:";
            // 
            // lblRebateNotes
            // 
            this.lblRebateNotes.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRebateNotes.AutoSize = true;
            this.lblRebateNotes.Location = new System.Drawing.Point(878, 102);
            this.lblRebateNotes.Name = "lblRebateNotes";
            this.lblRebateNotes.Size = new System.Drawing.Size(112, 20);
            this.lblRebateNotes.TabIndex = 38;
            this.lblRebateNotes.Text = "Rebate Notes:";
            // 
            // lblDefPOType
            // 
            this.lblDefPOType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDefPOType.AutoSize = true;
            this.lblDefPOType.Location = new System.Drawing.Point(878, 50);
            this.lblDefPOType.Name = "lblDefPOType";
            this.lblDefPOType.Size = new System.Drawing.Size(129, 20);
            this.lblDefPOType.TabIndex = 39;
            this.lblDefPOType.Text = "Default PO Type:";
            // 
            // lblProfileNotes
            // 
            this.lblProfileNotes.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblProfileNotes.AutoSize = true;
            this.lblProfileNotes.Location = new System.Drawing.Point(402, 102);
            this.lblProfileNotes.Name = "lblProfileNotes";
            this.lblProfileNotes.Size = new System.Drawing.Size(103, 20);
            this.lblProfileNotes.TabIndex = 36;
            this.lblProfileNotes.Text = "Profile Notes:";
            // 
            // textContextProfileNotes
            // 
            this.textContextProfileNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textContextProfileNotes.Location = new System.Drawing.Point(570, 95);
            this.textContextProfileNotes.Multiline = true;
            this.textContextProfileNotes.Name = "textContextProfileNotes";
            this.tlpContext.SetRowSpan(this.textContextProfileNotes, 2);
            this.textContextProfileNotes.Size = new System.Drawing.Size(302, 90);
            this.textContextProfileNotes.TabIndex = 24;
            // 
            // textContextRebateNotes
            // 
            this.textContextRebateNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textContextRebateNotes.Location = new System.Drawing.Point(1046, 95);
            this.textContextRebateNotes.Multiline = true;
            this.textContextRebateNotes.Name = "textContextRebateNotes";
            this.tlpContext.SetRowSpan(this.textContextRebateNotes, 2);
            this.textContextRebateNotes.Size = new System.Drawing.Size(304, 90);
            this.textContextRebateNotes.TabIndex = 27;
            // 
            // cmbContextPaymentTerms
            // 
            this.cmbContextPaymentTerms.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbContextPaymentTerms.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContextPaymentTerms.FormattingEnabled = true;
            this.cmbContextPaymentTerms.Location = new System.Drawing.Point(1046, 3);
            this.cmbContextPaymentTerms.Name = "cmbContextPaymentTerms";
            this.cmbContextPaymentTerms.Size = new System.Drawing.Size(304, 28);
            this.cmbContextPaymentTerms.TabIndex = 25;
            // 
            // cmbContextPOType
            // 
            this.cmbContextPOType.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbContextPOType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContextPOType.FormattingEnabled = true;
            this.cmbContextPOType.Location = new System.Drawing.Point(1046, 43);
            this.cmbContextPOType.Name = "cmbContextPOType";
            this.cmbContextPOType.Size = new System.Drawing.Size(304, 28);
            this.cmbContextPOType.TabIndex = 26;
            // 
            // textContextVendorShortDesc
            // 
            this.textContextVendorShortDesc.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textContextVendorShortDesc.Location = new System.Drawing.Point(570, 202);
            this.textContextVendorShortDesc.Name = "textContextVendorShortDesc";
            this.textContextVendorShortDesc.Size = new System.Drawing.Size(302, 26);
            this.textContextVendorShortDesc.TabIndex = 40;
            // 
            // lblVendorShort
            // 
            this.lblVendorShort.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblVendorShort.Location = new System.Drawing.Point(403, 200);
            this.lblVendorShort.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblVendorShort.Name = "lblVendorShort";
            this.lblVendorShort.Size = new System.Drawing.Size(130, 30);
            this.lblVendorShort.TabIndex = 41;
            this.lblVendorShort.Text = "Vendor Short:";
            // 
            // lblheaderImage
            // 
            this.lblheaderImage.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblheaderImage.AutoSize = true;
            this.lblheaderImage.Location = new System.Drawing.Point(878, 205);
            this.lblheaderImage.Name = "lblheaderImage";
            this.lblheaderImage.Size = new System.Drawing.Size(142, 20);
            this.lblheaderImage.TabIndex = 42;
            this.lblheaderImage.Text = "Invoice Hdr Image:";
            // 
            // cmbContextDefInvHdrImg
            // 
            this.cmbContextDefInvHdrImg.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cmbContextDefInvHdrImg.FormattingEnabled = true;
            this.cmbContextDefInvHdrImg.Location = new System.Drawing.Point(1046, 201);
            this.cmbContextDefInvHdrImg.Name = "cmbContextDefInvHdrImg";
            this.cmbContextDefInvHdrImg.Size = new System.Drawing.Size(304, 28);
            this.cmbContextDefInvHdrImg.TabIndex = 43;
            // 
            // tabNotes
            // 
            this.tabNotes.Controls.Add(this.tableLayoutPanel1);
            this.tabNotes.Location = new System.Drawing.Point(4, 29);
            this.tabNotes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabNotes.Name = "tabNotes";
            this.tabNotes.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabNotes.Size = new System.Drawing.Size(1361, 253);
            this.tabNotes.TabIndex = 1;
            this.tabNotes.Text = "Notes";
            this.tabNotes.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.lblNotes, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.textRebateNotes, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblRebNotes, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.textNotes, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 5);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1353, 243);
            this.tableLayoutPanel1.TabIndex = 32;
            // 
            // lblNotes
            // 
            this.lblNotes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblNotes.AutoSize = true;
            this.lblNotes.Location = new System.Drawing.Point(4, 11);
            this.lblNotes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNotes.Name = "lblNotes";
            this.lblNotes.Size = new System.Drawing.Size(51, 20);
            this.lblNotes.TabIndex = 8;
            this.lblNotes.Text = "Notes";
            // 
            // textRebateNotes
            // 
            this.textRebateNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textRebateNotes.Location = new System.Drawing.Point(680, 36);
            this.textRebateNotes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textRebateNotes.Multiline = true;
            this.textRebateNotes.Name = "textRebateNotes";
            this.textRebateNotes.Size = new System.Drawing.Size(669, 202);
            this.textRebateNotes.TabIndex = 33;
            // 
            // lblRebNotes
            // 
            this.lblRebNotes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblRebNotes.AutoSize = true;
            this.lblRebNotes.Location = new System.Drawing.Point(680, 11);
            this.lblRebNotes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRebNotes.Name = "lblRebNotes";
            this.lblRebNotes.Size = new System.Drawing.Size(108, 20);
            this.lblRebNotes.TabIndex = 48;
            this.lblRebNotes.Text = "Rebate Notes";
            // 
            // textNotes
            // 
            this.textNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textNotes.Location = new System.Drawing.Point(4, 36);
            this.textNotes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textNotes.Multiline = true;
            this.textNotes.Name = "textNotes";
            this.textNotes.Size = new System.Drawing.Size(668, 202);
            this.textNotes.TabIndex = 32;
            // 
            // tabCBA
            // 
            this.tabCBA.Controls.Add(this.tableLayoutPanel2);
            this.tabCBA.Location = new System.Drawing.Point(4, 29);
            this.tabCBA.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabCBA.Name = "tabCBA";
            this.tabCBA.Size = new System.Drawing.Size(1361, 253);
            this.tabCBA.TabIndex = 2;
            this.tabCBA.Text = "CBA Integration";
            this.tabCBA.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.lblCBASupplierType, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.cmbExtraCurrency, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblExtraCurrency, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.cmbLockCode, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblLockCode, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.textCBATerms, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.textCBASuppType, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblCBATerms, 2, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1361, 253);
            this.tableLayoutPanel2.TabIndex = 34;
            // 
            // lblCBASupplierType
            // 
            this.lblCBASupplierType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblCBASupplierType.AutoSize = true;
            this.lblCBASupplierType.Location = new System.Drawing.Point(4, 18);
            this.lblCBASupplierType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCBASupplierType.Name = "lblCBASupplierType";
            this.lblCBASupplierType.Size = new System.Drawing.Size(146, 20);
            this.lblCBASupplierType.TabIndex = 16;
            this.lblCBASupplierType.Text = "CBA Supplier Type:";
            // 
            // cmbExtraCurrency
            // 
            this.cmbExtraCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbExtraCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbExtraCurrency.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbExtraCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbExtraCurrency.FormattingEnabled = true;
            this.cmbExtraCurrency.Location = new System.Drawing.Point(864, 53);
            this.cmbExtraCurrency.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbExtraCurrency.Name = "cmbExtraCurrency";
            this.cmbExtraCurrency.Size = new System.Drawing.Size(493, 28);
            this.cmbExtraCurrency.TabIndex = 37;
            // 
            // lblExtraCurrency
            // 
            this.lblExtraCurrency.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblExtraCurrency.AutoSize = true;
            this.lblExtraCurrency.Location = new System.Drawing.Point(684, 58);
            this.lblExtraCurrency.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblExtraCurrency.Name = "lblExtraCurrency";
            this.lblExtraCurrency.Size = new System.Drawing.Size(117, 20);
            this.lblExtraCurrency.TabIndex = 19;
            this.lblExtraCurrency.Text = "Extra Currency:";
            // 
            // cmbLockCode
            // 
            this.cmbLockCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbLockCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbLockCode.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbLockCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLockCode.FormattingEnabled = true;
            this.cmbLockCode.Location = new System.Drawing.Point(184, 53);
            this.cmbLockCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbLockCode.Name = "cmbLockCode";
            this.cmbLockCode.Size = new System.Drawing.Size(492, 28);
            this.cmbLockCode.TabIndex = 35;
            // 
            // lblLockCode
            // 
            this.lblLockCode.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblLockCode.AutoSize = true;
            this.lblLockCode.Location = new System.Drawing.Point(4, 58);
            this.lblLockCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLockCode.Name = "lblLockCode";
            this.lblLockCode.Size = new System.Drawing.Size(89, 20);
            this.lblLockCode.TabIndex = 18;
            this.lblLockCode.Text = "Lock Code:";
            // 
            // textCBATerms
            // 
            this.textCBATerms.Dock = System.Windows.Forms.DockStyle.Top;
            this.textCBATerms.Location = new System.Drawing.Point(864, 13);
            this.textCBATerms.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textCBATerms.Name = "textCBATerms";
            this.textCBATerms.Size = new System.Drawing.Size(493, 26);
            this.textCBATerms.TabIndex = 36;
            // 
            // textCBASuppType
            // 
            this.textCBASuppType.Dock = System.Windows.Forms.DockStyle.Top;
            this.textCBASuppType.Location = new System.Drawing.Point(184, 13);
            this.textCBASuppType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textCBASuppType.Name = "textCBASuppType";
            this.textCBASuppType.Size = new System.Drawing.Size(492, 26);
            this.textCBASuppType.TabIndex = 34;
            // 
            // lblCBATerms
            // 
            this.lblCBATerms.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblCBATerms.AutoSize = true;
            this.lblCBATerms.Location = new System.Drawing.Point(684, 18);
            this.lblCBATerms.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCBATerms.Name = "lblCBATerms";
            this.lblCBATerms.Size = new System.Drawing.Size(94, 20);
            this.lblCBATerms.TabIndex = 15;
            this.lblCBATerms.Text = "CBA Terms:";
            // 
            // cbActive
            // 
            this.cbActive.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbActive.AutoSize = true;
            this.cbActive.Location = new System.Drawing.Point(842, 254);
            this.cbActive.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbActive.Name = "cbActive";
            this.cbActive.Size = new System.Drawing.Size(91, 24);
            this.cbActive.TabIndex = 20;
            this.cbActive.Text = "Active ?";
            this.cbActive.UseVisualStyleBackColor = true;
            // 
            // cbNonAccount
            // 
            this.cbNonAccount.AutoSize = true;
            this.cbNonAccount.Location = new System.Drawing.Point(842, 291);
            this.cbNonAccount.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbNonAccount.Name = "cbNonAccount";
            this.cbNonAccount.Size = new System.Drawing.Size(140, 24);
            this.cbNonAccount.TabIndex = 26;
            this.cbNonAccount.Text = "Non Account ?";
            this.cbNonAccount.UseVisualStyleBackColor = true;
            // 
            // lblDefaultCurrency
            // 
            this.lblDefaultCurrency.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDefaultCurrency.AutoSize = true;
            this.lblDefaultCurrency.Location = new System.Drawing.Point(4, 511);
            this.lblDefaultCurrency.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDefaultCurrency.Name = "lblDefaultCurrency";
            this.lblDefaultCurrency.Size = new System.Drawing.Size(132, 20);
            this.lblDefaultCurrency.TabIndex = 3;
            this.lblDefaultCurrency.Text = "Default Currency:";
            // 
            // lblParentSupplier
            // 
            this.lblParentSupplier.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblParentSupplier.AutoSize = true;
            this.lblParentSupplier.Location = new System.Drawing.Point(4, 591);
            this.lblParentSupplier.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblParentSupplier.Name = "lblParentSupplier";
            this.lblParentSupplier.Size = new System.Drawing.Size(122, 20);
            this.lblParentSupplier.TabIndex = 14;
            this.lblParentSupplier.Text = "Parent Supplier:";
            // 
            // lblSupplierID
            // 
            this.lblSupplierID.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSupplierID.AutoSize = true;
            this.lblSupplierID.Location = new System.Drawing.Point(4, 11);
            this.lblSupplierID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSupplierID.Name = "lblSupplierID";
            this.lblSupplierID.Size = new System.Drawing.Size(89, 20);
            this.lblSupplierID.TabIndex = 0;
            this.lblSupplierID.Text = "Supplier Id:";
            // 
            // frmSupplierMaint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1401, 1020);
            this.Controls.Add(this.tlpBackground);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmSupplierMaint";
            this.Text = "Maintain Suppliers";
            this.Load += new System.EventHandler(this.frmSupplierMaint_Load);
            this.tlpBackground.ResumeLayout(false);
            this.tlpFields.ResumeLayout(false);
            this.tlpFields.PerformLayout();
            this.tlpSuppId.ResumeLayout(false);
            this.tlpSuppId.PerformLayout();
            this.tlpActions.ResumeLayout(false);
            this.tlpActions.PerformLayout();
            this.tlpParentSupp.ResumeLayout(false);
            this.tlpParentSupp.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabContext.ResumeLayout(false);
            this.tlpContext.ResumeLayout(false);
            this.tlpContext.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContext)).EndInit();
            this.tlpContextActions.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tabNotes.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabCBA.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpBackground;
        private System.Windows.Forms.ImageList imageListActions;
        private System.Windows.Forms.TableLayoutPanel tlpFields;
        private System.Windows.Forms.Label lblSupplierID;
        private System.Windows.Forms.Label lblName1;
        private System.Windows.Forms.Label lblName2;
        private System.Windows.Forms.TextBox textName1;
        private System.Windows.Forms.TextBox textName2;
        private System.Windows.Forms.TableLayoutPanel tlpSuppId;
        private System.Windows.Forms.Button pbSuppIdSearch;
        private System.Windows.Forms.TextBox textSupplierId;
        private System.Windows.Forms.TableLayoutPanel tlpActions;
        private System.Windows.Forms.Button pbSave;
        private System.Windows.Forms.Button pbCancel;
        private System.Windows.Forms.Button pbDelete;
        private System.Windows.Forms.Label lblActions;
        private System.Windows.Forms.TableLayoutPanel tlpParentSupp;
        private System.Windows.Forms.Button pbParentSuppIdSearch;
        private System.Windows.Forms.TextBox textParentSuppId;
        private System.Windows.Forms.TextBox textParentSuppName;
        private System.Windows.Forms.Label lblParentSupplier;
        private System.Windows.Forms.Label lblDefaultPayTerms;
        private System.Windows.Forms.ComboBox cmbPaymentTerms;
        private System.Windows.Forms.Label lblDefaultPOType;
        private System.Windows.Forms.ComboBox cmbDefPOType;
        private System.Windows.Forms.ComboBox cmbDefCurrency;
        private System.Windows.Forms.Label lblContact;
        private System.Windows.Forms.TextBox textContact;
        private System.Windows.Forms.Label lblMobile;
        private System.Windows.Forms.TextBox textMobile;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblFax;
        private System.Windows.Forms.Label lblEmailAddress;
        private System.Windows.Forms.TextBox textPhone;
        private System.Windows.Forms.TextBox textFax;
        private System.Windows.Forms.TextBox textEmailAddress;
        private System.Windows.Forms.Label lblAddressLine1;
        private System.Windows.Forms.TextBox textAddr1;
        private System.Windows.Forms.Label lblAddressLine2;
        private System.Windows.Forms.TextBox textAddr2;
        private System.Windows.Forms.Label lblAddressLine3;
        private System.Windows.Forms.TextBox textAddr3;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.TextBox textAddrState;
        private System.Windows.Forms.Label lblCountry;
        private System.Windows.Forms.ComboBox cmbCountry;
        private System.Windows.Forms.Label lblDefaultPort;
        private System.Windows.Forms.ComboBox cmbDefPort;
        private System.Windows.Forms.Label lblPostCode;
        private System.Windows.Forms.TextBox textPostCode;
        private System.Windows.Forms.Label lblABN;
        private System.Windows.Forms.TextBox textABN;
        private System.Windows.Forms.Label LBLEntityType;
        private System.Windows.Forms.ComboBox cmbEntityType;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabContext;
        private System.Windows.Forms.TabPage tabNotes;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblNotes;
        private System.Windows.Forms.TextBox textRebateNotes;
        private System.Windows.Forms.Label lblRebNotes;
        private System.Windows.Forms.TextBox textNotes;
        private System.Windows.Forms.TabPage tabCBA;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lblCBASupplierType;
        private System.Windows.Forms.ComboBox cmbExtraCurrency;
        private System.Windows.Forms.Label lblExtraCurrency;
        private System.Windows.Forms.ComboBox cmbLockCode;
        private System.Windows.Forms.Label lblLockCode;
        private System.Windows.Forms.TextBox textCBATerms;
        private System.Windows.Forms.TextBox textCBASuppType;
        private System.Windows.Forms.Label lblCBATerms;
        private System.Windows.Forms.CheckBox cbActive;
        private System.Windows.Forms.CheckBox cbNonAccount;
        private System.Windows.Forms.Label lblDefaultCurrency;
        private System.Windows.Forms.TableLayoutPanel tlpContext;
        private System.Windows.Forms.DataGridView dgvContext;
        private System.Windows.Forms.Label lblInternalCompanyID;
        private System.Windows.Forms.Label lblSupplier;
        private System.Windows.Forms.TableLayoutPanel tlpContextActions;
        private System.Windows.Forms.Button pbContextDelete;
        private System.Windows.Forms.Button pbContextSave;
        private System.Windows.Forms.ComboBox cmbContextId;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TextBox textContextSuppId;
        private System.Windows.Forms.Button pbAllSuppIdSearch;
        private System.Windows.Forms.Label lblPaymentTerms;
        private System.Windows.Forms.Label lblRebateNotes;
        private System.Windows.Forms.Label lblDefPOType;
        private System.Windows.Forms.Label lblProfileNotes;
        private System.Windows.Forms.TextBox textContextProfileNotes;
        private System.Windows.Forms.TextBox textContextRebateNotes;
        private System.Windows.Forms.ComboBox cmbContextPaymentTerms;
        private System.Windows.Forms.ComboBox cmbContextPOType;
        private System.Windows.Forms.TextBox textContextVendorShortDesc;
        private System.Windows.Forms.Label lblVendorShort;
        private System.Windows.Forms.Label lblheaderImage;
        private System.Windows.Forms.ComboBox cmbContextDefInvHdrImg;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UbiSqlFramework;

namespace OMT.Forms
{
    public partial class dlgLogin : Form
    {
        int nTries = 0;
        
        public dlgLogin()
        {
            InitializeComponent();
        }

        private void dlgLogin_Load(object sender, EventArgs e)
        {
            if (!AppHost.InitApp())
            {
                MessageBox.Show("Application failed to load.", "Application Error");
                AppHost.DestructApp();
                Application.Exit();
            }
            LoadConnectionProfiles();

        }

        private void LoadConnectionProfiles()
        {
            cmbConnection.Items.Clear();
            string[] sFilters = SqlHost.DBConnectionFilter.Split(',');
            foreach (DatabaseConnectionProfile d in SqlHost.ConnectionProfiles.Values)
            {
                foreach (string sFilter in sFilters)
                {
                    if (d.Dbtype == sFilter || sFilter == "All")
                    {
                        cmbConnection.Items.Add(d);
                    }
                }
            }
            if (AppHost.RegistryValueExists("HKCU", "Software\\Uncle Bills Australia\\Masterfiles", "LastConnectionId"))
            {
                string sRegConnectionId = AppHost.RegistryGetKeyValue("HKCU", "Software\\Uncle Bills Australia\\Masterfiles", "LastConnectionId");
                for (int n = 0; n < cmbConnection.Items.Count; n++)
                {
                    if (((DatabaseConnectionProfile)cmbConnection.Items[n]).Id == sRegConnectionId)
                    {
                        cmbConnection.SelectedIndex = n;
                    }
                }
            }
            string sDefProfileId = SqlHost.ConfigValue("/ConfigurationSettings/ConnectionSettings/DefaultLogin", "connectionId");
            textBoxUserName.Text = SqlHost.ConfigValue("/ConfigurationSettings/ConnectionSettings/DefaultLogin", "userName");
            textBoxPassword.Text = SqlHost.ConfigValue("/ConfigurationSettings/ConnectionSettings/DefaultLogin", "passWord");
            for (int n = 0; n < cmbConnection.Items.Count; n++)
            {
                if (((DatabaseConnectionProfile)cmbConnection.Items[n]).Id == sDefProfileId)
                {
                    cmbConnection.SelectedIndex = n;
                }
            }
        }

        private void pbCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            if (AppHost.CurrentUser == null)
            {
                AppHost.DestructApp();
                Application.Exit();
            }
        }

        private void pbLogin_Click(object sender, EventArgs e)
        {
            string sConnectionId = "";
            if (cmbConnection.SelectedIndex != -1)
            {
                sConnectionId = ((DatabaseConnectionProfile)cmbConnection.SelectedItem).Id;
            }
            if (
                cmbConnection.SelectedIndex != -1 &&
                SqlHost.OpenConnectionMain(sConnectionId) &&
                AppHost.LoginUser(textBoxUserName.Text.ToUpper(), textBoxPassword.Text.ToUpper())
                )
            {
                bool bRegExists = AppHost.RegistryValueExists("HKCU", "Software\\Uncle Bills Australia\\Masterfiles", "LastConnectionId");
                if (bRegExists)
                {
                    AppHost.RegistryUpdateKeyValue("HKCU", "Software\\Uncle Bills Australia\\Masterfiles", "LastConnectionId", sConnectionId);
                }
                else
                {
                    AppHost.RegistryCreateKey("HKCU", "Software\\Uncle Bills Australia\\Masterfiles", "LastConnectionId", sConnectionId);
                }
                this.Hide();
                MDIWorkbench mdi = new MDIWorkbench();
                mdi.Show();
            }
            else
            {
                nTries++;
                if (nTries == 4)
                {
                    AppHost.DestructApp();
                    Application.Exit(); 
                }
                else
                {
                    lblMessage.Text = "Login Failed.";
                    lblMessage.Visible = true;
                }
            }
        }

    }
}

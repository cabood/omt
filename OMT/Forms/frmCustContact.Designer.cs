﻿namespace OMT.Forms
{
    partial class frmCustContact
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustContact));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.tlpActions = new System.Windows.Forms.TableLayoutPanel();
            this.pbSave = new System.Windows.Forms.Button();
            this.imageListActions = new System.Windows.Forms.ImageList(this.components);
            this.pbCancel = new System.Windows.Forms.Button();
            this.pbDeleteCurrent = new System.Windows.Forms.Button();
            this.lblActions = new System.Windows.Forms.Label();
            this.tlpHeader = new System.Windows.Forms.TableLayoutPanel();
            this.lblCustomers = new System.Windows.Forms.Label();
            this.textCustName = new System.Windows.Forms.TextBox();
            this.textCustId = new System.Windows.Forms.TextBox();
            this.tlpDataGrid = new System.Windows.Forms.TableLayoutPanel();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.pbAdd = new System.Windows.Forms.Button();
            this.pbDelete = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tlpAddress = new System.Windows.Forms.TableLayoutPanel();
            this.lblname1 = new System.Windows.Forms.Label();
            this.lblName2 = new System.Windows.Forms.Label();
            this.textName1 = new System.Windows.Forms.TextBox();
            this.textName2 = new System.Windows.Forms.TextBox();
            this.lblContactID = new System.Windows.Forms.Label();
            this.tlpAddressId = new System.Windows.Forms.TableLayoutPanel();
            this.textContactId = new System.Windows.Forms.TextBox();
            this.lblEmailAddress = new System.Windows.Forms.Label();
            this.lblContactType = new System.Windows.Forms.Label();
            this.lblPhone1 = new System.Windows.Forms.Label();
            this.lblPhone2 = new System.Windows.Forms.Label();
            this.textEmail = new System.Windows.Forms.TextBox();
            this.cmbContactType = new System.Windows.Forms.ComboBox();
            this.textPhone1 = new System.Windows.Forms.TextBox();
            this.textPhone2 = new System.Windows.Forms.TextBox();
            this.lblNotes = new System.Windows.Forms.Label();
            this.lblPhone3 = new System.Windows.Forms.Label();
            this.textPhone3 = new System.Windows.Forms.TextBox();
            this.textNotes = new System.Windows.Forms.TextBox();
            this.tlpMain.SuspendLayout();
            this.tlpActions.SuspendLayout();
            this.tlpHeader.SuspendLayout();
            this.tlpDataGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tlpAddress.SuspendLayout();
            this.tlpAddressId.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 3;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpMain.Controls.Add(this.tlpActions, 1, 4);
            this.tlpMain.Controls.Add(this.tlpHeader, 1, 1);
            this.tlpMain.Controls.Add(this.tlpDataGrid, 1, 2);
            this.tlpMain.Controls.Add(this.groupBox1, 1, 3);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 6;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 385F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpMain.Size = new System.Drawing.Size(1228, 794);
            this.tlpMain.TabIndex = 0;
            // 
            // tlpActions
            // 
            this.tlpActions.ColumnCount = 6;
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tlpActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpActions.Controls.Add(this.pbSave, 2, 0);
            this.tlpActions.Controls.Add(this.pbCancel, 3, 0);
            this.tlpActions.Controls.Add(this.pbDeleteCurrent, 4, 0);
            this.tlpActions.Controls.Add(this.lblActions, 1, 0);
            this.tlpActions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpActions.Location = new System.Drawing.Point(12, 736);
            this.tlpActions.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tlpActions.Name = "tlpActions";
            this.tlpActions.RowCount = 1;
            this.tlpActions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpActions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tlpActions.Size = new System.Drawing.Size(1204, 45);
            this.tlpActions.TabIndex = 8;
            // 
            // pbSave
            // 
            this.pbSave.ImageKey = "Add32.png";
            this.pbSave.ImageList = this.imageListActions;
            this.pbSave.Location = new System.Drawing.Point(728, 5);
            this.pbSave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbSave.Name = "pbSave";
            this.pbSave.Size = new System.Drawing.Size(112, 35);
            this.pbSave.TabIndex = 8;
            this.pbSave.Text = "&Save";
            this.pbSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbSave.UseVisualStyleBackColor = true;
            this.pbSave.Click += new System.EventHandler(this.pbSave_Click);
            // 
            // imageListActions
            // 
            this.imageListActions.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListActions.ImageStream")));
            this.imageListActions.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListActions.Images.SetKeyName(0, "Add32.png");
            this.imageListActions.Images.SetKeyName(1, "Cancel32.png");
            this.imageListActions.Images.SetKeyName(2, "Delete32.png");
            this.imageListActions.Images.SetKeyName(3, "Find32.png");
            this.imageListActions.Images.SetKeyName(4, "Address32.png");
            this.imageListActions.Images.SetKeyName(5, "Contact32.png");
            // 
            // pbCancel
            // 
            this.pbCancel.ImageKey = "Cancel32.png";
            this.pbCancel.ImageList = this.imageListActions;
            this.pbCancel.Location = new System.Drawing.Point(878, 5);
            this.pbCancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbCancel.Name = "pbCancel";
            this.pbCancel.Size = new System.Drawing.Size(112, 35);
            this.pbCancel.TabIndex = 9;
            this.pbCancel.Text = "&Cancel";
            this.pbCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbCancel.UseVisualStyleBackColor = true;
            this.pbCancel.Click += new System.EventHandler(this.pbCancel_Click);
            // 
            // pbDeleteCurrent
            // 
            this.pbDeleteCurrent.ImageKey = "Delete32.png";
            this.pbDeleteCurrent.ImageList = this.imageListActions;
            this.pbDeleteCurrent.Location = new System.Drawing.Point(1028, 5);
            this.pbDeleteCurrent.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbDeleteCurrent.Name = "pbDeleteCurrent";
            this.pbDeleteCurrent.Size = new System.Drawing.Size(112, 35);
            this.pbDeleteCurrent.TabIndex = 10;
            this.pbDeleteCurrent.Text = "&Delete";
            this.pbDeleteCurrent.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbDeleteCurrent.UseVisualStyleBackColor = true;
            this.pbDeleteCurrent.Click += new System.EventHandler(this.pbDeleteCurrent_Click);
            // 
            // lblActions
            // 
            this.lblActions.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblActions.AutoSize = true;
            this.lblActions.Location = new System.Drawing.Point(654, 12);
            this.lblActions.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblActions.Name = "lblActions";
            this.lblActions.Size = new System.Drawing.Size(66, 20);
            this.lblActions.TabIndex = 0;
            this.lblActions.Text = "Actions:";
            // 
            // tlpHeader
            // 
            this.tlpHeader.ColumnCount = 4;
            this.tlpHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 127F));
            this.tlpHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 278F));
            this.tlpHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 525F));
            this.tlpHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpHeader.Controls.Add(this.lblCustomers, 0, 1);
            this.tlpHeader.Controls.Add(this.textCustName, 2, 1);
            this.tlpHeader.Controls.Add(this.textCustId, 1, 1);
            this.tlpHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpHeader.Location = new System.Drawing.Point(8, 8);
            this.tlpHeader.Margin = new System.Windows.Forms.Padding(0);
            this.tlpHeader.Name = "tlpHeader";
            this.tlpHeader.RowCount = 3;
            this.tlpHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpHeader.Size = new System.Drawing.Size(1212, 61);
            this.tlpHeader.TabIndex = 0;
            // 
            // lblCustomers
            // 
            this.lblCustomers.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblCustomers.AutoSize = true;
            this.lblCustomers.Location = new System.Drawing.Point(4, 20);
            this.lblCustomers.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCustomers.Name = "lblCustomers";
            this.lblCustomers.Size = new System.Drawing.Size(82, 20);
            this.lblCustomers.TabIndex = 0;
            this.lblCustomers.Text = "Customer:";
            // 
            // textCustName
            // 
            this.textCustName.Dock = System.Windows.Forms.DockStyle.Top;
            this.textCustName.Location = new System.Drawing.Point(409, 15);
            this.textCustName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textCustName.Name = "textCustName";
            this.textCustName.ReadOnly = true;
            this.textCustName.Size = new System.Drawing.Size(517, 26);
            this.textCustName.TabIndex = 2;
            // 
            // textCustId
            // 
            this.textCustId.Dock = System.Windows.Forms.DockStyle.Top;
            this.textCustId.Location = new System.Drawing.Point(131, 15);
            this.textCustId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textCustId.Name = "textCustId";
            this.textCustId.ReadOnly = true;
            this.textCustId.Size = new System.Drawing.Size(270, 26);
            this.textCustId.TabIndex = 3;
            // 
            // tlpDataGrid
            // 
            this.tlpDataGrid.ColumnCount = 2;
            this.tlpDataGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDataGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tlpDataGrid.Controls.Add(this.dgvData, 0, 0);
            this.tlpDataGrid.Controls.Add(this.pbAdd, 1, 1);
            this.tlpDataGrid.Controls.Add(this.pbDelete, 1, 2);
            this.tlpDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpDataGrid.Location = new System.Drawing.Point(12, 74);
            this.tlpDataGrid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tlpDataGrid.Name = "tlpDataGrid";
            this.tlpDataGrid.RowCount = 4;
            this.tlpDataGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDataGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tlpDataGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tlpDataGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDataGrid.Size = new System.Drawing.Size(1204, 267);
            this.tlpDataGrid.TabIndex = 1;
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToAddRows = false;
            this.dgvData.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvData.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvData.Location = new System.Drawing.Point(4, 5);
            this.dgvData.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvData.Name = "dgvData";
            this.dgvData.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvData.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvData.RowHeadersWidth = 62;
            this.tlpDataGrid.SetRowSpan(this.dgvData, 4);
            this.dgvData.Size = new System.Drawing.Size(1142, 257);
            this.dgvData.TabIndex = 0;
            this.dgvData.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvData_CellContentDoubleClick);
            this.dgvData.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvData_CellDoubleClick);
            this.dgvData.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvData_RowHeaderMouseDoubleClick);
            // 
            // pbAdd
            // 
            this.pbAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbAdd.ImageKey = "Add32.png";
            this.pbAdd.ImageList = this.imageListActions;
            this.pbAdd.Location = new System.Drawing.Point(1154, 83);
            this.pbAdd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbAdd.Name = "pbAdd";
            this.pbAdd.Size = new System.Drawing.Size(46, 45);
            this.pbAdd.TabIndex = 1;
            this.pbAdd.UseVisualStyleBackColor = true;
            this.pbAdd.Click += new System.EventHandler(this.pbAdd_Click);
            // 
            // pbDelete
            // 
            this.pbDelete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbDelete.ImageKey = "Delete32.png";
            this.pbDelete.ImageList = this.imageListActions;
            this.pbDelete.Location = new System.Drawing.Point(1154, 138);
            this.pbDelete.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbDelete.Name = "pbDelete";
            this.pbDelete.Size = new System.Drawing.Size(46, 45);
            this.pbDelete.TabIndex = 2;
            this.pbDelete.UseVisualStyleBackColor = true;
            this.pbDelete.Click += new System.EventHandler(this.pbDelete_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tlpAddress);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(12, 351);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(1204, 375);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Address Information";
            // 
            // tlpAddress
            // 
            this.tlpAddress.ColumnCount = 4;
            this.tlpAddress.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 126F));
            this.tlpAddress.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpAddress.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tlpAddress.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpAddress.Controls.Add(this.lblname1, 0, 2);
            this.tlpAddress.Controls.Add(this.lblName2, 0, 3);
            this.tlpAddress.Controls.Add(this.textName1, 1, 2);
            this.tlpAddress.Controls.Add(this.textName2, 1, 3);
            this.tlpAddress.Controls.Add(this.lblContactID, 0, 1);
            this.tlpAddress.Controls.Add(this.tlpAddressId, 1, 1);
            this.tlpAddress.Controls.Add(this.lblEmailAddress, 0, 4);
            this.tlpAddress.Controls.Add(this.lblContactType, 2, 1);
            this.tlpAddress.Controls.Add(this.lblPhone1, 2, 2);
            this.tlpAddress.Controls.Add(this.lblPhone2, 2, 3);
            this.tlpAddress.Controls.Add(this.textEmail, 1, 4);
            this.tlpAddress.Controls.Add(this.cmbContactType, 3, 1);
            this.tlpAddress.Controls.Add(this.textPhone1, 3, 2);
            this.tlpAddress.Controls.Add(this.textPhone2, 3, 3);
            this.tlpAddress.Controls.Add(this.lblNotes, 0, 5);
            this.tlpAddress.Controls.Add(this.lblPhone3, 2, 4);
            this.tlpAddress.Controls.Add(this.textPhone3, 3, 4);
            this.tlpAddress.Controls.Add(this.textNotes, 0, 6);
            this.tlpAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpAddress.Location = new System.Drawing.Point(4, 24);
            this.tlpAddress.Margin = new System.Windows.Forms.Padding(0);
            this.tlpAddress.Name = "tlpAddress";
            this.tlpAddress.RowCount = 10;
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddress.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAddress.Size = new System.Drawing.Size(1196, 346);
            this.tlpAddress.TabIndex = 0;
            // 
            // lblname1
            // 
            this.lblname1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblname1.AutoSize = true;
            this.lblname1.Location = new System.Drawing.Point(4, 58);
            this.lblname1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblname1.Name = "lblname1";
            this.lblname1.Size = new System.Drawing.Size(68, 20);
            this.lblname1.TabIndex = 0;
            this.lblname1.Text = "Name 1:";
            // 
            // lblName2
            // 
            this.lblName2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblName2.AutoSize = true;
            this.lblName2.Location = new System.Drawing.Point(4, 98);
            this.lblName2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName2.Name = "lblName2";
            this.lblName2.Size = new System.Drawing.Size(68, 20);
            this.lblName2.TabIndex = 1;
            this.lblName2.Text = "Name 2:";
            // 
            // textName1
            // 
            this.textName1.Dock = System.Windows.Forms.DockStyle.Top;
            this.textName1.Location = new System.Drawing.Point(130, 53);
            this.textName1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textName1.Name = "textName1";
            this.textName1.Size = new System.Drawing.Size(437, 26);
            this.textName1.TabIndex = 0;
            // 
            // textName2
            // 
            this.textName2.Dock = System.Windows.Forms.DockStyle.Top;
            this.textName2.Location = new System.Drawing.Point(130, 93);
            this.textName2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textName2.Name = "textName2";
            this.textName2.Size = new System.Drawing.Size(437, 26);
            this.textName2.TabIndex = 1;
            // 
            // lblContactID
            // 
            this.lblContactID.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblContactID.AutoSize = true;
            this.lblContactID.Location = new System.Drawing.Point(4, 18);
            this.lblContactID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblContactID.Name = "lblContactID";
            this.lblContactID.Size = new System.Drawing.Size(90, 20);
            this.lblContactID.TabIndex = 21;
            this.lblContactID.Text = "Contact ID:";
            // 
            // tlpAddressId
            // 
            this.tlpAddressId.ColumnCount = 2;
            this.tlpAddressId.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 225F));
            this.tlpAddressId.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAddressId.Controls.Add(this.textContactId, 0, 0);
            this.tlpAddressId.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpAddressId.Location = new System.Drawing.Point(126, 8);
            this.tlpAddressId.Margin = new System.Windows.Forms.Padding(0);
            this.tlpAddressId.Name = "tlpAddressId";
            this.tlpAddressId.RowCount = 1;
            this.tlpAddressId.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAddressId.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpAddressId.Size = new System.Drawing.Size(445, 40);
            this.tlpAddressId.TabIndex = 23;
            // 
            // textContactId
            // 
            this.textContactId.Dock = System.Windows.Forms.DockStyle.Top;
            this.textContactId.Location = new System.Drawing.Point(4, 5);
            this.textContactId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textContactId.Name = "textContactId";
            this.textContactId.ReadOnly = true;
            this.textContactId.Size = new System.Drawing.Size(217, 26);
            this.textContactId.TabIndex = 22;
            // 
            // lblEmailAddress
            // 
            this.lblEmailAddress.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblEmailAddress.AutoSize = true;
            this.lblEmailAddress.Location = new System.Drawing.Point(4, 138);
            this.lblEmailAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmailAddress.Name = "lblEmailAddress";
            this.lblEmailAddress.Size = new System.Drawing.Size(115, 20);
            this.lblEmailAddress.TabIndex = 14;
            this.lblEmailAddress.Text = "Email Address:";
            // 
            // lblContactType
            // 
            this.lblContactType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblContactType.AutoSize = true;
            this.lblContactType.Location = new System.Drawing.Point(575, 18);
            this.lblContactType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblContactType.Name = "lblContactType";
            this.lblContactType.Size = new System.Drawing.Size(107, 20);
            this.lblContactType.TabIndex = 26;
            this.lblContactType.Text = "Contact Type:";
            // 
            // lblPhone1
            // 
            this.lblPhone1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblPhone1.AutoSize = true;
            this.lblPhone1.Location = new System.Drawing.Point(575, 58);
            this.lblPhone1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPhone1.Name = "lblPhone1";
            this.lblPhone1.Size = new System.Drawing.Size(72, 20);
            this.lblPhone1.TabIndex = 12;
            this.lblPhone1.Text = "Phone 1:";
            // 
            // lblPhone2
            // 
            this.lblPhone2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblPhone2.AutoSize = true;
            this.lblPhone2.Location = new System.Drawing.Point(575, 98);
            this.lblPhone2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPhone2.Name = "lblPhone2";
            this.lblPhone2.Size = new System.Drawing.Size(72, 20);
            this.lblPhone2.TabIndex = 13;
            this.lblPhone2.Text = "Phone 2:";
            // 
            // textEmail
            // 
            this.textEmail.Dock = System.Windows.Forms.DockStyle.Top;
            this.textEmail.Location = new System.Drawing.Point(130, 133);
            this.textEmail.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textEmail.Name = "textEmail";
            this.textEmail.Size = new System.Drawing.Size(437, 26);
            this.textEmail.TabIndex = 2;
            // 
            // cmbContactType
            // 
            this.cmbContactType.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbContactType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContactType.FormattingEnabled = true;
            this.cmbContactType.Location = new System.Drawing.Point(755, 13);
            this.cmbContactType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbContactType.Name = "cmbContactType";
            this.cmbContactType.Size = new System.Drawing.Size(437, 28);
            this.cmbContactType.TabIndex = 3;
            // 
            // textPhone1
            // 
            this.textPhone1.Dock = System.Windows.Forms.DockStyle.Top;
            this.textPhone1.Location = new System.Drawing.Point(755, 53);
            this.textPhone1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textPhone1.Name = "textPhone1";
            this.textPhone1.Size = new System.Drawing.Size(437, 26);
            this.textPhone1.TabIndex = 4;
            // 
            // textPhone2
            // 
            this.textPhone2.Dock = System.Windows.Forms.DockStyle.Top;
            this.textPhone2.Location = new System.Drawing.Point(755, 93);
            this.textPhone2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textPhone2.Name = "textPhone2";
            this.textPhone2.Size = new System.Drawing.Size(437, 26);
            this.textPhone2.TabIndex = 5;
            // 
            // lblNotes
            // 
            this.lblNotes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblNotes.AutoSize = true;
            this.lblNotes.Location = new System.Drawing.Point(4, 188);
            this.lblNotes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNotes.Name = "lblNotes";
            this.lblNotes.Size = new System.Drawing.Size(55, 20);
            this.lblNotes.TabIndex = 28;
            this.lblNotes.Text = "Notes:";
            // 
            // lblPhone3
            // 
            this.lblPhone3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblPhone3.AutoSize = true;
            this.lblPhone3.Location = new System.Drawing.Point(575, 138);
            this.lblPhone3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPhone3.Name = "lblPhone3";
            this.lblPhone3.Size = new System.Drawing.Size(72, 20);
            this.lblPhone3.TabIndex = 30;
            this.lblPhone3.Text = "Phone 3:";
            // 
            // textPhone3
            // 
            this.textPhone3.Dock = System.Windows.Forms.DockStyle.Top;
            this.textPhone3.Location = new System.Drawing.Point(755, 133);
            this.textPhone3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textPhone3.Name = "textPhone3";
            this.textPhone3.Size = new System.Drawing.Size(437, 26);
            this.textPhone3.TabIndex = 6;
            // 
            // textNotes
            // 
            this.tlpAddress.SetColumnSpan(this.textNotes, 4);
            this.textNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textNotes.Location = new System.Drawing.Point(4, 213);
            this.textNotes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textNotes.Multiline = true;
            this.textNotes.Name = "textNotes";
            this.tlpAddress.SetRowSpan(this.textNotes, 3);
            this.textNotes.Size = new System.Drawing.Size(1188, 110);
            this.textNotes.TabIndex = 7;
            // 
            // frmCustContact
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1228, 794);
            this.Controls.Add(this.tlpMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmCustContact";
            this.Text = "Customer Contacts";
            this.Load += new System.EventHandler(this.frmCustContact_Load);
            this.tlpMain.ResumeLayout(false);
            this.tlpActions.ResumeLayout(false);
            this.tlpActions.PerformLayout();
            this.tlpHeader.ResumeLayout(false);
            this.tlpHeader.PerformLayout();
            this.tlpDataGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.tlpAddress.ResumeLayout(false);
            this.tlpAddress.PerformLayout();
            this.tlpAddressId.ResumeLayout(false);
            this.tlpAddressId.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpHeader;
        private System.Windows.Forms.Label lblCustomers;
        private System.Windows.Forms.TextBox textCustName;
        private System.Windows.Forms.TableLayoutPanel tlpDataGrid;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tlpAddress;
        private System.Windows.Forms.Label lblname1;
        private System.Windows.Forms.Label lblName2;
        private System.Windows.Forms.TextBox textName1;
        private System.Windows.Forms.TextBox textName2;
        private System.Windows.Forms.Label lblPhone1;
        private System.Windows.Forms.Label lblPhone2;
        private System.Windows.Forms.Label lblEmailAddress;
        private System.Windows.Forms.TextBox textPhone1;
        private System.Windows.Forms.TextBox textPhone2;
        private System.Windows.Forms.TextBox textEmail;
        private System.Windows.Forms.Label lblContactID;
        private System.Windows.Forms.TextBox textContactId;
        private System.Windows.Forms.TableLayoutPanel tlpAddressId;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.TextBox textCustId;
        private System.Windows.Forms.Label lblContactType;
        private System.Windows.Forms.Button pbAdd;
        private System.Windows.Forms.Button pbDelete;
        private System.Windows.Forms.ComboBox cmbContactType;
        private System.Windows.Forms.ImageList imageListActions;
        private System.Windows.Forms.TableLayoutPanel tlpActions;
        private System.Windows.Forms.Button pbSave;
        private System.Windows.Forms.Button pbCancel;
        private System.Windows.Forms.Button pbDeleteCurrent;
        private System.Windows.Forms.Label lblActions;
        private System.Windows.Forms.Label lblNotes;
        private System.Windows.Forms.TextBox textNotes;
        private System.Windows.Forms.Label lblPhone3;
        private System.Windows.Forms.TextBox textPhone3;
    }
}
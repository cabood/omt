﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OMT.Forms
{
    public partial class dlgBitwiseOptionPicker : Form, IGenericExchange
    {
        private IGenericExchange cPartner;
        private string sId;
        private DataSet cData;
        
        public dlgBitwiseOptionPicker()
        {
            InitializeComponent();
        }

        public IGenericExchange Partner
        {
            get { return cPartner; }
            set { cPartner = value; }
        }

        private void pbAccept_Click(object sender, EventArgs e)
        {
            ReturnSelectedValue();
        }

        private void ReturnSelectedValue()
        {
            Int32 nValue;
            nValue = 0;
            if (cPartner != null)
            {
                nValue = CompileOptions();
                GenericParam g = new GenericParam();
                g.IdField = sId;
                g.ValueInt32 = nValue;
                cPartner.GenericTransfer(g);
            }
            this.Close();
        }

        private Int32 CompileOptions()
        {
            int n;
            Int32 nVal;
            nVal = 0;
            Control c;
            string sControlName;

            for (n = 0; n < 32; n++)
            {
                sControlName = "checkOption" + (n + 1).ToString("0");
                c = tlpGrid.Controls[sControlName];
                if (((CheckBox)c).Checked == true)
                {
                    nVal = nVal | (Int32)Math.Pow(2, (double)n);
                }
            }
            return nVal;
        }

        public void GenericTransfer(GenericParam g)
        {
            sId = g.IdField;
            if (sId == "SetBitwiseOptions")
            {
                SetupOptions(g.ValueField, g.ValueInt32);
            }
        }

        private void SetupOptions(string sOptionList, Int32 nOptionSettings)
        {
            int n;
            int ntlpRow;
            int nOptionCount;
            string[] sOptions;
            Control c;
            string sControlName;
            Int32 nResult;

            // First: Set up CheckBoxes
            sOptions = sOptionList.Split(',');
            nOptionCount = sOptions.Length;
            for (n = 0; n < nOptionCount; n++)
            {
                sControlName = "checkOption" + (n + 1).ToString("0");
                c = tlpGrid.Controls[sControlName];
                if (c != null)
                {
                    c.Text = sOptions[n];
                    c.Visible = true;
                }
            }
            for (n = nOptionCount; n < 32; n++)
            {
                sControlName = "checkOption" + (n + 1).ToString("0");
                c = tlpGrid.Controls[sControlName];
                if (c != null)
                {
                    c.Visible = false;
                }
                if (n % 2 == 0)
                {
                    //tlpGrid.RowStyles[(n / 2)].Height = 1;
                    //this.Height = this.Height - 25;
                }
            }

            // Second: Populate Checkboxes
            for (n = 0; n < 32; n++)
            {
                sControlName = "checkOption" + (n + 1).ToString("0");
                c = tlpGrid.Controls[sControlName];
                if ((nOptionSettings & (Int32)Math.Pow(2, (double)n)) == (Int32)Math.Pow(2, (double)n))
                {
                    ((CheckBox)c).Checked = true;
                }
                else
                {
                    ((CheckBox)c).Checked = false;
                }
            }

        }

        private void pbCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}

﻿namespace OMT.Forms
{
    partial class frmRefData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRefData));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabLayers = new System.Windows.Forms.TabControl();
            this.tabRefTable = new System.Windows.Forms.TabPage();
            this.tlpRefTable = new System.Windows.Forms.TableLayoutPanel();
            this.dgvRefTable = new System.Windows.Forms.DataGridView();
            this.tlpRefTableActions = new System.Windows.Forms.TableLayoutPanel();
            this.pbRefTableAdd = new System.Windows.Forms.Button();
            this.imageListActions = new System.Windows.Forms.ImageList(this.components);
            this.pbRefTableEdit = new System.Windows.Forms.Button();
            this.pbRefTableDelete = new System.Windows.Forms.Button();
            this.tabRefTableRow = new System.Windows.Forms.TabPage();
            this.tlpTableMaintenance = new System.Windows.Forms.TableLayoutPanel();
            this.textTableExtendedOptions = new System.Windows.Forms.TextBox();
            this.pbTableSave = new System.Windows.Forms.Button();
            this.lblReferenceTableID = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblDisplay = new System.Windows.Forms.Label();
            this.lblIDCodeLabel = new System.Windows.Forms.Label();
            this.lblescriptionLabel = new System.Windows.Forms.Label();
            this.lblShortDescriptionLabel = new System.Windows.Forms.Label();
            this.lblExtraString1Label = new System.Windows.Forms.Label();
            this.lblExtraString2Label = new System.Windows.Forms.Label();
            this.lblExtraString3Label = new System.Windows.Forms.Label();
            this.lblExtraNumber1Label = new System.Windows.Forms.Label();
            this.lblExtraNumber2Label = new System.Windows.Forms.Label();
            this.lblExtraDate1Label = new System.Windows.Forms.Label();
            this.lblExtraDate2Label = new System.Windows.Forms.Label();
            this.lblExtraBool1Label = new System.Windows.Forms.Label();
            this.lblExtraBool2Label = new System.Windows.Forms.Label();
            this.lblForeignKey1ID = new System.Windows.Forms.Label();
            this.lblForeignKey2ID = new System.Windows.Forms.Label();
            this.lblTootipHelp = new System.Windows.Forms.Label();
            this.lblNotes = new System.Windows.Forms.Label();
            this.textTableRefTableId = new System.Windows.Forms.TextBox();
            this.textTableDesc = new System.Windows.Forms.TextBox();
            this.textTableHelpDesc = new System.Windows.Forms.TextBox();
            this.textTableIdLabel = new System.Windows.Forms.TextBox();
            this.textTableDescLabel = new System.Windows.Forms.TextBox();
            this.textTableShortDescLabel = new System.Windows.Forms.TextBox();
            this.textTableExtStr1Label = new System.Windows.Forms.TextBox();
            this.textTableExtStr2Label = new System.Windows.Forms.TextBox();
            this.textTableExtStr3Label = new System.Windows.Forms.TextBox();
            this.textTableExtNum1Label = new System.Windows.Forms.TextBox();
            this.textTableExtNum2Label = new System.Windows.Forms.TextBox();
            this.textTableExtDt1Label = new System.Windows.Forms.TextBox();
            this.textTableExtDt2Label = new System.Windows.Forms.TextBox();
            this.textTableExtBool1Label = new System.Windows.Forms.TextBox();
            this.textTableExtBool2Label = new System.Windows.Forms.TextBox();
            this.textTableStr1FK = new System.Windows.Forms.TextBox();
            this.textTableStr2FK = new System.Windows.Forms.TextBox();
            this.textTableNotes = new System.Windows.Forms.TextBox();
            this.checkTableDisplayYn = new System.Windows.Forms.CheckBox();
            this.pbTableCancel = new System.Windows.Forms.Button();
            this.lblExtendedBooleanOptions = new System.Windows.Forms.Label();
            this.tabRefData = new System.Windows.Forms.TabPage();
            this.tlpRefData = new System.Windows.Forms.TableLayoutPanel();
            this.dgvRefData = new System.Windows.Forms.DataGridView();
            this.TextHelpNote = new System.Windows.Forms.Label();
            this.pbRefDataAdd = new System.Windows.Forms.Button();
            this.pbRefDataDelete = new System.Windows.Forms.Button();
            this.tabRefDataRow = new System.Windows.Forms.TabPage();
            this.tlpDataMaintenance = new System.Windows.Forms.TableLayoutPanel();
            this.labelExtendedOptions = new System.Windows.Forms.Label();
            this.labelRefTableId = new System.Windows.Forms.Label();
            this.labelRefId = new System.Windows.Forms.Label();
            this.labelDescription = new System.Windows.Forms.Label();
            this.labelShortDesc = new System.Windows.Forms.Label();
            this.labelExtStr1a = new System.Windows.Forms.Label();
            this.labelExtStr1b = new System.Windows.Forms.Label();
            this.labelExtStr2a = new System.Windows.Forms.Label();
            this.labelExtStr2b = new System.Windows.Forms.Label();
            this.labelExtStr3 = new System.Windows.Forms.Label();
            this.labelExtNum1 = new System.Windows.Forms.Label();
            this.labelExtNum2 = new System.Windows.Forms.Label();
            this.labelExtBool1 = new System.Windows.Forms.Label();
            this.labelExtBool2 = new System.Windows.Forms.Label();
            this.labelNotes = new System.Windows.Forms.Label();
            this.textDataRefTableId = new System.Windows.Forms.TextBox();
            this.textDataRefId = new System.Windows.Forms.TextBox();
            this.textDataDescription = new System.Windows.Forms.TextBox();
            this.textDataShortDesc = new System.Windows.Forms.TextBox();
            this.textDataExtStr1 = new System.Windows.Forms.TextBox();
            this.textDataExtStr2 = new System.Windows.Forms.TextBox();
            this.textDataExtStr3 = new System.Windows.Forms.TextBox();
            this.textDataExtNum1 = new System.Windows.Forms.TextBox();
            this.textDataExtNum2 = new System.Windows.Forms.TextBox();
            this.comboDataExtStr1 = new System.Windows.Forms.ComboBox();
            this.comboDataExtStr2 = new System.Windows.Forms.ComboBox();
            this.checkDataExtBool1 = new System.Windows.Forms.CheckBox();
            this.checkDataExtBool2 = new System.Windows.Forms.CheckBox();
            this.labelExtDt1 = new System.Windows.Forms.Label();
            this.labelExtDt2 = new System.Windows.Forms.Label();
            this.dtpDataExtDt1 = new System.Windows.Forms.DateTimePicker();
            this.dtpDataExtDt2 = new System.Windows.Forms.DateTimePicker();
            this.textDataNotes = new System.Windows.Forms.TextBox();
            this.pbRefDataSave = new System.Windows.Forms.Button();
            this.pbRefDataCancel = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.textDataExtendedOptions = new System.Windows.Forms.TextBox();
            this.pbDataExtendedOptions = new System.Windows.Forms.Button();
            this.imageListTabs = new System.Windows.Forms.ImageList(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblPasswordToModify = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.tabLayers.SuspendLayout();
            this.tabRefTable.SuspendLayout();
            this.tlpRefTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRefTable)).BeginInit();
            this.tlpRefTableActions.SuspendLayout();
            this.tabRefTableRow.SuspendLayout();
            this.tlpTableMaintenance.SuspendLayout();
            this.tabRefData.SuspendLayout();
            this.tlpRefData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRefData)).BeginInit();
            this.tabRefDataRow.SuspendLayout();
            this.tlpDataMaintenance.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabLayers
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.tabLayers, 2);
            this.tabLayers.Controls.Add(this.tabRefTable);
            this.tabLayers.Controls.Add(this.tabRefTableRow);
            this.tabLayers.Controls.Add(this.tabRefData);
            this.tabLayers.Controls.Add(this.tabRefDataRow);
            this.tabLayers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabLayers.ImageList = this.imageListTabs;
            this.tabLayers.Location = new System.Drawing.Point(4, 66);
            this.tabLayers.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabLayers.Name = "tabLayers";
            this.tabLayers.SelectedIndex = 0;
            this.tabLayers.Size = new System.Drawing.Size(1188, 933);
            this.tabLayers.TabIndex = 0;
            // 
            // tabRefTable
            // 
            this.tabRefTable.Controls.Add(this.tlpRefTable);
            this.tabRefTable.ImageKey = "admin48.png";
            this.tabRefTable.Location = new System.Drawing.Point(4, 29);
            this.tabRefTable.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabRefTable.Name = "tabRefTable";
            this.tabRefTable.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabRefTable.Size = new System.Drawing.Size(1180, 900);
            this.tabRefTable.TabIndex = 0;
            this.tabRefTable.Text = "Reference Tables";
            this.tabRefTable.UseVisualStyleBackColor = true;
            // 
            // tlpRefTable
            // 
            this.tlpRefTable.ColumnCount = 3;
            this.tlpRefTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpRefTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpRefTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpRefTable.Controls.Add(this.dgvRefTable, 1, 1);
            this.tlpRefTable.Controls.Add(this.tlpRefTableActions, 1, 2);
            this.tlpRefTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpRefTable.Location = new System.Drawing.Point(4, 5);
            this.tlpRefTable.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tlpRefTable.Name = "tlpRefTable";
            this.tlpRefTable.RowCount = 4;
            this.tlpRefTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpRefTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpRefTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.tlpRefTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpRefTable.Size = new System.Drawing.Size(1172, 890);
            this.tlpRefTable.TabIndex = 0;
            // 
            // dgvRefTable
            // 
            this.dgvRefTable.AllowUserToAddRows = false;
            this.dgvRefTable.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvRefTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvRefTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvRefTable.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvRefTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRefTable.Location = new System.Drawing.Point(12, 13);
            this.dgvRefTable.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvRefTable.Name = "dgvRefTable";
            this.dgvRefTable.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvRefTable.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvRefTable.RowHeadersWidth = 62;
            this.dgvRefTable.Size = new System.Drawing.Size(1148, 803);
            this.dgvRefTable.TabIndex = 0;
            this.dgvRefTable.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRefTable_CellDoubleClick);
            this.dgvRefTable.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvRefTable_RowHeaderMouseDoubleClick);
            this.dgvRefTable.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvRefTable_KeyDown);
            // 
            // tlpRefTableActions
            // 
            this.tlpRefTableActions.ColumnCount = 5;
            this.tlpRefTableActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpRefTableActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tlpRefTableActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tlpRefTableActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tlpRefTableActions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpRefTableActions.Controls.Add(this.pbRefTableAdd, 1, 0);
            this.tlpRefTableActions.Controls.Add(this.pbRefTableEdit, 2, 0);
            this.tlpRefTableActions.Controls.Add(this.pbRefTableDelete, 3, 0);
            this.tlpRefTableActions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpRefTableActions.Location = new System.Drawing.Point(12, 826);
            this.tlpRefTableActions.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tlpRefTableActions.Name = "tlpRefTableActions";
            this.tlpRefTableActions.RowCount = 1;
            this.tlpRefTableActions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpRefTableActions.Size = new System.Drawing.Size(1148, 51);
            this.tlpRefTableActions.TabIndex = 1;
            // 
            // pbRefTableAdd
            // 
            this.pbRefTableAdd.ImageKey = "Add32.png";
            this.pbRefTableAdd.ImageList = this.imageListActions;
            this.pbRefTableAdd.Location = new System.Drawing.Point(410, 5);
            this.pbRefTableAdd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbRefTableAdd.Name = "pbRefTableAdd";
            this.pbRefTableAdd.Size = new System.Drawing.Size(104, 35);
            this.pbRefTableAdd.TabIndex = 0;
            this.pbRefTableAdd.Text = "&New";
            this.pbRefTableAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbRefTableAdd.UseVisualStyleBackColor = true;
            this.pbRefTableAdd.Click += new System.EventHandler(this.pbRefTableAdd_Click);
            // 
            // imageListActions
            // 
            this.imageListActions.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListActions.ImageStream")));
            this.imageListActions.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListActions.Images.SetKeyName(0, "Add32.png");
            this.imageListActions.Images.SetKeyName(1, "Delete32.png");
            this.imageListActions.Images.SetKeyName(2, "Cancel32.png");
            this.imageListActions.Images.SetKeyName(3, "save32.png");
            this.imageListActions.Images.SetKeyName(4, "Edit32.png");
            // 
            // pbRefTableEdit
            // 
            this.pbRefTableEdit.ImageKey = "Edit32.png";
            this.pbRefTableEdit.ImageList = this.imageListActions;
            this.pbRefTableEdit.Location = new System.Drawing.Point(522, 5);
            this.pbRefTableEdit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbRefTableEdit.Name = "pbRefTableEdit";
            this.pbRefTableEdit.Size = new System.Drawing.Size(104, 35);
            this.pbRefTableEdit.TabIndex = 1;
            this.pbRefTableEdit.Text = "&Edit";
            this.pbRefTableEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbRefTableEdit.UseVisualStyleBackColor = true;
            this.pbRefTableEdit.Click += new System.EventHandler(this.pbRefTableEdit_Click);
            // 
            // pbRefTableDelete
            // 
            this.pbRefTableDelete.ImageKey = "Delete32.png";
            this.pbRefTableDelete.ImageList = this.imageListActions;
            this.pbRefTableDelete.Location = new System.Drawing.Point(634, 5);
            this.pbRefTableDelete.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbRefTableDelete.Name = "pbRefTableDelete";
            this.pbRefTableDelete.Size = new System.Drawing.Size(104, 35);
            this.pbRefTableDelete.TabIndex = 2;
            this.pbRefTableDelete.Text = "&Delete";
            this.pbRefTableDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbRefTableDelete.UseVisualStyleBackColor = true;
            this.pbRefTableDelete.Click += new System.EventHandler(this.pbRefTableDelete_Click);
            // 
            // tabRefTableRow
            // 
            this.tabRefTableRow.Controls.Add(this.tlpTableMaintenance);
            this.tabRefTableRow.Location = new System.Drawing.Point(4, 29);
            this.tabRefTableRow.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabRefTableRow.Name = "tabRefTableRow";
            this.tabRefTableRow.Size = new System.Drawing.Size(1180, 900);
            this.tabRefTableRow.TabIndex = 3;
            this.tabRefTableRow.Text = "Table Maintenance";
            this.tabRefTableRow.UseVisualStyleBackColor = true;
            // 
            // tlpTableMaintenance
            // 
            this.tlpTableMaintenance.ColumnCount = 4;
            this.tlpTableMaintenance.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tlpTableMaintenance.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.91324F));
            this.tlpTableMaintenance.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 79.08676F));
            this.tlpTableMaintenance.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tlpTableMaintenance.Controls.Add(this.textTableExtendedOptions, 2, 19);
            this.tlpTableMaintenance.Controls.Add(this.pbTableSave, 1, 21);
            this.tlpTableMaintenance.Controls.Add(this.lblReferenceTableID, 1, 1);
            this.tlpTableMaintenance.Controls.Add(this.lblDescription, 1, 2);
            this.tlpTableMaintenance.Controls.Add(this.lblDisplay, 1, 4);
            this.tlpTableMaintenance.Controls.Add(this.lblIDCodeLabel, 1, 5);
            this.tlpTableMaintenance.Controls.Add(this.lblescriptionLabel, 1, 6);
            this.tlpTableMaintenance.Controls.Add(this.lblShortDescriptionLabel, 1, 7);
            this.tlpTableMaintenance.Controls.Add(this.lblExtraString1Label, 1, 8);
            this.tlpTableMaintenance.Controls.Add(this.lblExtraString2Label, 1, 9);
            this.tlpTableMaintenance.Controls.Add(this.lblExtraString3Label, 1, 10);
            this.tlpTableMaintenance.Controls.Add(this.lblExtraNumber1Label, 1, 11);
            this.tlpTableMaintenance.Controls.Add(this.lblExtraNumber2Label, 1, 12);
            this.tlpTableMaintenance.Controls.Add(this.lblExtraDate1Label, 1, 13);
            this.tlpTableMaintenance.Controls.Add(this.lblExtraDate2Label, 1, 14);
            this.tlpTableMaintenance.Controls.Add(this.lblExtraBool1Label, 1, 15);
            this.tlpTableMaintenance.Controls.Add(this.lblExtraBool2Label, 1, 16);
            this.tlpTableMaintenance.Controls.Add(this.lblForeignKey1ID, 1, 17);
            this.tlpTableMaintenance.Controls.Add(this.lblForeignKey2ID, 1, 18);
            this.tlpTableMaintenance.Controls.Add(this.lblTootipHelp, 1, 3);
            this.tlpTableMaintenance.Controls.Add(this.lblNotes, 1, 20);
            this.tlpTableMaintenance.Controls.Add(this.textTableRefTableId, 2, 1);
            this.tlpTableMaintenance.Controls.Add(this.textTableDesc, 2, 2);
            this.tlpTableMaintenance.Controls.Add(this.textTableHelpDesc, 2, 3);
            this.tlpTableMaintenance.Controls.Add(this.textTableIdLabel, 2, 5);
            this.tlpTableMaintenance.Controls.Add(this.textTableDescLabel, 2, 6);
            this.tlpTableMaintenance.Controls.Add(this.textTableShortDescLabel, 2, 7);
            this.tlpTableMaintenance.Controls.Add(this.textTableExtStr1Label, 2, 8);
            this.tlpTableMaintenance.Controls.Add(this.textTableExtStr2Label, 2, 9);
            this.tlpTableMaintenance.Controls.Add(this.textTableExtStr3Label, 2, 10);
            this.tlpTableMaintenance.Controls.Add(this.textTableExtNum1Label, 2, 11);
            this.tlpTableMaintenance.Controls.Add(this.textTableExtNum2Label, 2, 12);
            this.tlpTableMaintenance.Controls.Add(this.textTableExtDt1Label, 2, 13);
            this.tlpTableMaintenance.Controls.Add(this.textTableExtDt2Label, 2, 14);
            this.tlpTableMaintenance.Controls.Add(this.textTableExtBool1Label, 2, 15);
            this.tlpTableMaintenance.Controls.Add(this.textTableExtBool2Label, 2, 16);
            this.tlpTableMaintenance.Controls.Add(this.textTableStr1FK, 2, 17);
            this.tlpTableMaintenance.Controls.Add(this.textTableStr2FK, 2, 18);
            this.tlpTableMaintenance.Controls.Add(this.textTableNotes, 2, 20);
            this.tlpTableMaintenance.Controls.Add(this.checkTableDisplayYn, 2, 4);
            this.tlpTableMaintenance.Controls.Add(this.pbTableCancel, 2, 21);
            this.tlpTableMaintenance.Controls.Add(this.lblExtendedBooleanOptions, 1, 19);
            this.tlpTableMaintenance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpTableMaintenance.Location = new System.Drawing.Point(0, 0);
            this.tlpTableMaintenance.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tlpTableMaintenance.Name = "tlpTableMaintenance";
            this.tlpTableMaintenance.RowCount = 24;
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpTableMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpTableMaintenance.Size = new System.Drawing.Size(1180, 900);
            this.tlpTableMaintenance.TabIndex = 0;
            // 
            // textTableExtendedOptions
            // 
            this.textTableExtendedOptions.Dock = System.Windows.Forms.DockStyle.Top;
            this.textTableExtendedOptions.Location = new System.Drawing.Point(265, 733);
            this.textTableExtendedOptions.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textTableExtendedOptions.Name = "textTableExtendedOptions";
            this.textTableExtendedOptions.Size = new System.Drawing.Size(856, 26);
            this.textTableExtendedOptions.TabIndex = 37;
            // 
            // pbTableSave
            // 
            this.pbTableSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbTableSave.ImageKey = "save32.png";
            this.pbTableSave.ImageList = this.imageListActions;
            this.pbTableSave.Location = new System.Drawing.Point(145, 845);
            this.pbTableSave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbTableSave.Name = "pbTableSave";
            this.pbTableSave.Size = new System.Drawing.Size(112, 35);
            this.pbTableSave.TabIndex = 0;
            this.pbTableSave.Text = "&Save";
            this.pbTableSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbTableSave.UseVisualStyleBackColor = true;
            this.pbTableSave.Click += new System.EventHandler(this.pbTableSave_Click);
            // 
            // lblReferenceTableID
            // 
            this.lblReferenceTableID.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblReferenceTableID.AutoSize = true;
            this.lblReferenceTableID.Location = new System.Drawing.Point(37, 17);
            this.lblReferenceTableID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblReferenceTableID.Name = "lblReferenceTableID";
            this.lblReferenceTableID.Size = new System.Drawing.Size(149, 20);
            this.lblReferenceTableID.TabIndex = 0;
            this.lblReferenceTableID.Text = "Reference Table Id:";
            // 
            // lblDescription
            // 
            this.lblDescription.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(37, 55);
            this.lblDescription.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(93, 20);
            this.lblDescription.TabIndex = 1;
            this.lblDescription.Text = "Description:";
            // 
            // lblDisplay
            // 
            this.lblDisplay.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDisplay.AutoSize = true;
            this.lblDisplay.Location = new System.Drawing.Point(37, 167);
            this.lblDisplay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDisplay.Name = "lblDisplay";
            this.lblDisplay.Size = new System.Drawing.Size(73, 20);
            this.lblDisplay.TabIndex = 2;
            this.lblDisplay.Text = "Display?:";
            // 
            // lblIDCodeLabel
            // 
            this.lblIDCodeLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblIDCodeLabel.AutoSize = true;
            this.lblIDCodeLabel.Location = new System.Drawing.Point(37, 205);
            this.lblIDCodeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIDCodeLabel.Name = "lblIDCodeLabel";
            this.lblIDCodeLabel.Size = new System.Drawing.Size(123, 20);
            this.lblIDCodeLabel.TabIndex = 3;
            this.lblIDCodeLabel.Text = "ID / Code Label:";
            // 
            // lblescriptionLabel
            // 
            this.lblescriptionLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblescriptionLabel.AutoSize = true;
            this.lblescriptionLabel.Location = new System.Drawing.Point(37, 243);
            this.lblescriptionLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblescriptionLabel.Name = "lblescriptionLabel";
            this.lblescriptionLabel.Size = new System.Drawing.Size(136, 20);
            this.lblescriptionLabel.TabIndex = 4;
            this.lblescriptionLabel.Text = "Description Label:";
            // 
            // lblShortDescriptionLabel
            // 
            this.lblShortDescriptionLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblShortDescriptionLabel.AutoSize = true;
            this.lblShortDescriptionLabel.Location = new System.Drawing.Point(37, 281);
            this.lblShortDescriptionLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShortDescriptionLabel.Name = "lblShortDescriptionLabel";
            this.lblShortDescriptionLabel.Size = new System.Drawing.Size(188, 20);
            this.lblShortDescriptionLabel.TabIndex = 5;
            this.lblShortDescriptionLabel.Text = "Short - Description Label:";
            // 
            // lblExtraString1Label
            // 
            this.lblExtraString1Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblExtraString1Label.AutoSize = true;
            this.lblExtraString1Label.Location = new System.Drawing.Point(37, 319);
            this.lblExtraString1Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblExtraString1Label.Name = "lblExtraString1Label";
            this.lblExtraString1Label.Size = new System.Drawing.Size(152, 20);
            this.lblExtraString1Label.TabIndex = 6;
            this.lblExtraString1Label.Text = "Extra String 1 Label:";
            // 
            // lblExtraString2Label
            // 
            this.lblExtraString2Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblExtraString2Label.AutoSize = true;
            this.lblExtraString2Label.Location = new System.Drawing.Point(37, 357);
            this.lblExtraString2Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblExtraString2Label.Name = "lblExtraString2Label";
            this.lblExtraString2Label.Size = new System.Drawing.Size(152, 20);
            this.lblExtraString2Label.TabIndex = 7;
            this.lblExtraString2Label.Text = "Extra String 2 Label:";
            // 
            // lblExtraString3Label
            // 
            this.lblExtraString3Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblExtraString3Label.AutoSize = true;
            this.lblExtraString3Label.Location = new System.Drawing.Point(37, 395);
            this.lblExtraString3Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblExtraString3Label.Name = "lblExtraString3Label";
            this.lblExtraString3Label.Size = new System.Drawing.Size(152, 20);
            this.lblExtraString3Label.TabIndex = 8;
            this.lblExtraString3Label.Text = "Extra String 3 Label:";
            // 
            // lblExtraNumber1Label
            // 
            this.lblExtraNumber1Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblExtraNumber1Label.AutoSize = true;
            this.lblExtraNumber1Label.Location = new System.Drawing.Point(37, 433);
            this.lblExtraNumber1Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblExtraNumber1Label.Name = "lblExtraNumber1Label";
            this.lblExtraNumber1Label.Size = new System.Drawing.Size(166, 20);
            this.lblExtraNumber1Label.TabIndex = 9;
            this.lblExtraNumber1Label.Text = "Extra Number 1 Label:";
            // 
            // lblExtraNumber2Label
            // 
            this.lblExtraNumber2Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblExtraNumber2Label.AutoSize = true;
            this.lblExtraNumber2Label.Location = new System.Drawing.Point(37, 471);
            this.lblExtraNumber2Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblExtraNumber2Label.Name = "lblExtraNumber2Label";
            this.lblExtraNumber2Label.Size = new System.Drawing.Size(166, 20);
            this.lblExtraNumber2Label.TabIndex = 10;
            this.lblExtraNumber2Label.Text = "Extra Number 2 Label:";
            // 
            // lblExtraDate1Label
            // 
            this.lblExtraDate1Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblExtraDate1Label.AutoSize = true;
            this.lblExtraDate1Label.Location = new System.Drawing.Point(37, 509);
            this.lblExtraDate1Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblExtraDate1Label.Name = "lblExtraDate1Label";
            this.lblExtraDate1Label.Size = new System.Drawing.Size(145, 20);
            this.lblExtraDate1Label.TabIndex = 11;
            this.lblExtraDate1Label.Text = "Extra Date 1 Label:";
            // 
            // lblExtraDate2Label
            // 
            this.lblExtraDate2Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblExtraDate2Label.AutoSize = true;
            this.lblExtraDate2Label.Location = new System.Drawing.Point(37, 547);
            this.lblExtraDate2Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblExtraDate2Label.Name = "lblExtraDate2Label";
            this.lblExtraDate2Label.Size = new System.Drawing.Size(145, 20);
            this.lblExtraDate2Label.TabIndex = 12;
            this.lblExtraDate2Label.Text = "Extra Date 2 Label:";
            // 
            // lblExtraBool1Label
            // 
            this.lblExtraBool1Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblExtraBool1Label.AutoSize = true;
            this.lblExtraBool1Label.Location = new System.Drawing.Point(37, 585);
            this.lblExtraBool1Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblExtraBool1Label.Name = "lblExtraBool1Label";
            this.lblExtraBool1Label.Size = new System.Drawing.Size(142, 20);
            this.lblExtraBool1Label.TabIndex = 13;
            this.lblExtraBool1Label.Text = "Extra Bool 1 Label:";
            // 
            // lblExtraBool2Label
            // 
            this.lblExtraBool2Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblExtraBool2Label.AutoSize = true;
            this.lblExtraBool2Label.Location = new System.Drawing.Point(37, 623);
            this.lblExtraBool2Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblExtraBool2Label.Name = "lblExtraBool2Label";
            this.lblExtraBool2Label.Size = new System.Drawing.Size(142, 20);
            this.lblExtraBool2Label.TabIndex = 14;
            this.lblExtraBool2Label.Text = "Extra Bool 2 Label:";
            // 
            // lblForeignKey1ID
            // 
            this.lblForeignKey1ID.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblForeignKey1ID.AutoSize = true;
            this.lblForeignKey1ID.Location = new System.Drawing.Point(37, 661);
            this.lblForeignKey1ID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblForeignKey1ID.Name = "lblForeignKey1ID";
            this.lblForeignKey1ID.Size = new System.Drawing.Size(131, 20);
            this.lblForeignKey1ID.TabIndex = 15;
            this.lblForeignKey1ID.Text = "Foreign Key 1 ID:";
            // 
            // lblForeignKey2ID
            // 
            this.lblForeignKey2ID.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblForeignKey2ID.AutoSize = true;
            this.lblForeignKey2ID.Location = new System.Drawing.Point(37, 699);
            this.lblForeignKey2ID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblForeignKey2ID.Name = "lblForeignKey2ID";
            this.lblForeignKey2ID.Size = new System.Drawing.Size(131, 20);
            this.lblForeignKey2ID.TabIndex = 16;
            this.lblForeignKey2ID.Text = "Foreign Key 2 ID:";
            // 
            // lblTootipHelp
            // 
            this.lblTootipHelp.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTootipHelp.AutoSize = true;
            this.lblTootipHelp.Location = new System.Drawing.Point(37, 111);
            this.lblTootipHelp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTootipHelp.Name = "lblTootipHelp";
            this.lblTootipHelp.Size = new System.Drawing.Size(97, 20);
            this.lblTootipHelp.TabIndex = 17;
            this.lblTootipHelp.Text = "Tooltip Help:";
            // 
            // lblNotes
            // 
            this.lblNotes.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblNotes.AutoSize = true;
            this.lblNotes.Location = new System.Drawing.Point(37, 793);
            this.lblNotes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNotes.Name = "lblNotes";
            this.lblNotes.Size = new System.Drawing.Size(55, 20);
            this.lblNotes.TabIndex = 18;
            this.lblNotes.Text = "Notes:";
            // 
            // textTableRefTableId
            // 
            this.textTableRefTableId.Location = new System.Drawing.Point(265, 13);
            this.textTableRefTableId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textTableRefTableId.Name = "textTableRefTableId";
            this.textTableRefTableId.Size = new System.Drawing.Size(253, 26);
            this.textTableRefTableId.TabIndex = 19;
            // 
            // textTableDesc
            // 
            this.textTableDesc.Dock = System.Windows.Forms.DockStyle.Top;
            this.textTableDesc.Location = new System.Drawing.Point(265, 51);
            this.textTableDesc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textTableDesc.Name = "textTableDesc";
            this.textTableDesc.Size = new System.Drawing.Size(856, 26);
            this.textTableDesc.TabIndex = 20;
            // 
            // textTableHelpDesc
            // 
            this.textTableHelpDesc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textTableHelpDesc.Location = new System.Drawing.Point(265, 89);
            this.textTableHelpDesc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textTableHelpDesc.Multiline = true;
            this.textTableHelpDesc.Name = "textTableHelpDesc";
            this.textTableHelpDesc.Size = new System.Drawing.Size(856, 64);
            this.textTableHelpDesc.TabIndex = 21;
            // 
            // textTableIdLabel
            // 
            this.textTableIdLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.textTableIdLabel.Location = new System.Drawing.Point(265, 201);
            this.textTableIdLabel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textTableIdLabel.Name = "textTableIdLabel";
            this.textTableIdLabel.Size = new System.Drawing.Size(856, 26);
            this.textTableIdLabel.TabIndex = 23;
            // 
            // textTableDescLabel
            // 
            this.textTableDescLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.textTableDescLabel.Location = new System.Drawing.Point(265, 239);
            this.textTableDescLabel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textTableDescLabel.Name = "textTableDescLabel";
            this.textTableDescLabel.Size = new System.Drawing.Size(856, 26);
            this.textTableDescLabel.TabIndex = 24;
            // 
            // textTableShortDescLabel
            // 
            this.textTableShortDescLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.textTableShortDescLabel.Location = new System.Drawing.Point(265, 277);
            this.textTableShortDescLabel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textTableShortDescLabel.Name = "textTableShortDescLabel";
            this.textTableShortDescLabel.Size = new System.Drawing.Size(856, 26);
            this.textTableShortDescLabel.TabIndex = 25;
            // 
            // textTableExtStr1Label
            // 
            this.textTableExtStr1Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.textTableExtStr1Label.Location = new System.Drawing.Point(265, 315);
            this.textTableExtStr1Label.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textTableExtStr1Label.Name = "textTableExtStr1Label";
            this.textTableExtStr1Label.Size = new System.Drawing.Size(856, 26);
            this.textTableExtStr1Label.TabIndex = 26;
            // 
            // textTableExtStr2Label
            // 
            this.textTableExtStr2Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.textTableExtStr2Label.Location = new System.Drawing.Point(265, 353);
            this.textTableExtStr2Label.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textTableExtStr2Label.Name = "textTableExtStr2Label";
            this.textTableExtStr2Label.Size = new System.Drawing.Size(856, 26);
            this.textTableExtStr2Label.TabIndex = 27;
            // 
            // textTableExtStr3Label
            // 
            this.textTableExtStr3Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.textTableExtStr3Label.Location = new System.Drawing.Point(265, 391);
            this.textTableExtStr3Label.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textTableExtStr3Label.Name = "textTableExtStr3Label";
            this.textTableExtStr3Label.Size = new System.Drawing.Size(856, 26);
            this.textTableExtStr3Label.TabIndex = 28;
            // 
            // textTableExtNum1Label
            // 
            this.textTableExtNum1Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.textTableExtNum1Label.Location = new System.Drawing.Point(265, 429);
            this.textTableExtNum1Label.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textTableExtNum1Label.Name = "textTableExtNum1Label";
            this.textTableExtNum1Label.Size = new System.Drawing.Size(856, 26);
            this.textTableExtNum1Label.TabIndex = 29;
            // 
            // textTableExtNum2Label
            // 
            this.textTableExtNum2Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.textTableExtNum2Label.Location = new System.Drawing.Point(265, 467);
            this.textTableExtNum2Label.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textTableExtNum2Label.Name = "textTableExtNum2Label";
            this.textTableExtNum2Label.Size = new System.Drawing.Size(856, 26);
            this.textTableExtNum2Label.TabIndex = 30;
            // 
            // textTableExtDt1Label
            // 
            this.textTableExtDt1Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.textTableExtDt1Label.Location = new System.Drawing.Point(265, 505);
            this.textTableExtDt1Label.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textTableExtDt1Label.Name = "textTableExtDt1Label";
            this.textTableExtDt1Label.Size = new System.Drawing.Size(856, 26);
            this.textTableExtDt1Label.TabIndex = 31;
            // 
            // textTableExtDt2Label
            // 
            this.textTableExtDt2Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.textTableExtDt2Label.Location = new System.Drawing.Point(265, 543);
            this.textTableExtDt2Label.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textTableExtDt2Label.Name = "textTableExtDt2Label";
            this.textTableExtDt2Label.Size = new System.Drawing.Size(856, 26);
            this.textTableExtDt2Label.TabIndex = 32;
            // 
            // textTableExtBool1Label
            // 
            this.textTableExtBool1Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.textTableExtBool1Label.Location = new System.Drawing.Point(265, 581);
            this.textTableExtBool1Label.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textTableExtBool1Label.Name = "textTableExtBool1Label";
            this.textTableExtBool1Label.Size = new System.Drawing.Size(856, 26);
            this.textTableExtBool1Label.TabIndex = 33;
            // 
            // textTableExtBool2Label
            // 
            this.textTableExtBool2Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.textTableExtBool2Label.Location = new System.Drawing.Point(265, 619);
            this.textTableExtBool2Label.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textTableExtBool2Label.Name = "textTableExtBool2Label";
            this.textTableExtBool2Label.Size = new System.Drawing.Size(856, 26);
            this.textTableExtBool2Label.TabIndex = 34;
            // 
            // textTableStr1FK
            // 
            this.textTableStr1FK.Location = new System.Drawing.Point(265, 657);
            this.textTableStr1FK.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textTableStr1FK.Name = "textTableStr1FK";
            this.textTableStr1FK.Size = new System.Drawing.Size(253, 26);
            this.textTableStr1FK.TabIndex = 35;
            // 
            // textTableStr2FK
            // 
            this.textTableStr2FK.Location = new System.Drawing.Point(265, 695);
            this.textTableStr2FK.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textTableStr2FK.Name = "textTableStr2FK";
            this.textTableStr2FK.Size = new System.Drawing.Size(253, 26);
            this.textTableStr2FK.TabIndex = 36;
            // 
            // textTableNotes
            // 
            this.textTableNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textTableNotes.Location = new System.Drawing.Point(265, 771);
            this.textTableNotes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textTableNotes.Multiline = true;
            this.textTableNotes.Name = "textTableNotes";
            this.textTableNotes.Size = new System.Drawing.Size(856, 64);
            this.textTableNotes.TabIndex = 38;
            // 
            // checkTableDisplayYn
            // 
            this.checkTableDisplayYn.AutoSize = true;
            this.checkTableDisplayYn.Location = new System.Drawing.Point(265, 163);
            this.checkTableDisplayYn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.checkTableDisplayYn.Name = "checkTableDisplayYn";
            this.checkTableDisplayYn.Size = new System.Drawing.Size(128, 24);
            this.checkTableDisplayYn.TabIndex = 22;
            this.checkTableDisplayYn.Text = "(Tick for Yes)";
            this.checkTableDisplayYn.UseVisualStyleBackColor = true;
            // 
            // pbTableCancel
            // 
            this.pbTableCancel.ImageKey = "Cancel32.png";
            this.pbTableCancel.ImageList = this.imageListActions;
            this.pbTableCancel.Location = new System.Drawing.Point(265, 845);
            this.pbTableCancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbTableCancel.Name = "pbTableCancel";
            this.pbTableCancel.Size = new System.Drawing.Size(112, 35);
            this.pbTableCancel.TabIndex = 1;
            this.pbTableCancel.Text = "&Cancel";
            this.pbTableCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbTableCancel.UseVisualStyleBackColor = true;
            this.pbTableCancel.Click += new System.EventHandler(this.pbTableCancel_Click);
            // 
            // lblExtendedBooleanOptions
            // 
            this.lblExtendedBooleanOptions.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblExtendedBooleanOptions.AutoSize = true;
            this.lblExtendedBooleanOptions.Location = new System.Drawing.Point(36, 737);
            this.lblExtendedBooleanOptions.Name = "lblExtendedBooleanOptions";
            this.lblExtendedBooleanOptions.Size = new System.Drawing.Size(203, 20);
            this.lblExtendedBooleanOptions.TabIndex = 39;
            this.lblExtendedBooleanOptions.Text = "Extended Boolean Options:";
            // 
            // tabRefData
            // 
            this.tabRefData.Controls.Add(this.tlpRefData);
            this.tabRefData.ImageKey = "Line48.png";
            this.tabRefData.Location = new System.Drawing.Point(4, 29);
            this.tabRefData.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabRefData.Name = "tabRefData";
            this.tabRefData.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabRefData.Size = new System.Drawing.Size(1180, 900);
            this.tabRefData.TabIndex = 1;
            this.tabRefData.Text = "Reference Data";
            this.tabRefData.UseVisualStyleBackColor = true;
            // 
            // tlpRefData
            // 
            this.tlpRefData.ColumnCount = 5;
            this.tlpRefData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpRefData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tlpRefData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpRefData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tlpRefData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpRefData.Controls.Add(this.dgvRefData, 0, 2);
            this.tlpRefData.Controls.Add(this.TextHelpNote, 0, 1);
            this.tlpRefData.Controls.Add(this.pbRefDataAdd, 1, 4);
            this.tlpRefData.Controls.Add(this.pbRefDataDelete, 3, 4);
            this.tlpRefData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpRefData.Location = new System.Drawing.Point(4, 5);
            this.tlpRefData.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tlpRefData.Name = "tlpRefData";
            this.tlpRefData.RowCount = 6;
            this.tlpRefData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpRefData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this.tlpRefData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpRefData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpRefData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tlpRefData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpRefData.Size = new System.Drawing.Size(1172, 890);
            this.tlpRefData.TabIndex = 1;
            // 
            // dgvRefData
            // 
            this.dgvRefData.AllowUserToAddRows = false;
            this.dgvRefData.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvRefData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvRefData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tlpRefData.SetColumnSpan(this.dgvRefData, 5);
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvRefData.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvRefData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRefData.Location = new System.Drawing.Point(4, 87);
            this.dgvRefData.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvRefData.Name = "dgvRefData";
            this.dgvRefData.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvRefData.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvRefData.RowHeadersWidth = 62;
            this.dgvRefData.Size = new System.Drawing.Size(1164, 733);
            this.dgvRefData.TabIndex = 0;
            this.dgvRefData.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRefData_CellDoubleClick);
            this.dgvRefData.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvRefData_RowHeaderMouseDoubleClick);
            this.dgvRefData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvRefData_KeyDown);
            // 
            // TextHelpNote
            // 
            this.TextHelpNote.AutoSize = true;
            this.tlpRefData.SetColumnSpan(this.TextHelpNote, 5);
            this.TextHelpNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TextHelpNote.Location = new System.Drawing.Point(4, 8);
            this.TextHelpNote.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.TextHelpNote.Name = "TextHelpNote";
            this.TextHelpNote.Size = new System.Drawing.Size(1164, 74);
            this.TextHelpNote.TabIndex = 1;
            this.TextHelpNote.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pbRefDataAdd
            // 
            this.pbRefDataAdd.ImageKey = "Add32.png";
            this.pbRefDataAdd.ImageList = this.imageListActions;
            this.pbRefDataAdd.Location = new System.Drawing.Point(474, 838);
            this.pbRefDataAdd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbRefDataAdd.Name = "pbRefDataAdd";
            this.pbRefDataAdd.Size = new System.Drawing.Size(104, 35);
            this.pbRefDataAdd.TabIndex = 2;
            this.pbRefDataAdd.Text = "&New";
            this.pbRefDataAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbRefDataAdd.UseVisualStyleBackColor = true;
            this.pbRefDataAdd.Click += new System.EventHandler(this.pbAdd_Click);
            // 
            // pbRefDataDelete
            // 
            this.pbRefDataDelete.ImageKey = "Delete32.png";
            this.pbRefDataDelete.ImageList = this.imageListActions;
            this.pbRefDataDelete.Location = new System.Drawing.Point(594, 838);
            this.pbRefDataDelete.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbRefDataDelete.Name = "pbRefDataDelete";
            this.pbRefDataDelete.Size = new System.Drawing.Size(104, 35);
            this.pbRefDataDelete.TabIndex = 3;
            this.pbRefDataDelete.Text = "&Delete";
            this.pbRefDataDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbRefDataDelete.UseVisualStyleBackColor = true;
            this.pbRefDataDelete.Click += new System.EventHandler(this.pbDelete_Click);
            // 
            // tabRefDataRow
            // 
            this.tabRefDataRow.Controls.Add(this.tlpDataMaintenance);
            this.tabRefDataRow.ImageKey = "Edit32.png";
            this.tabRefDataRow.Location = new System.Drawing.Point(4, 29);
            this.tabRefDataRow.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabRefDataRow.Name = "tabRefDataRow";
            this.tabRefDataRow.Size = new System.Drawing.Size(1180, 900);
            this.tabRefDataRow.TabIndex = 2;
            this.tabRefDataRow.Text = "Data Maintenance";
            this.tabRefDataRow.UseVisualStyleBackColor = true;
            // 
            // tlpDataMaintenance
            // 
            this.tlpDataMaintenance.ColumnCount = 4;
            this.tlpDataMaintenance.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpDataMaintenance.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.54795F));
            this.tlpDataMaintenance.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 79.45206F));
            this.tlpDataMaintenance.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tlpDataMaintenance.Controls.Add(this.labelExtendedOptions, 1, 18);
            this.tlpDataMaintenance.Controls.Add(this.labelRefTableId, 1, 3);
            this.tlpDataMaintenance.Controls.Add(this.labelRefId, 1, 4);
            this.tlpDataMaintenance.Controls.Add(this.labelDescription, 1, 5);
            this.tlpDataMaintenance.Controls.Add(this.labelShortDesc, 1, 6);
            this.tlpDataMaintenance.Controls.Add(this.labelExtStr1a, 1, 7);
            this.tlpDataMaintenance.Controls.Add(this.labelExtStr1b, 1, 8);
            this.tlpDataMaintenance.Controls.Add(this.labelExtStr2a, 1, 9);
            this.tlpDataMaintenance.Controls.Add(this.labelExtStr2b, 1, 10);
            this.tlpDataMaintenance.Controls.Add(this.labelExtStr3, 1, 11);
            this.tlpDataMaintenance.Controls.Add(this.labelExtNum1, 1, 12);
            this.tlpDataMaintenance.Controls.Add(this.labelExtNum2, 1, 13);
            this.tlpDataMaintenance.Controls.Add(this.labelExtBool1, 1, 16);
            this.tlpDataMaintenance.Controls.Add(this.labelExtBool2, 1, 17);
            this.tlpDataMaintenance.Controls.Add(this.labelNotes, 1, 19);
            this.tlpDataMaintenance.Controls.Add(this.textDataRefTableId, 2, 3);
            this.tlpDataMaintenance.Controls.Add(this.textDataRefId, 2, 4);
            this.tlpDataMaintenance.Controls.Add(this.textDataDescription, 2, 5);
            this.tlpDataMaintenance.Controls.Add(this.textDataShortDesc, 2, 6);
            this.tlpDataMaintenance.Controls.Add(this.textDataExtStr1, 2, 7);
            this.tlpDataMaintenance.Controls.Add(this.textDataExtStr2, 2, 9);
            this.tlpDataMaintenance.Controls.Add(this.textDataExtStr3, 2, 11);
            this.tlpDataMaintenance.Controls.Add(this.textDataExtNum1, 2, 12);
            this.tlpDataMaintenance.Controls.Add(this.textDataExtNum2, 2, 13);
            this.tlpDataMaintenance.Controls.Add(this.comboDataExtStr1, 2, 8);
            this.tlpDataMaintenance.Controls.Add(this.comboDataExtStr2, 2, 10);
            this.tlpDataMaintenance.Controls.Add(this.checkDataExtBool1, 2, 16);
            this.tlpDataMaintenance.Controls.Add(this.checkDataExtBool2, 2, 17);
            this.tlpDataMaintenance.Controls.Add(this.labelExtDt1, 1, 14);
            this.tlpDataMaintenance.Controls.Add(this.labelExtDt2, 1, 15);
            this.tlpDataMaintenance.Controls.Add(this.dtpDataExtDt1, 2, 14);
            this.tlpDataMaintenance.Controls.Add(this.dtpDataExtDt2, 2, 15);
            this.tlpDataMaintenance.Controls.Add(this.textDataNotes, 2, 19);
            this.tlpDataMaintenance.Controls.Add(this.pbRefDataSave, 1, 20);
            this.tlpDataMaintenance.Controls.Add(this.pbRefDataCancel, 2, 20);
            this.tlpDataMaintenance.Controls.Add(this.tableLayoutPanel2, 2, 18);
            this.tlpDataMaintenance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpDataMaintenance.Location = new System.Drawing.Point(0, 0);
            this.tlpDataMaintenance.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tlpDataMaintenance.Name = "tlpDataMaintenance";
            this.tlpDataMaintenance.RowCount = 25;
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDataMaintenance.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpDataMaintenance.Size = new System.Drawing.Size(1180, 900);
            this.tlpDataMaintenance.TabIndex = 0;
            // 
            // labelExtendedOptions
            // 
            this.labelExtendedOptions.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelExtendedOptions.AutoSize = true;
            this.labelExtendedOptions.Location = new System.Drawing.Point(34, 666);
            this.labelExtendedOptions.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelExtendedOptions.Name = "labelExtendedOptions";
            this.labelExtendedOptions.Size = new System.Drawing.Size(170, 20);
            this.labelExtendedOptions.TabIndex = 35;
            this.labelExtendedOptions.Text = "Extended Y/N Options:";
            // 
            // labelRefTableId
            // 
            this.labelRefTableId.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelRefTableId.AutoSize = true;
            this.labelRefTableId.Location = new System.Drawing.Point(34, 93);
            this.labelRefTableId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelRefTableId.Name = "labelRefTableId";
            this.labelRefTableId.Size = new System.Drawing.Size(131, 20);
            this.labelRefTableId.TabIndex = 0;
            this.labelRefTableId.Text = "Reference Table:";
            // 
            // labelRefId
            // 
            this.labelRefId.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelRefId.AutoSize = true;
            this.labelRefId.Location = new System.Drawing.Point(34, 131);
            this.labelRefId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelRefId.Name = "labelRefId";
            this.labelRefId.Size = new System.Drawing.Size(51, 20);
            this.labelRefId.TabIndex = 1;
            this.labelRefId.Text = "label2";
            // 
            // labelDescription
            // 
            this.labelDescription.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelDescription.AutoSize = true;
            this.labelDescription.Location = new System.Drawing.Point(34, 169);
            this.labelDescription.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(51, 20);
            this.labelDescription.TabIndex = 2;
            this.labelDescription.Text = "label3";
            // 
            // labelShortDesc
            // 
            this.labelShortDesc.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelShortDesc.AutoSize = true;
            this.labelShortDesc.Location = new System.Drawing.Point(34, 207);
            this.labelShortDesc.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelShortDesc.Name = "labelShortDesc";
            this.labelShortDesc.Size = new System.Drawing.Size(51, 20);
            this.labelShortDesc.TabIndex = 3;
            this.labelShortDesc.Text = "label4";
            // 
            // labelExtStr1a
            // 
            this.labelExtStr1a.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelExtStr1a.AutoSize = true;
            this.labelExtStr1a.Location = new System.Drawing.Point(34, 245);
            this.labelExtStr1a.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelExtStr1a.Name = "labelExtStr1a";
            this.labelExtStr1a.Size = new System.Drawing.Size(51, 20);
            this.labelExtStr1a.TabIndex = 4;
            this.labelExtStr1a.Text = "label5";
            // 
            // labelExtStr1b
            // 
            this.labelExtStr1b.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelExtStr1b.AutoSize = true;
            this.labelExtStr1b.Location = new System.Drawing.Point(34, 283);
            this.labelExtStr1b.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelExtStr1b.Name = "labelExtStr1b";
            this.labelExtStr1b.Size = new System.Drawing.Size(51, 20);
            this.labelExtStr1b.TabIndex = 5;
            this.labelExtStr1b.Text = "label6";
            // 
            // labelExtStr2a
            // 
            this.labelExtStr2a.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelExtStr2a.AutoSize = true;
            this.labelExtStr2a.Location = new System.Drawing.Point(34, 321);
            this.labelExtStr2a.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelExtStr2a.Name = "labelExtStr2a";
            this.labelExtStr2a.Size = new System.Drawing.Size(51, 20);
            this.labelExtStr2a.TabIndex = 6;
            this.labelExtStr2a.Text = "label7";
            // 
            // labelExtStr2b
            // 
            this.labelExtStr2b.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelExtStr2b.AutoSize = true;
            this.labelExtStr2b.Location = new System.Drawing.Point(34, 359);
            this.labelExtStr2b.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelExtStr2b.Name = "labelExtStr2b";
            this.labelExtStr2b.Size = new System.Drawing.Size(51, 20);
            this.labelExtStr2b.TabIndex = 7;
            this.labelExtStr2b.Text = "label8";
            // 
            // labelExtStr3
            // 
            this.labelExtStr3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelExtStr3.AutoSize = true;
            this.labelExtStr3.Location = new System.Drawing.Point(34, 397);
            this.labelExtStr3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelExtStr3.Name = "labelExtStr3";
            this.labelExtStr3.Size = new System.Drawing.Size(51, 20);
            this.labelExtStr3.TabIndex = 8;
            this.labelExtStr3.Text = "label9";
            // 
            // labelExtNum1
            // 
            this.labelExtNum1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelExtNum1.AutoSize = true;
            this.labelExtNum1.Location = new System.Drawing.Point(34, 435);
            this.labelExtNum1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelExtNum1.Name = "labelExtNum1";
            this.labelExtNum1.Size = new System.Drawing.Size(60, 20);
            this.labelExtNum1.TabIndex = 9;
            this.labelExtNum1.Text = "label10";
            // 
            // labelExtNum2
            // 
            this.labelExtNum2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelExtNum2.AutoSize = true;
            this.labelExtNum2.Location = new System.Drawing.Point(34, 473);
            this.labelExtNum2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelExtNum2.Name = "labelExtNum2";
            this.labelExtNum2.Size = new System.Drawing.Size(60, 20);
            this.labelExtNum2.TabIndex = 10;
            this.labelExtNum2.Text = "label11";
            // 
            // labelExtBool1
            // 
            this.labelExtBool1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelExtBool1.AutoSize = true;
            this.labelExtBool1.Location = new System.Drawing.Point(34, 587);
            this.labelExtBool1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelExtBool1.Name = "labelExtBool1";
            this.labelExtBool1.Size = new System.Drawing.Size(60, 20);
            this.labelExtBool1.TabIndex = 11;
            this.labelExtBool1.Text = "label12";
            // 
            // labelExtBool2
            // 
            this.labelExtBool2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelExtBool2.AutoSize = true;
            this.labelExtBool2.Location = new System.Drawing.Point(34, 625);
            this.labelExtBool2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelExtBool2.Name = "labelExtBool2";
            this.labelExtBool2.Size = new System.Drawing.Size(60, 20);
            this.labelExtBool2.TabIndex = 12;
            this.labelExtBool2.Text = "label13";
            // 
            // labelNotes
            // 
            this.labelNotes.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelNotes.AutoSize = true;
            this.labelNotes.Location = new System.Drawing.Point(34, 726);
            this.labelNotes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNotes.Name = "labelNotes";
            this.labelNotes.Size = new System.Drawing.Size(64, 20);
            this.labelNotes.TabIndex = 13;
            this.labelNotes.Text = "label14:";
            // 
            // textDataRefTableId
            // 
            this.textDataRefTableId.Location = new System.Drawing.Point(258, 89);
            this.textDataRefTableId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textDataRefTableId.Name = "textDataRefTableId";
            this.textDataRefTableId.ReadOnly = true;
            this.textDataRefTableId.Size = new System.Drawing.Size(262, 26);
            this.textDataRefTableId.TabIndex = 15;
            this.textDataRefTableId.TabStop = false;
            // 
            // textDataRefId
            // 
            this.textDataRefId.Location = new System.Drawing.Point(258, 127);
            this.textDataRefId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textDataRefId.Name = "textDataRefId";
            this.textDataRefId.Size = new System.Drawing.Size(262, 26);
            this.textDataRefId.TabIndex = 16;
            // 
            // textDataDescription
            // 
            this.textDataDescription.Dock = System.Windows.Forms.DockStyle.Top;
            this.textDataDescription.Location = new System.Drawing.Point(258, 165);
            this.textDataDescription.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textDataDescription.Name = "textDataDescription";
            this.textDataDescription.Size = new System.Drawing.Size(860, 26);
            this.textDataDescription.TabIndex = 17;
            // 
            // textDataShortDesc
            // 
            this.textDataShortDesc.Dock = System.Windows.Forms.DockStyle.Top;
            this.textDataShortDesc.Location = new System.Drawing.Point(258, 203);
            this.textDataShortDesc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textDataShortDesc.Name = "textDataShortDesc";
            this.textDataShortDesc.Size = new System.Drawing.Size(860, 26);
            this.textDataShortDesc.TabIndex = 18;
            // 
            // textDataExtStr1
            // 
            this.textDataExtStr1.Location = new System.Drawing.Point(258, 241);
            this.textDataExtStr1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textDataExtStr1.Name = "textDataExtStr1";
            this.textDataExtStr1.Size = new System.Drawing.Size(262, 26);
            this.textDataExtStr1.TabIndex = 19;
            // 
            // textDataExtStr2
            // 
            this.textDataExtStr2.Location = new System.Drawing.Point(258, 317);
            this.textDataExtStr2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textDataExtStr2.Name = "textDataExtStr2";
            this.textDataExtStr2.Size = new System.Drawing.Size(262, 26);
            this.textDataExtStr2.TabIndex = 21;
            // 
            // textDataExtStr3
            // 
            this.textDataExtStr3.Dock = System.Windows.Forms.DockStyle.Top;
            this.textDataExtStr3.Location = new System.Drawing.Point(258, 393);
            this.textDataExtStr3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textDataExtStr3.Name = "textDataExtStr3";
            this.textDataExtStr3.Size = new System.Drawing.Size(860, 26);
            this.textDataExtStr3.TabIndex = 23;
            // 
            // textDataExtNum1
            // 
            this.textDataExtNum1.Location = new System.Drawing.Point(258, 431);
            this.textDataExtNum1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textDataExtNum1.Name = "textDataExtNum1";
            this.textDataExtNum1.Size = new System.Drawing.Size(262, 26);
            this.textDataExtNum1.TabIndex = 24;
            // 
            // textDataExtNum2
            // 
            this.textDataExtNum2.Location = new System.Drawing.Point(258, 469);
            this.textDataExtNum2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textDataExtNum2.Name = "textDataExtNum2";
            this.textDataExtNum2.Size = new System.Drawing.Size(262, 26);
            this.textDataExtNum2.TabIndex = 25;
            // 
            // comboDataExtStr1
            // 
            this.comboDataExtStr1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboDataExtStr1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboDataExtStr1.Dock = System.Windows.Forms.DockStyle.Top;
            this.comboDataExtStr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDataExtStr1.FormattingEnabled = true;
            this.comboDataExtStr1.Location = new System.Drawing.Point(258, 279);
            this.comboDataExtStr1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.comboDataExtStr1.Name = "comboDataExtStr1";
            this.comboDataExtStr1.Size = new System.Drawing.Size(860, 28);
            this.comboDataExtStr1.TabIndex = 20;
            // 
            // comboDataExtStr2
            // 
            this.comboDataExtStr2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboDataExtStr2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboDataExtStr2.Dock = System.Windows.Forms.DockStyle.Top;
            this.comboDataExtStr2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDataExtStr2.FormattingEnabled = true;
            this.comboDataExtStr2.Location = new System.Drawing.Point(258, 355);
            this.comboDataExtStr2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.comboDataExtStr2.Name = "comboDataExtStr2";
            this.comboDataExtStr2.Size = new System.Drawing.Size(860, 28);
            this.comboDataExtStr2.TabIndex = 22;
            // 
            // checkDataExtBool1
            // 
            this.checkDataExtBool1.AutoSize = true;
            this.checkDataExtBool1.Location = new System.Drawing.Point(258, 583);
            this.checkDataExtBool1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.checkDataExtBool1.Name = "checkDataExtBool1";
            this.checkDataExtBool1.Size = new System.Drawing.Size(128, 24);
            this.checkDataExtBool1.TabIndex = 28;
            this.checkDataExtBool1.Text = "(Tick for Yes)";
            this.checkDataExtBool1.UseVisualStyleBackColor = true;
            // 
            // checkDataExtBool2
            // 
            this.checkDataExtBool2.AutoSize = true;
            this.checkDataExtBool2.Location = new System.Drawing.Point(258, 621);
            this.checkDataExtBool2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.checkDataExtBool2.Name = "checkDataExtBool2";
            this.checkDataExtBool2.Size = new System.Drawing.Size(128, 24);
            this.checkDataExtBool2.TabIndex = 29;
            this.checkDataExtBool2.Text = "(Tick for Yes)";
            this.checkDataExtBool2.UseVisualStyleBackColor = true;
            // 
            // labelExtDt1
            // 
            this.labelExtDt1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelExtDt1.AutoSize = true;
            this.labelExtDt1.Location = new System.Drawing.Point(34, 511);
            this.labelExtDt1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelExtDt1.Name = "labelExtDt1";
            this.labelExtDt1.Size = new System.Drawing.Size(51, 20);
            this.labelExtDt1.TabIndex = 28;
            this.labelExtDt1.Text = "label1";
            // 
            // labelExtDt2
            // 
            this.labelExtDt2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelExtDt2.AutoSize = true;
            this.labelExtDt2.Location = new System.Drawing.Point(34, 549);
            this.labelExtDt2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelExtDt2.Name = "labelExtDt2";
            this.labelExtDt2.Size = new System.Drawing.Size(60, 20);
            this.labelExtDt2.TabIndex = 29;
            this.labelExtDt2.Text = "label16";
            // 
            // dtpDataExtDt1
            // 
            this.dtpDataExtDt1.CustomFormat = "dd/MM/yyyy   HH:mm:ss";
            this.dtpDataExtDt1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDataExtDt1.Location = new System.Drawing.Point(258, 507);
            this.dtpDataExtDt1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dtpDataExtDt1.Name = "dtpDataExtDt1";
            this.dtpDataExtDt1.Size = new System.Drawing.Size(262, 26);
            this.dtpDataExtDt1.TabIndex = 26;
            // 
            // dtpDataExtDt2
            // 
            this.dtpDataExtDt2.Location = new System.Drawing.Point(258, 545);
            this.dtpDataExtDt2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dtpDataExtDt2.Name = "dtpDataExtDt2";
            this.dtpDataExtDt2.Size = new System.Drawing.Size(309, 26);
            this.dtpDataExtDt2.TabIndex = 27;
            // 
            // textDataNotes
            // 
            this.textDataNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textDataNotes.Location = new System.Drawing.Point(258, 704);
            this.textDataNotes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textDataNotes.Multiline = true;
            this.textDataNotes.Name = "textDataNotes";
            this.textDataNotes.Size = new System.Drawing.Size(860, 64);
            this.textDataNotes.TabIndex = 32;
            // 
            // pbRefDataSave
            // 
            this.pbRefDataSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbRefDataSave.ImageKey = "save32.png";
            this.pbRefDataSave.ImageList = this.imageListActions;
            this.pbRefDataSave.Location = new System.Drawing.Point(138, 778);
            this.pbRefDataSave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbRefDataSave.Name = "pbRefDataSave";
            this.pbRefDataSave.Size = new System.Drawing.Size(112, 35);
            this.pbRefDataSave.TabIndex = 33;
            this.pbRefDataSave.Text = "&Save";
            this.pbRefDataSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbRefDataSave.UseVisualStyleBackColor = true;
            this.pbRefDataSave.Click += new System.EventHandler(this.pbSave_Click);
            // 
            // pbRefDataCancel
            // 
            this.pbRefDataCancel.ImageKey = "Cancel32.png";
            this.pbRefDataCancel.ImageList = this.imageListActions;
            this.pbRefDataCancel.Location = new System.Drawing.Point(258, 778);
            this.pbRefDataCancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbRefDataCancel.Name = "pbRefDataCancel";
            this.pbRefDataCancel.Size = new System.Drawing.Size(112, 35);
            this.pbRefDataCancel.TabIndex = 34;
            this.pbRefDataCancel.Text = "&Cancel";
            this.pbRefDataCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbRefDataCancel.UseVisualStyleBackColor = true;
            this.pbRefDataCancel.Click += new System.EventHandler(this.pbCancel_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel2.Controls.Add(this.textDataExtendedOptions, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.pbDataExtendedOptions, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(254, 654);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(868, 45);
            this.tableLayoutPanel2.TabIndex = 30;
            // 
            // textDataExtendedOptions
            // 
            this.textDataExtendedOptions.Dock = System.Windows.Forms.DockStyle.Top;
            this.textDataExtendedOptions.Location = new System.Drawing.Point(4, 5);
            this.textDataExtendedOptions.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textDataExtendedOptions.Name = "textDataExtendedOptions";
            this.textDataExtendedOptions.Size = new System.Drawing.Size(339, 26);
            this.textDataExtendedOptions.TabIndex = 30;
            // 
            // pbDataExtendedOptions
            // 
            this.pbDataExtendedOptions.Location = new System.Drawing.Point(350, 4);
            this.pbDataExtendedOptions.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbDataExtendedOptions.Name = "pbDataExtendedOptions";
            this.pbDataExtendedOptions.Size = new System.Drawing.Size(171, 37);
            this.pbDataExtendedOptions.TabIndex = 31;
            this.pbDataExtendedOptions.Text = "Choose Options...";
            this.pbDataExtendedOptions.UseVisualStyleBackColor = true;
            this.pbDataExtendedOptions.Click += new System.EventHandler(this.pbDataExtendedOptions_Click);
            // 
            // imageListTabs
            // 
            this.imageListTabs.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTabs.ImageStream")));
            this.imageListTabs.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTabs.Images.SetKeyName(0, "admin48.png");
            this.imageListTabs.Images.SetKeyName(1, "Edit32.png");
            this.imageListTabs.Images.SetKeyName(2, "Line48.png");
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tabLayers, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblPasswordToModify, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBoxPassword, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1196, 1004);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // lblPasswordToModify
            // 
            this.lblPasswordToModify.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblPasswordToModify.AutoSize = true;
            this.lblPasswordToModify.Location = new System.Drawing.Point(4, 32);
            this.lblPasswordToModify.Margin = new System.Windows.Forms.Padding(4, 9, 4, 0);
            this.lblPasswordToModify.Name = "lblPasswordToModify";
            this.lblPasswordToModify.Size = new System.Drawing.Size(150, 20);
            this.lblPasswordToModify.TabIndex = 1;
            this.lblPasswordToModify.Text = "Password to Modify:";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxPassword.Location = new System.Drawing.Point(174, 25);
            this.textBoxPassword.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(306, 26);
            this.textBoxPassword.TabIndex = 2;
            this.textBoxPassword.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxPassword_Validating);
            // 
            // frmRefData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1196, 1004);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmRefData";
            this.Text = "Maintain Reference Data";
            this.Load += new System.EventHandler(this.frmRefData_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmRefData_KeyDown);
            this.tabLayers.ResumeLayout(false);
            this.tabRefTable.ResumeLayout(false);
            this.tlpRefTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRefTable)).EndInit();
            this.tlpRefTableActions.ResumeLayout(false);
            this.tabRefTableRow.ResumeLayout(false);
            this.tlpTableMaintenance.ResumeLayout(false);
            this.tlpTableMaintenance.PerformLayout();
            this.tabRefData.ResumeLayout(false);
            this.tlpRefData.ResumeLayout(false);
            this.tlpRefData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRefData)).EndInit();
            this.tabRefDataRow.ResumeLayout(false);
            this.tlpDataMaintenance.ResumeLayout(false);
            this.tlpDataMaintenance.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabLayers;
        private System.Windows.Forms.TabPage tabRefTable;
        private System.Windows.Forms.TabPage tabRefData;
        private System.Windows.Forms.TabPage tabRefDataRow;
        private System.Windows.Forms.TableLayoutPanel tlpRefTable;
        private System.Windows.Forms.DataGridView dgvRefTable;
        private System.Windows.Forms.TableLayoutPanel tlpRefData;
        private System.Windows.Forms.DataGridView dgvRefData;
        private System.Windows.Forms.Label TextHelpNote;
        private System.Windows.Forms.Button pbRefDataAdd;
        private System.Windows.Forms.Button pbRefDataDelete;
        private System.Windows.Forms.ImageList imageListActions;
        private System.Windows.Forms.TableLayoutPanel tlpDataMaintenance;
        private System.Windows.Forms.Label labelRefTableId;
        private System.Windows.Forms.Label labelRefId;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.Label labelShortDesc;
        private System.Windows.Forms.Label labelExtStr1a;
        private System.Windows.Forms.Label labelExtStr1b;
        private System.Windows.Forms.Label labelExtStr2a;
        private System.Windows.Forms.Label labelExtStr2b;
        private System.Windows.Forms.Label labelExtStr3;
        private System.Windows.Forms.Label labelExtNum1;
        private System.Windows.Forms.Label labelExtNum2;
        private System.Windows.Forms.Label labelExtBool1;
        private System.Windows.Forms.Label labelExtBool2;
        private System.Windows.Forms.Label labelNotes;
        private System.Windows.Forms.TextBox textDataRefTableId;
        private System.Windows.Forms.TextBox textDataRefId;
        private System.Windows.Forms.TextBox textDataDescription;
        private System.Windows.Forms.TextBox textDataShortDesc;
        private System.Windows.Forms.TextBox textDataExtStr1;
        private System.Windows.Forms.TextBox textDataExtStr2;
        private System.Windows.Forms.TextBox textDataExtStr3;
        private System.Windows.Forms.TextBox textDataExtNum1;
        private System.Windows.Forms.TextBox textDataExtNum2;
        private System.Windows.Forms.ComboBox comboDataExtStr1;
        private System.Windows.Forms.ComboBox comboDataExtStr2;
        private System.Windows.Forms.CheckBox checkDataExtBool1;
        private System.Windows.Forms.CheckBox checkDataExtBool2;
        private System.Windows.Forms.Label labelExtDt1;
        private System.Windows.Forms.Label labelExtDt2;
        private System.Windows.Forms.DateTimePicker dtpDataExtDt1;
        private System.Windows.Forms.DateTimePicker dtpDataExtDt2;
        private System.Windows.Forms.TextBox textDataNotes;
        private System.Windows.Forms.Button pbRefDataSave;
        private System.Windows.Forms.Button pbRefDataCancel;
        private System.Windows.Forms.ImageList imageListTabs;
        private System.Windows.Forms.TabPage tabRefTableRow;
        private System.Windows.Forms.TableLayoutPanel tlpRefTableActions;
        private System.Windows.Forms.Button pbRefTableAdd;
        private System.Windows.Forms.Button pbRefTableEdit;
        private System.Windows.Forms.Button pbRefTableDelete;
        private System.Windows.Forms.TableLayoutPanel tlpTableMaintenance;
        private System.Windows.Forms.Label lblReferenceTableID;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblDisplay;
        private System.Windows.Forms.Label lblIDCodeLabel;
        private System.Windows.Forms.Label lblescriptionLabel;
        private System.Windows.Forms.Label lblShortDescriptionLabel;
        private System.Windows.Forms.Label lblExtraString1Label;
        private System.Windows.Forms.Label lblExtraString2Label;
        private System.Windows.Forms.Label lblExtraString3Label;
        private System.Windows.Forms.Label lblExtraNumber1Label;
        private System.Windows.Forms.Label lblExtraNumber2Label;
        private System.Windows.Forms.Label lblExtraDate1Label;
        private System.Windows.Forms.Label lblExtraDate2Label;
        private System.Windows.Forms.Label lblExtraBool1Label;
        private System.Windows.Forms.Label lblExtraBool2Label;
        private System.Windows.Forms.Label lblForeignKey1ID;
        private System.Windows.Forms.Label lblForeignKey2ID;
        private System.Windows.Forms.Label lblTootipHelp;
        private System.Windows.Forms.Label lblNotes;
        private System.Windows.Forms.TextBox textTableRefTableId;
        private System.Windows.Forms.TextBox textTableDesc;
        private System.Windows.Forms.TextBox textTableHelpDesc;
        private System.Windows.Forms.TextBox textTableIdLabel;
        private System.Windows.Forms.TextBox textTableDescLabel;
        private System.Windows.Forms.TextBox textTableShortDescLabel;
        private System.Windows.Forms.TextBox textTableExtStr1Label;
        private System.Windows.Forms.TextBox textTableExtStr2Label;
        private System.Windows.Forms.TextBox textTableExtStr3Label;
        private System.Windows.Forms.TextBox textTableExtNum1Label;
        private System.Windows.Forms.TextBox textTableExtNum2Label;
        private System.Windows.Forms.TextBox textTableExtDt1Label;
        private System.Windows.Forms.TextBox textTableExtDt2Label;
        private System.Windows.Forms.TextBox textTableExtBool1Label;
        private System.Windows.Forms.TextBox textTableExtBool2Label;
        private System.Windows.Forms.TextBox textTableStr1FK;
        private System.Windows.Forms.TextBox textTableStr2FK;
        private System.Windows.Forms.TextBox textTableNotes;
        private System.Windows.Forms.CheckBox checkTableDisplayYn;
        private System.Windows.Forms.Button pbTableSave;
        private System.Windows.Forms.Button pbTableCancel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblPasswordToModify;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.TextBox textTableExtendedOptions;
        private System.Windows.Forms.Label lblExtendedBooleanOptions;
        private System.Windows.Forms.Label labelExtendedOptions;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox textDataExtendedOptions;
        private System.Windows.Forms.Button pbDataExtendedOptions;
    }
}
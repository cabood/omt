﻿namespace OMT.Forms
{
    partial class dlgSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dlgSearch));
            this.tlpSearch = new System.Windows.Forms.TableLayoutPanel();
            this.dgvSearch = new System.Windows.Forms.DataGridView();
            this.pbCancel = new System.Windows.Forms.Button();
            this.imageListActions = new System.Windows.Forms.ImageList(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxRefine = new System.Windows.Forms.TextBox();
            this.pbRefine = new System.Windows.Forms.Button();
            this.tlpSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpSearch
            // 
            this.tlpSearch.ColumnCount = 3;
            this.tlpSearch.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpSearch.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpSearch.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpSearch.Controls.Add(this.dgvSearch, 1, 2);
            this.tlpSearch.Controls.Add(this.pbCancel, 1, 3);
            this.tlpSearch.Controls.Add(this.tableLayoutPanel1, 1, 1);
            this.tlpSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpSearch.Location = new System.Drawing.Point(0, 0);
            this.tlpSearch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tlpSearch.Name = "tlpSearch";
            this.tlpSearch.RowCount = 5;
            this.tlpSearch.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpSearch.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 77F));
            this.tlpSearch.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpSearch.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tlpSearch.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tlpSearch.Size = new System.Drawing.Size(908, 500);
            this.tlpSearch.TabIndex = 0;
            // 
            // dgvSearch
            // 
            this.dgvSearch.AllowUserToAddRows = false;
            this.dgvSearch.AllowUserToDeleteRows = false;
            this.dgvSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSearch.Location = new System.Drawing.Point(12, 90);
            this.dgvSearch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvSearch.Name = "dgvSearch";
            this.dgvSearch.ReadOnly = true;
            this.dgvSearch.RowHeadersWidth = 62;
            this.dgvSearch.Size = new System.Drawing.Size(884, 348);
            this.dgvSearch.TabIndex = 1;
            this.dgvSearch.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSearch_CellContentDoubleClick);
            this.dgvSearch.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvSearch_RowHeaderMouseDoubleClick);
            this.dgvSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvSearch_KeyDown);
            // 
            // pbCancel
            // 
            this.pbCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.pbCancel.ImageKey = "Cancel32.png";
            this.pbCancel.ImageList = this.imageListActions;
            this.pbCancel.Location = new System.Drawing.Point(784, 448);
            this.pbCancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbCancel.Name = "pbCancel";
            this.pbCancel.Size = new System.Drawing.Size(112, 35);
            this.pbCancel.TabIndex = 1;
            this.pbCancel.Text = "&Cancel";
            this.pbCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbCancel.UseVisualStyleBackColor = true;
            this.pbCancel.Click += new System.EventHandler(this.pbCancel_Click);
            // 
            // imageListActions
            // 
            this.imageListActions.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListActions.ImageStream")));
            this.imageListActions.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListActions.Images.SetKeyName(0, "Cancel32.png");
            this.imageListActions.Images.SetKeyName(1, "Refresh32.png");
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 240F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBoxRefine, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.pbRefine, 2, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 13);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 29.54545F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70.45454F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(884, 67);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Refine Your Search";
            // 
            // textBoxRefine
            // 
            this.textBoxRefine.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBoxRefine.Location = new System.Drawing.Point(164, 28);
            this.textBoxRefine.Margin = new System.Windows.Forms.Padding(4, 9, 4, 5);
            this.textBoxRefine.Name = "textBoxRefine";
            this.textBoxRefine.Size = new System.Drawing.Size(476, 26);
            this.textBoxRefine.TabIndex = 0;
            this.textBoxRefine.Leave += new System.EventHandler(this.textBoxRefine_Leave);
            // 
            // pbRefine
            // 
            this.pbRefine.ImageKey = "Refresh32.png";
            this.pbRefine.ImageList = this.imageListActions;
            this.pbRefine.Location = new System.Drawing.Point(648, 24);
            this.pbRefine.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbRefine.Name = "pbRefine";
            this.pbRefine.Size = new System.Drawing.Size(112, 35);
            this.pbRefine.TabIndex = 2;
            this.pbRefine.Text = "Refresh";
            this.pbRefine.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.pbRefine.UseVisualStyleBackColor = true;
            this.pbRefine.Click += new System.EventHandler(this.pbRefine_Click);
            // 
            // dlgSearch
            // 
            this.AcceptButton = this.pbRefine;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.pbCancel;
            this.ClientSize = new System.Drawing.Size(908, 500);
            this.Controls.Add(this.tlpSearch);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "dlgSearch";
            this.Text = "Search";
            this.Activated += new System.EventHandler(this.dlgSearch_Activated);
            this.tlpSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpSearch;
        private System.Windows.Forms.DataGridView dgvSearch;
        private System.Windows.Forms.Button pbCancel;
        private System.Windows.Forms.ImageList imageListActions;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxRefine;
        private System.Windows.Forms.Button pbRefine;
    }
}
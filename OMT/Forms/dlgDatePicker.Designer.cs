﻿namespace OMT.Forms
{
    partial class dlgDatePicker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mcChooser = new System.Windows.Forms.MonthCalendar();
            this.pbCancel = new System.Windows.Forms.Button();
            this.pbOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // mcChooser
            // 
            this.mcChooser.Location = new System.Drawing.Point(-1, 0);
            this.mcChooser.Name = "mcChooser";
            this.mcChooser.TabIndex = 1;
            // 
            // pbCancel
            // 
            this.pbCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.pbCancel.Location = new System.Drawing.Point(147, 163);
            this.pbCancel.Name = "pbCancel";
            this.pbCancel.Size = new System.Drawing.Size(75, 23);
            this.pbCancel.TabIndex = 2;
            this.pbCancel.Text = "&Cancel";
            this.pbCancel.UseVisualStyleBackColor = true;
            this.pbCancel.Click += new System.EventHandler(this.pbCancel_Click);
            // 
            // pbOK
            // 
            this.pbOK.Location = new System.Drawing.Point(66, 163);
            this.pbOK.Name = "pbOK";
            this.pbOK.Size = new System.Drawing.Size(75, 23);
            this.pbOK.TabIndex = 3;
            this.pbOK.Text = "&Accept";
            this.pbOK.UseVisualStyleBackColor = true;
            this.pbOK.Click += new System.EventHandler(this.pbOK_Click);
            // 
            // dlgDatePicker
            // 
            this.AcceptButton = this.pbOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.pbCancel;
            this.ClientSize = new System.Drawing.Size(226, 189);
            this.Controls.Add(this.pbOK);
            this.Controls.Add(this.pbCancel);
            this.Controls.Add(this.mcChooser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "dlgDatePicker";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Choose Date";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MonthCalendar mcChooser;
        private System.Windows.Forms.Button pbCancel;
        private System.Windows.Forms.Button pbOK;

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using UbiSqlFramework;

namespace OMT.Forms
{
    public partial class frmCustAddress : Form, IGenericExchange
    {
        string sCustId;
        string sDefAddrId;
        string sDefDelAddrId;
        DataSet dsAddresses;
        bool bNewRow = false;

        public frmCustAddress()
        {
            InitializeComponent();
            InitFormData();
        }
        private void InitFormData()
        {
            LoadCountries();
            LoadAddressTypes();
            ClearFormDetails(true);
        }

        private void LoadCountries()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbCountry.DataSource = null;
                cmbCountry.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'COUNTRY' ORDER BY DESCRIPTION";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "CountryData", ref refContext);
                cmbCountry.DataSource = refContext.Tables["CountryData"];
                cmbCountry.ValueMember = "REF_ID";
                cmbCountry.DisplayMember = "DESCRIPTION";
                cmbCountry.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Countries Error", MessageBoxButtons.OK);
            }
        }

        private void LoadAddressTypes()
        {
            string sSql;
            bool b;
            DataSet refContext;
            try
            {
                cmbAddressType.DataSource = null;
                cmbAddressType.Items.Clear();
                sSql = "select REF_ID, DESCRIPTION from REF_DATA where DISPLAY_YN = 1 and REF_TABLE_ID = 'ADDRESSTYPE'";
                refContext = new DataSet();
                b = SqlHost.GetDataSet(sSql, "AddrTypeData", ref refContext);
                cmbAddressType.DataSource = refContext.Tables["AddrTypeData"];
                cmbAddressType.ValueMember = "REF_ID";
                cmbAddressType.DisplayMember = "DESCRIPTION";
                cmbAddressType.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Load Address Types Error", MessageBoxButtons.OK);
            }
        }
        
        private void ClearFormDetails(bool bClearCustId)
        {
            if (bClearCustId)
            {
                textCustId.Text = "";
                textCustName.Text = "";
                sDefAddrId = "";
                sDefDelAddrId = "";
            }
            textAddressId.Text = "";
            textAddr1.Text = "";
            textAddr2.Text = "";
            textAddr3.Text = "";
            textState.Text = "";
            textPostCode.Text = "";
            cmbCountry.SelectedIndex = -1;
            cmbAddressType.SelectedIndex = -1;
            textPhone.Text = "";
            textFax.Text = "";
            textEmail.Text = "";
            textNotes.Text = "";
            cbIsMain.Checked = false;
            cbIsDefaultDel.Checked = false;
            cbIsDefaultDel.Enabled = false;
            cbIsMain.Enabled = false;
            textAddrLabel.Text = "";
        }

        private void frmCustAddress_Load(object sender, EventArgs e)
        {
            this.ActiveControl = dgvData;
        }

        private void dgvData_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string sId = "";
            if (dgvData.SelectedRows.Count > 0)
            {
                sId = ((string)(((DataRowView)dgvData.SelectedRows[0].DataBoundItem).Row)["ADDRESS_ID"]).ToString();
            }
            else if (dgvData.SelectedCells.Count > 0)
            {
                int nId = dgvData.SelectedCells[0].RowIndex;
                object cId = ((DataGridViewRow)(dgvData.Rows[nId]).DataBoundItem);
            }
            if (sId.Length > 0)
                EditRow(sId);
        }

        private void dgvData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string sId = "";
            if (dgvData.SelectedRows.Count > 0)
            {
                sId = ((string)(((DataRowView)dgvData.SelectedRows[0].DataBoundItem).Row)["ADDRESS_ID"]).ToString();
            }
            else if (dgvData.SelectedCells.Count > 0)
            {
                int nId = dgvData.SelectedCells[0].RowIndex;
                sId = ((DataRowView)(dgvData.Rows[nId]).DataBoundItem).Row["ADDRESS_ID"].ToString();
            }
            if (sId.Length > 0)
                EditRow(sId);
        }

        private void dgvData_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string sId = "";
            if (dgvData.SelectedRows.Count > 0)
            {
                sId = ((string)(((DataRowView)dgvData.SelectedRows[0].DataBoundItem).Row)["ADDRESS_ID"]).ToString();
            }
            else if (dgvData.SelectedCells.Count > 0)
            {
                int nId = dgvData.SelectedCells[0].RowIndex;
                object cId = ((DataGridViewRow)(dgvData.Rows[nId]).DataBoundItem);
            }
            if (sId.Length > 0)
                EditRow(sId);
        }

        private void EditRow(string sId)
        {
            RetrieveAddressDetails(sId);
        }

        private void pbAdd_Click(object sender, EventArgs e)
        {
            string sAddrId = GetNextAddressId(sCustId);
            RetrieveAddressDetails(sAddrId);
            this.ActiveControl = textAddr1;
        }

        private string GetNextAddressId(string sId)
        {
            string sSql;
            SqlCommand sqlCmd;
            SqlDataReader cReader;
            string sCurrentMax;
            string sNewAddr = "";
            int nCurrentMax;
            int nNewMax;
            int nAddrCount = 0;
            string sAddrCount = "";
            try
            {
                sSql = "select count(*) as NumAddresses from CUSTOMER_ADDR where CUSTOMER_ID = @CUST_ID";
                sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                sqlCmd.Parameters.AddWithValue("@CUST_ID", sId);
                cReader = sqlCmd.ExecuteReader();
                while (cReader.Read())
                {
                    sAddrCount = cReader["NumAddresses"].ToString();
                    nAddrCount = Convert.ToInt32(sAddrCount);
                }
                cReader.Close();

                if (nAddrCount == 0)
                {
                    return "001";
                }
                else
                {
                    sSql = "select max(ADDRESS_ID) as CURRENT_MAX from CUSTOMER_ADDR where CUSTOMER_ID = @CUST_ID";
                    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                    sqlCmd.Parameters.AddWithValue("@CUST_ID", sId);
                    cReader = sqlCmd.ExecuteReader();
                    while (cReader.Read())
                    {
                        sCurrentMax = cReader["CURRENT_MAX"].ToString();
                        nCurrentMax = Convert.ToInt32(sCurrentMax);
                        nNewMax = nCurrentMax + 1;
                        sNewAddr = nNewMax.ToString("000");
                    }
                    cReader.Close();
                    return sNewAddr;
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        private void RetrieveAddressDetails(string sAddrId)
        {
            SqlCommand rCom;
            SqlDataReader cReader;
            try
            {
                ClearFormDetails(false);
                if (sAddrId.Length == 0)
                {
                    return;
                }

                textAddressId.Text = sAddrId;

                rCom = new SqlCommand(
                    "select " +
                    "A.CUSTOMER_ID, A.ADDRESS_ID, A.ADDRESS_TYPE, A.ADDR_1, A.ADDR_2, A.ADDR_3, A.STATE, A.POST_CODE, A.COUNTRY, A.PHONE, " +
                    "A.FAX, A.EMAIL_ADDRESS, A.IS_DELIVERY, A.IS_3PL, A.NOTES, B.PRIMARY_ADDRESS, B.PRIMARY_DEL_ADDR, A.ADDR_LABEL " +
                    "from " +
                    "CUSTOMER_ADDR A left outer join CUSTOMER B on A.CUSTOMER_ID = B.CUSTOMER_ID " +
                    "WHERE A.CUSTOMER_ID = @CUST_ID and A.ADDRESS_ID = @ADDR_ID", SqlHost.DBConn
                    );
                rCom.Parameters.AddWithValue("@CUST_ID", sCustId);
                rCom.Parameters.AddWithValue("@ADDR_ID", sAddrId);

                cReader = rCom.ExecuteReader();

                bNewRow = !cReader.HasRows;

                while (cReader.Read())
                {
                    if (cReader["ADDR_1"] != System.DBNull.Value)
                        textAddr1.Text = cReader["ADDR_1"].ToString();
                    if (cReader["ADDR_2"] != System.DBNull.Value)
                        textAddr2.Text = cReader["ADDR_2"].ToString();
                    if (cReader["ADDR_3"] != System.DBNull.Value)
                        textAddr3.Text = cReader["ADDR_3"].ToString();
                    if (cReader["STATE"] != System.DBNull.Value)
                        textState.Text = cReader["STATE"].ToString();
                    if (cReader["POST_CODE"] != System.DBNull.Value)
                        textPostCode.Text = cReader["POST_CODE"].ToString();
                    if (cReader["ADDRESS_TYPE"] != System.DBNull.Value && cReader["ADDRESS_TYPE"].ToString().Length > 0)
                    {
                        cmbAddressType.SelectedValue = (string)cReader["ADDRESS_TYPE"];
                    }
                    else
                    {
                        cmbAddressType.SelectedIndex = -1;
                    }
                    if (cReader["COUNTRY"] != System.DBNull.Value && cReader["COUNTRY"].ToString().Length > 0)
                    {
                        cmbCountry.SelectedValue = (string)cReader["COUNTRY"];
                    }
                    else
                    {
                        cmbCountry.SelectedIndex = -1;
                    }
                    if (cReader["PHONE"] != System.DBNull.Value)
                        textPhone.Text = cReader["PHONE"].ToString();
                    if (cReader["FAX"] != System.DBNull.Value)
                        textFax.Text = cReader["FAX"].ToString();
                    if (cReader["EMAIL_ADDRESS"] != System.DBNull.Value)
                        textEmail.Text = cReader["EMAIL_ADDRESS"].ToString();
                    if (cReader["PHONE"] != System.DBNull.Value)
                        textPhone.Text = cReader["PHONE"].ToString();
                    if (cReader["NOTES"] != System.DBNull.Value)
                        textNotes.Text = cReader["NOTES"].ToString();
                    if (cReader["ADDR_LABEL"] != System.DBNull.Value)
                        textAddrLabel.Text = cReader["ADDR_LABEL"].ToString();

                    if (cReader["IS_DELIVERY"] != System.DBNull.Value)
                        cbIsDel.Checked = (bool)cReader["IS_DELIVERY"];
                    if (cReader["IS_3PL"] != System.DBNull.Value)
                        cbIs3PL.Checked = (bool)cReader["IS_3PL"];

                    this.cbIsMain.Checked = (cReader["ADDRESS_ID"].ToString() == cReader["PRIMARY_ADDRESS"].ToString());
                    this.cbIsDefaultDel.Checked = (cReader["ADDRESS_ID"].ToString() == cReader["PRIMARY_DEL_ADDR"].ToString());

                    pbSetOffice.Enabled = !cbIsMain.Checked;
                    pbSetPrimaryDel.Enabled = !cbIsDefaultDel.Checked;

                }
                cReader.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Retrieve Address Details Error", MessageBoxButtons.OK);
            }

        }

        private void pbDelete_Click(object sender, EventArgs e)
        {
            string sId = "";
            if (dgvData.SelectedRows.Count > 0)
            {
                sId = ((string)(((DataRowView)dgvData.SelectedRows[0].DataBoundItem).Row)["ADDRESS_ID"]).ToString();
            }
            else if (dgvData.SelectedCells.Count > 0)
            {
                int nId = dgvData.SelectedCells[0].RowIndex;
                sId = ((DataRowView)(dgvData.Rows[nId]).DataBoundItem).Row["ADDRESS_ID"].ToString();
            }
            if (sId.Length > 0)
                DeleteRow(sId);
        }

        private bool IsPrimaryDel(string sId)
        {
            string sSql;
            SqlCommand sqlCmd;
            SqlDataReader cReader;
            bool bReturn = true;
            try
            {
                sSql = "select PRIMARY_DEL_ADDR from CUSTOMER where CUSTOMER_ID = @CUST_ID";
                sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                sqlCmd.Parameters.AddWithValue("@CUST_ID", sCustId);
                cReader = sqlCmd.ExecuteReader();
                while (cReader.Read())
                {
                    bReturn = ((string)cReader["PRIMARY_DEL_ADDR"] == sId);
                }
                cReader.Close();
                return bReturn;

            }
            catch (Exception ex)
            {
                return true;
            }
        }

        private bool IsOfficeAddr(string sId)
        {
            string sSql;
            SqlCommand sqlCmd;
            SqlDataReader cReader;
            bool bReturn = true;
            try
            {
                sSql = "select PRIMARY_ADDRESS from CUSTOMER where CUSTOMER_ID = @CUST_ID";
                sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                sqlCmd.Parameters.AddWithValue("@CUST_ID", sCustId);
                cReader = sqlCmd.ExecuteReader();
                while (cReader.Read())
                {
                    bReturn = ((string)cReader["PRIMARY_ADDRESS"] == sId);
                }
                cReader.Close();
                return bReturn;

            }
            catch (Exception ex)
            {
                return true;
            }
        }

        private void pbSetOffice_Click(object sender, EventArgs e)
        {
            if (SaveRow())
            {
                string sSql;
                SqlCommand sqlCmd;
                sSql = "update CUSTOMER set PRIMARY_ADDRESS = @PRIMARY_ADDRESS where CUSTOMER_ID = @CUSTOMER_ID";
                sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                sqlCmd.Parameters.AddWithValue("@CUSTOMER_ID", sCustId);
                sqlCmd.Parameters.AddWithValue("@PRIMARY_ADDRESS", textAddressId.Text);
                sqlCmd.ExecuteNonQuery();
                sqlCmd = null;
                RetrieveAddressDetails(textAddressId.Text);
            }
        }

        private void pbSetPrimaryDel_Click(object sender, EventArgs e)
        {
            string sSql;
            SqlCommand sqlCmd;
            if (SaveRow())
            {
                if (!cbIsDel.Checked)
                {
                    sSql = "update CUSTOMER_ADDR set IS_DELIVERY = 1 where CUSTOMER_ID = @CUSTOMER_ID and ADDRESS_ID = @ADDRESS_ID";
                    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                    sqlCmd.Parameters.AddWithValue("@CUSTOMER_ID", sCustId);
                    sqlCmd.Parameters.AddWithValue("@ADDRESS_ID", textAddressId.Text);
                    sqlCmd.ExecuteNonQuery();
                    sqlCmd = null;
                }
                sSql = "update CUSTOMER set PRIMARY_DEL_ADDR = @PRIMARY_ADDRESS where CUSTOMER_ID = @CUSTOMER_ID";
                sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                sqlCmd.Parameters.AddWithValue("@CUSTOMER_ID", sCustId);
                sqlCmd.Parameters.AddWithValue("@PRIMARY_ADDRESS", textAddressId.Text);
                sqlCmd.ExecuteNonQuery();
                sqlCmd = null;
                RetrieveAddressDetails(textAddressId.Text);
            }
        }

        private void pbSave_Click(object sender, EventArgs e)
        {
            if (SaveRow())
            {
                ClearFormDetails(false);
                RetrieveCustomerDetails(sCustId);
                this.ActiveControl = dgvData;
            }
        }

        private bool SaveRow()
        {
            string sSql;
            SqlCommand sqlCmd;
            try
            {
                if (textAddressId.Text.Length > 0)
                {
                    if (bNewRow)
                    {
                        sSql = "insert into CUSTOMER_ADDR ( " +
                               "CUSTOMER_ID, ADDRESS_ID, ADDRESS_TYPE, ADDR_1, ADDR_2, ADDR_3, STATE, POST_CODE, COUNTRY, " +
                               "PHONE, FAX, EMAIL_ADDRESS, IS_DELIVERY, IS_3PL, NOTES, ADDR_LABEL ) " +
                               "VALUES ( " +
                               "@CUSTOMER_ID, @ADDRESS_ID, @ADDRESS_TYPE, @ADDR_1, @ADDR_2, @ADDR_3, @STATE, @POST_CODE, @COUNTRY, " +
                               "@PHONE, @FAX, @EMAIL_ADDRESS, @IS_DELIVERY, @IS_3PL, @NOTES, @ADDR_LABEL )";
                    }
                    else
                    {
                        sSql = "update CUSTOMER_ADDR set " +
                               "CUSTOMER_ID = @CUSTOMER_ID, ADDRESS_ID = @ADDRESS_ID, ADDRESS_TYPE = @ADDRESS_TYPE, " +
                               "ADDR_1 = @ADDR_1, ADDR_2 = @ADDR_2, ADDR_3 = @ADDR_3, STATE = @STATE, POST_CODE = @POST_CODE, " +
                               "COUNTRY = @COUNTRY, PHONE = @PHONE, FAX = @FAX, EMAIL_ADDRESS = @EMAIL_ADDRESS, " +
                               "IS_DELIVERY = @IS_DELIVERY, IS_3PL = @IS_3PL, NOTES = @NOTES, ADDR_LABEL = @ADDR_LABEL " +
                               "where CUSTOMER_ID = @CUSTOMER_ID and ADDRESS_ID = @ADDRESS_ID";
                    }
                    sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
                    sqlCmd.Parameters.AddWithValue("@CUSTOMER_ID", sCustId.ToUpper());
                    sqlCmd.Parameters.AddWithValue("@ADDRESS_ID", textAddressId.Text.ToUpper());
                    sqlCmd.Parameters.AddWithValue("@ADDR_1", textAddr1.Text);
                    sqlCmd.Parameters.AddWithValue("@ADDR_2", textAddr2.Text);
                    sqlCmd.Parameters.AddWithValue("@ADDR_3", textAddr3.Text);
                    sqlCmd.Parameters.AddWithValue("@STATE", textState.Text);
                    sqlCmd.Parameters.AddWithValue("@POST_CODE", textPostCode.Text);
                    if (cmbAddressType.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@ADDRESS_TYPE", cmbAddressType.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@ADDRESS_TYPE", "DELIVERY");
                    }
                    if (cmbCountry.SelectedIndex != -1)
                    {
                        sqlCmd.Parameters.AddWithValue("@COUNTRY", cmbCountry.SelectedValue);
                    }
                    else
                    {
                        sqlCmd.Parameters.AddWithValue("@COUNTRY", "AUS");
                    }
                    sqlCmd.Parameters.AddWithValue("@PHONE", textPhone.Text);
                    sqlCmd.Parameters.AddWithValue("@FAX", textFax.Text);
                    sqlCmd.Parameters.AddWithValue("@EMAIL_ADDRESS", textEmail.Text);
                    sqlCmd.Parameters.AddWithValue("@NOTES", textNotes.Text);
                    sqlCmd.Parameters.AddWithValue("@IS_DELIVERY", (cbIsDel.Checked ? 1 : 0));
                    sqlCmd.Parameters.AddWithValue("@IS_3PL", (cbIs3PL.Checked ? 1 : 0));
                    sqlCmd.Parameters.AddWithValue("@ADDR_LABEL", textAddrLabel.Text);
                    sqlCmd.ExecuteNonQuery();
                }
                sqlCmd = null;
                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " returned an error: " + ex.Message, "Save Error", MessageBoxButtons.OK);
                return false;
            }
        }

        private void pbCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pbDeleteCurrent_Click(object sender, EventArgs e)
        {
            DeleteRow(textAddressId.Text);
        }

        private void DeleteRow(string sAddrId)
        {
            string sSql;
            if (IsPrimaryDel(sAddrId))
            {
                MessageBox.Show("Cannot delete the primary delivery address.", "No no no!", MessageBoxButtons.OK);
                return;
            }
            if (IsOfficeAddr(sAddrId))
            {
                MessageBox.Show("Cannot delete the office address.", "No no no!", MessageBoxButtons.OK);
                return;
            }
            if (bNewRow == false && MessageBox.Show(this, "Are you sure you want to delete this address?", "Wait Wait!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Hand) == DialogResult.Yes)
            {
                SqlCommand sqlCmd = new SqlCommand();
                sSql = "DELETE FROM CUSTOMER_ADDR WHERE CUSTOMER_ID = @CUST_ID and ADDRESS_ID = @ADDR_ID";
                sqlCmd.Parameters.AddWithValue("@CUST_ID", sCustId);
                sqlCmd.Parameters.AddWithValue("@ADDR_ID", sAddrId);
                sqlCmd.Connection = SqlHost.DBConn;
                sqlCmd.CommandText = sSql;
                sqlCmd.ExecuteNonQuery();
                ClearFormDetails(false);
                this.ActiveControl = dgvData;
            }
            RetrieveCustomerDetails(sCustId);
            this.ActiveControl = dgvData;
            ClearFormDetails(false);
        }

        private void RetrieveCustomerDetails(string sId)
        {
            string sSql;
            SqlCommand sqlCmd;
            SqlDataReader cReader;
            dsAddresses = new DataSet();
            bool b;

            sSql = "select * from CUSTOMER where CUSTOMER_ID = @CUST_ID";
            sqlCmd = new SqlCommand(sSql, SqlHost.DBConn);
            sqlCmd.Parameters.AddWithValue("@CUST_ID", sId);
            cReader = sqlCmd.ExecuteReader();
            while (cReader.Read())
            {
                textCustId.Text = cReader["CUSTOMER_ID"].ToString();
                textCustName.Text = cReader["NAME_1"].ToString();
                this.Text = "Customer Addresses - " + textCustName.Text;
            }
            cReader.Close();
            
            sSql = "select A.CUSTOMER_ID, A.ADDRESS_ID, B.DESCRIPTION, A.ADDR_1, A.ADDR_2, A.STATE, A.POST_CODE, " +
                "CASE when C.PRIMARY_DEL_ADDR = A.ADDRESS_ID THEN 'YES' ELSE '' END AS IS_PRIMARY_DEL " +
                "from CUSTOMER_ADDR A left outer join REF_DATA B on A.ADDRESS_TYPE = B.REF_ID AND B.REF_TABLE_ID = 'ADDRESSTYPE' " +
                "left outer join CUSTOMER C on C.CUSTOMER_ID = A.CUSTOMER_ID " +
                "where A.CUSTOMER_ID = '" + sId + "'";
            b = SqlHost.GetDataSet(sSql, "Addresses", ref dsAddresses);
            dgvData.DataSource = dsAddresses.Tables["Addresses"];
            dgvData.Refresh();

            FormatDataView();
        }

        private void FormatDataView()
        {
            dgvData.Columns["CUSTOMER_ID"].Visible = false;
            dgvData.Columns["ADDRESS_ID"].Width = 80;
            dgvData.Columns["ADDRESS_ID"].HeaderText = "Address Id";
            dgvData.Columns["DESCRIPTION"].Width = 150;
            dgvData.Columns["DESCRIPTION"].HeaderText = "Address Type";
            dgvData.Columns["ADDR_1"].Width = 190;
            dgvData.Columns["ADDR_1"].HeaderText = "Street";
            dgvData.Columns["ADDR_2"].Width = 150;
            dgvData.Columns["ADDR_2"].HeaderText = "Suburb / City";
            dgvData.Columns["STATE"].Width = 70;
            dgvData.Columns["STATE"].HeaderText = "State";
            dgvData.Columns["POST_CODE"].Width = 70;
            dgvData.Columns["POST_CODE"].HeaderText = "PostCode";
            dgvData.Columns["IS_PRIMARY_DEL"].Width = 100;
            dgvData.Columns["IS_PRIMARY_DEL"].HeaderText = "Primary Delivery?";
            dgvData.Columns["IS_PRIMARY_DEL"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

        }

        public void GenericTransfer(GenericParam g)
        {
            if (g.IdField == "CUSTOMER_ID")
            {
                sCustId = g.ValueField;
                RetrieveCustomerDetails(sCustId);
            }
        }

    }

}

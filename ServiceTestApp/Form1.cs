﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UbiWeb.Security;
using UbiSqlFramework;
using UbiWeb.Data;
using UbiWeb.Services;
using UbiWeb.Factory;


namespace ServiceTestApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public string MySessionKey
        {
            get { return textSessionKey.Text; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ////ServiceReference1.MFServicesSoapClient obj = new ServiceReference1.MFServicesSoapClient();
            ////string str = obj.GetNextId(textRequest.Text);
            ////textResponse.Text = str;
            ////obj = null;
            //RetrieveReportHeader();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ////ServiceReference1.StringResponse r = new ServiceReference1.StringResponse();
            ////ServiceReference1.MFServicesSoapClient obj = new ServiceReference1.MFServicesSoapClient();
            ////r = obj.TestStringResponse();
            ////textResponse.Text = r.Response;
            //UpdateReportHeader(ServiceReference1.OperationVerb.Update);
        }

        private void textResponse_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string un = textUserNameE.Text;
            string pwd = textPasswordE.Text;
            ServiceReference1.LoginResponse r = new ServiceReference1.LoginResponse();
            ServiceReference1.MFServicesSoapClient obj = new ServiceReference1.MFServicesSoapClient();
            r = obj.LoginUser(un, pwd);
            textSessionKey.Text = r.Response.SessionKey;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void pbEncrypt_Click(object sender, EventArgs e)
        {
            textUserNameE.Text = SecurityManager.EncryptWithRandom(textUsername.Text);
            textPasswordE.Text = SecurityManager.EncryptWithRandom(textPassword.Text);
        }


        //private void RetrieveReportHeader()
        //{
        //    try
        //    {
        //        string sId = textReportId.Text;
        //        if (sId != null && sId.Length > 0)
        //        {
        //            ServiceReference1.ReportQuickHeaderResponse h = new ServiceReference1.ReportQuickHeaderResponse();
        //            ServiceReference1.MFServicesSoapClient obj = new ServiceReference1.MFServicesSoapClient();

        //            h = obj.GetReportQuickHeader(MySessionKey, sId);
        //            textTitle.Text = h.Response.Title;
        //            textDescription.Text = h.Response.Description;
        //            textIDType.Text = h.Response.IdType;
        //            textComment.Text = h.Response.Comment;

        //            return;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        return;
        //    }
        //}

        //private void UpdateReportHeader(ServiceReference1.OperationVerb v)
        //{
        //    string sId = textReportId.Text;
        //    List<string> ops = new List<string>();
        //    List<ServiceReference1.ReportQuickHeaderOperation> qOps = new List<ServiceReference1.ReportQuickHeaderOperation>();
        //    if (sId != null && sId.Length > 0)
        //    {
        //        ServiceReference1.ReportQuickHeader c = new ServiceReference1.ReportQuickHeader();
        //        ServiceReference1.ReportQuickHeaderOperation o = new ServiceReference1.ReportQuickHeaderOperation();
        //        ServiceReference1.Transaction t = new ServiceReference1.Transaction();
        //        ServiceReference1.TransactionResponse tr = new ServiceReference1.TransactionResponse();

        //        ServiceReference1.MFServicesSoapClient obj = new ServiceReference1.MFServicesSoapClient();



        //        //t = UbiWebFactory.CreateTransaction(r, OperationVerb.Update);
        //        Single nTranSeq = 1;
        //        c.ReportId = textReportId.Text;
        //        c.Title = textTitle.Text;
        //        c.Description = textDescription.Text;
        //        c.IdType = textIDType.Text;
        //        c.Comment = textComment.Text;

        //        t.TransactionId = nTranSeq.ToString();
        //        o.OperationId = "ReportQuickHeader-" + nTranSeq.ToString("00000");
        //        o.Sequence = nTranSeq;
        //        if (t.OperationSequence == null)
        //        {
        //            ops = new List<string>();
        //        }
        //        else
        //        {
        //            ops = t.OperationSequence.ToList<string>();
        //        }
        //        ops.Add(o.OperationId);
        //        t.OperationSequence = ops.ToArray<string>();
        //        o.Verb = v;
        //        o.OperationEntity = ServiceReference1.OperationEntity.ReportQuickHeader;
        //        o.DataMember = c;
        //        if (t.ReportQuickHeaderOperations == null)
        //        {
        //            qOps = new List<ServiceReference1.ReportQuickHeaderOperation>();
        //        }
        //        else
        //        {
        //            qOps = t.ReportQuickHeaderOperations.ToList<ServiceReference1.ReportQuickHeaderOperation>();
        //        }
        //        qOps.Add(o);
        //        t.ReportQuickHeaderOperations = qOps.ToArray<ServiceReference1.ReportQuickHeaderOperation>();

        //        tr = obj.ProcessTransaction(MySessionKey, t);



        //        return;
        //    }

        //}

        private void pbDelete_Click(object sender, EventArgs e)
        {
            //UpdateReportHeader(ServiceReference1.OperationVerb.Delete);
        }

        private void pbNonPrimitiveTest_Click(object sender, EventArgs e)
        {
            //ServiceReference1.NonPrimitive p = new ServiceReference1.NonPrimitive();

            //ServiceReference1.MFServicesSoapClient obj = new ServiceReference1.MFServicesSoapClient();

            //p.PrimitiveId = "Test Non Primitive";
            //p.Description = "Non Primitive test case";
            //Single c = 0;
            //bool bConvert = Single.TryParse(textNPCounter.Text, out c);
            //p.Counter = c;

            //ServiceReference1.NonPrimitive q = obj.IncrementNonPrimitive(p);

            //textNPNewCounter.Text = q.Counter.ToString();
            
        }

        private void pbCreate_Click(object sender, EventArgs e)
        {
            //UpdateReportHeader(ServiceReference1.OperationVerb.Create);
        }

        private void pbEncryptRaw_Click(object sender, EventArgs e)
        {
            textUserNameE.Text = SecurityManager.EncryptText(textUsername.Text);
            textPasswordE.Text = SecurityManager.EncryptText(textPassword.Text);
        }
    }





}

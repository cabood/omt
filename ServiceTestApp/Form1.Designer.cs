﻿namespace ServiceTestApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbRetrieve = new System.Windows.Forms.Button();
            this.textUsername = new System.Windows.Forms.TextBox();
            this.textSessionKey = new System.Windows.Forms.TextBox();
            this.pbUpdate = new System.Windows.Forms.Button();
            this.pbDelete = new System.Windows.Forms.Button();
            this.pbLogin = new System.Windows.Forms.Button();
            this.textPassword = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pbCreate = new System.Windows.Forms.Button();
            this.textNPNewCounter = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textReportId = new System.Windows.Forms.TextBox();
            this.textTitle = new System.Windows.Forms.TextBox();
            this.textDescription = new System.Windows.Forms.TextBox();
            this.textIDType = new System.Windows.Forms.TextBox();
            this.textComment = new System.Windows.Forms.TextBox();
            this.pbNonPrimitiveTest = new System.Windows.Forms.Button();
            this.textNPCounter = new System.Windows.Forms.TextBox();
            this.pbEncrypt = new System.Windows.Forms.Button();
            this.pbEncryptRaw = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textPasswordE = new System.Windows.Forms.TextBox();
            this.textUserNameE = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbRetrieve
            // 
            this.pbRetrieve.Location = new System.Drawing.Point(543, 10);
            this.pbRetrieve.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbRetrieve.Name = "pbRetrieve";
            this.pbRetrieve.Size = new System.Drawing.Size(162, 37);
            this.pbRetrieve.TabIndex = 7;
            this.pbRetrieve.Text = "Retrieve";
            this.pbRetrieve.UseVisualStyleBackColor = true;
            this.pbRetrieve.Click += new System.EventHandler(this.button1_Click);
            // 
            // textUsername
            // 
            this.textUsername.Location = new System.Drawing.Point(159, 15);
            this.textUsername.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textUsername.Name = "textUsername";
            this.textUsername.Size = new System.Drawing.Size(175, 26);
            this.textUsername.TabIndex = 1;
            // 
            // textSessionKey
            // 
            this.textSessionKey.Location = new System.Drawing.Point(159, 85);
            this.textSessionKey.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textSessionKey.Name = "textSessionKey";
            this.textSessionKey.Size = new System.Drawing.Size(610, 26);
            this.textSessionKey.TabIndex = 3;
            this.textSessionKey.TextChanged += new System.EventHandler(this.textResponse_TextChanged);
            // 
            // pbUpdate
            // 
            this.pbUpdate.Location = new System.Drawing.Point(543, 55);
            this.pbUpdate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbUpdate.Name = "pbUpdate";
            this.pbUpdate.Size = new System.Drawing.Size(162, 37);
            this.pbUpdate.TabIndex = 8;
            this.pbUpdate.Text = "Update";
            this.pbUpdate.UseVisualStyleBackColor = true;
            this.pbUpdate.Click += new System.EventHandler(this.button2_Click);
            // 
            // pbDelete
            // 
            this.pbDelete.Location = new System.Drawing.Point(543, 100);
            this.pbDelete.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbDelete.Name = "pbDelete";
            this.pbDelete.Size = new System.Drawing.Size(162, 37);
            this.pbDelete.TabIndex = 9;
            this.pbDelete.Text = "Delete";
            this.pbDelete.UseVisualStyleBackColor = true;
            this.pbDelete.Click += new System.EventHandler(this.pbDelete_Click);
            // 
            // pbLogin
            // 
            this.pbLogin.Location = new System.Drawing.Point(341, 15);
            this.pbLogin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbLogin.Name = "pbLogin";
            this.pbLogin.Size = new System.Drawing.Size(162, 62);
            this.pbLogin.TabIndex = 6;
            this.pbLogin.Text = "Login";
            this.pbLogin.UseVisualStyleBackColor = true;
            this.pbLogin.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // textPassword
            // 
            this.textPassword.Location = new System.Drawing.Point(159, 50);
            this.textPassword.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textPassword.Name = "textPassword";
            this.textPassword.PasswordChar = '*';
            this.textPassword.Size = new System.Drawing.Size(175, 26);
            this.textPassword.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(63, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "User Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(74, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(55, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "Session Key";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 169F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 169F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.Controls.Add(this.pbCreate, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.textNPNewCounter, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.label10, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.label9, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.pbRetrieve, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.pbUpdate, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.pbDelete, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.label4, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label6, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label7, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label8, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.textReportId, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.textTitle, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.textDescription, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.textIDType, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.textComment, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.pbNonPrimitiveTest, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.textNPCounter, 2, 7);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(78, 220);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 11;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 6F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 6F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(731, 384);
            this.tableLayoutPanel1.TabIndex = 13;
            // 
            // pbCreate
            // 
            this.pbCreate.Location = new System.Drawing.Point(543, 145);
            this.pbCreate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbCreate.Name = "pbCreate";
            this.pbCreate.Size = new System.Drawing.Size(162, 37);
            this.pbCreate.TabIndex = 10;
            this.pbCreate.Text = "Create";
            this.pbCreate.UseVisualStyleBackColor = true;
            this.pbCreate.Click += new System.EventHandler(this.pbCreate_Click);
            // 
            // textNPNewCounter
            // 
            this.textNPNewCounter.Location = new System.Drawing.Point(194, 305);
            this.textNPNewCounter.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textNPNewCounter.Name = "textNPNewCounter";
            this.textNPNewCounter.Size = new System.Drawing.Size(175, 26);
            this.textNPNewCounter.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(87, 306);
            this.label10.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 20);
            this.label10.TabIndex = 18;
            this.label10.Text = "New Counter";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(97, 261);
            this.label9.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 20);
            this.label9.TabIndex = 17;
            this.label9.Text = "NP Counter";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(109, 11);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 20);
            this.label4.TabIndex = 12;
            this.label4.Text = "Report ID";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(150, 56);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 20);
            this.label5.TabIndex = 13;
            this.label5.Text = "Title";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(99, 101);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 20);
            this.label6.TabIndex = 14;
            this.label6.Text = "Description";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(124, 146);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 20);
            this.label7.TabIndex = 15;
            this.label7.Text = "ID Type";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(110, 191);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 20);
            this.label8.TabIndex = 16;
            this.label8.Text = "Comment";
            // 
            // textReportId
            // 
            this.textReportId.Dock = System.Windows.Forms.DockStyle.Top;
            this.textReportId.Location = new System.Drawing.Point(194, 10);
            this.textReportId.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textReportId.Name = "textReportId";
            this.textReportId.Size = new System.Drawing.Size(343, 26);
            this.textReportId.TabIndex = 0;
            this.textReportId.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // textTitle
            // 
            this.textTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.textTitle.Location = new System.Drawing.Point(194, 55);
            this.textTitle.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textTitle.Name = "textTitle";
            this.textTitle.Size = new System.Drawing.Size(343, 26);
            this.textTitle.TabIndex = 1;
            // 
            // textDescription
            // 
            this.textDescription.Dock = System.Windows.Forms.DockStyle.Top;
            this.textDescription.Location = new System.Drawing.Point(194, 100);
            this.textDescription.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textDescription.Name = "textDescription";
            this.textDescription.Size = new System.Drawing.Size(343, 26);
            this.textDescription.TabIndex = 2;
            // 
            // textIDType
            // 
            this.textIDType.Dock = System.Windows.Forms.DockStyle.Top;
            this.textIDType.Location = new System.Drawing.Point(194, 145);
            this.textIDType.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textIDType.Name = "textIDType";
            this.textIDType.Size = new System.Drawing.Size(343, 26);
            this.textIDType.TabIndex = 3;
            // 
            // textComment
            // 
            this.textComment.Dock = System.Windows.Forms.DockStyle.Top;
            this.textComment.Location = new System.Drawing.Point(194, 190);
            this.textComment.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textComment.Name = "textComment";
            this.textComment.Size = new System.Drawing.Size(343, 26);
            this.textComment.TabIndex = 4;
            // 
            // pbNonPrimitiveTest
            // 
            this.pbNonPrimitiveTest.Location = new System.Drawing.Point(543, 260);
            this.pbNonPrimitiveTest.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbNonPrimitiveTest.Name = "pbNonPrimitiveTest";
            this.pbNonPrimitiveTest.Size = new System.Drawing.Size(162, 37);
            this.pbNonPrimitiveTest.TabIndex = 11;
            this.pbNonPrimitiveTest.Text = "NPTest";
            this.pbNonPrimitiveTest.UseVisualStyleBackColor = true;
            this.pbNonPrimitiveTest.Click += new System.EventHandler(this.pbNonPrimitiveTest_Click);
            // 
            // textNPCounter
            // 
            this.textNPCounter.Location = new System.Drawing.Point(194, 260);
            this.textNPCounter.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textNPCounter.Name = "textNPCounter";
            this.textNPCounter.Size = new System.Drawing.Size(175, 26);
            this.textNPCounter.TabIndex = 5;
            // 
            // pbEncrypt
            // 
            this.pbEncrypt.Location = new System.Drawing.Point(510, 15);
            this.pbEncrypt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbEncrypt.Name = "pbEncrypt";
            this.pbEncrypt.Size = new System.Drawing.Size(162, 62);
            this.pbEncrypt.TabIndex = 7;
            this.pbEncrypt.Text = "Encrypt Random";
            this.pbEncrypt.UseVisualStyleBackColor = true;
            this.pbEncrypt.Click += new System.EventHandler(this.pbEncrypt_Click);
            // 
            // pbEncryptRaw
            // 
            this.pbEncryptRaw.Location = new System.Drawing.Point(678, 12);
            this.pbEncryptRaw.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pbEncryptRaw.Name = "pbEncryptRaw";
            this.pbEncryptRaw.Size = new System.Drawing.Size(162, 62);
            this.pbEncryptRaw.TabIndex = 8;
            this.pbEncryptRaw.Text = "Encrypt Raw";
            this.pbEncryptRaw.UseVisualStyleBackColor = true;
            this.pbEncryptRaw.Click += new System.EventHandler(this.pbEncryptRaw_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(51, 159);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(103, 20);
            this.label11.TabIndex = 13;
            this.label11.Text = "Password (E)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(39, 124);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(114, 20);
            this.label12.TabIndex = 12;
            this.label12.Text = "User Name (E)";
            // 
            // textPasswordE
            // 
            this.textPasswordE.Location = new System.Drawing.Point(159, 155);
            this.textPasswordE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textPasswordE.Name = "textPasswordE";
            this.textPasswordE.Size = new System.Drawing.Size(610, 26);
            this.textPasswordE.TabIndex = 5;
            // 
            // textUserNameE
            // 
            this.textUserNameE.Location = new System.Drawing.Point(159, 120);
            this.textUserNameE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textUserNameE.Name = "textUserNameE";
            this.textUserNameE.Size = new System.Drawing.Size(610, 26);
            this.textUserNameE.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(976, 619);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.textPasswordE);
            this.Controls.Add(this.textUserNameE);
            this.Controls.Add(this.pbEncryptRaw);
            this.Controls.Add(this.pbEncrypt);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textPassword);
            this.Controls.Add(this.pbLogin);
            this.Controls.Add(this.textSessionKey);
            this.Controls.Add(this.textUsername);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form1";
            this.Text = "9.1";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button pbRetrieve;
        private System.Windows.Forms.TextBox textUsername;
        private System.Windows.Forms.TextBox textSessionKey;
        private System.Windows.Forms.Button pbUpdate;
        private System.Windows.Forms.Button pbDelete;
        private System.Windows.Forms.Button pbLogin;
        private System.Windows.Forms.TextBox textPassword;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textReportId;
        private System.Windows.Forms.TextBox textTitle;
        private System.Windows.Forms.TextBox textDescription;
        private System.Windows.Forms.TextBox textIDType;
        private System.Windows.Forms.TextBox textComment;
        private System.Windows.Forms.Button pbEncrypt;
        private System.Windows.Forms.Button pbNonPrimitiveTest;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textNPCounter;
        private System.Windows.Forms.TextBox textNPNewCounter;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button pbCreate;
        private System.Windows.Forms.Button pbEncryptRaw;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textPasswordE;
        private System.Windows.Forms.TextBox textUserNameE;
    }
}


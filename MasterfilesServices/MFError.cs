﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UbiSqlFramework;
using UbiWeb;
using UbiWeb.Services;
using UbiWeb.Security;

namespace MasterfilesServices
{
    public static class MFError
    {
        public enum ErrorTypes
        {
            InvalidKey,
            Other
        }

        public static bool ValidateKey(string key)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["UseSecurityTokens"] != "Y")
            {
                return true;
            }
            else
            {
                return SecurityManager.ValidateToken(key);
            }
        }


        //public static StringResponse GenerateStringResponse(ErrorTypes e)
        //{
        //    StringResponse r = new StringResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static StringResponse GenerateStringResponse(ErrorTypes e, string msg)
        //{
        //    StringResponse r = GenerateStringResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}


        public static BoolResponse GenerateBoolResponse(ErrorTypes e)
        {
            BoolResponse r = new BoolResponse(false);
            if (e == ErrorTypes.InvalidKey)
            {
                r.ErrorMessage = "Invalid token";
            }
            return r;
        }
        public static BoolResponse GenerateBoolResponse(ErrorTypes e, string msg)
        {
            BoolResponse r = GenerateBoolResponse(e);
            r.ErrorMessage = msg;
            return r;
        }
               
        public static LoginResponse GenerateLoginResponse(ErrorTypes e)
        {
            LoginResponse r = new LoginResponse(false);
            if (e == ErrorTypes.InvalidKey)
            {
                r.ErrorMessage = "Invalid token";
            }
            return r;
        }
        public static LoginResponse GenerateLoginResponse(ErrorTypes e, string msg)
        {
            LoginResponse r = GenerateLoginResponse(e);
            r.ErrorMessage = msg;
            return r;
        }

        //public static SupplierSearchResultResponse GenerateSupplierSearchResultResponse(ErrorTypes e)
        //{
        //    SupplierSearchResultResponse r = new SupplierSearchResultResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static SupplierSearchResultResponse GenerateSupplierSearchResultResponse(ErrorTypes e, string msg)
        //{
        //    SupplierSearchResultResponse r = GenerateSupplierSearchResultResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}


        //public static SupplierResponse GenerateSupplierResponse(ErrorTypes e)
        //{
        //    SupplierResponse r = new SupplierResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static SupplierResponse GenerateSupplierResponse(ErrorTypes e, string msg)
        //{
        //    SupplierResponse r = GenerateSupplierResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}


        //public static CustomerSearchResultResponse GenerateCustomerSearchResultResponse(ErrorTypes e)
        //{
        //    CustomerSearchResultResponse r = new CustomerSearchResultResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static CustomerSearchResultResponse GenerateCustomerSearchResultResponse(ErrorTypes e, string msg)
        //{
        //    CustomerSearchResultResponse r = GenerateCustomerSearchResultResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}


        //public static CustomerResponse GenerateCustomerResponse(ErrorTypes e)
        //{
        //    CustomerResponse r = new CustomerResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static CustomerResponse GenerateCustomerResponse(ErrorTypes e, string msg)
        //{
        //    CustomerResponse r = GenerateCustomerResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}


        //public static CustomerAddressResponse GenerateCustomerAddressResponse(ErrorTypes e)
        //{
        //    CustomerAddressResponse r = new CustomerAddressResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static CustomerAddressResponse GenerateCustomerAddressResponse(ErrorTypes e, string msg)
        //{
        //    CustomerAddressResponse r = GenerateCustomerAddressResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}


        //public static CustomerContactResponse GenerateCustomerContactResponse(ErrorTypes e)
        //{
        //    CustomerContactResponse r = new CustomerContactResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static CustomerContactResponse GenerateCustomerContactResponse(ErrorTypes e, string msg)
        //{
        //    CustomerContactResponse r = GenerateCustomerContactResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}

        //public static CustomerOrderTermsResponse GenerateCustomerOrderTermsResponse(ErrorTypes e)
        //{
        //    CustomerOrderTermsResponse r = new CustomerOrderTermsResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static CustomerOrderTermsResponse GenerateCustomerOrderTermsResponse(ErrorTypes e, string msg)
        //{
        //    CustomerOrderTermsResponse r = GenerateCustomerOrderTermsResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}


        //public static ContextProfileResponse GenerateContextProfileResponse(ErrorTypes e)
        //{
        //    ContextProfileResponse r = new ContextProfileResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static ContextProfileResponse GenerateContextProfileResponse(ErrorTypes e, string msg)
        //{
        //    ContextProfileResponse r = GenerateContextProfileResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}

        //public static DocTemplateResponse GenerateDocTemplateResponse(ErrorTypes e)
        //{
        //    DocTemplateResponse r = new DocTemplateResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static DocTemplateResponse GenerateDocTemplateResponse(ErrorTypes e, string msg)
        //{
        //    DocTemplateResponse r = GenerateDocTemplateResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}

        //public static FileReturnResponse GenerateFileReturnResponse(ErrorTypes e)
        //{
        //    FileReturnResponse r = new FileReturnResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static FileReturnResponse GenerateFileReturnResponse(ErrorTypes e, string msg)
        //{
        //    FileReturnResponse r = GenerateFileReturnResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}

        //public static StockResponse GenerateStockResponse(ErrorTypes e)
        //{
        //    StockResponse r = new StockResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static StockResponse GenerateStockResponse(ErrorTypes e, string msg)
        //{
        //    StockResponse r = GenerateStockResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}


        //public static PurchaseOrderResponse GeneratePurchaseOrderResponse(ErrorTypes e)
        //{
        //    PurchaseOrderResponse r = new PurchaseOrderResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static PurchaseOrderResponse GeneratePurchaseOrderResponse(ErrorTypes e, string msg)
        //{
        //    PurchaseOrderResponse r = GeneratePurchaseOrderResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}


        //public static SalesOrderResponse GenerateSalesOrderResponse(ErrorTypes e)
        //{
        //    SalesOrderResponse r = new SalesOrderResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static SalesOrderResponse GenerateSalesOrderResponse(ErrorTypes e, string msg)
        //{
        //    SalesOrderResponse r = GenerateSalesOrderResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}


        //public static MasterfilesJobSearchResultResponse GenerateMasterfilesJobSearchResultResponse(ErrorTypes e)
        //{
        //    MasterfilesJobSearchResultResponse r = new MasterfilesJobSearchResultResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static MasterfilesJobSearchResultResponse GenerateMasterfilesJobSearchResultResponse(ErrorTypes e, string msg)
        //{
        //    MasterfilesJobSearchResultResponse r = GenerateMasterfilesJobSearchResultResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}


        //public static MasterfilesJobResponse GenerateMasterfilesJobResponse(ErrorTypes e)
        //{
        //    MasterfilesJobResponse r = new MasterfilesJobResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static MasterfilesJobResponse GenerateMasterfilesJobResponse(ErrorTypes e, string msg)
        //{
        //    MasterfilesJobResponse r = GenerateMasterfilesJobResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}


        //public static ReportQuickHeaderResponse GenerateReportQuickHeaderResponse(ErrorTypes e)
        //{
        //    ReportQuickHeaderResponse r = new ReportQuickHeaderResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static ReportQuickHeaderResponse GenerateReportQuickHeaderResponse(ErrorTypes e, string msg)
        //{
        //    ReportQuickHeaderResponse r = GenerateReportQuickHeaderResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}

        public static TransactionResponse GenerateTransactionResponse(ErrorTypes e)
        {
            TransactionResponse r = new TransactionResponse(false);
            if (e == ErrorTypes.InvalidKey)
            {
                r.ErrorMessage = "Invalid token";
            }
            return r;
        }
        public static TransactionResponse GenerateTransactionResponse(ErrorTypes e, string msg)
        {
            TransactionResponse r = GenerateTransactionResponse(e);
            r.ErrorMessage = msg;
            return r;
        }

        //public static StockItemMergeCollectionResponse GenerateStockItemMergeCollectionResponse(ErrorTypes e)
        //{
        //    StockItemMergeCollectionResponse r = new StockItemMergeCollectionResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static StockItemMergeCollectionResponse GenerateStockItemMergeCollectionResponse(ErrorTypes e, string msg)
        //{
        //    StockItemMergeCollectionResponse r = GenerateStockItemMergeCollectionResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}



        //public static StockItemSalesContractCollectionResponse GenerateStockItemSalesContractCollectionResponse(ErrorTypes e)
        //{
        //    StockItemSalesContractCollectionResponse r = new StockItemSalesContractCollectionResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static StockItemSalesContractCollectionResponse GenerateStockItemSalesContractCollectionResponse(ErrorTypes e, string msg)
        //{
        //    StockItemSalesContractCollectionResponse r = GenerateStockItemSalesContractCollectionResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}


        //public static StockItemInvoiceCollectionResponse GenerateStockItemInvoiceCollectionResponse(ErrorTypes e)
        //{
        //    StockItemInvoiceCollectionResponse r = new StockItemInvoiceCollectionResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static StockItemInvoiceCollectionResponse GenerateStockItemInvoiceCollectionResponse(ErrorTypes e, string msg)
        //{
        //    StockItemInvoiceCollectionResponse r = GenerateStockItemInvoiceCollectionResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}


        //public static StockItemPurchaseOrderCollectionResponse GenerateStockItemPurchaseOrderCollectionResponse(ErrorTypes e)
        //{
        //    StockItemPurchaseOrderCollectionResponse r = new StockItemPurchaseOrderCollectionResponse(false);
        //    if (e == ErrorTypes.InvalidKey)
        //    {
        //        r.ErrorMessage = "Invalid token";
        //    }
        //    return r;
        //}
        //public static StockItemPurchaseOrderCollectionResponse GenerateStockItemPurchaseOrderCollectionResponse(ErrorTypes e, string msg)
        //{
        //    StockItemPurchaseOrderCollectionResponse r = GenerateStockItemPurchaseOrderCollectionResponse(e);
        //    r.ErrorMessage = msg;
        //    return r;
        //}

        public static ReturnDataSetResponse GenerateReturnDataSetResponse(ErrorTypes e)
        {
            ReturnDataSetResponse r = new ReturnDataSetResponse(false);
            if (e == ErrorTypes.InvalidKey)
            {
                r.ErrorMessage = "Invalid token";
            }
            return r;
        }
        public static ReturnDataSetResponse GenerateReturnDataSetResponse(ErrorTypes e, string msg)
        {
            ReturnDataSetResponse r = GenerateReturnDataSetResponse(e);
            r.ErrorMessage = msg;
            return r;
        }

    }
}
﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using UbiSqlFramework;
using UbiWeb.Services;
using UbiWeb.Security;

namespace MasterfilesServices
{
    public static class MFController
    {

        //private static XmlDocument xmlSettings;
        //private static string sAppPath;
        //private static string sLastError;

        public static SqlConnection DBConn
        {
            get
            {
                return UbiSqlFramework.SqlHost.DBConn;
            }
        }

        static MFController()
        {
            LoadControllerSettings();
        }


        //public static string LastError
        //{
        //    get { return sLastError; }
        //    set { sLastError = value; }
        //}

        //public static string Test()
        //{
        //    return "Successful Test";
        //}

        private static bool LoadControllerSettings()
        {
            try
            {
                if (!OpenConnection())
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool OpenConnection()
        {
            try
            {
                SqlHost.OpenServiceConnection();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }



        //public static string ConfigValue(string sNodeName, string sAttrib)
        //{
        //    XmlNode xmlN = xmlSettings.SelectSingleNode(sNodeName);
        //    XmlElement xmlE = (XmlElement)xmlN;
        //    string sVal = xmlE.GetAttribute(sAttrib);
        //    return sVal;
        //}
        //public static string ConfigValue(string sFullPath)
        //{
        //    XmlNode xmlN = xmlSettings.SelectSingleNode(sFullPath);
        //    return (string)xmlN.Value;
        //}
        //public static XmlElement ConfigElement(string sElementName)
        //{
        //    XmlElement xlE = xmlSettings.GetElementById(sElementName);
        //    return xlE;
        //}
        //public static string AppPath
        //{
        //    get { return sAppPath; }
        //}

        //public static StringResponse GetNextSQLCounter(string key, string sId)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateStringResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.GetNextCBASqlServer(sId);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        StringResponse r = MFError.GenerateStringResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}

        //Method LogingUser used for the ServiceTestApp project
        public static LoginResponse LoginUser(string userName, string password)
        {
            try
            {
                string un = SecurityManager.DecryptFromRandom(userName);
                string pwd = SecurityManager.DecryptFromRandom(password);
                LoginResponse r = UbiServiceHost.LoginUser(un, pwd);
                if (r.Success)
                {
                    r.Response.SessionKey = SecurityManager.GenerateToken("Valid");
                    return r;
                }
                else
                {
                    return r;
                }
            }
            catch (Exception ex)
            {
                LoginResponse r = MFError.GenerateLoginResponse(MFError.ErrorTypes.Other, ex.Message);
                return r;
            }
        }

        //public static BoolResponse GetUser(string key, string sUserName, string sPassword)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateBoolResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.GetUser(sUserName, sPassword);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        BoolResponse r = MFError.GenerateBoolResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}

        //public static SupplierSearchResultResponse SupplierSearch(string key, string companyId, string searchExpression, string parentId)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateSupplierSearchResultResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.SupplierSearch(companyId, searchExpression, parentId);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        SupplierSearchResultResponse r = MFError.GenerateSupplierSearchResultResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}

        //public static SupplierResponse GetSupplier(string key, string companyId, string supplierId)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateSupplierResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.GetSupplier(companyId, supplierId);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        SupplierResponse r = MFError.GenerateSupplierResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}

        //public static CustomerSearchResultResponse CustomerSearch(string key, string companyId, string searchExpression)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateCustomerSearchResultResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.CustomerSearch(companyId, searchExpression);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        CustomerSearchResultResponse r = MFError.GenerateCustomerSearchResultResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}
        //public static CustomerResponse GetCustomer(string key, string companyId, string customerId)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateCustomerResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.GetCustomer(companyId, customerId);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        CustomerResponse r = MFError.GenerateCustomerResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}
        //public static CustomerAddressResponse GetCustomerAddress(string key, string customerId, string addressId)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateCustomerAddressResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.GetCustomerAddress(customerId, addressId);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        CustomerAddressResponse r = MFError.GenerateCustomerAddressResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}
        //public static CustomerContactResponse GetCustomerContact(string key, string customerId, string contactId)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateCustomerContactResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.GetCustomerContact(customerId, contactId);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        CustomerContactResponse r = MFError.GenerateCustomerContactResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}

        //public static CustomerOrderTermsResponse GetCustomerOrderTerms(string key, string orderTermsId)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateCustomerOrderTermsResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.GetCustomerOrderTerms(orderTermsId);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        CustomerOrderTermsResponse r = MFError.GenerateCustomerOrderTermsResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}

        //public static ContextProfileResponse GetContextProfile(string key, string contextId, string currencyId, string countryId)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateContextProfileResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.GetContextProfile(contextId, currencyId, countryId);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ContextProfileResponse r = MFError.GenerateContextProfileResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}

        //public static DocTemplateResponse GetDocTemplate(string key, string templateCode)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateDocTemplateResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.GetDocTemplate(templateCode);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        DocTemplateResponse r = MFError.GenerateDocTemplateResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}
        //public static FileReturnResponse SetFileReturn(string key, string PrintFile)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateFileReturnResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.SetFileReturn(PrintFile);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        FileReturnResponse r = MFError.GenerateFileReturnResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}
        //public static StockResponse GetStock(string key, string stockCode, string lotId, string whsId)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateStockResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.GetStock(stockCode, lotId, whsId);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        StockResponse r = MFError.GenerateStockResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}

        //public static BoolResponse POExists(string key, string poNum)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateBoolResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.POExists(poNum);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        BoolResponse r = MFError.GenerateBoolResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}
        //public static PurchaseOrderResponse RetrievePO(string key, string poNum, bool byRef, bool headerOnly)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GeneratePurchaseOrderResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.RetrievePurchaseOrder(poNum, byRef, headerOnly);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        PurchaseOrderResponse r = MFError.GeneratePurchaseOrderResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}

        //public static BoolResponse OEExists(string key, string orderNo)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateBoolResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.OEExists(orderNo);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        BoolResponse r = MFError.GenerateBoolResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}
        //public static SalesOrderResponse RetrieveOE(string key, string orderNo, bool headerOnly)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateSalesOrderResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.RetrieveSalesOrder(orderNo, headerOnly);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        SalesOrderResponse r = MFError.GenerateSalesOrderResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}


        //public static BoolResponse MasterfilesJobExists(string key, string jobNo)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateBoolResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.MasterfilesJobExists(jobNo);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        BoolResponse r = MFError.GenerateBoolResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}
        //public static MasterfilesJobSearchResultResponse MasterfilesJobSearch(string key, string jobNo)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateMasterfilesJobSearchResultResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.MasterfilesJobSearch(jobNo);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MasterfilesJobSearchResultResponse r = MFError.GenerateMasterfilesJobSearchResultResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}
        //public static MasterfilesJobResponse RetrieveMasterfilesJob(string key, string jobNo)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateMasterfilesJobResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.RetrieveMasterfilesJob(jobNo);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MasterfilesJobResponse r = MFError.GenerateMasterfilesJobResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}


        //public static ReportQuickHeaderResponse GetReportQuickHeader(string key, string reportId)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateReportQuickHeaderResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.GetReportQuickHeader(reportId);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ReportQuickHeaderResponse r = MFError.GenerateReportQuickHeaderResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}

        public static TransactionResponse ProcessTransaction(string key, Transaction t)
        {
            try
            {
                if (!MFError.ValidateKey(key))
                {
                    return MFError.GenerateTransactionResponse(MFError.ErrorTypes.InvalidKey);
                }
                else
                {
                    return UbiServiceHost.ProcessTransaction(t);
                }
            }
            catch (Exception ex)
            {
                TransactionResponse r = MFError.GenerateTransactionResponse(MFError.ErrorTypes.Other, ex.Message);
                return r;
            }
        }

        //public static StockItemMergeCollectionResponse RetrieveStockItemMergeCollection(string key, StringList t)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateStockItemMergeCollectionResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.RetrieveStockItemMergeCollection(t);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        StockItemMergeCollectionResponse r = MFError.GenerateStockItemMergeCollectionResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}

        //public static StockItemSalesContractCollectionResponse RetrieveStockItemSalesContractCollection(string key, StringList t, string sCustId, string sWhsId)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateStockItemSalesContractCollectionResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.RetrieveStockItemSalesContractCollection(t, sCustId, sWhsId);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        StockItemSalesContractCollectionResponse r = MFError.GenerateStockItemSalesContractCollectionResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}

        //public static StockItemInvoiceCollectionResponse RetrieveStockItemInvoiceCollection(string key, StringList t, string sCustId, string sWhsId)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateStockItemInvoiceCollectionResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.RetrieveStockItemInvoiceCollection(t, sCustId, sWhsId);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        StockItemInvoiceCollectionResponse r = MFError.GenerateStockItemInvoiceCollectionResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}


        ////RetrieveStockItemPurchaseOrderCollection
        //public static StockItemPurchaseOrderCollectionResponse RetrieveStockItemPurchaseOrderCollection(string key, StringList t, string sCustId, string sWhsId)
        //{
        //    try
        //    {
        //        if (!MFError.ValidateKey(key))
        //        {
        //            return MFError.GenerateStockItemPurchaseOrderCollectionResponse(MFError.ErrorTypes.InvalidKey);
        //        }
        //        else
        //        {
        //            return UbiServiceHost.RetrieveStockItemPurchaseOrderCollection(t, sCustId, sWhsId);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        StockItemPurchaseOrderCollectionResponse r = MFError.GenerateStockItemPurchaseOrderCollectionResponse(MFError.ErrorTypes.Other, ex.Message);
        //        return r;
        //    }
        //}


        //public static UbiWeb.Data.NonPrimitive IncrementNonPrimitive(UbiWeb.Data.NonPrimitive p)
        //{
        //    return UbiServiceHost.IncrementPrimitive(p);
        //}

        public static ReturnDataSetResponse SetReturnDataSet(string key, string sSql, string sTable, ref DataSet oDs)
        {
            try
            {
                if (!MFError.ValidateKey(key))
                {
                    return MFError.GenerateReturnDataSetResponse(MFError.ErrorTypes.InvalidKey);
                }
                else
                {
                    return UbiServiceHost.SetReturnDataSet(sSql, sTable, ref oDs);
                }
            }
            catch (Exception ex)
            {
                ReturnDataSetResponse r = MFError.GenerateReturnDataSetResponse(MFError.ErrorTypes.Other, ex.Message);
                return r;
            }
        }

    }

}

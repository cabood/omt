﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using UbiWeb;
using UbiWeb.Services;
using UbiWeb.Security;
using UbiWeb.Data;
using System.IO;
using System.Xml;
using System.Data;
using System.Data.SqlClient;

namespace MasterfilesServices
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    //[WebService(Namespace = "http://ws.unclebills.com.au:17000/MasterfilesServices/", Description="Masterfiles Services Page")]
    [WebService(Namespace = "http://192.168.1.28:17500/MasterfilesServices/", Description = "Masterfiles Services Page")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class MFServices : System.Web.Services.WebService
    {

        //[WebMethod]
        //public StringResponse TestStringResponse()
        //{
        //    StringResponse r = new StringResponse();
        //    r.Success = true;
        //    r.Response = "Successful Test";
        //    return r;
        //}

        //[WebMethod]
        //public string ShowLastError()
        //{
        //    return MFController.LastError;
        //}

        //[WebMethod]
        //public NonPrimitive IncrementNonPrimitive(NonPrimitive p)
        //{
        //    return MFController.IncrementNonPrimitive(p);
        //}

        [WebMethod]
        public LoginResponse LoginUser(string userName, string password)
        {
            return MFController.LoginUser(userName, password);
        }

        //Functionality now done via above method LoginUser
        //[WebMethod]
        //public BoolResponse GetUser(string key, string sUserName, string sPassword)
        //{
        //    return MFController.GetUser(key, sUserName, sPassword);
        //}

        //[WebMethod]
        //public StringResponse GetNextId(string key, string id)
        //{
        //    return MFController.GetNextSQLCounter(key, id);
        //}

        [WebMethod]
        public TransactionResponse ProcessTransaction(string key, Transaction tran)
        {
            return MFController.ProcessTransaction(key, tran);
        }

        //[WebMethod]
        //public SupplierSearchResultResponse SupplierSearch(string key, string companyId, string searchExpression, string parentId)
        //{
        //    return MFController.SupplierSearch(key, companyId, searchExpression, parentId);
        //}

        //[WebMethod]
        //public SupplierResponse GetSupplier(string key, string companyId, string supplierId)
        //{
        //    return MFController.GetSupplier(key, companyId, supplierId);
        //}

        //[WebMethod]
        //public CustomerSearchResultResponse CustomerSearch(string key, string companyId, string searchExpression)
        //{
        //    return MFController.CustomerSearch(key, companyId, searchExpression);
        //}

        //[WebMethod]
        //public CustomerResponse GetCustomer(string key, string companyId, string customerId)
        //{
        //    return MFController.GetCustomer(key, companyId, customerId);
        //}

        //[WebMethod]
        //public CustomerAddressResponse GetCustomerAddress(string key, string customerId, string addressId)
        //{
        //    return MFController.GetCustomerAddress(key, customerId, addressId);
        //}

        //[WebMethod]
        //public CustomerContactResponse GetCustomerContact(string key, string customerId, string contactId)
        //{
        //    return MFController.GetCustomerContact(key, customerId, contactId);
        //}

        //[WebMethod]
        //public CustomerOrderTermsResponse GetCustomerOrderTerms(string key, string orderTermsId)
        //{
        //    return MFController.GetCustomerOrderTerms(key, orderTermsId);
        //}

        //[WebMethod]
        //public StockResponse GetStock(string key, string stockCode, string lotId, string whsId)
        //{
        //    return MFController.GetStock(key, stockCode, lotId, whsId);
        //}

        //[WebMethod]
        //public BoolResponse POExists(string key, string poNum)
        //{
        //    return MFController.POExists(key, poNum);
        //}
        //[WebMethod]
        //public PurchaseOrderResponse RetrievePO(string key, string poNum, bool byRef, bool headerOnly)
        //{
        //    return MFController.RetrievePO(key, poNum, byRef, headerOnly);
        //}

        //[WebMethod]
        //public BoolResponse OEExists(string key, string orderNo)
        //{
        //    return MFController.OEExists(key, orderNo);
        //}
        //[WebMethod]
        //public SalesOrderResponse RetrieveOE(string key, string orderNo, bool headerOnly)
        //{
        //    return MFController.RetrieveOE(key, orderNo, headerOnly);
        //}

        //[WebMethod]
        //public BoolResponse MasterfilesJobExists(string key, string jobNo)
        //{
        //    return MFController.MasterfilesJobExists(key, jobNo);
        //}
        //[WebMethod]
        //public MasterfilesJobSearchResultResponse MasterfilesJobSearch(string key, string jobNo)
        //{
        //    return MFController.MasterfilesJobSearch(key, jobNo);
        //}
        //[WebMethod]
        //public MasterfilesJobResponse RetrieveMasterfilesJob(string key, string jobNo)
        //{
        //    return MFController.RetrieveMasterfilesJob(key, jobNo);
        //}
        //[WebMethod]
        //public StockItemMergeCollectionResponse RetrieveStockItemMergeCollection(string key, StringList mergeList)
        //{
        //    return MFController.RetrieveStockItemMergeCollection(key, mergeList);
        //}
        //[WebMethod]
        //public StockItemSalesContractCollectionResponse RetrieveStockItemSalesContractCollection(string key, StringList mergeList, string custId, string whsId)
        //{
        //    return MFController.RetrieveStockItemSalesContractCollection(key, mergeList, custId, whsId);
        //}
        //[WebMethod]
        //public StockItemInvoiceCollectionResponse RetrieveStockItemInvoiceCollection(string key, StringList mergeList, string custId, string whsId)
        //{
        //    return MFController.RetrieveStockItemInvoiceCollection(key, mergeList, custId, whsId);
        //}
        //[WebMethod]
        //public StockItemPurchaseOrderCollectionResponse RetrieveStockItemPurchaseOrderCollection(string key, StringList mergeList, string custId, string whsId)
        //{
        //    return MFController.RetrieveStockItemPurchaseOrderCollection(key, mergeList, custId, whsId);
        //}

        //[WebMethod]
        //public ReportQuickHeaderResponse GetReportQuickHeader(string key, string reportId)
        //{
        //    return MFController.GetReportQuickHeader(key, reportId);
        //}

        //[WebMethod]
        //public ContextProfileResponse GetContextProfile(string key, string contextId, string currencyId, string countryId)
        //{
        //    return MFController.GetContextProfile(key, contextId, currencyId, countryId);
        //}

        //[WebMethod]
        //public DocTemplateResponse GetDocTemplate(string key, string templateCode)
        //{
        //    return MFController.GetDocTemplate(key, templateCode);
        //}

        //[WebMethod]
        //public FileReturnResponse SetFileReturn(string key, string PrintFile)
        //{
        //    return MFController.SetFileReturn(key, PrintFile);
        //}

        [WebMethod]
        public ReturnDataSetResponse SetReturnDataSet(string key, string sSql, string sTable, ref DataSet oDs)
        {
            return MFController.SetReturnDataSet(key, sSql, sTable, ref oDs);
        }

    }
}
